# About ANTARES

ANTARES is an Astronomy Alert Broker developed by the [NSF NOIRLab](noirlab.edu)
for optical surveys like ZTF and LSST. You can visit our deployment of ANTARES
at https://antares.noirlab.edu.

# Developer's Guide

## Helpful Links

* GitLab Repositories:
  - Main ANTARES code base:https://gitlab.com/nsf-noirlab/csdc/antares/antares 
  - ANTARES client library: https://gitlab.com/nsf-noirlab/csdc/antares/client 
  - ANTARES Helm charts (shared with other projects): https://gitlab.com/nsf-noirlab/helm-charts 
* JIRA: https://noirlab.atlassian.net/browse/AN

## Environment Setup

This section describes how to set up the recommended environment for contributing to ANTARES development. This guide was written for Debian-based Linux distributions but should work with little modification on MacOS.
Overview

You will need to install:

* An IDE (we strongly recommend PyCharm “Professional”: https://www.jetbrains.com/pycharm/ , which is free for educators and students)
* Docker (https://docs.docker.com/get-docker/ )
* Docker Compose (https://docs.docker.com/compose/install/ )

The rest of this guide assumes that you’ve installed PyCharm Professional.

1. Clone the ANTARES repository (you might need to add SSH keys to your GitLab account if you haven’t already, see: https://docs.gitlab.com/ee/ssh/ )
   ```shell
   git clone git@gitlab.com:nsf-noirlab/csdc/antares/antares
   cd antares
   ```
2. Open the newly cloned folder in PyCharm
3. Set up a new, Docker Compose interpreter in PyCharm: https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html#configuring-docker 
    * You might need to create a new Docker server.
    * The setting for “Configuration files:” should be .docker-compose.yml; .docker-compose.override.yml by default, change it to this if not.
    * In the “Service:” dropdown choose test. This is the docker-compose service that we’ll use to execute code in.
    * Accept the rest of the default settings, it might take a while to start as it will have to pull the Docker images if you don’t have them.
4. Test that your environment was set up correctly by running the unit test suite.
    * In the upper right corner of PyCharm select the “Unit Test” run configuration and click the Play button (or press Shift + F10)
    * All tests should pass (some are ignored)

## Development Guidelines

* Always do your work on a feature branch and create a merge request on GitLab when you are ready for the work to be merged. E.g.: 
  ```shell
  git checkout v2.0.0
  git checkout -b my-new-feature
  <work, work, work>
  git add .
  git commit -m "Description of work done"
  git push
  ```
* We practice test-driven development so always write a failing test before you write new code.
* To test new features, it is recommended to load some data when starting all containers. This
can be done following the instructions from https://gitlab.com/nsf-noirlab/csdc/antares/misc-repositories/load_local_data .
* We use the tool Black (https://github.com/psf/black ) as our code formatter. We follow most PEP8 guidelines except for the line length one, which Black relaxes to 88 characters.
* Write documentation for all modules and public functions. 
* We use the NumPy docstring format for documentation: https://numpydoc.readthedocs.io/en/latest/format.html 

# The ANTARES Pipeline

The science pipeline is likely the most complicated (and mission-critical)
part of ANTARES and for that reason alone deserves some accompanying text to explain
its rationale and structure. Additionally, the code within the `antares.pipeline`
module is in the midst of a refactoring process and it may confuse a new reader.

## Real-Time Processing in ANTARES

Our processing strategy comprises two high-level components: 1) ingestion and 2) science
processing. When we receive packets of alerts from surveys we carry out the following
steps:

### Ingestion

1. Find if there is a locus in our database that meets some "association criteria" that
   allows us to establish an ownership relationship from the locus to the new alerts.
   Typically this is spatial in nature (the nearest object within one arcsecond of the
   new alerts, say). Different surveys can have more complex association strategies--ZTF
   alerts for example use this one:
   
   1. Look for any locus within five arcseconds that has already been associated with
      another alert that shares a ZTF object ID with this one (i.e. defer to ZTF's
      association).
   2. If none found, look for the nearest locus within one arcsecond.
   
   Since we associate alert packets (which may contain multiple alerts) to a single
   object we also need to determine a strategy for reducing the (presumably) different
   locations of each alert w/in the packet to a single location. The usual strategy is
   finding the centroid of all alerts.
   
   If no viable candidates are found for association, we create and persists a new
   locus in our database and associate the alert with that one.
   
 ### Science Processing
 
 1. Following the successful ingestion of an alert packet, we kick off a processing job
    that runs scientific workflow over the new data. This workflow may look a bit
    different depending on the event that we're handling and the source of the new data.
    Generally we follow these steps:
    
    1. Fetch the entire history of data ("alert history") for the locus that we've
       seen new activity on.
    
    2. Run preliminary ("Level One" or "L1") filters over the new data. These are
       responsible for determining if the new information is real or bogus, if the
       signal-to-noise ratio is high or low, and make a decision as to whether or not
       further processing should be carried out.
    
    3. Find associations in our catalog (or "astro objects") database.
    
    4. Find associations in our user-defined "watch list" database (which is used to
       notify users of activity in particular locations).
       
    5. Compute updated features for the locus (e.g. how the period of the lightcurve
       has changed).
       
    6. Run user-defined filters (these may annotate the locus with new "tags", or
       themselves compute updated features).
       
    7. Persist changes to the databases (in our current, concrete implementation this
       involves saving the updated locus in Cassandra and also pushing a subset of its
       data into our ElasticSearch cluster for use as a search index).
       
    8. Send notifications to users about data that they've flagged as interesting.
