Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/

Unreleased
------------------

v.2.18.0 (08/28/2024)
------------------

Changed
~~~~~~~

- Kafka client consumers will use an specific logger. This helps changing the logging level of the `KafkaConsumer` class.

v.2.17.0 (08/23/2024)
------------------

Added
~~~~~~~

- Added `stamp_extractor` service. This service was in a different repository and didn't use helm charts.


v.2.16.1 (08/14/2024)
------------------

Fixed
~~~~~~~

- Fixed a bug that caused GCP metrics to be dropped off.

v.2.16.0 (08/13/2024)
------------------

Fixed
~~~~~~~

- Fixed a bug that caused flask to be instrumented twice. 

v.2.15.0 (08/12/2024)
------------------

Added
~~~~~~~

- Added tracing instrumentation in Flask when using GCP

v.2.14.0 (08/08/2024)
------------------

Added
~~~~~~~

- Acknowledgements docs page (same as antares-client) considering NSF rebranding requirements
- `fp_hists` is available to user filters. This is a non_persistant_variable which means won't be stored in the database.
- Fix gitlab-ci authentication to push docker images
- Added an option to send metrics to GCP
- Added an option to send traces to GCP
- Added Slack invitation to the ANTARES workspace in FAQ page.
- Added missing instrumentation of redis and sqlalchemy to gcp tracing.

Changed
~~~~~~~

- Use `U.S. National Science Foundation` and `NSF NOIRLab` throughout the docs and other files as part of NSF rebranding requirements
- BigtableAlertRepository.add_many_if_not_exists no longer overwrite previous candidate alerts
- Removed Cassandra services from e2e tests in gitlab-ci.
- Removed tracing.wrap() method because it causes confusion when profiling code and shows high RAM usage.
- The `publish_locus_to_users` stage was moved to the end of the pipeline because that way we won't send duplicates to Kafka when the pipeline fails.
- Survey Enum search inside `kafka_listener` is refactored and will raise an error when a topic doesn't match any survey.
- An abstraction layer was added to the tracing component to make the opentracing to opentelemetry migration easier.
- An abstraction layer was added to the metrics component to make the opentracing to opentelemetry migration easier.
- Changed logging to use a root logger which is simpler and we don't handle multiple loggers during the execution of our code.
- REQUIRES RENAMING `config.observability.tracing.type` to `config.observability.type` from config files
- REQUIRES ADDING `ANTARES_SLACK_INVITATION` entry to frontend config files

Fixed
~~~~~~~

- Fixed `send_watchlist_notification` tracing which was appending tags to an incorrect span leading to overwriting the same value multiples times.
- Fixed a bug when marking an OpenTracing span as failed caused the span to crash.

v.2.13.0 (04/01/2024)
------------------

Changed
~~~~~~~

- All e2e tests use Bigtable instead of Cassandra. The mypy errors from those tests were fixed.
- Removed all Cassandra integration tests.
- Local environment now uses Bigtable instead of Cassandra.
- Removed Cassandra option from repositories within the settings Container.
- `mkhtm_lut.py` script file is removed. This file was used to add catalogs htm indexes into Cassandra.
- Removed Cassandra concurrency, repository code and scripts used to create Cassandra users for pipeline and api.

v.2.12.1 (03/20/2024)
------------------

Fixed
~~~~~~~

- Fixed `BigtableCatalogObjectRepository.create_catalog_object_from_row` to avoid creating an angle when catalog object has None as radius value.

v.2.12.0 (03/18/2024)
------------------

Added
~~~~~~~

- Added new `InternalSlackApiNotificationService` class which is used to send internal notitifications. **REQUIRES** changes to the deployment configuration (`config.internal_notifications.channels`)
- Added new internal notifications for filter revision transitions, sent when transitioning a filter via HTTP API and also when a filter is automatically disabled by the pipeline
- Fastavro package is upgraded from version `1.5.1` to `1.9.4`. New tests are added to ensure we can read older ztf packets.
- Gitlab CI/CD pipeline to request a deployment in development.

Changed
~~~~~~~

- Set a new `InternalSlackApiNotificationService` instance as the `internal_notification_service` singleton inside the `antares.settings.Container` definition
- Moved internal notification for submissions of new filter revisions into the `InternalSlackApiNotificationService` class
- Updated frontend team members page with newest info and removed inactive members
- Devkit uses the client to retrieve data instead of connecting to Cassandra or Bigtable directly.
- `get_locus` method has the option to add gravitational wave data. This is because retrieving gravitational wave data takes time and not all work uses that information.
- `get_locus` method no longer has the include_newer argument which was never used.

Fixed
~~~~~~~

- Fixed `/catalog_search/<float:ra>/<float:dec>` endpoint to parse negative values.

v.2.11.0 (02/22/2024)
------------------

Added
~~~~~~~

- `/catalog_samples/<int:n>` and `/catalog_search/<float:ra>/<float:dec>` endpoints available to retrieve catalog samples and cross-matches
- Added a new endpoint to retrieve gravitational wave notices which supports multiple ids by query.
- `dynesty` package is upgraded from version `1.2.3` to `2.1.3`.

v.2.10.6 (02/16/2024)
------------------

Added
~~~~~~~

- Added a check step in repositories before using rowsets in bigtable to avoid scanning entire tables.

v.2.10.5 (02/14/2024)
------------------

Changed
~~~~~~~

- The `list_by_location` method in the watch object repository for bigtable now returns an iterator so that it behaves in the same way as the Cassandra version of this method.
- The `list_by_location` method in the watch object repository now avoids scanning the entire table if the rowset is empty.

v.2.10.4 (02/13/2024)
------------------

Fixed
~~~~~~~

- Fixed index worker validation added in v.2.10.3 which was ignoring negative values and leaving loci without dec.
- Fixed watch_object to infer a name when this value is missing.

v.2.10.3 (02/13/2024)
------------------

Fixed
~~~~~~~

- Fixed index worker to check if a float is an infinity value for elasticsearch.


v.2.10.2 (02/12/2024)
------------------

Fixed
~~~~~~~

- Fixed watch_list in Bigtable to fill columns in case the column enabled or slack_channel is missing.

v.2.10.1 (02/12/2024)
------------------

Fixed
~~~~~~~

- Fixed `tags` and `catalogs` properties in Locus because their data type for BigTable was list instead of set.
- Fixed watch_list in Bigtable to fill a created_datetime when that column is missing. This happens because some records where inserted directly into the database and that column was missing.

v.2.10.0 (02/08/2024)
------------------

Added
~~~~~~~

- Locus created using the devkit now have `grav_wave_events_metadata`.
- The packages `lightgbm` and `superphot-plus` are now included.

Changed
~~~~~~~

- Filters now are updated with each alert. This "update" means that filters that are disabled will be removed from the cache while other filters that are enabled and new will be loaded. Doing this will cause each filter to only be loaded once on each pod.
- REQUIRES REMOVING filter_executable_ttl from config files

v.2.9.0 (12/05/2023)
------------------

Added
~~~~~~~

- `/grav_wave_notices/<string:gracedb_id>/latest` and `/grav_wave_notices/<string:gracedb_id>/<datetime_from:notice_datetime>` endpoints available to retrieve gravitational wave notices data.

v.2.8.0 (11/21/2023)
------------------

Changed
~~~~~~~

- Use `timestamp` instead of `ctimestamp` to avoid naming confusion.
- `/tags` endpoint now shows tags from filters that are enabled and public. Plus, its pagination works with the number of filters instead of the number of tags.
- Update astro-ghost package to 2.0.17

v.2.7.0 (10/24/2023)
------------------

Added
~~~~~~~

- Devkit can now search for a locus by `ztf_object_id`

Changed
~~~~~~~

- Use `ctimestamp` instead of `timestamp` in all bigtable repositories because Cassandra uses it. Otherwise we won't be able to decrypt the data we migrated.
- Update bigtable encoding/decoding to use proper data type names.
- All BigTable queries use the `CellsColumnLimitFilter` filter which returns only the latest version of a row.
- Devkit no longer uses a direct connection to the database via repositories to get loci, it now uses the antares_client for it.
- Removed dependencies: xmltodict, wget, pythonping, python-json-logger, apispec and argh.

v.2.6.8 (08/23/2023)
------------------

Fixed
~~~~~~~

- Fixed `list_enabled_filters` because it was retrieving all filters since sqlalchemy doesn't allow using "is not None" in filter.

v.2.6.7 (08/22/2023)
------------------

Fixed
~~~~~~~

- Fixed `list_by_catalog_id` to search for catalogs using a string casted as an integer instead of just an integer.
- Fixed logging when a filter crashes. We now display the traceback instead of a <Traceback object> message
- Fixed missing filter version persist (update) when a filter crashes in `run_user_filters`

Added
~~~~~

- Added the list_enabled_filters method in SqlAlchemyFilterRepository to avoid iterating over all filters to find out which are enabled.
- Filters that crash when trying to transform to executables are now disabled automatically.
- Added support for Cassandra-style timestamp data type (integer milliseconds since the epoch) in Catalogs

v.2.6.6 (08/16/2023)
--------------------

Added
~~~~~

- Add smallint, tinyint and bigint support in decode_any.

v.2.6.5 (08/14/2023)
--------------------

Added
~~~~~

- More logs to bigtable repositories.

v.2.6.4 (08/14/2023)
--------------------

Changed
~~~~~

- Bigtable catalogs now require a yaml that includes all schemas.

v.2.6.3 (08/03/2023)
--------------------

Added
~~~~~

- Elasticsearch index worker will now remove NaN values from locus properties.

v.2.6.2 (08/03/2023)
--------------------

Fixed
~~~~~~~

- Fixed `_run_filter` when trying to inspect the attribute `id` inside a `FilterContextAlert` and that attribute doesn't exist in that class.

Added
~~~~~

- Add unit tests to `run_filter` and `run_many` to inspect each requirement separately.
- Elasticsearch index worker will now remove NaN values.
- User filters cannot store NaN values in numeric fields.

v.2.6.1 (07/31/2023)
--------------------

Fixed
~~~~~~~

- Bigtable: Returns all Locus table rows when looking up a locus by HTM ID


v.2.6.0 (07/31/2023)
--------------------

Added
~~~~~

- New data (`transitioned_at` and `feedback`) displayed for a user in their filter version as a tooltip at the right of the status label
- New admin feature for transitioning filter versions between statuses, using buttons inside admin filter view.
- New `transitioned_at` and `feedback` fields for `FilterRevision`. A few transitions require feedback (disabling and rejecting).
- Add build_filter_context_alert method.
- installed the following libraries libgl1-mesa-glx, libgl1, libglib2.0-0
for the astro-ghost package for LAISS_RFC_AD filter

Changed
~~~~~~~

- Transitioning a filter revision to `ENABLED` or `DISABLED` is now the **only** way to enable/disable filters
- Filter revisions now require a `filter_` parameter when transitioning to `ENABLED` or `DISABLED`
  in order to trigger side effects on the parent filter
- `Filter.enable_filter_revision` and `Filter.disable` are now internal methods (`Filter._enable_filter_revision`
  and `Filter._disable`, respectively) that perform side effects when enabling/disabling a filter revision.
  These methods **must** not be called anywhere in the application besides when transitioning `FilterRevision`s
- Filters disabled by the pipeline now transition the enabled filter revision to `DISABLED` with feedback
- `FilterRevision.to_filter_executable` now requires the status to be `ENABLED` instead of `REVIEWED`

v.2.5.7 (07/27/2023)
--------------------

Added
~~~~~

- astro-ghost package addition for LAISS_RFC_AD filter

v.2.5.6 (07/27/2023)
--------------------

Changed
~~~~~~~

- Populate alert.properties with normalized properties before filter execution; fixes bug where filters skip alerts


v.2.5.5 (07/27/2023)
--------------------

Changed
~~~~~~~

- Improve logs related to filter execution.

v.2.5.4 (07/25/2023)
--------------------

Changed
~~~~~~~

- Fixed typo for the optional package numpyro

v.2.5.3 (07/12/2023)
--------------------

Added
~~~~~

- Added packages: alerce, astro-ghost, opencv-python, timeout-decorator for LAISS_RFC_AD_filter


v.2.5.2 (07/11/2023)
--------------------

Added
~~~~~

- Added packages: jax, jaxlib, numpyro for superphot_v1 filter


v.2.5.1 (07/11/2023)
--------------------

Changed
~~~~~~~

- Update poetry.lock to fix missing light-curve package

v.2.5.0 (07/06/2023)
--------------------

Added
~~~~~

- Loci can now be filtered by gravitational wave event ID (GraceDB ID) in the loci page.
  This performs an Elasticsearch wildcard query so partial IDs are allowed.
- Gravitational wave events associated with a locus are displayed in a new section inside the locus detail page.
  Each event includes treasuremap external link and a table with alerts associated to that event.

Changed
~~~~~~~

- changed filter executable caching mechanism, moved filter executable loading to repo
- user filters have a new constraint constant `REQUIRED_GRAV_WAVE_PROB_REGION`.
  If this is set to a non-zero value, the filter will only be executed if the
  triggering alert is in the probability region containing no more than the
  requirement. For example: if `REQUIRED_GRAV_WAVE_PROB_REGION = 15`,
  the filter will only be run for alerts in the top 15% region of any current
  gravitation wave notice.
- Add corrections for astropy's deprecation of Angle as truthy
- Pin fastavro as future version deprecates support of current ZTF schema
- user filters can now access a dictionary of gravitional wave event notice
  data on the DevKit's `Locus` object (the `FilterContext` object). The
  event notices are simply `GravWaveNotice` objects without a skymap, so the
  user filter has access to all the notice data in the `.full_notice` attribute,
  though the fields in the notice are not validated. User filters should be
  defensive when using that data.
- removed light-curve-python package for filters; added newer light-curve==0.7.2

v.2.4.2 (05/23/2023)
--------------------

Added
~~~~~

- `Alert` and `Locus` that are within a gravitational wave event region have `.grav_wave_events` attributes populated by the pipeline.
- New filter versions viewer inside the `FilterDetail` frontend view. It includes the ID, status and comment of the selected filter version.
- New filter versions sidebar inside the `FilterDetail` frontend view. Displayed only for admin users.
- New gravitational wave Notice methods: `get_probability_density` and
  `get_probability_contour_level_and_area`
- New GravWaveRepository methods: `get_current_notices` and
  `get_latest_active_notice_ids.`
- New Alert and Locus attribute: `grav_wave_events`. `Alert.grav_wave_events` is a list of
  dictionarys with keys `gracedb_id`, `contour_level`, and `contour_area` as listed in
  `antares.domain.models.alert.AlertGravWaveEvent`. `Locus.grav_wave_events` is a list of
  gracedb id strings.
- Add a default radius value of 1 arcsecond when there is no `radius_column` and no `radius_unit` in the catalog objects.

Changed
~~~~~~~

- `FilterRevision` model now behaves as a state machine used to handle status transitions. New status values were also added
- Remove MARS broker references as it's not active anymore
- user filters now support all of:
  `INPUT_LOCUS_PROPERTIES`, `REQUIRED_LOCUS_PROPERTIES`,
  `INPUT_ALERT_PROPERTIES`, `REQUIRED_ALERT_PROPERTIES`,
  `INPUT_TAGS`, `REQUIRED_TAGS`.
  The `INPUT_` prefixed attributes will be deprecated soon.
- References to `INPUT_{LOCUS|ALERT}_PROPERTIES` and `INPUT_TAGS` in the documentation now use the `REQUIRED` prefix
- Updates on DevKit documentation:
  - `Python 3 (ANTARES 0.4)` DevKit Kernel references removed (now it's just `Python 3 (ANTARES)`)
  - Add `https` to the ANTARES website URL in DevKit welcome message

Fixed
~~~~~

- `{REQUIRED|INPUT}_ALERT_PROPERTIES` attribute in user filters wasn't being
  used to filter alerts that a user filter should run on. Fixed.
- Catalog Match was having a separation larger than radius, this is fixed.

v.2.4.1 (04/03/2023)
--------------------

Added
~~~~~

- The Gravitational Wave Watcher, a new entrypoint
  Connects to the GCN Kafka cluster and injests LIGO/Virgo/KAGRA notices into
  the ANTARES sql database.
- New internal notifications service which will be used for sending notifications to staff, initially via Slack.
- Internal Slack notification when user submits a new filter version

Changed
~~~~~~~

- Remove deprecated code of cassandra and mars

v2.4.0 (02/13/2023)
-------------------

Added
~~~~~

- Bigtable repository for catalog.

Fixed
~~~~~

- Fix bug CassandraCatalogObjectRepository add method because it was using the same catalog_id and id.
- Fix filters list endpoint 500 error when including owners.

v2.3.1 (01/20/2023)
-------------------

Added
~~~~~

- Trace locus id with each run_filter_executable.
- Remove misuse of asserts.
- New FilterTableList component which includes a table with the list of filters.
- New AdminFilterList view and associated route for listing all the available filters.
- New requireAdmin navigation guard for preventing non admin users to access the admin filters route.
- Tests for FilterTableList, AdminFilterList and FilterList.
- Add requireAdmin navigation guard to the rest of admin routes (admin/announcements, admin/announcements/create and admin/user-management)
- Move Admin view components under the @/views/Admin folder to group admin views there
- Update Jest config to avoid some dependencies errors occurring in test environment (styles and d3 package)
- Refactor test helpers to emulate the application Vue instance (config data was not being used)
- Remove unused Python code.
- Remove the antares/entrypoints/pipeline/stages/send_filter_crash_notification.py file as it's a copy and paste that's not being used.
- Refactor the Logout class to avoid always sending the same status code while suppressing exceptions.
- Fix "E731 do not assign a lambda expression, use a def" in devkit init.
- Replace pytest.mark.skip by pytest.mark.xfail in unimplemented tests
- Add locus_satisfies_filter_constraints validation in the run_many and _run_filter methods with their respective unit tests.
- Add a missing dk.init() line in the documentation.
- Tests for the new owner property of filters and http api responses when including (or not) the include=owner query param.
- Add owner email and name columns to admin filters table.
- Add a utils file that has methods to serialize some data types into bytes for bigtable.
- Bigtable repository for watch_object.
- Complete the missing methods in the locus repository for bigtable.


v2.3.0 (12/28/2022)
-------------------

Added
~~~~~

- Bigtable repositories for locus, alert and watchlist.
- Static team members page
- Frontend tests along with a CI job to run them.
- Update frontend dependencies.
- Update typing hints to Python 3.9

v2.2.0 (09/08/2022)
-------------------

Added
~~~~~

- Support turning off watchlists and filters per survey and topic

v2.1.11 (08/04/2022)
--------------------

Added
~~~~~

- Capture write timeouts and report via opentracing for loci and retry; if still failing continue to try to add alerts

v2.1.10 (07/28/2022)
--------------------

Changed
~~~~~~~

- Turn off LWT to prevent CAS write timeouts preventing dropped alerts

v2.1.9 (07/27/2022)
-------------------

Added
~~~~~

- When inserting Alerts do not use lightweight transactions (LWT).
- This caused them to drop due to a Cassandra bug.
- The system has to get overloaded for it to occur but there seems no good reason to use LWT for inserting uniquely ID-ed records.

v2.1.8 (07/27/2022)
-------------------

Changed
~~~~~~~

- Retry 30 seconds

v2.1.7 (07/27/2022)
-------------------

Changed
~~~~~~~

- Try CAS transaction for 5 minutes before giving up

v2.1.6 (07/26/2022)
-------------------

Changed
~~~~~~~

- Work around LWT (CAS write timeout) errors.  Retry 60 times sleeping 1 sec in between.

v2.1.5 (07/21/2022)
-------------------

Changed
~~~~~~~

- Update version.txt

v2.1.4 (07/21/2022)
-------------------

Added
~~~~~

- Support coordinate string format like 18 52 17.5478270448 +00 59 44.304835128
- Better message when no objects are found


v2.1.3 (07/20/2022)
-------------------

Added
~~~~~

- Enable cone search arcmin scale option


v2.1.2 (07/15/2022)
-------------------

Changed
~~~~~~~

- Put Back Properties Documentation listing


v2.1.1 (07/15/2022)
-------------------

Changed
~~~~~~~

- The Properties navigation menu item relies on LocusPropertyTable.vue so we put that back and created a LocusProperties.vue view


v2.1.0 (07/15/2022)
-------------------

Added
~~~~~

- Locus Properties are now viewable, filterable and exportable


v2.0.0 (04/15/2022)
-------------------

Changed
~~~~~~~

- Structure of code to follow a more Domain-Driven-Design model
- Refactored to break up processing into stages
- Many other structural and architectural changes


Added
~~~~~~
- Dependency Injection Framework



v2.0.0 (04/22/2022)
-------------------

Added
~~~~~

- Ability to ingest multiple surveys

Changed
~~~~~~~

- Configuration strategy (see `default.yaml`)

- Pipeline execution order and consistency strategy

- Abstract dependencies on databases & infrastructure

v1.12.0 (07/20/2021)
--------------------

Added
~~~~~

- Configuration setting for adding an additional C* datacenter

- Configuraiton setting for default C* consistency level

- (Pipeline) Added filter dependency ssi-forest v0.1.1

Changed
~~~~~~~

- Reads against C* catalog tables use LOCAL_ONE consistency

- (API) Updated flask to 2.X, flask-jwt-extended to 4.X

- (Pipeline) Updated dependent version of light-curve-python to 0.2.6


Fixed
~~~~~

- (Frontend) Alert table on locus page sorts by descending MJD now

v1.11.0 (07/09/2021)
--------------------

Added
~~~~~

- (API) Add /alerts/<alert_id> endpoint

- (Frontend) Add table of alert properties to locus detail page

Changed
~~~~~~~

- (API) Split locus search statistics out from /loci endpoint into /loci/statistics
  endpoint.

- (Frontend) Refactor to handle loading loci searches and statistcs concurrently, only
  reload statistics when necessary instead of each search action (e.g. pagination).

- (Pipeline) Switch from using mean to median for locus centroid computation

- (Pipeline) More descriptive warning metrics if catalog cross-matching is not able to
  find an object described in the lookup table.

v1.10.0 (06/23/2021)
--------------------

Added
~~~~~
- Support for Cassandra multi-DC in the pipeline client

Changed
~~~~~~~
- Update requirements.txt require Flask version 1.1.4


v1.9.2 (06/10/2021)
-------------------

Added
~~~~~

- Install PyMC3 for filter use

v1.9.1 (06/03/2021)
-------------------

Changed
~~~~~~~

- Remove downtime banner

v1.9.0 (06/01/2021)
-------------------

Added
~~~~~

- Kafka settings are configurable

v1.8.3 (05/29/2021)
--------------------

Changed
~~~~~~~

- Add a temporary banner about downtime


v1.8.2 (05/29/2021)
--------------------

Fixed
~~~~~

- Add missing certificate

v1.8.1 (05/29/2021)
--------------------

Added
~~~~~

- Add ANTARES Kafka SSL certificate to image build

v1.8.0 (05/28/2021)
--------------------

Added
~~~~~

- (API) /client/config/streaming/default endpoint for fetching client configuration
  settings dynamically.

Fixed
~~~~~

- (Frontend) Zero-pad numbers in HH:MM:SS and DD:MM:SS coordinate to two places
- (Frontend) Sponsoring agency logos link to their home pages

v1.7.5 (05/06/2021)
-------------------

Changed
~~~~~~~

- Change all references to noao.edu to noirlab.edu
- (Pipeline) Update filter writing documentation to remove references to output alert
  properties
- (Pipeline) Change variable name "trace" to "traceback" to avoid confusion with APM

v1.7.4 (04/12/2021)
-------------------

Added
~~~~~

- (API) Configure API email sender with config.API_MAIL_SENDER

v1.7.3 (04/08/2021)
-------------------

Added
~~~~~

- (Frontend) Links to view a locus on ALeRCE, IPAC, SNAD
- (Frontend) /loci/lookup/:queryString route to search for object IDs
- (Frontend) Clarify description of L1 filters

v1.7.2 (04/05/2021)
-------------------

Added
~~~~~

- (Pipeline) Hint at datadog to keep trace for any alert that disables a filter

v1.7.1 (04/05/2021)
-------------------

Added
~~~~~

- (Pipeline) Tags to datadog traces identifying alert IDs and, if applicable, locus IDs

v1.7.0 (03/29/2021)
-------------------

Added
~~~~~

- (AN-769) Add details about watch objects to Slack notifications

Fixed
~~~~~

- (AN-819) Clicking on active search filter text removes the filter
- (AN-775) Updated paper link
- (AN-822) Dependency issues with image builds

v1.6.2 (02/03/2021)
-------------------

Fixed
~~~~~

- Remove `pip debug` call

v1.6.1 (02/03/2021)
-------------------

Added
~~~~~

- Build script to deploy to datalab also deploys to gp12

v1.6.0 (02/03/2021)
-------------------

Added
~~~~~

- API: Expose `/catalogs` and `/loci/<locus_id>/catalog-matches` endpoints
- FRONTEND: Notes about citing ANTARES
- FRONTEND: Catalog cross-matches to locus pages

Changed
~~~~~~~

- Remove unnecessary configuration of DataDog API key

Fixed
~~~~~

- FRONTEND: Filter out negative magnitudes from plots

v1.5.0 (01/19/2021)
-------------------

Changed
~~~~~~~

- Deploys against `v2.2.0` of the ANTARES Helm chart.

- FRONTEND: Limit number of significant digits displayed in the alert table on the
  frontend's locus detail page.

Added
~~~~~

- FRONTEND: Runtime configuration settings for the frontend exposed in
  `frontend/public/config.json`.

Fixed
~~~~~

- FRONTEND: Issue where long tag names broke layout on locus detail page.
