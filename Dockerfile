FROM python:3.9-slim-bullseye

RUN apt-get update && apt-get install -y \
  build-essential \
  default-libmysqlclient-dev \
  libgl1-mesa-glx \
  libgl1 \
  libglib2.0-0 \
  git \
  libvips42 \
  && rm -rf /var/lib/apt/lists/*

# libgl1-mesa-glx, libgl1, and libglib2.0-0 are dependencies for astro_ghost 2.0.12 required by Filter LAISS_RFC_AD
# see https://noirlab.atlassian.net/browse/AN-1456

# libvips42 is required for the stamp extractor

# no confluent-kafka wheel for linux/arm64
RUN if [ $(arch) = "aarch64" ] ; then apt-get update && apt-get install -y librdkafka-dev; fi && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY pyproject.toml poetry.lock ./

RUN pip install --upgrade pip && \
    pip install poetry && \
    poetry config virtualenvs.create false

# tensorflow and dustmaps currently a pain to build on aarch64
RUN if [ $(arch) = "aarch64" ] ; \
    then poetry install --extras "mysql stamp" ; \
    else poetry install --extras "filters mysql stamp" ; fi && \
    rm -rf /root/.cache/* ;

ADD certificates/kafka-ca.pem /etc/ssl/certs/kafka-ca.pem

EXPOSE 8000

COPY . ./
