.. _catalogs:

Catalog Association
===================

Catalog Association is the process by which ANTARES associates Locii with objects in catalogs. This page describes
the association algorithm and its capabilities.

All objects are represented as circular regions of sky with a center and a radius. Catalogs are either
fixed-radius or variable-radius. In a fixed-radius catalog, all objects
have the same radius. In a variable-radius catalog, each object may have a custom radius.

Catalogs are indexed by HTM ID. All objects within a given catalog are indexed at the same HTM level,
but HTM level may vary between catalogs.

Algorithm
---------

The Catalog Association algorithm is here written in Python-like pseudocode::


   # Let:
   # `catalogs` be a list of objects which each describe a catalog table
   # `get_htm_region(ra, dec, radius, level)` return a list of HTM IDs
   # `query(catalog, htm_region)` return a list of catalog objects

   # Then, the Catalog Association algorithm is as follows:

   def catalog_association(ra, dec):
       confirmed_matches = []

       # Iterate through the catalogs and search each one
       for catalog in catalogs:

           # Compute search region and search the catalog table for candidates.
           # `htm_region` is a list of HTM IDs describing the search region.
           htm_region = get_htm_region(
               ra, dec, catalog.search_radius, catalog.htm_level
           )
           candidate_matches = query(catalog, htm_region)

           # Now we must check the exact angular distances to the objects
           for candidate in candidate_matches:
               p1 = (ra, dec)
               p2 = (candidate.ra, candidate.dec)

               # Determine radius of the object
               if catalog.is_variable_radius:
                   r_match = candidate.radius  # Object has a custom radius
               else:
                   r_match = catalog.search_radius  # Objects have the same radius

               # Check radius of object to confirm the match
               r_actual = angular_distance(p1, p2)
               if r_actual <= r_match:
                   confirmed_matches.append(candidate)

       return confirmed_matches


Caveats
-------

- Each catalog is searched exactly once per Locus. When a new catalog is added to the system, all Alerts will initially trigger searches on it.

Catalogs with broad distribution of object radii
------------------------------------------------

- The query on a given catalog must be performed with the radius of the largest object in that catalog.
- The length of the list of HTM points which is checked depends on the search radius of the catalog, and the HTM level of the catalog.
- The longer the list of HTM points which is searched, the slower the search.
- HTM levels should thus be chosen appropriately.
- The wider the search area relative to the sizes of the objects, the more false positives will be queried and then discarded.
- If a catalog contains some large objects (eg. 1 degree) and many small objects (eg. 1 arcsecond) then many false positives will be returned and discarded with each search.

Therefore:

- Catalogs with a wide distribution of object sizes should be broken up into multiple catalogs, each with a narrower distribution of object radii.
