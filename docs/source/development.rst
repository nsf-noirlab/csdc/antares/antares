.. _development:

Development Guidelines
======================

Recommended reading before contributing to development efforts on the ANTARES code base:

* Architecture Patterns with Python: Enabling Test-Driven Development, Domain-Driven
  Design, and Event-Driven Microservices

Recommended reference books:

* Mastering Kubernetes: Level up your container orchestration skills with Kubernetes to
  build, run, secure, and observe large-scale distributed apps

* Fluent Python: Clear, Concise, and Effective Programming
