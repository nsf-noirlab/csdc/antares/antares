.. _grav_waves:


Gravitational Wave Association
==============================

.. _antares: http://antares.noirlab.edu
.. _gcn: https://gcn.nasa.gov/

ANTARES_ ingests gravitational wave notices from GCN_ as the notices are
published. The position of new survey alerts are checked against the recent
gravitational wave notices' confidence regions. If the alert is inside
the 99.5% confidence region, then data is appended to the alert
and a tag is applied to the alert's locus.
The gravitational wave notice's metadata is provided to user filters
as a dictionary attached to the locus. The skymap is not made available
to user filters.

Gravitational Wave Notice Ingestion
-----------------------------------

ANTARES ingests gravitational wave notices from the GCN_ Kafka cluster.
The notices are stored and then made available via
the ``GravWaveNotice`` object:

.. autoclass:: antares.domain.models.GravWaveNotice
   :members: gracedb_id, notice_type, notice_datetime, event_datetime, false_alarm_rate, skymap_base64, external_coinc, full_notice
   :undoc-members:
   :member-order: bysource
   :exclude-members: __init__, __new__

The ``full_notice`` attribute is a dictionary of the full notice as-retrieved
from GCN, except the skymap has been removed. User filters should be defensive
when accessing ``external_coinc`` or ``full_notice`` since the keys are not
validated when ingested.
``GravWaveNotice.notice_type`` specifies the notice type using the enumeration:

.. autoclass:: antares.domain.models.GravWaveNoticeTypes
   :members:                                        
   :undoc-members:  
		     
Survey Alert Processing
-----------------------
		     
When the ANTARES pipeline ingests a survey alert,
it inspects the lastest gravitational wave notice for each event within the
previous **10 days** that has not been retracted.
		 
Using an algorithm from the `LIGO/Virgo/KAGRA Public Alerts User Guide <https://emfollow.docs.ligo.org/userguide/tutorial/multiorder_skymaps.html>`_,
ANTARES calculates the probability region contour for the location of each
survey alert. If the suvery alert location is within the 99.5% confidence
region, the contour value and area of that confidence region is 
attached to the survey alert. For example: if an alert is located on the edge
of the 25% confidence region (the region containing the top 25% of the
probability distribution), and the 25% confidence region is 60 square degrees,
then the gravitation wave event id, contour_level of 25, and contour_area of
60 are attached to the alert. The alert's locus is also tagged with the
gravitational wave's id.

These data are stored in the ``Alert.grav_wave_events`` attribute as a list
of dictionaries with the following structure: 

.. autoclass:: antares.domain.models.alert.AlertGravWaveEvent
   :show-inheritance:
   :members:
   :undoc-members:  

Loci with alerts with gravitation wave events have a list of GraceDB ID string
on the ``Locus.grav_wave_events`` attribute.

User Filter Access to Gravitational Wave Events
------------------------------------------------

User filters can restrict processing to alerts in the top X% of a
gravitation wave event's probability region using the
``REQUIRED_GRAV_WAVE_PROB_REGION`` class variable. For example, if that
variable was set to 10.5 in the user filter, only alerts in the top 10.5%
confidence region of any current gravitational wave event would be processed
by that user filter.

Besides the ``Alert.grav_wave_events``, user filters have access to
``Locus.grav_wave_events_metadata`` which is a dictionary of ``GravWaveNotice``
objects, as described above, without the probability skymaps. The keys
of the dictionary are GraceDB IDs.
User filters should be defensive
when accessing ``external_coinc`` or ``full_notice`` since the keys are not
validated when ingested.
