.. _devkit.getting_started:

Getting Started with Filter Development
---------------------------------------

.. _datalab: https://datalab.noirlab.edu
.. _jupyter: https://datalab.noirlab.edu/devbooks

The devkit is designed to be used in the NSF NOIRLab's DataLab Jupyter environment,
where it has access to the ANTARES databases. To get started:

1. Sign up for DataLab_.
2. Log in to the Jupyter_ server.
3. Create a new notebook using the **"Python 3 (ANTARES)"** kernel.
4. Paste in the following code:

::

    import antares.devkit as dk
    dk.init()

Run that code block, and you should see a success message.
Now you're ready to create and test ANTARES filters.

Let's make a simple ``HelloWorld`` filter which tags all loci with ``'hello_world'``::

    class HelloWorld(dk.Filter):
        OUTPUT_TAGS = [
            {
                'name': 'hello_world',
                'description': 'hello!',
            },
        ]

        def run(self, locus):
            print('Hello Locus ', locus.locus_id)
            locus.tag('hello_world')

Let's run the filter on a randomly locus from the ANTARES database::

    # Fetch 1 random Locus ID from the test dataset
    locus_id = dk.get_locus_ids(1)[0]

    # Execute HelloWorld filter on the locus
    report = dk.run_filter(HelloWorld, locus=locus_id)

    # `run_filter()` returns a report of what the filter did. Take a look at it:
    print(report)
