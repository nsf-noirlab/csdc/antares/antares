
.. _devkit.example_filter:

Example of a Real Filter
========================

Take a look at this example filter that the ANTARES team has written to find objects
with alerts with a high signal-noise ratio::

    class HighSNR(dk.Filter):
        """
        This filter detects alerts with a high SNR (over 50% in the g-band and over 55% in
        the R-band).
        """
        NAME = "High SNR"
        ERROR_SLACK_CHANNEL = ""  # Put your Slack user ID here
        REQUIRED_LOCUS_PROPERTIES = ["ztf_object_id"]
        REQUIRED_ALERT_PROPERTIES = ["ant_passband", "ztf_sigmapsf"]
        OUTPUT_LOCUS_PROPERTIES = []
        OUTPUT_TAGS = [
            {
                "name": "high_snr",
                "description": "Locus has one or more Alerts with high SNR.",
            },
        ]

        def run(self, locus):
            """
            If the most recent alert on this locus has a high SNR, add the "high_snr" tag.
            """
            # The threshold is dependent on the band that is being imaged.
            # These thresholds should flag ~2-3% of alerts.
            snr_threshold = {
                "g": 50.0,
                "R": 55.0,
            }

            # Determine the passband of the most recent alert at this locus.
            alert_passband = locus.alert.properties["ant_passband"]
            if alert_passband not in snr_threshold:
                print(f"alert_passband {alert_passband} is not supported by this filter.")
                return  # Do nothing.

            # Calculate the SNR of the latest alert
            alert_snr = 1.0 / locus.alert.properties["ztf_sigmapsf"]
            alert_id = locus.alert.alert_id  # Get the ANTARES alert_id
            ztf_object_id = locus.properties["ztf_object_id"]  # Get the ZTF Object ID

            # Tag this locus if the latest alert meets our SNR criteria
            if alert_snr > snr_threshold[alert_passband]:
                print("High SNR detected")
                locus.tag("high_snr")

.. _pipeline: http://antares.noirlab.edu/pipeline

For examples of real filters, check out the filters which are currently running
in the ANTARES Pipeline_.
