
.. _devkit.locus:

The Locus Object
================

.. _client: http://nsf-noirlab.gitlab.io/csdc/antares/client
.. _properties: http://antares.noirlab.edu/properties

ANTARES filters run in response to alerts from surveys but often, to determine if you're
interested in that alert, you might want more context. ANTARES provides contextual information
about alerts through the concept of "loci". An ANTARES locus is our best guess of the object
that each particular alert was from and also includes:

* The full history of alerts received on that same object
* Nearby objects from cross-matching against a dozen catalogs
* Pre-computed properties_ of the object and features of its lightcurve
* Annotations added by your filter and by other filters running in ANTARES

You might find an object of interest to you and explore it in the devkit while you read
this documentation to understand what is available to you in your filters::

    import antares.devkit as dk

    LOCUS_ID = "ANT2020blbrs"
    locus = dk.get_locus(LOCUS_ID)

The ``Locus`` object exposes the following fields and methods:


**(NOTE: Locus is aliased to FilterContext and contains the same properties and methods.)**

.. autoclass:: antares.domain.models.FilterContext
   :members:


