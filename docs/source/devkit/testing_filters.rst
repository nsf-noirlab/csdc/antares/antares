.. _devkit.testing_filters:

Testing Filters
===============

Filter Performance
------------------

Let's revisit the ``HelloWorld`` filter and walk through how we might test that it works
and is performant.::

    class HelloWorld(dk.Filter):
        OUTPUT_TAGS = [
            {
                'name': 'hello_world',
                'description': 'hello!',
            },
        ]

        def run(self, locus):
            print('Hello Locus ', locus.locus_id)
            locus.tag('hello_world')

The devkit provides the ``antares.devkit.run_filter`` helper function to run and profile
your filter while it's in development:

.. autofunction:: antares.devkit.run_filter

For example::

    report = dk.run_filter(HelloWorld)  # Run against a random locus
    report = dk.run_filter(HelloWorld, locus="ANT2020blbrs")  # Run against ANT2020blbrs

    # `run_filter()` returns a report of what the filter did. Take a look at it:
    print(report)

To get better performance statistics, you can run the filter on multiple loci with the
``antares.devkit.run_many`` helper:

.. autofunction:: antares.devkit.run_many

For example::

    report = dk.run_many(HelloWorld, n=100)  # Run against 100 loci
    print(report)


Building Test Cases
-------------------

If your filter is looking for relatively rare objects, it may be difficult to find an
object in the ANTARES database that you can use to test its behavior on different inputs.
We provide two methods of building realistic loci to use as part of your development and
testing workflow.

Editing Real Loci
~~~~~~~~~~~~~~~~~

The ``Locus`` instance that you get in the devkit has the same interface as the one your
filter will receive when it is running in ANTARES. Because of this, there are some
restrictions on what you can and cannot do with it (we don't want folks overwriting each
other's data!). You can use the following pattern to use a real locus as your starting
point and customize it to trigger certain behavior in your filter. Let's say that your
filter is looking for rapid changes in lightcurve magnitude::

    import antares.devkit as dk
    from antares.pipeline.locus import Locus
    from antares.pipeline.ingestion import is_ztf_candidate

    # Fetch a real locus as a starting point
    locus = dk.get_locus("ANT2020blbrs")

    # Convert the locus to a mutable dictionary
    locus_dict = locus.to_dict()

    # Edit the properties that you want to customize. We are interested in setting the
    # magnitude of the current candidate (i.e. the one that will trigger the filter) to
    # the magnitude of the previous candidate minus 5.
    candidates = [
        alert for alert in locus_dict["alerts"] if is_ztf_candidate(alert["alert_id"])
    ]
    previous_alert_mag = candidates[-2]["properties"]["ant_mag"]  # Previous Candidate
    locus_dict["alerts"][-1]["properties"]["ant_mag"] = previous_alert_mag - 5

    # Rebuild a Locus instance
    locus = Locus.from_dict(locus_dict)

    # Run your filter
    dk.run_filter(HelloWorld, locus=locus)

Constructing Synthetic Loci
~~~~~~~~~~~~~~~~~~~~~~~~~~~

You might need more flexibility in which case you can build a ``Locus`` from scratch::

    import antares.devkit as dk

    dk.init()

    ra, dec = 88.2744186, -5.0010774
    locus_dict = {
        'locus_id': 'locus1',
        'ra': ra,
        'dec': dec,
        'properties': {
            'num_alerts': 2,
            'num_mag_values': 2,
        },
        'tags': [],
        'watch_list_ids': [],
        'watch_object_ids': [],
        'catalog_objects': dk.search_catalogs(ra, dec),
        'alerts': [
            {
                'alert_id': 'alert1',
                'locus_id': 'locus1',
                'mjd': 58794.272488399874,
                'properties': {
                    'ant_mjd': 58794.272488399874,
                    'ant_mag': 15.1,
                    'ant_magerr': 0.1,
                    'ant_maglim': 0.1,
                    'ant_survey': 1,
                    'ant_passband': 'g',
                    'ant_ra': 88.2744186,
                    'ant_dec': -5.0010774,
                },
            },
            {
                'alert_id': 'alert2',
                'locus_id': 'locus1',
                'mjd': 58799.50587960007,
                'properties': {
                    'ant_mjd': 58799.50587960007,
                    'ant_mag': 15.2,
                    'ant_magerr': 0.1,
                    'ant_maglim': 0.1,
                    'ant_survey': 1,
                    'ant_passband': 'g',
                    'ant_ra': 88.2744186,
                    'ant_dec': -5.0010774,
                }
            },
        ],
    }

    locus = dk.locus_from_dict(locus_dict)
    dk.run_filter(HelloWorld, locus=locus)
