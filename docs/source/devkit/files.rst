.. _devkit.files:

Uploading and using Data Files
==============================

Some filters might need access to data files like statistical models.

ANTARES supports this by use-case by allowing filter authors to upload binary files that
can be retrieved and loaded when their filter is set up in the real-time pipeline.

Uploading files into ANTARES
----------------------------

Files are stored under a unique ``key``, which you create. The ``key`` must be formatted
as specified below.

.. note::
   You will only be able to upload a data file with a particular ``key`` once. If you need
   to update it, you'll have to change the file name, so we recommend adding a version
   specifier to ``key``.

Use the function ``dk.upload_file`` to upload your file into the Devkit DB.

.. autofunction:: antares.devkit.upload_file

Example
-------

First, place your file (eg. ``myFile1.txt``) in the directory in DataLab
in which you are writing your filter.

Then, in a DataLab Jupyter notebook::

  import antares.devkit as dk

  class MyFilter(dk.Filter):
      ...
      REQUIRES_FILES = [
          '<name>_myFile_v1.txt'
      ]

      def setup(self):
          """
          ANTARES will call this function once at the beginning of each night when filters
          are loaded.
          """
          # ANTARES will load all files in `REQUIRED_FILES` into a dict on the filter object
          # `self.files`. Values in this dictionary will be byte strings of class `bytes`.
          # You can then access them like:
          file_data: bytes = self.files['<name>_myFile_v1.txt']

          # Construct a Python object from the raw `file_data`. For example, you might
          # use `my_object = pickle.loads(file_data)`. You should store this object on
          # the filter instance for use in `run()`:
          self.my_object = my_object

      def run(self, locus):
          """
          ANTARES will call this function in real-time for each incoming Alert.
          """
          # Here you can use `self.my_object` in your processing of the alert.
          do_something_with(self.my_object)

  dk.upload_file(key='<name>_myFile_v1.txt', file_path='./myFile.txt')
  report = dk.run_filter(MyFilter)
  print(report)

You may now test your filter using ``dk.run_filter``.
