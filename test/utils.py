from datetime import datetime

from google.cloud.bigtable import column_family
from google.cloud.bigtable.table import Table

from antares.adapters.repository.bigtable.catalog import (
    BigtableCatalogObjectTableDescription,
)
from antares.settings import Container


def trim_to_milliseconds(dt: datetime) -> datetime:
    """Hbase timestamp is accurate to milliseconds."""
    return dt.replace(microsecond=round(dt.microsecond, -3))


def create_catalog_tables(
    table_with_descriptions: list[tuple[Table, BigtableCatalogObjectTableDescription]]
):
    for (
        catalog_table,
        table_description,
    ) in table_with_descriptions:
        max_versions_rule = column_family.MaxVersionsGCRule(1)
        column_families = {table_description["column_family"]: max_versions_rule}
        if not catalog_table.exists():
            catalog_table.create(column_families=column_families)


def delete_catalog_tables(
    table_with_descriptions: list[tuple[Table, BigtableCatalogObjectTableDescription]]
):
    for catalog_table, _ in table_with_descriptions:
        catalog_table.delete()


def clean_all_bigtable_tables(container: Container):
    container.alert_repository().alert_table.truncate()
    container.alert_repository().locus_by_alert_id_table.truncate()
    container.catalog_object_repository().catalog_lut.truncate()
    container.locus_repository().locus_table.truncate()
    container.locus_repository().alert_lut.truncate()
    container.watch_list_repository().watch_list_table.truncate()
    container.watch_object_repository().watch_object_table.truncate()
