import json

import pytest
from gcn_kafka import Consumer as GCNConsumer

from antares.external.kafka import KafkaConsumer

CLIENT_ID = None
CLIENT_SECRET = None

@pytest.mark.skipif(not CLIENT_ID, reason="Set id/secret, local only")
class TestGCNKafkaConsumer:

    def test_connects_to_gcn(self):
        config = {"bootstrap.servers": "kafka.gcn.nasa.gov",
                  "client_id": CLIENT_ID,
                  "client_secret": CLIENT_SECRET,
                  "domain": "gcn.nasa.gov",
                  'default.topic.config': {'auto.offset.reset': 'earliest'}}
        consumer = KafkaConsumer(config, random_group=True, topics = ["igwn.gwalert"])
        assert isinstance(consumer._consumer, GCNConsumer)
        result = consumer.poll()
        assert result[0] == "igwn.gwalert"
        assert result[1] is not None

