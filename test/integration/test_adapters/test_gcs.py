from antares.adapters.repository import GcsAlertThumbnailRepository
from antares.domain.models import AlertThumbnail, AlertThumbnailType

BUCKET = "storage.googleapis.com/antares-production-ztf-stamps"


def test_gcs_alert_thumbnail_repository_builds_proper_uris():
    alert_thumbnail_repository = GcsAlertThumbnailRepository(f"https://{BUCKET}/")
    thumbnails = alert_thumbnail_repository.list_by_alert_id(
        "ztf_candidate:1108123063615015013"
    )
    assert len(thumbnails) == 3
    print(thumbnails)
    assert (
        AlertThumbnail(
            id="candid1108123063615015013_pid1108123063615_targ_diff.fits.png",
            src=f"https://{BUCKET}/candid1108123063615015013_pid1108123063615_targ_diff.fits.png",
            filemime="image/png",
            filename="candid1108123063615015013_pid1108123063615_targ_diff.fits.png",
            thumbnail_type=AlertThumbnailType.DIFFERENCE,
        )
        in thumbnails
    )
    assert (
        AlertThumbnail(
            id="candid1108123063615015013_pid1108123063615_targ_sci.fits.png",
            src=f"https://{BUCKET}/candid1108123063615015013_pid1108123063615_targ_sci.fits.png",
            filemime="image/png",
            filename="candid1108123063615015013_pid1108123063615_targ_sci.fits.png",
            thumbnail_type=AlertThumbnailType.SCIENCE,
        )
        in thumbnails
    )
    assert (
        AlertThumbnail(
            id="candid1108123063615015013_ref.fits.png",
            src=f"https://{BUCKET}/candid1108123063615015013_ref.fits.png",
            filemime="image/png",
            filename="candid1108123063615015013_ref.fits.png",
            thumbnail_type=AlertThumbnailType.TEMPLATE,
        )
        in thumbnails
    )


def test_gcs_alert_thumbnail_repository_responds_empty_if_bad_alert_id():
    alert_thumbnail_repository = GcsAlertThumbnailRepository(f"https://{BUCKET}/")
    thumbnails = alert_thumbnail_repository.list_by_alert_id("bad-id")
    assert thumbnails == []
