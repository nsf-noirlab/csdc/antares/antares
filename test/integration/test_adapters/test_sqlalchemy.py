import logging
import uuid
from datetime import datetime, timedelta
from test.fakes import (
    build_filter,
    build_filter_revision,
    build_notice,
    build_retraction_notice,
    build_user,
)
from time import sleep
from unittest.mock import patch

import pytest
import sqlalchemy_utils
from sqlalchemy.orm import sessionmaker

from antares.adapters.repository import (
    SqlAlchemyFilterRepository,
    SqlAlchemyFilterRevisionRepository,
    SqlAlchemyGravWaveRepository,
    SqlAlchemyLocusAnnotationRepository,
    SqlAlchemyUserRepository,
)
from antares.adapters.repository.sqlalchemy import metadata
from antares.domain.models import (
    FilterExecutable,
    FilterRevisionStatus,
    GravWaveNoticeTypes,
    LocusAnnotation,
)
from antares.exceptions import KeyViolationException, StaleDataException
from antares.external.sql.bootstrap import drop_main_db
from antares.external.sql.engine import get_engine


@pytest.fixture
def session_factory(message_bus):
    drop_main_db()
    engine = get_engine()
    sqlalchemy_utils.create_database(engine.url)
    metadata.create_all(bind=engine)
    yield sessionmaker(engine)
    drop_main_db()


def test_locus_annotation_repository_stores_properly(session_factory):
    repository = SqlAlchemyLocusAnnotationRepository(session_factory)
    expected = LocusAnnotation(
        owner_id=uuid.UUID(int=1),
        locus_id="locus-001",
        comment="Test Comment",
        favorite=True,
    )
    repository.add(expected)
    actual = repository.get(expected.id)
    assert actual == expected


FILTER_BAD_SEEING_CODE = """
import antares.devkit as dk

class BadSeeing(dk.Filter):
    ERROR_SLACK_CHANNEL = None

    def run(self, locus):
        a = locus.alert.properties
        try:
            if a.get("ztf_fwhm") is not None:
                assert a["ztf_fwhm"] <= 5.0
            if a.get("ztf_elong") is not None:
                assert a["ztf_elong"] <= 1.2
        except AssertionError:
            raise locus.halt
"""

FILTER_SYNTAX_ERROR_CODE = """
    import antares.devkit as dk

class SyntaxErrorFilter(dk.Filter):
    def run(self, locus):
        pass
"""

FILTER_MODULE_NOT_FOUND_CODE = """
import antares.devkit as dk
import something_that_doesnt_exist

class ModuleNotFoundFilter(dk.Filter):
    def run(self, locus):
        pass
"""


def test_sqlalchemy_filter_repository_get_returns_none_if_not_found(
    session_factory,
):
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    assert filter_repository.get(123) is None


def test_sqlalchemy_filter_repository_can_add_filter(session_factory):
    created_at = datetime.utcnow()
    created_at = created_at.replace(microsecond=0)
    updated_at = datetime.utcnow()
    updated_at = updated_at.replace(microsecond=0)
    filter_ = build_filter(created_at=created_at, updated_at=updated_at)
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    filter_repository.add(filter_)
    test = filter_repository.get(filter_.id)
    assert test == filter_


def test_sqlalchemy_filter_repository_can_update_a_filter(session_factory):
    created_at = datetime.utcnow()
    created_at = created_at.replace(microsecond=0)
    updated_at = datetime.utcnow()
    updated_at = updated_at.replace(microsecond=0)
    filter_ = build_filter(
        public=True,
        created_at=created_at,
        updated_at=updated_at,
    )
    repo = SqlAlchemyFilterRepository(session_factory)
    repo.add(filter_)
    assert repo.get(filter_.id).public
    filter_.make_private()
    repo.update(filter_)
    assert not repo.get(filter_.id).public


def test_sqlalchemy_filter_repository_not_persisting_changes_without_update_call(
    session_factory,
):
    created_at = datetime.utcnow().replace(microsecond=0)
    updated_at = datetime.utcnow().replace(microsecond=0)
    filter_ = build_filter(
        public=True,
        created_at=created_at,
        updated_at=updated_at,
    )
    repo = SqlAlchemyFilterRepository(session_factory)
    repo.add(filter_)
    assert repo.get(filter_.id).public

    # Do some change
    filter_.make_private()

    # But we do not call repository update so changes are not persisted
    updated_persisted_filter = repo.get(filter_.id)
    assert updated_persisted_filter.public


@patch(
    "antares.adapters.notifications.AbstractInternalNotificationService", autospec=True
)
@patch("antares.observability.tracing.base.AbstractTracer", autospec=True)
@patch("antares.observability.tracing.base.AbstractSpan", autospec=True)
def test_get_filter_executables_returns_executable(
    mock_span, mock_tracer, mock_notifier, session_factory
):
    created_at = datetime.utcnow()
    created_at = created_at.replace(microsecond=0)
    updated_at = datetime.utcnow()
    updated_at = updated_at.replace(microsecond=0)
    filter_ = build_filter(created_at=created_at, updated_at=updated_at)
    filter_revision = build_filter_revision(
        filter_,
        code=FILTER_BAD_SEEING_CODE,
        created_at=created_at,
        updated_at=updated_at,
    )
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    filter_revision.enable(filter_=filter_)
    filter_repository.add(filter_)
    filter_revision_repository.add(filter_revision)
    mock_notifier.__enter__.return_value = mock_notifier
    returned = filter_repository.get_filter_executables(
        filter_revision_repository,
        mock_notifier,
        mock_tracer,
    )
    assert len(returned) == 1
    [returned_filter, filter_executable] = returned[0]
    assert returned_filter == filter_
    assert isinstance(filter_executable, FilterExecutable)
    mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()


@patch(
    "antares.adapters.notifications.AbstractInternalNotificationService", autospec=True
)
@patch("antares.observability.tracing.base.AbstractTracer", autospec=True)
@patch("antares.observability.tracing.base.AbstractSpan", autospec=True)
def test_get_filter_executables_caches(
    mock_span, mock_tracer, mock_notifier, session_factory
):
    created_at = datetime.utcnow()
    created_at = created_at.replace(microsecond=0)
    updated_at = datetime.utcnow()
    updated_at = updated_at.replace(microsecond=0)
    created_at_2 = datetime.utcnow() + timedelta(seconds=3)  # Variable not used
    created_at_2 = created_at.replace(microsecond=0)
    updated_at_2 = datetime.utcnow() + timedelta(seconds=3)  # Variable not used
    updated_at_2 = updated_at.replace(microsecond=0)
    filter_ = build_filter(created_at=created_at, updated_at=updated_at)
    filter_revision = build_filter_revision(
        filter_,
        code=FILTER_BAD_SEEING_CODE,
        created_at=created_at,
        updated_at=updated_at,
    )
    filter_revision_2 = build_filter_revision(
        filter_,
        id=2,
        code=FILTER_BAD_SEEING_CODE,
        created_at=created_at_2,
        updated_at=updated_at_2,
    )
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    filter_revision.enable(filter_=filter_)
    filter_repository.add(filter_)
    filter_revision_repository.add(filter_revision)
    mock_notifier.__enter__.return_value = mock_notifier
    returned = filter_repository.get_filter_executables(
        filter_revision_repository,
        mock_notifier,
        mock_tracer,
    )
    [returned_filter, filter_executable] = returned[0]
    assert returned_filter == filter_
    assert filter_executable.filter_revision_id == filter_revision.id
    mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()
    # We need to disable the enabled revision of a filter in order to enable another one
    filter_revision.disable(filter_=filter_, feedback="Disabling...")
    filter_revision_2.enable(filter_=filter_)
    filter_repository.update(filter_)
    filter_revision_repository.add(filter_revision_2)
    returned = filter_repository.get_filter_executables(
        filter_revision_repository,
        mock_notifier,
        mock_tracer,
    )
    [returned_filter, filter_executable] = returned[0]
    assert filter_executable.filter_revision_id == filter_revision_2.id
    mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()


@patch(
    "antares.adapters.notifications.AbstractInternalNotificationService", autospec=True
)
@patch("antares.observability.tracing.base.AbstractTracer", autospec=True)
@patch("antares.observability.tracing.base.AbstractSpan", autospec=True)
@pytest.mark.parametrize(
    "filter_string_code", [FILTER_SYNTAX_ERROR_CODE, FILTER_MODULE_NOT_FOUND_CODE]
)
def test_get_filter_executables_disables_filter_that_cannot_be_executable(
    mock_span,
    mock_tracer,
    mock_notifier,
    filter_string_code,
    session_factory,
    caplog,
):
    created_at = datetime.utcnow().replace(microsecond=0)
    updated_at = datetime.utcnow().replace(microsecond=0)
    filter_ = build_filter(created_at=created_at, updated_at=updated_at)
    filter_revision = build_filter_revision(
        filter_,
        code=filter_string_code,
        created_at=created_at,
        updated_at=updated_at,
    )
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    filter_revision.enable(filter_=filter_)
    filter_repository.add(filter_)
    filter_revision_repository.add(filter_revision)
    # Without sleep the updated_at assert fails because everything happens at the same time
    sleep(1)
    with caplog.at_level(logging.WARNING):
        mock_notifier.__enter__.return_value = mock_notifier
        filter_repository.get_filter_executables(
            filter_revision_repository,
            mock_notifier,
            mock_tracer,
        )
        updated_persisted_filter = filter_repository.get(filter_.id)
        updated_persisted_filter_revision = filter_revision_repository.get(
            filter_revision.id
        )
        assert updated_persisted_filter_revision.updated_at != updated_at
        assert (
            "Disabled automatically by the pipeline: Filter failed to load as executable"
            in updated_persisted_filter_revision.feedback
        )
        assert not updated_persisted_filter.enabled
        mock_notifier.notify_after_filter_revision_transitioned.assert_called_once()
        assert (
            "Filter Test Filter failed to load as executable:\nTraceback (most recent call last):"
            in caplog.text
        )


@patch(
    "antares.adapters.notifications.AbstractInternalNotificationService", autospec=True
)
@patch("antares.observability.tracing.base.AbstractTracer", autospec=True)
@patch("antares.observability.tracing.base.AbstractSpan", autospec=True)
def test_get_filter_executables_returns_only_enabled_filter(
    mock_span,
    mock_tracer,
    mock_notifier,
    session_factory,
):
    created_at = datetime.utcnow()
    created_at = created_at.replace(microsecond=0)
    updated_at = datetime.utcnow()
    updated_at = updated_at.replace(microsecond=0)
    created_at_2 = created_at.replace(microsecond=0)
    updated_at_2 = updated_at.replace(microsecond=0)
    filter_1 = build_filter(
        id=1, name="Test Filter 1", created_at=created_at, updated_at=updated_at
    )
    filter_revision_1 = build_filter_revision(
        filter_1,
        id=1,
        code=FILTER_BAD_SEEING_CODE,
        created_at=created_at,
        updated_at=updated_at,
    )
    filter_2 = build_filter(
        id=2, name="Test Filter 2", created_at=created_at, updated_at=updated_at
    )
    filter_revision_2 = build_filter_revision(
        filter_2,
        id=2,
        code=FILTER_BAD_SEEING_CODE,
        created_at=created_at_2,
        updated_at=updated_at_2,
    )
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    filter_revision_1.enable(filter_=filter_1)
    filter_repository.add(filter_1)
    filter_revision_repository.add(filter_revision_1)
    filter_repository.add(filter_2)
    filter_revision_repository.add(filter_revision_2)
    mock_notifier.__enter__.return_value = mock_notifier
    filters = filter_repository.get_filter_executables(
        filter_revision_repository, mock_notifier, mock_tracer
    )
    assert len(filters) == 1
    retrieved_filter, retrieved_fiter_executable = filters[0]
    assert retrieved_filter.id == 1
    assert retrieved_filter.name == "Test Filter 1"
    assert retrieved_fiter_executable.filter_id == 1
    mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()


def test_filter_submission_workflow(session_factory):
    created_at = datetime.utcnow().replace(microsecond=0)
    updated_at = datetime.utcnow().replace(microsecond=0)
    filter_ = build_filter(created_at=created_at, updated_at=updated_at)
    filter_revision = build_filter_revision(
        filter_,
        code=FILTER_BAD_SEEING_CODE,
        created_at=created_at,
        updated_at=updated_at,
    )
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    filter_revision.enable(filter_=filter_)
    filter_repository.add(filter_)
    filter_revision_repository.add(filter_revision)
    # Workaround to be able to compare dates between model and MariaDB abstraction
    filter_revision.transitioned_at = filter_revision.transitioned_at.replace(
        microsecond=0,
    )
    assert filter_revision_repository.get(filter_revision.id) == filter_revision
    assert list(filter_revision_repository.list_by_filter_id(filter_.id)) == [
        filter_revision
    ]


@pytest.mark.xfail
def test_sqlalchemy_filter_repository_can_load_storage_files_needed():
    raise NotImplementedError


def test_sqlalchemy_filter_repository_optimistic_concurrency(session_factory):
    created_at = datetime.utcnow()
    created_at = created_at.replace(microsecond=0)
    updated_at = datetime.utcnow()
    updated_at = updated_at.replace(microsecond=0)
    filter_ = build_filter(
        created_at=created_at,
        updated_at=updated_at,
        owner_id=uuid.UUID(int=1),
    )
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    filter_repository.add(filter_)
    # Imagine that two separate processes are trying to update this filter at the same
    # time
    filter_1 = filter_repository.get(filter_.id)
    filter_2 = filter_repository.get(filter_.id)
    # They both fetch before either one can update...
    filter_1.name = "New name"
    filter_2.name = "New different name"
    # Process 1 persists first
    filter_repository.update(filter_1)
    # Process 2 should raise an exception
    with pytest.raises(StaleDataException):
        filter_repository.update(filter_2)


def test_sqlalchemy_filter_revision_repository_get_returns_none_if_not_found(
    session_factory,
):
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    assert filter_revision_repository.get(123) is None


def test_sqlalchemy_filter_revision_repository_can_add_filter_revision(session_factory):
    initial_datetime = datetime.utcnow().replace(microsecond=0)
    filter_ = build_filter()
    filter_revision = build_filter_revision(
        filter_,
        created_at=initial_datetime,
        updated_at=initial_datetime,
        transitioned_at=initial_datetime,
    )
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    filter_revision_repository.add(filter_revision)
    test = filter_revision_repository.get(filter_revision.id)
    assert test == filter_revision


def test_sqlalchemy_filter_revision_repository_returns_filter_revisions_by_level(
    session_factory,
):
    initial_datetime = datetime.utcnow().replace(microsecond=0)
    filter_1 = build_filter(id=1)
    filter_2 = build_filter(id=2)
    filter_revisions = [
        build_filter_revision(
            filter_1,
            id=1,
            created_at=initial_datetime,
            updated_at=initial_datetime,
            transitioned_at=initial_datetime,
        ),
        build_filter_revision(
            filter_2,
            id=2,
            created_at=initial_datetime,
            updated_at=initial_datetime,
            transitioned_at=initial_datetime,
        ),
        build_filter_revision(
            filter_2,
            id=3,
            created_at=initial_datetime,
            updated_at=initial_datetime,
            transitioned_at=initial_datetime,
        ),
    ]
    repo = SqlAlchemyFilterRevisionRepository(session_factory)
    for filter_revision in filter_revisions:
        repo.add(filter_revision)
    assert list(repo.list_by_filter_id(1)) == filter_revisions[0:1]
    assert list(repo.list_by_filter_id(2)) == filter_revisions[1:3]


def test_sqlalchemy_filter_revision_repository_can_update_a_filter_revision(
    session_factory,
):
    filter_ = build_filter()
    comment = "This is a filter version comment"
    filter_revision = build_filter_revision(
        filter_,
        comment=comment,
    )
    repo = SqlAlchemyFilterRevisionRepository(session_factory)
    repo.add(filter_revision)
    persisted_filter_revision = repo.get(filter_revision.id)
    assert persisted_filter_revision.comment == comment
    updated_comment = "This is an updated filter version comment"
    original_transitioned_at = persisted_filter_revision.transitioned_at
    persisted_filter_revision.comment = updated_comment
    repo.update(persisted_filter_revision)
    updated_persisted_filter_revision = repo.get(filter_revision.id)
    assert updated_persisted_filter_revision.comment == updated_comment
    assert updated_persisted_filter_revision.transitioned_at == original_transitioned_at


def test_sqlalchemy_filter_revision_repository_can_transition_revision_without_feedback(
    session_factory,
):
    filter_ = build_filter()
    filter_revision = build_filter_revision(
        filter_,
        status=FilterRevisionStatus.PENDING_REVIEW,
    )
    repo = SqlAlchemyFilterRevisionRepository(session_factory)
    repo.add(filter_revision)
    persisted_filter_revision = repo.get(filter_revision.id)
    assert persisted_filter_revision.status == FilterRevisionStatus.PENDING_REVIEW
    persisted_filter_revision.transition_to(FilterRevisionStatus.REVIEWED)
    repo.update(persisted_filter_revision)
    updated_persisted_filter_revision = repo.get(filter_revision.id)
    assert updated_persisted_filter_revision.status == FilterRevisionStatus.REVIEWED
    assert updated_persisted_filter_revision.feedback == ""


def test_sqlalchemy_filter_revision_repository_can_transition_revision_with_feedback(
    session_factory,
):
    filter_ = build_filter()
    filter_revision = build_filter_revision(
        filter_,
        status=FilterRevisionStatus.PENDING_REVIEW,
    )
    repo = SqlAlchemyFilterRevisionRepository(session_factory)
    repo.add(filter_revision)
    persisted_filter_revision = repo.get(filter_revision.id)
    assert persisted_filter_revision.status == FilterRevisionStatus.PENDING_REVIEW
    feedback = "Good code style. Functionally correct"
    persisted_filter_revision.transition_to(
        FilterRevisionStatus.REVIEWED,
        feedback=feedback,
    )
    repo.update(persisted_filter_revision)
    updated_persisted_filter_revision = repo.get(filter_revision.id)
    assert updated_persisted_filter_revision.status == FilterRevisionStatus.REVIEWED
    assert updated_persisted_filter_revision.feedback == feedback


def test_sqlalchemy_filter_revision_repo_touches_transitioned_at_when_transitioning(
    session_factory,
):
    filter_ = build_filter()
    filter_revision = build_filter_revision(
        filter_,
        status=FilterRevisionStatus.PENDING_REVIEW,
    )
    repo = SqlAlchemyFilterRevisionRepository(session_factory)
    repo.add(filter_revision)
    persisted_filter_revision = repo.get(filter_revision.id)

    # Transitioning with transition_to
    original_transitioned_at = persisted_filter_revision.transitioned_at
    sleep(1)  # Needed because MariaDB datetime microsecond precision defaults to 0
    persisted_filter_revision.transition_to(FilterRevisionStatus.REVIEWED)
    repo.update(persisted_filter_revision)
    updated_persisted_filter_revision = repo.get(filter_revision.id)
    assert updated_persisted_filter_revision.transitioned_at > original_transitioned_at

    # Transitioning with event triggering
    original_transitioned_at = updated_persisted_filter_revision.transitioned_at
    sleep(1)  # Needed because MariaDB datetime microsecond precision defaults to 0
    updated_persisted_filter_revision.mark_as_pending()
    repo.update(updated_persisted_filter_revision)
    assert repo.get(filter_revision.id).transitioned_at > original_transitioned_at


def test_sqlalchemy_filter_revision_repo_not_persisting_changes_without_update_call(
    session_factory,
):
    filter_ = build_filter()
    filter_revision = build_filter_revision(
        filter_,
        status=FilterRevisionStatus.PENDING_REVIEW,
    )
    repo = SqlAlchemyFilterRevisionRepository(session_factory)
    repo.add(filter_revision)
    persisted_filter_revision = repo.get(filter_revision.id)
    feedback = "Looks good to us"

    # Do some change
    persisted_filter_revision.review(feedback=feedback)

    # But we do not call repository update so changes are not persisted
    updated_persisted_filter_revision = repo.get(filter_revision.id)
    assert updated_persisted_filter_revision.feedback != feedback
    assert updated_persisted_filter_revision.status != FilterRevisionStatus.REVIEWED


def test_sqlalchemy_filter_revision_repository_optimistic_concurrency(session_factory):
    created_at = datetime.utcnow()
    created_at = created_at.replace(microsecond=0)
    updated_at = datetime.utcnow()
    updated_at = updated_at.replace(microsecond=0)
    filter_ = build_filter()
    filter_revision = build_filter_revision(
        filter_, created_at=created_at, updated_at=updated_at
    )
    filter_revision_repository = SqlAlchemyFilterRevisionRepository(session_factory)
    filter_revision_repository.add(filter_revision)
    # Imagine that two separate processes are trying to update
    # this filter_revision at the same time
    filter_revision_1 = filter_revision_repository.get(filter_revision.id)
    filter_revision_2 = filter_revision_repository.get(filter_revision.id)
    # They both fetch before either one can update...
    filter_revision_1.comment = "me first!"
    filter_revision_2.comment = "no, me!"
    # Process 1 persists first
    filter_revision_repository.update(filter_revision_1)
    # Process 2 should raise an exception
    with pytest.raises(StaleDataException):
        filter_revision_repository.update(filter_revision_2)


def test_sqlalchemy_user_repository_get_returns_none_if_not_found(
    session_factory,
):
    user_repository = SqlAlchemyUserRepository(session_factory)
    assert user_repository.get(123) is None


def test_sqlalchemy_user_repository_can_add_user(session_factory):
    user = build_user()
    user_repository = SqlAlchemyUserRepository(session_factory)
    user_repository.add(user)
    test = user_repository.get(user.id)
    assert test == user


def test_sqlalchemy_user_repository_can_update_a_user(session_factory):
    user = build_user()
    repo = SqlAlchemyUserRepository(session_factory)
    repo.add(user)
    assert not repo.get(user.id).check_password("supersecret")
    user.set_password("supersecret")
    repo.update(user)
    assert repo.get(user.id).check_password("supersecret")


def test_sqlalchemy_user_repository_optimistic_concurrency(session_factory):
    user = build_user()
    user_repository = SqlAlchemyUserRepository(session_factory)
    user_repository.add(user)
    # Imagine that two separate processes are trying to update this user at the same
    # time
    user_1 = user_repository.get(user.id)
    user_2 = user_repository.get(user.id)
    # They both fetch before either one can update...
    user_1.set_password("me first!")
    user_2.set_password("no, me!")
    # Process 1 persists first
    user_repository.update(user_1)
    # Process 2 should raise an exception
    with pytest.raises(StaleDataException):
        user_repository.update(user_2)


class TestSqlAlchemyGravWaveRepository:
    def test_add(self, session_factory):
        notice = build_notice()
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)

    def test_add_retraction(self, session_factory):
        notice = build_retraction_notice()
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)

    def test_add_with_valid_notice_type_string(self, session_factory):
        notice = build_notice()
        notice.notice_type = "UPDATE"
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)
        returned_notice = grav_wave_repository.get(1)
        assert returned_notice.notice_type == GravWaveNoticeTypes.UPDATE

    def test_add_with_invalid_notice_type(self, session_factory):
        notice = build_notice()
        notice.notice_type = "INVALID_TYPE"
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        with pytest.raises(Exception) as excinfo:
            grav_wave_repository.add(notice)
        excinfo.typename == "DataError"

    def test_add_duplicate_notice(self, session_factory):
        notice = build_notice()
        notice2 = build_notice()
        notice2.notice_datetime = notice.notice_datetime
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)
        with pytest.raises(KeyViolationException) as excinfo:
            grav_wave_repository.add(notice2)
        assert "Duplicate entry" in str(excinfo.value)

    def test_get_real_notice(self, session_factory):
        notice = build_notice()
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)
        returned_notice = grav_wave_repository.get(1)
        # the float in the json could have a floating point error, that's fine
        assert returned_notice.full_notice["event"]["far"] == pytest.approx(
            notice.full_notice["event"]["far"]
        )
        returned_notice.full_notice["event"]["far"] = 0
        notice.full_notice["event"]["far"] = 0
        # everything else should be exact
        assert returned_notice == notice

    def test_get_retraction_notice(self, session_factory):
        notice = build_retraction_notice()
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)
        returned_notice = grav_wave_repository.get(1)
        assert returned_notice == notice

    def test_get_no_notice(self, session_factory):
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        returned_notice = grav_wave_repository.get(1)
        assert returned_notice is None

    def test_get_id_with_gracedb_id_and_notice_datetime(self, session_factory):
        notice = build_notice()
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)
        returned_id = grav_wave_repository.get_id(
            notice.gracedb_id, notice.notice_datetime
        )
        assert returned_id == 1

    def test_get_id_can_return_none(self, session_factory):
        notice = build_notice()
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        returned_notice = grav_wave_repository.get_id(
            notice.gracedb_id, notice.notice_datetime
        )
        assert returned_notice is None


class TestSqlAlchemyGravWaveRepositoryGetLatestActiveNoticeIDs:
    def test_get_one_notice_id(self, session_factory):
        event_datetime = datetime.utcnow() - timedelta(days=2)
        notice = build_notice()
        notice.event_datetime = event_datetime
        notice.id = 1
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)
        returned_ids = grav_wave_repository.get_latest_active_notice_ids()
        assert returned_ids == {1}

    def test_dont_return_old_notice_id(self, session_factory):
        event_datetime = datetime.utcnow() - timedelta(days=15)
        notice = build_notice()
        notice.event_datetime = event_datetime
        notice.id = 1
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(notice)
        returned_ids = grav_wave_repository.get_latest_active_notice_ids()
        assert returned_ids == set()

    def test_get_recent_notice_id(self, session_factory):
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        event_datetime = datetime.utcnow() - timedelta(days=5)

        notice = build_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=3)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 1
        grav_wave_repository.add(notice)

        notice = build_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=1)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 10
        grav_wave_repository.add(notice)

        notice = build_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=4)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 5
        grav_wave_repository.add(notice)

        returned_ids = grav_wave_repository.get_latest_active_notice_ids()
        assert returned_ids == {10}

    def test_get_multiple_recent_notice_ids(self, session_factory):
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        event_datetime = datetime.utcnow() - timedelta(days=5)

        notice = build_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=3)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 1
        grav_wave_repository.add(notice)

        notice = build_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=1)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 10
        grav_wave_repository.add(notice)

        notice = build_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=4)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 5
        grav_wave_repository.add(notice)

        notice = build_notice()
        notice.gracedb_id = "MS199101ab"
        notice_datetime = datetime.utcnow() - timedelta(days=8)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 100
        grav_wave_repository.add(notice)

        notice = build_notice()
        notice.gracedb_id = "MS199101ab"
        notice_datetime = datetime.utcnow() - timedelta(days=7)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 101
        grav_wave_repository.add(notice)

        returned_ids = grav_wave_repository.get_latest_active_notice_ids()
        assert returned_ids == {10, 101}

    def test_doesnt_return_retracted_notice_id(self, session_factory):
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        event_datetime = datetime.utcnow() - timedelta(days=5)

        notice = build_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=3)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 1
        grav_wave_repository.add(notice)

        notice = build_retraction_notice()
        notice_datetime = datetime.utcnow() - timedelta(days=1)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        notice.id = 10
        grav_wave_repository.add(notice)

        returned_ids = grav_wave_repository.get_latest_active_notice_ids()
        assert returned_ids == set()


class TestSqlAlchemyGravWaveRepositoryGetLatestbyGraceDBIDs:
    def test_returns_latest_alert_by_id(self, session_factory):
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        event_datetime = datetime.utcnow() - timedelta(days=5)
        notice = build_notice(superevent_id="MS181101at1")
        notice_datetime = datetime.utcnow() - timedelta(days=3)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        grav_wave_repository.add(notice)
        event_datetime = datetime.utcnow() - timedelta(days=2)
        notice = build_notice(superevent_id="MS181101at1")
        notice_datetime = datetime.utcnow() - timedelta(days=1)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        grav_wave_repository.add(notice)
        grav_wave_repository.add(build_notice(superevent_id="MS181101at3"))
        returned_notices = list(
            grav_wave_repository.get_latest_by_gracedb_ids(["MS181101at1"])
        )
        assert len(returned_notices) == 1
        notice = returned_notices[0]
        assert notice.event_datetime == event_datetime
        assert notice.notice_datetime == notice_datetime
        assert notice.id == 2
        assert notice.gracedb_id == "MS181101at1"

    def test_returns_multiple_latest_alerts_by_id(self, session_factory):
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        event_datetime = datetime.utcnow() - timedelta(days=5)
        notice = build_notice(superevent_id="MS181101at1")
        notice_datetime = datetime.utcnow() - timedelta(days=3)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        grav_wave_repository.add(notice)
        event_datetime = datetime.utcnow() - timedelta(days=2)
        notice = build_notice(superevent_id="MS181101at1")
        notice_datetime = datetime.utcnow() - timedelta(days=1)
        notice.notice_datetime = notice_datetime
        notice.event_datetime = event_datetime
        grav_wave_repository.add(notice)
        grav_wave_repository.add(build_notice(superevent_id="MS181101at3"))
        grav_wave_repository.add(build_notice(superevent_id="MS181101at3"))
        grav_wave_repository.add(build_notice(superevent_id="MS181101at4"))
        returned_notices = list(
            grav_wave_repository.get_latest_by_gracedb_ids(
                ["MS181101at1", "MS181101at3"]
            )
        )
        assert len(returned_notices) == 2
        for notice in returned_notices:
            assert notice.id in (2, 4)

    def test_returns_empty_list_when_receives_an_empty_list(self, session_factory):
        grav_wave_repository = SqlAlchemyGravWaveRepository(session_factory)
        grav_wave_repository.add(build_notice())
        returned_notices = list(grav_wave_repository.get_latest_by_gracedb_ids([]))
        assert returned_notices == []
