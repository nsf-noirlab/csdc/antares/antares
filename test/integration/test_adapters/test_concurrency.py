import time
from contextlib import nullcontext as does_not_raise
from test.fakes import FakeDistributedLock
from typing import Generator, Optional
from unittest.mock import MagicMock

import pytest
from redis import Redis

from antares.adapters.concurrency import (
    AbstractDistributedLockFactory,
    RedisDistributedLock,
)


@pytest.fixture
def redis_distributed_lock_factory() -> (
    Generator[AbstractDistributedLockFactory, None, None]
):
    client = Redis("redis", 6379, db=0)

    def factory(id_: str, ttl: Optional[float] = None) -> RedisDistributedLock:
        return RedisDistributedLock(client, id_, ttl=ttl)

    yield factory
    client.flushall()


@pytest.fixture
def fake_distributed_lock_factory() -> AbstractDistributedLockFactory:
    database: set[tuple[str, Optional[float]]] = set()

    def factory(id_: str, ttl: Optional[float] = None) -> FakeDistributedLock:
        return FakeDistributedLock(database, id_, ttl)

    return factory


@pytest.fixture(
    params=[
        "fake_distributed_lock_factory",
        "redis_distributed_lock_factory",
    ]
)
def distributed_lock_factory(request) -> AbstractDistributedLockFactory:
    """
    PyTest fixture that we can parameterize to test multiple external backends for
    compliance with our lock interface.
    """
    return request.getfixturevalue(request.param)


@pytest.mark.parametrize(
    "id_,expectation",
    [
        (3.14, pytest.raises(ValueError)),
        (b"bytes dont work", pytest.raises(ValueError)),
        (123456, pytest.raises(ValueError)),
        ("i should work", does_not_raise()),
    ],
)
def test_distributed_lock_accepts_only_string_id(
    id_, expectation, distributed_lock_factory
):
    with expectation:
        _ = distributed_lock_factory(id_)


def test_distributed_lock_can_acquire_lock(distributed_lock_factory):
    distributed_lock = distributed_lock_factory("lock-name")
    assert distributed_lock.acquire()
    distributed_lock.release()


def test_different_keyed_distributed_locks_can_acquire_lock(distributed_lock_factory):
    distributed_lock1 = distributed_lock_factory("lock-name")
    distributed_lock2 = distributed_lock_factory("lock-name2")
    assert distributed_lock1.acquire()
    assert distributed_lock2.acquire()
    distributed_lock1.release()
    distributed_lock2.release()


def test_distributed_lock_context_can_acquire_lock(distributed_lock_factory):
    distributed_lock = distributed_lock_factory("lock-name")
    test_function = MagicMock()
    with distributed_lock as lock:
        test_function()
        assert lock.locked()
    test_function.assert_called_once()
    assert not distributed_lock.locked()


def test_distributed_lock_context_blocks_other_processes(distributed_lock_factory):
    distributed_lock = distributed_lock_factory("lock-name")
    distributed_lock2 = distributed_lock_factory("lock-name")
    val = 0
    with distributed_lock:
        val += 1
        assert not distributed_lock2.acquire(blocking=False)
    assert not distributed_lock.locked()
    assert val == 1


def test_distributed_lock_context_releases_after_error(distributed_lock_factory):
    distributed_lock = distributed_lock_factory("lock-name")
    distributed_lock2 = distributed_lock_factory("lock-name")
    val = 0
    try:
        with distributed_lock:
            raise ValueError
            val += 1
    except ValueError:
        pass
    assert not distributed_lock.locked()
    assert val == 0
    assert distributed_lock2.acquire()
    distributed_lock2.release()


def test_distributed_lock_lease_expires_after_ttl(distributed_lock_factory):
    ttl = 1.0
    distributed_lock1 = distributed_lock_factory("lock-name", ttl=ttl)
    distributed_lock2 = distributed_lock_factory("lock-name")
    assert distributed_lock1.acquire()
    assert not distributed_lock2.acquire(blocking=False)
    time.sleep(ttl * 2)
    assert distributed_lock2.acquire(blocking=False)
    distributed_lock2.release()


def test_distributed_lock_prevents_other_process_from_acquiring_lock(
    distributed_lock_factory,
):
    distributed_lock1 = distributed_lock_factory("lock-name")
    distributed_lock2 = distributed_lock_factory("lock-name")
    assert distributed_lock1.acquire(blocking=False)
    assert not distributed_lock2.acquire(blocking=False)
    distributed_lock1.release()
    assert distributed_lock2.acquire(blocking=False)
    distributed_lock2.release()


def test_distributed_lock_timesout_waiting_for_lock(distributed_lock_factory):
    distributed_lock1 = distributed_lock_factory("lock-name")
    distributed_lock2 = distributed_lock_factory("lock-name")
    start_time = time.perf_counter()
    timeout = 5.0
    with distributed_lock1:
        result = distributed_lock2.acquire(timeout=timeout)
        assert not result
        assert (time.perf_counter() - start_time) > timeout


@pytest.mark.xfail
def test_distributed_lock_only_locking_process_can_release(distributed_lock_factory):
    distributed_lock1 = distributed_lock_factory("lock-name")
    distributed_lock2 = distributed_lock_factory("lock-name")
    assert distributed_lock1.acquire(blocking=False)
    # TODO: What happens if another lock attempts to release?
    # A: threading.Lock has an 'owner' attribute that tracks which can release a lock
    # and errors if it's not correct
    distributed_lock2.release()
    distributed_lock1.release()
    raise NotImplementedError


@pytest.mark.xfail
def test_distributed_lock_behavior_release_after_expire(distributed_lock_factory):
    distributed_lock1 = distributed_lock_factory("lock-name", ttl=0.5)
    assert distributed_lock1.acquire()
    time.sleep(1.0)
    # TODO: Define the behavior of release after ttl lapses
    distributed_lock1.release()
    raise NotImplementedError


@pytest.mark.xfail
def test_distributed_lock_behavior_check_status_after_expire(distributed_lock_factory):
    distributed_lock1 = distributed_lock_factory("lock-name", ttl=0.5)
    assert distributed_lock1.acquire()
    time.sleep(1.0)
    # TODO: Define the behavior of locked after ttl lapses
    distributed_lock1.locked()
    raise NotImplementedError
