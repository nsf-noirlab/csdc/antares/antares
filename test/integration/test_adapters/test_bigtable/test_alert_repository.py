from datetime import datetime
from itertools import repeat
from test.fakes import build_alert, build_alerts
from test.utils import trim_to_milliseconds


class TestBigtableAlertRepository:
    def test_add_and_get_alerts(self, int_bt_alert_repo):
        alert1 = build_alert(
            id="alert-001",
            properties={"name": "1"},
            created_at=trim_to_milliseconds(datetime.utcnow()),
        )
        int_bt_alert_repo.add(alert1, "l1")
        alert2 = build_alert(
            id="alert-002",
            properties={"name": "2"},
            created_at=trim_to_milliseconds(datetime.utcnow()),
        )
        int_bt_alert_repo.add(alert2, "l1")
        alert3 = build_alert(
            id="alert-003",
            properties={"name": "3"},
            created_at=trim_to_milliseconds(datetime.utcnow()),
        )
        int_bt_alert_repo.add(alert3, "l2")
        assert alert1 == int_bt_alert_repo.get("alert-001")
        assert alert2 == int_bt_alert_repo.get("alert-002")
        assert alert3 == int_bt_alert_repo.get("alert-003")

    def test_add_and_get_alerts_with_gw_events(self, int_bt_alert_repo):
        alert1_gw = [{"gracedb_id": "MS123"}, {"gracedb_id": "MS111"}]
        alert2_gw = [{"gracedb_id": "MS123"}]
        pre_alert1 = build_alert(
            id="alert-001", properties={"name": "1"}, grav_wave_events=alert1_gw
        )
        assert pre_alert1.grav_wave_events == alert1_gw
        int_bt_alert_repo.add(pre_alert1, "l1")
        int_bt_alert_repo.add(
            build_alert(
                id="alert-002", properties={"name": "2"}, grav_wave_events=alert2_gw
            ),
            "l1",
        )
        int_bt_alert_repo.add(
            build_alert(id="alert-003", properties={"name": "3"}), "l2"
        )
        alert1 = int_bt_alert_repo.get("alert-001")
        assert alert1.properties == {"name": "1"}
        assert alert1.grav_wave_events == alert1_gw
        alert2 = int_bt_alert_repo.get("alert-002")
        assert alert2.properties == {"name": "2"}
        assert alert2.grav_wave_events == alert2_gw
        alert3 = int_bt_alert_repo.get("alert-003")
        assert alert3.properties == {"name": "3"}
        assert alert3.grav_wave_events == []

    def test_add_duplicate_alert_overwrites(self, int_bt_alert_repo):
        int_bt_alert_repo.add(
            build_alert(id="alert-001", properties={"name": "1"}), "l1"
        )
        int_bt_alert_repo.add(
            build_alert(id="alert-001", properties={"name": "2"}), "l1"
        )
        assert int_bt_alert_repo.get("alert-001").properties == {"name": "2"}

    def test_handles_add_many_alerts(self, int_bt_alert_repo):
        alerts = [
            build_alert(
                id="alert-001", created_at=trim_to_milliseconds(datetime.utcnow())
            ),
            build_alert(
                id="alert-002", created_at=trim_to_milliseconds(datetime.utcnow())
            ),
            build_alert(
                id="alert-003", created_at=trim_to_milliseconds(datetime.utcnow())
            ),
        ]
        int_bt_alert_repo.add_many_if_not_exists(zip(alerts, repeat("locus-001")))
        assert int_bt_alert_repo.get("alert-001") == alerts[0]
        assert int_bt_alert_repo.get("alert-002") == alerts[1]
        assert int_bt_alert_repo.get("alert-003") == alerts[2]

    def test_bigtable_alert_repository_add_many_alerts_doesnt_overwrite_duplicates(
        self, int_bt_alert_repo
    ):
        alert = build_alert(id="alert-001", properties={"foo": "bar"})
        int_bt_alert_repo.add(alert, "locus-001")
        alerts = [
            build_alert(id="alert-001", properties={"foo": "baz"}),
            build_alert(id="alert-002"),
            build_alert(id="alert-003"),
        ]
        int_bt_alert_repo.add_many_if_not_exists(zip(alerts, repeat("locus-001")))
        assert int_bt_alert_repo.get("alert-001")
        assert int_bt_alert_repo.get("alert-002")
        assert int_bt_alert_repo.get("alert-003")
        assert int_bt_alert_repo.get("alert-001").properties == {"foo": "bar"}

    def test_bigtable_int_bt_alert_repo_saves_alert(self, int_bt_alert_repo):
        alert = build_alert(created_at=trim_to_milliseconds(datetime.utcnow()))
        int_bt_alert_repo.add(alert, "locus-001")
        actual = int_bt_alert_repo.get(alert.id)
        assert actual == alert

    def test_can_get_alerts_by_locus_id(self, int_bt_alert_repo):
        alerts = build_alerts(n=3, created_at=trim_to_milliseconds(datetime.utcnow()))
        int_bt_alert_repo.add(alerts[0], "locus-001")
        int_bt_alert_repo.add(alerts[1], "locus-001")
        int_bt_alert_repo.add(alerts[2], "locus-002")
        assert int_bt_alert_repo.list_by_locus_id("locus-001") == [alerts[0], alerts[1]]

    def test_can_get_alert_by_alert_id(self, int_bt_alert_repo):
        alerts = build_alerts(n=3, created_at=trim_to_milliseconds(datetime.utcnow()))
        int_bt_alert_repo.add(alerts[0], "locus-001")
        int_bt_alert_repo.add(alerts[1], "locus-001")
        int_bt_alert_repo.add(alerts[2], "locus-001")
        assert int_bt_alert_repo.get(alerts[0].id) == alerts[0]
        assert int_bt_alert_repo.get(alerts[1].id) == alerts[1]
        assert int_bt_alert_repo.get(alerts[2].id) == alerts[2]
