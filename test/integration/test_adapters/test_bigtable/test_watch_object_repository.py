from test.fakes import build_watch_object
from uuid import UUID

from astropy import units as u
from astropy.coordinates import Angle, SkyCoord


class TestBigtableWatchObjectRepository:
    def test_returns_all_watch_objects_at_location(self, int_bt_watch_object_repo):
        object1 = build_watch_object(
            id=UUID(int=1),
            location=SkyCoord(ra=0, dec=0, unit=u.deg),
            radius=Angle(1, unit=u.deg),
        )
        object2 = build_watch_object(
            id=UUID(int=1),
            location=SkyCoord(ra=0, dec=2, unit=u.deg),
            radius=Angle(1, unit=u.deg),
        )
        int_bt_watch_object_repo.add(object1)
        int_bt_watch_object_repo.add(object2)

        location = SkyCoord(ra=0, dec=0, unit=u.deg)
        results = list(int_bt_watch_object_repo.list_by_location(location))
        assert len(results) == 1
        assert results[0] == object1

    def test_returns_empty_list_if_no_object_at_location(
        self, int_bt_watch_object_repo
    ):
        for i in range(2, 4):
            int_bt_watch_object_repo.add(
                build_watch_object(
                    location=SkyCoord(ra=0, dec=i, unit=u.deg),
                    radius=Angle(1, unit=u.deg),
                )
            )
        location = SkyCoord(ra=0, dec=0, unit=u.deg)
        results = list(int_bt_watch_object_repo.list_by_location(location))
        assert results == []

    def test_returns_objects_of_a_watch_list(self, int_bt_watch_object_repo):
        int_bt_watch_object_repo.add(
            build_watch_object(id=UUID(int=2), watch_list_id=UUID(int=1))
        )
        int_bt_watch_object_repo.add(
            build_watch_object(id=UUID(int=3), watch_list_id=UUID(int=1))
        )
        int_bt_watch_object_repo.add(
            build_watch_object(id=UUID(int=4), watch_list_id=UUID(int=2))
        )
        results = list(int_bt_watch_object_repo.list_by_watch_list_id(UUID(int=1)))
        uuid_results = [w.id for w in results]
        assert len(results) == 2
        assert UUID(int=2) in uuid_results
        assert UUID(int=3) in uuid_results
