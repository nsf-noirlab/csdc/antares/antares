from test.fakes import build_watch_list
from uuid import UUID


class TestBigtableWatchListRepository:
    def test_returns_watch_lists_owned_by_a_user(self, int_bt_watch_list_repo):
        int_bt_watch_list_repo.add(
            build_watch_list(
                id=UUID(hex="00000000-0000-0000-0000-000000000001"),
                owner_id=UUID(int=1),
            )
        )
        int_bt_watch_list_repo.add(
            build_watch_list(
                id=UUID(hex="00000000-0000-0000-0000-000000000002"),
                owner_id=UUID(int=1),
            )
        )
        int_bt_watch_list_repo.add(
            build_watch_list(
                id=UUID(hex="00000000-0000-0000-0000-000000000003"),
                owner_id=UUID(int=2),
            )
        )
        results = int_bt_watch_list_repo.list_by_owner_id(UUID(int=1))

        results = list(results)  # Returns an iterable
        assert len(results) == 2
        watch_list_ids = [watch_list.id for watch_list in results]
        assert UUID(hex="00000000-0000-0000-0000-000000000001") in watch_list_ids
        assert UUID(hex="00000000-0000-0000-0000-000000000002") in watch_list_ids

    def test_can_get_by_id(self, int_bt_watch_list_repo):
        int_bt_watch_list_repo.add(build_watch_list(id=UUID(int=1)))
        assert (
            int_bt_watch_list_repo.get(
                watchlist_id="00000000-0000-0000-0000-000000000001"
            )
            is not None
        )

    def test_can_delete(self, int_bt_watch_list_repo):
        int_bt_watch_list_repo.add(
            build_watch_list(id="00000000-0000-0000-0000-000000000001")
        )
        assert (
            int_bt_watch_list_repo.get("00000000-0000-0000-0000-000000000001")
            is not None
        )
        int_bt_watch_list_repo.delete("00000000-0000-0000-0000-000000000001")
        assert (
            int_bt_watch_list_repo.get("00000000-0000-0000-0000-000000000001") is None
        )
