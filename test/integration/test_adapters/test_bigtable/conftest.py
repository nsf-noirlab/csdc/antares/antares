from test.bootstrap import bootstrap_bigtable, generate_container_config_dict

import pytest

from antares.bootstrap import bootstrap

# NOTE: Destry would prefer that the Repos were created here instead
# of having to bootstrap, but creating the cassandra session requires
# dependency injection to be wired up currently


@pytest.fixture(scope="module")  # so that it's only called once
def container():
    container = bootstrap()
    container.config.from_dict(generate_container_config_dict("bigtable"))
    yield container
    container.unwire()


@pytest.fixture
def int_bt_locus_repo(container):
    bootstrap_bigtable(container)
    locus_repository = container.locus_repository()
    yield locus_repository
    locus_repository.locus_table.truncate()
    locus_repository.alert_lut.truncate()


@pytest.fixture
def int_bt_alert_repo(container):
    bootstrap_bigtable(container)
    alert_repository = container.alert_repository()
    yield alert_repository
    alert_repository.alert_table.truncate()
    alert_repository.locus_by_alert_id_table.truncate()


@pytest.fixture
def int_bt_watch_list_repo(container):
    bootstrap_bigtable(container)
    watch_list_repository = container.watch_list_repository()
    yield watch_list_repository
    watch_list_repository.watch_list_table.truncate()


@pytest.fixture
def int_bt_watch_object_repo(container):
    bootstrap_bigtable(container)
    watch_object_repository = container.watch_object_repository()
    yield watch_object_repository
    watch_object_repository.watch_object_table.truncate()
    watch_object_repository.alert_lut.truncate()
