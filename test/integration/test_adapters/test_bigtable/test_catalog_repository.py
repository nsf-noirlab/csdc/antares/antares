import datetime
import decimal
from test.fakes import (
    build_bigtable_catalog_object_table_description,
    build_catalog_object_using_table_description,
)
from test.utils import create_catalog_tables, delete_catalog_tables

import pytest
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from dependency_injector import providers
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter

from antares.adapters.repository.bigtable.catalog import BigtableCatalogObjectRepository
from antares.adapters.repository.bigtable.schemas import CatalogLutColumns
from antares.adapters.repository.bigtable.utils import encode_any
from antares.bootstrap import bootstrap
from antares.domain.models import Catalog
from antares.observability.metrics.base import AbstractMetrics
from antares.settings import bigtable_instance_factory
from antares.utils import spatial


@pytest.fixture
def int_bt_catalog_object_repo(request, tmp_path):
    """
    Attribute of request:
        param : list[BigtableCatalogObjectTableDescription]
    Note:
    The param inside of request are the catalogs,
    this fixture doesn't work when catalogs are pass directly
    """
    container = bootstrap()
    container.config.from_dict(
        {
            "repositories": {
                "catalog": {"datastore": "bigtable"},
            },
            "external": {
                "bigtable": {
                    "schemas": {"catalog": {"tables": request.param}},
                },
            },
        }
    )
    schema_file = tmp_path / "table_schemas.yaml"
    schema_file.touch()
    schema_file.write_text(
        """
        catalog_001:
            id: int
            ra_deg: double
            dec_deg: double
            name: str
            feature1: str
            feature400: double
            feature314: decimal
            radius: double
            some_datetime: timestamp
            some_tinyint: tinyint
            some_smallint: smallint
            some_bigint: bigint
    """
    )
    container.init_resources()
    catalog_repository_singleton = providers.Singleton(
        BigtableCatalogObjectRepository,
        instance_=providers.Factory(
            bigtable_instance_factory,
            project_id=container.config.external.bigtable.parameters.project_id,
            instance_id=container.config.external.bigtable.parameters.instance_id,
        ),
        catalog_lut_id=container.config.external.bigtable.schemas.catalog_lut.table_name,
        catalog_lut_cf=container.config.external.bigtable.schemas.catalog_lut.column_family_id,
        schemas=container.config.external.bigtable.schemas.catalog.tables,
        schema_file=tmp_path / "table_schemas.yaml",
    )
    container.catalog_object_repository.override(catalog_repository_singleton)
    catalog_object_repository = container.catalog_object_repository()
    create_catalog_tables(
        catalog_object_repository.catalog_tables_with_description.values()
    )
    yield catalog_object_repository
    catalog_object_repository.catalog_lut.truncate()
    delete_catalog_tables(
        catalog_object_repository.catalog_tables_with_description.values()
    )
    container.unwire()


@pytest.fixture
def int_bt_catalog_object_repo_using_file(request):
    """
    Attribute of request:
        param : list[BigtableCatalogObjectTableDescription]
    Note:
    The param inside of request are the catalogs,
    this fixture doesn't work when catalogs are pass directly
    """
    container = bootstrap()

    container.config.from_dict(
        {
            "repositories": {
                "catalog": {
                    "datastore": "bigtable",
                    "schema_file": "test/data/table_schemas.yaml",
                },
            },
            "external": {
                "bigtable": {
                    "schemas": {"catalog": {"tables": request.param}},
                },
            },
        }
    )
    container.init_resources()
    catalog_repository = container.catalog_object_repository()
    create_catalog_tables(catalog_repository.catalog_tables_with_description.values())
    yield catalog_repository
    catalog_repository.catalog_lut.truncate()
    delete_catalog_tables(catalog_repository.catalog_tables_with_description.values())
    container.unwire()


COMMON_CATALOG_TABLE_DESCRIPTIONS = [
    build_bigtable_catalog_object_table_description(),
]

CATALOG_TABLE_GAIA_DR3_VARIABILITY = [
    build_bigtable_catalog_object_table_description(
        table="gaia_dr3_variability",
        column_family="C",
        catalog_id=26,
        display_name="GAIA_DR3_VARIABILITY",
        enabled=True,
        ra_column="ra_icrs",
        dec_column="de_icrs",
        object_id_column="source",
        object_id_type="int",
        object_name_column="class",
        radius=0.00028,
        radius_column=None,
        radius_unit=None,
    ),
]

CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN = [
    build_bigtable_catalog_object_table_description(
        radius_column="radius",
        radius_unit="degree",
    ),
]

CATALOG_TABLE_DESCRIPTIONS_W_RADIUS = [
    build_bigtable_catalog_object_table_description(
        radius=Angle(angle=2, unit=u.arcsecond).deg,
    ),
]


class TestBigtableCatalogObjectRepository:
    @pytest.mark.parametrize("int_bt_catalog_object_repo", [[]], indirect=True)
    def test_raises_value_error_if_adding_object_without_catalog_definition(
        self,
        int_bt_catalog_object_repo: BigtableCatalogObjectRepository,
    ):
        catalog_object = build_catalog_object_using_table_description(
            {"id": 15, "ra_deg": 0, "dec_deg": 0, "name": "Catalog Object"},
            COMMON_CATALOG_TABLE_DESCRIPTIONS[0],
        )
        with pytest.raises(
            ValueError, match="Catalog table description could not be found "
        ):
            int_bt_catalog_object_repo.add(catalog_object)

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo", [COMMON_CATALOG_TABLE_DESCRIPTIONS], indirect=True
    )
    def test_correctly_builds_catalogs(
        self, int_bt_catalog_object_repo: BigtableCatalogObjectRepository
    ):
        catalog_list = int_bt_catalog_object_repo.list_catalogs()
        assert catalog_list == [Catalog("1", "catalog_001")]

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo", [COMMON_CATALOG_TABLE_DESCRIPTIONS], indirect=True
    )
    def test_raises_value_error_if_no_radius_nor_default(
        self, int_bt_catalog_object_repo: BigtableCatalogObjectRepository
    ):
        (
            _,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 1,
                "ra_deg": 100.0,
                "dec_deg": 40.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        with pytest.raises(ValueError, match="no radius"):
            int_bt_catalog_object_repo.add(catalog_object)

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo", [COMMON_CATALOG_TABLE_DESCRIPTIONS], indirect=True
    )
    def test_catalog_object_use_global_default_radius(
        self, int_bt_catalog_object_repo: BigtableCatalogObjectRepository
    ):
        (
            catalog_table,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 5,
                "ra_deg": 100.0,
                "dec_deg": 40.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        int_bt_catalog_object_repo.insert_catalog_object_into_its_catalog_table(
            catalog_object,
            catalog_table,
            table_description,
        )
        row = catalog_table.read_row(b"5", filter_=CellsColumnLimitFilter(1))
        retrieved_catalog_object = (
            int_bt_catalog_object_repo.create_catalog_object_from_row(
                row, table_description
            )
        )
        assert retrieved_catalog_object.radius is not None
        assert retrieved_catalog_object.radius.deg == Angle(1, unit=u.arcsecond).deg

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo",
        [CATALOG_TABLE_DESCRIPTIONS_W_RADIUS],
        indirect=True,
    )
    def test_catalog_object_use_catalog_default_radius(
        self, int_bt_catalog_object_repo: BigtableCatalogObjectRepository
    ):
        (
            catalog_table,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 5,
                "ra_deg": 100.0,
                "dec_deg": 40.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        int_bt_catalog_object_repo.add(catalog_object)
        row = catalog_table.read_row(b"5", filter_=CellsColumnLimitFilter(1))
        retrieved_catalog_object = (
            int_bt_catalog_object_repo.create_catalog_object_from_row(
                row, table_description
            )
        )
        assert retrieved_catalog_object.radius == Angle(2, unit=u.arcsecond)

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo_using_file",
        [CATALOG_TABLE_GAIA_DR3_VARIABILITY],
        indirect=True,
    )
    def test_catalog_object_roundtrip_using_file(
        self, int_bt_catalog_object_repo_using_file: BigtableCatalogObjectRepository
    ):
        (
            catalog_table,
            table_description,
        ) = int_bt_catalog_object_repo_using_file.get_catalog_table_with_description(
            CATALOG_TABLE_GAIA_DR3_VARIABILITY[0]["catalog_id"]
        )
        catalog_object = build_catalog_object_using_table_description(
            properties={
                "source": decimal.Decimal("5859798777346665000"),
                "class": "SOLAR_LIKE",
                "classifier": "nTransits:5+",
                "de_icrs": decimal.Decimal("-67.02499306546"),
                "ra_icrs": decimal.Decimal("188.5840734099"),
                "solid": decimal.Decimal("375316653866487550"),
            },
            table_description=table_description,
        )
        int_bt_catalog_object_repo_using_file.add(catalog_object)
        row = catalog_table.read_row(
            b"5859798777346665000", filter_=CellsColumnLimitFilter(1)
        )
        retrieved_catalog_object = (
            int_bt_catalog_object_repo_using_file.create_catalog_object_from_row(
                row, table_description
            )
        )
        assert catalog_object.id == retrieved_catalog_object.id
        assert catalog_object.catalog_id == retrieved_catalog_object.catalog_id
        assert catalog_object.location == retrieved_catalog_object.location
        assert catalog_object.radius is not None
        assert retrieved_catalog_object.radius is not None
        assert catalog_object.radius.deg == retrieved_catalog_object.radius.deg
        assert catalog_object.radius.arcsec == retrieved_catalog_object.radius.arcsec
        assert catalog_object.name == retrieved_catalog_object.name
        assert catalog_object.catalog_name == retrieved_catalog_object.catalog_name
        assert catalog_object.properties == retrieved_catalog_object.properties

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo",
        [CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN],
        indirect=True,
    )
    def test_catalog_object_roundtrip(
        self, int_bt_catalog_object_repo: BigtableCatalogObjectRepository
    ):
        (
            catalog_table,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 2,
                "ra_deg": 100.0,
                "dec_deg": 40.0,
                "radius": Angle("13s"),
                "name": "Catalog Object 001",
                "some_datetime": datetime.datetime(2023, 2, 9),
                "feature1": "abcd",
                "feature400": 0.1,
                "feature314": decimal.Decimal(0.1),
                "some_tinyint": 1,
                "some_smallint": 1,
                "some_bigint": 1,
            },
            table_description,
        )
        int_bt_catalog_object_repo.add(catalog_object)
        row = catalog_table.read_row(b"2", filter_=CellsColumnLimitFilter(1))
        retrieved_catalog_object = (
            int_bt_catalog_object_repo.create_catalog_object_from_row(
                row, table_description
            )
        )
        assert catalog_object.id == retrieved_catalog_object.id
        assert catalog_object.catalog_id == retrieved_catalog_object.catalog_id
        assert catalog_object.location == retrieved_catalog_object.location
        assert catalog_object.radius is not None
        assert retrieved_catalog_object.radius is not None
        assert catalog_object.radius.deg == retrieved_catalog_object.radius.deg
        assert catalog_object.radius.arcsec == retrieved_catalog_object.radius.arcsec
        assert catalog_object.name == retrieved_catalog_object.name
        assert catalog_object.catalog_name == retrieved_catalog_object.catalog_name
        assert catalog_object.properties == retrieved_catalog_object.properties

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo_using_file",
        [CATALOG_TABLE_GAIA_DR3_VARIABILITY],
        indirect=True,
    )
    def test_retrieve_when_a_column_is_missing(
        self, int_bt_catalog_object_repo_using_file: BigtableCatalogObjectRepository
    ):
        (
            catalog_table,
            table_description,
        ) = int_bt_catalog_object_repo_using_file.get_catalog_table_with_description(
            CATALOG_TABLE_GAIA_DR3_VARIABILITY[0]["catalog_id"]
        )
        row = catalog_table.direct_row(b"2")
        row.set_cell(
            table_description["column_family"],
            table_description["object_name_column"],
            "Catalog Object 001",
        )
        row.set_cell(
            table_description["column_family"],
            table_description["ra_column"],
            encode_any(decimal.Decimal(101.4), "decimal"),
        )
        row.set_cell(
            table_description["column_family"],
            table_description["dec_column"],
            encode_any(decimal.Decimal(40.7), "decimal"),
        )
        row.commit()
        row = catalog_table.read_row(b"2", filter_=CellsColumnLimitFilter(1))
        retrieved_catalog_object = (
            int_bt_catalog_object_repo_using_file.create_catalog_object_from_row(
                row, table_description
            )
        )
        assert retrieved_catalog_object.id == "2"
        assert retrieved_catalog_object.catalog_id == str(
            CATALOG_TABLE_GAIA_DR3_VARIABILITY[0]["catalog_id"]
        )
        assert retrieved_catalog_object.location == SkyCoord(
            ra=101.4, dec=40.7, unit=u.deg
        )
        assert retrieved_catalog_object.radius is not None
        assert Angle(0.00028, u.deg).deg == retrieved_catalog_object.radius.deg
        assert Angle(0.00028, u.deg).arcsec == retrieved_catalog_object.radius.arcsec
        assert retrieved_catalog_object.name == "Catalog Object 001"
        assert retrieved_catalog_object.catalog_name == "gaia_dr3_variability"
        assert retrieved_catalog_object.properties == {
            "source": 2,
            "class": "Catalog Object 001",
            "classifier": None,
            "de_icrs": decimal.Decimal(40.7),
            "ra_icrs": decimal.Decimal(101.4),
            "solid": None,
        }

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo",
        [CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN],
        indirect=True,
    )
    def test_list_by_location_filters_on_radius(
        self,
        int_bt_catalog_object_repo: BigtableCatalogObjectRepository,
        metrics: AbstractMetrics,
    ):
        (
            _,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 2,
                "ra_deg": 0.0,
                "dec_deg": 0.0,
                "radius": 6.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        int_bt_catalog_object_repo.add(catalog_object)
        filtered_objects = int_bt_catalog_object_repo.list_by_location(
            SkyCoord(ra=6.0001, dec=0, unit=u.deg), metrics=metrics
        )
        assert not (list(filtered_objects))
        filtered_objects = int_bt_catalog_object_repo.list_by_location(
            SkyCoord(ra=5.9999, dec=0, unit=u.deg),
            metrics=metrics,
        )
        assert len(list(filtered_objects)) == 1
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 5,
                "ra_deg": 1.0,
                "dec_deg": 1.0,
                "radius": 8.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        int_bt_catalog_object_repo.add(catalog_object)
        filtered_objects = int_bt_catalog_object_repo.list_by_location(
            SkyCoord(ra=9.002, dec=1, unit=u.deg),
            metrics=metrics,
        )
        assert not (list(filtered_objects))
        filtered_objects = int_bt_catalog_object_repo.list_by_location(
            SkyCoord(ra=7.999, dec=1, unit=u.deg),
            metrics=metrics,
        )
        assert len(list(filtered_objects)) == 1

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo",
        [CATALOG_TABLE_DESCRIPTIONS_W_RADIUS],
        indirect=True,
    )
    def test_returns_all_catalog_objects_at_location(
        self,
        int_bt_catalog_object_repo: BigtableCatalogObjectRepository,
        metrics: AbstractMetrics,
    ):
        (
            _,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        object1 = build_catalog_object_using_table_description(
            {
                "id": 15,
                "ra_deg": 0.0,
                "dec_deg": 0.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        object2 = build_catalog_object_using_table_description(
            {
                "id": 31,
                "ra_deg": 100.0,
                "dec_deg": 40.0,
                "name": "Catalog Object 002",
            },
            table_description,
        )
        int_bt_catalog_object_repo.add(object1)
        int_bt_catalog_object_repo.add(object2)
        catalog_objects = list(
            int_bt_catalog_object_repo.list_by_location(
                SkyCoord(ra=0, dec=0, unit=u.deg), metrics
            )
        )
        assert len(catalog_objects) == 1
        assert catalog_objects[0].id == "15"

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo",
        [CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN],
        indirect=True,
    )
    def test_works_correctly_for_extended_sources(
        self,
        int_bt_catalog_object_repo: BigtableCatalogObjectRepository,
        metrics: AbstractMetrics,
    ):
        (
            _,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 60,
                "ra_deg": 0.0,
                "dec_deg": 0.0,
                "radius": 4.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        int_bt_catalog_object_repo.add(catalog_object)
        catalog_objects = list(
            int_bt_catalog_object_repo.list_by_location(
                SkyCoord(ra=3.9, dec=0, unit=u.deg), metrics
            )
        )
        assert len(catalog_objects) == 1
        assert catalog_objects[0].id == "60"

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo",
        [CATALOG_TABLE_DESCRIPTIONS_W_RADIUS],
        indirect=True,
    )
    def test_returns_empty_list_if_no_objects_at_location(
        self,
        metrics: AbstractMetrics,
        int_bt_catalog_object_repo: BigtableCatalogObjectRepository,
    ):
        (
            _,
            table_description,
        ) = int_bt_catalog_object_repo.get_catalog_table_with_description(1)
        object1 = build_catalog_object_using_table_description(
            {
                "id": 3,
                "ra_deg": 0.0,
                "dec_deg": 0.0,
                "name": "Catalog Object 001",
            },
            table_description,
        )
        object2 = build_catalog_object_using_table_description(
            {
                "id": 8,
                "ra_deg": 100.0,
                "dec_deg": 40.0,
                "name": "Catalog Object 002",
            },
            table_description,
        )
        int_bt_catalog_object_repo.add(object1)
        int_bt_catalog_object_repo.add(object2)
        assert not (
            list(
                int_bt_catalog_object_repo.list_by_location(
                    SkyCoord(ra=10, dec=10, unit=u.deg), metrics
                )
            )
        )

    @pytest.mark.parametrize(
        "int_bt_catalog_object_repo",
        [CATALOG_TABLE_DESCRIPTIONS_W_RADIUS],
        indirect=True,
    )
    def test_handles_case_when_phantom_lookup_entry(
        self,
        int_bt_catalog_object_repo: BigtableCatalogObjectRepository,
        metrics: AbstractMetrics,
    ):
        htm_ids = spatial.get_htm_region_denormalized(
            SkyCoord(ra=0, dec=0, unit=u.deg), Angle(angle=1, unit=u.arcsec), 16
        )
        value = "1:1"
        # 1. Insert into the lookup table
        for htm_id in htm_ids:
            row = int_bt_catalog_object_repo.catalog_lut.direct_row(
                CatalogLutColumns.create_row_key(htm_id, value)
            )
            row.set_cell(
                int_bt_catalog_object_repo.catalog_lut_cf, CatalogLutColumns.DATA, b"."
            )
            row.commit()
        results = list(
            int_bt_catalog_object_repo.list_by_location(
                SkyCoord(ra=0, dec=0, unit=u.deg), metrics
            )
        )
        assert not results
