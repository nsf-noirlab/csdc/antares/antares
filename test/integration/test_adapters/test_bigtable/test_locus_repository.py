import random

import pytest
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from astropy.timeseries import TimeSeries
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter

from antares.adapters.repository.bigtable.locus import LocusNotFoundError
from antares.domain.models import Locus
from antares.utils import mjd_to_datetime, spatial


def build_timeseries_for_locus(length: int):
    return TimeSeries(
        data=[
            {
                "alert_id": f"alert-{i}",
                "ant_mjd": 59000.0 + i,
                "ant_mag": random.random() + 15.0,
            }
            for i in range(length)
        ],
        time=[mjd_to_datetime(59000.0 + i) for i in range(length)],
    )


class TestBigtableLocusRepository:
    def test_correctly_roundtrips_lightcurve(self, int_bt_locus_repo):
        lightcurve = build_timeseries_for_locus(10)
        locus = Locus(
            "locus-001", SkyCoord(ra=0, dec=0, unit=u.arcsec), lightcurve=lightcurve
        )
        int_bt_locus_repo.add(locus)
        actual = int_bt_locus_repo.get(locus.id).lightcurve
        assert all(lightcurve == actual)

    def test_add_and_get_locus(self, int_bt_locus_repo):
        lightcurve = build_timeseries_for_locus(1)
        input_locus = Locus(
            "locus-001", SkyCoord(ra=0, dec=0, unit=u.deg), lightcurve=lightcurve
        )
        int_bt_locus_repo.add(input_locus)
        locus = int_bt_locus_repo.get(input_locus.id)
        assert locus is not None
        assert locus.id == "locus-001"
        assert locus.location == SkyCoord(ra=0, dec=0, unit=u.deg)
        assert locus.catalogs == set()
        assert locus.tags == set()
        assert locus.watch_object_matches == set()
        assert locus.properties == {}
        assert all(lightcurve == locus.lightcurve)
        assert locus.grav_wave_events == []

    def test_add_and_get_locus_with_grav_wave_events(self, int_bt_locus_repo):
        grav_wave_events = ["MS123", "MS321"]
        input_locus = Locus(
            "locus-001",
            SkyCoord(ra=0, dec=0, unit=u.deg),
            grav_wave_events=grav_wave_events,
        )
        int_bt_locus_repo.add(input_locus)
        locus = int_bt_locus_repo.get(input_locus.id)
        assert locus.grav_wave_events == grav_wave_events

    def test_adding_locus_adds_htm_lookup_entry(self, int_bt_locus_repo):
        int_bt_locus_repo.add(Locus("locus-001", SkyCoord(ra=0, dec=0, unit=u.deg)))

        htm_lookup_entries = []

        for row in int_bt_locus_repo.alert_lut.read_rows(
            filter_=CellsColumnLimitFilter(1)
        ):
            htm, type_, value = row.row_key.decode().split("#")
            htm_lookup_entries.append(
                {
                    "htm": int(htm),
                    "type": int(type_),
                    "value": value,
                },
            )

        assert sorted(htm_lookup_entries, key=lambda entry: entry["htm"]) == [
            {
                "htm": spatial.get_htm_id(SkyCoord(ra=0, dec=0, unit=u.deg), htm_level),
                "type": 1,
                "value": "locus-001",
            }
            for htm_level in (4, 6, 8, 10, 12, 14, 16)
        ]
        for entry in htm_lookup_entries:
            assert entry["value"] == "locus-001"

    def test_returns_all_loci_within_cone_search(self, int_bt_locus_repo):
        int_bt_locus_repo.add(Locus("locus-001", SkyCoord(ra=0, dec=0, unit=u.deg)))
        int_bt_locus_repo.add(
            Locus("locus-002", SkyCoord(ra=0, dec=2, unit=[u.deg, u.arcsec]))
        )

        result = int_bt_locus_repo.list_by_cone_search(
            SkyCoord(ra=0, dec=0, unit=u.deg), Angle(1, unit=u.arcsec)
        )
        result = list(result)

        assert len(result) == 1
        assert result[0].id == "locus-001"

    def test_returns_empty_list_if_no_loci_within_cone_search(self, int_bt_locus_repo):
        int_bt_locus_repo.add(
            Locus("locus-001", SkyCoord(ra=0, dec=2, unit=[u.deg, u.arcsec]))
        )
        int_bt_locus_repo.add(
            Locus("locus-002", SkyCoord(ra=0, dec=3, unit=[u.deg, u.arcsec]))
        )

        result = int_bt_locus_repo.list_by_cone_search(
            SkyCoord(ra=0, dec=0, unit=u.deg), Angle(1, unit=u.arcsec)
        )
        result = list(result)

        assert len(result) == 0

    def test_raises_error_if_no_locus_with_locus_id(self, int_bt_locus_repo):
        int_bt_locus_repo.add(Locus("locus-001", SkyCoord(ra=0, dec=0, unit=u.deg)))
        with pytest.raises(LocusNotFoundError) as err:
            int_bt_locus_repo.get("locus-002")
            assert "Locus locus-002 not found" in str(err)

    def test_can_update_locus(self, int_bt_locus_repo):
        int_bt_locus_repo.add(Locus("locus-001", SkyCoord(ra=0, dec=0, unit=u.deg)))

        locus = int_bt_locus_repo.get("locus-001")
        locus.location = SkyCoord(ra=1, dec=1, unit=u.deg)
        int_bt_locus_repo.update(locus)

        updated_locus = int_bt_locus_repo.get("locus-001")
        assert updated_locus.location == SkyCoord(ra=1, dec=1, unit=u.deg)

    def test_can_update_locus_properties(self, int_bt_locus_repo):
        int_bt_locus_repo.add(Locus("locus-001", SkyCoord(ra=0, dec=0, unit=u.deg)))

        locus = int_bt_locus_repo.get("locus-001")
        locus.properties = {"new_property": None}
        int_bt_locus_repo.update(locus)
        updated_locus = int_bt_locus_repo.get("locus-001")
        assert updated_locus.properties == {"new_property": None}

    def test_ignores_replaced_loci_in_cone_search(self, int_bt_locus_repo):
        int_bt_locus_repo.add(
            Locus(
                "locus-001",
                SkyCoord(ra=0, dec=0, unit=u.arcsec),
                properties={"replaced_by": "locus-002"},
            )
        )
        int_bt_locus_repo.add(Locus("locus-002", SkyCoord(ra=0, dec=0, unit=u.arcsec)))

        result = int_bt_locus_repo.list_by_cone_search(
            SkyCoord(ra=0, dec=0, unit=u.deg), Angle(1, unit=u.arcsec)
        )
        result = list(result)
        assert len(result) == 1
        assert result[0].id == "locus-002"
