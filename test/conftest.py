from typing import Optional
from unittest.mock import create_autospec

import pytest
from google.cloud.bigtable_v2.types import MutateRowsRequest

from antares import bootstrap
from antares.domain.models import (
    Alert,
    Announcement,
    Blob,
    Catalog,
    CatalogObject,
    Filter,
    FilterRevision,
    GravWaveNotice,
    Locus,
    LocusAnnotation,
    User,
    WatchList,
    WatchObject,
)
from antares.settings import Container

from .fakes import (
    FakeAlertRepository,
    FakeAnnouncementRepository,
    FakeBlobRepository,
    FakeCatalogObjectRepository,
    FakeDistributedLock,
    FakeFilterRepository,
    FakeFilterRevisionRepository,
    FakeGravWaveRepository,
    FakeInternalNotificationService,
    FakeJwtBlocklistRepository,
    FakeLocusAnnotationRepository,
    FakeLocusRepository,
    FakeMessagePublicationService,
    FakeMetrics,
    FakeNotificationService,
    FakeTracer,
    FakeUserRepository,
    FakeWatchListRepository,
    FakeWatchObjectRepository,
)


@pytest.fixture
def successful_mutate_row_mock():
    mutate_row_entry_mock = create_autospec(MutateRowsRequest.Entry, instance=True)
    mutate_row_entry_mock.message = ""
    mutate_row_entry_mock.code = 0
    return mutate_row_entry_mock


@pytest.fixture
def unsuccessful_mutate_row_mock():
    mutate_row_entry_mock = create_autospec(MutateRowsRequest.Entry, instance=True)
    mutate_row_entry_mock.message = "test_error"
    mutate_row_entry_mock.code = 400
    return mutate_row_entry_mock


@pytest.fixture
def alert_repository():
    yield FakeAlertRepository()


@pytest.fixture
def alert_repository_populated(alert_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, Alert):
                alert_repository.add(model)
    yield alert_repository


@pytest.fixture
def announcement_repository():
    yield FakeAnnouncementRepository()


@pytest.fixture
def announcement_repository_populated(announcement_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, Announcement):
                announcement_repository.add(model)
    yield announcement_repository


@pytest.fixture
def blob_repository():
    yield FakeBlobRepository()


@pytest.fixture
def blob_repository_populated(blob_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, Blob):
                blob_repository.add(model)
    yield blob_repository


@pytest.fixture
def catalog_object_repository(request):
    """
    The setup of this repository is a bit more complicated than the others because of
    the way that catalogs are configured in the system.
    """
    marker = request.node.get_closest_marker("with_data")
    catalogs = []
    if marker:
        catalogs = [model for model in marker.args if isinstance(model, Catalog)]
    yield FakeCatalogObjectRepository(catalogs=catalogs)


@pytest.fixture
def catalog_object_repository_populated(catalog_object_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, CatalogObject):
                catalog_object_repository.add(model)
    yield catalog_object_repository


@pytest.fixture
def filter_repository():
    yield FakeFilterRepository()


@pytest.fixture
def filter_repository_populated(filter_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, Filter):
                filter_repository.add(model)
    yield filter_repository


@pytest.fixture
def filter_revision_repository():
    yield FakeFilterRevisionRepository()


@pytest.fixture
def filter_revision_repository_populated(filter_revision_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in request.node.get_closest_marker("with_data").args:
            if isinstance(model, FilterRevision):
                filter_revision_repository.add(model)
    yield filter_revision_repository


@pytest.fixture
def grav_wave_repository():
    yield FakeGravWaveRepository()


@pytest.fixture
def grav_wave_repository_populated(grav_wave_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, GravWaveNotice):
                grav_wave_repository.add(model)
    yield grav_wave_repository


@pytest.fixture
def locus_repository():
    yield FakeLocusRepository()


@pytest.fixture
def locus_repository_populated(locus_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, Locus):
                locus_repository.add(model)
    yield locus_repository


@pytest.fixture
def locus_annotation_repository():
    yield FakeLocusAnnotationRepository()


@pytest.fixture
def locus_annotation_repository_populated(locus_annotation_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in request.node.get_closest_marker("with_data").args:
            if isinstance(model, LocusAnnotation):
                locus_annotation_repository.add(model)
    yield locus_annotation_repository


@pytest.fixture
def user_repository():
    yield FakeUserRepository()


@pytest.fixture
def user_repository_populated(user_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, User):
                user_repository.add(model)
    yield user_repository


@pytest.fixture
def watch_list_repository():
    yield FakeWatchListRepository()


@pytest.fixture
def watch_list_repository_populated(watch_list_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, WatchList):
                watch_list_repository.add(model)
    yield watch_list_repository


@pytest.fixture
def watch_object_repository():
    yield FakeWatchObjectRepository()


@pytest.fixture
def watch_object_repository_populated(watch_object_repository, request):
    marker = request.node.get_closest_marker("with_data")
    if marker:
        for model in marker.args:
            if isinstance(model, WatchObject):
                watch_object_repository.add(model)
    yield watch_object_repository


@pytest.fixture
def lock_factory():
    database = set()

    def factory(id_: str, ttl: Optional[float] = None) -> FakeDistributedLock:
        return FakeDistributedLock(database, id_, ttl)

    return factory


@pytest.fixture
def notification_service():
    return FakeNotificationService()


@pytest.fixture
def internal_notification_service():
    return FakeInternalNotificationService()


@pytest.fixture
def message_publication_service():
    return FakeMessagePublicationService()


@pytest.fixture
def tracer():
    return FakeTracer()


@pytest.fixture
def metrics():
    return FakeMetrics()


@pytest.fixture
def container(
    request,
    alert_repository_populated,
    announcement_repository_populated,
    blob_repository_populated,
    catalog_object_repository_populated,
    filter_repository_populated,
    filter_revision_repository_populated,
    grav_wave_repository_populated,
    locus_repository_populated,
    locus_annotation_repository_populated,
    user_repository_populated,
    watch_list_repository_populated,
    watch_object_repository_populated,
    lock_factory,
    notification_service,
    internal_notification_service,
    message_publication_service,
    tracer,
    metrics,
):
    container = Container(
        alert_repository=alert_repository_populated,
        announcement_repository=announcement_repository_populated,
        blob_repository=blob_repository_populated,
        catalog_object_repository=catalog_object_repository_populated,
        locus_repository=locus_repository_populated,
        filter_repository=filter_repository_populated,
        filter_revision_repository=filter_revision_repository_populated,
        grav_wave_repository=grav_wave_repository_populated,
        jwt_blocklist_repository=FakeJwtBlocklistRepository(),
        locus_annotation_repository=locus_annotation_repository_populated,
        user_repository=user_repository_populated,
        watch_list_repository=watch_list_repository_populated,
        watch_object_repository=watch_object_repository_populated,
        message_publication_service=message_publication_service,
        notification_service=notification_service,
        internal_notification_service=internal_notification_service,
        lock_factory=lock_factory,
        tracer=tracer,
        metrics=metrics,
    )
    container.init_resources()
    for marker in request.node.iter_markers("override"):
        container.config.from_dict(marker.args[0])
    yield container


@pytest.fixture
def message_bus(request, container):
    message_bus = bootstrap.bootstrap(container)
    yield message_bus
    container.unwire()
