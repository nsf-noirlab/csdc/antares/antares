import os
import tempfile
import test
import time
import uuid
from collections.abc import Iterable
from datetime import datetime
from test.bootstrap import bootstrap
from test.fakes import (
    build_bigtable_catalog_object_table_description,
    build_filter,
    build_filter_revision,
)
from test.utils import clean_all_bigtable_tables, create_catalog_tables
from typing import Any, Generator, Optional

import pytest
from astropy.coordinates import Angle, SkyCoord
from dependency_injector import providers
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter

import antares
from antares.adapters.messages import (
    KafkaMessagePublicationService,
    KafkaMessageSubscriptionService,
)
from antares.adapters.repository.bigtable import BigtableCatalogObjectRepository
from antares.domain.models import CatalogObject
from antares.entrypoints.pipeline.kafka_listener import (
    SurveyTopic,
    process_kafka_messages,
)
from antares.external import kafka
from antares.external.elasticsearch.ingest import (
    get_all_documents as es_get_all_documents,
)
from antares.external.sql.bootstrap import drop_main_db
from antares.observability.logging.default import init_default_logging
from antares.settings import Container, bigtable_instance_factory


@pytest.fixture
def container() -> Generator[Container, None, None]:
    container = bootstrap()
    container.config.from_dict(
        {
            "external": {
                "bigtable": {
                    "schemas": {
                        "catalog": {
                            "tables": [
                                build_bigtable_catalog_object_table_description(
                                    catalog_id=0, table="catalog_table"
                                )
                            ]
                        }
                    },
                },
            },
        }
    )
    with tempfile.NamedTemporaryFile("w") as schema_file:
        schema_file.write(
            """
            catalog_table:
                id: str
                ra_deg: double
                dec_deg: double
                name: str
                radius: double
        """
        )
        schema_file.flush()
        catalog_repository_singleton = providers.Singleton(
            BigtableCatalogObjectRepository,
            instance_=providers.Factory(
                bigtable_instance_factory,
                project_id=container.config.external.bigtable.parameters.project_id,
                instance_id=container.config.external.bigtable.parameters.instance_id,
            ),
            catalog_lut_id=container.config.external.bigtable.schemas.catalog_lut.table_name,
            catalog_lut_cf=container.config.external.bigtable.schemas.catalog_lut.column_family_id,
            schemas=container.config.external.bigtable.schemas.catalog.tables,
            schema_file=schema_file.name,
        )
        container.catalog_object_repository.override(catalog_repository_singleton)
        catalog_object_repository = container.catalog_object_repository()
        create_catalog_tables(
            catalog_object_repository.catalog_tables_with_description.values()
        )
        for catalog_object in EXPECTED_CATALOG_MATCHES:
            container.catalog_object_repository().add(catalog_object)
        yield container
    clean_all_bigtable_tables(container)
    drop_main_db()
    container.shutdown_resources()
    container.unwire()


@pytest.fixture
def kafka_alert_queue():
    survey_input = [
        {
            "topic": "ztf_process_alerts_test",
            "enabled": True,
            "directory": "data/alerts/ztf",  # Relative to antares/test
            "alerts": [
                "1069343660015010008.avro",
                "1069343660115010015.avro",
                "1069343660215015001.avro",
                "1069343661515015018.avro",
                "1069343660015010011.avro",
                "1069343660115015014.avro",
                "1069343660215015013.avro",
                "1069343661515015019.avro",
                "1069343660115010006.avro",
                "1069343660115015015.avro",
                "1069343661515015016.avro",
                "1069343660115010012.avro",
                "1069343660115015016.avro",
                "1069343661515015017.avro",
            ],
        },
        # {
        #     "topic": "decat_dev",
        #     "enabled": False,
        #     "directory": "data/alerts/decat",  # Relative to antares/test
        #     "alerts": [
        #         "0.avro",
        #         "1.avro",
        #         "2.avro",
        #         "3.avro",
        #         "4.avro",
        #         "5.avro",
        #         "6.avro",
        #         "7.avro",
        #         "8.avro",
        #         "9.avro",
        #     ],
        # },
        # {
        #     "topic": "elasticc_dev",
        #     "enabled": True,
        #     "directory": "data/alerts/elasticc_v2",
        #     "alerts": [
        #         "alert_mjd59582.0513_obj1130_src2260000.avro",
        #         "alert_mjd59582.0513_obj1131_src2262000.avro",
        #         "alert_mjd59582.0513_obj1138_src2276000.avro",
        #         "alert_mjd59582.0513_obj1147_src2294000.avro",
        #         "alert_mjd59583.0511_obj1131_src2262001.avro",
        #         "alert_mjd59583.0511_obj1134_src2268001.avro",
        #         "alert_mjd59583.0511_obj1138_src2276001.avro",
        #         "alert_mjd59583.0511_obj1147_src2294001.avro",
        #         "alert_mjd59583.0889_obj843_src1686000.avro",
        #         "alert_mjd59588.1079_obj806_src1612001.avro",
        #         "alert_mjd59588.1079_obj843_src1686001.avro",
        #         "alert_mjd59588.1079_obj845_src1690001.avro",
        #         "alert_mjd59589.2819_obj949_src1898001.avro",
        #         "alert_mjd59590.0349_obj806_src1612002.avro",
        #         "alert_mjd59590.0349_obj843_src1686002.avro",
        #         "alert_mjd59590.2368_obj282_src564001.avro",
        #         "alert_mjd59594.0370_obj806_src1612003.avro",
        #         "alert_mjd59594.0370_obj843_src1686003.avro",
        #         "alert_mjd59594.1724_obj949_src1898002.avro",
        #         "alert_mjd59595.0543_obj1130_src2260002.avro",
        #         "alert_mjd59595.0543_obj1131_src2262002.avro",
        #         "alert_mjd59595.0543_obj1147_src2294002.avro",
        #         "alert_mjd59598.0504_obj806_src1612004.avro",
        #         "alert_mjd59598.0504_obj843_src1686004.avro",
        #         "alert_mjd59598.0504_obj845_src1690004.avro",
        #         "alert_mjd59602.3239_obj282_src564003.avro",
        #     ],
        # },
    ]
    # TODO: use config
    admin = kafka.KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    )
    admin.delete_all_topics(force=True)
    time.sleep(10)
    message_publication_service = KafkaMessagePublicationService(
        {
            "bootstrap.servers": "kafka",
        }
    )
    for survey in (s for s in survey_input if s["enabled"]):
        for alert in survey["alerts"]:
            filename = os.path.join(
                os.path.dirname(os.path.abspath(test.__file__)),
                survey["directory"],
                alert,
            )
            with open(filename, "rb") as f:
                blob = f.read()
            message_publication_service.publish(survey["topic"], blob)
    yield KafkaMessageSubscriptionService(
        {
            "bootstrap.servers": "kafka",
            "group.id": str(uuid.uuid4()),
            "default.topic.config": {"auto.offset.reset": "earliest"},
            "enable.auto.commit": False,
            "max.poll.interval.ms": 1000 * 60 * 30,
        },
        [survey["topic"] for survey in survey_input],
    )
    admin = kafka.KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    )
    admin.delete_all_topics(force=True)


TEST_FILTER_CODE = """
import antares.devkit as dk
from antares.devkit import Filter

class TestFilter(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "test_property",
            "type": "int",
            "description": "",
        },
    ]
    OUTPUT_TAGS = [
        {
            "name": "test_tag_dev",
            "description": "",
        },
    ]
    def run(self, locus):
        locus.properties["test_property"] = 100
        locus.tag("test_tag_dev")
"""


def enable_filter(container, filter_code):
    filter_repo = container.filter_repository()
    filter_revision_repo = container.filter_revision_repository()
    # test.bootstrap creates a filter at id=1
    filter_ = build_filter(id=99, name="E2E Filter Test")
    filter_revision = build_filter_revision(filter_, code=filter_code)
    filter_revision.enable(filter_=filter_)
    filter_repo.add(filter_)
    filter_revision_repo.add(filter_revision)
    return None


def drain_kafka_output(
    topics: list[str], timeout: float = 10.0
) -> Iterable[tuple[str, Any, Optional[datetime]]]:
    kafka_subscription = KafkaMessageSubscriptionService(
        {
            "bootstrap.servers": "kafka",
            "group.id": str(uuid.uuid4()),
            "default.topic.config": {"auto.offset.reset": "earliest"},
            "enable.auto.commit": False,
            "max.poll.interval.ms": 1000 * 60 * 30,
        },
        topics,
    )
    while True:
        try:
            yield kafka_subscription.poll(timeout)
        except antares.exceptions.Timeout:
            return


EXPECTED_CATALOG_MATCHES = [
    CatalogObject(
        id="0",
        catalog_id="0",
        name="catalog-object-name-001",
        catalog_name="catalog_table",
        location=SkyCoord("88.2744186d -5.0010774d"),
        radius=Angle("0.01d"),
        properties={
            "id": "0",
            "ra_deg": 88.2744186,
            "dec_deg": -5.0010774,
            "name": "catalog-object-name-001",
            "radius": 0.01,
        },
    ),
]


def test_process_alerts(kafka_alert_queue, container):
    """
    This E2E test verifies the alert processing service of ANTARES.

    IF
        * A ZTF alert is published onto a Kafka topic we are subscribed to

        * A filter is configured that tags loci with `test_tag`

    THEN

        * Each alert should be stored in our database (incl. previous candidates);

        * Each alert should be associated with an existing locus or should trigger
          the creation of a new locus;

        * Catalog matches should be persisted against loci that have cross-matches;

        * TODO: Watch list notifications should be distributed for matching objects;

        * A notification should be sent out to the `client.test_tag` topic in the
          configured Kafka cluster for each locus tagged with `test_tag`;

        * A notification should be sent out to the Elasticsearch (or index worker's)
          Kafka topic that serves as a buffer for search index updates for each locus;

        * After some short delay the Elasticsearch index should be updated with all new
          loci;
    """
    init_default_logging("INFO")
    enable_filter(container, TEST_FILTER_CODE)
    container.config.pipeline.output()["triggers"] = [
        {
            "name": "e2e test trigger",
            "type": "kafka",
            "criteria": {"has_tag": "test_tag_dev"},
            "topic": "test_tag_dev",
        }
    ]

    surveys_topics = [
        SurveyTopic(name="ztf", topic="^ztf_.*", watchlists_on=True, filters_on=True),
        SurveyTopic(
            name="decat_dev", topic="decat", watchlists_on=True, filters_on=True
        ),
        SurveyTopic(
            name="elasticc_dev", topic="elasticc", watchlists_on=True, filters_on=True
        ),
    ]
    num_processed = process_kafka_messages(
        kafka_subscription=kafka_alert_queue,
        surveys_topics=surveys_topics,
        locus_repository=container.locus_repository(),
        tracer=container.tracer(),
        metrics=container.metrics(),
        lock_factory=container.lock_factory(),
        lock_ttl=10.0,
        limit=14,
        # Set the timeout value to higher if the next assertion fails
        timeout=10.0,
    )

    assert num_processed == 14

    loci = [
        container.locus_repository().create_locus_from_row(row)
        for row in container.locus_repository().locus_table.read_rows(
            filter_=CellsColumnLimitFilter(1)
        )
    ]
    assert len(loci) == 14
    alerts = [
        row
        for row in container.alert_repository().alert_table.read_rows(
            filter_=CellsColumnLimitFilter(1)
        )
    ]
    assert len(alerts) == 127

    # Check correct number of alerts were sent out on Kafka
    kafka_topics = kafka.KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    ).list_topic_names()

    assert "client.test_tag_dev" in kafka_topics
    # TODO: Add format assert
    assert len(list(drain_kafka_output(["client.test_tag_dev"]))) == 9

    # Check correct number of Loci exist in ES
    time.sleep(10)  # give enough time for the index worker to work
    assert len(list(drain_kafka_output(["system.locus_search_index_dev"]))) == 9
    search_index_documents = es_get_all_documents("loci_dev")
    assert len(search_index_documents) == 9

    # Check that catalog matches worked by verifying the expected matches
    for catalog_object in EXPECTED_CATALOG_MATCHES:
        matched = False
        for locus in loci:
            if locus.location.separation(catalog_object.location) < Angle("0.01d"):
                assert catalog_object.catalog_name in locus.catalogs
                matched = True
                break
        assert matched

    # TODO: more asserts
    # TODO: instead of calling process_kakfa_messages, boot up a pipeline worker and
    #       send the alerts into kafka
    # TODO: query the frontend
