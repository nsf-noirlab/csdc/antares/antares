import os
import test
import time
import uuid
from test.bootstrap import bootstrap
from typing import Generator

import pytest
from google.auth.credentials import AnonymousCredentials
from google.cloud import storage
from google.cloud.storage.bucket import Bucket

from antares.adapters.messages import (
    KafkaMessagePublicationService,
    KafkaMessageSubscriptionService,
)
from antares.entrypoints.stamp_extractor import process_stamp_kafka_messages
from antares.external import kafka
from antares.observability.logging.default import init_default_logging
from antares.settings import Container


@pytest.fixture
def container() -> Generator[Container, None, None]:
    container = bootstrap()
    yield container
    container.shutdown_resources()
    container.unwire()


@pytest.fixture
def kafka_alert_queue():
    survey_input = [
        {
            "topic": "ztf_process_alerts_test",
            "enabled": True,
            "directory": "data/alerts/ztf",  # Relative to antares/test
            "alerts": [
                "1069343660015010008.avro",
                "1069343660115010015.avro",
                "1069343660215015001.avro",
                "1069343661515015018.avro",
                "1069343660015010011.avro",
                "1069343660115015014.avro",
                "1069343660215015013.avro",
                "1069343661515015019.avro",
                "1069343660115010006.avro",
                "1069343660115015015.avro",
                "1069343661515015016.avro",
                "1069343660115010012.avro",
                "1069343660115015016.avro",
                "1069343661515015017.avro",
            ],
        },
    ]
    # TODO: use config
    admin = kafka.KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    )
    admin.delete_all_topics(force=True)
    time.sleep(10)
    message_publication_service = KafkaMessagePublicationService(
        {
            "bootstrap.servers": "kafka",
        }
    )
    for survey in (s for s in survey_input if s["enabled"]):
        for alert in survey["alerts"]:
            filename = os.path.join(
                os.path.dirname(os.path.abspath(test.__file__)),
                survey["directory"],
                alert,
            )
            with open(filename, "rb") as f:
                blob = f.read()
            message_publication_service.publish(survey["topic"], blob)
    yield KafkaMessageSubscriptionService(
        {
            "bootstrap.servers": "kafka",
            "group.id": str(uuid.uuid4()),
            "default.topic.config": {"auto.offset.reset": "earliest"},
            "enable.auto.commit": False,
            "max.poll.interval.ms": 1000 * 60 * 30,
        },
        [survey["topic"] for survey in survey_input],
    )
    admin = kafka.KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    )
    admin.delete_all_topics(force=True)


@pytest.fixture
def bucket() -> Generator[Bucket, None, None]:
    gcs_client = storage.Client(
        credentials=AnonymousCredentials(),
        project="test",
        client_options={"api_endpoint": "http://gcs:4443"},
    )
    bucket = gcs_client.bucket(f"test_bucket_{os.urandom(15).hex()}")
    if not bucket.exists():
        bucket.create()
    yield bucket


def test_process_stamp_extractor(kafka_alert_queue, container, bucket):
    init_default_logging("INFO")
    num_processed = process_stamp_kafka_messages(
        kafka_subscription=kafka_alert_queue,
        gcs_bucket=bucket,
        overwrite=True,
        metrics=container.metrics(),
        limit=14,
        # Set the timeout value to higher if the next assertion fails
        timeout=10.0,
    )
    assert num_processed == 14
    stamp_names = (
        "candid1069343660015010008_ref.fits.png",
        "candid1069343660015010008_pid1069343660015_targ_diff.fits.png",
        "candid1069343660115010015_pid1069343660115_targ_sci.fits.png",
        "candid1069343660115010015_ref.fits.png",
        "candid1069343660115010015_pid1069343660115_targ_diff.fits.png",
        "candid1069343660215015001_pid1069343660215_targ_sci.fits.png",
        "candid1069343660215015001_ref.fits.png",
        "candid1069343660215015001_pid1069343660215_targ_diff.fits.png",
        "candid1069343661515015018_pid1069343661515_targ_sci.fits.png",
        "candid1069343661515015018_ref.fits.png",
        "candid1069343661515015018_pid1069343661515_targ_diff.fits.png",
        "candid1069343660015010011_pid1069343660015_targ_sci.fits.png",
        "candid1069343660015010011_ref.fits.png",
        "candid1069343660015010011_pid1069343660015_targ_diff.fits.png",
        "candid1069343660115015014_pid1069343660115_targ_sci.fits.png",
        "candid1069343660115015014_ref.fits.png",
        "candid1069343660115015014_pid1069343660115_targ_diff.fits.png",
        "candid1069343660215015013_pid1069343660215_targ_sci.fits.png",
        "candid1069343660215015013_ref.fits.png",
        "candid1069343660215015013_pid1069343660215_targ_diff.fits.png",
        "candid1069343661515015019_pid1069343661515_targ_sci.fits.png",
        "candid1069343661515015019_ref.fits.png",
        "candid1069343661515015019_pid1069343661515_targ_diff.fits.png",
        "candid1069343660115010006_pid1069343660115_targ_sci.fits.png",
        "candid1069343660115010006_ref.fits.png",
        "candid1069343660115010006_pid1069343660115_targ_diff.fits.png",
        "candid1069343660115015015_pid1069343660115_targ_sci.fits.png",
        "candid1069343660115015015_ref.fits.png",
        "candid1069343660115015015_pid1069343660115_targ_diff.fits.png",
        "candid1069343661515015016_pid1069343661515_targ_sci.fits.png",
        "candid1069343661515015016_ref.fits.png",
        "candid1069343661515015016_pid1069343661515_targ_diff.fits.png",
        "candid1069343660115010012_pid1069343660115_targ_sci.fits.png",
        "candid1069343660115010012_ref.fits.png",
        "candid1069343660115010012_pid1069343660115_targ_diff.fits.png",
        "candid1069343660115015016_pid1069343660115_targ_sci.fits.png",
        "candid1069343660115015016_ref.fits.png",
        "candid1069343660115015016_pid1069343660115_targ_diff.fits.png",
        "candid1069343661515015017_pid1069343661515_targ_sci.fits.png",
        "candid1069343661515015017_ref.fits.png",
        "candid1069343661515015017_pid1069343661515_targ_diff.fits.png",
    )
    for stamp_name in stamp_names:
        assert bucket.get_blob(stamp_name) is not None
