import os
import test
import time
from test.bootstrap import bootstrap
from test.utils import clean_all_bigtable_tables
from typing import Generator

import pytest
import requests

import antares.exceptions
from antares.adapters.messages import (
    KafkaMessagePublicationService,
    KafkaMessageSubscriptionService,
)
from antares.external import kafka
from antares.external.sql.bootstrap import drop_main_db
from antares.settings import Container


@pytest.fixture
def alert_packets():
    survey_input = [
        {
            "topic": "ztf_dev",
            "directory": "data/alerts/ztf",  # Relative to antares/test
            "alerts": [
                "1069343660015010008.avro",
                "1069343660115010015.avro",
                "1069343660215015001.avro",
                "1069343661515015018.avro",
                "1069343660015010011.avro",
                "1069343660115015014.avro",
                "1069343660215015013.avro",
                "1069343661515015019.avro",
                "1069343660115010006.avro",
                "1069343660115015015.avro",
                "1069343661515015016.avro",
                "1069343660115010012.avro",
                "1069343660115015016.avro",
                "1069343661515015017.avro",
            ],
        },
    ]
    # TODO: use config
    admin = kafka.KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    )
    admin.delete_all_topics(force=True)
    alerts = []
    for survey in survey_input:
        for alert in survey["alerts"]:
            filename = os.path.join(
                os.path.dirname(os.path.abspath(test.__file__)),
                survey["directory"],
                alert,
            )
            with open(filename, "rb") as f:
                alerts.append(f.read())
    yield alerts
    admin = kafka.KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    )
    admin.delete_all_topics(force=True)


@pytest.fixture
def container() -> Generator[Container, None, None]:
    container = bootstrap()
    yield container
    clean_all_bigtable_tables(container)
    drop_main_db()
    container.shutdown_resources()
    container.unwire()


def test_filter_creation_and_approval(alert_packets: list):
    # Some preliminary setup for pushing/pulling messages from the input/output Kafka
    # TODO: use config
    bootstrap()
    time.sleep(10)
    producer = KafkaMessagePublicationService(
        {
            "bootstrap.servers": "kafka",
        }
    )
    consumer = KafkaMessageSubscriptionService(
        {
            "bootstrap.servers": "kafka",
            "group.id": "antares_dev",
            "default.topic.config": {"auto.offset.reset": "earliest"},
            "enable.auto.commit": False,
            "max.poll.interval.ms": 1000 * 60 * 30,
        },
        ["client.test_tag_dev"],
    )
    iter_alert_packets = iter(alert_packets)

    # 1) User submits a new filter:
    access_token = api_login("user", "password")
    filter_id = api_create_filter(access_token)
    filter_revision_id = api_create_filter_revision(access_token, filter_id)
    # 1a) Verify that if the system receives a new alert it is processed but doesn't use
    # the newly submitted filter (not reviewed/enabled).
    producer.publish("test_topic", next(iter_alert_packets))
    with pytest.raises(antares.exceptions.Timeout):
        consumer.poll(timeout=10)

    # The user can see their submitted filter version:
    response = api_get_filter_revision(access_token, filter_id, filter_revision_id)
    revision_attr = response.json()["data"]["attributes"]
    assert revision_attr["status"] == "PENDING_REVIEW"
    assert revision_attr["public"] is False
    assert revision_attr["code"] == TEST_FILTER_CODE

    # TODO
    # 2) Admin logs-in and reviews/enables the newly submitted filter.
    # access_token = api_login("admin", "password")
    # api_review_filter_revision(access_token, filter_id, filter_revision_id)
    # api_enable_filter_revision(access_token, filter_id, filter_revision_id)
    # 1a) Verify that once the filter is enabled, it will run against the next alert we
    # receive.
    # producer.publish("test_topic", next(iter_alert_packets))
    # topic, message, _ = consumer.poll(timeout=10)
    # consumer.commit()
    # assert topic == "client.test_tag_dev"
    # assert message

    # System receives and creates in DB, doesn't do anything with it until...
    #
    # ... and also to enable the filter
    # Check that:
    #     - New rows created in locus_property table if necessary
    #     - New rows created in tags table if necessary
    #     - New mapping fields pushed into Elasticsearch if necessary
    #     - Filter set as enabled version
    #
    # Pipeline receives new alerts
    #
    # The pipeline should process them with the new filter and, if applicable, update
    # the locus with new properties, tags
    #
    # New fields should get pushed into the ES search index
    pass


TEST_FILTER_CODE = """
import antares.devkit as dk
from antares.devkit import Filter

class TestFilter(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "test_property",
            "type": "int",
            "description": "",
        },
    ]
    OUTPUT_TAGS = [
        {
            "name": "test_tag_dev",
            "description": "",
        },
    ]
    def run(self, locus):
        locus.properties["test_property"] = 100
        locus.tag("test_tag_dev")
"""


def api_login(username: str, password: str) -> str:
    response = requests.post(
        "http://api:8000/v1/auth/login",
        json={
            "username": username,
            "password": password,
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json())
    assert response.json().get("access_token")
    return response.json()["access_token"]


def api_create_filter(access_token: str) -> int:
    # TODO: Remove the magic `3`, comes from `antares/test/bootstrap.py` user w/ username "user"
    response = requests.post(
        "http://api:8000/v1/users/3/filters",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "E2E Filter Workflow",
                    "public": False,
                },
            }
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json())
    return response.json()["data"]["id"]


def api_create_filter_revision(access_token: str, filter_id: int) -> int:
    response = requests.post(
        f"http://api:8000/v1/filters/{filter_id}/versions",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "data": {
                "type": "filter_version",
                "attributes": {
                    "comment": "Test Filter Version",
                    "code": TEST_FILTER_CODE,  # TODO
                },
                "relationships": {
                    "filter": {
                        "data": {"type": "filter", "id": filter_id},
                    },
                },
            },
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json())
    return response.json()["data"]["id"]


def api_get_filter_revision(
    access_token: str, filter_id: int, filter_revision_id: int
) -> requests.models.Response:
    return requests.get(
        f"http://api:8000/v1/filters/{filter_id}/versions/{filter_revision_id}",
        headers={"Authorization": f"Bearer {access_token}"},
    )


def api_review_filter_revision(
    access_token: str, filter_id: int, filter_revision_id: int
):
    print(filter_id, "FILTER ID")
    print(filter_revision_id, "FILTER REVISION ID")
    response = requests.patch(
        f"http://api:8000/v1/filters/{filter_id}/versions/{filter_revision_id}",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "data": {
                "id": filter_revision_id,
                "type": "filter_version",
                "attributes": {
                    "status": "REVIEWED",
                },
            },
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json())


def api_enable_filter_revision(
    access_token: str, filter_id: int, filter_revision_id: int
):
    response = requests.patch(
        f"http://api:8000/v1/filters/{filter_id}",
        headers={"Authorization": f"Bearer {access_token}"},
        json={
            "data": {
                "id": filter_id,
                "type": "filter",
                "relationships": {
                    "enabled_version": {
                        "data": {"type": "filter_version", "id": filter_revision_id},
                    },
                },
            },
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json())
