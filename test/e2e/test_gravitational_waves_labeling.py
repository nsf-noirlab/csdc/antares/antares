import io
import json
import logging
import time
import uuid
from datetime import datetime, timedelta
from random import randint
from test.bootstrap import bootstrap
from test.fakes import build_filter, build_filter_revision
from test.utils import clean_all_bigtable_tables
from typing import Generator

import fastavro
import pytest
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from pytest import approx

from antares.external.elasticsearch.ingest import connect as es_connect
from antares.external.kafka import KafkaAdmin, KafkaConsumer, KafkaProducer
from antares.external.sql.bootstrap import drop_main_db
from antares.settings import Container
from antares.utils import dt_to_mjd, mjd_to_jd

GRAV_WAVE_ALERT_TOPIC = "grav_wave_notices_dev"
RAW_GW_MATCH_TOPIC = "raw_gw_match_topic_dev"


@pytest.fixture
def container() -> Generator[Container, None, None]:
    container = bootstrap()
    yield container
    clean_all_bigtable_tables(container)
    drop_main_db()
    container.shutdown_resources()
    container.unwire()


@pytest.fixture
def kafka_producer_and_consumer(container):
    admin = KafkaAdmin(
        {
            "bootstrap.servers": "kafka",
        }
    )
    admin.delete_all_topics(force=True)
    # All the below could be handled by `delete_consumer_groups`
    # which is part of newer confluent-kafka
    grav_wave_watcher_consumer_config = (
        container.config.grav_wave_watcher.broker.settings()
    )
    grav_wave_watcher_consumer = KafkaConsumer(grav_wave_watcher_consumer_config)
    grav_wave_watcher_consumer.reset_offsets()
    index_worker_consumer_config = container.config.archive.ingestion.broker.settings()
    index_worker_consumer = KafkaConsumer(index_worker_consumer_config)
    index_worker_consumer.reset_offsets()
    pipeline_consumer_config = container.config.pipeline.ingestion.broker.settings()
    pipeline_consumer = KafkaConsumer(pipeline_consumer_config)
    pipeline_consumer.reset_offsets()
    producer_config = {
        "bootstrap.servers": "kafka",
        "message.max.bytes": 5000000,
    }  # https://github.com/confluentinc/confluent-kafka-python/issues/325
    kafka_producer = KafkaProducer(producer_config)
    consumer_config = {
        "bootstrap.servers": "kafka",
        "group.id": str(uuid.uuid4()),
        "auto.offset.reset": "earliest",
    }

    kafka_consumer = KafkaConsumer(consumer_config)
    yield kafka_producer, kafka_consumer
    admin.delete_all_topics(force=True)


def test_e2e_gravitational_wave_alerts(container, kafka_producer_and_consumer):
    # SetUp

    elasticsearch_client = es_connect()
    es_loci_index = container.config.archive.ingestion.storage.parameters.index()
    kafka_producer, kafka_consumer = kafka_producer_and_consumer

    # Get Kafka Topic Names
    surveys = container.config.pipeline.ingestion.broker.surveys()
    ztf_survey = [survey for survey in surveys if survey["name"] == "ztf"][0]
    ztf_survey_topic = ztf_survey["topic"]
    grav_wave_alert_topic = container.config.grav_wave_watcher.broker.topic()
    es_kafka_topic = container.config.archive.ingestion.broker.topic()

    kafka_consumer.subscribe([grav_wave_alert_topic, ztf_survey_topic, es_kafka_topic])
    kafka_consumer._consumer.poll(timeout=1.0)  # connect to topic
    logging.info("Kafka Connection Complete")

    # A GW Notice is loaded in the kafka topic the gravwavewatcher is watching
    with open("test/data/e2e/grav_wave/MS181101ab-earlywarning.json", "rt") as f:
        raw_notice = json.load(f)
        raw_notice["time_created"] = (datetime.utcnow() - timedelta(days=4)).isoformat(
            " "
        )
        raw_notice["event"]["time"] = (datetime.utcnow() - timedelta(days=3)).isoformat(
            " "
        )
        kafka_producer.produce(grav_wave_alert_topic, json.dumps(raw_notice))
        kafka_producer.flush()
        kafka_producer.produce(grav_wave_alert_topic, json.dumps(raw_notice))
        kafka_producer.flush()

    time.sleep(5)

    # temp test to make sure we're loading the alert into kafka correctly
    _, notice_message = kafka_consumer.poll(timeout=3.0)
    notice_json = notice_message.value()
    notice = json.loads(notice_json)
    assert notice["superevent_id"] == "MS181101ab"

    # The gravwave watcher pulls the new alert and loads it into the grav wave repo
    grav_wave_repo = container.grav_wave_repository()

    notice = grav_wave_repo.get(1)
    counter = 0
    while notice is None and counter < 10:
        notice = grav_wave_repo.get(1)
        counter += 1
        time.sleep(3)
    assert notice.gracedb_id == "MS181101ab"

    notices = grav_wave_repo.get_current_notices()

    assert len(notices) == 1

    notice = notices[0]

    # A new alert located in the confidence region is loaded into
    # the kafka topic the pipeline worker is watching

    with open("test/data/alerts/ztf/1069343660015010011.avro", "rb") as f:
        blob = f.read()

    buffer = io.BytesIO(blob)
    buffer.seek(0)

    # deserialize blob
    reader = fastavro.reader(buffer)
    writer_schema = fastavro.parse_schema(reader.writer_schema)
    message = next(reader)

    # set ID (so that the alert is written without having to wipe the DB)
    candid_id = randint(1069343660015010011, 9069343660015010011)
    message["candidate"]["candid"] = candid_id
    alert_id = f"ztf_candidate:{message['candidate']['candid']}"

    # # # Update alert for test # # #

    # update timestamp
    alert_datetime = datetime.utcnow()
    alert_mjd = dt_to_mjd(alert_datetime)
    message["candidate"]["jd"] = mjd_to_jd(alert_mjd)
    # remove prv candidates
    message["prv_candidates"] = []
    message["candidate"]["ra"] = 193.30
    message["candidate"]["dec"] = -17.85
    updated_blob = io.BytesIO()
    fastavro.writer(updated_blob, writer_schema, [message])
    updated_blob.seek(0)
    updated_message = updated_blob.read()

    kafka_producer.produce(ztf_survey_topic, updated_message)
    kafka_producer.flush()

    time.sleep(5)

    # temp check to see that the alert and locus are loaded...
    alert_repo = container.alert_repository()
    locus_repo = container.locus_repository()

    location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
    radius = Angle(1, unit=u.arcsec)
    alert = None
    counter = 0
    while alert is None and counter < 10:
        alert = alert_repo.get(alert_id)
        counter += 1
        time.sleep(3)
    assert alert.id == alert_id
    assert alert.mjd == approx(alert_mjd)

    loci = list(locus_repo.list_by_cone_search(location, radius))
    assert len(loci) == 1
    locus = loci[0]
    alerts = alert_repo.list_by_locus_id(locus.id)
    assert alert in alerts

    # The pipeline worker puts the alert/locus info into the
    # 'raw Grav Wave match' kafka topic
    # # # TODO # # #

    # The pipeline adds a grav wave label of some sort to the alert and locus
    assert alert.grav_wave_events
    assert len(alert.grav_wave_events) == 1
    assert locus.grav_wave_events
    assert len(locus.grav_wave_events) == 1

    # The index worker has updated the elasticsearch index
    # and the grav wave id is searchable
    query_body = {"query": {"match": {"grav_wave_events": locus.grav_wave_events[0]}}}
    es_results = elasticsearch_client.search(index=es_loci_index, body=query_body)
    es_grav_waves = es_results["hits"]["hits"][0]["_source"]["grav_wave_events"]
    assert es_grav_waves == locus.grav_wave_events

    # A user filter that requires a grav wave event is created and enabled
    MATCH_FILTER = """
import antares.devkit as dk

class TestFilter(dk.Filter):

    REQUIRED_GRAV_WAVE_PROB_REGION = 99
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "match_grav_area",
            "type": "int",
            "description": "",
        },
        {
            "name": "match_grav_level",
            "type": "int",
            "description": "",
        },
        {
            "name": "BNS_class",
            "type": "float",
            "description": "",
        },
    ]
    OUTPUT_TAGS = [
        {
            "name": "match_grav_test_tag",
            "description": "",
        },
    ]
    def run(self, locus):
        alert = locus.alert
        gw_alert_event = alert.grav_wave_events[0]
        gw_event = locus.grav_wave_events_metadata[gw_alert_event["gracedb_id"]]
        locus.properties["match_grav_level"] = gw_alert_event["contour_level"]
        locus.properties["match_grav_area"] = gw_alert_event["contour_area"]
        locus.properties["BNS_class"] = gw_event.full_notice["event"]["classification"]["BNS"]
        locus.tag("match_grav_test_tag")
    """
    NONMATCH_FILTER = """
import antares.devkit as dk

class TestFilter(dk.Filter):

    REQUIRED_GRAV_WAVE_PROB_REGION = 5
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "nonmatch_grav_test_prop",
            "type": "int",
            "description": "",
        },
    ]
    OUTPUT_TAGS = [
        {
            "name": "nonmatch_grav_test_tag",
            "description": "",
        },
    ]
    def run(self, locus):
        locus.properties["nonmatch_grav_test_prop"] = 102
        locus.tag("nonmatch_grav_test_tag")
    """

    # TODO: This should be abstracted
    filter_repo = container.filter_repository()
    filter_revision_repo = container.filter_revision_repository()
    filter_match = build_filter(id=100, name="Matching E2E Grav Wave Filter")
    filter_revision = build_filter_revision(filter_match, id=100, code=MATCH_FILTER)
    filter_revision.enable(filter_=filter_match)
    filter_repo.add(filter_match)
    filter_revision_repo.add(filter_revision)
    filter_nonmatch = build_filter(id=101, name="NonMatching E2E Grav Wave Filter")
    filter_revision_2 = build_filter_revision(
        filter_nonmatch, id=101, code=NONMATCH_FILTER
    )
    filter_revision_2.enable(filter_=filter_nonmatch)
    filter_repo.add(filter_nonmatch)
    filter_revision_repo.add(filter_revision_2)

    # A new alert located in the confidence region is loaded into
    # the kafka topic the pipeline worker is watching

    second_alert_message, alert_id, alert_mjd = kafka_ready_alert()

    kafka_producer.produce(ztf_survey_topic, second_alert_message)
    kafka_producer.flush()

    time.sleep(5)

    # The filter should have tagged the locus
    loci = list(locus_repo.list_by_cone_search(location, radius))
    assert len(loci) == 1
    locus = loci[0]
    assert "match_grav_test_tag" in locus.tags
    # The non-matching filter shouldn't have run
    assert "nonmatch_grav_test_tag" not in locus.tags
    assert "nonmatch_grav_test_prop" not in locus.properties

    # The matching filter should have added a property pulled from the
    # alert's grav wave event
    assert "match_grav_area" in locus.properties
    assert "match_grav_level" in locus.properties
    assert locus.properties["match_grav_level"] == approx(76.626, 0.0001)
    assert locus.properties["match_grav_area"] == approx(18.851, 0.0001)

    # The matching filter should have added a property pulled from the
    # full grav wave event metadata
    assert locus.properties["BNS_class"] == 0.95

    # A new alert outside the confidence region is loaded into
    # the kafka topic the pipeline worker is watching

    # The new alert is not added to the 'raw grav wave match' kafka topic

    # The new alert is not labelled with the grav wave label

    # An 'update' grav wave alert is loaded in the kafka topic
    # the grave wave watcher is watching

    # The updated grav wave alert is loaded into the grav wave alert repo

    # A new alert in the original confidence region,
    # but outside the updated confidence region

    # is loaded into kafka and ingested by the pipeline worker

    # The new alert isn't emitted into the raw grav wave alert topic,
    # and isn't labelled

    # A new alert in the updated confidence region is ingested
    # by the pipeline worker

    # It is emitted in the raw grav wave topic, and labelled.

    # A retraction grav wave alert is loaded in the grav wave topic

    # The grav wave watcher loads the retraction into the grav wave repo

    # A new alert inside the updated confidence region is loaded into kafka
    # and read by the pipeline

    # The new alert isn't emitted in the raw stream, nor labelled

    # A fresh grav wave alert arrives in kafka

    # A new alert in the confidence region is ingested,
    # emitted in the raw steam, and labelled.

    # Time goes by (how long?)

    # A new alert in the confidence region is ingested

    # It isn't emitted in the raw stream, nor labelled.


def kafka_ready_alert():
    with open("test/data/alerts/ztf/1069343660015010011.avro", "rb") as f:
        blob = f.read()

    buffer = io.BytesIO(blob)
    buffer.seek(0)

    # deserialize blob
    reader = fastavro.reader(buffer)
    writer_schema = fastavro.parse_schema(reader.writer_schema)
    message = next(reader)

    # set ID (so that the alert is written without having to wipe the DB)
    candid_id = randint(1069343660015010011, 9069343660015010011)
    message["candidate"]["candid"] = candid_id
    alert_id = f"ztf_candidate:{message['candidate']['candid']}"

    # # # Update alert for test # # #

    # update timestamp
    alert_datetime = datetime.utcnow()
    alert_mjd = dt_to_mjd(alert_datetime)
    message["candidate"]["jd"] = mjd_to_jd(alert_mjd)
    # remove prv candidates
    message["prv_candidates"] = []
    message["candidate"]["ra"] = 193.30
    message["candidate"]["dec"] = -17.85
    updated_blob = io.BytesIO()
    fastavro.writer(updated_blob, writer_schema, [message])
    updated_blob.seek(0)
    updated_message = updated_blob.read()
    return updated_message, alert_id, alert_mjd
