from test.bootstrap import bootstrap

from antares.entrypoints.pipeline import kafka_listener


def e2e_pipeline_init():
    bootstrap()
    kafka_listener.main()


if __name__ == "__main__":
    e2e_pipeline_init()
