import json
import logging
import os
import uuid

import sqlalchemy_utils
from google.cloud import bigtable
from sqlalchemy.orm import sessionmaker

import antares.bootstrap as real_bootstrap
import antares.exceptions
import antares.external.elasticsearch
from antares.adapters.repository import (
    SqlAlchemyFilterRepository,
    SqlAlchemyUserRepository,
)
from antares.adapters.repository.bigtable.schemas import SCHEMAS as BIGTABLE_SCHEMAS
from antares.adapters.repository.sqlalchemy import metadata
from antares.domain.models import Filter, User
from antares.external.bigtable.bigtable import (
    replace_emulator_channel_fn,
    wait_for_bigtable,
)
from antares.external.elasticsearch.ingest import create_index, delete_index
from antares.external.sql.bootstrap import drop_main_db
from antares.external.sql.engine import get_engine

# ======================================================================================
# Models for populating the databases
# ======================================================================================

users = [
    User(
        id=uuid.UUID(int=1),
        name="admin",
        username="admin",
        email="admin@noirlab.edu",
        admin=True,
    ),
    User(
        id=uuid.UUID(int=2),
        name="staff",
        username="staff",
        email="staff@noirlab.edu",
        staff=True,
    ),
    User(id=uuid.UUID(int=3), name="user", username="user", email="user@noirlab.edu"),
]
for user in users:
    user.set_password("password")

filters = [
    Filter(
        id=1,
        description="A test filter",
        name="Test Filter",
        public=True,
        owner_id=uuid.UUID(int=1),
    )
]


# ======================================================================================
# Bootstrap Code
# ======================================================================================
def bootstrap_elasticsearch(container):
    delete_index(container.config.archive.ingestion.storage.parameters.index())
    mapping_path = os.path.join(
        os.path.dirname(antares.external.elasticsearch.__file__), "mapping.json"
    )
    with open(mapping_path) as f:
        mapping = json.load(f)
    create_index(
        container.config.archive.ingestion.storage.parameters.index(),
        mappings=mapping["mappings"],
    )


def generate_container_config_dict(datastore: str):
    return {
        "repositories": {
            "locus": {"datastore": datastore},
            "alert": {"datastore": datastore},
            "watch_list": {"datastore": datastore},
            "watch_object": {"datastore": datastore},
            "catalog": {"datastore": datastore},
        }
    }


def bootstrap_bigtable(container):
    wait_for_bigtable(
        project_id=container.config.repositories.locus.bigtable.parameters.project_id(),
        instance_id=container.config.repositories.locus.bigtable.parameters.instance_id(),
    )

    client = bigtable.Client(
        project=container.config.external.bigtable.parameters.project_id(), admin=True
    )
    if os.getenv("BIGTABLE_EMULATOR_HOST"):
        # see https://github.com/GoogleCloudPlatform/cloud-sdk-docker/issues/253#issuecomment-972247899
        # for details on why we do this
        client._emulator_channel = replace_emulator_channel_fn
    instance = client.instance(
        container.config.external.bigtable.parameters.instance_id()
    )
    bigtables_schemas_cfg = container.config.external.bigtable.schemas()
    for schema in BIGTABLE_SCHEMAS:
        schema = schema(bigtables_schemas_cfg)
        table = instance.table(schema.__table_name__)
        if not table.exists():
            logging.info(
                "Creating Bigtable table %s, column families=%s",
                schema.__table_name__,
                schema.column_families,
            )
            table.create(column_families=schema.column_families)
    logging.info("bootstrap_bigtable complete")


def bootstrap_mysql():
    logging.info("Bootstrapping mysql")
    drop_main_db()
    engine = get_engine()
    sqlalchemy_utils.create_database(engine.url)
    metadata.create_all(bind=engine)
    session_factory = sessionmaker(engine)
    filter_repository = SqlAlchemyFilterRepository(session_factory)
    user_repository = SqlAlchemyUserRepository(session_factory)
    for user_ in users:
        user_repository.add(user_)
    for filter_ in filters:
        filter_repository.add(filter_)


def bootstrap():
    logging.info("Bootstrapping services")
    container = real_bootstrap.bootstrap()
    bootstrap_bigtable(container)
    bootstrap_mysql()
    bootstrap_elasticsearch(container)
    return container


def main():
    bootstrap()


if __name__ == "__main__":
    main()
