import datetime
import logging
import uuid
from unittest.mock import patch

import jwt
import pytest
import requests
from flask_jwt_extended.exceptions import NoAuthorizationError

from antares.domain.models import User


def build_user():
    user = User(
        id=uuid.UUID(int=123), name="name", username="user", email="user@noirlab.edu"
    )
    user.set_password("password")
    return user


@pytest.mark.with_data(build_user())
def test_reset_password_resets_password(api_client):
    response = api_client.post(
        "/v1/auth/login-fresh",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "access_token" in response.json
    access_token = response.json["access_token"]
    response = api_client.post(
        "/v1/auth/reset",
        headers={"Authorization": f"Bearer {access_token}"},
        json={"password": "NeWpaSs"},
    )
    assert response.status_code == 200
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "NeWpaSs",
        },
    )
    assert "access_token" in response.json
    assert "refresh_token" in response.json
    assert response.status_code == 200


@pytest.mark.with_data(build_user())
def test_reset_password_requires_fresh_token(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "access_token" in response.json
    access_token = response.json["access_token"]
    response = api_client.post(
        "/v1/auth/reset",
        headers={"Authorization": f"Bearer {access_token}"},
        json={"password": "NeWpaSs"},
    )
    assert response.status_code == 401
    # Login w/ fresh token
    response = api_client.post(
        "/v1/auth/login-fresh",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "access_token" in response.json
    access_token = response.json["access_token"]
    response = api_client.post(
        "/v1/auth/reset",
        headers={"Authorization": f"Bearer {access_token}"},
        json={"password": "NeWpaSs"},
    )
    assert response.status_code == 200


@pytest.mark.with_data(build_user())
def test_login_fresh_returns_fresh_token(api_client):
    # Login w/ fresh token
    response = api_client.post(
        "/v1/auth/login-fresh",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "access_token" in response.json
    access_token = response.json["access_token"]
    decoded_access_token = jwt.decode(
        access_token, options={"verify_signature": False}, algorithms=["HS256"]
    )
    assert decoded_access_token["fresh"]


@pytest.mark.with_data(build_user())
def test_login_fresh_does_not_return_refresh_token(api_client):
    response = api_client.post(
        "/v1/auth/login-fresh",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "refresh_token" not in response.json


@pytest.mark.with_data(build_user())
def test_login_returns_stale_token(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "access_token" in response.json
    access_token = response.json["access_token"]
    decoded_access_token = jwt.decode(
        access_token, options={"verify_signature": False}, algorithms=["HS256"]
    )
    assert not decoded_access_token["fresh"]


@pytest.mark.with_data(build_user())
def test_cookie_based_token_refresh_works(api_client):
    # Login
    response = api_client.post(
        "/v1/auth/login",
        query_string={"type": "cookie"},
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    cookies = requests.utils.dict_from_cookiejar(api_client.cookie_jar)
    response = api_client.post(
        "/v1/auth/refresh",
        query_string={"type": "cookie"},
        headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]},
    )
    try:
        assert response.status_code == 204
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(build_user())
def test_login_sets_access_and_refresh_cookies(api_client):
    # Login
    response = api_client.post(
        "/v1/auth/login",
        query_string={"type": "cookie"},
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "access_token_cookie" in [cookie.name for cookie in api_client.cookie_jar]
    assert "csrf_access_token" in [cookie.name for cookie in api_client.cookie_jar]
    assert "refresh_token_cookie" in [cookie.name for cookie in api_client.cookie_jar]
    assert "csrf_refresh_token" in [cookie.name for cookie in api_client.cookie_jar]
    for cookie in api_client.cookie_jar:
        if cookie.name in ["access_token_cookie", "refresh_token_cookie"]:
            assert "HttpOnly" in cookie.__dict__["_rest"]


@pytest.mark.with_data(build_user())
def test_logout_adds_refresh_token_to_blocklist(api_client, container):
    # Login
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "refresh_token" in response.json
    # Logout
    refresh_token = response.json["refresh_token"]
    response = api_client.post(
        "/v1/auth/logout", headers={"Authorization": f"Bearer {refresh_token}"}
    )
    assert response.status_code == 204
    decoded_refresh_token = jwt.decode(
        refresh_token, options={"verify_signature": False}, algorithms=["HS256"]
    )
    blocklist_token = container.jwt_blocklist_repository().get_by_jti(
        decoded_refresh_token["jti"]
    )
    assert blocklist_token is not None
    assert blocklist_token.token_type == "refresh"
    assert blocklist_token.expires == datetime.datetime.fromtimestamp(
        decoded_refresh_token["exp"]
    )


@pytest.mark.with_data(build_user())
def test_success_response_from_logout_clears_cookies(api_client):
    # Login
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
        query_string={"type": "cookie"},
    )
    assert response.status_code == 200
    cookies = requests.utils.dict_from_cookiejar(api_client.cookie_jar)
    assert "access_token_cookie" in cookies
    assert "refresh_token_cookie" in cookies
    assert "csrf_access_token" in cookies
    assert "csrf_refresh_token" in cookies
    # Logout
    response = api_client.post(
        "/v1/auth/logout", headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]}
    )
    cookies = requests.utils.dict_from_cookiejar(api_client.cookie_jar)
    assert response.status_code == 204
    assert "access_token_cookie" not in cookies
    assert "refresh_token_cookie" not in cookies
    assert "csrf_access_token" not in cookies
    assert "csrf_refresh_token" not in cookies


@patch("antares.entrypoints.http_api.resources.auth.views.verify_jwt_in_request")
@pytest.mark.with_data(build_user())
def test_401_error_response_from_logout_clears_cookies(mock_verify, api_client):
    # Login
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
        query_string={"type": "cookie"},
    )
    assert response.status_code == 200
    cookies = requests.utils.dict_from_cookiejar(api_client.cookie_jar)
    assert "access_token_cookie" in cookies
    assert "refresh_token_cookie" in cookies
    assert "csrf_access_token" in cookies
    assert "csrf_refresh_token" in cookies
    # Logout
    mock_verify.side_effect = NoAuthorizationError("Token is expired")
    response = api_client.post(
        "/v1/auth/logout", headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]}
    )
    mock_verify.assert_called_with(refresh=True)
    cookies = requests.utils.dict_from_cookiejar(api_client.cookie_jar)
    assert response.status_code == 401
    assert "access_token_cookie" not in cookies
    assert "refresh_token_cookie" not in cookies
    assert "csrf_access_token" not in cookies
    assert "csrf_refresh_token" not in cookies


@patch("antares.entrypoints.http_api.resources.auth.views.verify_jwt_in_request")
@pytest.mark.with_data(build_user())
def test_500_error_response_from_logout_clears_cookies(mock_verify, api_client, caplog):
    # Login
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
        query_string={"type": "cookie"},
    )
    assert response.status_code == 200
    cookies = requests.utils.dict_from_cookiejar(api_client.cookie_jar)
    assert "access_token_cookie" in cookies
    assert "refresh_token_cookie" in cookies
    assert "csrf_access_token" in cookies
    assert "csrf_refresh_token" in cookies
    # Logout
    mock_verify.side_effect = Exception("Some problem")
    with caplog.at_level(logging.ERROR):
        response = api_client.post(
            "/v1/auth/logout", headers={"X-CSRF-TOKEN": cookies["csrf_refresh_token"]}
        )
        mock_verify.assert_called_with(refresh=True)
        cookies = requests.utils.dict_from_cookiejar(api_client.cookie_jar)
        assert response.status_code == 500
        assert "access_token_cookie" not in cookies
        assert "refresh_token_cookie" not in cookies
        assert "csrf_access_token" not in cookies
        assert "csrf_refresh_token" not in cookies
        assert "Exception: Some problem" in caplog.text


@pytest.mark.with_data(build_user())
def test_cant_get_new_tokens_with_blocklisted_refresh_token(api_client):
    # Login
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "refresh_token" in response.json
    # Logout
    refresh_token = response.json["refresh_token"]
    response = api_client.post(
        "/v1/auth/logout", headers={"Authorization": f"Bearer {refresh_token}"}
    )
    assert response.status_code == 204
    # Try to get fresh tokens with the blocklisted refresh token
    response = api_client.post(
        "/v1/auth/refresh", headers={"Authorization": f"Bearer {refresh_token}"}
    )
    assert 401 == response.status_code
    assert "revoked" in response.json["errors"][0]["detail"].lower()


@pytest.mark.with_data(build_user())
def test_login_returns_refresh_token(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "refresh_token" in response.json


@pytest.mark.with_data(build_user())
def test_refresh_token_can_be_used_for_new_access_token_and_refresh_token(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    refresh_token = response.json["refresh_token"]
    response = api_client.post(
        "/v1/auth/refresh", headers={"Authorization": f"Bearer {refresh_token}"}
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    assert "access_token" in response.json
    assert "refresh_token" in response.json
    assert "bearer" == response.json["token_type"]


@pytest.mark.with_data(build_user())
def test_access_token_cannot_be_used_for_new_access_token_and_refresh_token(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    access_token = response.json["access_token"]
    response = api_client.post(
        "/v1/auth/refresh", headers={"Authorization": f"Bearer {access_token}"}
    )
    assert response.status_code == 422
    assert "only refresh" in response.json["errors"][0]["detail"].lower()


@pytest.mark.with_data(build_user())
def test_user_with_correct_password_gets_authorization(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "password",
        },
    )
    assert response.status_code == 200
    assert "access_token" in response.json
    assert "refresh_token" in response.json
    assert "bearer" == response.json["token_type"]


def test_login_with_non_existent_username_responds_with_401(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "IDontExist",
            "password": "DoesntMatter",
        },
    )
    assert response.status_code == 401
    assert response.json["error"] == "invalid_client"


@pytest.mark.with_data(build_user())
def test_login_with_incorrect_password_responds_with_401(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "username": "user",
            "password": "WrongPassword",
        },
    )
    assert response.status_code == 401
    assert response.json["error"] == "invalid_client"


def test_login_with_missing_parameters_raises(api_client):
    response = api_client.post(
        "/v1/auth/login",
        json={
            "password": "NoUsername!",
        },
    )
    assert response.status_code == 400
    assert response.json["error, ValidationError"] == "invalid_request"
