from test.fakes import build_notice

import pytest


class TestApiGravWaveNotices:
    def test_grav_wave_notices(self, api_client, grav_wave_repository):
        notice = build_notice(superevent_id="MS181101at1")
        grav_wave_repository.add(notice)
        response = api_client.get(
            f"/v1/grav_wave_notices/{notice.gracedb_id}/{notice.notice_datetime.isoformat()}",
        )
        assert response.status_code == 200
        assert response.json["data"]["attributes"]["gracedb_id"] == "MS181101at1"

    def test_grav_wave_notices_not_found(self, api_client):
        response = api_client.get(
            "/v1/grav_wave_notices/MS181101at1/2023-12-01T14:00:11.678884",
        )
        assert response.status_code == 404
        assert (
            response.json["errors"][0]["detail"]
            == "No gravitational wave found with gracedb_id MS181101at1 and notice_datetime 2023-12-01 14:00:11.678884"
        )


class TestApiLatestGravWaveNotices:
    @pytest.mark.with_data(
        build_notice(
            superevent_id="MS181101at2",
        )
    )
    def test_grav_wave_notices_latest(self, api_client):
        response = api_client.get(
            "/v1/grav_wave_notices/MS181101at2/latest",
        )
        assert response.status_code == 200
        assert response.json["data"]["attributes"]["gracedb_id"] == "MS181101at2"

    def test_grav_wave_notices_latest_not_found(self, api_client):
        response = api_client.get(
            "/v1/grav_wave_notices/MS181101at2/latest",
        )
        assert response.status_code == 404
        assert (
            response.json["errors"][0]["detail"]
            == "No gravitational wave found with gracedb_id MS181101at2"
        )


class TestLatestGravWaveNoticeList:
    @pytest.mark.with_data(
        build_notice(
            superevent_id="MS181101at1",
        ),
        build_notice(
            superevent_id="MS181101at2",
        ),
        build_notice(
            superevent_id="MS181101at3",
        ),
    )
    def test_get_grav_wave_notices_returns_data(self, api_client):
        response = api_client.get(
            "/v1/grav_wave_notices/latest?ids=MS181101at1,MS181101at2",
        )
        assert response.status_code == 200
        for notice in response.json["data"]:
            gracedb_id = notice["attributes"]["gracedb_id"]
            assert gracedb_id in ("MS181101at1", "MS181101at2")
            assert gracedb_id != "MS181101at3"

    def test_get_grav_wave_notices_returns_nothing(self, api_client):
        response = api_client.get(
            "/v1/grav_wave_notices/latest?ids=ids_that_doesnt_exist",
        )
        assert response.status_code == 200
        assert response.json["data"] == []

    @pytest.mark.parametrize(
        "endpoint_url",
        [
            "/v1/grav_wave_notices/latest",
            "/v1/grav_wave_notices/latest?ids",
            "/v1/grav_wave_notices/latest?ids=",
        ],
    )
    def test_get_grav_wave_notices_returns_error_when_query_has_no_ids(
        self, endpoint_url, api_client
    ):
        response = api_client.get(endpoint_url)
        assert response.status_code == 400
        error = response.json["errors"][0]
        assert error["title"] == "Bad Request"
        assert error["status"] == 400
        assert (
            error["detail"]
            == "This endpoint receives comma separated ids in the query. Example: <url>?ids=id1,id2,id3"
        )
