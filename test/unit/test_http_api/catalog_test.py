import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import Catalog, CatalogObject


@pytest.mark.with_data(Catalog(id=1, name="test_catalog"))
def test_user_can_get_catalog_responds_200(api_client):
    response = api_client.get("/v1/catalogs/1")
    assert response.status_code == 200
    assert response.json["data"]["id"] == "1"
    assert response.json["data"]["attributes"]["name"] == "test_catalog"


def test_user_can_get_catalog_responds_404_if_not_found(api_client):
    response = api_client.get("/v1/catalogs/999")
    assert response.status_code == 404


def test_delete_method_not_allowed_responds_405(api_client):
    response = api_client.delete("/v1/catalogs/1")
    assert response.status_code == 405


def test_patch_method_not_allowed_responds_405(api_client):
    response = api_client.patch("/v1/catalogs/1")
    assert response.status_code == 405


@pytest.mark.with_data(Catalog(id="1", name="cat1"), Catalog(id="2", name="cat2"))
def test_user_can_get_catalogs_responds_200(api_client):
    response = api_client.get("/v1/catalogs")
    assert response.status_code == 200
    catalogs = response.json["data"]
    assert catalogs is not None
    assert len(catalogs) == 2


def test_user_can_get_catalog_responds_empty_if_no_catalogs(api_client):
    response = api_client.get("/v1/catalogs")
    assert len(response.json["data"]) == 0


def test_post_method_not_allowed_responds_405(api_client):
    response = api_client.post("/v1/catalogs")
    assert response.status_code == 405


@pytest.mark.with_data(
    Catalog(id="1", name="Test Catalog 1"),
    Catalog(id="2", name="Test Catalog 2"),
    *(
        CatalogObject(
            id=f"1_{i}",
            catalog_id="1",
            catalog_name="Test Catalog 1",
            location=SkyCoord("0d 0d"),
            radius=Angle("1s"),
            name=f"Test Catalog Object 1_{i}",
            properties={},
        )
        for i in range(10)
    ),
    *(
        CatalogObject(
            id=f"2_{i}",
            catalog_id="2",
            catalog_name="Test Catalog 2",
            location=SkyCoord("0d 0d"),
            radius=Angle("1s"),
            name=f"Test Catalog Object 2_{i}",
            properties={},
        )
        for i in range(10)
    ),
)
def test_can_get_catalog_sample_rows(api_client):
    response = api_client.get("/v1/catalog_samples")
    assert response.status_code == 200
    catalog_samples = response.json["data"]
    # This API endpoint returns 5 sample rows from each catalog
    assert len(catalog_samples) == 10
    assert (
        len(
            [
                s
                for s in catalog_samples
                if s["attributes"]["catalog_name"] == "Test Catalog 1"
            ]
        )
        == 5
    )
    assert (
        len(
            [
                s
                for s in catalog_samples
                if s["attributes"]["catalog_name"] == "Test Catalog 2"
            ]
        )
        == 5
    )


@pytest.mark.with_data(
    Catalog(id="1", name="Test Catalog 1"),
    Catalog(id="2", name="Test Catalog 2"),
    *(
        CatalogObject(
            id=f"1_{i}",
            catalog_id="1",
            catalog_name="Test Catalog 1",
            location=SkyCoord("0d 0d"),
            radius=Angle("1s"),
            name=f"Test Catalog Object 1_{i}",
            properties={},
        )
        for i in range(10)
    ),
    *(
        CatalogObject(
            id=f"2_{i}",
            catalog_id="2",
            catalog_name="Test Catalog 2",
            location=SkyCoord("0d 0d"),
            radius=Angle("1s"),
            name=f"Test Catalog Object 2_{i}",
            properties={},
        )
        for i in range(10)
    ),
)
def test_can_get_n_catalog_sample_rows(api_client):
    response = api_client.get("/v1/catalog_samples?n=10")
    assert response.status_code == 200
    catalog_samples = response.json["data"]
    # This API endpoint returns 10 sample rows from each catalog
    assert len(catalog_samples) == 20
    assert (
        len(
            [
                s
                for s in catalog_samples
                if s["attributes"]["catalog_name"] == "Test Catalog 1"
            ]
        )
        == 10
    )
    assert (
        len(
            [
                s
                for s in catalog_samples
                if s["attributes"]["catalog_name"] == "Test Catalog 2"
            ]
        )
        == 10
    )


@pytest.mark.with_data(
    Catalog(id="1", name="Test Catalog 1"),
    Catalog(id="2", name="Test Catalog 2"),
    Catalog(id="3", name="Test Catalog 3"),
    CatalogObject(
        id="1_1",
        catalog_id="1",
        catalog_name="Test Catalog 1",
        location=SkyCoord("10d 10d"),
        radius=Angle("1s"),
        name="Test Catalog Object 1_1",
        properties={"prop": "value_1"},
    ),
    CatalogObject(
        id="2_1",
        catalog_id="2",
        catalog_name="Test Catalog 2",
        location=SkyCoord("10d 10d"),
        radius=Angle("1s"),
        name="Test Catalog Object 2_1",
        properties={"prop": "value_2"},
    ),
    CatalogObject(
        id="3_1",
        catalog_id="3",
        catalog_name="Test Catalog 3",
        location=SkyCoord("9d 9d"),
        radius=Angle("1s"),
        name="Test Catalog Object 3_1",
        properties={"prop": "value_3"},
    ),
)
def test_can_search_catalogs(api_client):
    response = api_client.get("/v1/catalog_search/10.0/10.0")
    assert response.status_code == 200
    catalog_samples = response.json["data"]
    assert len(catalog_samples) == 2
    catalog_names = [f"Test Catalog {i}" for i in range(1, 3)]
    for catalog in catalog_samples:
        assert catalog["attributes"]["catalog_name"] in catalog_names
        assert "prop" in catalog["attributes"]["data"]


@pytest.mark.with_data(
    Catalog(id="1", name="Test Catalog 1"),
    Catalog(id="2", name="Test Catalog 2"),
    Catalog(id="3", name="Test Catalog 3"),
    CatalogObject(
        id="1_1",
        catalog_id="1",
        catalog_name="Test Catalog 1",
        location=SkyCoord("-10d -10d"),
        radius=Angle("1s"),
        name="Test Catalog Object 1_1",
        properties={"prop": "value_1"},
    ),
    CatalogObject(
        id="2_1",
        catalog_id="2",
        catalog_name="Test Catalog 2",
        location=SkyCoord("-10d -10d"),
        radius=Angle("1s"),
        name="Test Catalog Object 2_1",
        properties={"prop": "value_2"},
    ),
    CatalogObject(
        id="3_1",
        catalog_id="3",
        catalog_name="Test Catalog 3",
        location=SkyCoord("-9d -9d"),
        radius=Angle("1s"),
        name="Test Catalog Object 3_1",
        properties={"prop": "value_3"},
    ),
)
def test_can_search_catalogs_with_negative_coordinates(api_client):
    response = api_client.get("/v1/catalog_search/-10.0/-10.0")
    assert response.status_code == 200
    catalog_samples = response.json["data"]
    assert len(catalog_samples) == 2
    catalog_names = [f"Test Catalog {i}" for i in range(1, 3)]
    for catalog in catalog_samples:
        assert catalog["attributes"]["catalog_name"] in catalog_names
        assert "prop" in catalog["attributes"]["data"]
