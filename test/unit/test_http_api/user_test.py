import uuid

import pytest

from antares.domain.models import User


def test_create_user(api_client):
    response = api_client.post(
        "/v1/users",
        json={
            "data": {
                "type": "user",
                "attributes": {
                    "name": "User",
                    "username": "user",
                    "email": "user@noirlab.edu",
                    "password": "password",
                },
            }
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    assert response.json["data"]["attributes"]["username"] == "user"
    assert response.json["data"]["attributes"]["email"] == "user@noirlab.edu"
    assert "password" not in response.json["data"]["attributes"]


@pytest.mark.with_data(User(name="foo", username="username", email="foo@test.com"))
def test_create_user_fails_if_non_unique_username(api_client):
    response = api_client.post(
        "/v1/users",
        json={
            "data": {
                "type": "user",
                "attributes": {
                    "name": "bar",
                    "username": "username",
                    "email": "bar@test.com",
                    "password": "password",
                },
            }
        },
    )
    try:
        assert response.status_code == 409
    except AssertionError:
        raise Exception(response.json)


def test_user_can_get_self(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{authentication.user.id}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200


def test_get_user_doesnt_respond_with_password(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{authentication.user.id}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert "password" not in response.json["data"]["attributes"]


@pytest.mark.with_data(
    User(id=uuid.uuid4(), name="User 1", username="user1", email="user1@noirlab.edu"),
    User(id=uuid.uuid4(), name="User 2", username="user2", email="user2@noirlab.edu"),
    User(id=uuid.uuid4(), name="User 3", username="user3", email="user3@noirlab.edu"),
)
@pytest.mark.with_authenticated_user(admin=True)
def test_admin_user_can_view_list_of_users(api_client, authentication):
    response = api_client.get(
        "/v1/users",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert (
        len(response.json["data"]) == 4
    )  # Created three plus the admin role is automatically created


@pytest.mark.with_authenticated_user(admin=False)
def test_non_admin_user_cannot_view_other_user_list(api_client, authentication):
    response = api_client.get(
        "/v1/users",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    User(id=123, name="User 123", username="user123", email="user123@noirlab.edu"),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), admin=False)
def test_non_admin_user_cannot_view_other_user_detail(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=123)}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(admin=False)
def test_non_admin_user_cannot_grant_admin_rights(api_client, authentication):
    response = api_client.patch(
        f"/v1/users/{authentication.user.id}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "type": "user",
                "attributes": {
                    "admin": True,
                },
            },
        },
    )
    try:
        assert response.status_code == 401
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_authenticated_user(admin=False)
def test_non_admin_user_cannot_grant_staff_rights(api_client, authentication):
    response = api_client.patch(
        f"/v1/users/{authentication.user.id}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "type": "user",
                "attributes": {
                    "staff": True,
                },
            },
        },
    )
    try:
        assert response.status_code == 401
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    User(
        id=uuid.UUID(int=123),
        name="Other",
        username="other",
        email="other@noirlab.edu",
        admin=False,
    )
)
@pytest.mark.with_authenticated_user(admin=True)
def test_admin_user_can_grant_admin_rights(api_client, authentication):
    response = api_client.patch(
        f"/v1/users/{uuid.UUID(int=123)}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "id": authentication.user.id,
                "type": "user",
                "attributes": {
                    "admin": True,
                },
            },
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    User(
        id=uuid.UUID(int=123),
        name="Other",
        username="other",
        email="other@noirlab.edu",
        admin=True,
    )
)
@pytest.mark.with_authenticated_user(admin=True)
def test_admin_user_can_revoke_admin_rights(api_client, authentication):
    response = api_client.patch(
        f"/v1/users/{uuid.UUID(int=123)}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "id": authentication.user.id,
                "type": "user",
                "attributes": {
                    "admin": False,
                },
            },
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    User(
        id=uuid.UUID(int=123),
        name="Other",
        username="other",
        email="other@noirlab.edu",
        staff=False,
    )
)
@pytest.mark.with_authenticated_user(admin=True)
def test_admin_user_can_grant_staff_rights(api_client, authentication):
    response = api_client.patch(
        f"/v1/users/{uuid.UUID(int=123)}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "id": authentication.user.id,
                "type": "user",
                "attributes": {
                    "staff": True,
                },
            },
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    User(
        id=uuid.UUID(int=123),
        name="Other",
        username="other",
        email="other@noirlab.edu",
        staff=True,
    )
)
@pytest.mark.with_authenticated_user(admin=True)
def test_admin_user_can_revoke_staff_rights(api_client, authentication):
    response = api_client.patch(
        f"/v1/users/{uuid.UUID(int=123)}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "id": authentication.user.id,
                "type": "user",
                "attributes": {
                    "staff": False,
                },
            },
        },
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
