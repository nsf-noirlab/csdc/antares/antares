import uuid

import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import WatchList, WatchObject
from antares.external.sql import engine


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(hex="00000000-0000-0000-0000-000000000000"),
        name="My WL",
        owner_id=uuid.UUID(hex="00000000-0000-0000-0000-000000000001"),
        description="",
    )
)
@pytest.mark.with_authenticated_user(
    id=uuid.UUID(hex="00000000-0000-0000-0000-000000000001")
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_user_can_get_a_watch_list_they_own(api_client, authentication):
    response = api_client.get(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    assert response.json["data"]["id"] == "00000000-0000-0000-0000-000000000000"
    assert response.json["data"]["attributes"]["name"] == "My WL"


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_user_cannot_get_a_watch_list_that_doesnt_exist_returns_401(
    api_client, authentication
):
    response = api_client.get(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_request_with_malformed_id_returns_404(api_client, authentication):
    response = api_client.get(
        "/v1/watch_lists/notauuid",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 404


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=0), name="My WL", owner_id=uuid.UUID(int=123), description=""
    )
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_user_cannot_get_a_watch_list_they_dont_own(api_client, authentication):
    response = api_client.get(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=0), name="My WL", owner_id=uuid.UUID(int=123), description=""
    )
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), admin=True)
def test_admin_cannot_get_a_watch_list_they_dont_own(api_client, authentication):
    response = api_client.get(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


def test_unauthenticated_user_cannot_access_get_endpoint(api_client):
    response = api_client.get("/v1/watch_lists/00000000-0000-0000-0000-000000000000")
    assert response.status_code == 401


def test_unauthenticated_user_cannot_delete_watch_list(api_client):
    response = api_client.delete("/v1/watch_lists/00000000-0000-0000-0000-000000000000")
    assert response.status_code == 401


@pytest.mark.xfail
@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=0), name="My WL", owner_id=uuid.UUID(int=1), description=""
    )
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), admin=True)
def test_user_can_delete_watch_list_they_own(api_client, authentication, container):
    assert container.watch_list_repository().get(uuid.UUID(int=0))
    response = api_client.delete(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    engine.get_session()
    try:
        assert response.status_code == 204
    except AssertionError:
        raise Exception(response.json)
    assert not container.watch_list_repository().get(uuid.UUID(int=0))


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(hex="00000000-0000-0000-0000-000000000000"),
        name="WL",
        owner_id=uuid.UUID(int=123),
        description="",
    )
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_cannot_delete_watch_list_they_dont_own(api_client, authentication):
    response = api_client.delete(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=0), name="WL", owner_id=uuid.UUID(int=123), description=""
    )
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=False)
def test_staff_user_cannot_delete_watch_list_they_dont_own(api_client, authentication):
    response = api_client.delete(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=0), name="WL", owner_id=uuid.UUID(int=123), description=""
    )
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_cannot_delete_watch_list_they_dont_own(api_client, authentication):
    response = api_client.delete(
        "/v1/watch_lists/00000000-0000-0000-0000-000000000000",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


def test_patch_method_not_allowed_responds_405(api_client):
    response = api_client.patch("/v1/watch_lists/00000000-0000-0000-0000-000000000000")
    assert response.status_code == 405


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_authenticated_user_can_post_watch_list(
    api_client, authentication, watch_list_repository, watch_object_repository
):
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=1)}/watch_lists",
        json={
            "data": {
                "type": "watch_list",
                "relationships": {
                    "owner": {"data": {"type": "user", "id": uuid.UUID(int=1)}}
                },
                "attributes": {
                    "name": "Watch List",
                    "slack_channel": "#test_channel",
                    "enabled": True,
                    "description": "Test watch list",
                    "objects": [
                        {
                            "right_ascension": 100.0,
                            "declination": 30.0,
                            "radius": 0.00028,
                            "comment": "A made up object",
                        }
                    ],
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    watch_list_id = uuid.UUID(response.json["data"]["id"])
    watch_list = watch_list_repository.get(watch_list_id)
    watch_objects = list(watch_object_repository.list_by_watch_list_id(watch_list_id))
    assert watch_list
    assert len(watch_objects) == 1
    assert watch_objects[0].location == SkyCoord("100.0d 30.0d")


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_authenticated_user_cannot_post_watch_list_if_wrong_payload_user_id_gets_401(
    api_client, authentication
):
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=1)}/watch_lists",
        json={
            "data": {
                "type": "watch_list",
                # See here, wrong user ID in relationship
                "relationships": {
                    "owner": {"data": {"type": "user", "id": uuid.uuid4()}}
                },
                "attributes": {
                    "name": "Watch List",
                    "slack_channel": "#test_channel",
                    "enabled": True,
                    "description": "Test watch list",
                    "objects": [
                        {
                            "right_ascension": 100.0,
                            "declination": 30.0,
                            "radius": 0.00028,
                            "comment": "A made up object",
                        }
                    ],
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 401
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_authenticated_user_cannot_post_watch_list_if_wrong_route_user_id_gets_400(
    api_client, authentication
):
    response = api_client.post(
        # See here, wrong user ID in route
        f"/v1/users/{uuid.UUID(int=123)}/watch_lists",
        json={
            "data": {
                "type": "watch_list",
                "relationships": {"owner": {"data": {"type": "user", "id": "1"}}},
                "attributes": {
                    "name": "Watch List",
                    "slack_channel": "#test_channel",
                    "enabled": True,
                    "description": "Test watch list",
                    "objects": [
                        {
                            "right_ascension": 100.0,
                            "declination": 30.0,
                            "radius": 0.00028,
                            "comment": "A made up object",
                        }
                    ],
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 400
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), admin=True)
def test_authenticated_admin_cannot_post_watch_list_if_wrong_route_user_id_gets_401(
    api_client, authentication
):
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=123)}/watch_lists",
        json={
            "data": {
                "type": "watch_list",
                "relationships": {
                    "owner": {"data": {"type": "user", "id": uuid.UUID(int=123)}}
                },
                "attributes": {
                    "name": "Watch List",
                    "slack_channel": "#test_channel",
                    "enabled": True,
                    "description": "Test watch list",
                    "objects": [
                        {
                            "right_ascension": 100.0,
                            "declination": 30.0,
                            "radius": 0.00028,
                            "comment": "A made up object",
                        }
                    ],
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 401
    except AssertionError:
        raise Exception(response.json)


def test_unauthenticated_user_cannot_post_filter_gets_401(api_client):
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=123)}/watch_lists",
        json={
            "data": {
                "type": "watch_list",
                "relationships": {
                    "owner": {"data": {"type": "user", "id": uuid.UUID(int=123)}}
                },
                "attributes": {
                    "name": "Watch List",
                    "slack_channel": "#test_channel",
                    "enabled": True,
                    "description": "Test watch list",
                    "objects": [
                        {
                            "right_ascension": 100.0,
                            "declination": 30.0,
                            "radius": 0.00028,
                            "comment": "A made up object",
                        }
                    ],
                },
            }
        },
    )
    try:
        assert response.status_code == 401
    except AssertionError:
        raise Exception(response.json)


def test_slash_watch_lists_route_404(api_client):
    """
    For the time being we intentionally don't expose a /watch_lists
    route.
    """
    response = api_client.get("v1/watch_lists")
    assert response.status_code == 404


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_regular_user_can_get_their_watch_lists(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=1)}/watch_lists",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=1), owner_id=uuid.UUID(int=1), name="My WL", description=""
    ),
    WatchList(
        id=uuid.UUID(int=2),
        owner_id=uuid.UUID(int=123),
        name="Their WL",
        description="",
    ),
    WatchObject(
        id=uuid.UUID(int=3),
        watch_list_id=uuid.UUID(int=1),
        name="My Object",
        location=SkyCoord("0d 0d"),
        radius=Angle("1s"),
    ),
    WatchObject(
        id=uuid.UUID(int=4),
        watch_list_id=uuid.UUID(int=2),
        name="Their Object",
        location=SkyCoord("0d 0d"),
        radius=Angle("1s"),
    ),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_user_watch_lists_list_only_returns_users_watch_lists(
    api_client, authentication
):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=1)}/watch_lists",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    assert response.json["meta"]["count"] == 1
    assert len(response.json["data"]) == 1
    assert response.json["data"][0]["attributes"]["name"] == "My WL"


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_regular_user_cannot_get_other_users_watch_lists(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=123)}/watch_lists",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True)
def test_staff_user_cannot_get_other_users_watch_lists(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=123)}/watch_lists",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), admin=True)
def test_admin_user_cannot_get_other_users_watch_lists(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=123)}/watch_lists",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401
