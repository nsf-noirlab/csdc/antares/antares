import uuid

import pytest

from antares.domain.models import Filter, FilterRevision, FilterRevisionStatus, User


def make_filter_post_payload(filter_attributes: dict) -> dict:
    return {
        "data": {
            "type": "filter",
            "attributes": filter_attributes,
        }
    }


def make_filter_patch_payload(
    id_: str,
    filter_attributes: dict,
    filter_revision_id="",
) -> dict:
    payload = {
        "id": id_,
        "type": "filter",
        "attributes": filter_attributes,
    }
    if filter_revision_id:
        payload["relationships"] = {
            "enabled_version": {
                "data": {"type": "filter_version", "id": filter_revision_id},
            },
        }
    return {"data": payload}


def make_filter_version_post_payload(filter_revision):
    return {
        "data": {
            "type": "filter_version",
            "attributes": filter_revision,
        }
    }


def make_filter_version_patch_payload(
    id_: str,
    filter_version_attributes: dict,
) -> dict:
    return {
        "data": {
            "id": id_,
            "type": "filter_version",
            "attributes": filter_version_attributes,
        }
    }


@pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123)))
def test_unauthenticated_user_cannot_patch_filter(api_client):
    payload = {
        "data": {
            "id": "1",
            "type": "filter",
            "attributes": {"public": False},
        }
    }
    response = api_client.patch("/v1/filters/1", json=payload)
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=True)
)
def test_unauthenticated_user_can_get_a_public_filter_they_dont_own(api_client):
    response = api_client.get("/v1/filters/1")
    assert response.status_code == 200
    assert response.json["data"]["id"] == "1"
    assert response.json["data"]["attributes"]["name"] == "Filter"


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=False)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_cannot_get_a_private_filter_they_dont_own(api_client, authentication):
    response = api_client.get("/v1/filters/1")
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1), public=False)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_patch_filter_they_own(api_client, authentication):
    response = api_client.patch(
        "/v1/filters/1",
        json={
            "data": {
                "id": "1",
                "type": "filter",
                "attributes": {"public": True},
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise ValueError(response.json)


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=False)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_authenticated_user_cant_patch_filter_they_dont_own(api_client, authentication):
    response = api_client.patch(
        "/v1/filters/1",
        json={
            "data": {
                "id": "1",
                "type": "filter",
                "attributes": {"public": True},
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(staff=False, admin=False)
def test_authenticated_user_gets_404_if_try_to_patch_filter_does_not_exist(
    api_client, authentication
):
    response = api_client.patch(
        "/v1/filters/123",
        json={
            "data": {
                "id": "123",
                "type": "filter",
                "attributes": {"public": False},
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 404


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=2, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_only_patch_public_field(api_client, authentication):
    # Check user can't PATCH description field
    payload = make_filter_patch_payload(
        "1",
        {
            "description": "new description",
        },
    )
    response = api_client.patch(
        "/v1/filters/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 400

    # Check user can't PATCH enabled_filter_revision_id field
    payload = make_filter_patch_payload("1", {}, filter_revision_id="2")
    response = api_client.patch(
        "/v1/filters/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_get_a_filter_they_own(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["data"]["id"] == "1"


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=True)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_get_a_public_filter_they_dont_own(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["data"]["id"] == "1"


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=False)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_cant_get_a_filter_they_dont_own(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=False)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_can_patch_filter_public_field(api_client, authentication):
    response = api_client.patch(
        "/v1/filters/1",
        json={
            "data": {
                "id": "1",
                "type": "filter",
                "attributes": {"public": True},
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    # Check that field is public now
    response = api_client.get(
        "/v1/filters/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["data"]["attributes"]["public"] is True


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=2, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_cannot_patch_filter_enabled_filter_revision_id_if_not_reviewed(
    api_client,
    authentication,
):
    payload = make_filter_patch_payload("1", {}, filter_revision_id="2")
    response = api_client.patch(
        "/v1/filters/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 500


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=2, code="", status=FilterRevisionStatus.REVIEWED),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_can_patch_filter_enabled_filter_revision_id_if_reviewed(
    api_client,
    authentication,
):
    enabled_filter_revision_id = "2"
    payload = make_filter_patch_payload(
        "1",
        {},
        filter_revision_id=enabled_filter_revision_id,
    )
    response = api_client.patch(
        "/v1/filters/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200

    # Check that filter is enabled now
    response = api_client.get(
        "/v1/filters/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert (
        response.json["data"]["relationships"]["enabled_version"]["data"]["id"]
        == enabled_filter_revision_id
    )

    # Check that filter version has ENABLED status
    response = api_client.get(
        f"/v1/filters/1/versions/{enabled_filter_revision_id}",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert (
        response.json["data"]["attributes"]["status"]
        == FilterRevisionStatus.ENABLED.name
    )


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1), public=False)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_cannot_patch_unallowed_fields(api_client, authentication):
    # Check can't PATCH description
    payload = make_filter_patch_payload(
        "1",
        {
            "description": "new description",
        },
    )
    response = api_client.patch(
        "/v1/filters/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 400

    # Check can't PATCH name
    payload = make_filter_patch_payload(
        "1",
        {
            "name": "this won't work",
        },
    )
    response = api_client.patch(
        "/v1/filters/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 400


def test_filter_delete_method_not_allowed_responds_405(api_client):
    response = api_client.delete("/v1/filters/1")
    assert response.status_code == 405


@pytest.mark.with_authenticated_user(staff=False, admin=False)
def test_authenticated_user_can_post_filter(api_client, authentication):
    response = api_client.post(
        f"/v1/users/{authentication.user.id}/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "Filter",
                    "user_id": authentication.user.id,
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_authenticated_user(staff=False, admin=False)
def test_authenticated_user_cannot_post_filter_to_other_user_endpoint_gets_401(
    api_client, authentication
):
    response = api_client.post(
        f"/v1/users/{uuid.uuid4()}/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "Filter",
                    "user_id": authentication.user.id,
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(staff=False, admin=False)
def test_authenticated_user_cannot_post_filter_to_other_user_payload_gets_401(
    api_client, authentication
):
    response = api_client.post(
        f"/v1/users/{authentication.user.id}/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "Filter",
                    "user_id": uuid.uuid4(),
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(staff=False, admin=True)
def test_admin_user_cannot_post_filter_to_other_user_endpoint_gets_401(
    api_client, authentication
):
    response = api_client.post(
        f"/v1/users/{uuid.uuid4()}/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "Filter",
                    "user_id": authentication.user.id,
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(staff=False, admin=True)
def test_admin_user_cannot_post_filter_to_other_user_payload_gets_401(
    api_client, authentication
):
    response = api_client.post(
        f"/v1/users/{authentication.user.id}/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "Filter",
                    "user_id": uuid.uuid4(),
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


def test_unauthenticated_user_cannot_post_filter_gets_401(api_client):
    other_user_id = uuid.uuid4()
    response = api_client.post(
        f"/v1/users/{other_user_id}/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {"name": "Filter", "user_id": other_user_id},
            }
        },
    )
    assert response.status_code == 401


def test_post_method_not_allowed_filters_list(api_client, authentication):
    response = api_client.post(
        "/v1/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "Filter",
                    "user_id": uuid.uuid4(),
                },
            }
        },
    )
    assert response.status_code == 405


@pytest.mark.with_authenticated_user(staff=False, admin=True)
def test_post_method_not_allowed_filters_list_even_logged_in_as_admin(
    api_client, authentication
):
    response = api_client.post(
        "/v1/filters",
        json={
            "data": {
                "type": "filter",
                "attributes": {
                    "name": "Filter",
                    "user_id": uuid.uuid4(),
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 405


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=2), public=True),
    Filter(id=3, name="Filter", owner_id=uuid.UUID(int=3), public=False),
)
@pytest.mark.with_authenticated_user(staff=False, admin=True)
def test_admin_user_gets_all_filters(api_client, authentication):
    response = api_client.get(
        "/v1/filters",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200, response.json
    assert response.json["meta"]["count"] == 3
    assert sorted([f["id"] for f in response.json["data"]]) == ["1", "2", "3"]
    assert response.json.get("included") is None


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=2), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=3), public=True),
    Filter(id=3, name="Filter", owner_id=uuid.UUID(int=4), public=False),
    User(
        id=uuid.UUID(int=2), name="User 2", username="user2", email="user2@noirlab.edu"
    ),
    User(
        id=uuid.UUID(int=3), name="User 3", username="user3", email="user3@noirlab.edu"
    ),
    User(
        id=uuid.UUID(int=4), name="User 4", username="user4", email="user4@noirlab.edu"
    ),
)
@pytest.mark.with_authenticated_user(staff=False, admin=True)
def test_admin_user_gets_all_filters_including_owners(api_client, authentication):
    response = api_client.get(
        "/v1/filters?include=owner",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200, response.json
    assert response.json["meta"]["count"] == 3
    assert sorted([f["id"] for f in response.json["data"]]) == ["1", "2", "3"]
    included_property = response.json.get("included")
    assert included_property is not None
    included_users = [elem for elem in included_property if elem["type"] == "user"]
    owner_ids = [
        str(id) for id in [uuid.UUID(int=2), uuid.UUID(int=3), uuid.UUID(int=4)]
    ]
    assert sorted([u["id"] for u in included_users]) == owner_ids
    assert [u["attributes"].get("password") for u in included_users] == [
        None,
        None,
        None,
    ]


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=2), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=3), public=True),
    Filter(id=3, name="Filter", owner_id=uuid.UUID(int=4), public=False),
    User(
        id=uuid.UUID(int=2), name="User 2", username="user2", email="user2@noirlab.edu"
    ),
)
@pytest.mark.with_authenticated_user(staff=False, admin=True)
def test_admin_user_gets_all_filters_including_only_existing_owners(
    api_client, authentication
):
    response = api_client.get(
        "/v1/filters?include=owner",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200, response.json
    assert response.json["meta"]["count"] == 3
    assert sorted([f["id"] for f in response.json["data"]]) == ["1", "2", "3"]
    # Filters can reference deleted users which are not in the "included" property
    included_property = response.json.get("included")
    assert included_property is not None
    included_users = [elem for elem in included_property if elem["type"] == "user"]
    owner_ids = [str(uuid.UUID(int=2))]
    assert [u["id"] for u in included_users] == owner_ids
    assert [u["attributes"].get("password") for u in included_users] == [None]


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=2), public=True),
    Filter(id=3, name="Filter", owner_id=uuid.UUID(int=2), public=False),
)
def test_unauthenticated_user_can_get_public_filters(api_client):
    response = api_client.get("/v1/filters")
    assert response.status_code == 200, response.json
    assert len(response.json["data"]) == 2
    assert response.json["meta"]["count"] == 2
    assert sorted([f["id"] for f in response.json["data"]]) == ["1", "2"]
    assert response.json.get("included") is None


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=2), public=True),
    Filter(id=3, name="Filter", owner_id=uuid.UUID(int=2), public=False),
    User(
        id=uuid.UUID(int=1), name="User 1", username="user1", email="user1@noirlab.edu"
    ),
    User(
        id=uuid.UUID(int=2), name="User 2", username="user2", email="user2@noirlab.edu"
    ),
)
def test_unauthenticated_user_can_get_public_filters_including_owners(api_client):
    response = api_client.get("/v1/filters?include=owner")
    assert response.status_code == 200, response.json
    assert len(response.json["data"]) == 2
    assert response.json["meta"]["count"] == 2
    assert sorted([f["id"] for f in response.json["data"]]) == ["1", "2"]
    included_property = response.json.get("included")
    assert included_property is not None
    included_users = [elem for elem in included_property if elem["type"] == "user"]
    owner_ids = [str(id) for id in [uuid.UUID(int=1), uuid.UUID(int=2)]]
    assert sorted([u["id"] for u in included_users]) == owner_ids
    assert [u["attributes"].get("password") for u in included_users] == [None, None]


@pytest.mark.with_authenticated_user(staff=False, admin=False)
def test_regular_user_can_get_their_filters(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{authentication.user.id}/filters",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=123), public=False),
)
def test_unauthenticated_user_can_get_specific_users_public_filters(api_client):
    response = api_client.get(f"/v1/users/{uuid.UUID(int=123)}/filters")
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 1
    assert response.json["data"][0]["id"] == "1"


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=123), public=False),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_regular_user_can_get_other_users_public_filters(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=123)}/filters",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 1
    assert len(response.json["data"]) == 1
    assert response.json["data"][0]["id"] == "1"


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=1), public=False),
    Filter(id=3, name="Filter", owner_id=uuid.UUID(int=2), public=True),
    Filter(id=4, name="Filter", owner_id=uuid.UUID(int=2), public=False),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_get_their_filters_and_other_users_public_filters(
    api_client, authentication
):
    response = api_client.get(
        "/v1/filters",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 3
    assert len(response.json["data"]) == 3
    assert sorted([f["id"] for f in response.json["data"]]) == ["1", "2", "3"]
    assert response.json.get("included") is None


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1), public=True),
    Filter(id=2, name="Filter", owner_id=uuid.UUID(int=1), public=False),
    Filter(id=3, name="Filter", owner_id=uuid.UUID(int=2), public=True),
    Filter(id=4, name="Filter", owner_id=uuid.UUID(int=2), public=False),
    User(
        id=uuid.UUID(int=2), name="User 2", username="user2", email="user2@noirlab.edu"
    ),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_get_their_filters_and_other_users_public_filters_including_owners(
    api_client, authentication
):
    response = api_client.get(
        "/v1/filters?include=owner",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 3
    assert len(response.json["data"]) == 3
    assert sorted([f["id"] for f in response.json["data"]]) == ["1", "2", "3"]
    included_property = response.json.get("included")
    assert included_property is not None
    included_users = [elem for elem in included_property if elem["type"] == "user"]
    owner_ids = [str(id) for id in [uuid.UUID(int=1), uuid.UUID(int=2)]]
    assert sorted([u["id"] for u in included_users]) == owner_ids
    assert [u["attributes"].get("password") for u in included_users] == [None, None]


@pytest.mark.with_authenticated_user(staff=False, admin=False)
def test_authenticated_user_posts_revision_to_non_existent_filter_gets_401(
    api_client, authentication
):
    response = api_client.post(
        "/v1/filters/123/versions",
        json={
            "data": {
                "type": "filter_version",
                "attributes": {"code": ""},
                "relationships": {"filter": {"data": {"type": "filter", "id": "1"}}},
            },
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 401
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_authenticated_user_can_post_filter_version_to_their_filter(
    api_client, authentication
):
    response = api_client.post(
        "/v1/filters/1/versions",
        json={
            "data": {
                "type": "filter_version",
                "attributes": {"code": "", "comment": "My Filter Version"},
                "relationships": {"filter": {"data": {"type": "filter", "id": "1"}}},
            },
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["data"]["attributes"]["comment"] == "My Filter Version"


@pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123)))
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_authenticated_user_cannot_post_filter_version_to_other_user_gets_401(
    api_client, authentication
):
    payload = {
        "data": {
            "type": "filter_version",
            "attributes": {"code": "", "comment": "My Filter Version"},
            "relationships": {"filter": {"data": {"type": "filter", "id": "1"}}},
        }
    }
    response = api_client.post(
        "/v1/filters/1/versions",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123), public=True)
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_authenticated_user_cannot_post_filter_version_to_other_user_public_filter(
    api_client, authentication
):
    payload = {
        "data": {
            "type": "filter_version",
            "attributes": {"code": "", "comment": "My Filter Version"},
            "relationships": {"filter": {"data": {"type": "filter", "id": "1"}}},
        }
    }
    response = api_client.post(
        "/v1/filters/1/versions",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=123)))
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_can_post_filter_version_to_other_user(api_client, authentication):
    payload = {
        "data": {
            "type": "filter_version",
            "attributes": {"code": "", "comment": "My Filter Version"},
            "relationships": {"filter": {"data": {"type": "filter", "id": "1"}}},
        }
    }
    response = api_client.post(
        "/v1/filters/1/versions",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["data"]["attributes"]["comment"] == "My Filter Version"


@pytest.mark.with_data(Filter(id=1, name="Test Filter", owner_id=uuid.UUID(int=123)))
def test_unauthenticated_user_cannot_post_filter_version_gets_401(api_client):
    payload = {
        "data": {
            "type": "filter_version",
            "attributes": {"code": "", "comment": "My Filter Version"},
            "relationships": {"filter": {"data": {"type": "filter", "id": "1"}}},
        }
    }
    response = api_client.post("/v1/filters/1/versions", json=payload)
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=1, code=""),
    Filter(id=2, name="Filter 2", owner_id=uuid.UUID(int=2)),
    FilterRevision(filter_id=2, id=2, code=""),
    Filter(id=3, name="Filter 3", owner_id=uuid.UUID(int=3)),
    FilterRevision(filter_id=3, id=3, code=""),
    Filter(id=4, name="Filter 4", owner_id=uuid.UUID(int=4)),
    FilterRevision(filter_id=4, id=4, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=5), staff=False, admin=True)
def test_admin_user_gets_all_filters_filter_versions(api_client, authentication):
    for filter_id, filter_revision_id in [(1, 1), (2, 2), (3, 3), (4, 4)]:
        response = api_client.get(
            f"/v1/filters/{filter_id}/versions",
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        assert response.status_code == 200
        assert response.json["meta"]["count"] == 1
        assert response.json["data"][0]["id"] == str(filter_revision_id)


@pytest.mark.with_data(Filter(id=1, name="Test Filter", owner_id=uuid.UUID(int=1)))
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_regular_user_can_get_their_filter_revisions(api_client, authentication):
    for i in range(3):
        payload = {
            "data": {
                "type": "filter_version",
                "attributes": {"code": "", "comment": f"v{i}"},
                "relationships": {"filter": {"data": {"type": "filter", "id": "1"}}},
            }
        }
        response = api_client.post(
            "/v1/filters/1/versions",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        assert response.status_code == 200
    response = api_client.get(
        "/v1/filters/1/versions",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert len(response.json["data"]) == 3
    for i in range(3):
        assert f"v{i}" in [
            version["attributes"]["comment"] for version in response.json["data"]
        ]


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_get_their_filters(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1/versions",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 1
    assert sorted([fr["id"] for fr in response.json["data"]]) == ["1"]


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=False),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_cannot_get_other_users_private_filter_versions(
    api_client, authentication
):
    response = api_client.get(
        "/v1/filters/1/versions",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=True),
    FilterRevision(filter_id=1, id=1, code=""),
    FilterRevision(filter_id=1, id=2, code=""),
)
def test_unauthenticated_user_can_get_other_users_public_filter_versions(api_client):
    response = api_client.get("/v1/filters/1/versions")
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 2


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=True),
    FilterRevision(filter_id=1, id=1, code=""),
    FilterRevision(filter_id=1, id=2, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_user_can_get_other_users_public_filter_versions(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1/versions",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 2


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=False)
def test_staff_user_can_get_their_filters(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1/versions",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 1


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=False),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=False)
def test_staff_user_cannot_get_other_users_filters(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1/versions",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=1), public=False),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=False)
def test_user_can_get_a_filter_version_they_own(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1/versions/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    assert response.json["data"]["id"] == "1"


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=False),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=False)
def test_user_cannot_get_a_private_filter_version_they_dont_own(
    api_client, authentication
):
    response = api_client.get(
        "/v1/filters/1/versions/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=True),
    FilterRevision(filter_id=1, id=1, code=""),
)
def test_unauthenticated_user_can_get_a_public_filter_version_they_dont_own(api_client):
    response = api_client.get(
        "/v1/filters/1/versions/1",
    )
    assert response.status_code == 200
    assert response.json["data"]["id"] == "1"
    assert response.json["data"]["attributes"]["code"] == ""


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=True),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=False)
def test_user_can_get_a_public_filter_version_they_dont_own(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1/versions/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["data"]["id"] == "1"
    assert response.json["data"]["attributes"]["code"] == ""


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123), public=False),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=True)
def test_admin_can_get_a_filter_version_they_dont_own(api_client, authentication):
    response = api_client.get(
        "/v1/filters/1/versions/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["data"]["id"] == "1"
    assert response.json["data"]["attributes"]["code"] == ""


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=1)),
)
def test_unauthenticated_user_cannot_access_get_endpoint(api_client):
    response = api_client.get("/v1/filters/1/versions")
    assert response.status_code == 401


def test_delete_method_not_allowed_responds_405(api_client):
    response = api_client.delete("/v1/filters/1/versions")
    assert response.status_code == 405


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=1, code=""),
)
def test_unauthenticated_user_cannot_patch_filter_version(api_client):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.REVIEWED.name}
    )
    response = api_client.patch("/v1/filters/1/versions/1", json=payload)
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=1)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
def test_authenticated_user_cannot_patch_filter_version(api_client, authentication):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.REVIEWED.name}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True, admin=False)
def test_staff_user_cannot_patch_filter_version(api_client, authentication):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.REVIEWED.name}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_gets_404_if_trying_to_patch_filter_version_for_non_existent_filter(
    api_client, authentication
):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.REVIEWED.name}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 404


@pytest.mark.with_data(Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123)))
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_gets_404_if_trying_to_patch_non_existent_filter_version(
    api_client, authentication
):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.REVIEWED.name}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 404


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123)),
    Filter(id=2, name="Filter 2", owner_id=uuid.UUID(int=123)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_cannot_patch_filter_version_of_filter_not_matching_filter_param(
    api_client, authentication
):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.REVIEWED.name}
    )
    response = api_client.patch(
        "/v1/filters/2/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_cannot_patch_filter_version_unallowed_fields(
    api_client, authentication
):
    # Every field except for `status` is not allowed.
    # This test includes the most relevant ones
    unallowed_fields_with_values = {
        "code": "New code",
        "comment": "New comment",
        "created_at": "2023-05-26T23:33:27",
        "updated_at": "2023-05-26T23:33:27",
        "transitioned_at": "2023-05-26T23:33:27",
    }
    payload = make_filter_version_patch_payload("1", unallowed_fields_with_values)
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 400


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_can_patch_filter_version_status_when_transition_is_allowed(
    api_client, authentication
):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.REVIEWED.name}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200


@pytest.mark.with_data(
    Filter(id=1, name="Filter 1", owner_id=uuid.UUID(int=123)),
    FilterRevision(filter_id=1, id=1, code=""),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_cannot_patch_filter_version_status_when_transition_is_not_allowed(
    api_client, authentication
):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.DISABLED.name}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 500


@pytest.mark.with_data(
    Filter(
        id=1, name="Filter 1", owner_id=uuid.UUID(int=123), enabled_filter_revision_id=1
    ),
    FilterRevision(filter_id=1, id=1, code="", status=FilterRevisionStatus.ENABLED),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_cannot_patch_filter_version_status_if_not_allowed_without_feedback(
    api_client, authentication
):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.DISABLED.name}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 500


@pytest.mark.with_data(
    Filter(
        id=1, name="Filter 1", owner_id=uuid.UUID(int=123), enabled_filter_revision_id=1
    ),
    FilterRevision(filter_id=1, id=1, code="", status=FilterRevisionStatus.ENABLED),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=True)
def test_admin_user_can_patch_filter_version_status_when_allowed_only_with_feedback(
    api_client, authentication
):
    payload = make_filter_version_patch_payload(
        "1", {"status": FilterRevisionStatus.DISABLED.name, "feedback": "Disabling..."}
    )
    response = api_client.patch(
        "/v1/filters/1/versions/1",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
