import json
from unittest.mock import ANY

import pytest

from antares.domain.models import Announcement


@pytest.mark.with_data(
    Announcement(id=1, message="foobar", variant="primary", active=True),
    Announcement(id=2, message="bazboo", variant="warning", active=False),
)
def test_anyone_can_view_list_of_announcements(api_client):
    response = api_client.get("/v1/announcements")
    try:
        assert response.status_code == 200
        assert len(response.json["data"]) == 2
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    Announcement(id=1, message="foobar", variant="primary", active=True),
    Announcement(id=2, message="bazboo", variant="warning", active=False),
)
def test_anyone_can_view_list_of_active_announcements(api_client):
    response = api_client.get(
        "/v1/announcements",
        query_string={
            "filter": json.dumps([{"field": "active", "op": "eq", "value": True}])
        },
    )
    try:
        assert response.status_code == 200
        assert len(response.json["data"]) == 1
        assert response.json["data"][0]["id"] == "1"
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_authenticated_user(admin=True)
def test_admin_can_create_announcement(
    api_client, authentication, announcement_repository
):
    response = api_client.post(
        "/v1/announcements",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "type": "announcement",
                "attributes": {
                    "variant": "warning",
                    "message": "Test Announcement",
                    "active": True,
                },
            },
            "meta": {},
        },
    )
    try:
        assert response.status_code == 200
        id_ = response.json["data"]["id"]
        assert announcement_repository.get(int(id_)) is not None
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_authenticated_user(admin=False)
def test_non_admin_cant_create_announcement(api_client, authentication):
    response = api_client.post(
        "/v1/announcements",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "type": "announcement",
                "attributes": {
                    "variant": "warning",
                    "message": "Test Announcement",
                    "active": True,
                },
            },
        },
    )
    try:
        assert response.status_code == 401
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    Announcement(id=1, message="foobar", variant="primary", active=True),
)
def test_anyone_can_get_an_announcement(api_client):
    response = api_client.get("/v1/announcements/1")
    try:
        assert response.status_code == 200
        assert response.json["data"] == {
            "type": "announcement",
            "id": "1",
            "attributes": {
                "active": True,
                "message": "foobar",
                "variant": "primary",
                "created_at": ANY,
            },
        }
    except AssertionError:
        raise Exception(response.json)


def test_getting_non_existent_announcement_returns_404(api_client):
    response = api_client.get("/v1/announcements/1")
    try:
        assert response.status_code == 404
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    Announcement(id=1, message="foobar", variant="primary", active=False),
)
@pytest.mark.with_authenticated_user(admin=True)
def test_admin_can_make_announcement_active(
    api_client, authentication, announcement_repository
):
    assert not announcement_repository.get(1).active
    response = api_client.patch(
        "/v1/announcements/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "id": "1",
                "type": "announcement",
                "attributes": {
                    "active": True,
                },
            },
        },
    )
    try:
        assert response.status_code == 200
        assert announcement_repository.get(1).active
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_data(
    Announcement(id=1, message="foobar", variant="primary", active=False),
)
@pytest.mark.with_authenticated_user(admin=False)
def test_non_admin_user_cant_update_announcement(
    api_client, authentication, announcement_repository
):
    assert not announcement_repository.get(1).active
    response = api_client.patch(
        "/v1/announcements/1",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
        json={
            "data": {
                "id": "1",
                "type": "announcement",
                "attributes": {
                    "active": True,
                },
            },
        },
    )
    try:
        assert response.status_code == 401
        assert not announcement_repository.get(1).active
    except AssertionError:
        raise Exception(response.json)
