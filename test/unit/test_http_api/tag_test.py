from test.fakes import build_filter_revision_sets_tag
from uuid import UUID

from pytest import mark

from antares.domain.models import Filter, FilterRevisionStatus


@mark.with_data(
    Filter(
        id=1,
        name="Filter 1",
        owner_id=UUID(int=1),
        public=True,
        enabled_filter_revision_id=1,
    ),
    build_filter_revision_sets_tag(
        id=1,
        filter_=Filter(id=1, name="Filter 1", owner_id=UUID(int=1)),
        tag="some_tag",
        status=FilterRevisionStatus.ENABLED,
    ),
    Filter(id=2, name="Filter 2", owner_id=UUID(int=2)),
    build_filter_revision_sets_tag(
        filter_=Filter(id=2, name="Filter 2", owner_id=UUID(int=2)), tag="no_tag"
    ),
)
def test_users_can_see_tags_from_enabled_and_public_filters(api_client):
    response = api_client.get("/v1/tags")
    assert len(response.json["data"]) == 1
    assert response.json["data"][0] == {
        "type": "tag",
        "id": "some_tag",
        "attributes": {"description": "Test tag", "filter_version_id": 1},
        "links": {"self": "http://localhost/v1/tags/some_tag"},
    }


@mark.with_data(
    Filter(
        id=1,
        name="Filter 1",
        owner_id=UUID(int=1),
        public=True,
        enabled_filter_revision_id=1,
    ),
    build_filter_revision_sets_tag(
        id=1,
        filter_=Filter(id=1, name="Filter 1", owner_id=UUID(int=1)),
        tag="some_tag",
        status=FilterRevisionStatus.ENABLED,
    ),
)
def test_users_can_see_details_from_tag(api_client):
    response = api_client.get("/v1/tags/some_tag")
    assert response.json["data"] == {
        "type": "tag",
        "attributes": {"description": "Test tag", "filter_version_id": 1},
        "id": "some_tag",
        "links": {"self": "http://localhost/v1/tags/some_tag"},
    }


def test_users_get_404_when_tag_doesnt_exist(api_client):
    response = api_client.get("/v1/tags/some_tag")
    assert response.status_code == 404
