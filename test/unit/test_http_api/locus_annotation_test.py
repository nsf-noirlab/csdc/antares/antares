import uuid

import pytest

from antares.domain.models import LocusAnnotation

DEFAULT_LOCUS_ANNOTATION = {
    "comment": "My comment",
    "favorited": True,
}


def make_locus_annotation_post_payload(locus_annotation, owner_id, locus_id):
    return {
        "data": {
            "type": "locus_annotation",
            "attributes": locus_annotation,
            "relationships": {
                "owner": {"data": {"id": owner_id, "type": "user"}},
                "locus": {"data": {"id": locus_id, "type": "locus"}},
            },
        }
    }


def make_locus_annotation_patch_payload(id_, locus_annotation):
    return {
        "data": {
            "id": id_,
            "type": "locus_annotation",
            "attributes": locus_annotation,
        }
    }


@pytest.mark.with_data(
    LocusAnnotation(id=8, owner_id=uuid.UUID(int=1), locus_id="locus-001")
)
def test_unauthenticated_user_cannot_patch_locus_annotation(api_client):
    payload = make_locus_annotation_patch_payload(8, {"favorited": False})
    response = api_client.patch("/v1/locus_annotations/8", json=payload)
    assert response.status_code == 401


@pytest.mark.with_data(
    LocusAnnotation(id=8, owner_id=uuid.UUID(int=2), locus_id="locus-001")
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_authenticated_user_cannot_patch_locus_annotation_they_dont_own(
    api_client, authentication
):
    payload = make_locus_annotation_patch_payload(8, {"favorited": False})
    response = api_client.patch(
        "/v1/locus_annotations/8",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_authenticated_user_gets_401_if_try_to_patch_locus_annotation_does_not_exist(
    api_client, authentication
):
    payload = make_locus_annotation_patch_payload(666, {"favorited": False})
    response = api_client.patch(
        "/v1/locus_annotations/666",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_data(
    LocusAnnotation(id=8, owner_id=uuid.UUID(int=1), locus_id="locus-001")
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_user_can_patch_locus_annotation_they_own(api_client, authentication):
    payload = make_locus_annotation_patch_payload(8, {"favorited": False})
    response = api_client.patch(
        "/v1/locus_annotations/8",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200


def test_get_method_not_allowed_responds_405(api_client):
    response = api_client.get("/v1/locus_annotations/1")
    assert response.status_code == 405


def test_delete_method_not_allowed_responds_405(api_client):
    response = api_client.delete("/v1/locus_annotations/1")
    assert response.status_code == 405


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_authenticated_user_can_post_locus_annotation(api_client, authentication):
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=1)}/locus_annotations",
        json={
            "data": {
                "type": "locus_annotation",
                "attributes": {
                    "comment": "My comment",
                    "favorited": True,
                },
                "relationships": {
                    "owner": {"data": {"id": uuid.UUID(int=1), "type": "user"}},
                    "locus": {"data": {"id": "locus-001", "type": "locus"}},
                },
            }
        },
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_authenticated_user_cannot_post_locus_annotation_to_other_user_gets_401(
    api_client, authentication
):
    payload = make_locus_annotation_post_payload(
        DEFAULT_LOCUS_ANNOTATION, uuid.UUID(int=1), "locus-uuid"
    )
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=2)}/locus_annotations",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), admin=True)
def test_admin_user_cannot_post_locus_annotation_to_other_user_gets_401(
    api_client, authentication
):
    payload = make_locus_annotation_post_payload(
        DEFAULT_LOCUS_ANNOTATION, uuid.UUID(int=1), "locus-uuid"
    )
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=2)}/locus_annotations",
        json=payload,
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 401


def test_unauthenticated_user_cannot_post_locus_annotation_gets_401(api_client):
    payload = make_locus_annotation_post_payload(
        DEFAULT_LOCUS_ANNOTATION, owner_id=uuid.UUID(int=2), locus_id="locus-uuid"
    )
    response = api_client.post(
        f"/v1/users/{uuid.UUID(int=2)}/locus_annotations", json=payload
    )
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_regular_user_can_get_their_locus_annotations(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=1)}/locus_annotations",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200


@pytest.mark.with_data(
    LocusAnnotation(id=7, owner_id=uuid.UUID(int=1), locus_id="locus-001"),
    LocusAnnotation(id=8, owner_id=uuid.UUID(int=1), locus_id="locus-002"),
    LocusAnnotation(id=9, owner_id=uuid.UUID(int=2), locus_id="locus-001"),
)
@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_locus_annotations_list_only_returns_users_locus_annotations(
    api_client, authentication
):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=1)}/locus_annotations",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200
    assert response.json["meta"]["count"] == 2
    assert len(response.json["data"]) == 2


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1))
def test_regular_user_cannot_get_other_users_locus_annotations(
    api_client, authentication
):
    response = api_client.get(f"/v1/users/{uuid.UUID(int=2)}/locus_annotations")
    assert response.status_code == 401


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True)
def test_staff_user_can_get_their_locus_annotations(api_client, authentication):
    response = api_client.get(
        f"/v1/users/{uuid.UUID(int=1)}/locus_annotations",
        headers={"Authorization": f"Bearer {authentication.access_token}"},
    )
    assert response.status_code == 200


@pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=True)
def test_staff_user_cannot_get_other_users_locus_annotations(
    api_client, authentication
):
    response = api_client.get(f"/v1/users/{uuid.UUID(int=2)}/locus_annotations")
    assert response.status_code == 401


def test_locus_annotation_list_endpoint_404(api_client):
    response = api_client.get("/v1/locus_annotations")
    assert response.status_code == 404
