import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import Catalog, CatalogObject, Locus


@pytest.mark.with_data(
    Locus("locus-001", location=SkyCoord("0d 0d")),
    Catalog(id="1", name="Catalog 1"),
    Catalog(id="2", name="Catalog 2"),
    CatalogObject(
        "1",
        catalog_id="1",
        catalog_name="catalog_1",
        location=SkyCoord("0s 1s"),
        radius=Angle("2s"),
        name="Object 1",
        properties={},
    ),
    CatalogObject(
        "2",
        catalog_id="2",
        catalog_name="catalog_2",
        location=SkyCoord("0s 5s"),
        radius=Angle("10s"),
        name="Object 2",
        properties={},
    ),
)
def test_catalog_matches_include_angular_separation(api_client):
    response = api_client.get("/v1/loci/locus-001/catalog-matches")
    try:
        assert response.status_code == 200
    except AssertionError:
        raise Exception(response.json)
    catalog_matches = response.json["data"]
    assert catalog_matches
    assert len(catalog_matches) == 2


@pytest.mark.with_data(
    Locus("locus-001", location=SkyCoord("0d 0d")),
    Catalog(id="1", name="Catalog 1"),
    CatalogObject(
        "1",
        catalog_id="1",
        catalog_name="catalog_1",
        location=SkyCoord("0s 100s"),
        radius=Angle("2s"),
        name="Object 1",
        properties={},
    ),
)
def test_user_can_get_catalog_responds_empty_list_if_no_matches(api_client):
    response = api_client.get("/v1/loci/locus-001/catalog-matches")
    assert response.status_code == 200
    assert response.json["data"] == []


def test_post_method_not_allowed_responds_405(api_client):
    response = api_client.post("/v1/catalogs")
    assert response.status_code == 405
