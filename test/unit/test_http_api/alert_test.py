from antares.domain.models.alert import AlertThumbnail, AlertThumbnailType
from antares.entrypoints.http_api.resources.alert.schemas import AlertThumbnailSchema


def test_alert_schema_serializes_thumbnail_type_properly(api_client):
    alert_thumbnail = AlertThumbnail(
        "alert-thumbnail-001",
        "/src",
        "image/png",
        "filename.png",
        AlertThumbnailType.DIFFERENCE,
    )
    assert AlertThumbnailSchema().dump(alert_thumbnail) == {
        "data": {
            "type": "alert_thumbnail",
            "id": "alert-thumbnail-001",
            "attributes": {
                "src": "/src",
                "filename": "filename.png",
                "filemime": "image/png",
                "thumbnail_type": "difference",
            },
        }
    }
