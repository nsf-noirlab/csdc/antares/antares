import io

import fastavro
import pytest


def read_avro(path: str):
    with open(path, "rb") as f:
        blob = f.read()
        buffer = io.BytesIO(blob)
        buffer.seek(0)
        reader = fastavro.reader(buffer)
        return reader.writer_schema, list(reader)


version_3_columns = set(
    [
        "schemavsn",
        "publisher",
        "objectId",
        "candid",
        "candidate",
        "prv_candidates",
        "cutoutScience",
        "cutoutTemplate",
        "cutoutDifference",
    ]
)

version_4_columns = version_3_columns | set(["fp_hists",])


class TestFastAvroReader:
    """
    These tests are added because some fastavro versions are not able to read
    the schemas that ztf defines in their avro packages
    """

    @pytest.mark.parametrize(
        "avro_path,avro_version,version_columns",
        [
            (
                "test/data/alerts/ztf/version/2454460970015015003.avro",
                "3.3",
                version_3_columns,
            ),
            (
                "test/data/alerts/ztf/version/2454460970015010006.avro",
                "4.01",
                version_4_columns,
            ),
            (
                "test/data/alerts/ztf/version/2493109080015010005.avro",
                "4.02",
                version_4_columns,
            ),
        ],
    )
    def test_can_read_ztf_messages(self, avro_path, avro_version, version_columns):
        schema, messages = read_avro(avro_path)
        assert len(messages) == 1
        assert schema["version"] == avro_version
        assert set(messages[0].keys()) == version_columns
