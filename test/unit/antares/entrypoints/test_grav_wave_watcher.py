import json
import logging
from test.fakes import FakeGravWaveRepository
from unittest.mock import patch

import pytest

from antares.entrypoints import grav_wave_watcher
from antares.exceptions import Timeout

MODULE = "antares.entrypoints.grav_wave_watcher"


@pytest.fixture
def notice_json():
    with open(
        "test/data/e2e/grav_wave/MS181101ab-earlywarning.json", "rt", encoding="utf8"
    ) as notice_file:
        return notice_file.read()


@pytest.mark.usefixtures("message_bus")
@patch(f"{MODULE}.KafkaMessageSubscriptionService.poll", autospec=True)
class TestMain:
    def test_exits_if_loops_set(self, mock_poll):
        mock_poll.side_effect = Timeout
        grav_wave_watcher.main(1)

    def test_polls_kafka(self, mock_poll):
        mock_poll.side_effect = Timeout
        grav_wave_watcher.main(1)
        mock_poll.assert_called_once()

    def test_adds_notice_to_grav_wave_repo(self, mock_poll, notice_json):
        notice_dict = json.loads(notice_json)
        mock_poll.return_value = (None, notice_json, None)
        fake_grav_wave_repo = FakeGravWaveRepository()
        grav_wave_watcher.main(1, grav_wave_repo=fake_grav_wave_repo)
        notice = fake_grav_wave_repo.get(1)
        assert notice.gracedb_id == notice_dict["superevent_id"]

    def test_logs_but_doesnt_fail_on_duplicate_notice(
        self, mock_poll, notice_json, caplog
    ):
        with caplog.at_level(logging.INFO):
            notice_dict = json.loads(notice_json)
            mock_poll.return_value = (None, notice_json, None)
            fake_grav_wave_repo = FakeGravWaveRepository()
            grav_wave_watcher.main(1, grav_wave_repo=fake_grav_wave_repo)
            grav_wave_watcher.main(1, grav_wave_repo=fake_grav_wave_repo)
            assert "duplicate" in caplog.text.lower()
            notice = fake_grav_wave_repo.get(1)
            assert notice.gracedb_id == notice_dict["superevent_id"]
            assert fake_grav_wave_repo.get(2) is None

    def test_skips_test_notice_by_default(self, mock_poll, notice_json):
        mock_poll.return_value = (None, notice_json, None)
        fake_grav_wave_repo = FakeGravWaveRepository()
        grav_wave_watcher.main(1, grav_wave_repo=fake_grav_wave_repo, skip_test=True)
        assert fake_grav_wave_repo.get(1) is None

    def test_ingests_real_notice(self, mock_poll, notice_json):
        notice_dict = json.loads(notice_json)
        notice_dict["superevent_id"] = "S181101ab"
        mock_poll.return_value = (None, json.dumps(notice_dict), None)
        fake_grav_wave_repo = FakeGravWaveRepository()
        grav_wave_watcher.main(1, grav_wave_repo=fake_grav_wave_repo, skip_test=True)
        notice = fake_grav_wave_repo.get(1)
        assert notice.gracedb_id == notice_dict["superevent_id"]
