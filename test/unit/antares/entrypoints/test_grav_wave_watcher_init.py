from unittest.mock import patch

from antares.entrypoints import grav_wave_watcher_init


class TestInit:
    @patch("antares.entrypoints.grav_wave_watcher_init.main")
    def test_init_doesnt_call_main_when_name_isnt_main(self, mock_main):
        grav_wave_watcher_init.init()
        mock_main.assert_not_called()

    @patch.object(grav_wave_watcher_init, "__name__", "__main__")
    @patch("antares.entrypoints.grav_wave_watcher_init.main")
    def test_init_calls_main_when_name_is_main(self, mock_main):
        grav_wave_watcher_init.init()
        mock_main.assert_called_once_with()
