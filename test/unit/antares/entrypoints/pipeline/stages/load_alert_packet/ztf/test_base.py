from test.fakes import build_ztf_alert_packet


class TestZtfAlertPacket:
    def test_packet_has_triggering_alert(self):
        packet = build_ztf_alert_packet()
        assert packet.triggering_alert.id == "ztf_candidate:ztf-candid-001"

    def test_packet_has_previous_alerts(self):
        packet = build_ztf_alert_packet()
        for alert in packet.previous_alerts:
            assert alert.id is not None
