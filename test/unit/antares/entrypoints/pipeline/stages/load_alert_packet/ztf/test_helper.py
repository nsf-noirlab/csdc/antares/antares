from datetime import datetime
from test.fakes import build_ztf_alert_packet, build_ztf_event, build_ztf_event_fp_hist
from unittest.mock import patch

import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import Locus
from antares.entrypoints.pipeline.stages.load_alert_packet.ztf.helper import (
    compute_normalized_alert_properties,
    compute_survey_specific_alert_properties,
    create_alert_from_ztf_candidate,
    extract_alerts,
    get_alert_id_from_ztf_candidate,
    is_measurement,
)
from antares.utils import jd_to_mjd


def test_handle_case_where_no_previous_candidates():
    ztf_event = build_ztf_event(prv_candidates=None)
    assert extract_alerts(ztf_event)


@pytest.mark.with_data(
    Locus("locus-001", SkyCoord("0s 0.0s"), properties={"ztf_object_id": "ztf-001"}),
    Locus("locus-002", SkyCoord("0s 0.2s"), properties={"ztf_object_id": "ztf-001"}),
)
def test_handle_case_where_two_nearby_loci_with_same_ztf_object_id(
    message_bus, locus_repository, metrics
):
    alert_packet = build_ztf_alert_packet(
        locus_association_radius=Angle("1s"),
        locus_association_search_radius=Angle("2d"),
        candidate_kwargs={"ra": 0.0, "dec": 0.0},
        objectId="ztf-001",
    )
    locus = alert_packet.get_associated_locus(locus_repository, metrics=metrics)
    assert locus.id == "locus-001"


def test_ztf_alert_packet_raises_value_error_if_assocation_radius_is_greater_than_search_radius():
    with pytest.raises(ValueError):
        build_ztf_alert_packet(
            locus_association_radius=Angle("2d"),
            locus_association_search_radius=Angle("1d"),
        )


@pytest.mark.with_data(Locus("locus-001", SkyCoord("10d 0d")))
def test_ztf_alert_packet_get_associated_locus_returns_none_if_nothing_close(
    message_bus,
    locus_repository,
    metrics,
):
    alert_packet = build_ztf_alert_packet(candidate_kwargs={"ra": 0.0, "dec": 0.0})
    locus = alert_packet.get_associated_locus(locus_repository, metrics=metrics)
    assert locus is None


@pytest.mark.with_data(Locus("locus-001", SkyCoord("1d 0d")))
def test_ztf_alert_packet_get_associated_locus_returns_locus_within_association_radius(
    message_bus,
    locus_repository,
    metrics,
):
    alert_packet = build_ztf_alert_packet(
        locus_association_radius=Angle("2d"),
        locus_association_search_radius=Angle("2d"),
        candidate_kwargs={"ra": 0.0, "dec": 0.0},
    )
    locus = alert_packet.get_associated_locus(locus_repository, metrics=metrics)
    assert locus.id == "locus-001"


@pytest.mark.with_data(
    Locus("locus-001", SkyCoord("1d 0d")),
    Locus("locus-002", SkyCoord("2d 0d"), properties={"ztf_object_id": "ztf-001"}),
)
def test_ztf_alert_packet_get_associated_locus_prefers_locus_with_matching_ztf_object_id_even_if_other_is_closer(
    message_bus,
    locus_repository,
    metrics,
):
    alert_packet = build_ztf_alert_packet(
        locus_association_radius=Angle("3d"),
        locus_association_search_radius=Angle("3d"),
        candidate_kwargs={"ra": 0.0, "dec": 0.0},
        objectId="ztf-001",
    )
    locus = alert_packet.get_associated_locus(locus_repository, metrics=metrics)
    assert locus.id == "locus-002"


def test_ztf_alert_packet_computes_location_as_alert_centroid():
    alert_packet = build_ztf_alert_packet(
        candidate_kwargs={"ra": 10.0, "dec": 0.0},
        prv_candidate_kwargs=[
            {"ra": 5.0, "dec": 0.0},
        ],
    )
    assert alert_packet.location.separation(SkyCoord("7.5d 0d")) < Angle(
        1e-15, unit="deg"
    )


@pytest.mark.xfail
@patch("antares.service_layer.handlers.process_survey_message.ztf.datetime")
def test_ztf_alert_packet_computes_correct_normalized_candidate_alert_properties(
    mock_datetime,
):
    mock_datetime.utcnow.return_value = datetime(2021, 1, 1)
    event = build_ztf_event(candidate_kwargs={**EXAMPLE_CANDIDATE})
    assert compute_normalized_alert_properties(event["candidate"]) == {
        "ant_ra": 82.5827759,
        "ant_dec": 54.5418511,
        "ant_mjd": jd_to_mjd(2458428.8197338),
        "ant_time_received": round(datetime(2021, 1, 1).timestamp()),
        "ant_input_msg_time": round(datetime(2021, 1, 1).timestamp()),
        "ant_mag": 16.454681396484375,
        "ant_magerr": 0.18503333628177643,
        "ant_passband": "g",
        "ant_maglim": 20.72220802307129,
        "ant_survey": 1,
    }


@pytest.mark.xfail
@patch("antares.service_layer.handlers.process_survey_message.ztf.datetime")
def test_compute_normalized_alert_upper_limit_alert_properties(mock_datetime):
    mock_datetime.utcnow.return_value = datetime(2021, 1, 1)
    event = build_ztf_event(prv_candidate_kwargs=[EXAMPLE_UPPER_LIMIT])
    assert compute_normalized_alert_properties(event["prv_candidates"][0]) == {
        "ant_mjd": jd_to_mjd(2458486.9737037),
        "ant_time_received": round(datetime(2021, 1, 1).timestamp()),
        "ant_input_msg_time": round(datetime(2021, 1, 1).timestamp()),
        "ant_passband": "R",
        "ant_maglim": 20.320899963378906,
        "ant_survey": 2,
    }


def test_compute_survey_specific_alert_properties():
    event = build_ztf_event(
        candidate_kwargs={
            "test1": "test1",
            "test2": "test2",
            "test3": "test3",
        }
    )
    properties = compute_survey_specific_alert_properties(event["candidate"], event)
    assert "ztf_test1" in properties
    assert "ztf_test2" in properties
    assert "ztf_test3" in properties


def test_compute_survey_specific_alert_properties_ignores_items_with_none_value():
    event = build_ztf_event(
        candidate_kwargs={
            "test1": "test1",
            "test2": None,
        }
    )
    properties = compute_survey_specific_alert_properties(event["candidate"], event)
    assert "ztf_test1" in properties
    assert "ztf_test2" not in properties


def test_compute_survey_specific_alert_properties_adds_ztf_object_id():
    event = build_ztf_event(objectId="ztf-001")
    properties = compute_survey_specific_alert_properties(event["candidate"], event)
    assert properties["ztf_object_id"] == "ztf-001"


def test_create_alert_from_ztf_candidate_has_none_location_for_upper_limit():
    event = build_ztf_event(prv_candidate_kwargs=[EXAMPLE_UPPER_LIMIT])
    alert = create_alert_from_ztf_candidate(event["prv_candidates"][0], event)
    assert alert.location is None


def test_create_alert_from_ztf_candidate_has_proper_location_for_measurement():
    event = build_ztf_event(
        candidate_kwargs={
            **EXAMPLE_CANDIDATE,
            "ra": 10.0,
            "dec": 10.0,
        }
    )
    alert = create_alert_from_ztf_candidate(event["candidate"], event)
    assert alert.location.separation(SkyCoord("10d 10d")) < Angle(1e-15, unit="deg")


def test_create_alert_from_ztf_candidate_with_scientific_notation():
    event = build_ztf_event(
        candidate_kwargs={
            **EXAMPLE_CANDIDATE,
            "ra": -7e-07,
            "dec": 10.0,
        }
    )
    alert = create_alert_from_ztf_candidate(event["candidate"], event)
    assert alert.location.separation(SkyCoord(ra=-7e-07, dec=10, unit="deg")) < Angle(
        1e-15, unit="deg"
    )


@pytest.mark.parametrize(
    "event,expected_value",
    [
        (build_ztf_event(), []),
        (
            build_ztf_event(fp_hist_kwargs=[build_ztf_event_fp_hist()]),
            [build_ztf_event_fp_hist()],
        ),
    ],
)
def test_create_alert_that_includes_fp_hists(event, expected_value):
    alert = create_alert_from_ztf_candidate(event["candidate"], event)
    assert "ztf_fp_hists" in alert.non_persistent_properties
    assert alert.non_persistent_properties["ztf_fp_hists"] == expected_value


def test_is_measurement_for_measurement():
    assert is_measurement(EXAMPLE_CANDIDATE)


def test_is_measurement_for_upper_limit():
    assert not is_measurement(EXAMPLE_UPPER_LIMIT)


def test_get_alert_id_from_ztf_candidate_for_measurement():
    event = build_ztf_event(
        candidate_kwargs={**EXAMPLE_CANDIDATE, "candid": "alert-001"},
    )
    assert (
        get_alert_id_from_ztf_candidate(event["candidate"], event)
        == "ztf_candidate:alert-001"
    )


def test_get_alert_id_from_ztf_candidate_for_upper_limit():
    event = build_ztf_event(
        candidate_kwargs={**EXAMPLE_CANDIDATE, "candid": "alert-001"},
        prv_candidate_kwargs=[{**EXAMPLE_UPPER_LIMIT, "pid": "prv-candidate-001"}],
        objectId="ztf-001",
    )
    assert (
        get_alert_id_from_ztf_candidate(event["prv_candidates"][0], "ztf-001")
        == "ztf_upper_limit:ztf-001-prv-candidate-001"
    )


def test_ztf_alert_packet_ignores_none_values_when_building_alert_properties():
    event = build_ztf_event(
        candidate_kwargs={
            **EXAMPLE_CANDIDATE,
            "ndethist": "some",
            "candid": "alert-001",
        },
    )
    alert = create_alert_from_ztf_candidate(event["candidate"], event)
    assert "ztf_ndethist" in alert.properties
    event = build_ztf_event(
        candidate_kwargs={**EXAMPLE_CANDIDATE, "ndethist": None, "candid": "alert-001"},
    )
    alert = create_alert_from_ztf_candidate(event["candidate"], event)
    assert "ztf_ndethist" not in alert.properties


@pytest.mark.parametrize(
    ("key,val"),
    [
        ("rb", 0.34),
        ("nbad", 1),
        ("magdiff", -1.1),
        ("magdiff", 1.1),
    ],
)
def test_ztf_alert_packet_finds_bad_detection(key, val):
    alert_packet = build_ztf_alert_packet()
    assert alert_packet.is_good
    alert_packet = build_ztf_alert_packet(candidate_kwargs={key: val})
    assert not alert_packet.is_good


@pytest.mark.parametrize(
    ("key,val"),
    [
        ("fwhm", 5.1),
        ("elong", 1.3),
    ],
)
def test_ztf_alert_packet_finds_bad_seeing(key, val):
    alert_packet = build_ztf_alert_packet()
    assert alert_packet.is_good
    alert_packet = build_ztf_alert_packet(candidate_kwargs={key: val})
    assert not alert_packet.is_good


EXAMPLE_UPPER_LIMIT = {
    "jd": 2458486.9737037,
    "fid": 2,
    "pid": 732473703215,
    "diffmaglim": 20.320899963378906,
    "pdiffimfilename": "/ztf/archive/sci/2019/0103/473704/ztf_20190103473704_000568_zr_c09_o_q1_scimrefdiffimg.fits.fz",
    "programpi": "Kulkarni",
    "programid": 1,
    "candid": None,
    "isdiffpos": None,
    "tblid": None,
    "nid": 732,
    "rcid": 32,
    "field": 568,
    "xpos": None,
    "ypos": None,
    "ra": None,
    "dec": None,
    "magpsf": None,
    "sigmapsf": None,
    "chipsf": None,
    "magap": None,
    "sigmagap": None,
    "distnr": None,
    "magnr": None,
    "sigmagnr": None,
    "chinr": None,
    "sharpnr": None,
    "sky": None,
    "magdiff": None,
    "fwhm": None,
    "classtar": None,
    "mindtoedge": None,
    "magfromlim": None,
    "seeratio": None,
    "aimage": None,
    "bimage": None,
    "aimagerat": None,
    "bimagerat": None,
    "elong": None,
    "nneg": None,
    "nbad": None,
    "rb": None,
    "ssdistnr": None,
    "ssmagnr": None,
    "ssnamenr": None,
    "sumrat": None,
    "magapbig": None,
    "sigmagapbig": None,
    "ranr": None,
    "decnr": None,
    "scorr": None,
    "magzpsci": 26.107200622558594,
    "magzpsciunc": 8.903060006559826e-06,
    "magzpscirms": 0.027497300878167152,
    "clrcoeff": 0.10230900347232819,
    "clrcounc": 1.3531400327337906e-05,
    "rbversion": "t12_f5_c3",
}


EXAMPLE_CANDIDATE = {
    "aimage": 0.5820000171661377,
    "aimagerat": 0.24871794879436493,
    "bimage": 0.4449999928474426,
    "bimagerat": 0.19017094373703003,
    "candid": 674319732115015010,
    "chinr": 0.5289999842643738,
    "chipsf": 69.77619171142578,
    "classtar": 0.9990000128746033,
    "clrcoeff": -0.04871600121259689,
    "clrcounc": 8.626900125818793e-06,
    "clrmed": 0.6990000009536743,
    "clrrms": 0.1475829929113388,
    "dec": 54.5418511,
    "decnr": 54.5418002,
    "diffmaglim": 20.72220802307129,
    "distnr": 0.4988855719566345,
    "distpsnr1": 0.4565459191799164,
    "distpsnr2": 0.4946655035018921,
    "distpsnr3": 2.1048340797424316,
    "dsdiff": -204.67037963867188,
    "dsnrms": 137.41702270507812,
    "elong": 1.3078651428222656,
    "exptime": 30.0,
    "fid": 1,
    "field": 780,
    "fwhm": 2.3399999141693115,
    "isdiffpos": "t",
    "jd": 2458428.8197338,
    "jdendhist": 2458428.8197338,
    "jdendref": 2458422.947789,
    "jdstarthist": 2458427.8228356,
    "jdstartref": 2458168.6075,
    "magap": 17.016700744628906,
    "magapbig": 17.02429962158203,
    "magdiff": 0.5620179772377014,
    "magfromlim": 3.7055084705352783,
    "maggaia": 11.141946792602539,
    "maggaiabright": 11.141946792602539,
    "magnr": 13.420999526977539,
    "magpsf": 16.454681396484375,
    "magzpsci": 26.223682403564453,
    "magzpscirms": 0.021191999316215515,
    "magzpsciunc": 4.826999884244287e-06,
    "mindtoedge": 849.802978515625,
    "nbad": 0,
    "ncovhist": 28,
    "ndethist": 2,
    "neargaia": 0.49466580152511597,
    "neargaiabright": 0.49466580152511597,
    "nframesref": 15,
    "nid": 674,
    "nmatches": 2115,
    "nmtchps": 12,
    "nneg": 7,
    "objectidps1": 173450825830840687,
    "objectidps2": 173450825825740867,
    "objectidps3": 173450825832321700,
    "pdiffimfilename": "ztf_20181106319711_000780_zg_c06_o_q2_scimrefdiffimg.fits",
    "pid": 674319732115,
    "programid": 1,
    "programpi": "Kulkarni",
    "ra": 82.5827759,
    "ranr": 82.5830012,
    "rb": 0.2800000011920929,
    "rbversion": "t8_f5_c3",
    "rcid": 21,
    "rfid": 780120121,
    "scorr": 8.82487869262695,
    "seeratio": 1.2083905935287476,
    "sgmag1": 13.406000137329102,
    "sgmag2": 13.309399604797363,
    "sgmag3": -999.0,
    "sgscore1": 0.5,
    "sgscore2": 1.0,
    "sgscore3": 0.5,
    "sharpnr": -0.02800000086426735,
    "sigmagap": 0.03680000081658363,
    "sigmagapbig": 0.03830000013113022,
    "sigmagnr": 0.013000000268220901,
    "sigmapsf": 0.18503333628177643,
    "simag1": 10.824000358581543,
    "simag2": 10.95259952545166,
    "simag3": 13.768799781799316,
    "sky": 0.008049757219851017,
    "srmag1": 11.706000328063965,
    "srmag2": 12.448200225830078,
    "srmag3": -999.0,
    "ssdistnr": -999.0,
    "ssmagnr": -999.0,
    "ssnamenr": "null",
    "ssnrms": 342.08740234375,
    "sumrat": 0.7933757305145264,
    "szmag1": 10.222000122070312,
    "szmag2": 10.13860034942627,
    "szmag3": -999.0,
    "tblid": 10,
    "tooflag": 0,
    "xpos": 2222.697021484375,
    "ypos": 1099.9720458984375,
    "zpclrcov": -6.329999905574368e-06,
    "zpmed": 26.190000534057617,
}
