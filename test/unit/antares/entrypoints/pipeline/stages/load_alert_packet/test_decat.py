from datetime import datetime
from test.fakes import build_decat_alert_packet, build_decat_event
from typing import Optional
from unittest.mock import patch

import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import Locus
from antares.entrypoints.pipeline.stages.load_alert_packet.decat import (
    compute_normalized_alert_properties,
    compute_survey_specific_alert_properties,
    create_alert_from_decat_candidate,
)
from antares.utils import jd_to_mjd


def test_decat_alert_packet_raises_value_error_if_assocation_radius_is_greater_than_search_radius():
    with pytest.raises(ValueError):
        build_decat_alert_packet(
            locus_association_radius=Angle("2d"),
            locus_association_search_radius=Angle("1d"),
        )


@pytest.mark.with_data(Locus("locus-001", SkyCoord("10d 0d")))
def test_decat_alert_packet_get_associated_locus_returns_none_if_nothing_close(
    locus_repository,
    metrics,
):
    alert_packet = build_decat_alert_packet(sources_kwargs=[{"ra": 0.0, "dec": 0.0}])
    locus: Optional[Locus] = alert_packet.get_associated_locus(
        locus_repository, metrics
    )
    assert locus is None


@pytest.mark.with_data(Locus("locus-001", SkyCoord("1d 0d")))
def test_decat_alert_packet_get_associated_locus_returns_locus_within_association_radius(
    locus_repository, metrics
):
    alert_packet = build_decat_alert_packet(
        locus_association_radius=Angle("2d"),
        locus_association_search_radius=Angle("2d"),
        sources_kwargs=[{"ra": 0.0, "dec": 0.0}],
    )
    locus: Optional[Locus] = alert_packet.get_associated_locus(
        locus_repository, metrics
    )
    assert locus == locus_repository.get("locus-001")


@pytest.mark.with_data(
    Locus("locus-001", SkyCoord("1d 0d")),
    Locus(
        "locus-002",
        SkyCoord("2d 0d"),
        properties={"survey": {"decat": {"id": ["decat-001"]}}},
    ),
)
def test_decat_alert_packet_get_associated_locus_prefers_locus_with_matching_decat_object_id_even_if_other_is_closer(
    locus_repository, metrics
):
    alert_packet = build_decat_alert_packet(
        locus_association_radius=Angle("3d"),
        locus_association_search_radius=Angle("3d"),
        object_kwargs={"objectid": "decat-001"},
        sources_kwargs=[{"ra": 0.0, "dec": 0.0}],
    )
    locus: Optional[Locus] = alert_packet.get_associated_locus(
        locus_repository, metrics
    )
    assert locus == locus_repository.get("locus-002")


def test_decat_alert_packet_computes_location_as_alert_centroid():
    alert_packet = build_decat_alert_packet(
        sources_kwargs=[
            {"ra": 10.0, "dec": 0.0},
            {"ra": 5.0, "dec": 0.0},
        ],
    )
    assert alert_packet.location.separation(SkyCoord("7.5d 0d")) < Angle(
        1e-15, unit="deg"
    )


@pytest.mark.xfail
@patch("antares.service_layer.handlers.process_survey_message.decat.datetime")
def test_decat_alert_packet_computes_correct_normalized_candidate_alert_properties(
    mock_datetime,
):
    mock_datetime.utcnow.return_value = datetime(2021, 1, 1)
    # EXAMPLE_CANDIDATE doesn't exist in this file
    event = build_decat_event(sources_kwargs=[{**EXAMPLE_CANDIDATE}])
    raise NotImplementedError
    # These values are still from the old ZTF alert we copied the test from
    assert compute_normalized_alert_properties(event["sources"][0]) == {
        "ant_ra": 82.5827759,
        "ant_dec": 54.5418511,
        "ant_mjd": jd_to_mjd(2458428.8197338),
        "ant_time_received": round(datetime(2021, 1, 1).timestamp()),
        "ant_input_msg_time": round(datetime(2021, 1, 1).timestamp()),
        "ant_mag": 16.454681396484375,
        "ant_magerr": 0.18503333628177643,
        "ant_passband": "g",
        "ant_maglim": 20.72220802307129,
        "ant_survey": 1,
    }


def test_compute_survey_specific_alert_properties():
    event = build_decat_event(
        sources_kwargs=[
            {
                "test1": "test1",
                "test2": "test2",
                "test3": "test3",
            }
        ]
    )
    properties = compute_survey_specific_alert_properties(event["sources"][0], event)
    assert "decat_test1" in properties
    assert "decat_test2" in properties
    assert "decat_test3" in properties


def test_compute_survey_specific_alert_properties_ignores_items_with_none_value():
    event = build_decat_event(sources_kwargs=[{"test1": "test1", "test2": None}])
    properties = compute_survey_specific_alert_properties(event["sources"][0], event)
    assert "decat_test1" in properties
    assert "decat_test2" not in properties


def test_compute_survey_specific_alert_properties_adds_decat_object_id():
    event = build_decat_event(
        object_kwargs={"objectid": "decat-001"}, sources_kwargs=[{}]
    )
    properties = compute_survey_specific_alert_properties(event["sources"][0], event)
    assert properties["decat_object_id"] == "decat-001"


def test_create_alert_from_decat_candidate_has_proper_location_for_measurement():
    event = build_decat_event(sources_kwargs=[{"ra": 10.0, "dec": 10.0}])
    alert = create_alert_from_decat_candidate(event["sources"][0], event)
    assert alert.location.separation(SkyCoord("10d 10d")) < Angle(1e-15, unit="deg")


def test_decat_alert_packet_ignores_none_values_when_building_alert_properties():
    event = build_decat_event(
        sources_kwargs=[{"ndethist": "some", "candid": "alert-001"}],
    )
    alert = create_alert_from_decat_candidate(event["sources"][0], event)
    assert "decat_ndethist" in alert.properties
    event = build_decat_event(
        sources_kwargs=[{"ndethist": None, "candid": "alert-001"}],
    )
    alert = create_alert_from_decat_candidate(event["sources"][0], event)
    assert "decat_ndethist" not in alert.properties
