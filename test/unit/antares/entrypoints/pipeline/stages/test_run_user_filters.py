import logging
from test.fakes import build_alert, build_filter, build_filter_revision_raises_exception

import pytest
from astropy.coordinates import SkyCoord

from antares.domain.models import FilterRevisionStatus, Locus
from antares.entrypoints.pipeline.stages import run_user_filters, update_locus_history


@pytest.mark.usefixtures("message_bus")
class TestRunUserFilters:
    @pytest.mark.with_data(
        Locus("locus-001", location=SkyCoord("0d 0d")),
        build_filter(id=1, enabled_filter_revision_id=1),
        build_filter_revision_raises_exception(
            build_filter(id=1),
            id=1,
            status=FilterRevisionStatus.ENABLED,
        ),
    )
    def test_log_shows_entire_traceback_instead_of_object(
        self,
        alert_repository,
        locus_repository,
        caplog,
    ):
        with caplog.at_level(logging.WARNING):
            locus = locus_repository.get("locus-001")
            alert_repository.add(build_alert(), "locus-001")
            alert_history, timeseries = update_locus_history(locus)
            run_user_filters(locus, alert_history, timeseries, [])
            assert "<traceback object at" not in caplog.text
            assert "Filter Test Filter crashed on locus locus-001" in caplog.text
            assert "Exception: Oh No!" in caplog.text
