import pytest
from astropy.coordinates import SkyCoord

from antares.domain.models import Locus
from antares.entrypoints.pipeline.stages import publish_locus_to_users


@pytest.mark.usefixtures("message_bus")
class TestPublishLocusToUsers:
    # marshmallow_jsonapi.fields:155,165 needs to be updated
    @pytest.mark.filterwarnings("ignore:.*default:DeprecationWarning:marshmallow")
    @pytest.mark.override(
        {
            "pipeline": {
                "output": {
                    "triggers": [
                        {"name": "foo", "criteria": {"has_tag": "foo"}, "topic": "foo"},
                    ],
                }
            }
        }
    )
    def test_publish_locus_to_clients_sends_message_for_configured_tags(
        self,
        message_publication_service,
    ):
        locus = Locus("locus-001", SkyCoord("0d 0d"), tags={"foo"})
        publish_locus_to_users(locus)
        assert "client.foo" in message_publication_service.published
        assert len(message_publication_service.published["client.foo"]) == 1

    @pytest.mark.filterwarnings("ignore:.*default:DeprecationWarning:marshmallow")
    @pytest.mark.override({"pipeline": {"output": {"triggers": []}}})
    def test_publish_locus_to_clients_doesnt_send_message_for_unconfigured_tags(
        self,
        message_publication_service,
    ):
        locus = Locus("locus-001", SkyCoord("0d 0d"), tags={"foo"})
        publish_locus_to_users(locus)
        assert "client.foo" not in message_publication_service.published
