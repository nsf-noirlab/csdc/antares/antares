from test.fakes import FakeAlertPacket, build_alert, build_alerts

import pytest
from astropy.coordinates import SkyCoord
from dependency_injector import providers

from antares.domain.models import Locus
from antares.entrypoints.pipeline.stages import ingest_alert_packet
from antares.exceptions import KeyViolationException


@pytest.fixture(autouse=True)
def m_bus(message_bus):
    yield message_bus


class TestIngestAlertPacket:
    def test_ingest_alert_packet_handles_locus_id_clashes(
        self, container, alert_repository
    ):
        # Create a set of alerts that is large enough we can split in two. We use the
        # build_alerts function from the test fakes so that all of our alerts have unique
        # IDs. alerts1 represents the alerts on our first locus and alerts2 the alerts on
        # our second.
        alerts = build_alerts(20)
        alerts1, alerts2 = alerts[0:10], alerts[10:]

        # Assume that we generated the locus ID "locus-001" for this first locus. It should
        # save properly and all of the alerts will be stored in the database.
        with container.locus_id_factory.override(
            providers.Callable(lambda _: "locus-001")
        ):
            alert_packet1 = FakeAlertPacket(alerts1, centroid=SkyCoord("0d 0d"))
            ingest_alert_packet(alert_packet1)
            for alert in alerts1:
                assert alert_repository.get(alert.id) is not None

        # Attempt to create second locus. If the random ID generation routine generates a
        # clashing ID the handler should raise an `KeyViolationException` and none
        # of the alerts should be saved.
        with container.locus_id_factory.override(
            providers.Callable(lambda _: "locus-001")
        ):
            alert_packet2 = FakeAlertPacket(alerts2, centroid=SkyCoord("1d 1d"))
            with pytest.raises(KeyViolationException):
                ingest_alert_packet(alert_packet2)
            for alert in alerts2:
                assert alert_repository.get(alert.id) is None

        # If we try again and the locus ID generator is able to create a unique ID the
        # locus creation will succeed and all of the new alerts should be stored.
        with container.locus_id_factory.override(
            providers.Callable(lambda _: "locus-002")
        ):
            ingest_alert_packet(alert_packet2)
            for alert in alerts2:
                assert alert_repository.get(alert.id) is not None

    @pytest.mark.with_data(
        Locus("locus-001", SkyCoord("0d 0.8s")),
        Locus("locus-002", SkyCoord("0d 0.4s")),
    )
    def test_fake_alert_packet_associates_with_nearest_locus_within_one_arcsec(
        self, locus_repository, metrics
    ):
        alert_packet = FakeAlertPacket([], centroid=SkyCoord("0d 0d"))
        assert (
            alert_packet.get_associated_locus(locus_repository, metrics).id
            == "locus-002"
        )

    @pytest.mark.with_data(
        Locus("locus-001", SkyCoord("0d 1.8s")),
        Locus("locus-002", SkyCoord("0d 1.4s")),
    )
    def test_alert_packet_does_not_associate_if_all_loci_are_more_than_one_arcsec_away(
        self,
        locus_repository,
        metrics,
    ):
        alert_packet = FakeAlertPacket([], centroid=SkyCoord("0d 0d"))
        assert alert_packet.get_associated_locus(locus_repository, metrics) is None

    def test_ingests_all_alerts_in_alert_packet(self, alert_repository):
        alerts = build_alerts(10)
        alert_packet = FakeAlertPacket(alerts, centroid=SkyCoord("0d 0d"))
        ingest_alert_packet(alert_packet)
        for alert in alerts:
            assert alert_repository.get(alert.id) is not None

    @pytest.mark.with_data(Locus("locus-001", SkyCoord("0d 0.2s")))
    def test_ingest_alert_packet_associates_with_correct_locus_if_it_exists(
        self,
        alert_repository,
    ):
        alerts = build_alerts(1)
        alert_packet = FakeAlertPacket(alerts, centroid=SkyCoord("0s 0s"))
        ingest_alert_packet(alert_packet)
        assert list(alert_repository.list_by_locus_id("locus-001")) == alerts

    @pytest.mark.with_data(Locus("locus-001", SkyCoord("0d 0d")))
    def test_ingest_alert_packet_creates_new_locus_if_none_found(
        self,
        alert_repository,
    ):
        alerts = [build_alert(location=SkyCoord("10s 0s"))]
        alert_packet = FakeAlertPacket(alerts, centroid=SkyCoord("10s 0s"))
        ingest_alert_packet(alert_packet)
        # Suffices to check that this alert was persisted and that it was NOT
        # associated to the only locus that existed before ingestion.
        assert alert_repository.get(alerts[0].id) is not None
        assert list(alert_repository.list_by_locus_id("locus-001")) == []
