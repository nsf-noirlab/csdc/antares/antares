from datetime import datetime, timedelta
from test.fakes import (
    FakeAlertPacket,
    FakeGravWaveRepository,
    build_alerts,
    build_locus,
    build_notice,
)

import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord
from pytest import approx

from antares.domain.models.alert import AlertGravWaveEvent
from antares.domain.models.notice import GravWaveNotice
from antares.entrypoints.pipeline.stages import (
    add_grav_wave_events_to_alert_packet,
    add_grav_wave_events_to_locus,
)
from antares.utils import dt_to_mjd


@pytest.fixture
def fake_grav_wave_repo():
    repo = FakeGravWaveRepository()
    notice = build_notice()
    repo.add(notice)
    repo.current_notices.append(notice)
    return repo


class TestAddGravWaveEventsToAlertPacket:
    def test_doesnt_add_prop_if_outside_region(self, fake_grav_wave_repo):
        mjd = dt_to_mjd(datetime.utcnow())
        alerts = build_alerts(4, mjd=mjd)
        alert_packet = FakeAlertPacket(alerts, centroid=SkyCoord("0d 0d"))
        alert = alert_packet.triggering_alert
        add_grav_wave_events_to_alert_packet(
            alert_packet, fake_grav_wave_repo
        )
        assert alert.grav_wave_events == []

    def test_adds_prop_if_inside_regions(self, fake_grav_wave_repo):
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        mjd = dt_to_mjd(datetime.utcnow())
        alerts = build_alerts(4, location=location, mjd=mjd)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        alert = alert_packet.triggering_alert
        add_grav_wave_events_to_alert_packet(
            alert_packet, fake_grav_wave_repo
        )
        assert len(alert.grav_wave_events) == 1

    def test_use_packet_location(self, fake_grav_wave_repo):
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        mjd = dt_to_mjd(datetime.utcnow())
        alerts = build_alerts(4, location=None, mjd=mjd)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        alert = alert_packet.triggering_alert
        add_grav_wave_events_to_alert_packet(
            alert_packet, fake_grav_wave_repo
        )
        assert alert.location is None
        assert len(alert.grav_wave_events) == 1

    def test_packet_timestamp_before_notice(self, fake_grav_wave_repo):
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        mjd = dt_to_mjd(datetime.utcnow() - timedelta(days=14))
        alerts = build_alerts(4, location=location, mjd=mjd)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        alert = alert_packet.triggering_alert
        add_grav_wave_events_to_alert_packet(alert_packet, fake_grav_wave_repo)
        assert len(alert.grav_wave_events) == 0

    def test_grav_wave_event_is_alert_grav_wave_event(self, fake_grav_wave_repo):
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        notices = fake_grav_wave_repo.get_current_notices()
        mjd = dt_to_mjd(datetime.utcnow())
        alerts = build_alerts(4, location=location, mjd=mjd)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        alert = alert_packet.triggering_alert
        add_grav_wave_events_to_alert_packet(
            alert_packet, fake_grav_wave_repo
        )
        grav_wave_events = alert.grav_wave_events
        assert len(grav_wave_events) == 1
        grav_wave_event = grav_wave_events[0]
        assert isinstance(grav_wave_event, dict)
        assert grav_wave_event["gracedb_id"] == notices[0].gracedb_id
        assert grav_wave_event["contour_level"] == approx(76.6260, 0.0001)
        assert grav_wave_event["contour_area"] == approx(18.8513, 0.0001)

    def test_two_grav_wave_events_can_be_added(self, fake_grav_wave_repo):
        second_notice = build_notice(superevent_id="MS111111ab")
        fake_grav_wave_repo.add(second_notice)
        fake_grav_wave_repo.current_notices.append(second_notice)
        notices = fake_grav_wave_repo.get_current_notices()
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        mjd = dt_to_mjd(datetime.utcnow())
        alerts = build_alerts(4, location=location, mjd=mjd)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        alert = alert_packet.triggering_alert
        add_grav_wave_events_to_alert_packet(
            alert_packet, fake_grav_wave_repo
        )
        grav_wave_events = alert.grav_wave_events
        assert len(grav_wave_events) == 2
        returned_ids = {event["gracedb_id"] for event in grav_wave_events}
        notice_ids = {notice.gracedb_id for notice in notices}
        assert returned_ids == notice_ids

    def test_returns_events_metadata(self, fake_grav_wave_repo):
        second_notice = build_notice(superevent_id="MS111111ab")
        fake_grav_wave_repo.add(second_notice)
        fake_grav_wave_repo.current_notices.append(second_notice)
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        mjd = dt_to_mjd(datetime.utcnow())
        alerts = build_alerts(4, location=location, mjd=mjd)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        events_metadata = add_grav_wave_events_to_alert_packet(
            alert_packet, fake_grav_wave_repo
        )
        assert isinstance(events_metadata, dict)
        assert len(events_metadata) == 2
        event2 = events_metadata["MS111111ab"]
        assert isinstance(event2, GravWaveNotice)
        assert event2 is not second_notice
        assert event2.full_notice is not second_notice.full_notice
        assert event2.skymap_base64 is None
        assert second_notice.skymap_base64 is not None


class TestAddGravWaveEventsToLocus:

    def test_with_no_events(self):
        alerts = build_alerts(4)
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        locus = build_locus()
        add_grav_wave_events_to_locus(alert_packet, locus)
        assert len(locus.grav_wave_events) == 0

    def test_with_events(self):
        alerts = build_alerts(4)
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        alert_packet = FakeAlertPacket(alerts, centroid=location)
        grav_wave_event_1 = AlertGravWaveEvent(
            gracedb_id="ID-1",
            contour_level=60.1,
            contour_area=57.2)
        grav_wave_event_2 = AlertGravWaveEvent(
            gracedb_id="ID-2",
            contour_level=62.1,
            contour_area=59.2)
        alert_packet.triggering_alert.upsert_grav_wave_event(grav_wave_event_1)
        alert_packet.triggering_alert.upsert_grav_wave_event(grav_wave_event_2)
        locus = build_locus()
        add_grav_wave_events_to_locus(alert_packet, locus)
        assert len(locus.grav_wave_events) == 2
        assert "ID-1" in locus.grav_wave_events
        assert "ID-2" in locus.grav_wave_events
