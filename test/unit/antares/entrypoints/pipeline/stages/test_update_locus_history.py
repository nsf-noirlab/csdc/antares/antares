import traceback
import uuid
from test.fakes import (
    build_alert,
    build_alerts,
    build_filter,
    build_filter_revision_crashes_on_setup,
    build_filter_revision_raises_exception,
    build_filter_revision_sets_property,
    build_filter_revision_sets_tag,
    build_locus,
)
from unittest.mock import patch

import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import (
    Filter,
    FilterConstraints,
    FilterContext,
    FilterExecutable,
    FilterOutputSpecification,
    FilterOutputSpecificationProperty,
    FilterRevisionStatus,
    Locus,
    Survey,
    WatchList,
    WatchObject,
)
from antares.domain.models.filter import (
    FilterErrored,
    FilterSucceeded,
    run_filter_against_locus,
)
from antares.entrypoints.pipeline.stages import (
    crossmatch_locus_with_watchlists,
    run_user_filters,
    update_locus_history,
)
from antares.entrypoints.pipeline.stages.update_locus_history import (
    compute_survey_normalized_properties,
    compute_survey_specific_properties,
    compute_timeseries,
    get_surveys,
)


@pytest.mark.usefixtures("message_bus")
class TestUpdateLocusHistory:
    @pytest.mark.with_data(Locus("locus-001", SkyCoord("0s 0s")))
    def test_update_locus_computes_new_centroid(
        self, alert_repository, locus_repository
    ):
        locus = locus_repository.get("locus-001")
        # Let's say we first received an alert at 0, 0 and it's already been processed
        alert_repository.add(build_alert(location=SkyCoord("0s 0s")), "locus-001")
        # Later we receive an alert at 0.5, 0
        alert_repository.add(build_alert(location=SkyCoord("0.5s 0s")), "locus-001")
        # When the update_locus handler is called it should compute and persist a new centroid
        # location for this object.
        update_locus_history(locus)
        assert locus.location.separation(SkyCoord("0.25s 0s")) < Angle(
            1e-15, unit="arcsec"
        )

    @pytest.mark.with_data(
        Locus("locus-001", location=SkyCoord("0d 0d")),
        build_filter(id=1, enabled_filter_revision_id=1),
        build_filter(id=2, enabled_filter_revision_id=2),
        build_filter_revision_sets_property(
            build_filter(id=1),
            "key",
            "val",
            id=1,
            status=FilterRevisionStatus.ENABLED,
        ),
        build_filter_revision_sets_tag(
            build_filter(id=2),
            "tag",
            id=2,
            status=FilterRevisionStatus.ENABLED,
        ),
    )
    def test_update_locus_runs_filters_correctly(
        self, alert_repository, locus_repository, filter_repository
    ):
        filter_1 = filter_repository.get(1)
        filter_2 = filter_repository.get(2)
        assert filter_1.enabled
        assert filter_2.enabled
        locus = locus_repository.get("locus-001")
        for alert in build_alerts(10):
            alert_repository.add(alert, "locus-001")
        alert_history, timeseries = update_locus_history(locus)
        run_user_filters(locus, alert_history, timeseries, [])
        assert locus.properties.get("key") == "val"
        assert "tag" in locus.tags

    @patch(
        "antares.adapters.notifications.AbstractInternalNotificationService",
        autospec=True,
    )
    @pytest.mark.with_data(
        Locus("locus-001", location=SkyCoord("0d 0d")),
        build_filter(id=1, enabled_filter_revision_id=1),
        build_filter_revision_raises_exception(
            build_filter(id=1),
            id=1,
            status=FilterRevisionStatus.ENABLED,
        ),
    )
    def test_failing_filter_gets_disabled_and_notifies_staff(
        self,
        mock_notifier,
        container,
        alert_repository,
        locus_repository,
        filter_repository,
        filter_revision_repository,
    ):
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        filter_ = filter_repository.get(1)
        assert filter_.enabled
        locus = locus_repository.get("locus-001")
        for alert in build_alerts(10):
            alert_repository.add(alert, "locus-001")
        alert_history, timeseries = update_locus_history(locus)
        run_user_filters(locus, alert_history, timeseries, [])
        filter_ = filter_repository.get(1)
        filter_revision = filter_revision_repository.get(1)
        assert not filter_.enabled
        assert filter_revision.status == FilterRevisionStatus.DISABLED
        assert filter_revision.feedback.startswith(
            "Disabled automatically by the pipeline"
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_called_once()
        assert (
            mock_notifier.notify_after_filter_revision_transitioned.call_args.kwargs.get(  # noqa: E501
                "custom_message"
            )
            is not None
        )

    @pytest.mark.with_data(Locus("locus-001", location=SkyCoord("0d 0d")))
    def test_update_survey_specific_properties_stores_ztf_object_id_on_locus(
        self, alert_repository, locus_repository
    ):
        locus = locus_repository.get("locus-001")
        alert_repository.add(
            build_alert(properties={"ztf_object_id": "ztf-001"}), "locus-001"
        )
        update_locus_history(locus)
        assert locus.properties["ztf_object_id"] == "ztf-001"

    @pytest.mark.with_data(Locus("locus-001", location=SkyCoord("0d 0d")))
    def test_update_survey_specific_properties_stores_latest_ztf_object_id_on_locus(
        self, alert_repository, locus_repository
    ):
        locus = locus_repository.get("locus-001")
        for alert in [
            build_alert(properties={"ztf_object_id": "ztf-001"}),
            build_alert(properties={"ztf_object_id": "ztf-002"}),
        ]:
            alert_repository.add(alert, locus.id)
        update_locus_history(locus)
        assert locus.properties["ztf_object_id"] == "ztf-002"

    @pytest.mark.with_data(Locus("locus-001", location=SkyCoord("0d 0d")))
    def test_update_survey_specific_properties_handles_missing_ztf_object_ids(
        self, alert_repository, locus_repository
    ):
        locus = locus_repository.get("locus-001")
        for alert in [
            build_alert(properties={"ztf_object_id": "ztf-001"}),
            build_alert(properties={}),
        ]:
            alert_repository.add(alert, "locus-001")
        update_locus_history(locus)
        assert locus.properties["ztf_object_id"] == "ztf-001"

    @pytest.mark.with_data(
        Locus("locus-001", SkyCoord("0d 0.0s")),
        Filter(
            "filter-001", owner_id=uuid.UUID(int=1), id=1, enabled_filter_revision_id=1
        ),
        build_filter_revision_crashes_on_setup(
            Filter("filter-001", owner_id=uuid.UUID(int=1), id=1)
        ),
    )
    def test_bad_filter_setup_doesnt_crash_pipeline(
        self, alert_repository, locus_repository
    ):
        locus = locus_repository.get("locus-001")
        for alert in build_alerts(10):
            alert_repository.add(alert, "locus-001")
        update_locus_history(locus)


@pytest.mark.with_data(
    Locus("locus-001", SkyCoord("0d 0d")),
    WatchList(
        id=uuid.UUID(int=1),
        owner_id=uuid.UUID(int=1),
        name="Test",
        description="",
        slack_channel="#errors_decat",
    ),
    WatchObject(
        id=uuid.UUID(int=2),
        watch_list_id=uuid.UUID(int=1),
        name="Test Object",
        location=SkyCoord("0d 0.5s"),
        radius=Angle("1s"),
    ),
)
def test_watch_list_association(message_bus, alert_repository, locus_repository):
    locus = locus_repository.get("locus-001")
    for alert in build_alerts(10):
        alert_repository.add(alert, "locus-001")
    assert len(locus.watch_object_matches) == 0
    crossmatch_locus_with_watchlists(locus)
    assert len(locus.watch_object_matches) == 1
    ((watch_list_id, watch_object_id),) = locus.watch_object_matches
    assert watch_list_id == uuid.UUID(int=1)
    assert watch_object_id == uuid.UUID(int=2)


def test_get_surveys():
    alerts = [build_alert(survey=Survey.SURVEY_ZTF)]
    assert get_surveys(alerts) == {Survey.SURVEY_ZTF}


def test_compute_survey_specific_properties_for_ztf():
    alerts = [
        build_alert(
            id="alert-001",
            survey=Survey.SURVEY_ZTF,
            mjd=59000,
            properties={
                "ant_mjd": 59000,
                "ztf_object_id": "ztf-001",
                "ztf_rcid": 123,
                "ztf_field": 456,
                "ztf_ssnamenr": "ssname-001",
            },
        ),
        build_alert(
            id="alert-002",
            survey=Survey.SURVEY_ZTF,
            mjd=5001,
            properties={
                "ant_mjd": 5001,
                "ztf_object_id": "ztf-001",
                "ztf_rcid": 123,
                "ztf_field": 457,
            },
        ),
        build_alert(
            id="alert-003",
            survey=Survey.SURVEY_ZTF,
            mjd=5002,
            properties={
                "ant_mjd": 5002,
                "ztf_object_id": "ztf-002",
                "ztf_rcid": 124,
                "ztf_field": 456,
                "ztf_ssnamenr": "ssname-002",
            },
        ),
    ]
    ztf_properties = compute_survey_specific_properties(alerts, Survey.SURVEY_ZTF)
    assert ztf_properties == {
        "survey": {
            "ztf": {
                "id": ["ztf-001", "ztf-002"],
                "rcid": [123, 124],
                "field": [456, 457],
                "ssnamenr": ["ssname-001", "ssname-002"],
            }
        }
    }


def test_compute_antares_managed_locus_properties():
    alerts = [
        build_alert(
            id="alert-001",
            normalized_properties={"ant_mag": 13.0, "ant_mjd": 59000},
            mjd=59000,
        ),
        build_alert(
            id="alert-002",
            normalized_properties={"ant_mag": None, "ant_mjd": 59001},
            mjd=59001,
        ),
        build_alert(
            id="alert-003",
            normalized_properties={"ant_mag": 10.0, "ant_mjd": 59002},
            mjd=59002,
        ),
        build_alert(
            id="alert-004",
            normalized_properties={"ant_mag": 14.0, "ant_mjd": 59003},
            mjd=59003,
        ),
    ]
    timeseries = compute_timeseries(alerts)
    locus_properties = compute_survey_normalized_properties(alerts, timeseries)
    assert locus_properties == {
        "brightest_alert_id": "alert-003",
        "brightest_alert_magnitude": 10.0,
        "brightest_alert_observation_time": 59002,
        "oldest_alert_id": "alert-001",
        "oldest_alert_magnitude": 13.0,
        "oldest_alert_observation_time": 59000,
        "newest_alert_id": "alert-004",
        "newest_alert_magnitude": 14.0,
        "newest_alert_observation_time": 59003,
        "num_alerts": 4,
        "num_mag_values": 3,
        "is_corrected": "false",
    }


def test_filter_can_run_and_set_locus_property_if_specified_as_output_property():
    def callable_(locus_adapter: FilterContext):
        locus_adapter.properties["foo"] = 100

    locus = build_locus(properties={})
    alerts = build_alerts(10)
    timeseries = compute_timeseries(alerts)
    filter_executable = FilterExecutable(
        filter_id=1,
        filter_revision_id=1,
        callable=callable_,
        constraints=FilterConstraints(),
        output_specification=FilterOutputSpecification(
            properties=[
                FilterOutputSpecificationProperty(
                    name="foo",
                    description="Some property called foo",
                    type=int,
                )
            ]
        ),
    )
    run_filter_against_locus(filter_executable, locus, alerts, timeseries, [])
    ret = run_filter_against_locus(filter_executable, locus, alerts, timeseries, [])
    assert isinstance(ret, FilterSucceeded)
    assert "foo" in locus.properties
    assert locus.properties["foo"] == 100


def test_filter_may_not_set_locus_property_if_not_specified_as_output_property():
    def callable_(locus_adapter: FilterContext):
        locus_adapter.properties["foo"] = 100

    locus = build_locus(properties={})
    alerts = build_alerts(10)
    timeseries = compute_timeseries(alerts)
    filter_ = FilterExecutable(
        filter_id=1,
        filter_revision_id=1,
        callable=callable_,
        constraints=FilterConstraints(),
        output_specification=FilterOutputSpecification(properties=[]),
    )
    ret = run_filter_against_locus(filter_, locus, alerts, timeseries, [])
    assert isinstance(ret, FilterErrored)
    assert any(
        (
            "not specified writeable" in frame
            for frame in traceback.format_tb(ret.traceback)
        )
    )
    assert "foo" not in locus.properties


def test_filter_can_run_and_set_tag_if_specified_as_output_tag():
    def callable_(locus_adapter: FilterContext):
        locus_adapter.tag("foo")

    locus = build_locus(tags=set())
    alerts = build_alerts(10)
    timeseries = compute_timeseries(alerts)
    filter_ = FilterExecutable(
        filter_id=1,
        filter_revision_id=1,
        callable=callable_,
        constraints=FilterConstraints(),
        output_specification=FilterOutputSpecification(tags={"foo"}),
    )
    run_filter_against_locus(filter_, locus, alerts, timeseries, [])
    assert "foo" in locus.tags


def test_filter_may_not_set_tag_if_not_specified_as_output_tag():
    def callable_(locus_adapter: FilterContext):
        locus_adapter.tag("foo")

    locus = build_locus(tags=set())
    alerts = build_alerts(10)
    print(alerts)
    timeseries = compute_timeseries(alerts)
    filter_ = FilterExecutable(
        filter_id=1,
        filter_revision_id=1,
        callable=callable_,
        constraints=FilterConstraints(),
        output_specification=FilterOutputSpecification(tags=[]),
    )
    ret = run_filter_against_locus(filter_, locus, alerts, timeseries, [])
    assert isinstance(ret, FilterErrored)
    assert any(
        (
            "not specified writeable" in frame
            for frame in traceback.format_exception(
                type(ret.exception), ret.exception, ret.traceback
            )
        )
    )
    assert "foo" not in locus.tags
