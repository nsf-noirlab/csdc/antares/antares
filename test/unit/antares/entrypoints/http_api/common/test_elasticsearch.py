from unittest.mock import patch

import marshmallow
import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord

from antares.entrypoints.http_api.common.elasticsearch import (
    CoordinateString,
    transform_dictionary,
    transform_sky_distance_query,
)

ELASTICSEARCH_MODULE = "antares.entrypoints.http_api.common.elasticsearch"


class TestCoordinateString:
    @pytest.mark.parametrize(
        "ra, dec",
        [
            (0, 0),
            (0, 90),
            (1.2, 3.4),
            (0.00000001, 0.00000005),
        ],
    )
    def test_round_trip(self, ra, dec):
        coord = SkyCoord(ra, dec, unit=u.deg)
        coord_string = CoordinateString().serialize("coord", {"coord": coord})
        result = CoordinateString().deserialize(coord_string)
        assert result == coord

    def test_colon_separated_coordinates_treated_as_hms_dms(self):
        coordinate_string = "11:19:50 70:6:10"
        coordinate = CoordinateString().deserialize(coordinate_string)
        assert coordinate.to_string() == SkyCoord("11h19m50s 70d6m10s").to_string()

    def test_unitless_coordinates_treated_as_degrees(self):
        coordinate_string = "10 20"
        coordinate = CoordinateString().deserialize(coordinate_string)
        assert coordinate.to_string() == SkyCoord("10d 20d").to_string()

    def test_different_formats_equivalent(self):
        expected_coordinate = SkyCoord("90d 0d")
        for coordinate_string in [
            "90.0 0.0",
            "90d 0d",
            "90d+0d",
            "90d00 0d00",
            "90d00m00s 00d00m00s",
            "06h00m00s +00d00m00s",
            "06h00m00s+00d00m00s",
            "06:00:00 00:00:00",
            "06:00:00+00:00:00",
        ]:
            coordinate = CoordinateString().deserialize(coordinate_string)
            assert coordinate.to_string() == expected_coordinate.to_string()


class TestTransformDictionary:
    def test_transform_dictionary_transforms_keys_in_list(self):
        d = {"a": "hello", "nested": [{"a": "hello"}]}
        transform_dictionary(d, "a", lambda v: v + ", world!")
        assert d == {"a": "hello, world!", "nested": [{"a": "hello, world!"}]}

    def test_transform_dictionary_transforms_nested_keys(self):
        d = {"a": "hello", "nested": {"a": "hello"}}
        transform_dictionary(d, "a", lambda v: v + ", world!")
        assert d == {"a": "hello, world!", "nested": {"a": "hello, world!"}}

    def test_transform_dictionary_only_transforms_specified_keys(self):
        d = {"a": "hello", "b": "goodbye"}
        transform_dictionary(d, "a", lambda v: v + ", world!")
        assert d == {"a": "hello, world!", "b": "goodbye"}

    def test_transform_dictionary_renames_key(self):
        d = {"a": "hello", "b": "goodbye"}
        transform_dictionary(d, "a", lambda v: v, new_key="c")
        assert d == {"c": "hello", "b": "goodbye"}


class TestTransformSkyDistanceQuery:
    @patch(f"{ELASTICSEARCH_MODULE}.htm.get_htm_circle_region")
    def test_transforms_query_to_htm_ranges(self, mock_get_htm_circle_region):
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} degree",
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        mock_get_htm_circle_region.return_value = [(100, 200), (300, 400)]
        transform_sky_distance_query(query, "field_name")
        assert query == {
            "filter": {
                "bool": {
                    "should": [
                        {
                            "range": {
                                "field_name": {
                                    "gte": 100,
                                    "lte": 200,
                                }
                            }
                        },
                        {
                            "range": {
                                "field_name": {
                                    "gte": 300,
                                    "lte": 400,
                                }
                            }
                        },
                    ]
                }
            }
        }

    @patch(f"{ELASTICSEARCH_MODULE}.htm.get_htm_circle_region")
    def test_transforms_query_to_htm_ranges_with_complex_query(
        self, mock_get_htm_circle_region
    ):
        query = {
            "filter": {
                "bool": {
                    "must": [
                        {"range": {"properties.ztf_dec": {"gte": 333, "lte": 333}}},
                        {
                            "sky_distance": {
                                "distance": f"{1.0/3600.0} degree",
                                "field_name": {"center": "20d 10d"},
                            },
                        },
                    ]
                },
            }
        }
        mock_get_htm_circle_region.return_value = [(100, 200), (300, 400)]
        transform_sky_distance_query(query, "field_name")
        assert query == {
            "filter": {
                "bool": {
                    "must": [
                        {"range": {"properties.ztf_dec": {"gte": 333, "lte": 333}}},
                        {
                            "bool": {
                                "should": [
                                    {
                                        "range": {
                                            "field_name": {
                                                "gte": 100,
                                                "lte": 200,
                                            }
                                        }
                                    },
                                    {
                                        "range": {
                                            "field_name": {
                                                "gte": 300,
                                                "lte": 400,
                                            }
                                        }
                                    },
                                ]
                            }
                        },
                    ]
                },
            }
        }

    def test_accepts_correct_format(self):
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} degree",
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        transform_sky_distance_query(query, "field_name")

    def test_raises_error_if_wrong_format(self):
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} degree",
                    "field_name": {"center": "20d 10no"},
                }
            }
        }
        with pytest.raises(marshmallow.ValidationError):
            transform_sky_distance_query(query, "field_name")

    def test_raises_error_if_cant_parse_distance_string(self):
        # Check wrong units
        query = {
            "filter": {
                "sky_distance": {
                    "distance": f"{1.0/3600.0} arcsec",  # not supported yet
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        with pytest.raises(marshmallow.ValidationError):
            transform_sky_distance_query(query, "field_name")
        # Check bad number string
        query = {
            "filter": {
                "sky_distance": {
                    "distance": "one degree",
                    "field_name": {"center": "20d 10d"},
                }
            }
        }
        with pytest.raises(marshmallow.ValidationError):
            transform_sky_distance_query(query, "field_name")
