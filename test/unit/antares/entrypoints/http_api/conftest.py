import uuid
from typing import NamedTuple

import pytest

from antares.domain.models import User
from antares.entrypoints.http_api.main import create_app


@pytest.fixture
def api_wrapper(container, message_bus):
    app = create_app(container)
    client = app.test_client()
    client.testing = True
    yield client, container


@pytest.fixture
def api_client(api_wrapper):
    api_client, _ = api_wrapper
    yield api_client


class Authentication(NamedTuple):
    user: User
    access_token: str


@pytest.fixture
def authentication(api_client, request, container) -> Authentication:
    marker = request.node.get_closest_marker("with_authenticated_user")
    defaults = {
        "id": uuid.UUID(int=1),
        "name": "User",
        "username": "user",
        "email": "user@noirlab.edu",
    }
    if marker:
        defaults.update(marker.kwargs)
    user = User(**defaults)
    user.set_password("password")
    container.user_repository().add(user)
    response = api_client.post(
        "/v1/auth/login",
        json={"username": "user", "password": "password"},
    )
    if response.status_code == 401:
        raise Exception("Unable to login as user")
    elif response.status_code != 200:
        raise Exception(response.json)
    yield Authentication(user, response.json["access_token"])
