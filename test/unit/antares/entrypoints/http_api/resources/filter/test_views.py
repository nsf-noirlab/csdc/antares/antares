import uuid
from test.unit.test_http_api.filter_test import (
    make_filter_patch_payload,
    make_filter_version_patch_payload,
)
from unittest.mock import patch

import pytest

from antares.domain.models import Filter, FilterRevision, FilterRevisionStatus

ABSTRACT_INTERNAL_NOTIFICATION_SERVICE_PATH = "antares.entrypoints.http_api.resources.filter.views.AbstractInternalNotificationService"  # noqa: E501


@patch(ABSTRACT_INTERNAL_NOTIFICATION_SERVICE_PATH, autospec=True)
class TestFilterDetailPatchResource:
    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.REVIEWED,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_successful_that_enables_filter_revision_notifies_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        enabled_filter_revision_id = "2"
        payload = make_filter_patch_payload(
            "1",
            {},
            filter_revision_id=enabled_filter_revision_id,
        )
        api_client.patch(
            "/v1/filters/1",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_called_once()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.REVIEWED,
        ),
    )
    def test_update_fails_by_unauthenticated_user_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        enabled_filter_revision_id = "2"
        payload = make_filter_patch_payload(
            "1",
            {},
            filter_revision_id=enabled_filter_revision_id,
        )
        api_client.patch("/v1/filters/1", json=payload)
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_update_fails_by_non_existent_filter_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        enabled_filter_revision_id = "2"
        payload = make_filter_patch_payload(
            "1",
            {},
            filter_revision_id=enabled_filter_revision_id,
        )
        api_client.patch(
            "/v1/filters/1",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.REVIEWED,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_update_fails_by_disallowed_fields_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        enabled_filter_revision_id = "2"
        payload = make_filter_patch_payload(
            "1",
            {
                "name": "Updated name",
                "description": "Updated description",
            },  # Some disallowed fields
            filter_revision_id=enabled_filter_revision_id,
        )
        api_client.patch(
            "/v1/filters/1",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.REVIEWED,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_update_successful_without_enabling_filter_revision_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_patch_payload("1", {"public": True})
        api_client.patch(
            "/v1/filters/1",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.REVIEWED,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_update_fails_by_unauthorized_user_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        enabled_filter_revision_id = "2"
        payload = make_filter_patch_payload(
            "1",
            {},
            filter_revision_id=enabled_filter_revision_id,
        )
        api_client.patch(
            "/v1/filters/1",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_fails_by_non_existent_filter_revision_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        enabled_filter_revision_id = "2"
        payload = make_filter_patch_payload(
            "1",
            {},
            filter_revision_id=enabled_filter_revision_id,
        )
        api_client.patch(
            "/v1/filters/1",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=2,
            id=2,
            code="",
            status=FilterRevisionStatus.REVIEWED,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_fails_by_wrong_filter_revision_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        enabled_filter_revision_id = "2"
        payload = make_filter_patch_payload(
            "1",
            {},
            filter_revision_id=enabled_filter_revision_id,
        )
        api_client.patch(
            "/v1/filters/1",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()


@patch(ABSTRACT_INTERNAL_NOTIFICATION_SERVICE_PATH, autospec=True)
class TestFilterVersionDetailPatchResource:
    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.PENDING_REVIEW,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_successful_notifies_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2", {"status": FilterRevisionStatus.REVIEWED.name}
        )
        api_client.patch(
            "/v1/filters/1/versions/2",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_called_once()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.PENDING_REVIEW,
        ),
    )
    def test_update_fails_by_unauthenticated_user_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2", {"status": FilterRevisionStatus.REVIEWED.name}
        )
        api_client.patch("/v1/filters/1/versions/2", json=payload)
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.PENDING_REVIEW,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_update_fails_by_unauthorized_user_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2", {"status": FilterRevisionStatus.REVIEWED.name}
        )
        api_client.patch(
            "/v1/filters/1/versions/2",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_fails_by_non_existent_filter_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2", {"status": FilterRevisionStatus.REVIEWED.name}
        )
        api_client.patch(
            "/v1/filters/1/versions/2",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_fails_by_non_existent_filter_revision_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2", {"status": FilterRevisionStatus.REVIEWED.name}
        )
        api_client.patch(
            "/v1/filters/1/versions/2",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=2,
            id=2,
            code="",
            status=FilterRevisionStatus.PENDING_REVIEW,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_fails_by_wrong_filter_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2", {"status": FilterRevisionStatus.REVIEWED.name}
        )
        api_client.patch(
            "/v1/filters/1/versions/2",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.PENDING_REVIEW,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_fails_by_disallowed_fields_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2",
            {
                "status": FilterRevisionStatus.REVIEWED.name,
                "code": "class MyFilter(dk.Filter)",  # Disallowed field
                "comment": "Updated comment",  # Disallowed field
            },
        )
        api_client.patch(
            "/v1/filters/1/versions/2",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()

    @pytest.mark.with_data(
        Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)),
        FilterRevision(
            filter_id=1,
            id=2,
            code="",
            status=FilterRevisionStatus.PENDING_REVIEW,
        ),
    )
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=True)
    def test_update_fails_by_disallowed_transition_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
        authentication,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        payload = make_filter_version_patch_payload(
            "2", {"status": FilterRevisionStatus.ENABLED.name}
        )
        api_client.patch(
            "/v1/filters/1/versions/2",
            json=payload,
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_transitioned.assert_not_called()


@patch(ABSTRACT_INTERNAL_NOTIFICATION_SERVICE_PATH, autospec=True)
class TestFilterVersionListCreateResource:
    @pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_creation_successful_notifies_staff(
        self, mock_notifier, api_wrapper, authentication
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        api_client.post(
            "/v1/filters/1/versions",
            json={
                "data": {
                    "type": "filter_version",
                    "attributes": {"code": "", "comment": "My Filter Version"},
                    "relationships": {
                        "filter": {"data": {"type": "filter", "id": "1"}}
                    },
                },
            },
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_created.assert_called_once()

    @pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
    def test_creation_fails_by_unauthenticated_user_does_not_notify_staff(
        self,
        mock_notifier,
        api_wrapper,
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        api_client.post(
            "/v1/filters/1/versions",
            json={
                "data": {
                    "type": "filter_version",
                    "attributes": {"code": "", "comment": "My Filter Version"},
                    "relationships": {
                        "filter": {"data": {"type": "filter", "id": "1"}}
                    },
                },
            },
        )
        mock_notifier.notify_after_filter_revision_created.assert_not_called()

    @pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_creation_fails_by_wrong_filter_does_not_notify_staff(
        self, mock_notifier, api_wrapper, authentication
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        api_client.post(
            "/v1/filters/1/versions",
            json={
                "data": {
                    "type": "filter_version",
                    "attributes": {"code": "", "comment": "My Filter Version"},
                    "relationships": {
                        "filter": {"data": {"type": "filter", "id": "2"}}
                    },
                },
            },
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_created.assert_not_called()

    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_creation_fails_by_non_existent_filter_does_not_notify_staff(
        self, mock_notifier, api_wrapper, authentication
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        api_client.post(
            "/v1/filters/1/versions",
            json={
                "data": {
                    "type": "filter_version",
                    "attributes": {"code": "", "comment": "My Filter Version"},
                    "relationships": {
                        "filter": {"data": {"type": "filter", "id": "1"}}
                    },
                },
            },
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_created.assert_not_called()

    @pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=2), staff=False, admin=False)
    def test_creation_fails_by_wrong_owner_does_not_notify_staff(
        self, mock_notifier, api_wrapper, authentication
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        api_client.post(
            "/v1/filters/1/versions",
            json={
                "data": {
                    "type": "filter_version",
                    "attributes": {"code": "", "comment": "My Filter Version"},
                    "relationships": {
                        "filter": {"data": {"type": "filter", "id": "1"}}
                    },
                },
            },
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_created.assert_not_called()

    @pytest.mark.with_data(Filter(id=1, name="Filter", owner_id=uuid.UUID(int=1)))
    @pytest.mark.with_authenticated_user(id=uuid.UUID(int=1), staff=False, admin=False)
    def test_creation_fails_by_disallowed_status_does_not_notify_staff(
        self, mock_notifier, api_wrapper, authentication
    ):
        api_client, container = api_wrapper
        # Container's internal_notification_service overriden with mock to assert calls
        mock_notifier.__enter__.return_value = mock_notifier
        container.internal_notification_service.override(mock_notifier)

        api_client.post(
            "/v1/filters/1/versions",
            json={
                "data": {
                    "type": "filter_version",
                    "attributes": {
                        "code": "",
                        "comment": "My Filter Version",
                        "status": FilterRevisionStatus.REVIEWED.name,
                    },
                    "relationships": {
                        "filter": {"data": {"type": "filter", "id": "1"}}
                    },
                },
            },
            headers={"Authorization": f"Bearer {authentication.access_token}"},
        )
        mock_notifier.notify_after_filter_revision_created.assert_not_called()
