import datetime
from dataclasses import dataclass
from decimal import Decimal

import pytest
import pytz

from antares.utils.json_encoder import json_dumps


@pytest.mark.parametrize(
    "value, expected",
    [
        (Decimal(10), '"10"'),
        (datetime.datetime(2022, 8, 31), '"2022-08-31T00:00:00+00:00"'),
        (
            datetime.datetime(2022, 8, 31, 0, 0, 0, 0, pytz.UTC),
            '"2022-08-31T00:00:00+00:00"',
        ),
        (datetime.date(2022, 8, 31), '"2022-08-31"'),
        ({"a": "b"}, '{"a": "b"}'),
        ("a", '"a"'),
    ],
)
def test_antares_json_dumps(value, expected):
    returned_value = json_dumps(value)
    assert isinstance(returned_value, str)
    assert returned_value == expected


def test_antares_json_encoder_passing_an_undefined_custom_class():
    @dataclass
    class CustomTestClass:
        var: str

    with pytest.raises(TypeError) as err:
        json_dumps(CustomTestClass("a"))
        assert "Object of type CustomTestClass is not JSON serializable" in err
