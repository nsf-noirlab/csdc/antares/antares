import datetime
import math
import tempfile
from test.fakes import build_filter_context, build_locus

import pytest
from astropy.coordinates import Angle, SkyCoord
from astropy.table import Table
from astropy.timeseries import TimeSeries
from astropy.utils.diff import report_diff_values

from antares.utils import (
    astropy_table_from_string,
    astropy_table_to_string,
    format_dt,
    get_median_coords,
    jd_to_mjd,
    mjd_to_datetime,
    nearest,
    unindent,
)


@pytest.fixture
def locus_with_lightcurve():
    ft = build_filter_context()
    locus = build_locus(lightcurve=ft.timeseries)
    return locus


def check_all_rows_and_columns_in_string(table: Table, string: str):
    header_as_string = ",".join(table.columns)
    assert header_as_string in string
    for row in table:
        row_as_string = ",".join(str(value) for value in row)
        row_as_string = row_as_string.replace("--", "")
        assert row_as_string in string
    return True


class TestAstropyTableToString:
    def test_astropy_table_to_string_excluding_time(self, locus_with_lightcurve):
        lightcurve_string = astropy_table_to_string(
            locus_with_lightcurve.lightcurve, "csv", exclude=["time"]
        )
        assert isinstance(lightcurve_string, str)
        assert "time" not in lightcurve_string
        del locus_with_lightcurve.lightcurve.columns["time"]
        check_all_rows_and_columns_in_string(
            locus_with_lightcurve.lightcurve, lightcurve_string
        )

    def test_astropy_table_to_string_without_excluding(self, locus_with_lightcurve):
        lightcurve_string = astropy_table_to_string(
            locus_with_lightcurve.lightcurve, "csv"
        )
        assert isinstance(lightcurve_string, str)
        check_all_rows_and_columns_in_string(
            locus_with_lightcurve.lightcurve, lightcurve_string
        )


class TestAstropyTableFromString:
    def test_astropy_table_from_string_empty(self):
        table = astropy_table_from_string("", "csv")
        assert isinstance(table, Table)

    def test_astropy_table_from_string(self, locus_with_lightcurve):
        lightcurve_string_expected = astropy_table_to_string(
            locus_with_lightcurve.lightcurve, "csv", exclude=["time"]
        )
        table = astropy_table_from_string(lightcurve_string_expected, "csv")
        assert isinstance(table, Table)
        lightcurve_retrieved = TimeSeries(table, time=mjd_to_datetime(table["ant_mjd"]))
        with tempfile.NamedTemporaryFile(mode="w") as tmp_file:
            assert report_diff_values(
                locus_with_lightcurve.lightcurve, lightcurve_retrieved, fileobj=tmp_file
            )


class TestNearest:
    @pytest.mark.parametrize(
        "coordinate,candidates,expected",
        [
            (
                SkyCoord("0d 0d"),
                [SkyCoord("-2d -2d"), SkyCoord("1d 1d"), SkyCoord("2d 2d")],
                SkyCoord("1d 1d"),
            ),
            (
                SkyCoord("1d 1d"),
                [SkyCoord("-2d -2d"), SkyCoord("1d 1d"), SkyCoord("2d 2d")],
                SkyCoord("1d 1d"),
            ),
            (
                SkyCoord("1d 1d"),
                [SkyCoord("0.5d 0.5d"), SkyCoord("2d 2d"), SkyCoord("2d 2d")],
                SkyCoord("0.5d 0.5d"),
            ),
        ],
    )
    def test_nearest_point(self, coordinate, candidates, expected):
        assert nearest(coordinate, candidates) == expected

    def test_nearest_returns_none_if_nothing_near(self):
        p1 = SkyCoord("1d 1d")
        points = (SkyCoord("0.5d 0.5d"), SkyCoord("2d 2d"), SkyCoord("3d 3d"))
        assert nearest(p1, points, max_angle=Angle("0.1d")) is None

    def test_nearest_setting_max_angle_returns_points(self):
        p1 = SkyCoord("2d 2d")
        points = (SkyCoord("3.1d 3.1d"), SkyCoord("3d 3d"), SkyCoord("3.2d 3.2d"))
        assert nearest(p1, points, max_angle=Angle("2d")) == SkyCoord("3d 3d")


@pytest.mark.parametrize(
    "lines, expected",
    [
        ("  foo", "foo"),
        ("    foo\n        bar\n", "foo\n    bar\n"),
        ("", ""),
    ],
)
def test_unindent(lines, expected):
    output = unindent(lines)
    assert output == expected


def test_format_dt():
    utc_now = datetime.datetime.utcnow()
    assert utc_now.isoformat(sep=" ") == format_dt(utc_now)


def test_jd_to_mjd():
    assert 0 == jd_to_mjd(2400000.5)


def test_mjd_to_datetime():
    assert datetime.datetime(8429, 11, 8, 12, 00, 00) == mjd_to_datetime(2400000.5)


class TestGetMedianCoords:
    @pytest.mark.parametrize(
        "ra_dec_list, expected",
        [
            ([(90, 90)], (0, 90)),
            ([(359, 90)], (0, 90)),
            ([(45, 45), (60, 60), (90, 90)], (60, 60)),
            ([(1, 1), (45, 45), (12, 12)], (12, 12)),
            ([(1, 1), (3, 3)], (2, 2)),
            ([(0, -90), (0, 90)], (0, 0)),
            ([(0, 45), (360, 45)], (360, 45)),
            ([(90, 75), (180, 75), (270, 75), (360, 75)], (0, 90)),
            ([(180, -90), (180, -45), (180, 0), (180, 45), (180, 90)], (180, 0)),
            (
                [
                    (119.0267724, 19.204187),
                    (119.0268038, 19.2042276),
                    (119.0267488, 19.2042328),
                    (119.0267942, 19.204226),
                ],
                (119.026, 19.204),
            ),
        ],
    )
    def test_get_median_coords(self, ra_dec_list, expected):
        ra_centroid, dec_centroid = get_median_coords(ra_dec_list)
        assert math.isclose(ra_centroid, expected[0], rel_tol=0.1)
        assert math.isclose(dec_centroid, expected[1], rel_tol=0.1)

    def test_get_median_coords_with_zero_coordinate(self):
        zero_coordinate = (0, 0)
        ra_centroid, dec_centroid = get_median_coords([zero_coordinate])
        assert (ra_centroid, dec_centroid) == zero_coordinate
