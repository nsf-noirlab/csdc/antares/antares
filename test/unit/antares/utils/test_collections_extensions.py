import collections

import pytest

from antares.utils.collections_extensions import (
    RestrictedWriteDictWrapper,
    RestrictedWriteSetWrapper,
)


@pytest.fixture
def wrapped_dictionary():
    TestDictionary = collections.namedtuple("TestDictionary", ["real", "wrapped"])
    real_dict = {"a": 1, "b": 2}
    wrapped_dict = RestrictedWriteDictWrapper(real_dict, writeable_keys=("a",))
    return TestDictionary(real_dict, wrapped_dict)


class TestRestrictedWriteDictWrapper:
    def test_restricted_write_dict_wrapper_allows_write_for_specified_keys(
        self, wrapped_dictionary
    ):
        key = "a"
        new_value = 2
        wrapped_dictionary.wrapped[key] = new_value
        assert wrapped_dictionary.real[key] == new_value
        assert wrapped_dictionary.wrapped[key] == new_value

    def test_restricted_write_dict_wrapper_disallows_write_for_non_specified_keys(
        self, wrapped_dictionary
    ):
        key = "b"
        value = 2
        with pytest.raises(KeyError):
            wrapped_dictionary.wrapped[key] = 3
        assert wrapped_dictionary.real[key] == value
        assert wrapped_dictionary.wrapped[key] == value

    def test_restricted_write_dict_wrapper_disallows_write_for_nan_numeric_values(
        self, wrapped_dictionary
    ):
        key = "a"
        value = 2
        with pytest.raises(ValueError):
            wrapped_dictionary.wrapped[key] = float("nan")
            assert wrapped_dictionary.real[key] == value
            assert wrapped_dictionary.wrapped[key] == value

    def test_restricted_write_dict_wrapper_contains_key(self, wrapped_dictionary):
        assert "a" in wrapped_dictionary.wrapped

    def test_restricted_write_dict_wrapper_doesnt_contains_key(
        self, wrapped_dictionary
    ):
        assert "c" not in wrapped_dictionary.wrapped

    def test_restricted_write_dict_wrapper_deletes_key(self, wrapped_dictionary):
        key = "a"
        del wrapped_dictionary.wrapped[key]
        assert key not in wrapped_dictionary.wrapped

    def test_restricted_write_dict_wrapper_deletes_key_that_doesnt_exists(
        self, wrapped_dictionary
    ):
        with pytest.raises(KeyError):
            del wrapped_dictionary.wrapped["c"]

    def test_restricted_write_dict_wrapper_iter(self, wrapped_dictionary):
        for wrapped_key, expected in zip(wrapped_dictionary.wrapped, ("a", "b")):
            assert wrapped_key == expected

    def test_restricted_write_dict_wrapper_len(self, wrapped_dictionary):
        assert len(wrapped_dictionary.wrapped) == 2


@pytest.fixture
def wrapped_set():
    TestSet = collections.namedtuple("TestDictionary", ["real", "wrapped"])
    real_set = {"c"}
    wrapped_set = RestrictedWriteSetWrapper(real_set, writeable_elements=("a",))
    return TestSet(real_set, wrapped_set)


class TestRestrictedWriteSetWrapper:
    def test_restricted_write_set_wrapper_allows_write_for_specified_items(
        self, wrapped_set
    ):
        item = "a"
        wrapped_set.wrapped.add(item)
        assert item in wrapped_set.real
        assert item in wrapped_set.wrapped

    def test_restricted_write_set_wrapper_disallows_write_for_non_specified_items(
        self, wrapped_set
    ):
        item = "b"
        with pytest.raises(ValueError):
            wrapped_set.wrapped.add(item)
        assert item not in wrapped_set.real
        assert item not in wrapped_set.wrapped

    def test_restricted_write_set_wrapper_len(self, wrapped_set):
        assert len(wrapped_set.wrapped) == 1
        wrapped_set.wrapped.add("a")
        assert len(wrapped_set.wrapped) == 2

    def test_restricted_write_set_wrapper_iter(self, wrapped_set):
        item = "a"
        wrapped_set.wrapped.add(item)
        for wrapped_value, expected in zip(wrapped_set.wrapped, {"c", item}):
            assert wrapped_value == expected

    def test_restricted_write_set_wrapper_discard(self, wrapped_set):
        item = "a"
        wrapped_set.wrapped.add(item)
        wrapped_set.wrapped.discard(item)
        assert item not in wrapped_set.wrapped

    def test_restricted_write_set_wrapper_disallows_discard_for_non_specified_keys(
        self, wrapped_set
    ):
        item = "c"
        with pytest.raises(ValueError):
            wrapped_set.wrapped.discard(item)
        assert item in wrapped_set.wrapped
