from test.fakes import build_alerts

import numpy as np
import pandas as pd
import pytest
from astropy.timeseries import TimeSeries

from antares.utils import mjd_to_datetime, ztf_flux_correction


class TestIsVar:
    def test_is_var_star(self):
        df = pd.DataFrame()
        df["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
        df["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
        df["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
        assert ztf_flux_correction._is_var_star(df)

    def test_is_var_star_false_if_distnr_higher_than_threshold(self):
        df = pd.DataFrame()
        df["ztf_distnr"] = np.array([1.1, 1.2, 1.3])
        df["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
        df["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
        assert not ztf_flux_correction._is_var_star(df, match_radius_arcsec=1.0)

    def test_is_var_star_false_if_distpsnr1_higher_than_threshold(self):
        df = pd.DataFrame()
        df["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
        df["ztf_distpsnr1"] = np.array([1.1, 1.2, 1.3])
        df["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
        assert not ztf_flux_correction._is_var_star(df, match_radius_arcsec=1.0)

    def test_is_var_star_false_if_sgscore1_lower_than_threshold(self):
        df = pd.DataFrame()
        df["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
        df["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
        df["ztf_sgscore1"] = np.array([0.1, 0.2, 0.3])
        assert not ztf_flux_correction._is_var_star(df, star_galaxy_threshold=1.0)


REQUIRED_COLUMNS_BY_CORRECT_MAGS = (
    "ztf_fid",
    "ztf_field",
    "ztf_rcid",
    "ztf_distpsnr1",
    "ztf_distnr",
    "ztf_sgscore1",
    "ztf_magnr",
    "ztf_sigmagnr",
    "ztf_isdiffpos",
    "ztf_magpsf",
    "ztf_sigmapsf",
    "ztf_diffmaglim",
)


@pytest.fixture
def timeseries_sample():
    alerts = build_alerts(3)
    data = [
        {"alert_id": alert.id, "ant_mjd": alert.mjd, **alert.properties}
        for alert in alerts
    ]
    times = [mjd_to_datetime(alert.mjd) for alert in alerts]
    timeseries = TimeSeries(data=data, time=times)
    for column in REQUIRED_COLUMNS_BY_CORRECT_MAGS:
        timeseries[column] = 0
    return timeseries


class TestCorrectMags:
    CORRECTED_COLUMNS = (
        "ant_mag_corrected",
        "ant_magerr_corrected",
        "ant_magulim_corrected",
        "ant_magllim_corrected",
    )

    @pytest.mark.xfail(reason="TODO")
    def test_correct_mag_with_real_data(self):
        assert False

    def test_correct_mags_without_ztf_required_columns(self, timeseries_sample):
        timeseries_sample.remove_columns(REQUIRED_COLUMNS_BY_CORRECT_MAGS)
        df, is_var_star, corrected = ztf_flux_correction.correct_mags(timeseries_sample)
        assert not corrected
        assert not is_var_star
        for column in self.CORRECTED_COLUMNS:
            assert np.isnan(df[column].values).all()

    def test_correct_mags_if_var_star(self, timeseries_sample):
        timeseries_sample["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
        timeseries_sample["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
        timeseries_sample["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
        df, is_var_star, corrected = ztf_flux_correction.correct_mags(timeseries_sample)
        assert corrected
        assert is_var_star
        assert np.isfinite(df["ant_magulim_corrected"]).all()

    def test_correct_mags_if_not_var_star(self, timeseries_sample):
        timeseries_sample["ztf_distnr"] = np.array([1.1, 1.2, 1.3])
        timeseries_sample["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
        timeseries_sample["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
        df, is_var_star, corrected = ztf_flux_correction.correct_mags(timeseries_sample)
        assert not corrected
        assert not is_var_star
        for column in self.CORRECTED_COLUMNS:
            assert np.isnan(df[column].values).all()

    def test_correct_mags_with_negative_total_fluxes(self, timeseries_sample):
        timeseries_sample["ztf_distnr"] = np.array([0.1, 0.2, 0.3])
        timeseries_sample["ztf_distpsnr1"] = np.array([0.1, 0.2, 0.3])
        timeseries_sample["ztf_sgscore1"] = np.array([1.1, 1.2, 1.3])
        timeseries_sample["ztf_magnr"] = np.array([1, 2, 3])
        timeseries_sample["ztf_isdiffpos"] = np.array([-1, -1, -1])
        df, is_var_star, corrected = ztf_flux_correction.correct_mags(timeseries_sample)
        assert not corrected
        assert is_var_star
        for column in self.CORRECTED_COLUMNS:
            assert np.isnan(df[column].values).all()
