from unittest.mock import patch

import htm
import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.utils import spatial


@patch("antares.utils.spatial.htm.get_htm_circle_region")
def test_get_region_handles_precision_error(mock_get_htm_circle_region):
    ra = 109.1830019
    dec = 4.0257554
    location = SkyCoord(f"{ra}d {dec}d")
    radius = Angle("0.00028d")
    mock_get_htm_circle_region.side_effect = [htm.PrecisionError, (20, 22)]
    region = spatial.get_htm_region(location, radius, 16)
    assert region == (20, 22)
    mock_get_htm_circle_region.side_effect = [
        htm.PrecisionError,
        htm.PrecisionError,
    ]
    with pytest.raises(htm.PrecisionError):
        spatial.get_htm_region(location, radius, 16)


@patch("antares.utils.spatial.get_htm_region")
def test_get_region_denormalized(mock_get_region):
    mock_get_region.return_value = [(20, 22), (24, 25)]
    assert spatial.get_htm_region_denormalized(SkyCoord("0d 0d"), Angle("0d"), 16) == [
        20,
        21,
        22,
        24,
        25,
    ]


def test_get_htm_id():
    htm20 = spatial.get_htm_id(SkyCoord("10d 10d"), 20)
    assert htm20 == 17098002819647
