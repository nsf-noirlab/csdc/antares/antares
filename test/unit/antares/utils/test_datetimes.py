from datetime import datetime

import pytest

from antares.utils.datetimes import parse_datetime


class TestParseDatetime:
    def test_parse_succesfully(self):
        datetime_str = "2018-11-01 22:22:46.654"
        fmts = ["%Y-%m-%d %H:%M:%S.%f",
                "%Y-%m-%d %H:%M:%S",
                "%Y-%m-%dT%H:%M:%S.%fZ",
                "%Y-%m-%dT%H:%M:%SZ"]
        result = parse_datetime(datetime_str, fmts)
        assert result == datetime(2018, 11, 1, 22, 22, 46, 654000)

    def test_raises_if_unable_to_parse(self):
        datetime_str = "2018-11-01F22:22:46.654"
        fmts = ["%Y-%m-%d %H:%M:%S.%f",
                "%Y-%m-%d %H:%M:%S",
                "%Y-%m-%dT%H:%M:%S.%fZ",
                "%Y-%m-%dT%H:%M:%SZ"]
        with pytest.raises(ValueError):
            parse_datetime(datetime_str, fmts)

        
