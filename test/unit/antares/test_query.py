import pytest

from antares.domain.models import Blob
from antares.query import load_files


@pytest.mark.usefixtures("message_bus")
class TestLoadFiles:
    def setup_method(self):
        self.keys = ("match_1", "match_2")

    def test_empty_blob_repo(self):
        result = load_files(self.keys)
        assert result == {}

    @pytest.mark.with_data(Blob(id="no_match", data=b"val"))
    def test_no_keys_found_in_blob_repo(self):
        result = load_files(self.keys)
        assert result == {}

    @pytest.mark.with_data(
        Blob(id="no_match", data=b"val"), Blob(id="match_1", data=b"val_1")
    )
    def test_one_key_found_in_blob_repo(self):
        result = load_files(self.keys)
        assert result == {"match_1": b"val_1"}

    @pytest.mark.with_data(
        Blob(id="no_match", data=b"val"),
        Blob(id="match_1", data=b"val_1"),
        Blob(id="match_2", data=b"val_2"),
    )
    def test_two_keys_found_in_blob_repo(self):
        result = load_files(self.keys)
        assert result == {"match_1": b"val_1", "match_2": b"val_2"}
