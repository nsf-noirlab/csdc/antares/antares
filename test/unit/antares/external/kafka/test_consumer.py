import logging
from unittest.mock import patch

from antares.external.kafka import KafkaConsumer


@patch("antares.external.kafka.consumer.Consumer", autospec=True)
class TestKafkaConsumerInit:
    def test_basic_config_passed_unedited(self, mock_consumer_cls):
        fake_config = {"key": "value"}
        KafkaConsumer(fake_config)
        mock_consumer_cls.assert_called_once_with(
            fake_config, logger=logging.getLogger("kafka_consumer")
        )

    def test_group_settable(self, mock_consumer_cls):
        fake_config = {"key": "value"}
        test_group = "test_group"
        KafkaConsumer(fake_config, group=test_group)
        call_dict = mock_consumer_cls.call_args[0][0]
        assert "group.id" in call_dict
        assert call_dict["group.id"] == test_group

    def test_subscribes_to_topics(self, mock_consumer_cls):
        fake_config = {"key": "value"}
        test_topics = ["topic1", "topic2"]
        KafkaConsumer(fake_config, topics=test_topics)
        mock_consumer = mock_consumer_cls.return_value
        mock_consumer.subscribe.assert_called_once_with(test_topics)

    @patch("antares.external.kafka.consumer.GCNConsumer", autospec=True)
    def test_uses_gcn_consumer(self, mock_gcn_consumer_cls, mock_consumer_cls):
        fake_config = {"key": "value", "client_id": "fake_id"}
        KafkaConsumer(fake_config)
        mock_gcn_consumer_cls.assert_called_once_with(
            fake_config, logger=logging.getLogger("kafka_consumer")
        )
        mock_consumer_cls.assert_not_called()
