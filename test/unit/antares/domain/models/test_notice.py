import json
from base64 import b64decode
from datetime import datetime, timedelta
from io import BytesIO
from test.fakes import build_notice
from unittest.mock import patch

from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.table import Table
from pytest import approx

from antares.domain.models import GravWaveNotice, GravWaveNoticeTypes


class TestGravWaveNoticeTypes:
    def test_can_access_values(self):
        assert "EARLY_WARNING" in GravWaveNoticeTypes.__members__
        assert GravWaveNoticeTypes.INITIAL.value == "INITIAL"
        assert GravWaveNoticeTypes["PRELIMINARY"].name == "PRELIMINARY"
        assert GravWaveNoticeTypes["UPDATE"].value == "UPDATE"
        assert GravWaveNoticeTypes("RETRACTION").name == "RETRACTION"


class TestGravWaveNotice:
    def test_create_without_event_data(self):
        notice_datetime = datetime.utcnow()
        notice = GravWaveNotice(
            gracedb_id="test_gracedb_id",
            notice_type="Update",
            notice_datetime=notice_datetime,
        )
        assert notice.gracedb_id == "test_gracedb_id"
        assert notice.notice_type == "Update"
        assert notice.notice_datetime == notice_datetime
        assert notice.id is None
        assert notice.event_datetime is None
        assert notice.false_alarm_rate is None
        assert notice.skymap_base64 is None
        assert notice.external_coinc is None
        assert notice.full_notice is None

    def test_create_with_event_data(self):
        notice_datetime = datetime.utcnow()
        event_datetime = datetime.utcnow() - timedelta(minutes=1)
        notice = GravWaveNotice(
            id=1,
            gracedb_id="test_gracedb_id",
            notice_type="Update",
            notice_datetime=notice_datetime,
            event_datetime=event_datetime,
            false_alarm_rate=1.23,
            skymap_base64="notarealbasemap",
            external_coinc={"key": "value"},
            full_notice={"key2": "value2"},
        )
        assert notice.id == 1
        assert notice.gracedb_id == "test_gracedb_id"
        assert notice.notice_type == "Update"
        assert notice.notice_datetime == notice_datetime
        assert notice.event_datetime == event_datetime
        assert notice.false_alarm_rate == 1.23
        assert notice.skymap_base64 == "notarealbasemap"
        assert notice.external_coinc == {"key": "value"}
        assert notice.full_notice == {"key2": "value2"}

    def test_from_gcn_with_event(self):
        with open(
            "test/data/e2e/grav_wave/MS181101ab-earlywarning.json",
            "rt",
            encoding="utf8",
        ) as notice_file:
            notice_json = notice_file.read()
        notice_dict = json.loads(notice_json)
        notice_dict["external_coinc"] = {"test": "value"}
        notice = GravWaveNotice.from_gcn(notice_dict)
        assert notice.gracedb_id == notice_dict["superevent_id"]
        assert notice.notice_type == GravWaveNoticeTypes[notice_dict["alert_type"]]
        assert notice.notice_datetime == datetime.fromisoformat(
            notice_dict["time_created"][:-1]
        )
        assert notice.event_datetime == datetime.fromisoformat(
            notice_dict["event"]["time"][:-1]
        )
        assert notice.false_alarm_rate == notice_dict["event"]["far"]
        assert notice.skymap_base64 == notice_dict["event"]["skymap"]
        assert notice.external_coinc == {"test": "value"}
        notice_dict["event"]["skymap"] = None
        assert notice.full_notice == notice_dict

    def test_from_gcn_without_event(self):
        with open(
            "test/data/e2e/grav_wave/MS181101ab-retraction.json", "rt", encoding="utf8"
        ) as notice_file:
            notice_json = notice_file.read()
        notice_dict = json.loads(notice_json)
        notice = GravWaveNotice.from_gcn(notice_dict)
        assert notice.gracedb_id == notice_dict["superevent_id"]
        assert notice.notice_type == GravWaveNoticeTypes[notice_dict["alert_type"]]
        assert notice.notice_datetime == datetime.fromisoformat(
            notice_dict["time_created"][:-1]
        )
        assert notice.event_datetime is None
        assert notice.false_alarm_rate is None
        assert notice.skymap_base64 is None
        assert notice.external_coinc is None
        assert notice.full_notice == notice_dict

    def test_alternate_timestamp_format(self):
        with open(
            "test/data/e2e/grav_wave/MS181101ab-earlywarning.json",
            "rt",
            encoding="utf8",
        ) as notice_file:
            notice_json = notice_file.read()
        notice_dict = json.loads(notice_json)
        notice_dict["time_created"] = "2018-11-01 22:34:20"
        notice_dict["event"]["time"] = "2018-11-01 22:22:46.654"
        notice = GravWaveNotice.from_gcn(notice_dict)
        assert notice.notice_datetime == datetime(2018, 11, 1, 22, 34, 20)
        assert notice.event_datetime == datetime(2018, 11, 1, 22, 22, 46, 654000)

    def test_copy_without_skymap(self):
        notice_datetime = datetime.utcnow()
        event_datetime = datetime.utcnow() - timedelta(minutes=1)
        notice = GravWaveNotice(
            id=1,
            gracedb_id="test_gracedb_id",
            notice_type="Update",
            notice_datetime=notice_datetime,
            event_datetime=event_datetime,
            false_alarm_rate=1.23,
            skymap_base64="notarealbasemap",
            external_coinc={"key": "value"},
            full_notice={"key2": "value2"},
        )
        notice_metadata = notice.copy_without_skymap()
        assert isinstance(notice_metadata, GravWaveNotice)
        assert notice_metadata is not notice
        assert notice_metadata.skymap_base64 is None
        assert notice_metadata.full_notice is not notice.full_notice


class TestGravWaveNoticeGetProbablityDensity:
    def test_can_get_near_zero_probability_density(self):
        notice = build_notice()
        location = SkyCoord(ra=0, dec=15, unit=u.deg)
        prob = notice.get_probability_density(location)
        assert prob == approx(0)

    def test_can_get_nonzero_probability_density(self):
        notice = build_notice()
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        prob = notice.get_probability_density(location)
        assert prob == approx(56.64, 0.001)

    @patch("antares.domain.models.notice.np.argsort")
    def test_sort_cached(self, mock_argsort):
        notice = build_notice()
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)

        skymap_bytes = b64decode(notice.skymap_base64)
        skymap = Table.read(BytesIO(skymap_bytes))

        mock_argsort.return_value = range(len(skymap))
        notice.get_probability_density(location)
        notice.get_probability_density(location)

        mock_argsort.assert_called_once()

    def test_can_get_multiple_probability_densities(self):
        notice = build_notice()
        location = SkyCoord(ra=0, dec=15, unit=u.deg)
        prob = notice.get_probability_density(location)
        assert prob == approx(0)
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        prob = notice.get_probability_density(location)
        assert prob == approx(56.64, 0.001)


class TestGravWaveNoticeGetProbabilityContourLevelAndArea:
    def test_can_get_near_100_contour_level(self):
        notice = build_notice()
        location = SkyCoord(ra=0, dec=15, unit=u.deg)
        level, area = notice.get_probability_contour_level_and_area(location)

        assert level == approx(100.0)
        assert area.value == approx(21536.2749, 0.0001)

    def test_can_get_non_100_contour_level(self):
        notice = build_notice()
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)
        level, area = notice.get_probability_contour_level_and_area(location)

        assert level == approx(76.6260, 0.0001)
        assert area.value == approx(18.8513, 0.0001)

    @patch("antares.domain.models.notice.np.argsort")
    def test_sort_cached(self, mock_argsort):
        notice = build_notice()
        location = SkyCoord(ra=193.30, dec=-17.85, unit=u.deg)

        skymap_bytes = b64decode(notice.skymap_base64)
        skymap = Table.read(BytesIO(skymap_bytes))

        mock_argsort.return_value = range(len(skymap))
        notice.get_probability_contour_level_and_area(location)
        notice.get_probability_contour_level_and_area(location)
        notice.get_probability_contour_level_and_area(location)

        # 1 sort of healpix to get prod density at position
        # 1 sort of prob density for contour level and area
        assert len(mock_argsort.mock_calls) == 2
