import datetime
import tempfile
import uuid
from string import Template
from test.fakes import build_alert, build_alerts, build_catalog_object, build_filter
from test.fakes import build_filter_context as fake_build_filter_context
from test.fakes import (
    build_filter_revision,
    build_locus,
    build_notice,
    build_user,
    build_ztf_event_fp_hist,
)
from unittest.mock import patch

import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord
from statemachine.exceptions import TransitionNotAllowed

from antares.domain.models import (
    AlertGravWaveEvent,
    Blob,
    Filter,
    FilterContext,
    FilterContextAlert,
    FilterOutputSpecification,
    FilterOutputSpecificationProperty,
    FilterRevision,
    FilterRevisionStatus,
)
from antares.domain.models.filter import (
    FilterDevkitApi,
    FilterErrored,
    FilterExecutable,
    FilterSkipped,
    FilterSucceeded,
    alert_satisfies_filter_constraints,
    build_filter_context,
    build_filter_context_alert,
    filter_context_guard,
    locus_satisfies_filter_constraints,
    run_filter_against_locus,
)
from antares.entrypoints.pipeline.stages.update_locus_history import compute_timeseries
from antares.exceptions import HaltPipeline

FILTER_REVISION_WITH_SETUP_CODE = """
import antares.devkit as dk

class FilterWithSetup(dk.Filter):
    ERROR_SLACK_CHANNEL = None

    def setup(self):
        self.foo = "bar"

    def run(self, locus):
        assert self.foo == "bar"
"""


FILTER_REVISION_WITH_SLACK_CHANNEL = """
import antares.devkit as dk

class FilterWithFile(dk.Filter):
    ERROR_SLACK_CHANNEL = "#test"

    def run(self, locus):
        pass
"""


FILTER_REVISION_WITH_FILE_CODE = """
import antares.devkit as dk

class FilterWithFile(dk.Filter):
    ERROR_SLACK_CHANNEL = None
    REQUIRES_FILES = ["key"]

    def run(self, locus):
        assert self.files["key"] == b"val"
"""


FILTER_REVISION_BAD_SEEING_CODE = """
import antares.devkit as dk

class BadSeeing(dk.Filter):
    ERROR_SLACK_CHANNEL = None

    def run(self, locus):
        a = locus.alert.properties
        try:
            if a.get("ztf_fwhm") is not None:
                assert a["ztf_fwhm"] <= 5.0
            if a.get("ztf_elong") is not None:
                assert a["ztf_elong"] <= 1.2
        except AssertionError:
            raise locus.halt
"""

FILTER_REVISION_DUPLICATED = """
import antares.devkit as dk

class Filter1(dk.Filter):
    def run(self, locus):
        pass
class Filter2(dk.Filter):
    def run(self, locus):
        pass
"""

FILTER_REVISION_UNSUPPORTED_TYPE = """
import antares.devkit as dk

class Filter(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
        "name": "name",
        "type": "dict",
        "description": "description",
        },
    ]
    def run(self, locus):
        pass
"""

FILTER_REVISION_TYPE_TEMPLATE = Template(
    """
import antares.devkit as dk

class Filter(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
        "name": "name",
        "type": "$type",
        "description": "description",
        },
    ]
    def run(self, locus):
        pass
"""
)


class PassingFilter(FilterDevkitApi):
    def run(self, locus):
        pass


class FilterUsingNonPersistantProperties(FilterDevkitApi):
    def run(self, locus):
        if "ztf_fp_hists" in locus.alerts[-1].properties:
            print("NON_PERSISTANT_PROPERTIES_FOUND")


@pytest.fixture
def filter_context_alert_dict():
    return {
        "alert_id": "alert_id",
        "locus_id": "locus_id",
        "mjd": 59000.0,
        "processed_at": "2022-10-17T00:00:00",
        "properties": {},
    }


@pytest.fixture
def filter_context_alert_dict_w_gw_event():
    return {
        "alert_id": "alert_id",
        "locus_id": "locus_id",
        "mjd": 59000.0,
        "processed_at": "2022-10-17T00:00:00",
        "properties": {},
        "grav_wave_events": [
            {"gracedb_id": "testdb_id", "contour_level": 45, "contour_area": 76}
        ],
    }


class TestFilterExecutable:
    def test_filter_has_setup_method_called(self):
        filter_: Filter = build_filter()
        filter_revision = build_filter_revision(
            filter_,
            status=FilterRevisionStatus.ENABLED,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_executable = filter_revision.to_filter_executable()
        locus = build_locus(location=SkyCoord("0d 0d"))
        alert = build_alert()
        timeseries = compute_timeseries([alert])
        adapter = build_filter_context(locus, [alert], [], timeseries)
        filter_executable(adapter)

    @pytest.mark.with_data(Blob(id="key", data=b"val"))
    def test_filter_can_access_defined_required_files(self, message_bus):
        filter_: Filter = build_filter()
        filter_revision = build_filter_revision(
            filter_,
            status=FilterRevisionStatus.ENABLED,
            code=FILTER_REVISION_WITH_FILE_CODE,
        )
        filter_executable = filter_revision.to_filter_executable()
        locus = build_locus(location=SkyCoord("0d 0d"))
        alert = build_alert()
        timeseries = compute_timeseries([alert])
        adapter = build_filter_context(locus, [alert], [], timeseries)
        filter_executable(adapter)

    def test_filter_loads_slack_channel(self):
        filter_: Filter = build_filter()
        filter_revision = build_filter_revision(
            filter_,
            status=FilterRevisionStatus.ENABLED,
            code=FILTER_REVISION_WITH_SLACK_CHANNEL,
        )
        filter_executable = filter_revision.to_filter_executable()
        assert filter_executable.slack_channel == "#test"


class TestFilterContextAlert:
    def test_filter_context_alert_from_dict(self, filter_context_alert_dict):
        filter_context_alert = FilterContextAlert.from_dict(filter_context_alert_dict)
        assert isinstance(filter_context_alert, FilterContextAlert)
        assert filter_context_alert.alert_id == "alert_id"
        assert filter_context_alert.locus_id == "locus_id"
        assert filter_context_alert.mjd == 59000.0
        assert filter_context_alert.processed_at == datetime.datetime(2022, 10, 17)
        assert filter_context_alert.properties == {}
        assert filter_context_alert.grav_wave_events == []

    def test_filter_context_alert_to_dict_with_processed_at(
        self, filter_context_alert_dict
    ):
        filter_context_alert = FilterContextAlert(
            alert_id="alert_id",
            locus_id="locus_id",
            mjd=59000.0,
            processed_at=datetime.datetime(2022, 10, 17),
            properties={},
        )
        retrieved_dict = filter_context_alert.to_dict()
        assert retrieved_dict["grav_wave_events"] == []
        retrieved_dict.pop("grav_wave_events")
        assert filter_context_alert_dict == retrieved_dict

    def test_filter_context_alert_to_dict_without_processed_at(
        self, filter_context_alert_dict
    ):
        filter_context_alert_dict["processed_at"] = None
        filter_context_alert = FilterContextAlert(
            alert_id="alert_id",
            locus_id="locus_id",
            mjd=59000.0,
            processed_at=None,
            properties={},
        )
        retrieved_dict = filter_context_alert.to_dict()
        assert retrieved_dict["grav_wave_events"] == []
        retrieved_dict.pop("grav_wave_events")
        assert filter_context_alert_dict == retrieved_dict


class TestFilterContext:
    def test_filter_context_comparison_with_other_classes_notimplemented(self):
        filter_context = fake_build_filter_context()
        comparison = filter_context == "str"
        assert not comparison

    def test_filter_context_repr(self):
        filter_context = fake_build_filter_context()
        assert (
            repr(filter_context)
            == f'FilterContext(locus_id="{filter_context.locus_id}")'
        )

    def test_filter_context_str(self):
        filter_context = fake_build_filter_context()
        assert str(filter_context) == f"{vars(filter_context)}"

    def test_filter_context_builds_lightcurve_and_timeseries_if_not_passed(self):
        filter_context = fake_build_filter_context()
        filter_context_1 = FilterContext(
            locus_id=filter_context.locus_id,
            alerts=filter_context.alerts,
            ra=filter_context.ra,
            dec=filter_context.dec,
            properties=filter_context.properties,
            tags=filter_context.tags,
            catalog_objects=filter_context.catalog_objects,
            lightcurve=filter_context.lightcurve,
            timeseries=filter_context.timeseries,
        )
        filter_context_2 = FilterContext(
            locus_id=filter_context.locus_id,
            alerts=filter_context.alerts,
            ra=filter_context.ra,
            dec=filter_context.dec,
            properties=filter_context.properties,
            tags=filter_context.tags,
            catalog_objects=filter_context.catalog_objects,
            lightcurve=None,
            timeseries=None,
        )
        assert filter_context_1 == filter_context_2

    def test_filter_context_halt(self):
        filter_context = fake_build_filter_context()
        assert filter_context.halt == HaltPipeline

    def test_filter_context_returns_last_alert(self):
        filter_context = fake_build_filter_context()
        assert filter_context.alert == filter_context.alerts[-1]

    def test_filter_context_return_skycoord_location(self):
        filter_context = fake_build_filter_context()
        assert filter_context.location == SkyCoord(
            ra=filter_context.ra, dec=filter_context.dec, unit=u.deg
        )

    def test_filter_context_locus_to_dict_from_dict_roundtrip(self):
        filter_context = fake_build_filter_context()
        assert filter_context == FilterContext.from_dict(filter_context.to_dict())

    def test_create_filter_context_from_manual_dict(self):
        locus_dict = {
            "locus_id": "locus1",
            "ra": 88.2744186,
            "dec": -5.0010774,
            "properties": {
                "num_alerts": 2,
                "num_mag_values": 2,
            },
            "tags": [],
            "watch_list_ids": [],
            "watch_object_ids": [],
            "catalog_objects": {},
            "alerts": [
                {
                    "alert_id": "alert1",
                    "locus_id": "locus1",
                    "mjd": 58794.272488399874,
                    "properties": {
                        "ant_mag": 15.1,
                    },
                },
                {
                    "alert_id": "alert2",
                    "locus_id": "locus1",
                    "mjd": 58799.50587960007,
                    "properties": {
                        "ant_mag": 15.2,
                    },
                },
            ],
        }
        retrieved_locus = FilterContext.from_dict(locus_dict)
        assert isinstance(retrieved_locus, FilterContext)

    def test_create_filter_context_from_manual_dict_without_mjd_raises_value_error(
        self,
    ):
        locus_dict = {
            "locus_id": "locus1",
            "ra": 88.2744186,
            "dec": -5.0010774,
            "properties": {
                "num_alerts": 2,
                "num_mag_values": 2,
            },
            "tags": [],
            "watch_list_ids": [],
            "watch_object_ids": [],
            "catalog_objects": {},
            "alerts": [
                {
                    "alert_id": "alert1",
                    "locus_id": "locus1",
                    "properties": {},
                },
                {"alert_id": "alert2", "locus_id": "locus1", "properties": {}},
            ],
        }
        with pytest.raises(TypeError) as err:
            FilterContext.from_dict(locus_dict)
            assert "mjd" in err

    def test_filter_context_locus_to_file_from_file_roundtrip(self):
        filter_context = fake_build_filter_context()
        with tempfile.NamedTemporaryFile("w") as file:
            filter_context.to_file(file.name)
            assert filter_context == FilterContext.from_file(file.name)

    def test_filter_locus_adapter_exposes_catalog_objects(self):
        locus = build_locus(location=SkyCoord("10d 10d"))
        alerts = build_alerts(10)
        timeseries = compute_timeseries(alerts)
        catalog_objects = [
            build_catalog_object(catalog_name="catalog-001", properties={"id": "1"}),
            build_catalog_object(catalog_name="catalog-001", properties={"id": "2"}),
            build_catalog_object(catalog_name="catalog-002", properties={"id": "3"}),
        ]
        filter_context = build_filter_context(
            locus, alerts, catalog_objects, timeseries
        )
        assert filter_context.catalog_objects == {
            "catalog-001": [{"id": "1"}, {"id": "2"}],
            "catalog-002": [{"id": "3"}],
        }

    def test_filter_locus_adapter_exposes_ra_property_as_degrees(self):
        locus = build_locus(location=SkyCoord("10d 10d"))
        alerts = build_alerts(10)
        timeseries = compute_timeseries(alerts)
        adapter = build_filter_context(locus, alerts, [], timeseries)
        assert adapter.ra == 10

    def test_filter_locus_adapter_exposes_dec_property_as_degrees(self):
        locus = build_locus(location=SkyCoord("10d 10d"))
        alerts = build_alerts(10)
        timeseries = compute_timeseries(alerts)
        adapter = build_filter_context(locus, alerts, [], timeseries)
        assert adapter.dec == 10

    def test_filter_locus_adapter_exposes_latest_alert_as_alert_property(self):
        locus = build_locus()
        alerts = build_alerts(10)
        timeseries = compute_timeseries(alerts)
        adapter = build_filter_context(locus, alerts, [], timeseries)
        assert adapter.alert.mjd == alerts[-1].mjd

    def test_filter_locus_adapter_exposes_latest_alert_in_expected_format(self):
        locus = build_locus()
        alerts = [build_alert()]
        timeseries = compute_timeseries(alerts)
        adapter = build_filter_context(locus, alerts, [], timeseries)
        assert adapter.alert.properties == {
            **alerts[0].properties,
            **alerts[0].normalized_properties,
        }

    def test_filter_locus_adapter_exposes_gw_event_on_alert(self):
        locus = build_locus()
        alerts = [build_alert()]
        alert_gw_event = AlertGravWaveEvent(
            gracedb_id="test1", contour_level=5, contour_area=40
        )
        alerts[0].grav_wave_events.append(alert_gw_event)
        timeseries = compute_timeseries(alerts)
        adapter = build_filter_context(locus, alerts, [], timeseries)
        assert adapter.alert.grav_wave_events == [alert_gw_event]

    def test_filter_locus_adapter_exposes_grav_wave_events_metadata(self):
        locus = build_locus()
        alerts = [build_alert()]
        alert_gw_event = AlertGravWaveEvent(
            gracedb_id="test1", contour_level=5, contour_area=40
        )
        alerts[0].grav_wave_events.append(alert_gw_event)
        timeseries = compute_timeseries(alerts)
        grav_wave_events_metadata = {"test1": build_notice()}
        adapter = build_filter_context(
            locus, alerts, [], timeseries, grav_wave_events_metadata
        )
        assert isinstance(adapter.grav_wave_events_metadata, dict)
        assert adapter.grav_wave_events_metadata == grav_wave_events_metadata


abstractmethods = "antares.domain.models.filter.FilterDevkitApi.__abstractmethods__"


class TestFilterDevkitApi:
    @patch(abstractmethods, set())
    def test_filter_devkit_api_run_isnt_implemented(self):
        filter_devkit_api = FilterDevkitApi()
        filter_config_names = [
            "FILTER_ID",
            "FILTER_VERSION_ID",
            "NAME",
            "ERROR_SLACK_CHANNEL",
            "RUN_IN_MUTEX",
            "REQUIRES_FILES",
            "INPUT_LOCUS_PROPERTIES",
            "REQUIRED_LOCUS_PROPERTIES",
            "INPUT_ALERT_PROPERTIES",
            "REQUIRED_ALERT_PROPERTIES",
            "INPUT_TAGS",
            "REQUIRED_TAGS",
            "REQUIRED_GRAV_WAVE_PROB_REGION",
            "OUTPUT_LOCUS_PROPERTIES",
            "OUTPUT_TAGS",
        ]
        assert filter_config_names == filter_devkit_api.get_config_names()

    @patch(abstractmethods, set())
    def test_filter_devkit_api_to_filter_executable(self):
        filter_devkit_api = FilterDevkitApi()
        filter_executable = filter_devkit_api.to_filter_executable()
        assert isinstance(filter_executable, FilterExecutable)
        assert filter_executable.filter_id == filter_devkit_api.FILTER_ID

    @pytest.mark.parametrize("test_type", ["int", "float", "str"])
    def test_filter_devkit_api_load_output_specification(self, test_type):
        name = "name"
        description = "description"

        class FilterDevKitOutputTemplate(FilterDevkitApi):
            OUTPUT_LOCUS_PROPERTIES = [
                {
                    "name": name,
                    "type": test_type,
                    "description": description,
                },
            ]

            def run(self, locus):
                pass

        filter_devkit_api = FilterDevKitOutputTemplate()
        filter_output_specification = filter_devkit_api._load_output_specification()
        assert filter_output_specification.properties[0].name == name
        assert filter_output_specification.properties[0].type == eval(test_type)
        assert filter_output_specification.properties[0].description == description

    def test_filter_devkit_api_load_output_specification_raises_valueerror(self):
        class FilterDevKitOutputDict(FilterDevkitApi):
            OUTPUT_LOCUS_PROPERTIES = [
                {
                    "name": "name",
                    "type": "dict",
                    "description": "description",
                },
            ]

            def run(self, locus):
                pass

        filter_devkit_api = FilterDevKitOutputDict()
        with pytest.raises(ValueError) as err:
            filter_devkit_api._load_output_specification()
            assert "Unknown or unsupported type: 'dict'" in err


ALLOWED_STATUS_EVENT_TUPLES = [
    (FilterRevisionStatus.PENDING_REVIEW, "review"),
    (FilterRevisionStatus.REVIEWED, "mark_as_pending"),
    (FilterRevisionStatus.APPROVED, "mark_as_pending"),
    (FilterRevisionStatus.REJECTED, "mark_as_pending"),
    (FilterRevisionStatus.DISABLED, "mark_as_pending"),
    (FilterRevisionStatus.PENDING_REVIEW, "approve"),
    (FilterRevisionStatus.REVIEWED, "enable"),
    (FilterRevisionStatus.APPROVED, "enable"),
    (FilterRevisionStatus.DISABLED, "enable"),
]


class TestFilterRevision:
    def test_casting_not_enabled_filter_revision_to_filter_executable_raises_error(
        self,
    ):
        non_executable_statuses = [
            s for s in FilterRevisionStatus if s != FilterRevisionStatus.ENABLED
        ]
        for status in non_executable_statuses:
            filter_: Filter = build_filter()
            filter_revision = build_filter_revision(
                filter_,
                status=status,
            )
            with pytest.raises(ValueError, match="must be enabled"):
                filter_revision.to_filter_executable()

    def test_can_cast_filter_revision_to_filter_executable(self):
        from antares.domain.models.filter import build_filter_context

        # Build and enable the bad seeing filter
        filter_: Filter = build_filter()
        filter_revision = build_filter_revision(
            filter_,
            status=FilterRevisionStatus.ENABLED,
            code=FILTER_REVISION_BAD_SEEING_CODE,
        )
        filter_executable = filter_revision.to_filter_executable()
        # Build a locus and alert history that will raise a PipelineHalt for bad seeing
        locus = build_locus(location=SkyCoord("10d 10d"))
        alert = build_alert(properties={"ztf_fwhm": 10.0})
        timeseries = compute_timeseries([alert])
        adapter = build_filter_context(locus, [alert], [], timeseries)
        with pytest.raises(adapter.halt):
            filter_executable(adapter)
        # Or that it doesn't if seeing is okay
        alert = build_alert(properties={"ztf_fwhm": 1.0})
        timeseries = compute_timeseries([alert])
        adapter = build_filter_context(locus, [alert], [], timeseries)
        filter_executable(adapter)

    def test_filter_revision_initial_status_is_pending_review(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        assert filter_revision.status == FilterRevisionStatus.PENDING_REVIEW

    def test_filter_revision_initial_feedback_is_empty(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        assert filter_revision.feedback == ""

    def test_filter_revision_transition_to_valid_transition(self):
        desired_status = FilterRevisionStatus.REVIEWED
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision.transition_to(desired_status)
        assert filter_revision.status == desired_status

    def test_filter_revision_transition_to_valid_transition_touches_transitioned_at(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        original_transitioned_at = filter_revision.transitioned_at
        filter_revision.transition_to(FilterRevisionStatus.REVIEWED)
        assert filter_revision.transitioned_at > original_transitioned_at

    def test_filter_revision_transition_to_valid_transition_without_feedback_sets_empty(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision.transition_to(FilterRevisionStatus.REVIEWED)
        assert filter_revision.feedback == ""

    def test_filter_revision_transition_to_valid_transition_with_feedback_field_is_set(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        feedback_text = "Ready to be enabled"
        filter_revision.transition_to(
            FilterRevisionStatus.REVIEWED,
            feedback=feedback_text,
        )
        assert filter_revision.feedback == feedback_text

    def test_filter_revision_transition_to_invalid_transition(self):
        desired_status = FilterRevisionStatus.ENABLED
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        original_status = filter_revision.status
        with pytest.raises(TransitionNotAllowed, match=r"Can't \w+ when in \w+"):
            filter_revision.transition_to(desired_status)

        assert filter_revision.status != desired_status
        assert filter_revision.status == original_status

    def test_filter_revision_transition_to_invalid_transition_keeps_transitioned_at(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        original_transitioned_at = filter_revision.transitioned_at
        with pytest.raises(TransitionNotAllowed, match=r"Can't \w+ when in \w+"):
            filter_revision.transition_to(FilterRevisionStatus.ENABLED)

        assert filter_revision.transitioned_at == original_transitioned_at

    def test_filter_revision_transition_to_invalid_transition_does_not_set_feedback(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        original_feedback = filter_revision.feedback
        feedback_text = "Ready to be enabled"
        with pytest.raises(TransitionNotAllowed, match=r"Can't \w+ when in \w+"):
            filter_revision.transition_to(
                FilterRevisionStatus.ENABLED,
                feedback=feedback_text,
            )

        assert filter_revision.feedback == original_feedback

    def test_filter_revision_load_filter_class_without_filter_subclasses(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code="",
        )
        with pytest.raises(RuntimeError) as err:
            filter_revision._load_filter_class()
            assert "No Filter subclasses found in code" in err

    def test_filter_revision_load_filter_class_with_multiple_filter_subclasses(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_DUPLICATED,
        )
        with pytest.raises(RuntimeError) as err:
            filter_revision._load_filter_class()
            assert "Multiple Filter subclasses found in code" in err

    @pytest.mark.parametrize("test_type", ["int", "float", "str"])
    def test_filter_revision_load_output_specification(self, test_type):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_TYPE_TEMPLATE.substitute(type=test_type),
        )
        filter_output_specification = filter_revision._load_output_specification()
        assert filter_output_specification.properties[0].name == "name"
        assert filter_output_specification.properties[0].type == eval(test_type)
        assert filter_output_specification.properties[0].description == "description"

    def test_filter_devkit_api_load_output_specification_raises_valueerror(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_UNSUPPORTED_TYPE,
        )
        with pytest.raises(ValueError) as err:
            filter_revision._load_output_specification()
            assert "Unknown or unsupported type: 'dict'" in err


class TestFilterRevisionStateMachine:
    def test_states_names_map_to_filter_revision_status_enum_names(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision_states_names = [s.name for s in filter_revision.states]
        filter_revision_status_enum_names = [e.name for e in FilterRevisionStatus]
        assert sorted(filter_revision_states_names) == sorted(
            filter_revision_status_enum_names
        )

    def test_status_machine_initial_state(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        assert (
            filter_revision.current_state.value == FilterRevisionStatus.PENDING_REVIEW
        )

    # Valid transitions

    def test_review_from_pending_review_transitions_to_reviewed(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision.review()
        assert filter_revision.current_state.value == FilterRevisionStatus.REVIEWED

    def test_mark_as_pending_from_reviewed_transitions_to_pending_review(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REVIEWED,
        )
        filter_revision.mark_as_pending()
        assert (
            filter_revision.current_state.value == FilterRevisionStatus.PENDING_REVIEW
        )

    def test_mark_as_pending_from_approved_transitions_to_pending_review(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.APPROVED,
        )
        filter_revision.mark_as_pending()
        assert (
            filter_revision.current_state.value == FilterRevisionStatus.PENDING_REVIEW
        )

    def test_mark_as_pending_from_rejected_transitions_to_pending_review(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REJECTED,
        )
        filter_revision.mark_as_pending()
        assert (
            filter_revision.current_state.value == FilterRevisionStatus.PENDING_REVIEW
        )

    def test_mark_as_pending_from_disabled_transitions_to_pending_review(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.DISABLED,
        )
        filter_revision.mark_as_pending()
        assert (
            filter_revision.current_state.value == FilterRevisionStatus.PENDING_REVIEW
        )

    def test_approve_from_pending_review_transitions_to_approved(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision.approve()
        assert filter_revision.current_state.value == FilterRevisionStatus.APPROVED

    def test_reject_from_pending_review_without_feedback_throws_and_keeps_status(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        original_status_value = filter_revision.current_state.value
        with pytest.raises(ValueError, match=r"FilterRevision must include feedback"):
            filter_revision.reject()
        assert filter_revision.current_state.value == original_status_value

    def test_reject_from_pending_review_transitions_to_rejected_sets_feedback(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        feedback_text = "Issues detected in code. Please submit new version with fixes"
        filter_revision.reject(feedback=feedback_text)
        assert filter_revision.current_state.value == FilterRevisionStatus.REJECTED
        assert filter_revision.feedback == feedback_text

    def test_enable_from_reviewed_raises_type_error_if_no_filter_parameter_passed(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REVIEWED,
        )
        with pytest.raises(TypeError, match="missing 1 required positional argument"):
            filter_revision.enable()

    def test_enable_from_reviewed_raises_value_error_if_no_filter_instance_passed(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REVIEWED,
        )
        with pytest.raises(ValueError, match="filter instance is required"):
            filter_revision.enable(filter_=None)

    def test_enable_from_reviewed_raises_value_error_if_revision_not_owned_by_filter(
        self,
    ):
        filter_1 = build_filter(id=1, enabled_filter_revision_id=1)
        filter_2 = build_filter(id=2, enabled_filter_revision_id=2)
        filter_revision = FilterRevision(
            id=filter_2.enabled_filter_revision_id,
            filter_id=filter_2.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REVIEWED,
        )
        with pytest.raises(ValueError, match="revision not owned by passed filter"):
            filter_revision.enable(filter_=filter_1)

    def test_enable_from_reviewed_transitions_to_enabled(self):
        filter_ = build_filter()
        filter_revision = FilterRevision(
            filter_id=filter_.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REVIEWED,
        )
        filter_revision.enable(filter_=filter_)
        assert filter_revision.current_state.value == FilterRevisionStatus.ENABLED

    def test_enable_from_approved_transitions_to_enabled(self):
        filter_ = build_filter()
        filter_revision = FilterRevision(
            filter_id=filter_.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.APPROVED,
        )
        filter_revision.enable(filter_=filter_)
        assert filter_revision.current_state.value == FilterRevisionStatus.ENABLED

    def test_enable_from_disabled_transitions_to_enabled(self):
        filter_ = build_filter()
        filter_revision = FilterRevision(
            filter_id=filter_.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.DISABLED,
        )
        filter_revision.enable(filter_=filter_)
        assert filter_revision.current_state.value == FilterRevisionStatus.ENABLED

    def test_disable_from_enabled_raises_type_error_if_no_filter_parameter_passed(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.ENABLED,
        )
        with pytest.raises(TypeError, match="missing 1 required positional argument"):
            filter_revision.disable(feedback="Disabling...")

    def test_disable_from_enabled_raises_value_error_if_no_filter_instance_passed(
        self,
    ):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.ENABLED,
        )
        with pytest.raises(ValueError, match="filter instance is required"):
            filter_revision.disable(filter_=None, feedback="Disabling...")

    def test_disable_from_enabled_raises_value_error_if_revision_not_owned_by_filter(
        self,
    ):
        filter_1 = build_filter(id=1, enabled_filter_revision_id=1)
        filter_2 = build_filter(id=2, enabled_filter_revision_id=2)
        filter_revision = FilterRevision(
            id=filter_2.enabled_filter_revision_id,
            filter_id=filter_2.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.ENABLED,
        )
        with pytest.raises(ValueError, match="revision not owned by passed filter"):
            filter_revision.disable(filter_=filter_1, feedback="Disabling...")

    def test_disable_from_enabled_without_feedback_throws_and_keeps_status(self):
        filter_ = build_filter(enabled_filter_revision_id=1)
        filter_revision = FilterRevision(
            filter_id=filter_.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.ENABLED,
        )
        original_status_value = filter_revision.current_state.value
        with pytest.raises(ValueError, match=r"FilterRevision must include feedback"):
            filter_revision.disable(filter_=filter_)
        assert filter_revision.current_state.value == original_status_value

    def test_disable_from_enabled_transitions_to_disabled_sets_feedback(self):
        filter_ = build_filter(enabled_filter_revision_id=1)
        filter_revision = FilterRevision(
            filter_id=filter_.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.ENABLED,
        )
        feedback_text = "Filter code is no longer valid. Try submitting a new version"
        filter_revision.disable(filter_=filter_, feedback=feedback_text)
        assert filter_revision.current_state.value == FilterRevisionStatus.DISABLED
        assert filter_revision.feedback == feedback_text

    # Invalid transitions

    def test_enable_from_pending_review_throws_error(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        with pytest.raises(
            TransitionNotAllowed, match="Can't enable when in PENDING_REVIEW"
        ):
            filter_revision.enable()

    def test_enable_from_rejected_throws_error(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REJECTED,
        )
        with pytest.raises(TransitionNotAllowed, match="Can't enable when in REJECTED"):
            filter_revision.enable()

    def test_disable_from_pending_review_throws_error(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        with pytest.raises(
            TransitionNotAllowed,
            match="Can't disable when in PENDING_REVIEW",
        ):
            filter_revision.disable()

    def test_disable_from_approved_throws_error(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.APPROVED,
        )
        with pytest.raises(
            TransitionNotAllowed,
            match="Can't disable when in APPROVED",
        ):
            filter_revision.disable()

    def test_disable_from_rejected_throws_error(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REJECTED,
        )
        with pytest.raises(
            TransitionNotAllowed,
            match="Can't disable when in REJECTED",
        ):
            filter_revision.disable()

    def test_disable_from_reviewed_throws_error(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REVIEWED,
        )
        with pytest.raises(
            TransitionNotAllowed,
            match="Can't disable when in REVIEWED",
        ):
            filter_revision.disable()

    # after_transition action side effects

    @pytest.mark.parametrize("status,event", ALLOWED_STATUS_EVENT_TUPLES)
    def test_allowed_transition_touches_transitioned_at(
        self,
        status,
        event,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_,
            status=status,
        )
        original_transitioned_at = filter_revision.transitioned_at
        filter_revision.send(event, filter_=filter_)
        assert filter_revision.transitioned_at > original_transitioned_at

    @pytest.mark.parametrize("status,event", ALLOWED_STATUS_EVENT_TUPLES)
    def test_allowed_transition_without_optional_feedback_sets_field_to_empty(
        self,
        status,
        event,
    ):
        feedback_text = "Test feedback"
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_,
            feedback=feedback_text,
            status=status,
        )
        assert filter_revision.feedback == feedback_text
        filter_revision.send(event, filter_=filter_)
        assert filter_revision.feedback == ""

    @pytest.mark.parametrize("status,event", ALLOWED_STATUS_EVENT_TUPLES)
    def test_allowed_transition_with_optional_feedback_sets_field_to_non_empty(
        self,
        status,
        event,
    ):
        feedback_text = "Test feedback"
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_,
            status=status,
        )
        assert filter_revision.feedback == ""
        filter_revision.send(event, filter_=filter_, feedback=feedback_text)
        assert filter_revision.feedback == feedback_text

    # transition_to

    def test_transition_to_can_receive_feedback_parameter_and_sets_field(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        feedback_text = "This is feedback provided to filter writer"
        filter_revision.transition_to(
            FilterRevisionStatus.REVIEWED,
            feedback=feedback_text,
        )
        assert filter_revision.feedback == feedback_text

    def test_transition_to_with_reviewed_transitions_successfully(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision.transition_to(FilterRevisionStatus.REVIEWED)
        assert filter_revision.current_state.value == FilterRevisionStatus.REVIEWED

    def test_transition_to_with_pending_review_transitions_successfully(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.REVIEWED,
        )
        filter_revision.transition_to(FilterRevisionStatus.PENDING_REVIEW)
        assert (
            filter_revision.current_state.value == FilterRevisionStatus.PENDING_REVIEW
        )

    def test_transition_to_with_approved_transitions_successfully(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision.transition_to(FilterRevisionStatus.APPROVED)
        assert filter_revision.current_state.value == FilterRevisionStatus.APPROVED

    def test_transition_to_with_rejected_transitions_successfully(self):
        filter_revision = FilterRevision(
            filter_id=1,
            code=FILTER_REVISION_WITH_SETUP_CODE,
        )
        filter_revision.transition_to(
            FilterRevisionStatus.REJECTED,
            feedback="Issues detected in code. Please submit a new version with fixes",
        )
        assert filter_revision.current_state.value == FilterRevisionStatus.REJECTED

    def test_transition_to_with_enabled_transitions_successfully(self):
        filter_ = build_filter()
        filter_revision = FilterRevision(
            filter_id=filter_.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.APPROVED,
        )
        filter_revision.transition_to(FilterRevisionStatus.ENABLED, filter_=filter_)
        assert filter_revision.current_state.value == FilterRevisionStatus.ENABLED

    def test_transition_to_with_disabled_transitions_successfully(self):
        filter_ = build_filter(enabled_filter_revision_id=1)
        filter_revision = FilterRevision(
            filter_id=filter_.id,
            code=FILTER_REVISION_WITH_SETUP_CODE,
            status=FilterRevisionStatus.ENABLED,
        )
        filter_revision.transition_to(
            FilterRevisionStatus.DISABLED,
            filter_=filter_,
            feedback="Filter code is no longer valid. Try submitting a new version",
        )
        assert filter_revision.current_state.value == FilterRevisionStatus.DISABLED


class TestFilter:
    def test_filter_without_id(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            public=False,
            description="Test filter description",
            enabled_filter_revision_id=None,
            version_id=0,
        )
        assert filter_.id is None

    def test_filter_without_public(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            description="Test filter description",
            enabled_filter_revision_id=None,
            version_id=0,
        )
        assert not filter_.public

    def test_filter_without_description(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            public=False,
            enabled_filter_revision_id=None,
            version_id=0,
        )
        assert filter_.description == ""

    def test_filter_without_enabled_filter_revision_id(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            public=False,
            description="Test filter description",
            version_id=0,
        )
        assert filter_.enabled_filter_revision_id is None

    def test_filter_without_disabled_at(self):
        filter_ = build_filter()
        assert filter_.disabled_at is None

    def test_filter__enable_filter_revision_for_enabled_filter_raises_value_error(self):
        filter_ = build_filter(enabled_filter_revision_id=2)
        filter_revision = build_filter_revision(filter_)
        with pytest.raises(ValueError, match="already has an enabled filter revision"):
            filter_._enable_filter_revision(filter_revision)

    def test_filter__enable_filter_revision_fails_if_not_owned_by_filter(self):
        filter_1 = build_filter(id=1)
        filter_2 = build_filter(id=2)
        assert not filter_1.enabled
        assert not filter_2.enabled
        filter_revision = build_filter_revision(filter_2)
        with pytest.raises(ValueError):
            filter_1._enable_filter_revision(filter_revision)

    def test_filter__enable_filter_revision_changes_enabled_flag(self):
        filter_ = build_filter()
        assert not filter_.enabled
        filter_revision = build_filter_revision(filter_)
        filter_._enable_filter_revision(filter_revision)
        assert filter_.enabled

    def test_filter__enable_filter_revision_changes_enabled_filter_revision_id(self):
        filter_ = build_filter()
        assert filter_.enabled_filter_revision_id is None
        filter_revision = build_filter_revision(filter_)
        filter_._enable_filter_revision(filter_revision)
        assert filter_.enabled_filter_revision_id == filter_revision.id

    def test_filter__enable_filter_revision_sets_disabled_at_to_none(self):
        filter_ = build_filter(disabled_at=datetime.datetime.utcnow())
        assert isinstance(filter_.disabled_at, datetime.datetime)
        filter_revision = build_filter_revision(filter_)
        filter_._enable_filter_revision(filter_revision)
        assert filter_.disabled_at is None

    def test_filter__disable_for_not_enabled_filter_raises_value_error(self):
        filter_ = build_filter()
        with pytest.raises(ValueError, match="cannot be disabled"):
            filter_._disable()

    def test_filter__disable_sets_enabled_filter_revision_id_to_none(self):
        filter_ = build_filter(enabled_filter_revision_id=1)
        assert filter_.enabled_filter_revision_id == 1
        filter_._disable()
        assert filter_.enabled_filter_revision_id is None

    def test_filter__disable_sets_disabled_at_when_not_present(self):
        filter_ = build_filter(enabled_filter_revision_id=1)
        assert filter_.disabled_at is None
        filter_._disable()
        assert isinstance(filter_.disabled_at, datetime.datetime)

    def test_filter__disable_touches_disabled_at_when_present(self):
        filter_ = build_filter(
            enabled_filter_revision_id=1,
            disabled_at=datetime.datetime.utcnow(),
        )
        assert isinstance(filter_.disabled_at, datetime.datetime)
        original_disabled_at = filter_.disabled_at
        filter_._disable()
        assert filter_.disabled_at > original_disabled_at

    def test_filter_make_public(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            public=False,
            enabled_filter_revision_id=1,
            version_id=0,
        )
        assert not filter_.public
        filter_.make_public()
        assert filter_.public

    def test_filter_make_private(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            public=True,
            enabled_filter_revision_id=1,
            version_id=0,
        )
        assert filter_.public
        filter_.make_private()
        assert not filter_.public

    def test_filter_owner_property_not_explicitly_set(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            public=False,
            description="Test filter description",
            version_id=0,
        )
        assert filter_.owner is None

    def test_filter_owner_property_explicitly_set_correctly(self):
        user = build_user()
        filter_ = Filter(
            name="Test Filter",
            owner_id=user.id,
            id=1,
            public=False,
            description="Test filter description",
            version_id=0,
        )
        filter_.owner = user
        assert filter_.owner == user

    def test_filter_owner_property_explicitly_set_incorrectly(self):
        user = build_user(id=uuid.UUID(int=123))
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            public=False,
            description="Test filter description",
            version_id=0,
        )
        with pytest.raises(ValueError, match="must match owner_id"):
            filter_.owner = user

    def test_filter_owner_property_explicitly_set_to_none(self):
        filter_ = Filter(
            name="Test Filter",
            owner_id=uuid.UUID(int=1),
            id=1,
            public=False,
            description="Test filter description",
            version_id=0,
        )
        with pytest.raises(ValueError, match="must be an instance of User"):
            filter_.owner = None


class TestFilterContextGuard:
    def test_filter_context_guard_resets_after_closing(self):
        filter_context = fake_build_filter_context()
        filter_output_specification = FilterOutputSpecification(
            properties=[
                FilterOutputSpecificationProperty(
                    name="foo",
                    description="Some property called foo",
                    type=int,
                )
            ],
            tags=("foo", "bar"),
        )
        with filter_context_guard(filter_context, filter_output_specification):
            filter_context.properties["foo"] = 1
            filter_context.tag("foo")
        with pytest.raises(ValueError):
            filter_context.tag("foo")
        with pytest.raises(KeyError):
            filter_context.properties["foo"] = 2

    def test_filter_context_guard_allows_partial_mutability(self):
        filter_context = fake_build_filter_context()
        filter_output_specification = FilterOutputSpecification(
            properties=[
                FilterOutputSpecificationProperty(
                    name="foo",
                    description="Some property called foo",
                    type=int,
                )
            ],
            tags=("foo", "bar"),
        )
        with filter_context_guard(filter_context, filter_output_specification):
            filter_context.properties["foo"] = 1
            filter_context.tag("foo")
            filter_context.tags.add("bar")
            with pytest.raises(KeyError):
                filter_context.properties["baz"] = 2
            with pytest.raises(ValueError):
                filter_context.tag("baz")
            with pytest.raises(ValueError):
                filter_context.tags.add("baz")


@patch("antares.domain.models.filter.alert_satisfies_filter_constraints", autospec=True)
@patch("antares.domain.models.filter.locus_satisfies_filter_constraints", autospec=True)
class TestRunFilterAgainstLocus:
    def test_with_safisfied_constraints_returns_FilterSucceeded(
        self, mock_locus_constraints, mock_alert_constraints
    ):
        locus = build_locus()
        alerts = build_alerts(2)
        timeseries = compute_timeseries(alerts)
        filter_executable = PassingFilter().to_filter_executable()

        mock_locus_constraints.return_value = True
        mock_alert_constraints.return_value = True

        run_output = run_filter_against_locus(
            filter_executable, locus, alerts, timeseries, []
        )
        assert run_output == FilterSucceeded()

    def test_with_non_persistent_properties_is_available_in_properties(
        self, mock_locus_constraints, mock_alert_constraints, capsys
    ):
        locus = build_locus()
        alerts = build_alerts(
            n=2, non_persistent_properties={"ztf_fp_hists": [build_ztf_event_fp_hist()]}
        )
        timeseries = compute_timeseries(alerts)
        filter_executable = FilterUsingNonPersistantProperties().to_filter_executable()

        mock_locus_constraints.return_value = True
        mock_alert_constraints.return_value = True

        run_output = run_filter_against_locus(
            filter_executable, locus, alerts, timeseries, []
        )
        out, err = capsys.readouterr()
        assert run_output == FilterSucceeded()
        assert "NON_PERSISTANT_PROPERTIES_FOUND\n" == out
        for alert in alerts:
            assert "ztf_fp_hists" not in alert.properties

    def test_when_non_persistent_properties_are_not_available_in_properties(
        self, mock_locus_constraints, mock_alert_constraints, capsys
    ):
        locus = build_locus()
        alerts = build_alerts(n=2)
        timeseries = compute_timeseries(alerts)
        filter_executable = FilterUsingNonPersistantProperties().to_filter_executable()

        mock_locus_constraints.return_value = True
        mock_alert_constraints.return_value = True

        run_output = run_filter_against_locus(
            filter_executable, locus, alerts, timeseries, []
        )
        out, err = capsys.readouterr()
        assert run_output == FilterSucceeded()
        assert "NON_PERSISTANT_PROPERTIES_FOUND\n" != out

    def test_with_unsafisfied_locus_constraints_returns_FilterSkipped(
        self, mock_locus_constraints, mock_alert_constraints
    ):
        locus = build_locus()
        alerts = build_alerts(2)
        timeseries = compute_timeseries(alerts)
        filter_executable = PassingFilter().to_filter_executable()

        mock_locus_constraints.return_value = False
        mock_alert_constraints.return_value = True

        run_output = run_filter_against_locus(
            filter_executable, locus, alerts, timeseries, []
        )
        assert run_output == FilterSkipped()

    def test_with_unsafisfied_alert_constraints_returns_FilterSkipped(
        self, mock_locus_constraints, mock_alert_constraints
    ):
        locus = build_locus()
        alerts = build_alerts(2)
        timeseries = compute_timeseries(alerts)
        filter_executable = PassingFilter().to_filter_executable()

        mock_locus_constraints.return_value = True
        mock_alert_constraints.return_value = False

        run_output = run_filter_against_locus(
            filter_executable, locus, alerts, timeseries, []
        )
        assert run_output == FilterSkipped()

    def test_with_an_error_returns_FilterErrored(
        self, mock_locus_constraints, mock_alert_constraints
    ):
        class ErroringFilter(FilterDevkitApi):
            def run(self, locus):
                raise ValueError

        locus = build_locus()
        alerts = build_alerts(2)
        timeseries = compute_timeseries(alerts)
        filter_executable = ErroringFilter().to_filter_executable()

        mock_locus_constraints.return_value = True
        mock_alert_constraints.return_value = True

        run_output = run_filter_against_locus(
            filter_executable, locus, alerts, timeseries, []
        )
        assert isinstance(run_output, FilterErrored)


class TestLocusSatisfiesFilterConstraints:
    class LocusInputPropsFilter(FilterDevkitApi):
        INPUT_LOCUS_PROPERTIES = ["required_locus_property"]

        def run(self, locus):
            pass

    class LocusInputTagsFilter(FilterDevkitApi):
        INPUT_TAGS = ["required_locus_tag"]

        def run(self, locus):
            pass

    class LocusRequiredPropsFilter(FilterDevkitApi):
        REQUIRED_LOCUS_PROPERTIES = ["required_locus_property"]

        def run(self, locus):
            pass

    class LocusRequiredTagsFilter(FilterDevkitApi):
        REQUIRED_TAGS = ["required_locus_tag"]

        def run(self, locus):
            pass

    # INPUT_ prefixes (to be deprecated)

    def test_with_input_locus_prop_is_true(self):
        locus = build_locus(properties={"required_locus_property": True})
        executable = self.LocusInputPropsFilter().to_filter_executable()
        assert locus_satisfies_filter_constraints(locus, executable)

    def test_without_input_locus_prop_is_false(self):
        locus = build_locus()
        executable = self.LocusInputPropsFilter().to_filter_executable()
        assert not locus_satisfies_filter_constraints(locus, executable)

    def test_with_input_locus_tag_is_true(self):
        locus = build_locus(tags={"required_locus_tag": True})
        executable = self.LocusInputTagsFilter().to_filter_executable()
        assert locus_satisfies_filter_constraints(locus, executable)

    def test_without_input_locus_tag_is_false(self):
        locus = build_locus()
        executable = self.LocusInputTagsFilter().to_filter_executable()
        assert not locus_satisfies_filter_constraints(locus, executable)

    # REQUIRED_ prefixes

    def test_with_required_locus_prop_is_true(self):
        locus = build_locus(properties={"required_locus_property": True})
        executable = self.LocusRequiredPropsFilter().to_filter_executable()
        assert locus_satisfies_filter_constraints(locus, executable)

    def test_without_required_locus_prop_is_false(self):
        locus = build_locus()
        executable = self.LocusRequiredPropsFilter().to_filter_executable()
        assert not locus_satisfies_filter_constraints(locus, executable)

    def test_with_required_locus_tag_is_true(self):
        locus = build_locus(tags={"required_locus_tag": True})
        executable = self.LocusRequiredTagsFilter().to_filter_executable()
        assert locus_satisfies_filter_constraints(locus, executable)

    def test_without_required_locus_tag_is_false(self):
        locus = build_locus()
        executable = self.LocusRequiredTagsFilter().to_filter_executable()
        assert not locus_satisfies_filter_constraints(locus, executable)


class TestAlertSatisfiesFilterConstraints:
    class AlertInputPropsFilter(FilterDevkitApi):
        INPUT_ALERT_PROPERTIES = ["required_alert_property"]

        def run(self, locus):
            pass

    class AlertRequiredPropsFilter(FilterDevkitApi):
        REQUIRED_ALERT_PROPERTIES = ["required_alert_property"]

        def run(self, locus):
            pass

    class AlertRequiredGravWaveProbRegionFilter(FilterDevkitApi):
        REQUIRED_GRAV_WAVE_PROB_REGION = 50

        def run(self, locus):
            pass

    def test_with_input_alert_instance(self):
        alert = build_alert(properties={"required_alert_property": True})
        executable = self.AlertInputPropsFilter().to_filter_executable()
        with pytest.raises(ValueError) as err:
            alert_satisfies_filter_constraints(alert, executable)
            assert (
                "Alert properties contains survey specific"
                " properties and normalized properties separated."
            ) in err

    # INPUT_ prefixes (to be deprecated)

    def test_with_input_alert_prop_is_true(self):
        alert = build_alert(properties={"required_alert_property": True})
        filter_context_alert = build_filter_context_alert(alert, "locus-001")
        executable = self.AlertInputPropsFilter().to_filter_executable()
        assert alert_satisfies_filter_constraints(filter_context_alert, executable)

    def test_without_input_alert_prop_is_false(self):
        alert = build_alert()
        filter_context_alert = build_filter_context_alert(alert, "locus-001")
        executable = self.AlertInputPropsFilter().to_filter_executable()
        assert not alert_satisfies_filter_constraints(filter_context_alert, executable)

    # REQUIRED_ prefixes

    def test_with_required_alert_prop_is_true(self):
        alert = build_alert(properties={"required_alert_property": True})
        filter_context_alert = build_filter_context_alert(alert, "locus-001")
        executable = self.AlertRequiredPropsFilter().to_filter_executable()
        assert alert_satisfies_filter_constraints(filter_context_alert, executable)

    def test_without_required_alert_prop_is_false(self):
        alert = build_alert()
        filter_context_alert = build_filter_context_alert(alert, "locus-001")
        executable = self.AlertRequiredPropsFilter().to_filter_executable()
        assert not alert_satisfies_filter_constraints(filter_context_alert, executable)

    def test_with_required_grav_wave_prob_region_is_true(self):
        alert = build_alert()
        alert.grav_wave_events.append(
            AlertGravWaveEvent(gracedb_id="test1", contour_level=5, contour_area=40)
        )
        filter_context_alert = build_filter_context_alert(alert, "locus-001")
        executable = self.AlertRequiredGravWaveProbRegionFilter().to_filter_executable()
        assert alert_satisfies_filter_constraints(filter_context_alert, executable)

    def test_without_required_grav_wave_prob_region_is_false(self):
        alert = build_alert()
        filter_context_alert = build_filter_context_alert(alert, "locus-001")
        executable = self.AlertRequiredGravWaveProbRegionFilter().to_filter_executable()
        assert not alert_satisfies_filter_constraints(filter_context_alert, executable)
