import uuid

from antares.domain.models import WatchList


class TestWatchList:
    def test_watch_list_without_id(self):
        watch_list = WatchList(
            owner_id=uuid.UUID(int=123),
            name="My WL",
            description="",
            slack_channel="A00BCDEFGHI",
            enabled=False,
        )
        assert watch_list.id is not None

    def test_watch_list_without_slack_channel(self):
        watch_list = WatchList(
            owner_id=uuid.UUID(int=123),
            name="My WL",
            description="",
            enabled=False,
            id=uuid.UUID(int=0),
        )
        assert watch_list.slack_channel == ""

    def test_watch_list_without_enabled(self):
        watch_list = WatchList(
            owner_id=uuid.UUID(int=123),
            name="My WL",
            description="",
            slack_channel="A00BCDEFGHI",
            id=uuid.UUID(int=0),
        )
        assert watch_list.enabled is True
