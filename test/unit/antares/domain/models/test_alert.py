from copy import deepcopy

import pytest

from antares.domain.models.alert import (
    Alert,
    AlertGravWaveEvent,
    AlertNormalizedProperties,
    Survey,
)


class TestSurvey:
    @pytest.mark.parametrize(
        "value, expected",
        [(1, Survey.SURVEY_ZTF), (2, Survey.SURVEY_ZTF), (3, Survey.SURVEY_DECAT)],
    )
    def test_survey_from_int(self, value, expected):
        assert Survey.from_int(value) == expected

    def test_survey_from_int_raise_valueerror(self):
        with pytest.raises(ValueError) as err:
            Survey.from_int(4)
            assert "Unrecognized integer survey representation" in err


class TestAlertGravWaveEvent:
    def test_minimal_initialize(self):
        gw_event = AlertGravWaveEvent(
            gracedb_id="MS1234", contour_level=57.456, contour_area=35.675
        )
        assert isinstance(gw_event, dict)


class TestAlert:
    @pytest.fixture
    def norm_props(self):
        return AlertNormalizedProperties(
            ant_mjd=12345678.123,
            ant_time_received=12345678,
            ant_input_msg_time=12345678,
            ant_passband="passband",
            ant_maglim=123.45,
            ant_survey=1,
        )

    @pytest.fixture
    def gw_event(self):
        return AlertGravWaveEvent(
            gracedb_id="MS1234", contour_level=57.456, contour_area=35.675
        )

    def test_minimal_initialize(self, norm_props):
        alert = Alert(
            id="alert_id",
            location=None,
            mjd=12345678.123,
            survey=Survey.SURVEY_ZTF,
            normalized_properties=norm_props,
        )
        assert isinstance(alert, Alert)

    def test_upsert_grav_wave_event_once(self, norm_props, gw_event):
        alert = Alert(
            id="alert_id",
            location=None,
            mjd=12345678.123,
            survey=Survey.SURVEY_ZTF,
            normalized_properties=norm_props,
        )
        assert alert.grav_wave_events == []
        alert.upsert_grav_wave_event(gw_event)
        assert alert.grav_wave_events == [gw_event]

    def test_upsert_multiple_grav_wave_events(self, norm_props, gw_event):
        alert = Alert(
            id="alert_id",
            location=None,
            mjd=12345678.123,
            survey=Survey.SURVEY_ZTF,
            normalized_properties=norm_props,
        )
        gw_event_2 = deepcopy(gw_event)
        gw_event_3 = deepcopy(gw_event)
        gw_event_2["contour_area"] = 89.98
        gw_event_3["gracedb_id"] = "other"
        assert alert.grav_wave_events == []
        alert.upsert_grav_wave_event(gw_event)
        assert alert.grav_wave_events == [gw_event]
        alert.upsert_grav_wave_event(gw_event_2)
        assert alert.grav_wave_events == [gw_event_2]
        alert.upsert_grav_wave_event(gw_event_3)
        assert alert.grav_wave_events == [gw_event_2, gw_event_3]

    def test_upsert_non_persistent_properties(self, norm_props):
        alert = Alert(
            id="alert_id",
            location=None,
            mjd=12345678.123,
            survey=Survey.SURVEY_ZTF,
            normalized_properties=norm_props,
        )
        assert alert.non_persistent_properties == {}
        non_persistent_properties = {
            "ztf_fp_hists": [
                {
                    "a": "a",
                    "b": 1,
                }
            ]
        }
        alert.update_non_persistent_properties(non_persistent_properties)
        assert (
            alert.non_persistent_properties["ztf_fp_hists"]
            == non_persistent_properties["ztf_fp_hists"]
        )
