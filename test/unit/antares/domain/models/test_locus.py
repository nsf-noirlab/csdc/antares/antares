import uuid
from test.fakes import build_catalog_object, build_locus, build_watch_object

import pytest

from antares.domain.models import LocusAnnotation


class TestLocusAnnotation:
    def test_locus_annotation_without_id(self):
        locus_annotation = LocusAnnotation(
            owner_id=uuid.UUID(int=1),
            locus_id="locus-001",
            comment="a" * 255,
            favorite=True,
        )
        assert locus_annotation.id is None

    def test_locus_annotation_without_comment(self):
        locus_annotation = LocusAnnotation(
            owner_id=uuid.UUID(int=1), locus_id="locus-001", id=1, favorite=True
        )
        assert locus_annotation.comment == ""

    def test_locus_annotation_without_favorite(self):
        locus_annotation = LocusAnnotation(
            owner_id=uuid.UUID(int=1), locus_id="locus-001", id=1, comment="a" * 255
        )
        assert not locus_annotation.favorite

    def test_locus_annotation_invalid_comment_raise_valueerror(self):
        with pytest.raises(ValueError) as err:
            LocusAnnotation(
                id=1, owner_id=uuid.UUID(int=1), locus_id="locus-001", comment="a" * 256
            )
            assert "LocusAnnotation comment must be <= 255 characters" in err

    def test_locus_annotation_validates_comment_field_on_update(self):
        locus_annotation = LocusAnnotation(
            id=1, owner_id=uuid.UUID(int=1), locus_id="locus-001", comment="a" * 255
        )
        locus_annotation.set_comment("this is okay")
        with pytest.raises(ValueError):
            locus_annotation.set_comment("a" * 256)

    def test_locus_annotation_set_favorite(self):
        locus_annotation = LocusAnnotation(
            owner_id=uuid.UUID(int=1), locus_id="locus-001", id=1, comment="a" * 255
        )
        locus_annotation.set_favorite(True)
        assert locus_annotation.favorite


class TestLocus:
    def test_add_catalog_object_association_updates_catalogs_member_with_catalog_id(
        self,
    ):
        locus = build_locus()
        catalog_object = build_catalog_object(catalog_name="catalog-001")
        locus.add_catalog_object_association(catalog_object)
        assert "catalog-001" in locus.catalogs

    def test_add_watch_object_association_updates(self):
        locus = build_locus()
        watch_object = build_watch_object(watch_list_id=uuid.UUID(int=1))
        locus.add_watch_object_association(watch_object)
        assert (uuid.UUID(int=1), watch_object.id) in locus.watch_object_matches

    def test_add_grav_wave_event_association(self):
        locus = build_locus()
        assert locus.grav_wave_events == []
        locus.add_grav_wave_event_association("MS1234")
        assert locus.grav_wave_events == ["MS1234"]
        locus.add_grav_wave_event_association("MS1234")
        assert locus.grav_wave_events == ["MS1234"]
        locus.add_grav_wave_event_association("MS4321")
        assert locus.grav_wave_events == ["MS1234", "MS4321"]
