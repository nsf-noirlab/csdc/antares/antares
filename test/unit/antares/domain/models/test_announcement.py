from antares.domain.models import Announcement


class TestAnnouncement:
    def test_announcement_without_id(self):
        announcement = Announcement(
            message="test",
            variant="primary",
            active=True,
        )
        assert announcement.id is None

    def test_announcement_without_active(self):
        announcement = Announcement(
            message="test",
            variant="primary",
            id=1,
        )
        assert announcement.active is False
