import datetime

from passlib.hash import pbkdf2_sha256

from antares.domain.models import JwtRecord, User


class TestUser:
    def test_user_without_id(self):
        user = User(name="John Doe", username="jdoe", email="john.doe@test.test")
        assert user.id is None

    def test_user_set_password_stores_hash(self):
        user = User(
            id=1, name="Nic Wolf", username="nwolf", email="nic.wolf@noirlab.edu"
        )
        assert user.password_hash is None
        user_password = "supersecret"
        user.set_password(user_password)
        assert user.password_hash is not None
        assert user.password_hash != user_password

    def test_user_check_password_verifies_hash(self):
        user = User(
            id=1, name="Nic Wolf", username="nwolf", email="nic.wolf@noirlab.edu"
        )
        assert user.password_hash is None
        user_password = "supersecret"
        user.set_password(user_password)
        assert user.check_password(user_password)
        assert pbkdf2_sha256.verify(user_password, user.password_hash)

    def test_user_password_cant_be_verified_if_no_password_set(self):
        user = User(
            id=1, name="Nic Wolf", username="nwolf", email="nic.wolf@noirlab.edu"
        )
        assert user.password_hash is None
        assert not user.check_password("")


class TestJwtRecord:
    def test_jwt_record_without_token_blocklist_entry_id(self):
        jwt_record = JwtRecord(
            token_type="refresh", jti="1", expires=datetime.datetime(2023, 10, 13)
        )
        assert jwt_record.token_blocklist_entry_id is None
