import pytest
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import CatalogObject


class TestCatalogObject:
    def test_catalog_object_with_proper_id_and_catalog_id(self):
        id_value = "1"
        catalog_id_value = "2"
        catalog_name = "catalog_name"
        name = "name"
        catalog_object = CatalogObject(
            id=id_value,
            catalog_id=catalog_id_value,
            catalog_name=catalog_name,
            location=SkyCoord(ra=0, dec=0, unit=u.deg),
            radius=Angle("1s"),
            properties={},
            name=name,
        )
        assert catalog_object.id == id_value
        assert catalog_object.catalog_id == catalog_id_value
        assert catalog_object.catalog_name == catalog_name
        assert catalog_object.name == name

    def test_catalog_object_with_wrong_id_raises_valueerror(self):
        with pytest.raises(ValueError) as err:
            CatalogObject(
                id="a1",
                catalog_id="2",
                catalog_name="catalog_name",
                location=SkyCoord(ra=0, dec=0, unit=u.deg),
                radius=Angle("1s"),
                properties={},
                name="test",
            )
            assert "id must be string representation of a base 10 integer" in err

    def test_catalog_object_with_wrong_catalog_id_raises_valueerror(self):
        with pytest.raises(ValueError) as err:
            CatalogObject(
                id="1",
                catalog_id="a2",
                catalog_name="catalog_name",
                location=SkyCoord(ra=0, dec=0, unit=u.deg),
                radius=Angle("1s"),
                properties={},
                name="test",
            )
            assert "id must be string representation of a base 10 integer" in err
