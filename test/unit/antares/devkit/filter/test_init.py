import logging
from test.fakes import build_alert, build_filter_context, build_locus
from test.unit.antares.devkit.filters_for_test import (
    TFilterCrash,
    TFilterHaltPipeline,
    TFilterTagMessage,
    TFilterWithInputTag,
    TFilterWithRequiredAlertProperty,
    TFilterWithRequiredLocusProperty,
    TFilterWriteLocus,
)
from unittest.mock import patch

import pytest
from astropy.coordinates import SkyCoord

from antares.devkit.filter import _run_filter, run_filter, run_many
from antares.domain.models import Locus

test_data_run_filter = [TFilterTagMessage, TFilterTagMessage()]


@pytest.mark.usefixtures("message_bus")
@pytest.mark.parametrize("tfilter", test_data_run_filter)
def test_run_filter_add_tag(tfilter):
    report = run_filter(tfilter, locus=build_filter_context())
    assert report["locus_id"] == "locus-001"
    assert report["new_tags"] == {"message"}


@pytest.mark.usefixtures("message_bus")
class Test_RunFilter:
    @patch("antares.devkit.filter.get_locus_id", return_value="locus-001")
    @patch("antares.devkit.filter.get_locus", return_value=build_filter_context())
    def test_running_filter_without_locus(self, mock_get_locus_id, mock_get_locus):
        report = _run_filter(TFilterTagMessage().to_filter_executable())
        assert report["locus_id"] == "locus-001"
        assert report["new_tags"] == {"message"}

    def test_running_filter_raises_valueerror_due_to_invalid_argument(self):
        with pytest.raises(ValueError) as err:
            _run_filter(TFilterTagMessage(), locus=1)
            assert "Invalid argument passed" in str(err)

    def test_running_filter_raises_haltpipeline_sets_raised_halt_to_true(self):
        report = _run_filter(
            TFilterHaltPipeline().to_filter_executable(), locus=build_filter_context()
        )
        assert report["raised_halt"] is True

    def test_running_filter_crash_and_log_exception(self, caplog):
        with caplog.at_level(logging.DEBUG):
            _run_filter(
                TFilterCrash().to_filter_executable(), locus=build_filter_context()
            )
            assert "Filter crashed" in caplog.text

    def test_running_filter_modify_locus_property(self):
        report = _run_filter(
            TFilterWriteLocus().to_filter_executable(), locus=build_filter_context()
        )
        assert report["locus_data"].properties["test_property"] == "bar"

    def test_running_filter_on_locus_that_satisfies_tag_filter_requirements(self):
        report = _run_filter(
            TFilterWithInputTag().to_filter_executable(),
            locus=build_filter_context(build_locus(tags={"some_tag"})),
        )
        assert report["locus_id"] == "locus-001"

    def test_filter_skip_locus_that_doesnt_satisfy_tag_filter_requirements(
        self, capsys
    ):
        report = _run_filter(
            TFilterWithInputTag().to_filter_executable(), locus=build_filter_context()
        )
        out, err = capsys.readouterr()
        assert "Locus locus-001 skipped because it" in out
        assert report == {}

    def test_running_filter_on_locus_that_satisfies_locus_filter_requirements(self):
        report = _run_filter(
            TFilterWithRequiredLocusProperty().to_filter_executable(),
            locus=build_filter_context(
                build_locus(properties={"some_locus_property": 1})
            ),
        )
        assert report["locus_id"] == "locus-001"

    def test_filter_skip_locus_that_doesnt_satisfy_locus_filter_requirements(
        self, capsys
    ):
        report = _run_filter(
            TFilterWithRequiredLocusProperty().to_filter_executable(),
            locus=build_filter_context(),
        )
        out, err = capsys.readouterr()
        assert "Locus locus-001 skipped because it" in out
        assert report == {}

    def test_running_filter_on_locus_that_satisfies_alert_filter_requirements(self):
        report = _run_filter(
            TFilterWithRequiredAlertProperty().to_filter_executable(),
            locus=build_filter_context(
                locus=build_locus(),
                alerts=[build_alert(properties={"some_alert_property": 1})],
            ),
        )
        assert report["locus_id"] == "locus-001"

    def test_filter_skip_locus_that_doesnt_satisfy_alert_filter_requirements(
        self, capsys
    ):
        report = _run_filter(
            TFilterWithRequiredAlertProperty().to_filter_executable(),
            locus=build_filter_context(),
        )
        out, err = capsys.readouterr()
        assert "Locus locus-001 skipped because Alert" in out
        assert report == {}


@pytest.mark.usefixtures("message_bus", "alert_repository")
@pytest.mark.with_data(Locus("locus-001", SkyCoord("0d 0d"), properties={}))
@patch("antares.devkit.filter.get_locus_ids", return_value=["locus-001"])
@patch("antares.devkit.filter.get_locus", return_value=build_filter_context())
class TestRunMany:
    test_data_run_many = [
        (TFilterTagMessage, {"message"}),
        (TFilterTagMessage(), set()),
    ]

    @pytest.mark.parametrize("tfilter, message", test_data_run_many)
    def test_run_many_add_tag(
        self, mock_get_locus, mock_get_locus_ids, tfilter, message
    ):
        report = run_many(tfilter, n=1, locus_ids=["locus-001"])
        assert report["results"][0]["locus_id"] == "locus-001"
        assert report["results"][0]["new_tags"] == message

    def test_running_filter_without_locus_ids_and_n_raises_runtimeerror(
        self, mock_get_locus, mock_get_locus_ids
    ):
        with pytest.raises(RuntimeError) as err:
            run_many(TFilterTagMessage(), n=0)
            assert "run_many() requires either `alert_ids` or `n`." in str(err)

    def test_running_filter_without_keeping_results_return_report_without_results(
        self, mock_get_locus, mock_get_locus_ids
    ):
        report = run_many(
            TFilterTagMessage(), n=1, locus_ids=["locus-001"], keep_results=False
        )
        assert report["results"] is None

    def test_running_filter_with_n_and_without_locus_ids_use_random_locus(
        self, mock_get_locus, mock_get_locus_ids
    ):
        report = run_many(TFilterTagMessage(), n=1)
        assert report["results"][0]["locus_id"] == "locus-001"
        assert report["results"][0]["new_tags"] == set()

    def test_running_filter_on_locus_that_satisfies_tag_filter_requirements(
        self, mock_get_locus, mock_get_locus_ids, capsys
    ):
        mock_get_locus.side_effect = [
            build_filter_context(),
            build_filter_context(build_locus(id="locus-002", tags={"some_tag"})),
        ]
        mock_get_locus_ids.return_value = ["locus-001", "locus-002"]
        report = run_many(TFilterWithInputTag(), n=2)
        assert len(report["results"]) == 1
        assert report["n"] == 2
        out, err = capsys.readouterr()
        assert "Locus locus-001 skipped because it" in out

    def test_filter_skip_all_locus(self, mock_get_locus, mock_get_locus_ids):
        mock_get_locus.return_value = build_filter_context()
        mock_get_locus_ids.return_value = ["locus-001"]
        report = run_many(TFilterWithInputTag(), n=1)
        assert report == {
            "n": 1,
            "results": None,
            "t_50_percentile": None,
            "t_90_percentile": None,
            "t_95_percentile": None,
            "t_99_percentile": None,
        }

    def test_filter_skip_locus_that_doesnt_satisfy_locus_filter_requirements(
        self, mock_get_locus, mock_get_locus_ids, capsys
    ):
        mock_get_locus.side_effect = [
            build_filter_context(),
            build_filter_context(
                locus=build_locus(id="locus-002", properties={"some_locus_property": 1})
            ),
        ]
        mock_get_locus_ids.return_value = ["locus-001", "locus-002"]
        report = run_many(TFilterWithRequiredLocusProperty(), n=2)
        assert len(report["results"]) == 1
        assert report["n"] == 2
        out, err = capsys.readouterr()
        assert "Locus locus-001 skipped because it" in out

    def test_filter_skip_locus_that_doesnt_satisfy_alert_filter_requirements(
        self, mock_get_locus, mock_get_locus_ids, capsys
    ):
        mock_get_locus.side_effect = [
            build_filter_context(),
            build_filter_context(
                locus=build_locus(id="locus-002"),
                alerts=[build_alert(properties={"some_alert_property": 1})],
            ),
        ]
        mock_get_locus_ids.return_value = ["locus-001", "locus-002"]
        report = run_many(TFilterWithRequiredAlertProperty(), n=1)
        assert len(report["results"]) == 1
        assert report["n"] == 2
        out, err = capsys.readouterr()
        assert "Locus locus-001 skipped because Alert" in out
