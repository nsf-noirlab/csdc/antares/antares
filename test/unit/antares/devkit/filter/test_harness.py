from test.fakes import build_filter_context
from test.unit.antares.devkit.filters_for_test import (
    TFilterCrash,
    TFilterHaltPipeline,
    TFilterOutputs,
    TFilterWithoutLocusDescription,
    TFilterWithoutLocusName,
    TFilterWithoutLocusType,
    TFilterWithoutTagDescription,
    TFilterWithoutTagName,
)
from unittest.mock import MagicMock

import numpy as np
import pytest

from antares.devkit.filter.harness import (
    FilterConfigError,
    LocusFilterAdapter,
    RunnableFilter,
    _validate_filter_name,
    _validate_property_name,
    _validate_property_type,
    _validate_property_type_definition,
    _validate_property_value,
    _validate_tag_name,
)
from antares.domain.models import FilterContextAlert
from antares.exceptions import HaltPipeline
from antares.utils.collections_extensions import (
    RestrictedWriteDictWrapper,
    RestrictedWriteSetWrapper,
)


class TestRunnableFilter:
    @pytest.fixture()
    def runnable_filter(self):
        return RunnableFilter(TFilterOutputs())

    def test_init_filter(self, runnable_filter):
        assert type(runnable_filter._filter) == TFilterOutputs

    def test_setup_filter(self, runnable_filter):
        runnable_filter._validate_filter_config = MagicMock()
        runnable_filter._load_files = MagicMock()
        returned_value = runnable_filter.setup()
        assert returned_value is None
        runnable_filter._validate_filter_config.assert_called_once()
        runnable_filter._load_files.assert_called_once()

    def test_filter_runs_on_a_locus(self, runnable_filter):
        locus = build_filter_context()
        returned_value = runnable_filter.run(locus)
        assert returned_value is None

    def test_filter_validate_locus_property(self, runnable_filter):
        value = runnable_filter.validate_locus_property("test_property", 1000)
        assert value == 1000

    def test_filter_validate_locus_property_undeclared_raises_runtimeerror(
        self, runnable_filter
    ):
        with pytest.raises(RuntimeError) as err:
            runnable_filter.validate_locus_property("not_test_property", 1000)
        assert "Undeclared Locus property" in str(err)

    def test_filter_validate_locus_property_none_returns_none(self, runnable_filter):
        value = runnable_filter.validate_locus_property("test_property", None)
        assert value is None

    def test_filter_validate_alert_property_int_returns_int(self, runnable_filter):
        value = runnable_filter.validate_alert_property("test_property", 1000)
        assert isinstance(value, int)

    def test_filter_validate_alert_property_undeclared_raises_runtimeerror(
        self, runnable_filter
    ):
        with pytest.raises(RuntimeError) as err:
            runnable_filter.validate_alert_property("not_test_property", 1000)
        assert "Undeclared Alert property" in str(err)

    def test_filter_validate_alert_property_none_returns_none(self, runnable_filter):
        value = runnable_filter.validate_alert_property("test_property", None)
        assert value is None

    def test_filter_validate_tag(self, runnable_filter):
        returned_value = runnable_filter.validate_tag("test_property")
        assert returned_value is None

    def test_filter_validate_tag_undeclared_raises_runtimeerror(self, runnable_filter):
        with pytest.raises(RuntimeError) as err:
            runnable_filter.validate_tag("not_test_property")
        assert "Undeclared Tag" in str(err)

    def test_filter_validate_filter_config(self, runnable_filter):
        returned_value = runnable_filter._validate_filter_config()
        assert returned_value is None

    def test_filter_id(self, runnable_filter):
        assert runnable_filter.filter_id is None

    def test_filter_version_id(self, runnable_filter):
        assert runnable_filter.filter_version_id is None

    def test_filter_name_is_assigned(self, runnable_filter):
        assert runnable_filter.name == "TEST FILTER"

    def test_filter_level(self, runnable_filter):
        assert runnable_filter.level is None

    def test_filter_priority(self, runnable_filter):
        assert runnable_filter.priority is None

    def test_filter_disable_isnt_implemented_yet(self, runnable_filter):
        with pytest.raises(NotImplementedError):
            runnable_filter.disable("log_id")

    def test_filter_get_config_returns_config(self, runnable_filter):
        assert TFilterOutputs.OUTPUT_TAGS == runnable_filter.get_config("OUTPUT_TAGS")

    def test_filter_str_returns_filter_string_representation(self, runnable_filter):
        assert str(runnable_filter).startswith("<RunnableFilter")

    def test_filter_run_raises_haltpipeline(self):
        locus = build_filter_context()
        filter = RunnableFilter(TFilterHaltPipeline())
        with pytest.raises(HaltPipeline):
            filter.run(locus)

    def test_filter_run_crash_returns_traceback(self):
        locus = build_filter_context()
        filter = RunnableFilter(TFilterCrash)
        traceback = filter.run(locus)
        assert traceback.startswith("Traceback")

    def test_runnable_filter_validate_filter_config_without_locus_name_raises_filterconfigerror(
        self,
    ):
        filter = RunnableFilter(TFilterWithoutLocusName)
        with pytest.raises(FilterConfigError) as err:
            filter._validate_filter_config()
        assert 'OUTPUT_LOCUS_PROPERTIES must have a "name"' in str(err)

    def test_runnable_filter_validate_filter_config_without_locus_type_raises_filterconfigerror(
        self,
    ):
        filter = RunnableFilter(TFilterWithoutLocusType)
        with pytest.raises(FilterConfigError) as err:
            filter._validate_filter_config()
        assert 'OUTPUT_LOCUS_PROPERTIES must have a "type"' in str(err)

    def test_runnable_filter_validate_filter_config_without_locus_description_raises_filterconfigerror(
        self,
    ):
        filter = RunnableFilter(TFilterWithoutLocusDescription)
        with pytest.raises(FilterConfigError) as err:
            filter._validate_filter_config()
        assert 'OUTPUT_LOCUS_PROPERTIES must have a "description"' in str(err)

    def test_runnable_filter_validate_filter_config_without_tag_name_raises_filterconfigerror(
        self,
    ):
        filter = RunnableFilter(TFilterWithoutTagName)
        with pytest.raises(FilterConfigError) as err:
            filter._validate_filter_config()
        assert 'OUTPUT_TAGS must have a "name"' in str(err)

    def test_runnable_filter_validate_filter_config_without_tag_description_raises_filterconfigerror(
        self,
    ):
        filter = RunnableFilter(TFilterWithoutTagDescription)
        with pytest.raises(FilterConfigError) as err:
            filter._validate_filter_config()
        assert 'OUTPUT_TAGS must have a "description"' in str(err)


@pytest.mark.parametrize("input_test", ["int", "float", "str"])
def test_allowed_property_types(input_test):
    returned_value = _validate_property_type_definition(input_test)
    assert returned_value is None


def test_raises_for_unrecognized_string_raises_filterconfigerror():
    with pytest.raises(FilterConfigError) as err:
        _validate_property_type_definition("dict")
    assert 'Output property type "dict" must be one of' in str(err)


def test_validate_filter_name():
    returned_value = _validate_filter_name("TEST NAME")
    assert returned_value is None


def test_validate_filter_name_of_len_31_fail_and_raises_assertionerror():
    with pytest.raises(FilterConfigError):
        _validate_filter_name("a" * 31)


@pytest.mark.parametrize("test_input", ["1", "a" * 101])
def test_validate_property_name(test_input):
    with pytest.raises(FilterConfigError):
        returned_value = _validate_property_name(test_input)
        assert returned_value is None


@pytest.mark.parametrize("test_input", ["1", "a" * 101, "a" * 250])
def test_validate_tag_name(test_input):
    with pytest.raises(FilterConfigError):
        returned_value = _validate_tag_name(test_input)
        assert returned_value is None


@pytest.mark.parametrize("input_value", [None, float("nan"), np.nan])
def test_validate_properties_value(input_value):
    assert _validate_property_value(input_value) is None


test_data_test_validate_property_value = [(np.float64(1), float), (np.int64(1), int)]


@pytest.mark.parametrize(
    "input_value, expected_type", test_data_test_validate_property_value
)
def test_validate_property_value_instance(input_value, expected_type):
    assert isinstance(_validate_property_value(input_value), expected_type)


test_data_validate_property_type_instance = [
    (1, "int", int),
    (1.0, "float", float),
    ("str", "str", str),
]


@pytest.mark.parametrize(
    "input_value, input_type, expected_type", test_data_validate_property_type_instance
)
def test_validate_property_type_instance(input_value, input_type, expected_type):
    assert isinstance(_validate_property_type(input_value, input_type), expected_type)


def test_validate_property_type_dict_raises_filterconfigerror():
    with pytest.raises(FilterConfigError) as err:
        _validate_property_type({}, "dict")
    assert 'Output property type "dict" must be one of' in str(err)


class TestLocusFilterAdapter:
    @pytest.fixture
    def locus_adapter(self):
        locus = build_filter_context()
        return LocusFilterAdapter(
            locus,
            writeable_properties=["test_property", "writeable"],
            writeable_tags=["test_property", "writeable"],
        )

    def test_alert(self, locus_adapter):
        last_alert = locus_adapter.alert
        assert isinstance(last_alert, FilterContextAlert)

    def test_halt_property(self, locus_adapter):
        assert isinstance(locus_adapter.halt(), HaltPipeline)

    def test_properties_are_instances_of_restrictedwritedictwrapper(
        self, locus_adapter
    ):
        assert isinstance(locus_adapter.properties, RestrictedWriteDictWrapper)

    def test_tags_are_instances_of_restrictedwritesetwrapper(self, locus_adapter):
        assert isinstance(locus_adapter.tags, RestrictedWriteSetWrapper)
