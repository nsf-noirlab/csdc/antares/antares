from test.fakes import (
    build_alert,
    build_client_locus,
    build_filter_context,
    build_locus,
)
from unittest.mock import create_autospec, patch

import pytest

import antares
import antares.devkit as dk
from antares.adapters.repository import (
    AbstractAlertRepository,
    AbstractCatalogObjectRepository,
    AbstractLocusRepository,
)


@patch("antares.bootstrap.bootstrap", autospec=True)
@patch("antares_client.search.search", autospec=True)
@patch("antares_client.search.get_by_id", autospec=True)
def test_init_uses_antares_bootstrap(
    mock_client_get_by_id,
    mock_client_search,
    mock_bootstrap,
    capsys,
):
    mock_client_get_by_id.return_value = build_client_locus()
    mock_client_search.return_value = [build_client_locus()]
    dk.init()
    out, err = capsys.readouterr()
    assert f"ANTARES v{antares.__version__} DevKit is ready!" in out
    mock_bootstrap.assert_called_once()


@patch("antares_client.search.search", autospec=True)
def test_init_doesnt_find_locus(
    mock_client,
    capsys,
):
    mock_client.return_value = []
    dk.init()
    out, err = capsys.readouterr()
    assert "contact ANTARES team for support" in out


def test_get_ligo_events_isnt_implemented_yet():
    with pytest.raises(NotImplementedError):
        dk.get_ligo_events()
