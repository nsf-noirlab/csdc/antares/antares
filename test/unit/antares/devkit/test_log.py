import logging
import sys

from antares.devkit.log import log_init


def test_log_init_level_none():
    log_init(level=None)


def test_log_init_system_wide_true():
    test_logger = logging.getLogger("antares")
    stream_handler = logging.StreamHandler(sys.stdout)
    test_logger.addHandler(stream_handler)
    log_init(level="DEBUG", system_wide=True)
