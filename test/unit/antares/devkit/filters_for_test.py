import antares.devkit as dk
from antares.exceptions import HaltPipeline


class TFilterOutputs(dk.Filter):
    NAME = "TEST FILTER"
    OUTPUT_TAGS = [
        {
            "name": "test_property",
            "description": "description",
        },
    ]
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "test_property",
            "type": "int",
            "description": "test_description",
        },
    ]
    OUTPUT_ALERT_PROPERTIES = [
        {
            "name": "test_property",
            "type": "int",
            "description": "test_description",
        },
    ]

    def run(self, locus):
        print(locus.locus_id)


class TFilterHaltPipeline(dk.Filter):
    def run(self, locus):
        raise HaltPipeline


class TFilterCrash(dk.Filter):
    def run(self, locus):
        print("Locus doesn't have an id property", locus.id)


class TFilterWithoutLocusName(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "type": "int",
            "description": "test_description",
        },
    ]

    def run(self, locus):
        print(locus.locus_id)


class TFilterWithoutLocusType(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "test_property",
            "description": "test_description",
        },
    ]


class TFilterWithoutLocusDescription(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "type": "int",
            "name": "test_property",
        },
    ]


class TFilterWithoutTagName(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "test_property",
            "type": "int",
            "description": "test_description",
        },
    ]
    OUTPUT_TAGS = [
        {
            "description": "description",
        },
    ]


class TFilterWithoutTagDescription(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "test_property",
            "type": "int",
            "description": "test_description",
        },
    ]
    OUTPUT_TAGS = [
        {
            "name": "test_property",
        },
    ]


class TFilterTagMessage(dk.Filter):
    OUTPUT_TAGS = [
        {
            "name": "message",
            "description": "adds tag",
        },
    ]

    def run(self, locus):
        locus.tag("message")


class TFilterWriteLocus(dk.Filter):
    OUTPUT_LOCUS_PROPERTIES = [
        {
            "name": "test_property",
            "type": "str",
            "description": "test_description",
        },
    ]

    def run(self, locus):
        locus.properties["test_property"] = "bar"


class TFilterWithInputTag(dk.Filter):
    REQUIRED_TAGS = [
        "some_tag",
    ]

    def run(self, locus):
        pass


class TFilterWithRequiredLocusProperty(dk.Filter):
    REQUIRED_LOCUS_PROPERTIES = [
        "some_locus_property",
    ]

    def run(self, locus):
        pass


class TFilterWithRequiredAlertProperty(dk.Filter):
    REQUIRED_ALERT_PROPERTIES = [
        "some_alert_property",
    ]

    def run(self, locus):
        pass
