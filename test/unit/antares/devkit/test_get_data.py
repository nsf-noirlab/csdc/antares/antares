import tempfile
from test.fakes import build_client_locus, build_filter_context, build_notice
from unittest.mock import patch

import pytest

from antares import query
from antares.devkit import get_data


@patch("antares_client.search.get_by_ztf_object_id", autospec=True)
def test_get_locus_by_ztf_object_id(mock_client):
    locus_id_to_search = "client-locus-001"
    mock_client.return_value = build_client_locus(locus_id=locus_id_to_search)
    locus_found = get_data.get_locus(
        locus_id=None,
        alert_id=None,
        ztf_object_id="ztf_object_id",
    )
    assert locus_found.locus_id == locus_id_to_search


@pytest.mark.xfail(reason="TODO")
def test_get_locus_using_locus_id_and_alert_id_none():
    assert False


def test_get_locus_id_by_alert_id_isnt_implemented_yet():
    with pytest.raises(NotImplementedError):
        get_data.get_locus(
            locus_id=None,
            alert_id=True,
            ztf_object_id=None,
        )


@patch("antares_client.search.get_by_id", autospec=True)
def test_get_locus_by_locus_id_returns_locus_found(mock_client):
    locus_id_to_search = "client-locus-001"
    mock_client.return_value = build_client_locus(locus_id=locus_id_to_search)
    locus_found = get_data.get_locus(
        locus_id=locus_id_to_search,
        alert_id=None,
        ztf_object_id=None,
    )
    assert locus_found.locus_id == locus_id_to_search


@patch("antares_client.search.get_by_id", autospec=True)
def test_get_locus_by_locus_id_returns_none_because_it_didnt_find_it(mock_client):
    mock_client.return_value = None
    locus_found = get_data.get_locus(
        locus_id="client-locus-001",
        alert_id=None,
        ztf_object_id=None,
    )
    assert locus_found is None


@patch("antares_client.search.search", autospec=True)
def test_get_locus_id_none_returns_random_locus_id(mock_client):
    mock_client.return_value = [build_filter_context()]
    random_locus_id = get_data.get_locus_id()
    assert random_locus_id is not None
    assert isinstance(random_locus_id, str)


@patch("antares_client.search.search", autospec=True)
def test_get_locus_with_n_returns_n_locus_ids(mock_client):
    n_wanted_locus = 5
    mock_client.return_value = [build_filter_context() for _ in range(n_wanted_locus)]
    locus_ids = get_data.get_locus_ids(n_wanted_locus)
    assert n_wanted_locus == len(locus_ids)


def test_get_locus_ids_greater_than_1000_raises_valueerror():
    n_wanted_locus = 1001
    with pytest.raises(ValueError) as err:
        get_data.get_locus_ids(n_wanted_locus)
        assert "n must be less than or equal to 1000" in str(err)


def test_create_locus_from_dict():
    tested_locus = build_filter_context()
    locus_dict = tested_locus.to_dict()
    retrieved_locus = get_data.locus_from_dict(locus_dict)
    assert tested_locus == retrieved_locus


def test_create_locus_from_file(tmpdir):
    tested_locus = build_filter_context()
    file_test_name = tmpdir.join("test_locus_from_file.json")
    tested_locus.to_file(file_test_name)
    retrieved_locus = get_data.locus_from_file(file_test_name)
    assert tested_locus == retrieved_locus


@pytest.mark.usefixtures("message_bus")
def test_upload_file():
    file_key = "foo"
    with tempfile.NamedTemporaryFile("rb+") as f:
        f.write(b"foobar")
        f.seek(0)
        returned_value = get_data.upload_file(key=file_key, file_path=f.name)
        files = query.load_files([file_key])
        assert returned_value is not None
        assert file_key in files


@pytest.mark.usefixtures("message_bus")
def test_upload_file_raise_fileexisterror_due_to_duplicate_key():
    with tempfile.NamedTemporaryFile("rb+") as f:
        f.write(b"foobar")
        f.seek(0)
        get_data.upload_file(key="foo", file_path=f.name)
        with pytest.raises(FileExistsError) as err:
            get_data.upload_file(key="foo", file_path=f.name)
            assert "There is already a file with the same key" in str(err)


@pytest.mark.xfail(reason="TODO")
def test_get_crash_log_retrieves_data_from_database():
    assert False


@pytest.mark.xfail(reason="TODO")
def test_print_crash_log_retrieves_data_from_database():
    assert False


@pytest.mark.xfail(reason="TODO")
def test_locus_from_crash_log_retrieves_data_from_database():
    assert False


def test_get_locus_id_by_ztf_object_id_isnt_implemented_yet():
    with pytest.raises(NotImplementedError):
        get_data.get_locus_id_by_ztf_object_id("ztf_object_id")


@patch("antares_client.search.get_catalog_samples", autospec=True)
def test_get_sample_catalog_data(mock_client_get_catalog_samples):
    get_data.get_sample_catalog_data()
    mock_client_get_catalog_samples.assert_called_once()


@patch("antares_client.search.catalog_search", autospec=True)
def test_search_catalogs(mock_catalog_search):
    get_data.search_catalogs(ra=1, dec=1)
    mock_catalog_search.assert_called_once()


@patch("antares_client.search.get_thumbnails", autospec=True)
def test_get_thumbnails(mock_get_thumbnails):
    get_data.get_thumbnails("some_alert_id")
    mock_get_thumbnails.assert_called_once()


@patch("antares_client.search.get_by_id", autospec=True)
@patch("antares_client.search.get_multiple_grav_wave_notices", autospec=True)
def test_get_locus_by_locus_id_has_grav_wave_events_metadata(
    mock_client_get_latest_grav_wave_notices, mock_client_get_by_id
):
    locus_id_to_search = "client-locus-001"
    mock_client_get_by_id.return_value = build_client_locus(
        locus_id=locus_id_to_search, grav_wave_events=["S1", "S2"]
    )
    mock_client_get_latest_grav_wave_notices.return_value = [
        build_notice(superevent_id="S1"),
        build_notice(superevent_id="S2"),
    ]
    locus_found = get_data.get_locus(
        locus_id=locus_id_to_search,
        alert_id=None,
        ztf_object_id=None,
        include_gravitational_wave_data=True,
    )
    assert locus_found.grav_wave_events_metadata["S1"].gracedb_id == "S1"
    assert locus_found.grav_wave_events_metadata["S2"].gracedb_id == "S2"
