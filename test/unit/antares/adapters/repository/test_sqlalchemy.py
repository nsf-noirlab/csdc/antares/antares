from collections import namedtuple
from datetime import datetime
from test.fakes import build_notice
from unittest.mock import Mock, create_autospec, patch

import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import object_mapper
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.expression import Alias

from antares.adapters.repository import SqlAlchemyGravWaveRepository
from antares.adapters.repository.sqlalchemy import grav_wave_table
from antares.exceptions import KeyViolationException


@pytest.fixture
def session_factory():
    session = create_autospec(Session, instance=True)
    return Mock(return_value=session)


@pytest.fixture
def grav_wave_repo():
    session = create_autospec(Session, instance=True)
    return SqlAlchemyGravWaveRepository(Mock(return_value=session))


class TestSqlAlchemyGravWaveRepository:
    def test_init(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        assert grav_wave_repo.session_factory == session_factory

    def test_add(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        notice = build_notice()
        grav_wave_repo.add(notice)
        session = session_factory()
        session.add.assert_called_once_with(notice)
        session.commit.assert_called_once()
        session.close.assert_called_once()

    def test_add_returns_key_violation(self, session_factory):
        notice = build_notice()
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        session = session_factory()
        session.commit.side_effect = IntegrityError("test", "test2", "test3")
        with pytest.raises(KeyViolationException):
            grav_wave_repo.add(notice)
        session.add.assert_called_once_with(notice)
        session.commit.assert_called_once()
        session.close.assert_called_once()

    def test_add_returns_general_exception(self, session_factory):
        notice = build_notice()
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        session = session_factory()
        session.commit.side_effect = KeyError()
        with pytest.raises(KeyError):
            grav_wave_repo.add(notice)
        session.add.assert_called_once_with(notice)
        session.commit.assert_called_once()
        session.close.assert_called_once()

    def test_get_returns_one_notice(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        notice = build_notice()
        session = session_factory()
        session.query().get.return_value = notice
        returned_notice = grav_wave_repo.get(1)
        assert returned_notice == notice
        session.query().get.assert_called_once_with(1)
        session.expunge.assert_called_once()
        session.close.assert_called_once()

    def test_get_returns_no_notice(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        session = session_factory()
        session.query().get.return_value = None
        returned_notice = grav_wave_repo.get(1)
        assert returned_notice is None
        session.query().get.assert_called_once_with(1)
        session.expunge.assert_not_called()
        session.close.assert_called_once()

    def test_get_still_closes_session_on_exception(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        session = session_factory()
        session.query().get.side_effect = KeyError
        with pytest.raises(KeyError):
            grav_wave_repo.get(1)
        session.query().get.assert_called_once_with(1)
        session.expunge.assert_not_called()
        session.close.assert_called_once()

    def test_get_id_using_gracedb_id_and_notice_datetime(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        notice = build_notice()
        session = session_factory()
        ReturnID = namedtuple("ReturnID", ["id"])
        session.query().filter_by().first.return_value = ReturnID(1)
        returned_id = grav_wave_repo.get_id(notice.gracedb_id, notice.notice_datetime)
        assert returned_id == 1
        session.query().filter_by.assert_called_with(
            gracedb_id=notice.gracedb_id, notice_datetime=notice.notice_datetime
        )
        session.close.assert_called_once()

    def test_get_id_can_return_none(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        notice = build_notice()
        session = session_factory()
        session.query().filter_by().first.return_value = None
        returned_id = grav_wave_repo.get_id(notice.gracedb_id, notice.notice_datetime)
        assert returned_id is None
        session.query().filter_by.assert_called_with(
            gracedb_id=notice.gracedb_id, notice_datetime=notice.notice_datetime
        )
        session.close.assert_called_once()

    def test_get_id_still_closes_session_on_exception(self, session_factory):
        grav_wave_repo = SqlAlchemyGravWaveRepository(session_factory)
        notice = build_notice()
        session = session_factory()
        session.query().filter_by().first.side_effect = KeyError
        with pytest.raises(KeyError):
            returned_notice = grav_wave_repo.get_id(
                notice.gracedb_id, notice.notice_datetime
            )
        session.query().filter_by.assert_called_with(
            gracedb_id=notice.gracedb_id, notice_datetime=notice.notice_datetime
        )
        session.close.assert_called_once()

    def test_notice_mapped_to_grav_wave_table(self):
        notice = build_notice()
        mapper = object_mapper(notice)
        assert grav_wave_table == mapper.tables[0]


class TestSqlAlchemyGravWaveRepositoryGetLatestActiveNoticeIds:
    def test_returns_empty_set_if_no_notices(self, grav_wave_repo):
        session = grav_wave_repo.session_factory()
        mock_subquery = create_autospec(Alias)
        mock_subquery.c.n = 1
        session.query().filter().subquery.return_value = mock_subquery
        session.query().filter().all.return_value = None
        returned_notices = grav_wave_repo.get_latest_active_notice_ids()
        assert returned_notices == set()
        session.close.assert_called_once()

    def test_returns_notice_ids(self, grav_wave_repo):
        session = grav_wave_repo.session_factory()
        QRes = namedtuple("QRes", "id")
        mock_subquery = create_autospec(Alias)
        mock_subquery.c.n = 1
        session.query().filter().subquery.return_value = mock_subquery
        session.query().filter().filter().all.return_value = [QRes(x) for x in range(3)]
        returned_notices = grav_wave_repo.get_latest_active_notice_ids()
        assert returned_notices == set([0, 1, 2])
        session.close.assert_called_once()

    def test_closes_session_after_exception(self, grav_wave_repo):
        session = grav_wave_repo.session_factory()
        session.query().filter().subquery.side_effect = ValueError
        with pytest.raises(ValueError):
            grav_wave_repo.get_latest_active_notice_ids()
        session.close.assert_called_once()


@patch.object(SqlAlchemyGravWaveRepository, "get_latest_active_notice_ids", autospec=True)
@patch.object(SqlAlchemyGravWaveRepository, "get", autospec=True)
class TestSqlAlchemyGravWaveRepositoryGetCurrentNotices:
    def test_returns_empty_list_if_no_notices(
        self, mock_get, mock_active_ids, grav_wave_repo
    ):
        mock_active_ids.return_value = set()
        returned_notices = grav_wave_repo.get_current_notices()
        assert returned_notices == []
        mock_get.assert_not_called()


    def test_returns_one_current_notice(
        self, mock_get, mock_active_ids, grav_wave_repo
    ):
        mock_active_ids.return_value = set([1])
        notice = build_notice()
        mock_get.return_value = notice
        returned_notices = grav_wave_repo.get_current_notices()
        assert returned_notices == [notice]
        mock_get.assert_called_once_with(grav_wave_repo, 1)

    def test_caches_notice(
        self, mock_get, mock_active_ids, grav_wave_repo
    ):
        mock_active_ids.return_value = set([1])
        notice = build_notice()
        mock_get.return_value = notice
        returned_notices = grav_wave_repo.get_current_notices()
        assert returned_notices == [notice]
        mock_get.assert_called_once_with(grav_wave_repo, 1)
        returned_notices = grav_wave_repo.get_current_notices()
        mock_get.assert_called_once()

    def test_drops_notices_that_arent_current(
        self, mock_get, mock_active_ids, grav_wave_repo
    ):
        mock_active_ids.return_value = set([1])
        notice = build_notice()
        mock_get.return_value = notice
        returned_notices = grav_wave_repo.get_current_notices()
        assert returned_notices == [notice]
        mock_get.assert_called_once_with(grav_wave_repo, 1)

        mock_active_ids.return_value = set()
        returned_notices = grav_wave_repo.get_current_notices()
        assert returned_notices == []

