import logging
from collections import OrderedDict
from test.fakes import build_watch_object
from unittest.mock import create_autospec, patch
from uuid import UUID

import pytest
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData, Row
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter
from google.cloud.bigtable.row_set import RowSet
from google.cloud.bigtable.table import Table

from antares.adapters.repository.bigtable import BigtableWatchObjectRepository
from antares.adapters.repository.bigtable.schemas import WatchObjectColumns
from antares.adapters.repository.bigtable.utils import (
    AlertLookupType,
    create_prefix_row_set,
    encode_any,
    get_optimal_htm_level,
)
from antares.domain.models import WatchObject


@pytest.fixture
def mock_bigtable_watch_object_repository():
    mock_instance = create_autospec(BigtableInstance, instance=True)
    bt_watch_object_repository = BigtableWatchObjectRepository(
        mock_instance,
        "watch_object",
        "W",
        "alert_lut",
        "A",
    )
    bt_watch_object_repository.watch_object_table = create_autospec(
        Table, instance=True
    )
    bt_watch_object_repository.alert_lut = create_autospec(Table, instance=True)
    return bt_watch_object_repository


def build_watch_object_row_mock(watch_object: WatchObject):
    watch_object_row_mock = create_autospec(PartialRowData, instance=True)
    watch_object_row_mock.row_key = WatchObjectColumns.create_row_key(
        str(watch_object.watch_list_id), str(watch_object.id)
    )

    watch_object_row_mock.cells = OrderedDict(
        {
            "W": OrderedDict(
                {
                    WatchObjectColumns.NAME: watch_object.name.encode(),
                    WatchObjectColumns.RA: encode_any(
                        watch_object.location.ra.deg, "double"
                    ),
                    WatchObjectColumns.DEC: encode_any(
                        watch_object.location.dec.deg, "double"
                    ),
                    WatchObjectColumns.HTM_LEVEL: get_optimal_htm_level(
                        watch_object.radius
                    ),
                    WatchObjectColumns.RADIUS: encode_any(
                        watch_object.radius.deg, "double"
                    ),
                }
            )
        }
    )

    def mock_cell_value(column_family_id, column):
        if column == WatchObjectColumns.NAME:
            return watch_object.name.encode()
        elif column == WatchObjectColumns.RA:
            return encode_any(watch_object.location.ra.deg, "double")
        elif column == WatchObjectColumns.DEC:
            return encode_any(watch_object.location.dec.deg, "double")
        elif column == WatchObjectColumns.HTM_LEVEL:
            return get_optimal_htm_level(watch_object.radius)
        elif column == WatchObjectColumns.RADIUS:
            return encode_any(watch_object.radius.deg, "double")

    watch_object_row_mock.cell_value = mock_cell_value
    return watch_object_row_mock


def build_watch_object_row_mock_with_missing_columns(watch_object: WatchObject):
    watch_object_row_mock = create_autospec(PartialRowData, instance=True)
    watch_object_row_mock.row_key = WatchObjectColumns.create_row_key(
        str(watch_object.watch_list_id), str(watch_object.id)
    )

    watch_object_row_mock.cells = OrderedDict(
        {
            "W": OrderedDict(
                {
                    WatchObjectColumns.RA: encode_any(
                        watch_object.location.ra.deg, "double"
                    ),
                    WatchObjectColumns.DEC: encode_any(
                        watch_object.location.dec.deg, "double"
                    ),
                    WatchObjectColumns.HTM_LEVEL: get_optimal_htm_level(
                        watch_object.radius
                    ),
                    WatchObjectColumns.RADIUS: encode_any(
                        watch_object.radius.deg, "double"
                    ),
                }
            )
        }
    )

    def mock_cell_value(column_family_id, column):
        if column == WatchObjectColumns.RA:
            return encode_any(watch_object.location.ra.deg, "double")
        elif column == WatchObjectColumns.DEC:
            return encode_any(watch_object.location.dec.deg, "double")
        elif column == WatchObjectColumns.HTM_LEVEL:
            return get_optimal_htm_level(watch_object.radius)
        elif column == WatchObjectColumns.RADIUS:
            return encode_any(watch_object.radius.deg, "double")

    watch_object_row_mock.cell_value = mock_cell_value
    return watch_object_row_mock


def build_alert_lut_row_mock(htm: int, type_value: int, value: str) -> Row:
    alert_lut_row_mock = create_autospec(PartialRowData, instance=True)
    alert_lut_row_mock.row_key = f"{htm}#{type_value}#{value}".encode()
    return alert_lut_row_mock


@pytest.mark.usefixtures("message_bus")
class TestBigtableWatchObjectRepository:
    def test_create_watch_object_from_row(self, mock_bigtable_watch_object_repository):
        watch_object = build_watch_object()
        watch_object_mock_row = build_watch_object_row_mock(watch_object)
        retrieved_watch_object = (
            mock_bigtable_watch_object_repository.create_watch_object_from_row(
                watch_object_mock_row
            )
        )
        assert isinstance(retrieved_watch_object, WatchObject)
        assert watch_object == retrieved_watch_object

    def test_create_watch_object_from_row_without_name_col(
        self, mock_bigtable_watch_object_repository
    ):
        watch_object = build_watch_object()
        watch_object_mock_row = build_watch_object_row_mock_with_missing_columns(
            watch_object
        )
        retrieved_watch_object = (
            mock_bigtable_watch_object_repository.create_watch_object_from_row(
                watch_object_mock_row
            )
        )
        assert isinstance(retrieved_watch_object, WatchObject)
        assert retrieved_watch_object.name == ""
        assert watch_object.id == retrieved_watch_object.id
        assert watch_object.radius == retrieved_watch_object.radius
        assert watch_object.location == retrieved_watch_object.location
        assert watch_object.watch_list_id == retrieved_watch_object.watch_list_id

    def test_create_watch_object_row(self, mock_bigtable_watch_object_repository):
        direct_row_mock = (
            mock_bigtable_watch_object_repository.watch_object_table.direct_row
        )
        watch_object = build_watch_object()
        mock_bigtable_watch_object_repository.create_watch_object_row(watch_object)
        direct_row_mock.assert_called_with(
            f"{watch_object.watch_list_id}#{watch_object.id}".encode()
        )

    def test_add(
        self, mock_bigtable_watch_object_repository, successful_mutate_row_mock
    ):
        mock_row_sample = create_autospec(DirectRow, instance=True)
        direct_row_mock = (
            mock_bigtable_watch_object_repository.watch_object_table.direct_row
        )
        direct_row_mock.return_value = mock_row_sample
        alert_lut_mutate_rows_mock = (
            mock_bigtable_watch_object_repository.alert_lut.mutate_rows
        )
        alert_lut_mutate_rows_mock.return_value = [successful_mutate_row_mock]
        watch_object = build_watch_object()
        mock_bigtable_watch_object_repository.add(watch_object)
        direct_row_mock.assert_called_with(
            f"{watch_object.watch_list_id}#{watch_object.id}".encode()
        )
        mock_row_sample.commit.assert_called_once()

    def test_add_fails_writing_multiple_rows(
        self,
        mock_bigtable_watch_object_repository,
        caplog,
        unsuccessful_mutate_row_mock,
    ):
        mock_row_sample = create_autospec(DirectRow, instance=True)
        direct_row_mock = (
            mock_bigtable_watch_object_repository.watch_object_table.direct_row
        )
        direct_row_mock.return_value = mock_row_sample
        alert_lut_mutate_rows_mock = (
            mock_bigtable_watch_object_repository.alert_lut.mutate_rows
        )
        alert_lut_mutate_rows_mock.return_value = [unsuccessful_mutate_row_mock]
        watch_object = build_watch_object()
        with caplog.at_level(logging.ERROR):
            mock_bigtable_watch_object_repository.add(watch_object)
            direct_row_mock.assert_called_with(
                f"{watch_object.watch_list_id}#{watch_object.id}".encode()
            )
            mock_row_sample.commit.assert_called_once()
            assert "1 errors while writing watch_objects in alert_lut" in caplog.text

    def test_list_by_location_returns_none(self, mock_bigtable_watch_object_repository):
        watch_object_read_rows_mock = (
            mock_bigtable_watch_object_repository.watch_object_table.read_rows
        )
        watch_object_read_rows_mock.return_value = []
        alert_lut_read_rows_mock = (
            mock_bigtable_watch_object_repository.alert_lut.read_rows
        )
        alert_lut_read_rows_mock.return_value = []
        retrieved_watch_objects = list(
            mock_bigtable_watch_object_repository.list_by_location(
                SkyCoord(ra=0, dec=0, unit=u.deg)
            )
        )
        assert retrieved_watch_objects == []

    @patch(
        "antares.adapters.repository.bigtable.utils.AlertLookupMethods.create_row_set_for_htm_levels",
        autospec=True,
    )
    def test_list_by_location_returns_none_when_alert_lut_row_set_is_empty(
        self, mock_create_row_set_for_htm_levels, mock_bigtable_watch_object_repository
    ):
        mock_create_row_set_for_htm_levels.return_value = RowSet()
        retrieved_watch_objects = list(
            mock_bigtable_watch_object_repository.list_by_location(
                SkyCoord(ra=0, dec=0, unit=u.deg)
            )
        )
        assert retrieved_watch_objects == []

    def test_list_by_location_returns_none_when_watch_object_row_set_is_empty(
        self, mock_bigtable_watch_object_repository
    ):
        alert_lut_read_rows_mock = (
            mock_bigtable_watch_object_repository.alert_lut.read_rows
        )
        alert_lut_read_rows_mock.return_value = []
        retrieved_watch_objects = list(
            mock_bigtable_watch_object_repository.list_by_location(
                SkyCoord(ra=0, dec=0, unit=u.deg)
            )
        )
        assert retrieved_watch_objects == []

    def test_list_by_location_returns_watch_objects(
        self, mock_bigtable_watch_object_repository
    ):
        alert_lut_read_rows_mock = (
            mock_bigtable_watch_object_repository.alert_lut.read_rows
        )
        watch_list_id_object_id = (
            "11d1eb21-5ff6-400c-89db-d95648665280:503e7ab3-7800-4f62-9051-5a619d0fe621"
        )
        alert_lut_read_rows_mock.return_value = [
            build_alert_lut_row_mock(
                htm,
                AlertLookupType.WATCH_OBJECT.value,
                watch_list_id_object_id,
            )
            for htm in (
                66571993088,
                3968,
                63488,
                1015808,
                16252928,
                260046848,
                4160749568,
            )
        ]
        watch_object_read_rows_mock = (
            mock_bigtable_watch_object_repository.watch_object_table.read_rows
        )
        location = SkyCoord(ra=0, dec=0, unit=u.deg)
        watch_object_read_rows_mock.return_value = [
            build_watch_object_row_mock(
                build_watch_object(location=location, radius=Angle(0, unit=u.deg))
            ),
            build_watch_object_row_mock(
                build_watch_object(location=location, radius=Angle(1, unit=u.deg))
            ),
        ]
        retrieved_watch_objects = list(
            mock_bigtable_watch_object_repository.list_by_location(location)
        )
        assert len(retrieved_watch_objects) == 1

    def test_list_by_watch_list_id_returns_none(
        self, mock_bigtable_watch_object_repository
    ):
        read_rows_mock = (
            mock_bigtable_watch_object_repository.watch_object_table.read_rows
        )
        read_rows_mock.return_value = []
        retrieved_watch_objects = list(
            mock_bigtable_watch_object_repository.list_by_watch_list_id(UUID(int=1))
        )
        assert retrieved_watch_objects == []

    @patch(
        "antares.adapters.repository.bigtable.watch_object.create_prefix_row_set",
        autospec=True,
    )
    def test_list_by_watch_list_id_returns_none_when_row_set_is_empty(
        self, mock_create_prefix_row_set, mock_bigtable_watch_object_repository
    ):
        mock_create_prefix_row_set.return_value = RowSet()
        retrieved_watch_objects = list(
            mock_bigtable_watch_object_repository.list_by_watch_list_id(UUID(int=1))
        )
        assert retrieved_watch_objects == []

    def test_list_by_watch_list_id_returns_watch_objects(
        self, mock_bigtable_watch_object_repository
    ):
        read_rows_mock = (
            mock_bigtable_watch_object_repository.watch_object_table.read_rows
        )
        watch_list_id = UUID(int=1)
        read_rows_mock.return_value = [
            build_watch_object_row_mock(
                build_watch_object(name="A", watch_list_id=watch_list_id)
            ),
            build_watch_object_row_mock(
                build_watch_object(name="B", watch_list_id=watch_list_id)
            ),
        ]
        retrieved_watch_objects = list(
            mock_bigtable_watch_object_repository.list_by_watch_list_id(watch_list_id)
        )
        assert len(retrieved_watch_objects) == 2
        read_rows_mock.assert_called_with(
            filter_=CellsColumnLimitFilter(1),
            row_set=create_prefix_row_set(str(watch_list_id)),
        )
