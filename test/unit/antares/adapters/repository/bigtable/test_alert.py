import logging
from datetime import datetime
from test.fakes import build_alert
from test.utils import trim_to_milliseconds
from unittest.mock import create_autospec, patch

import bson
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter
from google.cloud.bigtable.row_set import RowSet
from google.cloud.bigtable.table import Table
from pytest import fixture, mark, raises

from antares.adapters.repository.bigtable.alert import (
    BigtableAlertRepository,
    MissingAlertError,
)
from antares.adapters.repository.bigtable.schemas import AlertColumns
from antares.adapters.repository.bigtable.utils import create_prefix_row_set, encode_any
from antares.domain.models import Alert


@fixture
def mock_bigtable_alert_repository():
    mock_instance = create_autospec(spec=BigtableInstance, instance=True)
    bt_alert_repository = BigtableAlertRepository(
        instance_=mock_instance,
        alert_table_id="alert",
        alert_cf="A",
        locus_by_alert_id_table_id="locus_by_alert_id",
        locus_by_alert_id_cf="L",
    )
    bt_alert_repository.alert_table = create_autospec(spec=Table, instance=True)
    bt_alert_repository.alert_by_locus_id_table = create_autospec(
        spec=Table, instance=True
    )
    return bt_alert_repository


def build_alert_row_mock(alert: Alert, locus_id: str):
    alert_row_mock = create_autospec(spec=PartialRowData, instance=True)
    alert_row_mock.row_key = AlertColumns.create_row_key(locus_id, alert.mjd, alert.id)

    def mock_cell_value(column_family_id, column):
        if column == AlertColumns.PROPERTIES:
            properties = (
                alert.properties
                | alert.normalized_properties
                | {"ant_grav_wave_events": alert.grav_wave_events}
            )
            return bson.dumps(properties)
        elif column == AlertColumns.INSTANCE_CREATED_DATETIME:
            return encode_any(alert.created_at, "timestamp")

    alert_row_mock.cell_value = mock_cell_value
    return alert_row_mock


@mark.usefixtures("message_bus")
class TestBigtableAlertRepository:
    def test_get_returns_none_when_key_doesnt_exist(
        self, mock_bigtable_alert_repository
    ):
        alert_id = "key_that_doesnt_exist"
        lookup_read_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.read_row
        )
        lookup_read_row_mock.return_value = None
        retrieved_alert = mock_bigtable_alert_repository.get(alert_id)
        assert retrieved_alert is None
        lookup_read_row_mock.assert_called_with(
            alert_id, filter_=CellsColumnLimitFilter(1)
        )

    @patch(
        "antares.adapters.repository.bigtable.alert.create_prefix_row_set",
        autospec=True,
    )
    def test_get_returns_none_when_row_set_is_empty(
        self, mock_create_prefix_row_set, mock_bigtable_alert_repository
    ):
        mock_create_prefix_row_set.return_value = RowSet()
        lookup_read_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.read_row
        )
        lookup_mock_row = lookup_read_row_mock.return_value
        lookup_mock_row.cell_value.return_value = b"ANT2022test"
        retrieved_alert = mock_bigtable_alert_repository.get("TEST22asdfgh-0000000000")
        assert retrieved_alert is None

    def test_get_raises_valueerror_when_alert_doesnt_exist(
        self, mock_bigtable_alert_repository
    ):
        lookup_read_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.read_row
        )
        lookup_mock_row = lookup_read_row_mock.return_value
        lookup_mock_row.cell_value.return_value = b"ANT2022test"
        mock_bigtable_alert_repository.alert_table.read_rows.return_value = []
        alert_id = "TEST22asdfgh-0000000000"
        with raises(MissingAlertError) as err:
            mock_bigtable_alert_repository.get(alert_id)
            assert (
                f"There's a locus_id_by_alert_id: {alert_id} but the alert"
                " doesn't exist"
            ) in err
        lookup_read_row_mock.assert_called_with(
            alert_id, filter_=CellsColumnLimitFilter(1)
        )

    def test_get_returns_alert(self, mock_bigtable_alert_repository):
        lookup_read_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.read_row
        )
        lookup_mock_row = lookup_read_row_mock.return_value
        locus_id = "ANT2022test"
        lookup_mock_row.cell_value.return_value = locus_id.encode()
        alert_read_rows_mock = mock_bigtable_alert_repository.alert_table.read_rows
        alert_id = "TEST22asdfgh-0000000000"
        expected_alert = build_alert(
            id=alert_id, created_at=trim_to_milliseconds(datetime.utcnow())
        )
        wanted_alert_mock_row = build_alert_row_mock(expected_alert, locus_id)
        unwanted_alert_mock_row = build_alert_row_mock(
            build_alert(id="TEST22asdfgh0000000001"), locus_id
        )
        alert_read_rows_mock.return_value = [
            unwanted_alert_mock_row,
            wanted_alert_mock_row,
        ]
        retrieved_alert = mock_bigtable_alert_repository.get(alert_id)
        assert retrieved_alert == expected_alert
        lookup_read_row_mock.assert_called_with(
            alert_id, filter_=CellsColumnLimitFilter(1)
        )
        expected_rowset = create_prefix_row_set(locus_id)
        alert_read_rows_mock.assert_called_with(
            filter_=CellsColumnLimitFilter(1), row_set=expected_rowset
        )

    def test_list_by_locus_id_returns_none(self, mock_bigtable_alert_repository):
        alert_read_rows_mock = mock_bigtable_alert_repository.alert_table.read_rows
        alert_read_rows_mock.return_value = []
        locus_id = "key_that_doesnt_exist"
        retrieved_alerts = mock_bigtable_alert_repository.list_by_locus_id(locus_id)
        assert len(retrieved_alerts) == 0
        expected_rowset = create_prefix_row_set(locus_id)
        alert_read_rows_mock.assert_called_with(
            filter_=CellsColumnLimitFilter(1), row_set=expected_rowset
        )

    @patch(
        "antares.adapters.repository.bigtable.alert.create_prefix_row_set",
        autospec=True,
    )
    def test_list_by_locus_id_returns_none_when_row_set_is_empty(
        self, mock_create_prefix_row_set, mock_bigtable_alert_repository
    ):
        mock_create_prefix_row_set.return_value = RowSet()
        retrieved_alerts = mock_bigtable_alert_repository.list_by_locus_id(
            "ANT2022test"
        )
        assert len(retrieved_alerts) == 0

    def test_list_by_locus_id_returns_alert(self, mock_bigtable_alert_repository):
        alert_read_rows_mock = mock_bigtable_alert_repository.alert_table.read_rows
        alert_id = "TEST22asdfgh-0000000000"
        expected_alert = build_alert(
            id=alert_id,
            mjd=59003.10,
            created_at=trim_to_milliseconds(datetime.utcnow()),
        )
        locus_id = "ANT2022test"
        alert_mock_row = build_alert_row_mock(expected_alert, locus_id)
        alert_read_rows_mock.return_value = [alert_mock_row]
        retrieved_alerts = mock_bigtable_alert_repository.list_by_locus_id(locus_id)
        assert len(retrieved_alerts) == 1
        assert isinstance(retrieved_alerts[0], Alert)
        assert retrieved_alerts[0] == expected_alert
        expected_rowset = create_prefix_row_set(locus_id)
        alert_read_rows_mock.assert_called_with(
            filter_=CellsColumnLimitFilter(1), row_set=expected_rowset
        )

    def test_create_locus_by_alert_id_row(self, mock_bigtable_alert_repository):
        lookup_direct_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.direct_row
        )
        alert_id = "TEST22asdfgh0000000001"
        mock_bigtable_alert_repository.create_locus_by_alert_id_row(
            alert_id=alert_id, locus_id="ANT2022test"
        )
        lookup_direct_row_mock.assert_called_with(alert_id.encode())

    @mark.parametrize(
        "alert",
        [
            build_alert(created_at=trim_to_milliseconds(datetime.utcnow())),
            build_alert(
                grav_wave_events=[
                    {"gracedb_id": "MS123", "contour_level": 1.23, "contour_area": 3.21}
                ],
                created_at=trim_to_milliseconds(datetime.utcnow()),
            ),
            build_alert(
                location=None,
                normalized_properties={
                    "ant_ra": None,
                    "ant_dec": None,
                    "ant_survey": 1,
                },
                created_at=trim_to_milliseconds(datetime.utcnow()),
            ),
        ],
    )
    def test_create_alert_from_row(self, mock_bigtable_alert_repository, alert):
        locus_id = "ANT2022test"
        alert_mock_row = build_alert_row_mock(alert, locus_id)
        retrieved_alert = mock_bigtable_alert_repository.create_alert_from_row(
            alert_mock_row
        )
        assert isinstance(alert, Alert)
        assert alert == retrieved_alert

    def test_create_alert_row(self, mock_bigtable_alert_repository):
        alert_direct_row_mock = mock_bigtable_alert_repository.alert_table.direct_row
        alert = build_alert()
        locus_id = "ANT2022test"
        mock_bigtable_alert_repository.create_alert_row(alert, locus_id)
        alert_direct_row_mock.assert_called_with(
            f"{locus_id}#{alert.mjd}#{alert.id}".encode()
        )

    def test_add(self, mock_bigtable_alert_repository):
        lookup_mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        lookup_direct_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.direct_row
        )
        lookup_direct_row_mock.return_value = lookup_mock_row_sample
        alert_mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        alert_direct_row_mock = mock_bigtable_alert_repository.alert_table.direct_row
        alert_direct_row_mock.return_value = alert_mock_row_sample
        alert = build_alert()
        locus_id = "ANT2022test"
        mock_bigtable_alert_repository.add(alert, locus_id)
        lookup_direct_row_mock.assert_called_with(alert.id.encode())
        lookup_mock_row_sample.commit.assert_called_once()
        alert_direct_row_mock.assert_called_with(
            f"{locus_id}#{alert.mjd}#{alert.id}".encode()
        )
        alert_mock_row_sample.commit.assert_called_once()

    def test_add_many_if_not_exists(self, mock_bigtable_alert_repository):
        lookup_mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        lookup_direct_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.direct_row
        )
        lookup_direct_row_mock.return_value = lookup_mock_row_sample
        lookup_mutate_rows_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.mutate_rows
        )
        alert_mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        alert_direct_row_mock = mock_bigtable_alert_repository.alert_table.direct_row
        alert_direct_row_mock.return_value = alert_mock_row_sample
        alert_mutate_rows_mock = mock_bigtable_alert_repository.alert_table.mutate_rows
        alerts_with_locus_id = [(build_alert(), "ANT2022test")]
        mock_bigtable_alert_repository.add_many_if_not_exists(alerts_with_locus_id)
        lookup_direct_row_mock.assert_called_once()
        lookup_mutate_rows_mock.assert_called_once()
        alert_direct_row_mock.assert_called_once()
        alert_mutate_rows_mock.assert_called_once()

    def test_add_many_fails_all(
        self, mock_bigtable_alert_repository, caplog, unsuccessful_mutate_row_mock
    ):
        n_rows = 2
        lookup_mutate_rows_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.mutate_rows
        )
        lookup_mutate_rows_mock.return_value = [
            unsuccessful_mutate_row_mock for _ in range(n_rows)
        ]
        alert_mutate_rows_mock = mock_bigtable_alert_repository.alert_table.mutate_rows
        alert_mutate_rows_mock.return_value = [
            unsuccessful_mutate_row_mock for _ in range(n_rows)
        ]
        alerts_with_locus_id = [(build_alert(), "ANT2022test") for _ in range(n_rows)]
        with caplog.at_level(logging.ERROR):
            mock_bigtable_alert_repository.add_many_if_not_exists(alerts_with_locus_id)
            lookup_mutate_rows_mock.assert_called_once()
            alert_mutate_rows_mock.assert_called_once()
            assert f"{n_rows*2} errors while writing alerts" in caplog.text

    @mark.parametrize(
        "lookup_mutate_row_return_value, alert_mutate_row_return_value",
        [
            ("unsuccessful_mutate_row_mock", "successful_mutate_row_mock"),
            ("successful_mutate_row_mock", "unsuccessful_mutate_row_mock"),
        ],
    )
    def test_add_many_fails_one_write(
        self,
        mock_bigtable_alert_repository,
        caplog,
        lookup_mutate_row_return_value,
        alert_mutate_row_return_value,
        request,
    ):
        lookup_mutate_row_return_value = [
            request.getfixturevalue(lookup_mutate_row_return_value) for _ in range(2)
        ]
        alert_mutate_row_return_value = [
            request.getfixturevalue(alert_mutate_row_return_value) for _ in range(2)
        ]
        n_rows = 2
        lookup_mutate_rows_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.mutate_rows
        )
        lookup_mutate_rows_mock.return_value = lookup_mutate_row_return_value
        alert_mutate_rows_mock = mock_bigtable_alert_repository.alert_table.mutate_rows
        alert_mutate_rows_mock.return_value = alert_mutate_row_return_value
        alerts_with_locus_id = [(build_alert(), "ANT2022test") for _ in range(n_rows)]
        with caplog.at_level(logging.ERROR):
            mock_bigtable_alert_repository.add_many_if_not_exists(alerts_with_locus_id)
            lookup_mutate_rows_mock.assert_called_once()
            alert_mutate_rows_mock.assert_called_once()
            assert f"{n_rows} errors while writing alerts" in caplog.text

    def test__get_locus_id_by_alert_id_returns_none(
        self, mock_bigtable_alert_repository
    ):
        lookup_read_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.read_row
        )
        lookup_read_row_mock.return_value = None
        alert_id = "key_that_doesnt_exist"
        locus_id = mock_bigtable_alert_repository._get_locus_id_by_alert_id(alert_id)
        assert locus_id is None
        lookup_read_row_mock.assert_called_with(
            alert_id, filter_=CellsColumnLimitFilter(1)
        )

    def test__get_locus_id_by_alert_id_returns_id(self, mock_bigtable_alert_repository):
        lookup_read_row_mock = (
            mock_bigtable_alert_repository.locus_by_alert_id_table.read_row
        )
        lookup_mock_row = lookup_read_row_mock.return_value
        locus_id = b"ANT2022test"
        lookup_mock_row.cell_value.return_value = locus_id
        alert_id = "TEST22asdfgh-0000000000"
        retrieved_locus_id = mock_bigtable_alert_repository._get_locus_id_by_alert_id(
            alert_id
        )
        assert retrieved_locus_id == locus_id.decode()
        lookup_read_row_mock.assert_called_with(
            alert_id, filter_=CellsColumnLimitFilter(1)
        )
