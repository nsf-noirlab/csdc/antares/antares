from collections import OrderedDict
from datetime import datetime
from test.fakes import build_watch_list
from test.utils import trim_to_milliseconds
from unittest.mock import create_autospec
from uuid import UUID

from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import ConditionalRow, DirectRow, PartialRowData
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter
from google.cloud.bigtable.table import Table
from pytest import fixture, mark

from antares.adapters.repository.bigtable.schemas import WatchListColumns
from antares.adapters.repository.bigtable.utils import encode_any
from antares.adapters.repository.bigtable.watch_list import (
    BigtableWatchListRepository,
    build_owner_id_filter,
)
from antares.domain.models import WatchList


@fixture
def mock_bigtable_watch_list_repository():
    mock_instance = create_autospec(spec=BigtableInstance, instance=True)
    bt_watch_list_repository = BigtableWatchListRepository(
        instance_=mock_instance,
        watch_list_table_id="watch_list",
        watch_list_cf="W",
    )
    bt_watch_list_repository.watch_list_table = create_autospec(
        spec=Table, instance=True
    )
    return bt_watch_list_repository


def build_watch_list_row_mock(watch_list: WatchList):
    watch_list_row_mock = create_autospec(spec=PartialRowData, instance=True)
    watch_list_row_mock.row_key = str(watch_list.id).encode()

    watch_list_row_mock.cells = OrderedDict(
        {
            "W": OrderedDict(
                {
                    WatchListColumns.OWNER_ID: str(watch_list.owner_id).encode(),
                    WatchListColumns.NAME: watch_list.name.encode(),
                    WatchListColumns.DESCRIPTION: watch_list.description.encode(),
                    WatchListColumns.SLACK_CHANNEL: watch_list.slack_channel.encode(),
                    WatchListColumns.CREATED_DATETIME: encode_any(
                        watch_list.created_at, "timestamp"
                    ),
                    WatchListColumns.ENABLED: encode_any(watch_list.enabled, "bool"),
                }
            )
        }
    )

    def mock_cell_value(column_family_id, column):
        if column == WatchListColumns.OWNER_ID:
            return str(watch_list.owner_id).encode()
        elif column == WatchListColumns.NAME:
            return watch_list.name.encode()
        elif column == WatchListColumns.DESCRIPTION:
            return watch_list.description.encode()
        elif column == WatchListColumns.SLACK_CHANNEL:
            return watch_list.slack_channel.encode()
        elif column == WatchListColumns.CREATED_DATETIME:
            return encode_any(watch_list.created_at, "timestamp")
        elif column == WatchListColumns.ENABLED:
            return encode_any(watch_list.enabled, "bool")

    watch_list_row_mock.cell_value = mock_cell_value
    return watch_list_row_mock


def build_watch_list_row_mock_with_missing_columns(watch_list: WatchList):
    watch_list_row_mock = create_autospec(spec=PartialRowData, instance=True)
    watch_list_row_mock.row_key = str(watch_list.id).encode()

    watch_list_row_mock.cells = OrderedDict(
        {
            "W": OrderedDict(
                {
                    WatchListColumns.OWNER_ID: str(watch_list.owner_id).encode(),
                    WatchListColumns.NAME: watch_list.name.encode(),
                    WatchListColumns.DESCRIPTION: watch_list.description.encode(),
                }
            )
        }
    )

    def mock_cell_value(column_family_id, column):
        if column == WatchListColumns.OWNER_ID:
            return str(watch_list.owner_id).encode()
        elif column == WatchListColumns.NAME:
            return watch_list.name.encode()
        elif column == WatchListColumns.DESCRIPTION:
            return watch_list.description.encode()

    watch_list_row_mock.cell_value = mock_cell_value
    return watch_list_row_mock


@mark.usefixtures("message_bus")
class TestBigtableWatchListRepository:
    def test_create_watchlist_from_row(self, mock_bigtable_watch_list_repository):
        watch_list = build_watch_list(
            created_at=trim_to_milliseconds(datetime.utcnow())
        )
        watch_list_mock_row = build_watch_list_row_mock(watch_list)
        retrieved_watch_list = (
            mock_bigtable_watch_list_repository.create_watchlist_from_row(
                watch_list_mock_row
            )
        )
        assert isinstance(retrieved_watch_list, WatchList)
        assert watch_list == retrieved_watch_list

    def test_create_watchlist_from_row_with_missing_columns(
        self, mock_bigtable_watch_list_repository
    ):
        watch_list = build_watch_list(
            created_at=trim_to_milliseconds(datetime.utcnow())
        )
        watch_list_mock_row = build_watch_list_row_mock_with_missing_columns(watch_list)
        retrieved_watch_list = (
            mock_bigtable_watch_list_repository.create_watchlist_from_row(
                watch_list_mock_row
            )
        )
        assert isinstance(retrieved_watch_list, WatchList)
        assert watch_list.id == retrieved_watch_list.id
        assert watch_list.name == retrieved_watch_list.name
        assert watch_list.description == retrieved_watch_list.description
        assert watch_list.owner_id == retrieved_watch_list.owner_id
        assert retrieved_watch_list.enabled == False
        assert retrieved_watch_list.created_at == datetime.min
        assert retrieved_watch_list.slack_channel == ""

    def test_create_watchlist_row(self, mock_bigtable_watch_list_repository):
        direct_row_mock = (
            mock_bigtable_watch_list_repository.watch_list_table.direct_row
        )
        watch_list = build_watch_list()
        mock_bigtable_watch_list_repository.create_watchlist_row(watch_list)
        direct_row_mock.assert_called_with(str(watch_list.id).encode())

    def test_add(self, mock_bigtable_watch_list_repository):
        mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        direct_row_mock = (
            mock_bigtable_watch_list_repository.watch_list_table.direct_row
        )
        direct_row_mock.return_value = mock_row_sample
        watch_list = build_watch_list()
        mock_bigtable_watch_list_repository.add(watch_list)
        direct_row_mock.assert_called_with(str(watch_list.id).encode())
        mock_row_sample.commit.assert_called_once()

    def test_get_returns_none(self, mock_bigtable_watch_list_repository):
        read_row_mock = mock_bigtable_watch_list_repository.watch_list_table.read_row
        read_row_mock.return_value = None
        watch_list_id = UUID(int=5000)
        retrieved_watch_list = mock_bigtable_watch_list_repository.get(watch_list_id)
        assert retrieved_watch_list is None
        read_row_mock.assert_called_with(
            str(watch_list_id), filter_=CellsColumnLimitFilter(1)
        )

    def test_get_returns_watchlist(self, mock_bigtable_watch_list_repository):
        read_row_mock = mock_bigtable_watch_list_repository.watch_list_table.read_row
        watch_list = build_watch_list(
            created_at=trim_to_milliseconds(datetime.utcnow())
        )
        watch_list_mock_row = build_watch_list_row_mock(watch_list)
        read_row_mock.return_value = watch_list_mock_row
        retrieved_watch_list = mock_bigtable_watch_list_repository.get(watch_list.id)
        assert isinstance(retrieved_watch_list, WatchList)
        assert watch_list == retrieved_watch_list

    def test_list_by_owner_id_returns_none(self, mock_bigtable_watch_list_repository):
        read_rows_mock = mock_bigtable_watch_list_repository.watch_list_table.read_rows
        read_rows_mock.return_value = []
        watch_list_owner_id = UUID(int=5000)
        retrieved_watch_lists = mock_bigtable_watch_list_repository.list_by_owner_id(
            watch_list_owner_id
        )
        assert len(retrieved_watch_lists) == 0
        read_rows_mock.assert_called_once()
        read_rows_mock.assert_called_with(
            filter_=build_owner_id_filter(watch_list_owner_id)
        )

    def test_list_by_owner_id_returns_watch_lists(
        self, mock_bigtable_watch_list_repository
    ):
        read_rows_mock = mock_bigtable_watch_list_repository.watch_list_table.read_rows
        watch_list = build_watch_list()
        read_rows_mock.return_value = [
            build_watch_list_row_mock(watch_list),
            build_watch_list_row_mock(watch_list),
        ]
        retrieved_watch_lists = mock_bigtable_watch_list_repository.list_by_owner_id(
            watch_list.owner_id
        )
        assert len(retrieved_watch_lists) == 2
        read_rows_mock.assert_called_with(
            filter_=build_owner_id_filter(watch_list.owner_id)
        )

    def test_delete(self, mock_bigtable_watch_list_repository):
        repository_row_mock = mock_bigtable_watch_list_repository.watch_list_table.row
        row_mock = create_autospec(spec=ConditionalRow, instance=True)
        repository_row_mock.return_value = row_mock
        watch_list_id = UUID(int=1)
        mock_bigtable_watch_list_repository.delete(watch_list_id)
        repository_row_mock.assert_called_with(
            str(watch_list_id), filter_=CellsColumnLimitFilter(1)
        )
        row_mock.delete.assert_called_once()
        row_mock.commit.assert_called_once()
