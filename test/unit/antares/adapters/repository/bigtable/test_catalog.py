import decimal
import logging
from collections import OrderedDict
from pathlib import Path
from test.fakes import (
    build_bigtable_catalog_object_table_description,
    build_catalog_object_using_table_description,
)
from unittest.mock import call, create_autospec

import pytest
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import Cell, PartialRowData
from google.cloud.bigtable.table import Table

from antares.adapters.repository.bigtable.catalog import (
    BigtableCatalogObjectRepository,
    BigtableCatalogObjectTableDescription,
    check_properties_have_minimal_columns,
    load_schema_values,
)
from antares.adapters.repository.bigtable.schemas import (
    CatalogColumns,
    CatalogLutColumns,
)
from antares.adapters.repository.bigtable.utils import encode_any
from antares.domain.models import CatalogObject

CATALOG_TABLE = build_bigtable_catalog_object_table_description()
CATALOG_TABLE_WITH_NON_NUMERIC_RADIUS = build_bigtable_catalog_object_table_description(
    radius="some_str"
)
CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN = (
    build_bigtable_catalog_object_table_description(
        radius_column="radius",
        radius_unit="degree",
    )
)
CATALOG_TABLE_DESCRIPTIONS_W_RADIUS = build_bigtable_catalog_object_table_description(
    radius=Angle(angle=2, unit=u.arcsecond).deg,
)


@pytest.fixture
def mock_bigtable_catalog_repository(request, tmp_path):
    """
    Attribute of request:
        param : list[BigtableCatalogObjectTableDescription]
    Note:
    The param inside of request are the catalogs,
    this fixture doesn't work when catalogs are pass directly
    """
    schema_file = tmp_path / "table_schemas.yaml"
    schema_file.touch()
    if len(request.param) > 0:
        schema_text = """
            catalog_001:
                ra_deg: double
                dec_deg: double
                name: str
                feature1: str
                feature400: double
                feature314: decimal
        """
        if request.param[0]["object_id_type"] == "int":
            schema_text += """
                id: int
            """
        else:
            schema_text += """
                id: str
            """
        if request.param[0]["radius_column"] and request.param[0]["radius_unit"]:
            schema_text += """
                radius: double
            """
        schema_file.write_text(schema_text)
    mock_instance = create_autospec(BigtableInstance, instance=True)
    mock_instance.table.return_value = create_autospec(Table, instance=True)
    bt_catalog_repository = BigtableCatalogObjectRepository(
        mock_instance,
        "catalog_lut",
        "C",
        request.param,
        str(tmp_path / "table_schemas.yaml"),
    )
    return bt_catalog_repository


def build_catalog_lut_row_mock(htm_level: int, value: str):
    catalog_lut_row_mock = create_autospec(PartialRowData, instance=True)
    catalog_lut_row_mock.row_key = CatalogLutColumns.create_row_key(htm_level, value)
    return catalog_lut_row_mock


def build_catalog_row_mock(
    catalog_object: CatalogObject,
    table_description: BigtableCatalogObjectTableDescription,
):
    catalog_row_mock = create_autospec(PartialRowData, instance=True)
    catalog_row_mock.row_key = CatalogColumns.create_row_key(catalog_object.id)
    catalog_row_mock.cells = OrderedDict({"C": OrderedDict({})})
    for column, data in catalog_object.properties.items():
        if column in table_description["columns_with_data_types"].keys():
            if data is not None:
                catalog_row_mock.cells["C"][column.encode()] = [
                    Cell(
                        value=encode_any(
                            data,
                            table_description["columns_with_data_types"][column],
                        ),
                        timestamp_micros=-1,
                    )
                ]
        else:
            catalog_row_mock.cells["C"][column.encode()] = [
                Cell(value=str(data).encode(), timestamp_micros=-1)
            ]
    return catalog_row_mock


class TestCheckPropertiesHaveMinimalColumns:
    def test_success(self):
        properties = {
            "id": 1,
            "ra_deg": 0.0,
            "dec_deg": 0.0,
            "name": "name",
            "feature1": "value",
            "feature400": 0.1,
            "feature314": decimal.Decimal(0.1),
        }
        assert check_properties_have_minimal_columns(properties, CATALOG_TABLE) is None

    def test_raises_value_error(self):
        with pytest.raises(ValueError) as err:
            check_properties_have_minimal_columns({}, CATALOG_TABLE)
            assert "Catalog row doesn't have the required catalog properties" in err


class TestLoadSchemaValues:
    def test_success(self, tmp_path):
        table_description = build_bigtable_catalog_object_table_description(radius=8.0)
        assert "columns_with_data_types" not in table_description.keys()
        schema_file = tmp_path / "table_schemas.yaml"
        schema_file.touch()
        schema_file.write_text(
            """
            catalog_001:
                id: int
                ra_deg: double
                dec_deg: double
                name: str
                feature1: str
                feature400: double
                feature314: decimal
                radius: double
        """
        )
        updated_schema = load_schema_values(
            tmp_path / "table_schemas.yaml", table_description
        )
        assert "columns_with_data_types" in updated_schema.keys()
        assert updated_schema["radius"] == Angle(8, unit=u.deg)

    def test_raises_file_not_found_error(self, tmp_path):
        with pytest.raises(FileNotFoundError) as err:
            table_description = CATALOG_TABLE
            file_path = Path("path_that_doesnt_exit.yaml")
            load_schema_values(schema_file=file_path, schema=table_description)
            assert f"No such file or directory: '{file_path}'" in err

    def test_raises_non_numeric_radius_error(self, tmp_path):
        table_description = CATALOG_TABLE_WITH_NON_NUMERIC_RADIUS
        schema_file = tmp_path / "table_schemas.yaml"
        schema_file.touch()
        schema_file.write_text(
            """
            catalog_001:
                id: int
                ra_deg: double
                dec_deg: double
                name: str
                feature1: str
                feature400: double
                feature314: decimal
                radius: double
        """
        )
        with pytest.raises(RuntimeError) as err:
            load_schema_values(tmp_path / "table_schemas.yaml", table_description)
            assert "Schema radius must be a number in degrees." in err


@pytest.mark.usefixtures("message_bus")
class TestBigtableCatalogObjectRepository:
    def test_schema_file_is_empty(self, tmp_path):
        schema_file = tmp_path / "table_schemas.yaml"
        schema_file.touch()
        with pytest.raises(ValueError) as err:
            BigtableCatalogObjectRepository(
                create_autospec(BigtableInstance, instance=True),
                "catalog_lut",
                "C",
                [
                    CATALOG_TABLE,
                ],
                str(tmp_path / "table_schemas.yaml"),
            )
            assert "is empty" in err

    def test_schema_file_doesnt_include_catalog(self, tmp_path):
        schema_file = tmp_path / "table_schemas.yaml"
        schema_file.touch()
        schema_file.write_text(
            """
            catalog_002:
                id: int
        """
        )
        with pytest.raises(ValueError) as err:
            BigtableCatalogObjectRepository(
                create_autospec(BigtableInstance, instance=True),
                catalog_lut_id="catalog_lut",
                catalog_lut_cf="C",
                schemas=[
                    CATALOG_TABLE,
                ],
                schema_file=str(tmp_path / "table_schemas.yaml"),
            )
            assert "not in schema_file:" in err

    def test_doesnt_load_disabled_catalogs(self, tmp_path):
        schema_file = tmp_path / "table_schemas.yaml"
        schema_file.touch()
        schema_file.write_text(
            """
            catalog_001:
                id: int
                ra_deg: double
                dec_deg: double
                name: str
                feature1: str
                feature400: double
                feature314: decimal
                radius: double
        """
        )
        bigtable_catalog_repository = BigtableCatalogObjectRepository(
            create_autospec(BigtableInstance, instance=True),
            "catalog_lut",
            "C",
            [
                build_bigtable_catalog_object_table_description(enabled=False),
                CATALOG_TABLE,
            ],
            str(tmp_path / "table_schemas.yaml"),
        )
        assert len(bigtable_catalog_repository._catalogs) == 1

    def test_init_raises_value_error_when_duplicated_catalog_ids(self, tmp_path):
        schema_file = tmp_path / "table_schemas.yaml"
        schema_file.touch()
        schema_file.write_text(
            """
            catalog_001:
                id: int
                ra_deg: double
                dec_deg: double
                name: str
                feature1: str
                feature400: double
                feature314: decimal
                radius: double
        """
        )
        with pytest.raises(ValueError) as err:
            BigtableCatalogObjectRepository(
                create_autospec(BigtableInstance, instance=True),
                "catalog_lut",
                "C",
                [CATALOG_TABLE, CATALOG_TABLE],
                str(tmp_path / "table_schemas.yaml"),
            )
            assert (
                "There are some duplicates of catalog_id in catalog table descriptions"
                in err
            )

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE]],
        indirect=True,
    )
    def test_get_catalog_table_with_description(
        self,
        mock_bigtable_catalog_repository,
    ):
        (
            _,
            retrieved_catalog,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        assert retrieved_catalog == {
            "table": "catalog_001",
            "column_family": "C",
            "catalog_id": 1,
            "display_name": "Catalog 001",
            "enabled": True,
            "ra_column": "ra_deg",
            "dec_column": "dec_deg",
            "object_id_column": "id",
            "object_id_type": "int",
            "object_name_column": "name",
            "radius": None,
            "radius_column": None,
            "radius_unit": None,
            "schema_file": "catalog_schema.yaml",
            "columns_with_data_types": {
                "ra_deg": "double",
                "dec_deg": "double",
                "name": "str",
                "feature1": "str",
                "feature400": "double",
                "feature314": "decimal",
                "id": "int",
            },
        }

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[]],
        indirect=True,
    )
    def test_get_catalog_table_with_description_raises_value_error_when_id_is_not_ind(
        self, mock_bigtable_catalog_repository
    ):
        with pytest.raises(ValueError) as err:
            mock_bigtable_catalog_repository.get_catalog_table_with_description("1")
            assert "catalog_id must be an integer. Received type: <class 'str'>" in err

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[]],
        indirect=True,
    )
    def test_get_catalog_table_with_description_raises_value_error_when_catalog_not_found(
        self, mock_bigtable_catalog_repository
    ):
        with pytest.raises(ValueError) as err:
            mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
            assert (
                "Catalog table description could not be found for catalog id: 1" in err
            )

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[]],
        indirect=True,
    )
    def test_add_without_table_description_raises_error(
        self, mock_bigtable_catalog_repository
    ):
        with pytest.raises(ValueError) as err:
            mock_bigtable_catalog_repository.add(
                build_catalog_object_using_table_description(
                    {
                        "id": 3,
                        "ra_deg": 0.0,
                        "dec_deg": 0.0,
                        "radius": 0.0019444444444444444,
                        "name": "Catalog Object 003",
                    },
                    CATALOG_TABLE,
                )
            )
            assert "Catalog table description could not be found for catalog id:" in err

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE]],
        indirect=True,
    )
    def test_add_without_radius_in_table_description_and_object_raises_error(
        self, mock_bigtable_catalog_repository
    ):
        (
            _,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        with pytest.raises(ValueError) as err:
            mock_bigtable_catalog_repository.add(
                build_catalog_object_using_table_description(
                    {
                        "id": 9,
                        "ra_deg": 0.0,
                        "dec_deg": 0.0,
                        "radius": None,
                        "name": "Catalog Object 009",
                    },
                    table_description,
                )
            )
            assert (
                "Catalog object has no radius and/or table has no default radius" in err
            )

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN]],
        indirect=True,
    )
    def test_add_catalog_successfully(self, mock_bigtable_catalog_repository):
        catalog_lut_direct_row_mock = (
            mock_bigtable_catalog_repository.catalog_lut.direct_row
        )
        (
            catalog_table,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        catalog_direct_row_mock = catalog_table.direct_row
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 19,
                "ra_deg": 0.0,
                "dec_deg": 0.0,
                "radius": float(
                    Angle(4, unit=u.arcsec).to(table_description["radius_unit"]).value
                ),
                "name": "Catalog Object 019",
            },
            table_description,
        )
        mock_bigtable_catalog_repository.add(catalog_object=catalog_object)
        calls = [
            call(
                CatalogLutColumns.create_row_key(
                    htm_level, f"{catalog_object.catalog_id}:{catalog_object.id}"
                )
            )
            for htm_level in [
                2147483648,
                3087007744,
                3221225472,
                4160749568,
            ]
        ]
        for current_call in calls:
            assert current_call in catalog_lut_direct_row_mock.mock_calls
        mock_bigtable_catalog_repository.catalog_lut.mutate_rows.assert_called_once()
        catalog_direct_row_mock.assert_called_with(
            CatalogColumns.create_row_key(catalog_object.id)
        )
        catalog_direct_row_mock.return_value.commit.assert_called_once()

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS]],
        indirect=True,
    )
    def test_add_catalog_successfully_using_catalog_radius(
        self, mock_bigtable_catalog_repository
    ):
        catalog_lut_direct_row_mock = (
            mock_bigtable_catalog_repository.catalog_lut.direct_row
        )
        (
            catalog_table,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        catalog_direct_row_mock = catalog_table.direct_row
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 19,
                "ra_deg": 0.0,
                "dec_deg": 0.0,
                "name": "Catalog Object 019",
            },
            table_description,
        )
        catalog_object = CatalogObject(
            id=catalog_object.id,
            catalog_id=catalog_object.catalog_id,
            catalog_name=catalog_object.catalog_name,
            location=catalog_object.location,
            radius=None,
            properties=catalog_object.properties,
            name=catalog_object.name,
        )
        mock_bigtable_catalog_repository.add(catalog_object=catalog_object)
        calls = [
            call(
                CatalogLutColumns.create_row_key(
                    htm_level, f"{catalog_object.catalog_id}:{catalog_object.id}"
                )
            )
            for htm_level in [
                34359738368,
                49392123904,
                51539607552,
                66571993088,
            ]
        ]
        for current_call in calls:
            assert current_call in catalog_lut_direct_row_mock.mock_calls
        mock_bigtable_catalog_repository.catalog_lut.mutate_rows.assert_called_once()
        catalog_direct_row_mock.assert_called_with(
            CatalogColumns.create_row_key(catalog_object.id)
        )
        catalog_direct_row_mock.return_value.commit.assert_called_once()

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE]],
        indirect=True,
    )
    def test_create_catalog_lut_row(self, mock_bigtable_catalog_repository):
        catalog_lut_direct_row_mock = (
            mock_bigtable_catalog_repository.catalog_lut.direct_row
        )
        mock_bigtable_catalog_repository.create_catalog_lut_row(0, "1:1")
        catalog_lut_direct_row_mock.assert_called_with(b"0#1:1")

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN]],
        indirect=True,
    )
    def test_insert_catalog_object_into_its_catalog_table_using_radius_column(
        self, mock_bigtable_catalog_repository
    ):
        (
            catalog_table,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        catalog_direct_row_mock = catalog_table.direct_row
        properties = {
            "id": 25,
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "name": "Catalog Object 025",
            "radius": float(
                Angle(3, unit=u.arcsec).to(table_description["radius_unit"]).value
            ),
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        mock_bigtable_catalog_repository.insert_catalog_object_into_its_catalog_table(
            catalog_object, catalog_table, table_description
        )
        isinstance(catalog_object.radius, Angle)
        catalog_direct_row_mock.assert_called_with(b"25")
        catalog_direct_row_mock.return_value.commit.assert_called_once()

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE]],
        indirect=True,
    )
    def test_insert_catalog_object_into_its_catalog_table_using_global_default_radius(
        self, mock_bigtable_catalog_repository
    ):
        (
            catalog_table,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        catalog_direct_row_mock = catalog_table.direct_row
        properties = {
            "id": 25,
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "name": "Catalog Object 025",
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        mock_bigtable_catalog_repository.insert_catalog_object_into_its_catalog_table(
            catalog_object, catalog_table, table_description
        )
        catalog_direct_row_mock.assert_called_with(b"25")
        catalog_direct_row_mock.return_value.commit.assert_called_once()

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE]],
        indirect=True,
    )
    def test_create_catalog_object_from_row_using_global_default_radius(
        self, mock_bigtable_catalog_repository
    ):
        (
            _,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        properties = {
            "id": 15,
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "name": "Catalog Object 015",
            "feature1": "value",
            "feature400": 0.1,
            "feature314": decimal.Decimal(0.1),
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        catalog_object_row = build_catalog_row_mock(catalog_object, table_description)
        retrieved_catalog = (
            mock_bigtable_catalog_repository.create_catalog_object_from_row(
                catalog_object_row, table_description
            )
        )
        assert retrieved_catalog.id == catalog_object.id
        assert retrieved_catalog.catalog_id == catalog_object.catalog_id
        assert retrieved_catalog.properties == catalog_object.properties
        assert retrieved_catalog.location == catalog_object.location
        assert retrieved_catalog.radius == Angle(1, unit=u.arcsecond)

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN]],
        indirect=True,
    )
    def test_create_catalog_object_from_row_using_radius_column(
        self, mock_bigtable_catalog_repository
    ):
        (
            _,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        properties = {
            "id": 15,
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "radius": float(
                Angle(3, unit=u.arcsec).to(table_description["radius_unit"]).value
            ),
            "name": "Catalog Object 015",
            "feature1": "value",
            "feature400": 0.1,
            "feature314": decimal.Decimal(0.1),
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        catalog_object_row = build_catalog_row_mock(catalog_object, table_description)
        retrieved_catalog = (
            mock_bigtable_catalog_repository.create_catalog_object_from_row(
                catalog_object_row, table_description
            )
        )
        assert retrieved_catalog.id == catalog_object.id
        assert retrieved_catalog.catalog_id == catalog_object.catalog_id
        assert retrieved_catalog.properties == catalog_object.properties
        assert retrieved_catalog.location == catalog_object.location
        assert retrieved_catalog.radius.deg == catalog_object.radius.deg

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN]],
        indirect=True,
    )
    def test_create_catalog_object_from_row_using_radius_column_when_radius_is_none(
        self, mock_bigtable_catalog_repository
    ):
        (
            _,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        properties = {
            "id": 15,
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "radius": None,
            "name": "Catalog Object 016",
            "feature1": "value",
            "feature400": 0.1,
            "feature314": decimal.Decimal(0.1),
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        catalog_object_row = build_catalog_row_mock(catalog_object, table_description)
        retrieved_catalog = (
            mock_bigtable_catalog_repository.create_catalog_object_from_row(
                catalog_object_row, table_description
            )
        )
        assert retrieved_catalog.id == catalog_object.id
        assert retrieved_catalog.catalog_id == catalog_object.catalog_id
        assert retrieved_catalog.properties == catalog_object.properties
        assert retrieved_catalog.location == catalog_object.location
        assert retrieved_catalog.radius.deg == Angle(angle=1, unit=u.arcsecond).deg

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS]],
        indirect=True,
    )
    def test_create_catalog_object_from_row_using_catalog_default_radius(
        self, mock_bigtable_catalog_repository
    ):
        (
            _,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        properties = {
            "id": 15,
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "name": "Catalog Object 015",
            "feature1": "value",
            "feature400": 0.1,
            "feature314": decimal.Decimal(0.1),
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        catalog_object_row = build_catalog_row_mock(catalog_object, table_description)
        retrieved_catalog = (
            mock_bigtable_catalog_repository.create_catalog_object_from_row(
                catalog_object_row, table_description
            )
        )
        assert retrieved_catalog.id == catalog_object.id
        assert retrieved_catalog.catalog_id == catalog_object.catalog_id
        assert retrieved_catalog.properties == catalog_object.properties
        assert retrieved_catalog.location == catalog_object.location
        assert retrieved_catalog.radius == Angle(angle=2, unit=u.arcsecond)

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [
            [
                build_bigtable_catalog_object_table_description(
                    object_id_type="str", radius=5.0
                )
            ]
        ],
        indirect=True,
    )
    def test_create_catalog_object_from_row_using_str_id(
        self, mock_bigtable_catalog_repository
    ):
        (
            _,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        properties = {
            "id": "15",
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "name": "Catalog Object 015",
            "feature1": "value",
            "feature400": 0.1,
            "feature314": decimal.Decimal(0.1),
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        catalog_object_row = build_catalog_row_mock(catalog_object, table_description)
        retrieved_catalog = (
            mock_bigtable_catalog_repository.create_catalog_object_from_row(
                catalog_object_row, table_description
            )
        )
        assert retrieved_catalog.id == catalog_object.id
        assert retrieved_catalog.catalog_id == catalog_object.catalog_id
        assert retrieved_catalog.properties == catalog_object.properties
        assert retrieved_catalog.location == catalog_object.location
        assert retrieved_catalog.radius == Angle(angle=5, unit=u.deg)

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE]],
        indirect=True,
    )
    def test_create_catalog_object_from_row_raises_value_error_when_column_is_not_in_schema(
        self, mock_bigtable_catalog_repository
    ):
        (
            _,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        properties = {
            "id": 15,
            "ra_deg": 100.0,
            "dec_deg": 40.0,
            "name": "Catalog Object 015",
            "feature999": "some_string",
        }
        catalog_object = build_catalog_object_using_table_description(
            properties,
            table_description,
        )
        catalog_object_row = build_catalog_row_mock(catalog_object, table_description)
        with pytest.raises(ValueError) as err:
            mock_bigtable_catalog_repository.create_catalog_object_from_row(
                catalog_object_row, table_description
            )
            assert (
                "Row 15 have a column that is not specified in the columns_with_data_types dictionary"
                in err
            )

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE]],
        indirect=True,
    )
    def test_list_by_catalog_id(self, mock_bigtable_catalog_repository):
        (
            catalog_table,
            _,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        list(mock_bigtable_catalog_repository.list_by_catalog_id("1"))
        catalog_table.read_rows.assert_called_once()

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[]],
        indirect=True,
    )
    def test_list_by_catalog_id_raises_value_error_when_catalog_doesnt_exist(
        self, mock_bigtable_catalog_repository
    ):
        with pytest.raises(ValueError) as err:
            list(mock_bigtable_catalog_repository.list_by_catalog_id("1"))
            assert (
                "Catalog table description could not be found for catalog id: 1" in err
            )

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[]],
        indirect=True,
    )
    def test_list_by_location_ignores_when_there_is_no_table_description_match(
        self,
        mock_bigtable_catalog_repository,
        caplog,
        metrics,
    ):
        read_rows_mock = mock_bigtable_catalog_repository.catalog_lut.read_rows
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 100,
                "ra_deg": 100.0,
                "dec_deg": 40.0,
                "name": "Catalog Object 100",
            },
            CATALOG_TABLE,
        )
        read_rows_mock.return_value = [
            build_catalog_lut_row_mock(
                htm_level, f"{catalog_object.catalog_id}:{catalog_object.id}"
            )
            for htm_level in [34359738368, 49392123904, 51539607552, 66571993088]
        ]
        with caplog.at_level(logging.WARN):
            list(
                mock_bigtable_catalog_repository.list_by_location(
                    SkyCoord(ra=0, dec=0, unit=u.deg), metrics
                )
            )
            assert (
                "Catalog_id = 1 not found, it should be disabled or no longer exist"
                in caplog.text
            )

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN]],
        indirect=True,
    )
    def test_list_by_location_filter_object_by_radius(
        self, mock_bigtable_catalog_repository, metrics
    ):
        catalog_lut_read_rows_mock = (
            mock_bigtable_catalog_repository.catalog_lut.read_rows
        )
        (
            catalog_table,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        # htm_ids of catalog_object [2147483648, 3087007744, 3221225472, 4160749568]
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 4,
                "ra_deg": 0.0,
                "dec_deg": 0.0,
                "radius": float(
                    Angle(8, unit=u.arcsec).to(table_description["radius_unit"]).value
                ),
                "name": "Catalog Object 004",
            },
            table_description,
        )
        """
        htm_ids of catalog_object_outside
        [66635153445, 66635153450, 66635153452, 66635153455]
        """
        catalog_object_outside = build_catalog_object_using_table_description(
            {
                "id": 6,
                "ra_deg": 3.0,
                "dec_deg": 3.0,
                "radius": float(
                    Angle(2, unit=u.arcsec).to(table_description["radius_unit"]).value
                ),
                "name": "Catalog Object 006",
            },
            table_description,
        )
        catalog_lut_read_rows_mock.return_value = [
            build_catalog_lut_row_mock(
                4160749568, f"{catalog_object.catalog_id}:{catalog_object.id}"
            ),
            build_catalog_lut_row_mock(
                66635153445,
                f"{catalog_object_outside.catalog_id}:{catalog_object_outside.id}",
            ),
        ]
        catalog_read_row_mock = catalog_table.read_row
        catalog_read_row_mock.side_effect = [
            build_catalog_row_mock(catalog_object, table_description),
            build_catalog_row_mock(catalog_object_outside, table_description),
        ]
        """
        htm_ids that will be searched by using ra=0 dec=0 unit=u.deg
        [66571993088, 3968, 63488, 1015808, 16252928, 260046848, 4160749568]
        The id 66635153445 is added to show that list_by_location is 
        filtering objects that have a high separation.
        """
        matches = list(
            mock_bigtable_catalog_repository.list_by_location(
                SkyCoord(ra=0, dec=0, unit=u.deg), metrics
            )
        )
        assert len(matches) == 1
        assert matches[0].id == "4"

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[CATALOG_TABLE_DESCRIPTIONS_W_RADIUS_COLUMN]],
        indirect=True,
    )
    def test_list_by_location_htm_id_from_lut_is_not_found(
        self, mock_bigtable_catalog_repository, caplog, metrics
    ):
        catalog_lut_read_rows_mock = (
            mock_bigtable_catalog_repository.catalog_lut.read_rows
        )
        (
            catalog_table,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        catalog_object = build_catalog_object_using_table_description(
            {
                "id": 45,
                "ra_deg": 3.0,
                "dec_deg": 3.0,
                "radius": float(
                    Angle(2, unit=u.arcsec).to(table_description["radius_unit"]).value
                ),
                "name": "Catalog Object 045",
            },
            table_description,
        )
        catalog_lut_read_rows_mock.return_value = [
            build_catalog_lut_row_mock(
                66635153445, f"{catalog_object.catalog_id}:{catalog_object.id}"
            )
        ]
        catalog_table.read_row.return_value = None
        with caplog.at_level(logging.WARN):
            list(
                mock_bigtable_catalog_repository.list_by_location(
                    SkyCoord(ra=0, dec=0, unit=u.deg),
                    metrics,
                )
            )
            assert "Catalog object not found" in caplog.text

    @pytest.mark.parametrize(
        "mock_bigtable_catalog_repository",
        [[build_bigtable_catalog_object_table_description(radius=0.00028)]],
        indirect=True,
    )
    def test_list_by_location_use_global_default_radius(
        self, mock_bigtable_catalog_repository, metrics
    ):
        catalog_lut_read_rows_mock = (
            mock_bigtable_catalog_repository.catalog_lut.read_rows
        )
        (
            catalog_table,
            table_description,
        ) = mock_bigtable_catalog_repository.get_catalog_table_with_description(1)
        # htm_ids = [3791, 60668, 970701, 15531222, 248499557, 3975992926, 63615886824]
        tns_catalog_object = build_catalog_object_using_table_description(
            {
                "id": 126744,
                "ra_deg": 114.581931225,
                "dec_deg": 42.1037804375,
                "name": "2023erk",
            },
            table_description,
        )
        # htm_ids =  [3791, 60668, 970701, 15531222, 248499557, 3975992927, 63615886832]
        gaia_dr2_catalog_object = build_catalog_object_using_table_description(
            {
                "id": 85763960,
                "ra_deg": 114.5835694020635,
                "dec_deg": 42.1041267048962,
                "name": "Gaia DR2 924614477811411712",
            },
            table_description,
        )
        # htm_ids = [3791, 60668, 970701, 15531222, 248499557, 3975992927, 63615886832]
        sdss_gals_catalog_object = build_catalog_object_using_table_description(
            {
                "id": 149455588,
                "ra_deg": 114.58360584,
                "dec_deg": 42.10407722,
                "name": "587737809028776363",
            },
            table_description,
        )
        catalog_lut_read_rows_mock.return_value = [
            build_catalog_lut_row_mock(3791, "1:126744"),
            build_catalog_lut_row_mock(
                3791,
                "1:85763960",
            ),
            build_catalog_lut_row_mock(
                3791,
                "1:149455588",
            ),
        ]
        catalog_read_row_mock = catalog_table.read_row
        catalog_read_row_mock.side_effect = [
            build_catalog_row_mock(tns_catalog_object, table_description),
            build_catalog_row_mock(gaia_dr2_catalog_object, table_description),
            build_catalog_row_mock(sdss_gals_catalog_object, table_description),
        ]
        """
        htm_ids that will be searched by using ra=114.5819 dec=42.1038 unit=u.deg
        [63615886824, 3791, 60668, 970701, 15531222, 248499557, 3975992926]
        """
        location = SkyCoord(ra=114.5819, dec=42.1038, unit=u.deg)
        matches = list(
            mock_bigtable_catalog_repository.list_by_location(location, metrics)
        )
        assert len(matches) == 1
        assert matches[0].properties["name"] == "2023erk"
        assert (
            location.separation(SkyCoord(matches[0].location, unit=u.deg))
            < matches[0].radius
        )
