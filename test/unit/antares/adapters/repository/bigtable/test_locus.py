import logging
import random
from test.fakes import build_locus
from unittest.mock import create_autospec

import bson
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from astropy.timeseries import TimeSeries
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData, Row
from google.cloud.bigtable.table import Table
from pytest import fixture, mark, raises

from antares.adapters.repository.bigtable.locus import (
    BigtableLocusRepository,
    LocusNotFoundError,
)
from antares.adapters.repository.bigtable.schemas import AlertLutColumns, LocusColumns
from antares.adapters.repository.bigtable.utils import AlertLookupType, encode_any
from antares.domain.models import Locus
from antares.exceptions import KeyViolationException
from antares.utils import astropy_table_to_string, mjd_to_datetime


@fixture
def mock_bigtable_locus_repository():
    mock_instance = create_autospec(spec=BigtableInstance, instance=True)
    bt_locus_repository = BigtableLocusRepository(
        instance_=mock_instance,
        locus_table_id="locus",
        locus_cf="L",
        alert_lut_id="alert_lut",
        alert_lut_cf="A",
    )
    bt_locus_repository.locus_table = create_autospec(spec=Table, instance=True)
    bt_locus_repository.alert_lut = create_autospec(spec=Table, instance=True)
    return bt_locus_repository


def build_alert_lut_row_mock(htm: int, type_value: int, value: str) -> Row:
    alert_lut_row_mock = create_autospec(spec=PartialRowData, instance=True)
    alert_lut_row_mock.row_key = AlertLutColumns.create_row_key(htm, type_value, value)
    return alert_lut_row_mock


def build_locus_row_mock(locus: Locus):
    locus_row_mock = create_autospec(spec=PartialRowData, instance=True)
    locus_row_mock.row_key = LocusColumns.create_row_key(locus.id)

    def mock_cell_value(column_family_id, column):
        if column == LocusColumns.RA:
            return encode_any(locus.location.ra.deg, "double")
        elif column == LocusColumns.DEC:
            return encode_any(locus.location.dec.deg, "double")
        elif column == LocusColumns.PROPS:
            return bson.dumps(
                {"ant_grav_wave_events": locus.grav_wave_events, **locus.properties}
            )
        elif column == LocusColumns.TAGS:
            return str(locus.tags).encode()
        elif column == LocusColumns.LIGHTCURVE:
            lightcurve = ""
            if locus.lightcurve:
                lightcurve = astropy_table_to_string(
                    table=locus.lightcurve, format_="csv", exclude=["time"]
                )
            return lightcurve.encode()
        elif column == LocusColumns.CATALOGS:
            return str(locus.catalogs).encode()

    locus_row_mock.cell_value = mock_cell_value
    return locus_row_mock


test_loci = [
    build_locus(),
    build_locus(grav_wave_events=["MS123", "MS321"]),
    build_locus(
        lightcurve=TimeSeries(
            data=[
                {
                    "alert_id": f"alert-{i}",
                    "ant_mjd": 59000.0 + i,
                    "ant_mag": random.random() + 15.0,
                }
                for i in range(10)
            ],
            time=[mjd_to_datetime(59000.0 + i) for i in range(10)],
        )
    ),
]


@mark.usefixtures("message_bus")
class TestBigtableLocusRepository:
    @mark.parametrize("locus", test_loci)
    def test_create_locus_row(self, mock_bigtable_locus_repository, locus):
        locus_direct_row_mock = mock_bigtable_locus_repository.locus_table.direct_row
        mock_bigtable_locus_repository.create_locus_row(locus)
        locus_direct_row_mock.assert_called_with(locus.id.encode())

    @mark.parametrize("locus", test_loci)
    def test_create_locus_from_row(self, mock_bigtable_locus_repository, locus):
        retrieved_locus = mock_bigtable_locus_repository.create_locus_from_row(
            build_locus_row_mock(locus)
        )
        assert isinstance(retrieved_locus, Locus)
        assert locus.id == retrieved_locus.id
        assert locus.grav_wave_events == retrieved_locus.grav_wave_events

    def test_get_raises_locus_not_found_error(self, mock_bigtable_locus_repository):
        locus_read_row_mock = mock_bigtable_locus_repository.locus_table.read_row
        locus_read_row_mock.return_value = None
        with raises(LocusNotFoundError) as err:
            mock_bigtable_locus_repository.get("key_that_doesnt_exist")
            assert "Locus key_that_doesnt_exist not found" in str(err)

    def test_get_returns_locus(self, mock_bigtable_locus_repository):
        locus_read_row_mock = mock_bigtable_locus_repository.locus_table.read_row
        locus = build_locus()
        locus_read_row_mock.return_value = build_locus_row_mock(locus)
        retrieved_locus = mock_bigtable_locus_repository.get(locus.id)
        assert locus.id == retrieved_locus.id

    def test_add(self, mock_bigtable_locus_repository, successful_mutate_row_mock):
        locus_read_row_mock = mock_bigtable_locus_repository.locus_table.read_row
        locus_read_row_mock.return_value = None
        locus_direct_row_mock = mock_bigtable_locus_repository.locus_table.direct_row
        locus_mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        locus_direct_row_mock.return_value = locus_mock_row_sample
        alert_lut_mutate_rows_mock = (
            mock_bigtable_locus_repository.alert_lut.mutate_rows
        )
        alert_lut_mutate_rows_mock.return_value = [successful_mutate_row_mock]
        locus = build_locus()
        mock_bigtable_locus_repository.add(locus)
        locus_direct_row_mock.assert_called_with(locus.id.encode())
        alert_lut_mutate_rows_mock.assert_called_once()
        locus_mock_row_sample.commit.assert_called_once()

    def test_add_raise_error_because_locus_already_exists(
        self, mock_bigtable_locus_repository
    ):
        locus_read_row_mock = mock_bigtable_locus_repository.locus_table.read_row
        locus = build_locus()
        locus_read_row_mock.side_effect = [None, build_locus_row_mock(locus)]
        mock_bigtable_locus_repository.add(locus)
        with raises(KeyViolationException):
            mock_bigtable_locus_repository.add(locus)

    def test_add_fails_writing_multiple_rows(
        self, mock_bigtable_locus_repository, caplog, unsuccessful_mutate_row_mock
    ):
        locus_read_row_mock = mock_bigtable_locus_repository.locus_table.read_row
        locus_read_row_mock.return_value = None
        locus_direct_row_mock = mock_bigtable_locus_repository.locus_table.direct_row
        locus_mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        locus_direct_row_mock.return_value = locus_mock_row_sample
        alert_lut_mutate_rows_mock = (
            mock_bigtable_locus_repository.alert_lut.mutate_rows
        )
        alert_lut_mutate_rows_mock.return_value = [unsuccessful_mutate_row_mock]
        locus = build_locus()
        with caplog.at_level(logging.ERROR):
            mock_bigtable_locus_repository.add(locus)
            locus_direct_row_mock.assert_called_with(locus.id.encode())
            alert_lut_mutate_rows_mock.assert_called_once()
            locus_mock_row_sample.commit.assert_called_once()
            assert "1 errors while writing locus in alert_lut" in caplog.text

    def test_update(self, mock_bigtable_locus_repository):
        locus_direct_row_mock = mock_bigtable_locus_repository.locus_table.direct_row
        locus_mock_row_sample = create_autospec(spec=DirectRow, instance=True)
        locus_direct_row_mock.return_value = locus_mock_row_sample
        locus = build_locus()
        mock_bigtable_locus_repository.update(locus)
        locus_direct_row_mock.assert_called_with(locus.id.encode())
        locus_mock_row_sample.commit.assert_called_once()

    def test_list_by_cone_search(self, mock_bigtable_locus_repository):
        alert_lut_read_rows_mock = mock_bigtable_locus_repository.alert_lut.read_rows
        alert_lut_read_rows_mock.return_value = [
            build_alert_lut_row_mock(
                htm=66575925503,
                type_value=AlertLookupType.LOCUS.value,
                value="locus-001",
            ),
        ]
        locus_read_rows_mock = mock_bigtable_locus_repository.locus_table.read_rows
        locus_read_rows_mock.return_value = [
            build_locus_row_mock(
                build_locus(id="locus-001", location=SkyCoord(ra=0, dec=2, unit=u.deg))
            ),
            build_locus_row_mock(
                build_locus(id="locus-002", location=SkyCoord(ra=0, dec=0, unit=u.deg))
            ),
        ]
        loci = mock_bigtable_locus_repository.list_by_cone_search(
            SkyCoord(ra=0, dec=0, unit=u.deg), Angle(angle=1, unit=u.deg)
        )
        for locus in loci:
            assert locus.id != "locus-001"

    def test__get_locus_ids_by_htm_ids(self, mock_bigtable_locus_repository):
        alert_lut_read_rows_mock = mock_bigtable_locus_repository.alert_lut.read_rows
        htm_ids = [66571993088, 3968, 63488, 1015808, 16252928, 260046848, 4160749568]
        locus_id = "locus-001"
        alert_lut_read_rows_mock.return_value = [
            build_alert_lut_row_mock(
                htm,
                AlertLookupType.LOCUS.value,
                locus_id,
            )
            for htm in htm_ids
        ]
        locus_ids = mock_bigtable_locus_repository._get_locus_ids_by_htm_ids(htm_ids)
        assert locus_ids == [locus_id for _ in htm_ids]

    def test__get_locus_ids_by_htm_ids_returns_empty_list_when_htm_ids_list_is_empty(
        self, mock_bigtable_locus_repository
    ):
        locus_ids = mock_bigtable_locus_repository._get_locus_ids_by_htm_ids([])
        assert locus_ids == []

    def test__list_by_locus_id(self, mock_bigtable_locus_repository):
        locus_read_rows_mock = mock_bigtable_locus_repository.locus_table.read_rows
        locus_read_rows_mock.return_value = [
            build_locus_row_mock(build_locus(id="locus-001")),
            build_locus_row_mock(build_locus(id="locus-002")),
        ]
        locus_ids = ["locus-001", "locus-002"]
        loci = mock_bigtable_locus_repository._list_by_locus_ids(locus_ids)
        for locus in loci:
            assert locus.id in locus_ids

    def test__list_by_locus_id_for_missing_locus(self, mock_bigtable_locus_repository):
        locus_ids = ["locus-999"]
        loci = mock_bigtable_locus_repository._list_by_locus_ids(locus_ids)
        assert list(loci) == []

    def test__list_by_locus_id_gets_replaced(self, mock_bigtable_locus_repository):
        locus_read_rows_mock = mock_bigtable_locus_repository.locus_table.read_rows
        locus_read_rows_mock.return_value = [
            build_locus_row_mock(
                build_locus(id="locus-001", properties={"replaced_by": "locus-002"})
            ),
            build_locus_row_mock(build_locus(id="locus-002")),
        ]
        locus_ids = ["locus-001", "locus-002"]
        loci = mock_bigtable_locus_repository._list_by_locus_ids(locus_ids)
        for locus in loci:
            assert locus.id != "locus-001"
