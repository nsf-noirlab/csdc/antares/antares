import uuid
from datetime import datetime
from decimal import Decimal
from test.fakes import build_locus, build_watch_object
from unittest.mock import call, create_autospec

from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from google.cloud.bigtable.row_set import RowRange
from google.cloud.bigtable.table import Table
from pytest import approx, fixture, mark, raises

from antares.adapters.repository.bigtable.utils import (
    AlertLookupMethods,
    AlertLookupType,
    create_prefix_row_set,
    decode_any,
    encode_any,
    get_optimal_htm_level,
)


def test_create_prefix_row_set():
    row_set = create_prefix_row_set("some_prefix")
    assert row_set.row_ranges[0].end_key == b"some_prefix$"


@mark.parametrize(
    "radius,expected_value",
    [
        (Angle(angle=0.001, unit=u.deg), 16),
        (Angle(angle=0.005, unit=u.deg), 14),
        (Angle(angle=0.02, unit=u.deg), 12),
        (Angle(angle=0.05, unit=u.deg), 10),
        (Angle(angle=0.2, unit=u.deg), 8),
        (Angle(angle=1.0, unit=u.deg), 6),
        (Angle(angle=2.0, unit=u.deg), 4),
    ],
)
def test_get_optimal_htm_level(radius, expected_value):
    value = get_optimal_htm_level(radius)
    assert value == expected_value


@fixture
def mock_alert_lookup_methods():
    bt_alert_lookup_methods = AlertLookupMethods(
        alert_lut=create_autospec(spec=Table, instance=True),
        alert_lut_cf="A"
    )
    return bt_alert_lookup_methods


class TestAlertLookupMethods:
    def test_create_alert_lut_row(self, mock_alert_lookup_methods):
        alert_lut_direct_row_mock = mock_alert_lookup_methods.alert_lut.direct_row
        mock_alert_lookup_methods.create_alert_lut_row(htm=4, type_value=1, value="A")
        alert_lut_direct_row_mock.assert_called_with(b"4#1#A")

    def test_create_alert_lut_rows_for_locus(self, mock_alert_lookup_methods):
        alert_lut_direct_row_mock = mock_alert_lookup_methods.alert_lut.direct_row
        rows = mock_alert_lookup_methods.create_alert_lut_rows_for_locus(build_locus())
        assert len(rows) == 7
        calls = [
            call(f"{htm_id}#1#locus-001".encode())
            for htm_id in [
                66571993088,
                3968,
                63488,
                1015808,
                16252928,
                260046848,
                4160749568,
            ]
        ]
        for current_call in calls:
            assert current_call in alert_lut_direct_row_mock.mock_calls

    def test_create_alert_lut_rows_for_watch_object(self, mock_alert_lookup_methods):
        alert_lut_direct_row_mock = mock_alert_lookup_methods.alert_lut.direct_row
        rows = mock_alert_lookup_methods.create_alert_lut_rows_for_watch_object(
            build_watch_object(
                id=uuid.UUID(int=1),
                watch_list_id=uuid.UUID(int=1),
                radius=Angle(1, unit=u.deg),
            )
        )
        assert len(rows) == 8
        calls = [
            call(
                (
                    f"{htm_id}#2#00000000-0000-0000-0000-000000000001"
                    ":00000000-0000-0000-0000-000000000001"
                ).encode()
            )
            for htm_id in [32768, 32771, 47104, 47107, 49152, 49155, 63488, 63491]
        ]
        for current_call in calls:
            assert current_call in alert_lut_direct_row_mock.mock_calls

    def test_create_row_set_for_htm_levels(self, mock_alert_lookup_methods):
        row_set = mock_alert_lookup_methods.create_row_set_for_htm_levels(
            SkyCoord(ra=0, dec=0, unit=u.deg), AlertLookupType.LOCUS
        )
        assert len(row_set.row_ranges) == 7
        row_ranges = [
            RowRange(start_key=b"66571993088#1#", end_key=b"66571993088#1$"),
            RowRange(start_key=b"3968#1#", end_key=b"3968#1$"),
            RowRange(start_key=b"63488#1#", end_key=b"63488#1$"),
            RowRange(start_key=b"1015808#1#", end_key=b"1015808#1$"),
            RowRange(start_key=b"16252928#1#", end_key=b"16252928#1$"),
            RowRange(start_key=b"260046848#1#", end_key=b"260046848#1$"),
            RowRange(start_key=b"4160749568#1#", end_key=b"4160749568#1$"),
        ]
        assert row_ranges == row_set.row_ranges


@mark.parametrize(
    "value,expected_value,data_type",
    [
        (True, b"\x01", "bool"),
        ("a", b"a", "str"),
        (1.0, b"?\x80\x00\x00", "float"),
        (1.0, b"?\xf0\x00\x00\x00\x00\x00\x00", "double"),
        (1, b"\x01", "tinyint"),
        (1, b"\x00\x01", "smallint"),
        (1, b"\x00\x00\x00\x01", "int"),
        (1, b"\x00\x00\x00\x00\x00\x00\x00\x01", "bigint"),
        (Decimal("0.123456789"), b"0.123456789", "decimal"),
        (
            datetime(
                year=2020,
                month=11,
                day=3,
                hour=22,
                minute=33,
                second=9,
                microsecond=912000,
            ),
            b"\x00\x00\x01u\x90>\x18\x18",
            "timestamp",
        ),
        (
            Decimal(0.1234567),
            b"0.1234567000000000025483615218035993166267871856689453125",
            "decimal",
        ),
    ],
)
def test_encode_any(value, expected_value, data_type):
    encoded_value = encode_any(value, data_type)
    assert encoded_value == expected_value


def test_encode_any_raises_value_error():
    with raises(ValueError) as err:
        encode_any(value=None, data_type=None)  # type: ignore
        assert "value is type <class 'NoneType'>; expected value to be" in err


@mark.parametrize(
    "some_bytes,data_type,expected_value",
    [
        (b"\x01", "bool", True),
        (b"a", "str", "a"),
        (b"?\x80\x00\x00", "float", 1.0),
        (b"?\xf0\x00\x00\x00\x00\x00\x00", "double", 1.0),
        (b"\x01", "tinyint", 1),
        (b"\x00\x01", "smallint", 1),
        (b"\x00\x00\x00\x01", "int", 1),
        (b"\x00\x00\x00\x00\x00\x00\x00\x01", "bigint", 1),
        (
            b"\x00\x00\x01u\x90>\x18\x18",
            "timestamp",
            datetime(
                year=2020,
                month=11,
                day=3,
                hour=22,
                minute=33,
                second=9,
                microsecond=912000,
            ),
        ),
        (b"0.123456789", "decimal", Decimal("0.123456789")),
        (
            b"0.1234567000000000025483615218035993166267871856689453125",
            "decimal",
            Decimal(0.1234567),
        ),
    ],
)
def test_decode_any(some_bytes, data_type, expected_value):
    value = decode_any(some_bytes, data_type)
    assert value == expected_value


def test_decode_any_raises_value_error():
    with raises(ValueError) as err:
        decode_any(value=None, data_type="unexpected_data_type")  # type: ignore
        assert 'Cannot decode b"unexpected_data_type"; expected value to be' in err


def test_decode_data_from_bigtable_after_a_dataproc_ingestion():
    # TIMESTAMP
    assert decode_any(
        value=b"\x00\x00\x01\x8a>R\xc8\xe6", data_type="timestamp"
    ) == datetime(
        year=2023, month=8, day=28, hour=22, minute=45, second=27, microsecond=398000
    )
    # FLOAT, this can cause errors because it stores numbers like 1234.56787109375 when storing 1234.56789
    assert decode_any(value=b"D\x9aR,", data_type="float") == approx(
        expected=1234.56789, rel=0.00001
    )
    # DOUBLE
    assert (
        decode_any(value=b"@\x93JE\x84\xf4\xc6\xe7", data_type="double") == 1234.56789
    )
    # INTEGER
    assert decode_any(value=b"\x00\x0009", data_type="int") == 12345
    # LONG
    assert decode_any(value=b"\x00\x00\x00\x00\x00\x0009", data_type="bigint") == 12345
    # SHORT
    assert decode_any(value=b"09", data_type="smallint") == 12345
    # STRING
    assert decode_any(value=b"some_word", data_type="str") == "some_word"
