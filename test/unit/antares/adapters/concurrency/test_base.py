from unittest.mock import MagicMock, patch

import pytest

from antares.adapters.concurrency.base import AbstractDistributedLock

abstractmethods = (
    "antares.adapters.concurrency.base.AbstractDistributedLock.__abstractmethods__"
)


class MockAbstractDistributedLock(AbstractDistributedLock):
    def acquire(self):
        pass

    def release(self):
        pass

    def locked(self):
        pass


class TestAbstractDistributedLock:
    def test_cant_instantiate_abc(self):
        with pytest.raises(TypeError) as err:
            AbstractDistributedLock()
            assert "Can't instantiate abstract class" in err

    @patch(abstractmethods, set())
    def test_abstract_methods(self):
        lock = AbstractDistributedLock()
        with pytest.raises(NotImplementedError):
            lock.acquire()
        with pytest.raises(NotImplementedError):
            lock.release()
        with pytest.raises(NotImplementedError):
            lock.locked()

    def test_lock_context_manager_calls_acquire(self):
        lock = MockAbstractDistributedLock()
        lock.acquire = MagicMock()
        with lock:
            pass
        lock.acquire.assert_called_once()

    def test_lock_context_manager_calls_release(self):
        lock = MockAbstractDistributedLock()
        lock.release = MagicMock()
        with lock:
            pass
        lock.release.assert_called_once()

    def test_lock_context_manager_calls_release_after_error(self):
        lock = MockAbstractDistributedLock()
        lock.release = MagicMock()
        try:
            with lock:
                raise ValueError
        except ValueError:
            pass
        lock.release.assert_called_once()
