import time
from unittest.mock import create_autospec

import pytest
from redis.client import Redis

from antares.adapters.concurrency.redis import RedisDistributedLock


class TestRedisDistributedLock:
    @pytest.fixture
    def mock_redis(self):
        return create_autospec(Redis)

    def test_id_must_be_a_string(self, mock_redis):
        with pytest.raises(ValueError) as err:
            _ = RedisDistributedLock(mock_redis, 123)
            assert "must be of type `str`" in err

    def test_begins_unlocked(self, mock_redis):
        lock = RedisDistributedLock(mock_redis, "key")
        assert not lock.locked()

    def test_setting_block_false_and_nonzero_timeout_raises_valuerror(self, mock_redis):
        mock_redis.set.return_value = True
        lock = RedisDistributedLock(mock_redis, "key")
        with pytest.raises(ValueError) as err:
            lock.acquire(blocking=False, timeout=1)
            assert "can't specify a timeout" in err

    def test_can_acquire_lock(self, mock_redis):
        mock_redis.set.return_value = True
        lock = RedisDistributedLock(mock_redis, "key")
        result = lock.acquire()
        assert result

    def test_locked_after_acquired_lock(self, mock_redis):
        mock_redis.set.return_value = True
        lock = RedisDistributedLock(mock_redis, "key")
        _ = lock.acquire()
        assert lock.locked()

    def test_lock_fails_if_non_blocking_and_no_timeout(self, mock_redis):
        mock_redis.set.return_value = False
        lock = RedisDistributedLock(mock_redis, "key")
        result = lock.acquire(blocking=False)
        assert not result

    def test_lock_fails_after_timeout(self, mock_redis):
        timeout = 1.1
        starttime = time.time()
        mock_redis.set.return_value = False
        lock = RedisDistributedLock(mock_redis, "key")
        result = lock.acquire(timeout=timeout)
        endtime = time.time()
        assert not result
        assert endtime - starttime > timeout

    def test_lock_succeeds_before_timeout(self, mock_redis):
        timeout = 1.1
        starttime = time.time()
        mock_redis.set.side_effect = [False] * 5 + [True]
        lock = RedisDistributedLock(mock_redis, "key")
        result = lock.acquire(timeout=timeout)
        endtime = time.time()
        assert result
        assert endtime - starttime < timeout

    def test_ttl_passed_to_redis_set(self, mock_redis):
        mock_redis.set.return_value = True
        lock = RedisDistributedLock(mock_redis, "key", 1.1)
        result = lock.acquire()
        assert result
        set_kwargs = mock_redis.set.call_args.kwargs
        assert "px" in set_kwargs and set_kwargs["px"] == 1100

    def test_can_release_lock(self, mock_redis):
        mock_redis.set.return_value = True
        lock = RedisDistributedLock(mock_redis, "key")
        result = lock.acquire()
        assert result
        assert lock.locked()
        lock.release()
        mock_redis.eval.assert_called_once()
        assert not lock.locked()
