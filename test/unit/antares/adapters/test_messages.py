from datetime import datetime
from unittest.mock import create_autospec, patch

import pytest
from confluent_kafka import TIMESTAMP_CREATE_TIME, TIMESTAMP_NOT_AVAILABLE
from confluent_kafka.cimpl import Message

from antares.adapters.messages import (
    AbstractMessagePublicationService,
    AbstractMessageSubscriptionService,
    KafkaMessagePublicationService,
    KafkaMessageSubscriptionService,
)
from antares.exceptions import Timeout
from antares.external.kafka import KafkaConsumer


class TestAbstractMessagePublicationService:
    abstractmethods = (
        "antares.adapters.messages."
        "AbstractMessagePublicationService.__abstractmethods__"
    )

    def test_cant_instantiate_abc(self):
        with pytest.raises(TypeError) as err:
            AbstractMessagePublicationService()
            assert "Can't instantiate abstract class" in err

    @patch(abstractmethods, set())
    def test_abstract_methods(self):
        pub_service = AbstractMessagePublicationService()
        with pytest.raises(NotImplementedError):
            pub_service.publish("destination", "message")


class TestAbstractMessageSubscriptionService:
    abstractmethods = (
        "antares.adapters.messages."
        "AbstractMessageSubscriptionService.__abstractmethods__"
    )

    def test_cant_instantiate_abc(self):
        with pytest.raises(TypeError) as err:
            AbstractMessageSubscriptionService()
            assert "Can't instantiate abstract class" in err

    @patch(abstractmethods, set())
    def test_abstract_methods(self):
        sub_service = AbstractMessageSubscriptionService()
        with pytest.raises(NotImplementedError):
            sub_service.poll()


class TestKafkaMessagePublicationService:
    @patch("antares.adapters.messages.KafkaProducer", autospec=True)
    def test_publish(self, mock_kafka_producer_class):
        kafka_config = {"test": "value"}
        mock_kafka_producer = mock_kafka_producer_class.return_value
        pub_service = KafkaMessagePublicationService(kafka_config)
        pub_service.publish("destination", "message")
        mock_kafka_producer_class.assert_called_once_with(kafka_config, True)
        mock_kafka_producer.produce.assert_called_once_with("destination", "message")
        mock_kafka_producer.flush.assert_called_once()


class TestKafkaMessageSubscriptionService:
    @pytest.fixture
    def mock_message(self):
        mock_message = create_autospec(Message)
        timestamp = 1655142002000
        mock_message.timestamp.return_value = (TIMESTAMP_CREATE_TIME, timestamp)
        mock_message.value.return_value = "message_value"
        return mock_message

    @pytest.fixture
    def mock_kafka_consumer(self, mock_message):
        mock_kafka_consumer = create_autospec(KafkaConsumer)
        mock_kafka_consumer.poll.return_value = ("topic", mock_message)
        return mock_kafka_consumer

    @pytest.fixture
    def sub_service(self, mock_message, mock_kafka_consumer):
        kafka_config = {"test": "value"}
        sources = ["topic1", "topic2"]
        with patch(
            "antares.adapters.messages.KafkaConsumer", autospec=True
        ) as mock_kafka_consumer_class:
            mock_kafka_consumer_class.return_value = mock_kafka_consumer
            yield KafkaMessageSubscriptionService(kafka_config, sources)
            mock_kafka_consumer_class.assert_called_once()

    def test_poll(self, sub_service):
        expected_timestamp = datetime.fromtimestamp(1655142002)
        result_topic, result_value, result_timestamp = sub_service.poll()
        assert result_topic == "topic"
        assert result_value == "message_value"
        assert result_timestamp == expected_timestamp

    def test_poll_errors_when_timeout(self, sub_service, mock_kafka_consumer):
        mock_kafka_consumer.poll.return_value = (None, None)
        with pytest.raises(Timeout):
            sub_service.poll()

    def test_poll_can_return_unavailable_timestamp(self, sub_service, mock_message):
        mock_message.timestamp.return_value = (TIMESTAMP_NOT_AVAILABLE, None)
        result_topic, result_value, result_timestamp = sub_service.poll()
        assert result_topic == "topic"
        assert result_value == "message_value"
        assert result_timestamp is None

    def test_commit(self, sub_service, mock_kafka_consumer):
        sub_service.commit()
        mock_kafka_consumer.commit.assert_called_once()

    def test_get_watermark_offsets(self, sub_service, mock_kafka_consumer):
        sub_service.get_watermark_offsets()
        mock_kafka_consumer.get_watermark_offsets.assert_called_once()
