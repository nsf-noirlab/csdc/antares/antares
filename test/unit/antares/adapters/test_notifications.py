import uuid
from test.fakes import build_filter, build_filter_revision, build_user
from unittest.mock import ANY, patch

import pytest

from antares.adapters.notifications import (
    AbstractInternalNotificationService,
    AbstractNotificationService,
    EmailNotificationService,
    InternalSlackApiNotificationService,
    SlackApiNotificationService,
    SlackWebhookNotificationService,
)
from antares.domain.models import FilterRevisionStatus

abstractmethods = (
    "antares.adapters.notifications.AbstractNotificationService.__abstractmethods__"
)


class TestAbstractNotificationService:
    def test_cant_instantiate_abc(self):
        with pytest.raises(TypeError) as err:
            AbstractNotificationService()
        assert "Can't instantiate abstract class" in str(err.value)

    @patch(abstractmethods, set())
    def test_abstract_methods(self):
        notification_service = AbstractNotificationService()
        with pytest.raises(NotImplementedError):
            notification_service.send("dest", "msg")

    @patch(abstractmethods, set())
    def test_abstract_context_manager(self):
        with AbstractNotificationService() as notification_service:
            with pytest.raises(NotImplementedError):
                notification_service.send("dest", "msg")


@patch(
    "antares.adapters.notifications.AbstractInternalNotificationService.__abstractmethods__",  # noqa: E501
    set(),
)
class TestAbstractInternalNotificationService:
    def test_abstract_methods(self):
        notification_service = AbstractInternalNotificationService()
        filter_ = build_filter()
        filter_revision = build_filter_revision(filter_=filter_)
        with pytest.raises(NotImplementedError):
            notification_service.notify_after_filter_revision_created(
                filter_,
                filter_revision,
            )
        with pytest.raises(NotImplementedError):
            notification_service.notify_after_filter_revision_transitioned(
                filter_,
                filter_revision,
                "custom_message",
            )

    def test_abstract_context_manager(self):
        with AbstractInternalNotificationService() as notification_service:
            filter_ = build_filter()
            filter_revision = build_filter_revision(filter_=filter_)
            with pytest.raises(NotImplementedError):
                notification_service.notify_after_filter_revision_created(
                    filter_,
                    filter_revision,
                )
            with pytest.raises(NotImplementedError):
                notification_service.notify_after_filter_revision_transitioned(
                    filter_,
                    filter_revision,
                    "custom_message",
                )


@patch("antares.adapters.notifications.smtplib.SMTP", autospec=True)
class TestEmailNotificationService:
    def test_send(self, mock_SMTP):
        mock_smtp = mock_SMTP.return_value
        email_service = EmailNotificationService("host", 1234, "sender")
        email_service.send("destination", "message")
        mock_smtp.sendmail.assert_called_once_with("sender", "destination", ANY)
        sent_message = mock_smtp.sendmail.call_args.args[2]
        assert "message" in sent_message

    def test_context_manager_wo_login(self, mock_SMTP):
        mock_smtp = mock_SMTP.return_value
        with EmailNotificationService("host", 1234, "sender"):
            pass
        mock_smtp.login.assert_not_called()
        mock_smtp.quit.assert_called_once()

    def test_context_manager_w_login(self, mock_SMTP):
        mock_smtp = mock_SMTP.return_value
        with EmailNotificationService("host", 1234, "sender", "username", "password"):
            pass
        mock_smtp.login.assert_called_once_with("username", "password")
        mock_smtp.quit.assert_called_once()


@patch("antares.adapters.notifications.requests", autospec=True)
class TestSlackApiNotificationService:
    def test_send(self, mock_requests):
        slack_service = SlackApiNotificationService("api_token")
        slack_service.send("destination", "message")
        mock_requests.post.assert_called_once()


@patch("antares.adapters.notifications.requests", autospec=True)
class TestSlackWebhookNotificationService:
    def test_send(self, mock_requests):
        slack_service = SlackWebhookNotificationService("url")
        slack_service.send("destination", "message")
        mock_requests.post.assert_called_once()


INTERNAL_SLACK_SERVICE_DEFAULTS = {
    "api_token": "api_token",
    "environment": "test",
    "frontend_base_url": "http://localhost:port",
    "notification_channels": {
        "filter_activity": "some_slack_channel_id",
    },
}


class TestInternalSlackApiNotificationService:
    @patch.object(
        InternalSlackApiNotificationService,
        "build_filter_revision_notification_message",
        autospec=True,
    )
    @patch.object(InternalSlackApiNotificationService, "send", autospec=True)
    @pytest.mark.with_data(build_user(id=uuid.UUID(int=1)))
    def test_notify_filter_version_activity_builds_message_and_sends_notification(
        self,
        mock_send,
        mock_build_filter_revision_notification_message,
        container,
    ):
        notification_msg = "Some notification message"
        mock_build_filter_revision_notification_message.return_value = notification_msg
        filter_ = build_filter(owner_id=uuid.UUID(int=1))
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.PENDING_REVIEW,
        )
        internal_slack_service = InternalSlackApiNotificationService(
            **INTERNAL_SLACK_SERVICE_DEFAULTS,
            user_repository=container.user_repository(),
        )
        internal_slack_service.notify_filter_revision_activity(
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=True,
        )
        mock_send.assert_called_once()
        assert notification_msg in mock_send.call_args.args

    @patch.object(
        InternalSlackApiNotificationService,
        "notify_filter_revision_activity",
        autospec=True,
    )
    def test_notify_after_filter_revision_transitioned_calls_notify_filter_version_activity(  # noqa: E501
        self,
        mock_notify_filter_revision_activity,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.REVIEWED,
        )
        internal_slack_service = InternalSlackApiNotificationService(
            **INTERNAL_SLACK_SERVICE_DEFAULTS,
            user_repository=container.user_repository(),
        )
        internal_slack_service.notify_after_filter_revision_transitioned(
            filter_, filter_revision
        )
        mock_notify_filter_revision_activity.assert_called_once()
        assert (
            mock_notify_filter_revision_activity.call_args.kwargs[
                "is_new_filter_revision"
            ]
            is False
        )
        assert (
            mock_notify_filter_revision_activity.call_args.kwargs.get("custom_message")
            == ""
        )

    @patch.object(
        InternalSlackApiNotificationService,
        "notify_filter_revision_activity",
        autospec=True,
    )
    def test_notify_after_filter_revision_transitioned_calls_notify_filter_version_activity_with_custom_msg(  # noqa: E501
        self,
        mock_notify_filter_revision_activity,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.REVIEWED,
        )
        custom_msg = "Traceback from error"
        internal_slack_service = InternalSlackApiNotificationService(
            **INTERNAL_SLACK_SERVICE_DEFAULTS,
            user_repository=container.user_repository(),
        )
        internal_slack_service.notify_after_filter_revision_transitioned(
            filter_,
            filter_revision,
            custom_msg,
        )
        mock_notify_filter_revision_activity.assert_called_once()
        assert (
            mock_notify_filter_revision_activity.call_args.kwargs[
                "is_new_filter_revision"
            ]
            is False
        )
        assert (
            mock_notify_filter_revision_activity.call_args.kwargs["custom_message"]
            == custom_msg
        )

    @patch.object(
        InternalSlackApiNotificationService,
        "notify_filter_revision_activity",
        autospec=True,
    )
    def test_notify_after_filter_revision_created_calls_notify_filter_version_activity(
        self,
        mock_notify_filter_revision_activity,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.PENDING_REVIEW,
        )
        internal_slack_service = InternalSlackApiNotificationService(
            **INTERNAL_SLACK_SERVICE_DEFAULTS,
            user_repository=container.user_repository(),
        )
        internal_slack_service.notify_after_filter_revision_created(
            filter_, filter_revision
        )
        mock_notify_filter_revision_activity.assert_called_once()
        assert (
            mock_notify_filter_revision_activity.call_args.kwargs[
                "is_new_filter_revision"
            ]
            is True
        )
        assert (
            mock_notify_filter_revision_activity.call_args.kwargs.get("custom_message")
            is None
        )


class TestInternalSlackApiBuildFilterRevisionNotificationMessage:
    def test_passing_new_filter_revision_and_existing_user_returns_proper_msg(
        self,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.PENDING_REVIEW,
        )
        user = build_user()
        internal_notifications_params = dict(
            environment="test",
            frontend_base_url="http://localhost:port_a",
        )
        internal_slack_service = InternalSlackApiNotificationService(
            **{
                **INTERNAL_SLACK_SERVICE_DEFAULTS,
                **internal_notifications_params,
            },
            user_repository=container.user_repository(),
        )
        result = internal_slack_service.build_filter_revision_notification_message(
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=True,
            user=user,
        )
        assert "New filter revision submitted" in result
        assert internal_notifications_params["environment"] in result
        assert internal_notifications_params["frontend_base_url"] in result
        assert f"Filter ID: {filter_.id}" in result
        assert f"Revision ID: {filter_revision.id}" in result
        assert f"Submitted by {user.name}" in result
        assert f"Comment: {filter_revision.comment}" in result

    def test_passing_new_filter_revision_and_existing_user_and_custom_msg_returns_proper_msg(  # noqa: E501
        self,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.PENDING_REVIEW,
        )
        user = build_user()
        custom_msg = "Some custom message"
        internal_slack_service = InternalSlackApiNotificationService(
            **INTERNAL_SLACK_SERVICE_DEFAULTS,
            user_repository=container.user_repository(),
        )
        result = internal_slack_service.build_filter_revision_notification_message(
            custom_message=custom_msg,
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=True,
            user=user,
        )
        assert "New filter revision submitted" in result
        assert f"Submitted by {user.name}" in result
        assert f"Comment: {filter_revision.comment}" in result
        assert f"Additional info: {custom_msg}" not in result

    def test_passing_new_filter_revision_and_no_user_returns_proper_msg(
        self,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.PENDING_REVIEW,
        )
        internal_notifications_params = dict(
            environment="test2",
            frontend_base_url="http://localhost:port_b",
        )
        internal_slack_service = InternalSlackApiNotificationService(
            **{
                **INTERNAL_SLACK_SERVICE_DEFAULTS,
                **internal_notifications_params,
            },
            user_repository=container.user_repository(),
        )
        result = internal_slack_service.build_filter_revision_notification_message(
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=True,
            user=None,
        )
        assert "New filter revision submitted" in result
        assert internal_notifications_params["environment"] in result
        assert internal_notifications_params["frontend_base_url"] in result
        assert f"Filter ID: {filter_.id}" in result
        assert f"Revision ID: {filter_revision.id}" in result
        assert "Submitted by" not in result
        assert "Submitted at" in result
        assert f"Comment: {filter_revision.comment}" in result

    def test_passing_existing_filter_revision_and_existing_user_returns_proper_msg(
        self,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.REVIEWED,
            feedback="Some feedback",
        )
        user = build_user()
        internal_notifications_params = dict(
            environment="test3",
            frontend_base_url="http://localhost:port_c",
        )
        internal_slack_service = InternalSlackApiNotificationService(
            **{
                **INTERNAL_SLACK_SERVICE_DEFAULTS,
                **internal_notifications_params,
            },
            user_repository=container.user_repository(),
        )
        result = internal_slack_service.build_filter_revision_notification_message(
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=False,
            user=user,
        )
        assert "Filter revision transitioned to " in result
        assert filter_revision.status.name in result
        assert internal_notifications_params["environment"] in result
        assert internal_notifications_params["frontend_base_url"] in result
        assert f"Filter ID: {filter_.id}" in result
        assert f"Revision ID: {filter_revision.id}" in result
        assert f"Transitioned at UTC {filter_revision.transitioned_at}" in result
        assert f"Author: {user.name}" in result
        assert f"Feedback: {filter_revision.feedback}" in result

    def test_passing_existing_filter_revision_and_no_user_returns_proper_msg(
        self,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.ENABLED,
            feedback="Some other feedback",
        )
        internal_notifications_params = dict(
            environment="test4",
            frontend_base_url="http://localhost:port_d",
        )
        internal_slack_service = InternalSlackApiNotificationService(
            **{
                **INTERNAL_SLACK_SERVICE_DEFAULTS,
                **internal_notifications_params,
            },
            user_repository=container.user_repository(),
        )
        result = internal_slack_service.build_filter_revision_notification_message(
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=False,
            user=None,
        )
        assert "Filter revision transitioned to " in result
        assert filter_revision.status.name in result
        assert internal_notifications_params["environment"] in result
        assert internal_notifications_params["frontend_base_url"] in result
        assert f"Filter ID: {filter_.id}" in result
        assert f"Revision ID: {filter_revision.id}" in result
        assert f"Transitioned at UTC {filter_revision.transitioned_at}" in result
        assert "Author:" not in result
        assert f"Feedback: {filter_revision.feedback}" in result

    def test_passing_existing_filter_revision_and_no_user_and_custom_msg_returns_proper_msg(  # noqa: E501
        self,
        container,
    ):
        filter_ = build_filter()
        filter_revision = build_filter_revision(
            filter_=filter_,
            status=FilterRevisionStatus.ENABLED,
            feedback="Some other feedback",
        )
        custom_msg = "Some custom message"
        internal_slack_service = InternalSlackApiNotificationService(
            **INTERNAL_SLACK_SERVICE_DEFAULTS,
            user_repository=container.user_repository(),
        )
        result = internal_slack_service.build_filter_revision_notification_message(
            custom_message=custom_msg,
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=False,
            user=None,
        )
        assert "Filter revision transitioned to " in result
        assert "Author:" not in result
        assert f"Feedback: {filter_revision.feedback}" in result
        assert f"Additional info: {custom_msg}" in result
