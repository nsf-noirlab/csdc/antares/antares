def test_watchlists_filters_execution_configured(container):
    for survey in container.config.pipeline.ingestion.broker.surveys():
        assert survey["watchlists_on"] in (True, False)

    for survey in container.config.pipeline.ingestion.broker.surveys():
        assert survey["filters_on"] in (True, False)
