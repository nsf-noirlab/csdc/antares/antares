import uuid
from test.fakes import (
    FakeAlertRepository,
    FakeCatalogObjectRepository,
    FakeLocusRepository,
    FakeMetrics,
    build_catalog_object,
)

import pytest
from astropy.coordinates import Angle, SkyCoord

from antares.domain.models import (
    Alert,
    Announcement,
    Locus,
    Survey,
    WatchList,
    WatchObject,
)


@pytest.fixture(autouse=True)
def m_bus(message_bus):
    yield message_bus


def test_fake_locus_repository_saves_locus():
    repo = FakeLocusRepository()
    locus = Locus("locus-001", SkyCoord("0d 0d"))
    repo.add(locus)
    assert repo.get("locus-001") == locus


def test_fake_locus_repository_returns_all_loci_within_cone_search():
    repo = FakeLocusRepository()
    loci = [
        Locus("locus-001", SkyCoord("0d 0d")),
        Locus("locus-002", SkyCoord("0d 2d")),
    ]
    for locus in loci:
        repo.add(locus)
    assert repo.list_by_cone_search(SkyCoord("0d 0d"), Angle("1d")) == [loci[0]]


def test_fake_locus_repository_returns_empty_list_if_no_loci_within_cone_search():
    repo = FakeLocusRepository()
    loci = [
        Locus("locus-001", SkyCoord("0d 2d")),
        Locus("locus-002", SkyCoord("0d 3d")),
    ]
    for locus in loci:
        repo.add(locus)
    assert repo.list_by_cone_search(SkyCoord("0d 0d"), Angle("1d")) == []


def test_fake_locus_repository_returns_locus_if_found_by_locus_id():
    locus = Locus("locus-001", SkyCoord("0d 0d"))
    repo = FakeLocusRepository([locus])
    assert repo.get("locus-001") == locus


def test_fake_locus_repostiory_returns_none_if_no_locus_with_locus_id():
    locus = Locus("locus-001", SkyCoord("0d 0d"))
    repo = FakeLocusRepository([locus])
    assert repo.get("locus-002") is None


def test_fake_alert_repository_saves_alert():
    repo = FakeAlertRepository()
    alert = Alert(
        "alert-001",
        mjd=59000.0,
        survey=Survey.SURVEY_ZTF,
        location=SkyCoord("0d 0d"),
        normalized_properties={},
    )
    repo.add(alert, "locus-001")
    actual = repo.get("alert-001")
    assert actual == alert


def test_fake_catalog_object_repository_returns_all_catalog_objects_at_location():
    catalog_objects = [
        build_catalog_object(location=SkyCoord("0d 0d"), radius=Angle("1d")),
        build_catalog_object(location=SkyCoord("0d 2d"), radius=Angle("1d")),
    ]
    repo = FakeCatalogObjectRepository(catalog_objects)
    assert repo.list_by_location(SkyCoord("0d 0d"), FakeMetrics()) == [catalog_objects[0]]


def test_fake_catalog_object_repository_returns_empty_list_if_no_catalog_objects_at_location():
    catalog_objects = [
        build_catalog_object(location=SkyCoord("0d 2d"), radius=Angle("1d")),
        build_catalog_object(location=SkyCoord("0d 3d"), radius=Angle("2d")),
    ]
    repo = FakeCatalogObjectRepository(catalog_objects)
    assert repo.list_by_location(SkyCoord("0d 0d"), FakeMetrics()) == []


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=1), owner_id=uuid.UUID(int=1), name="WL 001", description=""
    ),
    WatchList(
        id=uuid.UUID(int=2), owner_id=uuid.UUID(int=1), name="WL 002", description=""
    ),
    WatchObject(
        id=uuid.UUID(int=1),
        watch_list_id=uuid.UUID(int=1),
        name="WO 001",
        location=SkyCoord("0d 0d"),
        radius=Angle("1s"),
    ),
    WatchObject(
        id=uuid.UUID(int=2),
        watch_list_id=uuid.UUID(int=2),
        name="WO 002",
        location=SkyCoord("2d 0d"),
        radius=Angle("1s"),
    ),
)
def test_fake_watch_object_repository_returns_all_watch_objects_at_location(container):
    watch_objects = list(
        container.watch_object_repository().list_by_location(SkyCoord("0d 0d"))
    )
    assert len(watch_objects) == 1
    assert watch_objects[0].id == uuid.UUID(int=1)


@pytest.mark.with_data(
    WatchList(
        id=uuid.UUID(int=1), owner_id=uuid.UUID(int=1), name="WL 001", description=""
    ),
    WatchList(
        id=uuid.UUID(int=2), owner_id=uuid.UUID(int=1), name="WL 002", description=""
    ),
    WatchObject(
        id=uuid.UUID(int=1),
        watch_list_id=uuid.UUID(int=1),
        name="WO 001",
        location=SkyCoord("8d 0d"),
        radius=Angle("1s"),
    ),
    WatchObject(
        id=uuid.UUID(int=2),
        watch_list_id=uuid.UUID(int=2),
        name="WO 002",
        location=SkyCoord("2d 0d"),
        radius=Angle("1s"),
    ),
)
def test_fake_watch_object_repository_returns_empty_list_if_no_watch_objects_at_location(
    container,
):
    assert (
        list(container.watch_object_repository().list_by_location(SkyCoord("0d 0d")))
        == []
    )


@pytest.mark.with_data(
    Announcement(id=1, message="foobar", variant="warning", active=True),
    Announcement(id=2, message="foobar", variant="success", active=False),
)
def test_fake_announcement_repository_supports_filtering(announcement_repository):
    announcements = announcement_repository.list(
        query_filters=[{"field": "active", "op": "eq", "value": True}]
    )
    announcements = list(announcements)
    assert len(announcements) == 1
    assert announcements[0].id == 1
