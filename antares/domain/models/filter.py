from __future__ import annotations

import abc
import contextlib
import copy
import inspect
import json
import uuid
from collections import defaultdict
from collections.abc import Callable, Collection, Container
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from types import TracebackType
from typing import Any, Optional, Union

import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.timeseries import TimeSeries
from statemachine import State, StateMachine

from antares.exceptions import HaltPipeline
from antares.utils import mjd_to_datetime
from antares.utils.collections_extensions import (
    RestrictedWriteDictWrapper,
    RestrictedWriteSetWrapper,
)
from antares.utils.validation import validate_lists_are_equal_length
from antares.utils.ztf_flux_correction import correct_mags

from .alert import Alert, AlertGravWaveEvent
from .catalog import CatalogObject
from .locus import Locus
from .notice import GravWaveNotice
from .user import User


class FilterRevisionStatus(Enum):
    PENDING_REVIEW = 1
    REVIEWED = 2
    APPROVED = 3
    REJECTED = 4
    ENABLED = 5
    DISABLED = 6


@dataclass(frozen=True)
class FilterConstraints:
    required_locus_properties: Collection[str] = field(default_factory=set)
    required_grav_wave_prob_region: float = field(default=0)
    required_alert_properties: Collection[str] = field(default_factory=set)
    required_tags: Collection[str] = field(default_factory=set)


@dataclass(frozen=True)
class FilterOutputSpecificationProperty:
    name: str
    type: Union[type[int], type[str], type[float]]
    description: str


@dataclass(frozen=True)
class FilterOutputSpecification:
    properties: Collection[FilterOutputSpecificationProperty] = field(
        default_factory=set
    )
    tags: Container[str] = field(default_factory=set)


class FilterReturn:
    pass


@dataclass(frozen=True)
class FilterSucceeded(FilterReturn):
    pass


@dataclass(frozen=True)
class FilterSkipped(FilterReturn):
    pass


@dataclass(frozen=True)
class FilterErrored(FilterReturn):
    exception: Exception
    traceback: Optional[TracebackType]


@dataclass(frozen=True)
class FilterExecutable:
    filter_id: int
    filter_revision_id: int
    callable: Callable
    constraints: FilterConstraints
    output_specification: FilterOutputSpecification
    slack_channel: Optional[str] = None

    def __call__(self, *args, **kwargs):
        self.callable(*args, **kwargs)


@dataclass(frozen=True)
class FilterContextAlert:
    alert_id: str
    locus_id: str
    mjd: float
    processed_at: datetime
    properties: dict
    grav_wave_events: list[AlertGravWaveEvent] = field(default_factory=list)

    def to_dict(self) -> dict:
        processed_at = None
        if self.processed_at:
            processed_at = self.processed_at.isoformat()
        return {
            "alert_id": self.alert_id,
            "locus_id": self.locus_id,
            "mjd": self.mjd,
            "processed_at": processed_at,
            "properties": self.properties,
            "grav_wave_events": self.grav_wave_events,
        }

    @staticmethod
    def from_dict(data: dict) -> "FilterContextAlert":
        processed_at = None
        if data.get("processed_at"):
            processed_at = datetime.fromisoformat(data["processed_at"])
        return FilterContextAlert(**{**data, "processed_at": processed_at})


def build_filter_context_alert(alert: Alert, locus_id: str):
    return FilterContextAlert(
        alert_id=alert.id,
        locus_id=locus_id,
        mjd=alert.mjd,
        processed_at=alert.created_at,
        properties={
            **alert.normalized_properties,
            **alert.properties,
            **alert.non_persistent_properties,
        },
        grav_wave_events=alert.grav_wave_events,
    )


@dataclass(frozen=True, eq=False)
class FilterContext:
    #: Unique identifier of a locus (e.g. ANT2020s6ylc)
    locus_id: str
    #: list of alerts
    alerts: list[FilterContextAlert]
    #: Right Ascencion
    ra: float
    #: Declination
    dec: float
    #: Properties from the survey and those set by filters
    properties: RestrictedWriteDictWrapper
    #: Keywords attached to loci by filters to callout certain objects
    tags: RestrictedWriteSetWrapper
    #: Catalogs that the locus pipeline found matches against, cross-matching
    catalog_objects: dict[str, list[dict[str, Any]]]
    #: The lightcurve:
    #: alert_id,ant_mjd, ant_survey, ant_ra, ant_dec, ant_passband, ant_mag, ant_magerr, ant_maglim, ant_mag_corrected,
    #: ant_magerr_corrected
    lightcurve: Optional[TimeSeries] = None
    #: The lightcurve:
    #: alert_id, ant_mjd, ant_survey, ant_ra, ant_dec, ant_passband, ant_mag, ant_magerr, ant_maglim, ant_mag_corrected,
    #: ant_magerr_corrected plus many more properties from the survey(s)
    timeseries: Optional[TimeSeries] = None
    #: Dictionary of events metadata, keyed on gracedb_id
    grav_wave_events_metadata: dict[str, GravWaveNotice] = field(default_factory=dict)
    # TODO: Watch list integration?

    def __post_init__(self):
        if self.timeseries is None:
            super().__setattr__("timeseries", self._build_timeseries())
        if self.lightcurve is None:
            super().__setattr__("lightcurve", self._build_lightcurve())

    def __eq__(self, other):
        if not isinstance(other, FilterContext):
            return NotImplemented
        return (
            self.locus_id == other.locus_id
            and self.alerts == other.alerts
            and self.ra == other.ra
            and self.dec == other.dec
            and self.properties == other.properties
            and self.tags == other.tags
            and self.catalog_objects == other.catalog_objects
            and self.lightcurve.to_pandas().equals(other.lightcurve.to_pandas())
            and self.timeseries.to_pandas().equals(other.timeseries.to_pandas())
            and self.grav_wave_events_metadata == other.grav_wave_events_metadata
        )

    def __repr__(self):
        return f'FilterContext(locus_id="{self.locus_id}")'

    def __str__(self):
        return f"{vars(self)}"

    def _build_timeseries(self):
        data = [
            {"alert_id": alert.alert_id, "ant_mjd": alert.mjd, **alert.properties}
            for alert in self.alerts
        ]
        times = [mjd_to_datetime(alert.mjd) for alert in self.alerts]
        timeseries = TimeSeries(data=data, time=times)
        df, _, _ = correct_mags(timeseries)
        # fmt: off
        validate_lists_are_equal_length(df["ant_mag_corrected"], timeseries)
        timeseries["ant_mag_corrected"] = np.ma.masked_invalid(df["ant_mag_corrected"])
        timeseries["ant_magerr_corrected"] = np.ma.masked_invalid(df["ant_magerr_corrected"])
        timeseries["ant_magulim_corrected"] = np.ma.masked_invalid(df["ant_magulim_corrected"])
        timeseries["ant_magllim_corrected"] = np.ma.masked_invalid(df["ant_magllim_corrected"])
        # fmt: on
        return timeseries

    def _build_lightcurve(self):
        columns = [
            "alert_id",
            "ant_mjd",
            "ant_survey",
            "ant_ra",
            "ant_dec",
            "ant_passband",
            "ant_mag",
            "ant_magerr",
            "ant_maglim",
            "ant_mag_corrected",
            "ant_magerr_corrected",
            "ant_magulim_corrected",
            "ant_magllim_corrected",
        ]
        missing_columns = set(columns) - set(self.timeseries.colnames)
        if missing_columns:
            print(
                "Building the lightcurve with the following missing columns:"
                f" {missing_columns}"
            )
            rows = len(self.timeseries)
            for column in missing_columns:
                self.timeseries[column] = np.ma.masked_invalid(
                    np.full(shape=rows, fill_value=np.nan)
                )
        return self.timeseries[columns]

    @property
    def alert(self):
        """Exposed for backwards compatibility: and continued exposure due to needing to check if an
        alert satisfies a filter constraint for an alert's properties
        in antares.domain.models.filter.run_filter_against_locus
        """
        return self.alerts[-1]

    @property
    def location(self) -> SkyCoord:
        """Exposed for backwards compatibility"""
        return SkyCoord(ra=self.ra, dec=self.dec, unit=u.deg)

    @property
    def halt(self):
        """
        Return the HaltPipeline exception class.

        Stages can raise this exception to halt the pipeline like::

          raise locus.halt

        :return: class `HaltPipeline`
        """
        return HaltPipeline

    def tag(self, name):
        """
        Tag the Locus.

        :param name: Tag name
        """
        self.tags.add(name)

    def to_dict(self) -> dict:
        """
        Return the locus as a dictionary type
        """
        return {
            "locus_id": self.locus_id,
            "ra": self.ra,
            "dec": self.dec,
            "properties": self.properties.dict,
            "tags": list(self.tags.set),
            # "watch_list_ids": list(self.watch_list_ids),
            # "watch_object_ids": list(self.watch_object_ids),
            "catalog_objects": self.catalog_objects,
            "alerts": [alert.to_dict() for alert in self.alerts],
        }

    @staticmethod
    def from_dict(data: dict) -> "FilterContext":
        return FilterContext(
            locus_id=data["locus_id"],
            alerts=[FilterContextAlert.from_dict(alert) for alert in data["alerts"]],
            ra=data["ra"],
            dec=data["dec"],
            properties=RestrictedWriteDictWrapper(data["properties"]),
            tags=RestrictedWriteSetWrapper(set(data["tags"])),
            # "watch_list_ids": set(data["watch_list_ids"]),
            # "watch_object_ids": set(data["watch_object_ids"]),
            catalog_objects=data["catalog_objects"],
        )

    def to_file(self, filename: str) -> None:
        """
        Write the locus to specified file
        """
        with open(filename, "w") as f:
            json.dump(self.to_dict(), f)

    @staticmethod
    def from_file(filename: str) -> "FilterContext":
        with open(filename, "r") as f:
            return FilterContext.from_dict(json.load(f))


def build_filter_context(
    locus: Locus,
    alerts: list[Alert],
    catalog_objects: list[CatalogObject],
    timeseries: Optional[TimeSeries] = None,
    grav_wave_events_metadata: Optional[dict[str, GravWaveNotice]] = None,
) -> FilterContext:
    catalog_objects_combined = defaultdict(list)
    for catalog_object in catalog_objects:
        catalog_objects_combined[catalog_object.catalog_name].append(
            catalog_object.properties
        )
    return FilterContext(
        locus_id=locus.id,
        ra=locus.location.ra.deg,
        dec=locus.location.dec.deg,
        alerts=[
            build_filter_context_alert(alert=alert, locus_id=locus.id)
            for alert in alerts
        ],
        catalog_objects=catalog_objects_combined,
        properties=RestrictedWriteDictWrapper(locus.properties),
        tags=RestrictedWriteSetWrapper(locus.tags),
        timeseries=timeseries,
        grav_wave_events_metadata=(grav_wave_events_metadata or {}),
    )


class FilterDevkitApi(abc.ABC):
    """
    ANTARES Filter base class.
    """

    FILTER_ID = 0
    FILTER_VERSION_ID = 0
    NAME = None
    ERROR_SLACK_CHANNEL = None
    RUN_IN_MUTEX = False
    REQUIRES_FILES: list[str] = [
        # 'cstubens_myfile.txt',
    ]

    INPUT_LOCUS_PROPERTIES: list[str] = [
        # 'num_mag_values',
    ]
    REQUIRED_LOCUS_PROPERTIES: list[str] = []
    INPUT_ALERT_PROPERTIES: list[str] = [
        # 'mag',
    ]
    REQUIRED_ALERT_PROPERTIES: list[str] = []
    INPUT_TAGS: list[str] = [
        # 'foo',
    ]
    REQUIRED_TAGS: list[str] = []
    REQUIRED_GRAV_WAVE_PROB_REGION = 0

    OUTPUT_LOCUS_PROPERTIES: list[dict[str, str]] = [
        # {
        #     'name': 'foo',
        #     'type': 'int',
        #     'description': 'blah blah',
        # },
    ]
    OUTPUT_TAGS: list[dict[str, str]] = [
        # {
        #     'name': 'bar',
        #     'description': 'blah blah',
        # },
    ]

    def __init__(self):
        self.files = {}  # Loaded by FilterWrapper

    def setup(self):
        pass

    @abc.abstractmethod
    def run(self, locus_data: FilterContext):
        raise NotImplementedError

    @classmethod
    def get_config_names(cls):
        return [
            name
            for name in cls.__dict__
            if not name.startswith("_") and name == name.upper()
        ]

    def to_filter_executable(self) -> FilterExecutable:
        from antares import query

        self.files = query.load_files(self.REQUIRES_FILES)
        self.setup()
        required_locus_properties = (
            self.INPUT_LOCUS_PROPERTIES + self.REQUIRED_LOCUS_PROPERTIES
        )
        required_alert_properties = (
            self.INPUT_ALERT_PROPERTIES + self.REQUIRED_ALERT_PROPERTIES
        )
        required_tags = self.INPUT_TAGS + self.REQUIRED_TAGS
        required_grav_wave_prob_region = self.REQUIRED_GRAV_WAVE_PROB_REGION
        return FilterExecutable(
            filter_id=self.FILTER_ID,
            filter_revision_id=self.FILTER_VERSION_ID,
            callable=self.run,
            constraints=FilterConstraints(
                required_locus_properties=required_locus_properties,
                required_alert_properties=required_alert_properties,
                required_grav_wave_prob_region=required_grav_wave_prob_region,
                required_tags=required_tags,
            ),
            output_specification=self._load_output_specification(),
            slack_channel=self.ERROR_SLACK_CHANNEL,
        )

    def _load_output_specification(self) -> FilterOutputSpecification:
        output_properties = []
        for property_ in self.OUTPUT_LOCUS_PROPERTIES:
            type_: Union[type[int], type[str], type[float]]
            if property_["type"] == "int":
                type_ = int
            elif property_["type"] == "float":
                type_ = float
            elif property_["type"] == "str":
                type_ = str
            else:
                raise ValueError(f"Unknown or unsupported type: '{property_['type']}'")
            output_properties.append(
                FilterOutputSpecificationProperty(
                    name=property_["name"],
                    type=type_,
                    description=property_["description"],
                )
            )
        return FilterOutputSpecification(
            properties=output_properties,
            tags=[tag["name"] for tag in self.OUTPUT_TAGS],
        )


@dataclass
class FilterRevision(StateMachine):
    """
    This class represents a particular revision of a user-submitted filter on ANTARES.

    Instances of this class must uphold these invariants:

    * A `FilterRevision` instance MUST not be cast to `FilterExecutable` unless it is
      has a `status` of `FilterRevisionStatus.ENABLED`. This is to prevent any user-
      submitted code from being executed without being reviewed and sanitized by our
      team.
    * No updates to the `status` field should be done directly (through assignment or
      calling setattr) but only through triggering its state machine events directly
      or calling the `transition_to` method that receives a target status

    """

    filter_id: int
    code: str
    status: FilterRevisionStatus = field(
        default_factory=lambda: FilterRevisionStatus.PENDING_REVIEW
    )
    id: Optional[int] = field(default_factory=lambda: None)
    comment: str = field(default_factory=str)
    feedback: str = field(default_factory=str)
    created_at: datetime = field(default_factory=datetime.utcnow)
    updated_at: datetime = field(default_factory=datetime.utcnow)
    transitioned_at: datetime = field(default_factory=datetime.utcnow)
    version_id: int = field(default_factory=lambda: 1)

    def __post_init__(self):
        super().__init__(model=self, state_field="status")

    # States definition
    pending_review = State(
        FilterRevisionStatus.PENDING_REVIEW.name,
        value=FilterRevisionStatus.PENDING_REVIEW,
        initial=True,
    )
    reviewed = State(
        FilterRevisionStatus.REVIEWED.name,
        value=FilterRevisionStatus.REVIEWED,
    )
    approved = State(
        FilterRevisionStatus.APPROVED.name,
        value=FilterRevisionStatus.APPROVED,
    )
    rejected = State(
        FilterRevisionStatus.REJECTED.name,
        value=FilterRevisionStatus.REJECTED,
    )
    enabled = State(
        FilterRevisionStatus.ENABLED.name,
        value=FilterRevisionStatus.ENABLED,
    )
    disabled = State(
        FilterRevisionStatus.DISABLED.name,
        value=FilterRevisionStatus.DISABLED,
    )

    # Events definition
    review = pending_review.to(reviewed)
    mark_as_pending = (
        reviewed.to(pending_review)
        | approved.to(pending_review)
        | rejected.to(pending_review)
        | disabled.to(pending_review)
    )
    approve = pending_review.to(approved)
    reject = pending_review.to(rejected, validators="has_feedback")
    enable = reviewed.to(enabled) | approved.to(enabled) | disabled.to(enabled)
    disable = enabled.to(disabled, validators="has_feedback")

    def transition_to(self, status: FilterRevisionStatus, **kwargs):
        """
        This methods allows to trigger an event based on a desired status. Useful when
        we don't know which event to call but we do know the status we want to reach.
        It only works when we have a unique event to transition from state A to state B
        """
        status_to_event = {
            FilterRevisionStatus.REVIEWED: "review",
            FilterRevisionStatus.PENDING_REVIEW: "mark_as_pending",
            FilterRevisionStatus.APPROVED: "approve",
            FilterRevisionStatus.REJECTED: "reject",
            FilterRevisionStatus.ENABLED: "enable",
            FilterRevisionStatus.DISABLED: "disable",
        }
        self.send(status_to_event[status], **kwargs)

    def after_transition(self, feedback: str = ""):
        """
        This is an action for the state machine that is automatically called after
        any transition successfully occurs. We want to update the `transitioned_at`
        field and also assign the `feedback` field to the `FilterRevision` instance
        """
        self.transitioned_at = datetime.utcnow()
        self.feedback = feedback

    def on_enable(self, filter_: Filter):
        if not isinstance(filter_, Filter):
            raise ValueError("A filter instance is required to perform this transition")
        if self.filter_id != filter_.id:
            raise ValueError("Filter revision not owned by passed filter")

        # ** WARNING **
        # _enable_filter_revision MUST NOT be called anywhere else than here
        # since it's a side effect of transitioning a filter revision to ENABLED
        # We're consciously calling this method explicitly even though it's a
        # Filter's internal method to keep data integrity
        filter_._enable_filter_revision(self)

    def on_disable(self, filter_: Filter):
        if not isinstance(filter_, Filter):
            raise ValueError("A filter instance is required to perform this transition")
        if self.filter_id != filter_.id:
            raise ValueError("Filter revision not owned by passed filter")

        # ** WARNING **
        # _disable MUST NOT be called anywhere else than here
        # since it's a side effect of transitioning a filter revision to DISABLED
        # We're consciously calling this method explicitly even though it's a
        # Filter's internal method to keep data integrity
        filter_._disable()

    def has_feedback(self, source, target, **kwargs):
        """
        This is a validator for the state machine transitions that requires
        the inclusion of non-empty feedback in order to perform the transition.
        Check the events definition the see which transitions use this validator
        """
        if not kwargs.get("feedback"):
            raise ValueError(
                f"FilterRevision must include feedback in order to transition from "
                f"{source.name} to {target.name}"
            )

    def to_filter_executable(self) -> FilterExecutable:
        """
        DANGEROUS! Converts this filter revision into a callable, execs user-submitted
        code in the process so the filter revision must be validated.
        """
        # TODO: Handle errors here
        if self.status != FilterRevisionStatus.ENABLED:
            raise ValueError("Filter version must be enabled before it can be loaded.")
        if self.id is None:
            raise ValueError("Filter without revision_id can not be loaded.")
        return FilterExecutable(
            filter_id=self.filter_id,
            filter_revision_id=self.id,
            callable=self._load_callable(),
            constraints=self._load_constraints(),
            output_specification=self._load_output_specification(),
            slack_channel=self._load_slack_channel(),
        )

    def _load_filter_class(self) -> type[FilterDevkitApi]:
        code = (
            "import antares.devkit as dk\n\n"
            + "def print(*a, **kw): pass\n\n"
            + self.code
            + "\n\n__locals_extractor__.update(locals())"
        )
        extracted_locals: dict[Any, Any] = {}
        exec(code, {"__locals_extractor__": extracted_locals})
        classes: list[type[FilterDevkitApi]] = [
            obj
            for obj in extracted_locals.values()
            if inspect.isclass(obj)
            and FilterDevkitApi.__subclasscheck__(obj)
            and obj is not FilterDevkitApi
        ]
        if not classes:
            raise RuntimeError("No Filter subclasses found in code")
        if len(classes) > 1:
            raise RuntimeError("Multiple Filter subclasses found in code")
        return classes[0]

    def _load_slack_channel(self) -> Optional[str]:
        filter_class = self._load_filter_class()
        return filter_class.ERROR_SLACK_CHANNEL

    def _load_callable(self) -> Callable[[FilterContext], None]:
        from antares import query

        filter_class = self._load_filter_class()
        filter_ = filter_class()
        filter_.files = query.load_files(filter_class.REQUIRES_FILES)
        filter_.setup()
        return filter_.run

    def _load_constraints(self) -> FilterConstraints:
        filter_class = self._load_filter_class()
        required_locus_properties = (
            filter_class.INPUT_LOCUS_PROPERTIES + filter_class.REQUIRED_LOCUS_PROPERTIES
        )
        required_alert_properties = (
            filter_class.INPUT_ALERT_PROPERTIES + filter_class.REQUIRED_ALERT_PROPERTIES
        )
        required_tags = filter_class.INPUT_TAGS + filter_class.REQUIRED_TAGS
        required_grav_wave_prob_region = filter_class.REQUIRED_GRAV_WAVE_PROB_REGION

        return FilterConstraints(
            required_locus_properties=required_locus_properties,
            required_alert_properties=required_alert_properties,
            required_grav_wave_prob_region=required_grav_wave_prob_region,
            required_tags=required_tags,
        )

    def _load_output_specification(self) -> FilterOutputSpecification:
        filter_class = self._load_filter_class()
        output_properties = []
        for property in filter_class.OUTPUT_LOCUS_PROPERTIES:
            type_: Union[type[int], type[str], type[float]]
            if property["type"] == "int":
                type_ = int
            elif property["type"] == "float":
                type_ = float
            elif property["type"] == "str":
                type_ = str
            else:
                raise ValueError(f"Uknown or unsupported type: '{property['type']}'")
            output_properties.append(
                FilterOutputSpecificationProperty(
                    name=property["name"],
                    type=type_,
                    description=property["description"],
                )
            )
        return FilterOutputSpecification(
            properties=output_properties,
            tags=[tag["name"] for tag in filter_class.OUTPUT_TAGS],
        )


@dataclass
class Filter:
    """
    Describes a user-submitted ANTARES filter. These are the "functions" in the ANTARES
    Functions-as-a-Service offering. Each filter can have multiple associated revisions,
    see the `FilterRevision` class for a description of those. The `Filter` class
    asserts these invariants:

    * Only one revision of a filter may be enabled at any time (supported by the
      one-to-one mapping from `enabled_filter_revision_id` to a `FilterRevision`.

    * If `enabled_filter_revision_id` is `None`, `enabled` must be `False`.
    """

    name: str
    owner_id: uuid.UUID
    # The labmdas in these fields are necessary for the sqlalchemy object mapper
    id: Optional[int] = field(default_factory=lambda: None)
    public: bool = field(default_factory=lambda: False)
    description: Optional[str] = field(default_factory=lambda: "")
    enabled_filter_revision_id: Optional[int] = field(default_factory=lambda: None)
    created_at: datetime = field(default_factory=datetime.utcnow)
    updated_at: datetime = field(default_factory=datetime.utcnow)
    disabled_at: Optional[datetime] = field(default_factory=lambda: None)
    version_id: int = field(default_factory=lambda: 1)

    def __post_init__(self):
        if self.enabled and self.enabled_filter_revision_id is None:
            raise ValueError("Filter enabled with no enabled revision")

    def _enable_filter_revision(self, filter_revision: FilterRevision):
        """
        Internal method that performs side effects of enabling a `FilterRevision`.
        MUST be called only when transitioning a `FilterRevision` to ENABLED.
        Never ever call this method from other places as it will alter data integrity
        """
        if self.enabled:
            raise ValueError(
                "Filter already has an enabled filter revision. Disable that one first"
            )
        if filter_revision.filter_id != self.id:
            raise ValueError("Filter revision not owned by this filter")

        self.enabled_filter_revision_id = filter_revision.id
        self.disabled_at = None

    def _disable(self):
        """
        Internal method that performs side effects of disabling a `FilterRevision`.
        MUST be called only when transitioning a `FilterRevision` to DISABLED.
        Never ever call this method from other places as it will alter data integrity
        """
        if not self.enabled:
            raise ValueError("Filter is not enabled so it cannot be disabled")

        self.enabled_filter_revision_id = None
        self.disabled_at = datetime.utcnow()

    def make_public(self):
        self.public = True

    def make_private(self):
        self.public = False

    @property
    def enabled(self):
        return self.enabled_filter_revision_id is not None

    @property
    def owner(self) -> Optional[User]:
        try:
            return self._owner
        except AttributeError:
            # self._owner is not formally defined as field so it doesn't exist if
            # it's not explicitly set. Therefore we default to None when that happens
            return None

    @owner.setter
    def owner(self, owner: User):
        """
        Attaches the corresponding `User` instance as owner to the filter.
        Since the model does not load the owner automatically but only holds `owner_id`,
        this property setter allows to have access to the owner instance on demand
        """
        if not isinstance(owner, User):
            raise ValueError("Filter attached owner must be an instance of User")
        if owner.id != self.owner_id:
            raise ValueError("Filter attached owner's id must match owner_id")
        self._owner = owner


@contextlib.contextmanager
def filter_context_guard(
    filter_context: FilterContext, output_specification: FilterOutputSpecification
):
    writeable_properties = {p.name for p in output_specification.properties}
    writeable_tags = output_specification.tags
    original_writeable_properties = copy.copy(filter_context.properties.writeable_keys)
    original_writeable_tags = copy.copy(filter_context.tags.writeable_elements)
    filter_context.properties.writeable_keys = writeable_properties
    filter_context.tags.writeable_elements = writeable_tags
    yield
    filter_context.properties.writeable_keys = original_writeable_properties
    filter_context.tags.writeable_elements = original_writeable_tags


def run_filter_against_locus(
    filter_executable: FilterExecutable,
    locus: Locus,
    alerts: list[Alert],
    timeseries: TimeSeries,
    catalog_objects: list[CatalogObject],
    grav_wave_events_metadata: Optional[dict[str, GravWaveNotice]] = None,
) -> FilterReturn:
    """
    This function executes a user-submitted filter against a locus. We check that the
    locus satisfies any constraints defined in the filter as far as input properties,
    input tags, etc... are concerned.

    The filter runs not against the locus but against a FilterContext, a wrapper
    that limits read/write access to filter-specific fields of the locus.

    If a filter crashes while processing a locus it should be disabled.
    """
    if locus_satisfies_filter_constraints(locus, filter_executable):
        try:
            context = build_filter_context(
                locus, alerts, catalog_objects, timeseries, grav_wave_events_metadata
            )
            if alert_satisfies_filter_constraints(context.alert, filter_executable):
                with filter_context_guard(
                    context, filter_executable.output_specification
                ):
                    filter_executable(context)
                    return FilterSucceeded()
            else:
                return FilterSkipped()

        except Exception as e:
            return FilterErrored(exception=e, traceback=e.__traceback__)
    else:
        return FilterSkipped()


def locus_satisfies_filter_constraints(
    locus: Union[Locus, FilterContext], executable: FilterExecutable
) -> bool:
    for prop in executable.constraints.required_locus_properties:
        if prop not in locus.properties:
            return False
    for tag in executable.constraints.required_tags:
        if tag not in locus.tags:
            return False
    return True


def alert_satisfies_filter_constraints(
    alert: FilterContextAlert, executable: FilterExecutable
) -> bool:
    if isinstance(alert, Alert):
        raise ValueError(
            "Alert properties contains survey specific properties and normalized properties separated. "
            "Input should be a FilterContextAlert instance instead of an Alert."
        )
    for prop in executable.constraints.required_alert_properties:
        if prop not in alert.properties:
            return False
    if max_level := executable.constraints.required_grav_wave_prob_region:
        matches = [gw["contour_level"] <= max_level for gw in alert.grav_wave_events]
        if not any(matches):
            return False
    return True
