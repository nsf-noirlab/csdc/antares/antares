from dataclasses import dataclass, field
from datetime import datetime
from typing import Literal, Optional


@dataclass
class Announcement:
    message: str
    variant: Literal[
        "primary", "secondary", "success", "danger", "warning", "info", "light", "dark"
    ]
    id: Optional[int] = field(default_factory=lambda: None)
    active: bool = field(default_factory=lambda: False)
    created_at: datetime = field(default_factory=datetime.utcnow)
