import datetime
from dataclasses import dataclass, field
from uuid import UUID, uuid4

from astropy.coordinates import Angle, SkyCoord


@dataclass(frozen=True)
class WatchObject:
    watch_list_id: UUID
    name: str
    location: SkyCoord = field(hash=False)
    radius: Angle = field(hash=False)
    id: UUID = field(default_factory=lambda: uuid4())


@dataclass(frozen=True)
class WatchList:
    owner_id: UUID
    name: str
    description: str
    slack_channel: str = field(default_factory=lambda: "")
    created_at: datetime.datetime = field(default_factory=datetime.datetime.utcnow)
    enabled: bool = field(default_factory=lambda: True)
    id: UUID = field(default_factory=lambda: uuid4())
