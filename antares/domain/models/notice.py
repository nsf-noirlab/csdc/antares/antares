from __future__ import annotations

from base64 import b64decode
from copy import deepcopy
from dataclasses import InitVar, dataclass, field
from datetime import datetime
from enum import Enum
from io import BytesIO
from typing import ClassVar, Optional

import astropy.units as u
import astropy_healpix as ah
import numpy as np
from astropy.coordinates import SkyCoord
from astropy.table import Table

from antares.utils.datetimes import parse_datetime


class GravWaveNoticeTypes(Enum):
    EARLY_WARNING = "EARLY_WARNING"
    EARLYWARNING = "EARLY_WARNING"
    PRELIMINARY = "PRELIMINARY"
    INITIAL = "INITIAL"
    UPDATE = "UPDATE"
    RETRACTION = "RETRACTION"


@dataclass
class GravWaveNotice:
    gracedb_id: str
    notice_type: GravWaveNoticeTypes
    notice_datetime: datetime
    id: Optional[int] = field(default_factory=lambda: None)
    event_datetime: Optional[datetime] = field(default_factory=lambda: None)
    false_alarm_rate: Optional[float] = field(default_factory=lambda: None)
    skymap_base64: Optional[str] = field(default_factory=lambda: None)
    external_coinc: Optional[dict] = field(default_factory=lambda: None)
    full_notice: Optional[dict] = field(default_factory=lambda: None)
    version_id: int = field(default_factory=lambda: 1)

    _max_level: ClassVar[int] = 29
    _skymap: InitVar[Table | None] = None
    _healpix_nested_indices: InitVar[np.ndarray | None] = None
    _healpix_sorter: InitVar[np.ndarray | None] = None
    _probability_density_sorter: InitVar[np.ndarray | None] = None
    _sorted_pixel_areas: InitVar[np.ndarray | None] = None
    _cumprob: InitVar[np.ndarray | None] = None

    @classmethod
    def from_gcn(cls, notice_dict: dict) -> GravWaveNotice:
        notice_dict = deepcopy(notice_dict)  # no side effect, thank you
        fmts = [
            "%Y-%m-%d %H:%M:%S.%f",
            "%Y-%m-%d %H:%M:%S",
            "%Y-%m-%dT%H:%M:%S.%fZ",
            "%Y-%m-%dT%H:%M:%SZ",
        ]
        notice = cls(
            gracedb_id=notice_dict["superevent_id"],
            notice_type=GravWaveNoticeTypes[notice_dict["alert_type"]],
            notice_datetime=parse_datetime(notice_dict["time_created"], fmts),
        )
        if notice_dict.get("event"):
            notice.event_datetime = parse_datetime(notice_dict["event"]["time"], fmts)
            notice.false_alarm_rate = notice_dict["event"]["far"]
            notice.skymap_base64 = notice_dict["event"]["skymap"]
            notice_dict["event"]["skymap"] = None
        if notice_dict.get("external_coinc"):
            notice.external_coinc = notice_dict["external_coinc"]
        notice.full_notice = notice_dict
        return notice

    def copy_without_skymap(self):
        return GravWaveNotice(
            id=self.id,
            gracedb_id=self.gracedb_id,
            notice_type=self.notice_type,
            notice_datetime=self.notice_datetime,
            event_datetime=self.event_datetime,
            false_alarm_rate=self.false_alarm_rate,
            skymap_base64=None,
            external_coinc=deepcopy(self.external_coinc),
            full_notice=deepcopy(self.full_notice),
            version_id=self.version_id,
        )

    @property
    def skymap(self):
        if self._skymap is None:
            skymap_bytes = b64decode(self.skymap_base64)
            self._skymap = Table.read(BytesIO(skymap_bytes))
        return self._skymap

    @property
    def healpix_nested_indices(self):
        if self._healpix_nested_indices is None:
            level, ipix = ah.uniq_to_level_ipix(self.skymap["UNIQ"])
            index = ipix * (2 ** (self._max_level - level)) ** 2
            self._healpix_nested_indices = index
        return self._healpix_nested_indices

    @property
    def healpix_sorter(self):
        # sorted indices by nested healpix indices
        if self._healpix_sorter is None:
            self._healpix_sorter = np.argsort(self.healpix_nested_indices)
        return self._healpix_sorter

    @property
    def probability_density_sorter(self):
        # sorted indices by probability density
        if self._probability_density_sorter is None:
            self._probability_density_sorter = np.argsort(self.skymap["PROBDENSITY"])
        return self._probability_density_sorter

    @property
    def sorted_pixel_areas(self):
        # pixel areas using probdensity sort order
        if self._sorted_pixel_areas is None:
            level, _ = ah.uniq_to_level_ipix(
                self.skymap["UNIQ"][self.probability_density_sorter]
            )
            pixel_area = ah.nside_to_pixel_area(ah.level_to_nside(level))
            self._sorted_pixel_areas = pixel_area
        return self._sorted_pixel_areas

    @property
    def cumprob(self):
        # cumulative probability using low-to-high prob density sort order
        if self._cumprob is None:
            prob = (
                self.sorted_pixel_areas
                * self.skymap["PROBDENSITY"][self.probability_density_sorter]
            )
            self._cumprob = np.cumsum(prob)  # low to high density
        return self._cumprob

    def get_probability_density(self, location: SkyCoord) -> float:
        max_nside = ah.level_to_nside(self._max_level)
        match_ipix = ah.lonlat_to_healpix(
            location.ra, location.dec, max_nside, order="nested"
        )

        i = self.healpix_sorter[
            np.searchsorted(
                self.healpix_nested_indices,
                match_ipix,
                side="right",
                sorter=self.healpix_sorter,
            )
            - 1
        ]
        return self.skymap[i]["PROBDENSITY"]

    def get_probability_contour_level_and_area(self, location: SkyCoord):
        prob_density = self.get_probability_density(location)

        i = (
            np.searchsorted(
                self.skymap["PROBDENSITY"],
                prob_density,
                side="right",
                sorter=self.probability_density_sorter,
            )
            - 1  # include duplicates
        )
        contour_level = (1.0 - self.cumprob[i].value) * 100.0
        area = self.sorted_pixel_areas[i:].sum().to(u.deg**2)
        return contour_level, area
