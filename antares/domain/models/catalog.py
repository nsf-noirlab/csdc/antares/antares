from dataclasses import dataclass, field
from typing import Optional

from astropy.coordinates import Angle, SkyCoord


@dataclass(frozen=True)
class Catalog:
    id: str
    name: str


@dataclass(frozen=True)
class CatalogObject:
    id: str
    catalog_id: str
    catalog_name: str
    location: SkyCoord = field(hash=False)
    radius: Optional[Angle] = field(hash=False)
    properties: dict
    name: str

    def __post_init__(self):
        try:
            int(self.id)
        except ValueError:
            raise ValueError("id must be string representation of a base 10 integer")
        try:
            int(self.catalog_id)
        except ValueError:
            raise ValueError(
                "catalog_id must be string representation of a base 10 integer"
            )
