from dataclasses import dataclass


@dataclass
class Tag:
    name: str
    description: str
    filter_version_id: int
