import datetime
import uuid
from dataclasses import dataclass, field
from typing import Literal, Optional

from passlib.hash import pbkdf2_sha256


@dataclass
class User:
    """
    ANTARES user account.

    Parameters
    ----------
    user_id : int
    name : str
    username : str
    email : str
    password_hash : Optional[str]
        PBKDF2 SHA256 hash of a user's password, not required (defaults to None) but the
        user instance's `check_password` method will always return `False` if this value
        is not set.
    staff : bool (default, False)
        Should the user have staff permissions.
    admin : bool (default, False)
        Should the user have admin permissions.

    Notes
    -----
    The `field(default_factory=lambda: ...)` pattern that you will see for member fields
    with default values is necessary for integration with SQLAlchemy. It is unable to
    build object mappings to dataclasses with directly specified default values. This
    may be fixed in SQLAlchemy 1.4.
    """

    name: str
    username: str
    email: str
    id: Optional[uuid.UUID] = field(default_factory=lambda: None)
    password_hash: Optional[str] = field(default_factory=lambda: None)
    staff: bool = field(default_factory=lambda: False)
    admin: bool = field(default_factory=lambda: False)

    def set_password(self, password: str):
        self.password_hash = pbkdf2_sha256.hash(password)

    def check_password(self, password: str):
        if not self.password_hash:
            return False
        return pbkdf2_sha256.verify(password, self.password_hash)


@dataclass
class JwtRecord:
    token_type: Literal["refresh"]
    jti: str
    expires: datetime.datetime
    token_blocklist_entry_id: Optional[int] = field(default_factory=lambda: None)
