from dataclasses import dataclass
from typing import Union


@dataclass
class Blob:
    id: str
    data: Union[str, bytes]
