from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from typing import Literal, Optional, TypedDict

from astropy.coordinates import SkyCoord


class Survey(Enum):
    SURVEY_ZTF: str = "ztf"
    SURVEY_DECAT: str = "decat"

    @staticmethod
    def from_int(n: int):
        """
        Static constructor of a Survey enum member.

        Exists for backwards compatibility purposes with the `ant_survey` normalized
        alert property which maps many-to-one back to surveys. This method is useful,
        for example, to allow us to de/serialize a usable survey value from a database.
        """
        if (n == 1) or (n == 2):
            return Survey.SURVEY_ZTF
        elif n == 3:
            return Survey.SURVEY_DECAT
        else:
            raise ValueError(f"Unrecognized integer survey representation: {n}")


class AlertGravWaveEvent(TypedDict):
    gracedb_id: str
    contour_level: float
    contour_area: float


class AlertNormalizedProperties(TypedDict, total=False):
    """Some alerts can include ant_input_msg_time which is a duplicate of ant_time_received"""

    ant_ra: Optional[float]
    ant_dec: Optional[float]
    ant_mjd: float
    ant_time_received: int
    ant_mag: Optional[float]
    ant_magerr: Optional[float]
    ant_passband: str
    ant_maglim: float
    ant_survey: int


@dataclass(frozen=True)
class Alert:
    id: str
    location: Optional[SkyCoord]
    mjd: float
    survey: Survey
    normalized_properties: AlertNormalizedProperties
    properties: dict = field(default_factory=dict)
    non_persistent_properties: dict = field(default_factory=dict)
    grav_wave_events: list[AlertGravWaveEvent] = field(default_factory=list)
    created_at: datetime = field(default_factory=datetime.utcnow)

    def upsert_grav_wave_event(self, grav_wave_event: AlertGravWaveEvent):
        existing_gw = [
            event
            for event in self.grav_wave_events
            if event["gracedb_id"] == grav_wave_event["gracedb_id"]
        ]
        for gw_event in existing_gw:
            self.grav_wave_events.remove(gw_event)
        self.grav_wave_events.append(grav_wave_event)

    def update_non_persistent_properties(self, non_persistent_properties: dict):
        self.non_persistent_properties.update(non_persistent_properties)


class AlertThumbnailType(Enum):
    DIFFERENCE = "difference"
    SCIENCE = "science"
    TEMPLATE = "template"


@dataclass(frozen=True)
class AlertThumbnail:
    id: str
    src: str
    filemime: Literal["image/png"]
    filename: str
    thumbnail_type: AlertThumbnailType
