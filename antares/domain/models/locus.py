import uuid
from dataclasses import dataclass, field
from typing import Optional

from astropy.coordinates import SkyCoord
from astropy.timeseries import TimeSeries

from .catalog import CatalogObject
from .watch_list import WatchObject


@dataclass
class LocusAnnotation:
    owner_id: uuid.UUID
    locus_id: str
    id: Optional[int] = field(default_factory=lambda: None)
    comment: str = field(default_factory=lambda: "")
    favorite: bool = field(default_factory=lambda: False)

    def __post_init__(self):
        self._validate_comment(self.comment)

    def set_comment(self, value: str):
        self._validate_comment(value)
        self.comment = value

    def set_favorite(self, value: bool):
        self.favorite = value

    @staticmethod
    def _validate_comment(value: str):
        if len(value) > 255:
            raise ValueError("LocusAnnotation comment must be <= 255 characters")


@dataclass
class Locus:
    id: str
    location: SkyCoord
    catalogs: set[str] = field(default_factory=set)
    tags: set[str] = field(default_factory=set)
    grav_wave_events: list[str] = field(default_factory=list)
    watch_object_matches: set[tuple[uuid.UUID, uuid.UUID]] = field(default_factory=set)
    properties: dict = field(default_factory=dict, repr=False)
    lightcurve: TimeSeries = field(default_factory=TimeSeries, repr=False)

    def add_catalog_object_association(self, catalog_object: CatalogObject):
        self.catalogs.add(catalog_object.catalog_name)

    def add_watch_object_association(self, watch_object: WatchObject):
        self.watch_object_matches.add((watch_object.watch_list_id, watch_object.id))

    def add_grav_wave_event_association(self, gracedb_id: str):
        if gracedb_id not in self.grav_wave_events:
            self.grav_wave_events.append(gracedb_id)
