import datetime
import json

import bson
from sqlalchemy import Column, event, func
from sqlalchemy.dialects.mysql import (
    DATETIME,
    INTEGER,
    LONGBLOB,
    LONGTEXT,
    TEXT,
    TINYINT,
    VARCHAR,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property

from antares.external.sql.engine import session
from antares.utils.json_encoder import json_dumps

_Base = declarative_base()


utcnow = datetime.datetime.utcnow


class SBase(_Base):  # type: ignore # Sqlalchemy 2.0 fix this(see: https://docs.sqlalchemy.org/en/20/orm/extensions/mypy.html)
    __abstract__ = True

    @classmethod
    def get(cls, obj_id):
        with session() as s:
            return s.query(cls).get(obj_id)

    def as_dict(self):
        """
        Translate an object to a dict

        :return: dict
        """
        return {col: getattr(self, col) for col in self.__table__.columns.keys()}


class WrongPropertyType(Exception):
    pass


class ESMappingsAreImmutable(Exception):
    pass


class PropertyMixin:
    @classmethod
    def create_or_update(
        cls, name, type, origin, filter_version_id, description, es_mapping
    ):
        """
        Create or update a Property.

        Updates the filter_version_id and description only.

        Raises WrongPropertyType if the type does not match existing.

        :param name:
        :param type:
        :param origin:
        :param filter_version_id:
        :param description:
        :param es_mapping:
        :return: True if newly created, False if updated existing.
        """
        valid_types = {"int", "float", "str"}
        if type not in valid_types:
            raise ValueError(f"type '{type}' is not valid. Valid types = {valid_types}")
        if es_mapping is not None:
            es_mapping = json_dumps(es_mapping, indent=4, sort_keys=True)
        with session() as s:
            p = cls.get(name)
            if p:
                if p.type != type:
                    raise WrongPropertyType
                if p.es_mapping != es_mapping:
                    raise ESMappingsAreImmutable
                p.origin = origin
                p.filter_version_id = filter_version_id
                p.description = description
                s.commit()
                return False
            else:
                s.add(
                    cls(
                        name=name,
                        type=type,
                        origin=origin,
                        filter_version_id=filter_version_id,
                        description=description,
                        es_mapping=es_mapping,
                    )
                )
                s.commit()
                return True


class SAlertProperty(SBase, PropertyMixin):
    __tablename__ = "alert_property"
    name = Column(VARCHAR(100), primary_key=True)
    type = Column(VARCHAR(10))
    origin = Column(VARCHAR(100))
    filter_version_id = Column(INTEGER(unsigned=True))
    description = Column(TEXT)
    es_mapping = Column(TEXT)


class SLocusProperty(SBase, PropertyMixin):
    __tablename__ = "locus_property"
    name = Column(VARCHAR(100), primary_key=True)
    type = Column(VARCHAR(10))
    origin = Column(VARCHAR(100))
    filter_version_id = Column(INTEGER(unsigned=True))
    description = Column(TEXT)
    es_mapping = Column(TEXT)


class SFilter(SBase):
    __tablename__ = "filter"
    filter_id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    name = Column(VARCHAR(100), nullable=False, unique=True)
    created_at = Column(DATETIME, default=utcnow)
    disabled_at = Column(DATETIME)
    updated_at = Column(DATETIME)
    user_id = Column(INTEGER)
    description = Column(VARCHAR(2048))
    level = Column(INTEGER)
    priority = Column(INTEGER)
    latest_version_id = Column(INTEGER(unsigned=True))
    enabled_version_id = Column(INTEGER(unsigned=True))
    public = Column(TINYINT, nullable=False, default=False)

    @hybrid_property
    def enabled(self):
        return self.enabled_version_id is not None

    @classmethod
    def get_enabled(cls, level=None):
        """
        Return all enabled rows in order of priority.

        Rows with priority = NULL are sorted to the bottom.

        :param level: Optional `level` to filter on. May be int or the str 'NULL'.
        :return: list of PipelineStage
        """
        with session() as s:
            q = s.query(cls).filter(cls.enabled_version_id.isnot(None))
            if level == "NULL":
                q = q.filter_by(level=None)
            if isinstance(level, int):
                q = q.filter_by(level=level)
            return q.order_by(func.isnull(cls.priority).asc(), cls.priority.asc()).all()


class SFilterVersion(SBase):
    __tablename__ = "filter_version"
    filter_version_id = Column(
        INTEGER(unsigned=True), autoincrement=True, primary_key=True
    )
    filter_id = Column(INTEGER(unsigned=True))
    created_at = Column(DATETIME, default=utcnow)
    code = Column(TEXT)
    comment = Column(VARCHAR(255))
    validated_at = Column(DATETIME)
    enabled_at = Column(DATETIME)
    disabled_at = Column(DATETIME)
    disabled_log_id = Column(INTEGER(unsigned=True))
    public = Column(TINYINT, nullable=False, default=False)

    @classmethod
    def get_name(cls, filter_v_id):
        with session() as s:
            fv = s.query(cls).get(filter_v_id)
            f = s.query(SFilter).get(fv.filter_id)
            return f.name

    @classmethod
    def enable(cls, filter_v_id):
        """
        Enable a filter version and disable other versions.

        :param filter_v_id:
        :return:
        """
        with session() as s:
            # Get this version
            fv = s.query(cls).get(filter_v_id)
            if fv is None:
                raise KeyError("FilterVersion not found")

            # Mark this version as enabled
            fv.enabled_at = utcnow()
            fv.disabled_at = None

            # Mark other versions as DISABLED
            other_versions = (
                s.query(cls)
                .filter(
                    cls.filter_id == fv.filter_id, cls.filter_version_id != filter_v_id
                )
                .all()
            )
            for other_fv in other_versions:
                if other_fv.enabled_at:
                    other_fv.enabled_at = None
                    other_fv.disabled_at = utcnow()

            # Mark this as currently enabled version of SFilter
            f = s.query(SFilter).get(fv.filter_id)
            f.enabled_version_id = filter_v_id

            s.commit()

    @classmethod
    def disable(cls, filter_v_id, log_id=None):
        with session() as s:
            fv = s.query(cls).get(filter_v_id)
            if log_id:
                fv.disabled_log_id = log_id
            fv.enabled_at = None
            fv.disabled_at = utcnow()
            f = s.query(SFilter).get(fv.filter_id)
            f.enabled_version_id = None
            f.disabled_at = fv.disabled_at
            s.commit()


@event.listens_for(SFilterVersion, "after_insert")
def update_latest_filter_version(mapper, connection, target):
    q = (
        SFilter.__table__.update()
        .where(SFilter.filter_id == target.filter_id)
        .values(
            latest_version_id=target.filter_version_id, updated_at=target.created_at
        )
    )
    connection.execute(q)
    return


class SFilterCrashLog(SBase):
    __tablename__ = "filter_crash_log"
    log_id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    created_at = Column(DATETIME, default=utcnow)
    filter_id = Column(INTEGER(unsigned=True))
    filter_version_id = Column(INTEGER(unsigned=True))
    locus_id = Column(VARCHAR(100))
    alert_id = Column(VARCHAR(100))
    stacktrace = Column(LONGTEXT)
    locus = Column(LONGTEXT)

    @classmethod
    def save(
        cls, filter_id, filter_version_id, locus_id, alert_id, stacktrace, locus_dict
    ):
        lg = cls(
            filter_id=filter_id,
            filter_version_id=filter_version_id,
            locus_id=locus_id,
            alert_id=alert_id,
            stacktrace=stacktrace,
            locus=json_dumps(locus_dict, indent=4),
        )
        with session() as s:
            s.add(lg)
            s.commit()
            if lg.log_id is None:
                raise ValueError(
                    f"log_id is None when storing a filter crash log for filter_id={filter_id}"
                )
            return lg.log_id

    @classmethod
    def get(cls, log_id):
        with session() as s:
            lg = s.query(cls).get(log_id)
            if not lg:
                raise ValueError(f"SFilterCrashLog {log_id} not found")
            d = lg.as_dict()
            d["locus"] = json.loads(d["locus"])
            return d


class SStorage(SBase):
    __tablename__ = "storage"
    storage_key = Column(VARCHAR(255), primary_key=True)
    created_at = Column(DATETIME, default=utcnow)
    data = Column(LONGBLOB)

    @classmethod
    def put(cls, storage_key, data):
        """
        Save a blob to storage.

        :param storage_key: Storage.storage_key value
        :param data: string or bytes object
        """
        storage = cls(storage_key=storage_key, data=data)
        with session() as s:
            s.add(storage)
            s.commit()
            if storage.storage_key is None:
                raise ValueError(
                    f"stored object key is None when saving a blob with key {storage_key}"
                )
            return storage.storage_key

    @classmethod
    def put_bson(cls, key, obj):
        return cls.put(key, bson.dumps(obj))

    @classmethod
    def put_file(cls, key, fpath):
        with open(fpath, "rb") as f:
            return cls.put(key, f.read())

    @classmethod
    def get_data(cls, storage_key, require=False):
        """
        Get Storage by key.

        :param storage_key: Storage.storage_key value
        :return: bytes or None
        """
        with session() as s:
            st = s.query(cls).get(storage_key)
            if st:
                return st.data
            if require:
                raise KeyError(f'SStorage not found "{storage_key}"')

    @classmethod
    def get_bson(cls, key):
        data = cls.get_data(key)
        if data:
            return bson.loads(data)

    @classmethod
    def update(cls, storage_key, data):
        with session() as s:
            storage = s.query(cls).get(storage_key)
            if storage:
                storage.data = data
                s.commit()
                return storage.storage_key
            else:
                return SStorage.put(storage_key, data)

    @classmethod
    def update_bson(cls, storage_key, obj):
        cls.update(storage_key, bson.dumps(obj))

    @classmethod
    def keys(cls, prefix=None, like=None):
        if bool(prefix) + bool(like) >= 2:
            raise ValueError(
                "Keys can't have prefix and like arguments at the same time"
            )
        if prefix:
            like = prefix + "%"
        if not like:
            like = "%"
        with session() as s:
            rows = s.query(cls.storage_key).filter(cls.storage_key.like(like)).all()
            return [r[0] for r in rows]

    @classmethod
    def delete(cls, key):
        with session() as s:
            s.query(cls.storage_key).filter(cls.storage_key == key).delete(
                synchronize_session=False
            )
            s.commit()
