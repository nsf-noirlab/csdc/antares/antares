from dataclasses import dataclass
from typing import TypedDict, Union

from astropy.coordinates import SkyCoord

CatalogName = str
CatalogObjects = dict[CatalogName, list[dict]]


class AlertDict(TypedDict):
    """Dictionary representation of an ANTARES alert."""

    alert_id: str
    locus_id: str
    mjd: float
    properties: dict[str, Union[float, str, int]]


class LocusDict(TypedDict):
    """Dictionary representation of an ANTARES locus."""

    locus_id: str
    ra: float
    dec: float
    properties: dict
    tags: set[str]
    watch_list_ids: set[int]
    watch_object_ids: set[int]
    catalog_objects: CatalogObjects
    alerts: list[AlertDict]


@dataclass(frozen=True)
class Alert:
    """The base ANTARES alert object."""

    alert_id: str
    locus_id: str
    mjd: float
    properties: dict[str, Union[float, str, int]]

    def to_dict(self) -> AlertDict:
        """Returns a dict representation of this alert."""
        return {
            "alert_id": self.alert_id,
            "locus_id": self.locus_id,
            "mjd": self.mjd,
            "properties": self.properties,
        }


class Locus:
    """
    The base ANTARES locus object.

    A `Locus` represents a particular location in the sky (at an ra, dec) and is
    associated with one or more `Alert` objects. The name "alert" is used consistent
    with its meaning to the astronomical community (and is easy to confuse with "alert"
    as may be used in the context of event-handling software). To the astronomical
    community an alert is synonymous with an observation of an object.
    """

    def __init__(
        self,
        locus_id: str,
        location: SkyCoord,
        properties: dict,
        tags: set[str],
        catalog_objects: CatalogObjects,
        watch_object_ids: set[int],
        watch_list_ids: set[int],
        alerts: list[Alert],
    ):
        self.locus_id = locus_id
        self.location = location
        self.properties = properties
        self.tags = tags
        self.catalog_objects = catalog_objects
        self.watch_object_ids = watch_object_ids
        self.watch_list_ids = watch_list_ids
        self.alerts = alerts

    def has_been_replaced(self):
        return bool(self.properties.get("replaced_by", False))

    @property
    def alert(self) -> Alert:
        return self.alerts[-1]

    @property
    def ra(self) -> float:
        return self.location.ra

    @property
    def dec(self) -> float:
        return self.location.dec

    def to_dict(self) -> LocusDict:
        """
        Return a JSON-ready dict representation of the Locus.

        :return: JSON-ready dict
        """
        return {
            "locus_id": self.locus_id,
            "ra": self.ra,
            "dec": self.dec,
            "properties": self.properties,
            "tags": self.tags,
            "watch_list_ids": self.watch_list_ids,
            "watch_object_ids": self.watch_object_ids,
            "catalog_objects": self.catalog_objects,
            "alerts": [alert.to_dict() for alert in self.alerts],
        }
