import logging

from antares.bootstrap import bootstrap
from antares.domain.models import FilterRevisionStatus


def main():
    container = bootstrap()
    filter_repository = container.filter_repository()
    filter_revision_repository = container.filter_revision_repository()
    logging.info("----------------------------------")
    logging.info("Filter revision statuses migration")
    logging.info("----------------------------------")
    logging.info("Starting migration...")

    enabled_filters = []
    for filter_ in filter_repository.list():
        if filter_.enabled:
            enabled_filters.append(filter_)
    logging.info("There are %d enabled filters", len(enabled_filters))
    logging.info("Attempting to migrate their filter revision status to ENABLED...")

    unsuccessful_filter_revisions = []
    for filter_ in enabled_filters:
        logging.info(
            ("* Filter revision ID %s " "(Filter ID %s)"),
            filter_.enabled_filter_revision_id,
            filter_.id,
        )
        filter_revision = filter_revision_repository.get(
            filter_.enabled_filter_revision_id
        )
        if filter_revision.enabled.is_active:
            logging.info("* Already ENABLED")
            continue
        try:
            # We manually call _disable side effect to be able to enable it
            # Filter updates will not be persisted, so this is just to skip constraints
            filter_._disable()
            filter_revision.enable(filter_=filter_)
            filter_revision_repository.update(filter_revision)
            logging.info("* CHECK")
        except Exception as err:
            logging.warning(
                f"* Filter revision ID {filter_revision.id} couldn't be enabled "
                "normally for the following reason:"
            )
            logging.warning("* %s", err)
            logging.info(
                "* Attempting to manually set filter revision status as REVIEWED "
                "and then to enable it"
            )
            # The following MUST NOT be done normally
            filter_revision.status = FilterRevisionStatus.REVIEWED
            try:
                filter_revision.enable(filter_=filter_)
                filter_revision_repository.update(filter_revision)
                logging.info("* CHECK")
            except Exception as err:
                unsuccessful_filter_revisions.append(
                    {"filter_revision_id": filter_revision.id, "error": err}
                )

    logging.info("Finished migration!")
    successful_filter_revisions_len = len(enabled_filters) - len(
        unsuccessful_filter_revisions
    )
    logging.info(
        "%d out of %d filter revisions successfully migrated",
        successful_filter_revisions_len,
        len(enabled_filters),
    )

    if unsuccessful_filter_revisions:
        logging.warning("Filter revisions not migrated:")
        logging.warning(unsuccessful_filter_revisions)


if __name__ == "__main__":
    main()
