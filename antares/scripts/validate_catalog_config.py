import logging
import socket

from antares.bootstrap import bootstrap


def main():
    errors = []
    logging.info("Local ip is %s" % get_ip())

    # Create a temporary directory and clone the HELM values.yaml from ANTARES
    logging.info("Setting up working directory")

    logging.info("Validating catalog configuration")

    container = bootstrap()
    catalog_object_repository = container.catalog_object_repository()
    for catalog in catalog_object_repository.list_catalogs():
        catalog_object_iter = catalog_object_repository.list_by_catalog_id(catalog.id)
        try:
            if not next(catalog_object_iter):
                raise ValueError("Empty catalog object")
        except Exception as e:
            errors.append((catalog, e))

    # Report results
    if len(errors) > 0:
        print("ERROR")
        raise ValueError(
            f"Invalid configurations: {[(catalog.name, e) for (catalog, e) in errors]}"
        )
    else:
        print("SUCCESS!")


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(("10.255.255.255", 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = "127.0.0.1"
    finally:
        s.close()
    return ip


if __name__ == "__main__":
    main()
