/* eslint-disable import/prefer-default-export */
import { GetterTree } from 'vuex';
import { AnnouncementState } from './types';
import { RootState } from '../types';

export const getters: GetterTree<AnnouncementState, RootState> = {
  announcements(state) {
    return state.announcements;
  },
};
