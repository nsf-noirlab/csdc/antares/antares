/* eslint-disable import/prefer-default-export */
import { MutationTree } from 'vuex';
import { Announcement } from '@/common/types';
import { AnnouncementState } from './types';

import { SET_ANNOUNCEMENTS } from '../mutations';

export const mutations: MutationTree<AnnouncementState> = {
  [SET_ANNOUNCEMENTS](state, announcements: Announcement[]) {
    state.announcements = announcements;
  },
};
