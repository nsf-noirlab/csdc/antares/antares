import { Announcement } from '@/common/types';

export interface AnnouncementState {
  announcements: Announcement[];
}
