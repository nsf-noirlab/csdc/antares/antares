import { Module } from 'vuex';

import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { AnnouncementState } from './types';
import { RootState } from '../types';

export const state: AnnouncementState = {
  announcements: [],
};

const namespaced = false;

export const announcement: Module<AnnouncementState, RootState> = {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
