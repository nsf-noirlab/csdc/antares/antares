/* eslint-disable import/prefer-default-export */

import { ActionTree } from 'vuex';

import { AnnouncementService } from '@/api';
import { AnnouncementState } from './types';
import { RootState } from '../types';
import { FETCH_ANNOUNCEMENTS } from '../actions';
import { SET_ANNOUNCEMENTS } from '../mutations';

export const actions: ActionTree<AnnouncementState, RootState> = {
  async [FETCH_ANNOUNCEMENTS](context) {
    const { data: announcements } = await AnnouncementService.list({ filter: [{ field: 'active', op: 'eq', value: true }] });
    context.commit(SET_ANNOUNCEMENTS, announcements);
  },
};
