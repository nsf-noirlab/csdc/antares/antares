import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import _ from 'lodash';
import { RootState } from './types';
import { announcement } from './announcement';
import { auth } from './auth';
import { locus } from './locus';
import { user } from './user';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence<RootState>({
  storage: window.localStorage,
  modules: ['auth', 'user'],
});

const storeConfig = {
  state: {
    version: '0.1.0',
  },
  modules: {
    announcement,
    auth,
    locus,
    user,
  },
  plugins: [vuexLocal.plugin],
};

/**
 * Factory function for creating a Vuex store instance
 * @param initialConfig - Vuex store config object that can override default one partial or totally
 */
export function createStore(initialConfig: Record<string, unknown> = {}) {
  return new Vuex.Store<RootState>({
    ..._.cloneDeep(storeConfig),
    ...initialConfig,
  });
}

const store = createStore();

export default store;
