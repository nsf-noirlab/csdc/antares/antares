import { User, UserData } from '@/common/types';

export interface UserState {
  user: User | null;
  userData: UserData;
}
