/* eslint-disable import/prefer-default-export */
import { GetterTree } from 'vuex';
import { UserState } from './types';
import { RootState } from '../types';

export const getters: GetterTree<UserState, RootState> = {
  canPerformActionsOnFilter(state, restGetters) {
    const { user } = state;
    return (ownerId: string) => (Boolean(user) && user?.id === ownerId) || restGetters.isUserAdmin;
  },
  isUserAdmin(state) {
    return Boolean(state.user) && state.user?.admin;
  },
  user(state) {
    return state.user;
  },
  userData(state) {
    return state.userData;
  },
};
