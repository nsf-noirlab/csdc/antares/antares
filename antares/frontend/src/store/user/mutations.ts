/* eslint-disable import/prefer-default-export */
import { MutationTree } from 'vuex';
import { User, UserData } from '@/common/types';
import { UserState } from './types';

import {
  PURGE_USER, PURGE_USER_DATA, SET_USER, SET_USER_DATA,
} from '../mutations';

export const mutations: MutationTree<UserState> = {
  [SET_USER](state, user: User) {
    state.user = user;
  },
  [PURGE_USER](state) {
    state.user = null;
  },
  [SET_USER_DATA](state, userData: UserData) {
    state.userData = userData;
  },
  [PURGE_USER_DATA](state) {
    state.userData = {
      locusAnnotations: {},
    };
  },
};
