/* eslint-disable import/prefer-default-export, vars-on-top, no-var,
  no-throw-literal, block-scoped-var
*/
import axios from 'axios';
import { ActionTree } from 'vuex';

import { AuthService, UserService } from '@/api';
import { AuthState } from './types';
import { RootState } from '../types';
import {
  CHECK_AUTH, FETCH_USER_DATA, LOGIN, LOGOUT, REGISTER,
} from '../actions';
import {
  PURGE_AUTH, PURGE_USER, PURGE_USER_DATA, SET_AUTH,
} from '../mutations';

export const actions: ActionTree<AuthState, RootState> = {
  async [LOGIN](context, credentials) {
    try {
      var { data: auth } = await AuthService.login(credentials);
    } catch (error) {
      if (error.response.status === 401) {
        throw 'Incorrect username or password. Please try again.';
      }
      throw 'Server error.';
    }
    context.commit(SET_AUTH);
    try {
      await context.dispatch(FETCH_USER_DATA, { identity: auth.identity });
    } catch (error) {
      const { data: config } = await axios.get('/config.json');
      await context.dispatch(LOGOUT);
      throw `The server encountered an error. Please try again and if the problem persists email us at ${config.ANTARES_SUPPORT_EMAIL}.`;
    }
  },
  async [LOGOUT](context) {
    try {
      await AuthService.logout();
    } finally {
      context.commit(PURGE_AUTH);
      context.commit(PURGE_USER);
      context.commit(PURGE_USER_DATA);
    }
  },
  async [REGISTER](context, { user }) {
    return UserService.create(user);
  },
  async [CHECK_AUTH](): Promise<boolean> {
    try {
      await AuthService.checkAuth();
      return true;
    } catch {
      await AuthService.logout();
      return false;
    }
  },
};
