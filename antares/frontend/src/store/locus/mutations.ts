/* eslint-disable import/prefer-default-export */
import { MutationTree } from 'vuex';
import { LociStatistics, Locus, RequestQuery } from '@/common/types';
import { LocusState } from './types';

import {
  SET_LOCI,
  SET_LOCI_STATISTICS,
  SET_LOCI_COUNT,
  SET_LOCI_QUERY,
  SET_LOCUS,
} from '../mutations';

export const mutations: MutationTree<LocusState> = {
  [SET_LOCUS](state, locus: Locus) {
    state.locus = locus;
  },
  [SET_LOCI](state, loci: Locus[]) {
    state.loci = loci;
  },
  [SET_LOCI_STATISTICS](state, statistics: LociStatistics) {
    state.lociStatistics = statistics;
  },
  [SET_LOCI_COUNT](state, count: number) {
    state.lociCount = count;
  },
  [SET_LOCI_QUERY](state, query: RequestQuery) {
    state.lociQuery = query;
  },
};
