/* eslint-disable import/prefer-default-export */
import { ActionTree } from 'vuex';
import { LocusService } from '@/api';
import { LocusState } from './types';
import { RootState } from '../types';
import { FETCH_LOCI, FETCH_LOCUS, FETCH_LOCI_STATISTICS } from '../actions';
import {
  SET_LOCI,
  SET_LOCI_STATISTICS,
  SET_LOCI_COUNT,
  SET_LOCI_QUERY,
  SET_LOCUS,
} from '../mutations';

export const actions: ActionTree<LocusState, RootState> = {
  async [FETCH_LOCUS]({ commit }, { locusId }) {
    const { data } = await LocusService.get(locusId);
    commit(SET_LOCUS, data);
  },
  async [FETCH_LOCI]({ commit }, { query = null }) {
    const { data, meta } = await LocusService.list(query);
    commit(SET_LOCI_QUERY, query);
    commit(SET_LOCI, data);
    commit(SET_LOCI_COUNT, meta.count);
  },
  async [FETCH_LOCI_STATISTICS]({ commit }, { query = null }) {
    const { data } = await LocusService.statistics(query);
    commit(SET_LOCI_STATISTICS, data);
  },
};
