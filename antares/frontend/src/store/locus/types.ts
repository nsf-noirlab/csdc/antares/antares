import {
  LociStatistics, Locus, LocusListing, RequestQuery,
} from '@/common/types';

export interface LocusState {
  locus: Locus | null;
  loci: LocusListing | null;
  lociStatistics: LociStatistics | null;
  lociCount: number;
  lociQuery: Partial<RequestQuery> | null;
}
