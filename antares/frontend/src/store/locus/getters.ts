/* eslint-disable import/prefer-default-export */
import { GetterTree } from 'vuex';
import { LocusState } from './types';
import { RootState } from '../types';

export const getters: GetterTree<LocusState, RootState> = {
  locus(state) {
    return state.locus;
  },
  loci(state) {
    return state.loci;
  },
  lociStatistics(state) {
    return state.lociStatistics;
  },
  lociCount(state) {
    return state.lociCount;
  },
  lociQuery(state) {
    return state.lociQuery;
  },
};
