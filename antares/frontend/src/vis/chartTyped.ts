/* eslint-disable no-plusplus, no-shadow, no-restricted-syntax, func-names */
import Vuex from 'vuex';
import Vue from 'vue';
import * as d3 from 'd3';

/// ///////////////////////////////////////////////////////////////////////////
// managing /loci/:locus: visualization state with Vuex stores

// we use VueX store for most the state being managed, but
// the brush state and the associated callbacks are called potentially at 60 FPS;
//
// rebuilding the state at that rate probably generates too much garbage,
// so we'll use a separate callback mechanism for that.

// setField assumes things are all objects inside, no reactive arrays

const setField = (obj: any, fields: any, value: any) => {
  let innerObj = obj;
  let field;
  for (let i = 0; i < fields.length - 1; ++i) {
    field = fields[i];
    if (innerObj[field] === undefined) {
      Vue.set(innerObj, field, {});
    }
    innerObj = innerObj[field];
  }
  field = fields[fields.length - 1];
  Vue.set(innerObj, field, value);
};

const brushWatches: any = {};
export function watchBrush(group: string, k: any) {
  if (brushWatches[group] === undefined) {
    brushWatches[group] = [];
  }
  brushWatches[group].push(k);
}

export function triggerRedraw(group: string) {
  if (!brushWatches[group]) {
    return;
  }
  brushWatches[group].forEach(((k: any) => k()));
}

const createChartGroup = (state: any, name: string) => {
  if (state.chartGroups[name]) {
    return;
  }
  Vue.set(state.chartGroups, name, {
    // we need to use data from two sources:
    // - 'locus' has maglims,
    // - 'alerts' has rcids
    // we need to show both, hence we need to manage both.
    locusData: [],
    alertsData: [],
    annotations: {},
    predicates: {}, // a map from chart names to functions to filter points
  });
};

const provideAlertsData = (state: any, payload: any) => {
  const { group, data } = payload;
  createChartGroup(state, group);
  setField(state.chartGroups, [group, 'alertsData'], data);
};

const provideLocusData = (state: any, payload: any) => {
  const { group, data } = payload;
  createChartGroup(state, group);
  setField(state.chartGroups, [group, 'locusData'], data);
};
const annotate = (state: any, payload: any) => {
  const { group, key, annotation } = payload;
  createChartGroup(state, group);
  setField(state.chartGroups, [group, 'annotations', key], annotation);
};
const predicate = (state: any, payload: any) => {
  const { group, chartName, predicate } = payload;
  createChartGroup(state, group);
  setField(state.chartGroups, [group, 'predicates', chartName], predicate);
};

export const chartStore : any = new Vuex.Store({
  state: {
    chartGroups: {},
  },
  mutations: {
    createChartGroup,
    provideAlertsData,
    provideLocusData,
    annotate,
    predicate,
  },
});

export function getSelectionPredicate(groupName: string, chartName: string) {
  const group: any = chartStore.state.chartGroups[groupName];
  if (group === undefined) {
    return () => true;
  }

  const fns: any[] = [];
  for (const key in group.predicates) {
    if (key !== chartName && group.predicates[key]) {
      fns.push(group.predicates[key]);
    }
  }
  return function (element: any): boolean {
    for (let i = 0; i < fns.length; ++i) {
      if (!fns[i](element)) return false;
    }
    return true;
  };
}

/// /////////////////////////////////////////////////////////////////////////////
// d3 util functions

const mjdEpoch = new Date(1858, 10, 17);
export function mjdToCal(mjdDays: number): Date {
  return new Date(mjdEpoch.getTime() + mjdDays * (1000 * 60 * 60 * 24));
}

export function passbandScale() {
  return d3.scaleOrdinal()
    .domain(['g', 'R'])
    .range([d3.lab(50, -60, -10), d3.lab(40, 60, 30)]);
}

export function rcidScale() {
  return d3.scaleOrdinal()
    .range(d3.schemeCategory10);
}

export function alertTypeMarkScale() {
  return function (t: string): string {
    switch (t) {
      case 'ulim':
      case 'upper_limit': return 'M -1 -1 L 1 -1 L 0  1 L -1 -1 Z';

      case 'llim':
      case 'lower_limit': return 'M -1  1 L 1  1 L 0 -1 L -1  1 Z';

      case 'ERROR_OTHER': return 'M -2 -2 L 2 2 M -2 2 L 2 -2';

      case 'mag':
      case 'raw':
      case 'calibrated': return 'M -2 0 A 2 2 0 0 0 2 0 A 2 2 0 0 0 -2 0 Z';

      default:
        return 'M -2 -2 L 2 2 M -2 2 L 2 -2';
    }
  };
}

/// ///////////////////////////////////////////////////////////////////////////
// normalized access to alert properties

export const alertTypes = [
  'mag', 'ulim', 'llim', 'ERROR_OTHER',
];
