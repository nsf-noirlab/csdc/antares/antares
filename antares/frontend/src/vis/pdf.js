import './register-files';
import PDFDocument from 'pdfkit';
import svgToPDF from 'svg-to-pdfkit';
import blobStream from 'blob-stream';
import * as d3 from 'd3';

// 2021-04-27: Carlos spent three hours trying to make this work with custom fonts.
//   Ideally, we'd make it work with Raleway, but pdfkit keeps complaining
//   that the font format is not supported. I suspect I'm doing something wrong
//   because it also complains about Roboto, one of the fonts in the pdfkit docs.
//   It's probably some new brfs nonsense.

export function createPDFReport() {
  const lightCurve = d3.select('#d3-light-curve-chart').selectAll('svg').node();
  const raDec = d3.select('#d3-ra-dec-chart').selectAll('svg').node();

  const doc = new PDFDocument();
  const bs = blobStream();

  const summaryBox = d3.select('ul.locus-property-list');
  const alertName = summaryBox.select('h4').text().trim();
  const props = summaryBox.selectAll('li > span');

  // yikes
  const tags = d3.select(props.nodes()[2])
    .selectAll('a').nodes()
    .map((x) => x.innerText.trim())
    .join(', ');

  doc.fontSize(18);
  doc.text(`Locus: ${alertName}`);
  doc.text(window.location.href);
  doc.fontSize(12);
  doc.text(`Num. Alerts: ${props.nodes()[0].innerText}`);
  doc.text(`Num. Mag Values: ${props.nodes()[1].innerText}`);
  doc.text(`Associated Tags: ${tags}`);

  svgToPDF(doc, lightCurve, 72 * 0.25, 72 * 2.25, { useCSS: true });
  svgToPDF(doc, raDec, 72 * 0.25, 72 * 7.75, { useCSS: true });

  // What we _should_ be doing is a general DOM walker that prints things to a PDFDocument.
  // What we _are_ doing is extracting the summary data in a fairly ad hoc way.

  const stream = doc.pipe(bs);
  doc.end();
  stream.on('finish', () => {
    const blobUrl = stream.toBlobURL('application/pdf');
    window.open(blobUrl);
  });
}

export function exportElement(el) {
  const doc = new PDFDocument();
  svgToPDF(doc, el, 0, 0, { useCSS: true });
  const bs = blobStream();
  const stream = doc.pipe(bs);
  doc.end();
  stream.on('finish', () => {
    const blobUrl = stream.toBlobURL('application/pdf');
    window.open(blobUrl);
  });
}
