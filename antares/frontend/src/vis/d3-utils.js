/* eslint-disable no-proto, new-cap */
import * as d3 from 'd3';

window.d3 = d3;

/// ///////////////////////////////////////////////////////////////////////////

export function extendScaleDomain(scale, amount) {
  let [min, max] = scale.domain();

  // we do this conversion generically to support time scales

  // first, remember constructors
  const ctor = min.__proto__.constructor;

  // convert to Numbers
  min = Number(min);
  max = Number(max);

  const midPoint = (min + max) / 2;
  const radius = Math.abs(midPoint - min);

  // here, we convert back to the original type
  scale.domain([new ctor(midPoint - radius * amount),
    new ctor(midPoint + radius * amount)]);
}

/*
 wideExtent is a funciton similar to d3.extent
 d3.extent expects a single number for each value (so getter
 is any -> Number). wideExtent expects (any -> [Number, Number]), so that
 objects with "wide extents" can be accomodated easily.

 Equivalently, wideExtent returns the tightest extent that covers all
 given extents.
 */
export function wideExtent(lst, getter) {
  // we encode an empty extent the same way as d3.extent
  const result = [undefined, undefined];

  lst.forEach((n) => {
    const [min, max] = getter(n);
    if (min !== undefined && max !== undefined) {
      if (result[0] === undefined) {
        result[0] = min;
      } else {
        result[0] = Math.min(min, result[0]);
      }
      if (result[1] === undefined) {
        result[1] = max;
      } else {
        result[1] = Math.max(max, result[1]);
      }
    }
  });
  return result;
}
