import VueRouter, { Route, NavigationGuardNext, RouterOptions } from 'vue-router';
import { Store } from 'vuex';
import defaultStore from '@/store';
import { FETCH_ANNOUNCEMENTS, CHECK_AUTH } from '@/store/actions';
import { RootState } from '@/store/types';
import AdminFilterList from '@/views/Admin/FilterList.vue';
import AnnouncementCreate from '@/views/Admin/AnnouncementCreate.vue';
import AnnouncementList from '@/views/Admin/AnnouncementList.vue';
import FavoriteLoci from '@/views/FavoriteLoci.vue';
import FilterCreate from '@/views/FilterCreate.vue';
import FilterDetail from '@/views/FilterDetail.vue';
import FilterList from '@/views/FilterList.vue';
import LocusDetail from '@/views/LocusDetail.vue';
import LocusList from '@/views/LocusList.vue';
import Pipeline from '@/views/Pipeline.vue';
import Login from '@/views/Login.vue';
import ForgotPassword from '@/views/ForgotPassword.vue';
import Register from '@/views/Register.vue';
import UserManagement from '@/views/Admin/UserManagement.vue';
import UserSettings from '@/views/UserSettings.vue';
import Properties from '@/views/Properties.vue';
import WatchListCreate from '@/views/WatchListCreate.vue';
import WatchListDetail from '@/views/WatchListDetail.vue';
import WatchListList from '@/views/WatchListList.vue';
import TagList from '@/views/TagList.vue';
import CatalogSampleList from '@/views/CatalogSampleList.vue';
import FAQ from '@/views/FAQ.vue';
import Support from '@/views/Support.vue';
import TeamDetail from '@/views/TeamDetail.vue';

/**
 * The checkAuth route guard checks that a user's login session
 * is still valid before directing them to the requested page.
 * If they are not logged in, they will be allowed to continue.
 * If they are logged in but their session has expired, they will
 * be redirected to the login page.
 *
 * This guard differs from requireAuth in that if the user is not
 * logged in, in requireAuth, they will be redirected to the login
 * page and MUST login before being allowed to continue.
 *
 * checkAuth is useful for a page like the "Filters" page, where we
 * want to display some sort of "sign up or login" banner to folks
 * that were not logged in but probably don't want to do the same
 * for expired sessions.
 */
function checkAuth(store: Store<RootState>) {
  return (to: Route, from: Route, next: NavigationGuardNext) => {
    if (store.getters.isAuthenticated) {
      store.dispatch(CHECK_AUTH).then((authorized) => {
        if (!authorized) {
          next({ name: 'login', query: { reason: 'inactivity', redirect: to.fullPath } });
        } else {
          next();
        }
      });
    } else {
      next();
    }
  };
}

/**
 * The requireAuth route guard requires that the user be logged in to
 * access the requested page. If they are not logged in, they
 * will be redirected to the login page. If they are logged in
 * but their session has expired, they will be redirected to the
 * login page.
 *
 * This guard differs from checkAuth in that if the user is not
 * logged in, in checkAuth, they will be allowed to continue to
 * the requested route.
 */
function requireAuth(store: Store<RootState>) {
  return (to: Route, from: Route, next: NavigationGuardNext) => {
    if (!store.getters.isAuthenticated) {
      next({ name: 'login', query: { redirect: to.fullPath } });
    } else {
      store.dispatch(CHECK_AUTH).then((authorized) => {
        if (!authorized) {
          next({ name: 'login', query: { reason: 'inactivity', redirect: to.fullPath } });
        } else {
          next();
        }
      });
    }
  };
}

/**
 * The requireAdmin route guard requires that the user be logged in to
 * access the requested page but also that the user is admin.
 * If they are not logged in, they will be redirected to the login page.
 * After logging in, if the user is not admin, they will be redirected
 * to the home page.
 *
 * This guard performs almost the same actions as requireAuth, except
 * that it also checks the isUserAdmin getter. Since guards are required
 * to use one next call per logical path, the code for requireAuth was
 * duplicated here.
 */
function requireAdmin(store: Store<RootState>) {
  return (to: Route, from: Route, next: NavigationGuardNext) => {
    if (!store.getters.isAuthenticated) {
      next({ name: 'login', query: { redirect: to.fullPath } });
    } else if (!store.getters.isUserAdmin) {
      next({ name: 'home' });
    } else {
      store.dispatch(CHECK_AUTH).then((authorized) => {
        if (!authorized) {
          next({ name: 'login', query: { reason: 'inactivity', redirect: to.fullPath } });
        } else {
          next();
        }
      });
    }
  };
}

/**
 * Factory function for creating a router instance
 * @param options - Custom options passed to router
 * @param store - Vuex store instance as the app router depends on a store for a number of tasks.
 *     Defaults to the app store singleton
 */
export default function createRouter(
  options: RouterOptions = {}, store?: Store<RootState>,
): VueRouter {
  const { base, mode } = options;
  const routerStore = store || defaultStore;

  // TODO: remove per-route guards calls to checkAuth, requireAuth and requireAdmin
  // Instead, use meta properties + global beforeEach guards, so routes array is
  // more independent and it also avoids the routerStore dependency on each route
  const routes = [
    {
      path: '/',
      name: 'home',
      beforeEnter: (to: Route, from: Route, next: NavigationGuardNext) => next({ name: 'locusList' }),
    },
    {
      path: '/favorite-loci',
      name: 'favoriteLoci',
      component: FavoriteLoci,
      beforeEnter: checkAuth(routerStore),
    },
    {
      path: '/filters',
      name: 'filterList',
      component: FilterList,
      beforeEnter: checkAuth(routerStore),
    },
    {
      path: '/filters/create',
      name: 'filterCreate',
      component: FilterCreate,
      beforeEnter: requireAuth(routerStore),
    },
    {
      path: '/filters/:filterId',
      name: 'filterDetail',
      component: FilterDetail,
      beforeEnter: requireAuth(routerStore),
    },
    {
      path: '/loci/:locusId',
      name: 'locusDetail',
      component: LocusDetail,
    },
    {
      path: '/loci',
      name: 'locusList',
      component: LocusList,
    },
    {
      path: '/pipeline',
      name: 'pipeline',
      component: Pipeline,
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/forgot-password',
      name: 'forgotPassword',
      component: ForgotPassword,
    },
    {
      path: '/properties',
      name: 'properties',
      component: Properties,
    },
    {
      path: '/watch-lists',
      name: 'watchListList',
      component: WatchListList,
      beforeEnter: checkAuth(routerStore),
    },
    {
      path: '/watch-lists/create',
      name: 'watchListCreate',
      component: WatchListCreate,
      beforeEnter: requireAuth(routerStore),
    },
    {
      path: '/watch-lists/:watchListId',
      name: 'watchListDetail',
      component: WatchListDetail,
      beforeEnter: requireAuth(routerStore),
    },
    {
      path: '/user/settings',
      name: 'userSettings',
      component: UserSettings,
      beforeEnter: requireAuth(routerStore),
    },
    {
      path: '/admin/filters',
      name: 'adminFilterList',
      component: AdminFilterList,
      beforeEnter: requireAdmin(routerStore),
      meta: {
        title: 'Available Filters',
      },
    },
    {
      path: '/admin/filters/:filterId',
      name: 'adminFilterDetail',
      component: FilterDetail,
      beforeEnter: requireAdmin(routerStore),
      meta: {
        title: 'Filter detail (Admin view)',
        requireAdmin: true,
      },
    },
    {
      path: '/admin/user-management',
      name: 'userManagement',
      component: UserManagement,
      beforeEnter: requireAdmin(routerStore),
      meta: {
        title: 'User Management',
      },
    },
    {
      path: '/admin/announcements',
      name: 'announcementList',
      component: AnnouncementList,
      beforeEnter: requireAdmin(routerStore),
      meta: {
        title: 'Announcements',
      },
    },
    {
      path: '/admin/announcements/create',
      name: 'announcementCreate',
      component: AnnouncementCreate,
      beforeEnter: requireAdmin(routerStore),
      meta: {
        title: 'New Announcement',
      },
    },
    {
      path: '/tags',
      name: 'tagList',
      component: TagList,
    },
    {
      path: '/catalogs',
      name: 'catalogSampleList',
      component: CatalogSampleList,
    },
    {
      path: '/faq',
      name: 'faq',
      component: FAQ,
    },
    {
      path: '/support',
      name: 'support',
      component: Support,
    },
    {
      path: '/team',
      name: 'team',
      component: TeamDetail,
      meta: {
        title: 'Our Team',
      },
    },
    {
      path: '/loci/lookup/:lookupQuery',
      name: 'lookupLoci',
      redirect: (from: Route) => ({
        path: '/loci',
        query: {
          query: JSON.stringify({
            filters: [
              {
                type: 'query_string',
                field: {
                  query: `*${from.params.lookupQuery}*`,
                  fields: ['properties.ztf_object_id', 'locus_id'],
                },
                value: null,
                text: `ID Lookup: ${from.params.lookupQuery}`,
              },
            ],
          }),
        },
      }),
    },
  ];

  const router = new VueRouter({
    mode: mode || 'history',
    base: base || process.env.BASE_URL,
    routes,
  });

  router.beforeEach((to: Route, from: Route, next: NavigationGuardNext) => {
    if (process.env.NODE_ENV !== 'test') {
      routerStore.dispatch(FETCH_ANNOUNCEMENTS);
    }
    next();
  });

  router.beforeEach((to: Route, from: Route, next: NavigationGuardNext) => {
    const nearestWithTitle = [...(to.matched)].reverse().find((r) => r.meta && r.meta.title);
    document.title = `${nearestWithTitle ? `${nearestWithTitle.meta.title} | ` : ''}ANTARES`;
    next();
  });

  return router;
}
