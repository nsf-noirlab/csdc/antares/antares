import Vue from 'vue';

import { TooltipPlugin } from 'bootstrap-vue';

Vue.use(TooltipPlugin);
