/* eslint-disable no-shadow */
export enum LIST_STATUS {
  EMPTY = 'EMPTY',
  ERROR = 'ERROR',
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
}

export enum FILTER_VERSION_STATUS {
  APPROVED = 'APPROVED',
  DISABLED = 'DISABLED',
  REJECTED = 'REJECTED',
  REVIEWED = 'REVIEWED',
  PENDING_REVIEW = 'PENDING_REVIEW',
  ENABLED = 'ENABLED',
}

export const STATUS_METADATA = {
  [FILTER_VERSION_STATUS.APPROVED]: {
    event: 'Approve',
    requiresFeedback: false,
    variant: 'primary', // will exist
  },
  [FILTER_VERSION_STATUS.DISABLED]: {
    event: 'Disable',
    requiresFeedback: true,
    variant: 'secondary',
  },
  [FILTER_VERSION_STATUS.REJECTED]: {
    event: 'Reject',
    requiresFeedback: true,
    variant: 'danger', // will exist
  },
  [FILTER_VERSION_STATUS.REVIEWED]: {
    event: 'Mark as reviewed',
    requiresFeedback: false,
    variant: 'primary', // exists but will not
  },
  [FILTER_VERSION_STATUS.PENDING_REVIEW]: {
    event: 'Mark as pending',
    requiresFeedback: false,
    variant: 'warning',
  },
  [FILTER_VERSION_STATUS.ENABLED]: {
    event: 'Enable',
    requiresFeedback: false,
    variant: 'success',
  },
};

export const STATUS_TRANSITIONS = {
  [FILTER_VERSION_STATUS.PENDING_REVIEW]: [
    FILTER_VERSION_STATUS.REVIEWED,
  ],
  [FILTER_VERSION_STATUS.REVIEWED]: [
    FILTER_VERSION_STATUS.PENDING_REVIEW,
    FILTER_VERSION_STATUS.ENABLED,
  ],
  [FILTER_VERSION_STATUS.ENABLED]: [
    FILTER_VERSION_STATUS.DISABLED,
  ],
  [FILTER_VERSION_STATUS.DISABLED]: [
    FILTER_VERSION_STATUS.PENDING_REVIEW,
    FILTER_VERSION_STATUS.ENABLED,
  ],
};
