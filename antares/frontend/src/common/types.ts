/* eslint-disable camelcase, no-use-before-define, @typescript-eslint/no-empty-interface */
export interface ElasticsearchFilter {
  type: string;
  field: string;
  value: any;
  text: string;
}

export interface JWTPayload {
  id: number;
}

export interface FilterVersion {
  id: string;
  code: string;
  comment: string;
  feedback: string;
  created_at: string;
  updated_at: string;
  transitioned_at: string;
  status: string;
  filter: Partial<Filter>;
}

export interface RelatedResource {
  [key: string]: any;
}

export interface RelatedResources {
  [key: string]: RelatedResource;
}

export interface Filter {
  name: string;
  description: string;
  id: string;
  created_at: string;
  updated_at: string;
  enabled: boolean;
  public: boolean;
  versions: FilterVersion[];
  latest_version: FilterVersion; // Deprecated, will be removed
  enabled_version: FilterVersion;
  related: RelatedResources;
  priority: number;
  owner: User | null;
  user_id: string;
}

export interface User {
  id: string;
  username: string;
  name?: string;
  email: string;
  admin: boolean;
  staff: boolean;
}

export interface Locus {
  id: string;
  properties: { [key: string]: any };
  grav_wave_events: string[];
  meta: {
    newest_thumbnails: {
      data: {
        attributes: {
          thumbnail_type: string;
          src: string;
        }
      }
    }[],
    [key: string]: any
  };
}

export interface LocusAnnotation {
  id: string;
  comment: string;
  favorited: boolean;
  owner: Partial<User>;
  locus: Partial<Locus>;
}

export interface UserData {
  locusAnnotations: { [key: string]: LocusAnnotation };
}

export interface LociStatistics {
  tags: { [key: string]: number };
  catalogs: { [key: string]: number };
  min_num_measurements: number;
  max_num_measurements: number;
}

export interface Alert {
  id: string;
  mjd: number;
  grav_wave_events: GravWaveEvent[];
  properties: { [key: string]: any };
}

export interface LocusProperty {
    properties: { [key: string]: any };
}

export interface LocusListing {}

export interface Member {
  bio: string;
  email: string;
  link: string;
  name: string;
  photo?: string;
  title: string;
}

export interface RequestQuery {
  page: {
    limit: number;
    offset: number;
  };
  'page[limit]': number;
  'page[offset]': number;
  sort: string;
  fields: { [key: string]: any };
  include: string;
  elasticsearch_query: any;
  filter: any;
}

export interface WatchObject {
  comment: string;
  declination: number;
  radius: number;
  right_ascension: number;
}

export interface WatchList {
  name: string;
  description: string;
  id: string;
  objects: WatchObject[];
  owner?: Partial<User> & Pick<User, 'id'>;
}

export interface State {
  filter: Filter;
  filterEnabledVersion: FilterVersion;
  filterLatestVersion: FilterVersion;
  filterVersions: FilterVersion[];
  filters: Filter[];
  filtersCount: number;
  watchList: WatchList;
  watchLists: WatchList[];
  watchListsCount: number;
}

export interface Tag {
  id: string;
  filter_version_id: number;
  description: string;
}

export interface CatalogSample {
  object_id: string;
  catalog_name: string;
  data: object;
}

export interface CatalogMatch {
  id: string;
  ra: number;
  dec: number;
  properties: object;
  meta: {
    catalog_id: string;
    catalog_name: string;
    separation: number;
  };
}

export interface Announcement {
  id: string;
  active: boolean;
  variant: string;
  message: string;
  created_at: string;
}

export interface Thumbnail {
  id: string;
  filename: string;
  filemime: string;
  src: string;
  thumbnail_type: string;
}

export interface GravWaveEvent {
  gracedb_id: string;
  contour_level: number;
  contour_area: number;
}

export interface GravWaveAlert extends GravWaveEvent, Pick<Alert, 'id' | 'mjd' | 'properties'> {}
