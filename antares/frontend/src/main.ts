import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';
import VueGtag from 'vue-gtag';
import { mapGetters } from 'vuex';
import './plugins/bootstrap-vue';
import './plugins/fontawesome-vue';
import './plugins/mixins';
import './plugins/popper';
import './plugins/vue-clipboard2';
import { CHECK_AUTH } from '@/store/actions';
import axios from 'axios';
import App from './App.vue';
import createRouter from './router';
import store from './store';

axios.defaults.withCredentials = true;

Vue.use(Vuelidate as any);
Vue.use(VueRouter);

Vue.config.productionTip = false;

const setup = async () => {
  const router = createRouter({}, store);
  Vue.use(VueGtag, {
    config: { id: 'G-VQDMMZSD41' },
  }, router);

  // Inject configuration file
  const { data: config } = await axios.get('/config.json');
  Vue.mixin({
    data() {
      return {
        ANTARES_API_URL: config.ANTARES_API_URL,
        ANTARES_FRONTEND_URL: config.ANTARES_FRONTEND_URL,
        ANTARES_SUPPORT_EMAIL: config.ANTARES_SUPPORT_EMAIL,
        ANTARES_ISSUE_TRACKER_URL: config.ANTARES_ISSUE_TRACKER_URL,
        ANTARES_SLACK_INVITATION: config.ANTARES_SLACK_INVITATION,
      };
    },
  });

  new Vue({
    router,
    store,
    computed: {
      ...mapGetters(['isAuthenticated']),
    },
    created() {
      // If this session still thinks it is valid, check against the API server.
      if (this.isAuthenticated) {
        this.$store.dispatch(CHECK_AUTH);
      }
    },
    render: (h) => h(App),
  }).$mount('#app');
};
setup();
