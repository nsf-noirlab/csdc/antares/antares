/* eslint-disable no-await-in-loop, no-constant-condition */
import {
  LocusAnnotation, LocusListing, RequestQuery, User,
} from '@/common/types';
import { authenticated } from '@/api/services';
import { getClient } from '../client';

interface UserServiceDetailResponse {
  data: User;
  meta: { [key: string]: any } | null;
}

interface UserServiceListResponse {
  data: User[];
  meta: { count: number };
}

interface UserServiceFavoriteLociResponse {
  data: LocusListing[];
  meta: { count: number };
}

export default class {
  static async create(user: User): Promise<UserServiceDetailResponse> {
    const client = await getClient();
    return client.create('user', user);
  }

  @authenticated()
  static async get(userId: string): Promise<UserServiceDetailResponse> {
    const client = await getClient();
    return client.find('user', userId);
  }

  @authenticated()
  static async createLocusAnnotation(userId: string, locusAnnotation: Partial<LocusAnnotation>) {
    const client = await getClient();
    return client.one('user', userId).all('locus_annotation').post(locusAnnotation);
  }

  @authenticated()
  static async getLocusAnnotations(userId: string) {
    const locusAnnotationMap: { [key: string]: any } = {};
    const limit = 1000;
    let offset = 0;
    const client = await getClient();
    while (true) {
      const { data, meta } = await client
        .one('user', userId)
        .all('locus_annotations')
        .get({ page: { limit, offset } });
      data.forEach((annotation: any) => {
        if (annotation.locus) {
          locusAnnotationMap[annotation.locus.id] = {
            id: annotation.id,
            comment: annotation.comment,
            favorited: annotation.favorited,
          };
        }
      });
      if (meta.count > offset + limit) {
        offset += limit;
      } else {
        break;
      }
    }
    return locusAnnotationMap;
  }

  @authenticated()
  static async getFavoriteLoci(
    userId: string,
    query: Partial<RequestQuery>,
  ): Promise<UserServiceFavoriteLociResponse> {
    const client = await getClient();
    const response = await client
      .one('user', userId)
      .all('locus_annotations')
      .get({ ...query });
    response.data = await Promise.all(response.data
      .filter((annotation: any) => annotation.locus && annotation.favorited)
      .map(async (annotation: any) => (await client.one('locus', annotation.locus.id).get()).data));
    return response;
  }

  @authenticated()
  static async addFavoriteLoci(
    userId: string,
    locusIds: string[],
  ): Promise<UserServiceFavoriteLociResponse> {
    const client = await getClient();
    return client
      .one('user', userId)
      .relationships()
      .all('favorite_loci')
      .post(
        locusIds.map((locusId) => ({
          type: 'locus',
          id: locusId,
        })),
      );
  }

  @authenticated()
  static async removeFavoriteLoci(
    userId: string,
    locusIds: string[],
  ): Promise<UserServiceFavoriteLociResponse> {
    const client = await getClient();
    return client
      .one('user', userId)
      .relationships()
      .all('favorite_loci')
      .destroy(
        locusIds.map((locusId) => ({
          type: 'locus',
          id: locusId,
        })),
      );
  }

  @authenticated()
  static async list(query: Partial<RequestQuery>): Promise<UserServiceListResponse> {
    const client = await getClient();
    return client.findAll('user', query);
  }

  @authenticated()
  static async grantAdmin(userId: string): Promise<UserServiceDetailResponse> {
    const client = await getClient();
    return client.update('user', {
      id: userId,
      admin: true,
    });
  }

  @authenticated()
  static async revokeAdmin(userId: string): Promise<UserServiceDetailResponse> {
    const client = await getClient();
    return client.update('user', {
      id: userId,
      admin: false,
    });
  }

  @authenticated()
  static async grantStaff(userId: string): Promise<UserServiceDetailResponse> {
    const client = await getClient();
    return client.update('user', {
      id: userId,
      staff: true,
    });
  }

  @authenticated()
  static async revokeStaff(userId: string): Promise<UserServiceDetailResponse> {
    const client = await getClient();
    return client.update('user', {
      id: userId,
      staff: false,
    });
  }
}
