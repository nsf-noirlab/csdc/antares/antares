import { Alert, RequestQuery } from '@/common/types';
import { getClient } from '../client';

interface AlertServiceDetailResponse {
  data: Alert;
  meta: { [key: string]: any } | null;
}

export default {
  async get(alertId: string): Promise<AlertServiceDetailResponse> {
    const client = await getClient();
    return client.find('alert', alertId);
  },
  async listThumbnails(alertId: string, query: Partial<RequestQuery>) {
    const client = await getClient();
    return client.one('alert', alertId).all('thumbnail').get({ ...query });
  },
};
