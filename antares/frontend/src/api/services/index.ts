/* eslint-disable func-names, no-param-reassign, no-throw-literal */
import store from '@/store';
import { LOGOUT } from '@/store/actions';
import { Error } from '../client/middleware/error';
import { AuthService } from '..';

/**
 * authenticated is a function decorator that handles fetching a new access/refresh
 * token set in the event that an authenticated request is met with a 401 due to
 * an expired token.
 */
export function authenticated() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const method = descriptor.value;
    descriptor.value = async function (...args: any[]) {
      if (!store.getters.isAuthenticated) {
        throw `Must be authenticated to call ${propertyKey}`;
      }
      let result;
      try {
        result = await method.apply(this, args);
      } catch (errors) {
        try {
          const { 0: error } = errors as Error[];
          if (error.status === 401 && error.detail && error.detail.includes('expire')) {
            await AuthService.refresh();
            return method.apply(this, args);
          }
        } catch (error) {
          if (error.response.status === 401) {
            await store.dispatch(LOGOUT);
          }
          throw errors;
        }
        throw errors;
      }
      return result;
    };
    return descriptor;
  };
}

/**
 * authenticatedOptional is a function decorator that handles fetching a new access/refresh
 * token set in the event that an authenticated request is met with a 401 due to
 * an expired token. If a refresh token can't be obtained it will log the user out but
 * call the requested route anyways. If the user is not logged in it will call the
 * requested route as normal.
 */
export function authenticatedOptional() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const method = descriptor.value;
    descriptor.value = async function (...args: any[]) {
      if (!store.getters.isAuthenticated) {
        return method.apply(this, args);
      }
      let result;
      try {
        result = await method.apply(this, args);
      } catch (errors) {
        try {
          const { 0: error } = errors as Error[];
          if (error.status === 401 && error.detail && error.detail.includes('expire')) {
            await AuthService.refresh();
            return method.apply(this, args);
          }
        } catch (error) {
          if (error.response.status === 401) {
            await store.dispatch(LOGOUT);
          }
          return method.apply(this, args);
        }
        throw errors;
      }
      return result;
    };
    return descriptor;
  };
}
