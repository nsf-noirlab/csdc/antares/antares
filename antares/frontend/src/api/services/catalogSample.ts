import { CatalogSample, RequestQuery } from '@/common/types';
import { getClient } from '../client';

interface CatalogSampleServiceListResponse {
  data: CatalogSample[];
  meta: { count: number };
}

export default {
  async list(query: Partial<RequestQuery>): Promise<CatalogSampleServiceListResponse> {
    const client = await getClient();
    return client.findAll('catalog_sample', query);
  },
};
