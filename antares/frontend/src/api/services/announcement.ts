import { Announcement, RequestQuery } from '@/common/types';
import { authenticated } from '@/api/services';
import { getClient } from '../client';

interface AnnouncementServiceDetailResponse {
  data: Announcement;
  meta: { [key: string]: any } | null;
}

interface AnnouncementServiceListResponse {
  data: Announcement[];
  meta: { count: number };
}

export default class {
  @authenticated()
  static async create(
    announcement: Partial<Announcement>,
  ):Promise<AnnouncementServiceDetailResponse> {
    const client = await getClient();
    return client.all('announcement').post(announcement);
  }

  static async list(query: Partial<RequestQuery>): Promise<AnnouncementServiceListResponse> {
    const client = await getClient();
    return client.findAll('announcement', query);
  }

  @authenticated()
  static async makeActive(announcementId: string) {
    const client = await getClient();
    return client.update('announcement', {
      id: announcementId,
      active: true,
    });
  }

  @authenticated()
  static async makeInactive(announcementId: string) {
    const client = await getClient();
    return client.update('announcement', {
      id: announcementId,
      active: false,
    });
  }
}
