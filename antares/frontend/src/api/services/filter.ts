import { Filter, FilterVersion, RequestQuery } from '@/common/types';
import { authenticated, authenticatedOptional } from '@/api/services';
import store from '@/store';
import { getClient } from '../client';

interface FilterServiceDetailResponse {
  data: Filter;
  meta: { [key: string]: any } | null;
}

export interface FilterServiceListResponse {
  data: Filter[];
  meta: { count: number };
}

interface FilterVersionServiceDetailResponse {
  data: FilterVersion;
  meta: { count: number };
}

interface FilterVersionServiceListResponse {
  data: FilterVersion[];
  meta: { count: number };
}

export default class {
  @authenticated()
  static async create(filter: Filter): Promise<FilterServiceDetailResponse> {
    const client = await getClient();
    return client.one('user', store.getters.user.id).all('filter').post(filter);
  }

  @authenticatedOptional()
  static async get(
    filterId: string, query: Partial<RequestQuery>,
  ): Promise<FilterServiceDetailResponse> {
    const client = await getClient();
    return client.find('filter', filterId, query);
  }

  @authenticatedOptional()
  static async getVersions(
    filterId: string,
    query: Partial<RequestQuery>,
  ): Promise<FilterVersionServiceListResponse> {
    const client = await getClient();
    return client.one('filter', filterId).all('filter_version').get(query);
  }

  @authenticatedOptional()
  static async getEnabledVersion(
    filterId: string,
  ): Promise<FilterVersionServiceDetailResponse | null> {
    const client = await getClient();
    const { data: filter } = await client.one('filter', filterId).get();
    if (filter.enabled_version) {
      return client.one('filter', filterId).one('filter_version', filter.enabled_version.id).get();
    }
    return null;
  }

  @authenticated()
  static async addVersion(
    filterId: string,
    filterVersion: Pick<FilterVersion, 'code' | 'comment' | 'filter'>,
  ): Promise<FilterVersionServiceDetailResponse> {
    const client = await getClient();
    return client.one('filter', filterId).all('filter_version').post(filterVersion);
  }

  @authenticated()
  static async updateVersion(
    filterId: string,
    filterVersionId: string,
    filterVersionPayload: Pick<FilterVersion, 'status'>,
  ): Promise<FilterVersionServiceDetailResponse> {
    const client = await getClient();
    return client
      .one('filter', filterId)
      .one('filter_version', filterVersionId)
      .patch({
        id: filterVersionId,
        ...filterVersionPayload,
      });
  }

  @authenticatedOptional()
  static async list(query: Partial<RequestQuery>): Promise<FilterServiceListResponse> {
    const client = await getClient();
    return client.findAll('filter', query);
  }

  @authenticated()
  static async listUserFilters(
    userId: string, query: Partial<RequestQuery>,
  ): Promise<FilterServiceListResponse> {
    const client = await getClient();
    return client.one('user', userId).all('filter').get(query);
  }

  @authenticated()
  static async makePublic(filterId: string) {
    const client = await getClient();
    return client.update('filter', {
      id: filterId,
      public: true,
    });
  }

  @authenticated()
  static async makePrivate(filterId: string) {
    const client = await getClient();
    return client.update('filter', {
      id: filterId,
      public: false,
    });
  }
}
