import { RequestQuery } from '@/common/types';
import { getClient } from '../client';

export default {
  async get(locusId: string) {
    const client = await getClient();
    return client.find('locus', locusId);
  },
  async list(query: Partial<RequestQuery>) {
    const client = await getClient();
    return client.findAll('locus_property', query);
  },
};
