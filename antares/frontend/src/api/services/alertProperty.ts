import { RequestQuery } from '@/common/types';
import { getClient } from '../client';

export default {
  async get(name: string) {
    const client = await getClient();
    return client.find('alert_property', name);
  },
  async list(query: Partial<RequestQuery>) {
    const client = await getClient();
    return client.findAll('alert_property', query);
  },
};
