import AuthService from './services/auth';
import AlertPropertyService from './services/alertProperty';
import AnnouncementService from './services/announcement';
import LocusService from './services/locus';
import LocusPropertyService from './services/locusProperty';
import FilterService from './services/filter';
import UserService from './services/user';
import LocusAnnotationService from './services/locusAnnotation';
import WatchListService from './services/watchList';
import TagService from './services/tag';
import CatalogSampleService from './services/catalogSample';

export { default as AuthService } from './services/auth';
export { default as AlertService } from './services/alert';
export { default as AlertPropertyService } from './services/alertProperty';
export { default as AnnouncementService } from './services/announcement';
export { default as LocusService } from './services/locus';
export { default as LocusPropertyService } from './services/locusProperty';
export { default as FilterService } from './services/filter';
export { default as UserService } from './services/user';
export { default as LocusAnnotationService } from './services/locusAnnotation';
export { default as WatchListService } from './services/watchList';
export { default as TagService } from './services/tag';
export { default as CatalogSampleService } from './services/catalogSample';

export function getCookieValue(name: string): string {
  const value = document.cookie.match(`(^|[^;]+)\\s*${name}\\s*=\\s*([^;]+)`);
  return value ? <string>value.pop() : '';
}
declare global {
    interface Window { ANTARES: any; }
}

window.ANTARES = {
  API: {
    AuthService,
    AlertPropertyService,
    AnnouncementService,
    LocusService,
    LocusPropertyService,
    FilterService,
    UserService,
    LocusAnnotationService,
    WatchListService,
    TagService,
    CatalogSampleService,
  },
  Views: {},
};
