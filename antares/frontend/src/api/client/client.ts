/* eslint-disable no-param-reassign, import/prefer-default-export */

import axios from 'axios';
import JsonApi from 'devour-client';
import errorMiddleware from './middleware/error';
import { getCookieValue } from '..';

export async function getClient() {
  const { data: config } = await axios.get('/config.json');
  const client = new JsonApi({ apiUrl: config.ANTARES_API_URL });
  client.axios.defaults.withCredentials = true;

  // TODO: Middleware to convert request FROM camelCase TO underscore_separated
  client.insertMiddlewareBefore('axios-request', {
    name: 'serializeFilterParams',
    req: (payload: any) => {
      const csrfToken = getCookieValue('csrf_access_token');
      if (csrfToken) {
        payload.req.headers['X-CSRF-TOKEN'] = csrfToken;
      }
      if (payload.req.params && payload.req.params.filter) {
        payload.req.params.filter = JSON.stringify(payload.req.params.filter);
      }
      return payload;
    },
  });
  // client.replaceMiddleware('response', responseMiddleware);
  client.replaceMiddleware('errors', errorMiddleware);

  client.define(
    'filter',
    {
      created_at: '',
      disabled_at: '', // Deprecated, will not be present in schema
      updated_at: '',
      name: '',
      description: '',
      latest_version: { // Deprecated, will not be present in schema
        jsonApi: 'hasOne',
        type: 'filter_version',
      },
      enabled_version: {
        jsonApi: 'hasOne',
        type: 'filter_version',
      },
      priority: null,
      level: null,
      public: false,
      enabled: false,
      user_id: null,
      owner: {
        jsonApi: 'hasOne',
        type: 'user',
      },
    },
    {
      type: 'filter',
    },
  );

  client.define(
    'filter_version',
    {
      code: '',
      comment: '',
      created_at: '',
      feedback: '',
      enabled_at: '', // Deprecated, will not be present in schema
      validated_at: '', // Deprecated, will not be present in schema
      disabled_at: '', // Deprecated, will not be present in schema
      public: true, // Deprecated, will not be present in schema
      status: '',
      transitioned_at: '',
      updated_at: '',
      filter: {
        jsonApi: 'hasOne',
        type: 'filter',
      },
    },
    {
      type: 'filter_version',
      collectionPath: 'versions',
    },
  );

  client.define(
    'locus_listing',
    {
      properties: {},
      tags: [],
      ra: null,
      dec: null,
      htm16: '',
      catalogs: [],
      grav_wave_events: [],
      locus: {
        jsonApi: 'hasOne',
        type: 'locus',
      },
      catalog_matches: {
        jsonApi: 'hasMany',
        type: 'catalog_entry',
      },
      alerts: {
        jsonApi: 'hasMany',
        type: 'alert',
      },
    },
    {},
  );

  client.define(
    'locus_listing_statistic',
    {
      tags: {},
      catalogs: {},
      min_num_measurements: 0,
      max_num_measurements: 0,
    },
    {},
  );

  client.define(
    'locus',
    {
      properties: {},
      tags: [],
      ra: null,
      dec: null,
      lightcurve: null,
      htm16: '',
      catalogs: [],
      grav_wave_events: [],
      alerts: {
        jsonApi: 'hasMany',
        type: 'alert',
      },
      catalog_matches: {
        jsonApi: 'hasMany',
        type: 'catalog_entry',
      },
    },
    {},
  );

  client.define(
    'alert',
    {
      mjd: '',
      properties: {},
      grav_wave_events: [],
    },
    {
      type: 'alert',
      collectionPath: 'alerts',
    },
  );

  client.define(
    'locus_annotation',
    {
      comment: '',
      favorited: false,
      owner: {
        jsonApi: 'hasOne',
        type: 'user',
      },
      locus: {
        jsonApi: 'hasOne',
        type: 'locus',
      },
    },
    {
      type: 'locus_annotation',
      collectionPath: 'locus_annotations',
    },
  );

  client.define(
    'user',
    {
      name: '',
      username: '',
      email: '',
      password: '',
      staff: false,
      admin: false,
      favorite_loci: {
        jsonApi: 'hasMany',
        type: 'locus',
      },
    },
    {
      type: 'user',
      collectionPath: 'users',
    },
  );

  client.define(
    'alert_property',
    {
      type: '',
      origin: '',
      description: '',
      es_mapping: '',
    },
    {
      type: 'alert_property',
      collectionPath: 'alert_properties',
    },
  );

  client.define(
    'locus_property',
    {
      type: '',
      origin: '',
      description: '',
      es_mapping: '',
      properties: {},
    },
    {
      type: 'locus_property',
      collectionPath: 'loci_properties',
    },
  );

  client.define(
    'watch_list',
    {
      name: '',
      description: '',
      created_at: '',
      objects: [],
      owner: {
        jsonApi: 'hasOne',
        type: 'user',
      },
      slack_channel: '',
    },
    {
      type: 'watch_list',
      collectionPath: 'watch_lists',
    },
  );

  client.define(
    'tag',
    {
      name: '',
      filter_version_id: '',
      description: '',
    },
    {
      type: 'tag',
      collectionPath: 'tags',
    },
  );

  client.define(
    'catalog_sample',
    {
      object_id: null,
      catalog_name: null,
      data: {},
    },
    {
      type: 'catalog_sample',
      collectionPath: 'catalog_samples',
    },
  );

  client.define(
    'catalog',
    {
      name: null,
    },
    {
      type: 'catalog',
      collectionPath: 'catalogs',
    },
  );

  client.define(
    'catalog_entry',
    {
      ra: null,
      dec: null,
      properties: {},
      object_id: null,
      object_name: '',
      catalog: {
        jsonApi: 'hasOne',
        type: 'catalog',
      },
    },
    {
      type: 'catalog_entry',
      collectionPath: 'catalog-entries',
    },
  );

  client.define(
    'announcement',
    {
      active: false,
      variant: '',
      message: '',
      created_at: '',
    },
    {
      type: 'announcement',
      collectionPath: 'announcements',
    },
  );

  client.define(
    'alert_thumbnail',
    {
      filename: '',
      filemime: '',
      src: '',
      thumbnail_type: '',
    },
    {
      type: 'alert_thumbnail',
      collectionPath: 'thumbnails',
    },
  );

  return client;
}
