/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  verbose: true,
  coverageReporters: ['clover', 'json', 'lcov', 'text', 'cobertura'],
  preset: 'ts-jest', // old - '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  /* // this was originally added to deal with the pdfkit stuff
  // seems not needed but kept for reference
  setupFiles: [
    '<rootDir>/jest-register-context.js',
  ],
  */
  moduleNameMapper: {
    d3: '<rootDir>/node_modules/d3/dist/d3.min.js',
    '\\.afm$': 'jest-raw-loader',
    '^.+\\.(css|styl|less|sass|scss)$': 'jest-transform-stub',
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  transform: {
    '.\\*.spec.ts': ['ts-jest', {
    }],
    '.*\\afm$': 'jest-raw-loader',
    '^.+\\.vue$': '@vue/vue2-jest',
    '.+\\.(png|jpg|svg|ttf|woff|woff2)$': 'jest-transform-stub',
    '.+vis/pdf\\.js$': 'jest-transform-stub',
  },
  // # 'jsdom' sets global DOM object for testing
  // # with screen etc (https://testing-library.com/docs/queries/about/#screen)
  testEnvironment: 'jsdom',
};
