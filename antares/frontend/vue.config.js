// const { defineConfig } = require('@vue/cli-service');
// const Buffer = require('buffer/').Buffer;

const webpack = require('webpack');

// this is to prevent error with dev server
// ERROR  ValidationError: Progress Plugin Invalid Options
process.env.VUE_CLI_TEST = false;

module.exports = {
  // we need these for pdfkit and PDF exports of light curves to work

  // This is based on
  // https://github.com/blikblum/pdfkit-webpack-example/blob/master/webpack.config.js

  // We use the API syntax and chainWebpack because vue-cli-service doesn't
  // allow `resolve` and `module` entries.

  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
    ],
    resolve: {
      fallback: {
        stream: require.resolve('stream-browserify'),
        util: require.resolve('util/'),
        zlib: require.resolve('browserify-zlib'),
        os: require.resolve('os-browserify/browser'),
        assert: require.resolve('assert/'),
        buffer: require.resolve('buffer/'),
      },
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set('fs', 'pdfkit/js/virtual-fs.js');

    config.module
      .rule('fontkit')
      .test(/fontkit[/\\]index.js$/)
      .use('fontkit-brfs')
      .loader('transform-loader?brfs');

    config.module
      .rule('unicode-properties')
      .test(/unicode-properties[/\\]index.js$/)
      .use('up-brfs')
      .loader('transform-loader?brfs');

    config.module
      .rule('linebreaker')
      .test(/linebreak[/\\]src[/\\]linebreaker.js/)
      .use('lb-brfs')
      .loader('transform-loader?brfs');

    config.module
      .rule('font-assets-array-buffer')
      .test(/src[/\\]pdfkit-assets/)
      .use('arraybuffer-loader')
      .loader('arraybuffer-loader');

    config.module
      .rule('afm')
      .test(/\.afm$/)
      .use('raw')
      .loader('raw-loader');
  },
  lintOnSave: false,
};
