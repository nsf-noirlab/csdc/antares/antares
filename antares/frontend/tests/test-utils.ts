import Vue, { ComponentOptions } from 'vue';
import VueRouter from 'vue-router';
import { Store } from 'vuex';
import { BootstrapVue } from 'bootstrap-vue';
import { render, RenderOptions, RenderResult } from '@testing-library/vue';
import { createLocalVue, VueClass } from '@vue/test-utils';
import Vuelidate from 'vuelidate';
import { rest, RequestHandler } from 'msw';
import { setupServer, SetupServer } from 'msw/node';
import mixinObject from '@/plugins/mixins';
import createRouter from '@/router';
import defaultStore, { createStore } from '@/store';
import { RootState } from '@/store/types';
import configMock from './unit/mocks/config.json';

import '@/plugins/fontawesome-vue';

export function renderWithPlugins<V extends Vue, S>(
  TestComponent: VueClass<V> | ComponentOptions<V>,
  options?: RenderOptions<V, S>,
): RenderResult {
  const localVue = options?.localVue || createLocalVue();
  localVue.use(BootstrapVue);
  localVue.use(Vuelidate as any);

  localVue.mixin({
    data() {
      return {
        ...configMock,
      };
    },
    ...mixinObject,
  });

  return render(TestComponent, {
    ...options,
    localVue,
  });
}

function getStore(storeOrConfig: Store<RootState> | Record<string, unknown>) {
  if (storeOrConfig instanceof Store) return storeOrConfig;

  return createStore(storeOrConfig);
}

export async function renderRoute<V extends Vue, S>(
  path?: string,
  options?: RenderOptions<V, S>,
): Promise<RenderResult> {
  const localVue = options?.localVue || createLocalVue();
  localVue.use(VueRouter);

  // The createRouter method receives a store instance, not a store config object
  // So even though in principle we could let @testing-library/vue's render method
  // take care of creating the store, we are required to create the store manually before
  const store = options?.store ? getStore(options?.store as any) : defaultStore;

  const router = createRouter({
    mode: 'abstract',
  }, store);

  const TestApp = {
    template: '<router-view />',
  };

  const renderResult = renderWithPlugins(TestApp, {
    ...options,
    routes: router,
    store,
    localVue,
  });

  try {
    await router.push(path || '/');
  } catch (err) {
    // We want to allow only navigation redirect errors in tests
    // Any other error should be thrown normally
    if (!VueRouter.isNavigationFailure(err, VueRouter.NavigationFailureType.redirected)) {
      throw err;
    }
  }

  return renderResult;
}

/**
 * Creates and returns a MSW mock server with default handlers
 * and the ones passed in initialHandlers
 */
export function getMockServer(...initialHandlers: Array<RequestHandler>): SetupServer {
  const defaultHandlers = [
    rest.get('/config.json', (req, res, ctx) => res(ctx.json(configMock))),
    rest.get('/v1/auth/check', (req, res, ctx) => res(ctx.status(204))),
    rest.post('/v1/auth/logout', (req, res, ctx) => res(ctx.status(204))),
  ];
  const handlers = [
    ...defaultHandlers,
    ...initialHandlers || [],
  ];

  return setupServer(...handlers);
}
