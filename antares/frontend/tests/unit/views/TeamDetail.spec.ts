/* eslint-disable no-shadow, global-require, @typescript-eslint/no-var-requires */
import { RenderResult } from '@testing-library/vue';
import { renderRoute } from '../../test-utils';
import members from '../mocks/members.json';

jest.mock('@/data/team.json', () => {
  // TODO: avoid require for using members.json as mock
  const members = require('../mocks/members.json');
  return {
    internal: members,
    external: [],
  };
}, {
  virtual: true,
});

describe('TeamDetail view', () => {
  const memberAltRegex = /Picture\sof\s\w+/;
  let result: RenderResult;

  beforeEach(async () => {
    result = await renderRoute('/team');
  });

  it('renders route correctly', () => {
    expect(result.queryByText('Our Team')).not.toBeNull();
  });

  describe('when using members unsorted by last name', () => {
    it('displays members sorted by last name', () => {
      const elements = result.queryAllByAltText(memberAltRegex);
      expect(elements[0].getAttribute('alt')).not.toContain(members[0].name);
    });
  });
});
