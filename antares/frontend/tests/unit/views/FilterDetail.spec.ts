import { RenderResult, waitFor } from '@testing-library/vue';
import { rest } from 'msw';
import { renderRoute, getMockServer } from '../../test-utils';
import filterMock from '../mocks/filter-detail.json';

// Mocking both 'authenticated' and 'authenticatedOptional' decorators
// since both have the Vuex store singleton as a hard dependency.
// We should aim to remove that dependency so we can control which store
// these decorators use. Currently this is not possible so the following mocks
// should be present whenever a test depends on the decorators mentioned above
jest.mock('@/api/services', () => {
  const originalModule = jest.requireActual('@/api/services');
  return {
    __esModule: true,
    ...originalModule,
    authenticated: jest.fn(),
    authenticatedOptional: jest.fn(),
  };
});

describe('FilterDetail view', () => {
  let result: RenderResult;
  const { id: filterId } = filterMock.data;
  const stubs = {
    FilterVersionsViewer: true, // Avoid versions viewer rendering
  };

  describe('when user is not authenticated', () => {
    beforeEach(async () => {
      result = await renderRoute(`/filters/${filterId}`, {
        store: {
          getters: {
            isAuthenticated: () => false,
          },
        },
      });
    });

    it('redirects to login page', async () => {
      await waitFor(() => {
        expect(result.queryAllByText('Login').length).toBeGreaterThan(1);
      });
    });
  });

  describe('when user is authenticated but cannot perform actions on filters', () => {
    const server = getMockServer(
      rest.get('/v1/filters/:filter_id', (req, res, ctx) => res(ctx.json(filterMock))),
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    beforeEach(async () => {
      result = await renderRoute(`/filters/${filterId}`, {
        store: {
          getters: {
            isAuthenticated: () => true,
            canPerformActionsOnFilter: () => () => false,
          },
        },
        stubs,
      });
    });

    it('displays the title of the filter', async () => {
      await waitFor(() => {
        expect(result.queryByText(filterMock.data.attributes.name)).toBeTruthy();
      });
    });

    it('does not display the public switch', async () => {
      await waitFor(() => {
        expect(result.queryByText('Public')).toBeNull();
      });
    });
  });

  describe('when user is authenticated and can perform actions on filters', () => {
    const server = getMockServer(
      rest.get('/v1/filters/:filter_id', (req, res, ctx) => res(ctx.json(filterMock))),
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    beforeEach(async () => {
      result = await renderRoute(`/filters/${filterId}`, {
        store: {
          getters: {
            isAuthenticated: () => true,
            canPerformActionsOnFilter: () => () => true,
          },
        },
        stubs,
      });
    });

    it('displays the title of the filter', async () => {
      await waitFor(() => {
        expect(result.queryByText(filterMock.data.attributes.name)).toBeTruthy();
      });
    });

    it('displays the public switch', async () => {
      await waitFor(() => {
        expect(result.queryByText('Public')).toBeTruthy();
      });
    });
  });
});
