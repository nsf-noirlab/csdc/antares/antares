import { RenderResult, waitFor } from '@testing-library/vue';
import { rest } from 'msw';
import { renderRoute, getMockServer } from '../../test-utils';
import filtersMock from '../mocks/filters.json';

// Mocking both 'authenticated' and 'authenticatedOptional' decorators
// since both have the Vuex store singleton as a hard dependency.
// We should aim to remove that dependency so we can control which store
// these decorators use. Currently this is not possible so the following mocks
// should be present whenever a test depends on the decorators mentioned above
jest.mock('@/api/services', () => {
  const originalModule = jest.requireActual('@/api/services');
  return {
    __esModule: true,
    ...originalModule,
    authenticated: jest.fn(),
    authenticatedOptional: jest.fn(),
  };
});

const COPY_TEXT = {
  title: 'Find Objects of Interest',
  login: 'Login to create your first Filter',
  newFilter: 'Create your first Filter',
};

const FILTER_LIST_MOCKS = {
  empty: {
    data: [],
    meta: {
      count: 0,
    },
  },
  nonEmpty: filtersMock,
};

describe('FilterList view', () => {
  let result: RenderResult;

  describe('when user is not authenticated', () => {
    beforeEach(async () => {
      result = await renderRoute('/filters', {
        store: {
          getters: {
            isAuthenticated: () => false,
          },
        },
      });
    });

    it('displays title of the page', () => {
      expect(result.queryByText(COPY_TEXT.title)).toBeTruthy();
    });

    it('displays a link to login', () => {
      expect(result.queryByText(COPY_TEXT.login)).toBeTruthy();
    });
  });

  describe('when user is authenticated and filter list is empty', () => {
    const server = getMockServer(
      rest.get('/v1/users/:user_id/filters', (req, res, ctx) => res(ctx.json(FILTER_LIST_MOCKS.empty))),
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    beforeEach(async () => {
      result = await renderRoute('/filters', {
        store: {
          getters: {
            isAuthenticated: () => true,
            user: () => ({ id: 'user-id' }),
          },
        },
      });
    });

    it('displays title of the page', async () => {
      await waitFor(() => {
        expect(result.queryByText(COPY_TEXT.title)).toBeTruthy();
      });
    });

    it('displays a link to create a new filter', async () => {
      await waitFor(() => {
        expect(result.queryByText(COPY_TEXT.newFilter)).toBeTruthy();
      });
    });

    it('does not display a table', async () => {
      await waitFor(() => {
        const tableElem = result.container.querySelector('table');
        expect(tableElem).toBeNull();
      });
    });
  });

  describe('when user is authenticated and filter list is not empty', () => {
    const server = getMockServer(
      rest.get('/v1/users/:user_id/filters', (req, res, ctx) => res(ctx.json(FILTER_LIST_MOCKS.nonEmpty))),
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    beforeEach(async () => {
      result = await renderRoute('/filters', {
        store: {
          getters: {
            isAuthenticated: () => true,
            user: () => ({ id: 'user-id' }),
          },
        },
      });
    });

    it('displays a table', async () => {
      await waitFor(() => {
        const tableElem = result.container.querySelector('table');
        expect(tableElem).toBeTruthy();
      });
    });

    it('does not display "Author" column', async () => {
      await waitFor(() => {
        expect(result.queryByText('Author')).toBeNull();
      });
    });
  });
});
