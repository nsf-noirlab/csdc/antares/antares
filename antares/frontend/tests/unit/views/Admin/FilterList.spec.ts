import { RenderResult, waitFor } from '@testing-library/vue';
import { rest } from 'msw';
import { renderRoute, getMockServer } from '../../../test-utils';
import filtersMock from '../../mocks/filters.json';

// Mocking both 'authenticated' and 'authenticatedOptional' decorators
// since both have the Vuex store singleton as a hard dependency.
// We should aim to remove that dependency so we can control which store
// these decorators use. Currently this is not possible so the following mocks
// should be present whenever a test depends on the decorators mentioned above
jest.mock('@/api/services', () => {
  const originalModule = jest.requireActual('@/api/services');
  return {
    __esModule: true,
    ...originalModule,
    authenticated: jest.fn(),
    authenticatedOptional: jest.fn(),
  };
});

const COPY_TEXT = {
  title: 'Available filters',
};

const FILTER_LIST_MOCKS = {
  empty: {
    data: [],
    meta: {
      count: 0,
    },
  },
  nonEmpty: filtersMock,
};

describe('AdminFilterList view', () => {
  let result: RenderResult;

  describe('when user is not authenticated', () => {
    beforeEach(async () => {
      result = await renderRoute('/admin/filters', {
        store: {
          getters: {
            isAuthenticated: () => false,
            isUserAdmin: () => false,
          },
        },
      });
    });

    it('redirects to login page', async () => {
      await waitFor(() => {
        expect(result.queryAllByText('Login').length).toBeGreaterThan(1);
      });
    });
  });

  describe('when user is authenticated and filter list is empty', () => {
    const server = getMockServer(
      rest.get('/v1/filters', (req, res, ctx) => res(ctx.json(FILTER_LIST_MOCKS.empty))),
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    beforeEach(async () => {
      result = await renderRoute('/admin/filters', {
        store: {
          getters: {
            isAuthenticated: () => true,
            isUserAdmin: () => true,
          },
        },
      });
    });

    it('displays title of the page', () => {
      expect(result.queryByText(COPY_TEXT.title)).toBeTruthy();
    });

    it('displays a table without rows', async () => {
      await waitFor(() => {
        const rowElems = result.container.querySelectorAll('table tbody tr');
        expect(rowElems).toHaveLength(0);
      });
    });
  });

  describe('when user is authenticated and filter list is not empty', () => {
    const server = getMockServer(
      rest.get('/v1/filters', (req, res, ctx) => res(ctx.json(FILTER_LIST_MOCKS.nonEmpty))),
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    beforeEach(async () => {
      result = await renderRoute('/admin/filters', {
        store: {
          getters: {
            isAuthenticated: () => true,
            isUserAdmin: () => true,
          },
        },
      });
    });

    it('displays title of the page', () => {
      expect(result.queryByText(COPY_TEXT.title)).toBeTruthy();
    });

    it('displays a table with the same amount of rows as records', async () => {
      await waitFor(() => {
        const rowElems = result.container.querySelectorAll('table tbody tr');
        expect(rowElems).toHaveLength(FILTER_LIST_MOCKS.nonEmpty.meta.count);
      });
    });

    it('displays "Author" column', () => {
      expect(result.queryByText('Author')).toBeTruthy();
    });
  });
});
