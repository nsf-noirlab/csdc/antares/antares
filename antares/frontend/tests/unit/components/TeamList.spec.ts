import { RenderOptions, RenderResult } from '@testing-library/vue';
import Component from '@/components/TeamList.vue';
import { renderWithPlugins } from '../../test-utils';
import members from '../mocks/members.json';

const renderTeamList = <V extends Vue, S>(options?: RenderOptions<V, S>): RenderResult => (
  renderWithPlugins(Component, {
    ...options,
    props: {
      ...(options?.props || {}),
      title: 'Title',
    },
  })
);

describe('TeamList component', () => {
  const noMembersRegex = /No\s\w+\smembers/;
  const memberAltRegex = /Picture\sof\s\w+/;

  describe('when passing no members prop', () => {
    it('displays a message indicating there are no members', () => {
      const { queryByText } = renderTeamList();
      expect(queryByText(noMembersRegex)).not.toBeNull();
    });
  });

  describe('when passing empty members prop', () => {
    it('displays a message indicating there are no members', () => {
      const { queryByText } = renderTeamList({
        props: {
          members: [],
        },
      });
      expect(queryByText(noMembersRegex)).not.toBeNull();
    });
  });

  describe('when passing non empty members prop', () => {
    it('renders the members list', () => {
      const { getAllByAltText } = renderTeamList({
        props: {
          members,
        },
      });
      expect(getAllByAltText(memberAltRegex)).toHaveLength(members.length);
    });
  });
});
