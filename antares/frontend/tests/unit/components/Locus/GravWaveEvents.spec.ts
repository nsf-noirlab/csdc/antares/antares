import { RenderResult } from '@testing-library/vue';
import Component from '@/components/Locus/GravWaveEvents.vue';
import locusMock from '../../mocks/locus-detail-deserialized.json';
import { renderWithPlugins } from '../../../test-utils';

const { data: LOCUS_WITH_GRAV_WAVE_EVENTS } = locusMock;

const LOCUS_WITHOUT_GRAV_WAVE_EVENTS = {
  ...locusMock,
  grav_wave_events: [],
};

const NO_GRAV_WAVE_EVENTS_MSG = 'No gravitational wave events associated to this locus';

describe('LocusGravWaveEvents component', () => {
  let result: RenderResult;
  const stubs = {
    LocusGravWaveTable: true, // Avoid LocusGravWaveTable rendering
  };

  describe('when component receives a locus without grav wave events', () => {
    beforeEach(() => {
      result = renderWithPlugins(Component, {
        props: {
          locus: LOCUS_WITHOUT_GRAV_WAVE_EVENTS,
          alerts: [],
        },
        stubs,
      });
    });

    it('displays a message indicating there are no grav wave events', () => {
      expect(result.queryByText(NO_GRAV_WAVE_EVENTS_MSG)).toBeTruthy();
    });
  });

  describe('when component receives a locus with grav wave events', () => {
    beforeEach(() => {
      result = renderWithPlugins(Component, {
        props: {
          locus: LOCUS_WITH_GRAV_WAVE_EVENTS,
          alerts: [],
        },
        stubs,
      });
    });

    it('does not display message indicating there are no grav wave events', () => {
      expect(result.queryByText(NO_GRAV_WAVE_EVENTS_MSG)).not.toBeTruthy();
    });

    it('displays the grav wave event ID for each event', () => {
      LOCUS_WITHOUT_GRAV_WAVE_EVENTS.grav_wave_events.forEach((eventId) => {
        expect(result.queryByText(eventId)).toBeTruthy();
      });
    });
  });
});
