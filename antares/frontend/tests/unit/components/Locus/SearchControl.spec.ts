import { fireEvent, RenderResult } from '@testing-library/vue';
import Component from '@/components/Locus/SearchControl.vue';
import { renderWithPlugins } from '../../../test-utils';

describe('LocusSearchControl component', () => {
  let result: RenderResult;

  describe('when component receives errored prop as true', () => {
    const ERROR_MESSAGE = 'Test error message';
    const ERROR_TITLE = 'There was an error';
    beforeEach(() => {
      result = renderWithPlugins(Component, {
        props: {
          errored: true,
          errorMessage: ERROR_MESSAGE,
          filters: [],
        },
        store: {
          getters: {
            lociStatistics: () => null,
          },
        },
      });
    });

    it('displays a paragraph indicating there are errors', () => {
      expect(result.queryByText(ERROR_TITLE, { exact: false })).toBeTruthy();
    });

    it('displays the errorMessage passed as prop ', () => {
      expect(result.queryByText(ERROR_MESSAGE, { exact: false })).toBeTruthy();
    });
  });

  describe('gravWaveEventId filter', () => {
    const APPLY_FILTERS_BUTTON_TEXT = 'Apply Filters';

    describe('when there are no active grav_wave_events filters', () => {
      beforeEach(() => {
        result = renderWithPlugins(Component, {
          props: {
            filters: [],
          },
          store: {
            getters: {
              lociStatistics: () => null,
            },
          },
        });
      });

      it('does not display the button for applying filters', () => {
        expect(result.queryByText(APPLY_FILTERS_BUTTON_TEXT)).toBeNull();
      });

      describe('when filling input with a specific grav wave event ID', () => {
        it('displays the button for applying filters', async () => {
          const { container } = result;
          const gravWaveEventIdInput = container.querySelector('.grav-wave-event-id-input');
          await fireEvent.update(gravWaveEventIdInput || container, 'MS1234');
          expect(result.queryByText(APPLY_FILTERS_BUTTON_TEXT)).toBeTruthy();
        });
      });
    });

    describe('when there are active grav_wave_events filters', () => {
      const GRAV_WAVE_EVENT_ID = 'MS1234';
      const TEST_FILTER = {
        field: 'grav_wave_events.keyword',
        text: `GraceDB ID: ${GRAV_WAVE_EVENT_ID}`,
        type: 'wildcard',
        value: `${GRAV_WAVE_EVENT_ID}*`,
      };
      const ACTIVE_FILTERS = [TEST_FILTER];

      beforeEach(() => {
        result = renderWithPlugins(Component, {
          props: {
            filters: ACTIVE_FILTERS,
          },
          store: {
            getters: {
              lociStatistics: () => null,
            },
          },
        });
      });

      it('displays a section with active filters', () => {
        expect(result.queryByText('Active Filters')).toBeTruthy();
      });

      it('displays each active filter using its text property', () => {
        ACTIVE_FILTERS.forEach((filter) => {
          expect(result.queryByText(filter.text)).toBeTruthy();
        });
      });

      it('does not display the button for applying filters', () => {
        expect(result.queryByText(APPLY_FILTERS_BUTTON_TEXT)).toBeNull();
      });

      describe('when filling input with the same grav wave event ID as the active filter', () => {
        it('does not display the button for applying filters', async () => {
          const { container } = result;
          const gravWaveEventIdInput = container.querySelector('.grav-wave-event-id-input');
          await fireEvent.update(gravWaveEventIdInput || container, GRAV_WAVE_EVENT_ID);
          expect(result.queryByText(APPLY_FILTERS_BUTTON_TEXT)).toBeNull();
        });
      });

      describe('when filling input with a different grav wave event ID than the active filter', () => {
        it('displays the button for applying filters', async () => {
          const { container } = result;
          const gravWaveEventIdInput = container.querySelector('.grav-wave-event-id-input');
          await fireEvent.update(gravWaveEventIdInput || container, `${GRAV_WAVE_EVENT_ID}4321`);
          expect(result.queryByText(APPLY_FILTERS_BUTTON_TEXT)).toBeTruthy();
        });
      });
    });
  });
});
