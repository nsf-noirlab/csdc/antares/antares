/* eslint-disable @typescript-eslint/no-var-requires */
import { BootstrapVue } from 'bootstrap-vue';
import { createLocalVue, mount } from '@vue/test-utils';
import Component from '@/components/AlertThumbnailViewer.vue';
import { AlertService } from '@/api';
import { Thumbnail } from '@/common/types';

const thumbnailData = require('../mocks/alert_thumbnails.json');

const thumbnails = { data: [] as Thumbnail[] };
thumbnails.data = thumbnailData.data.map((d: any) => ({ id: d.id, ...d.attributes }));

describe('AlertThumbnailViewer Component', () => {
  it('Loads the thumbnail', async () => {
    const localVue = createLocalVue();

    localVue.use(BootstrapVue);

    jest.spyOn(AlertService, 'listThumbnails').mockImplementation(async () => Promise.resolve(thumbnails));

    const wrapper = await mount(Component, {
      propsData: {
        alertId: 'LordFarquad',
      },
      // Preloading the data here since it will error out
      // because of the async nature. This is just to get the stupid
      // thing to render.
      data() {
        return {
          thumbnails: thumbnails.data,
        };
      },
      localVue,
    });

    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick(); // to make sure the thing is fully rendered
    const containers = wrapper.findAll('.base-image-container');
    expect(containers.length).toBe(3);
    for (let x = 0; x < containers.length; x += 1) {
      const item = wrapper.find(`img[src='${thumbnails.data[x].src}']`);
      expect(item.exists()).toBe(true);
    }
    // console.log(wrapper.html());
  });
});
