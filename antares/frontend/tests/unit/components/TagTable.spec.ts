/* eslint-disable @typescript-eslint/no-var-requires */
import { BootstrapVue } from 'bootstrap-vue';
import { createLocalVue, mount } from '@vue/test-utils';
import Component from '@/components/TagTable.vue';
import { TagService } from '@/api';

const tags = require('../mocks/tags.json');

describe('TagTable component', () => {
  const localVue = createLocalVue();

  localVue.use(BootstrapVue);

  it('loads the component', async () => {
    jest.spyOn(TagService, 'list').mockImplementation(() => Promise.resolve(tags));

    const wrapper = mount(Component, {
      localVue,
    });

    // ensure the page fully renders before trying to test
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();
    // console.log(wrapper.html());

    expect(wrapper.vm.$data.count).toBe(24);
    expect(wrapper.vm.$data.loading).toBe(false);

    const tr = wrapper.findAll('tbody > tr');
    expect(tr.length).toBe(5); // shows 5 entries by default
  });
});
