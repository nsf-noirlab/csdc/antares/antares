import { render } from '@testing-library/vue';
import Component from '@/components/AladinViewer.vue';

describe('AladinViewer component', () => {
  const { container } = render(Component);
  it('Module has loaded', () => {
    const script = container.getElementsByTagName('script');
    expect(script.length).toEqual(1);
  });
});
