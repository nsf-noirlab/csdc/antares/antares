/* eslint-disable @typescript-eslint/no-var-requires */
import { BootstrapVue } from 'bootstrap-vue';
import { createLocalVue, mount } from '@vue/test-utils';
import Component from '@/components/LocusAlertDetails.vue';

import { AlertService } from '@/api';

const alert = require('../mocks/alert_details.json');

describe('LocusAlertDetails Component', () => {
  const localVue = createLocalVue();
  localVue.use(BootstrapVue);

  it('Loads the component', async () => {
    jest.spyOn(AlertService, 'get').mockImplementation(() => Promise.resolve(alert));

    const wrapper = mount(Component, {
      localVue,
      propsData: {
        alertId: 'FakeAlertId',
      },
    });
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();

    const tbody = wrapper.find('tbody');
    expect(tbody.exists()).toBe(true);

    const rows = tbody.findAll('tr');
    expect(rows.length).toBe(alert.meta.count);
  });
});
