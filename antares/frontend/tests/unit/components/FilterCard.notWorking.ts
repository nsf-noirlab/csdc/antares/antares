import { BootstrapVue } from 'bootstrap-vue';
import { createLocalVue, mount } from '@vue/test-utils';
import Component from '@/components/FilterCard.vue';

import { FilterVersion } from '@/common/types';
import { FilterService } from '@/api';

require('@/plugins/fontawesome-vue');

const version = {
  data: {
    id: 'someFilterVersionId',
    code: 'someFilterCode',
    comment: 'someFilterComment',
    created_at: new Date('01-01-2022').toISOString(),
    filter: {
      name: 'CoolFilter',
      description: 'Filter desc',
      public: true,
      versions: [] as FilterVersion[],
    },
  },
  meta: {
    count: 0,
  },
};

describe('FilterCard Component', () => {
  it('Loads the filtercard', () => {
    const localVue = createLocalVue();
    localVue.use(BootstrapVue);

    jest.spyOn(FilterService, 'getEnabledVersion').mockImplementation(async (filterId: string) => {
      console.log('filterId: ', filterId);
      return Promise.resolve(version);
    });

    document.body.innerHTML = `
      <div>
        <h1>Non Vue app</h1>
        <div id="app"></div>
      </div>
    `;

    const wrapper = mount(Component, {
      propsData: {
        filter: {
          id: 'someFilterVersionId',
        },
      },
      data() {
        return {
          enabledVersion: version.data,
        };
      },
      localVue,
    });
    console.log(wrapper.html());
    expect(3).toBe(3);
  });
});
