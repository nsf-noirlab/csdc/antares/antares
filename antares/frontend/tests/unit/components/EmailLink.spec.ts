import Vue from 'vue';
import {
  fireEvent, RenderOptions, RenderResult,
} from '@testing-library/vue';
import Component from '@/components/EmailLink.vue';
import { renderWithPlugins } from '../../test-utils';

const renderEmailLink = <V extends Vue, S>(
  email: string,
  options?: RenderOptions<V, S>,
): RenderResult => (
    renderWithPlugins(Component, {
      ...options,
      props: {
        ...(options?.props || {}),
        email,
      },
    })
  );

describe('EmailLink component', () => {
  const email = 'email@example.org';

  describe('content', () => {
    describe('when component has children', () => {
      it('displays children content', () => {
        const childrenContent = 'Any content';
        const { queryByText } = renderEmailLink(email, {
          slots: {
            default: childrenContent,
          },
        });
        expect(queryByText(childrenContent)).not.toBeNull();
      });
    });

    describe('when component does not have children', () => {
      it('does not display email text', () => {
        const { queryByText } = renderEmailLink(email);
        expect(queryByText(email)).toBeNull();
      });
    });
  });

  describe('link behavior', () => {
    describe('when initially rendered', () => {
      it('has an obfuscated email as href', () => {
        const { container } = renderEmailLink(email);
        const emailElem = container.querySelector('[href]');
        expect(emailElem?.getAttribute('href')).not.toContain(email);
      });
    });

    describe('when hovering over the link', () => {
      it('reveals actual email as href', async () => {
        const { container } = renderEmailLink(email);
        const emailElem = container.querySelector('[href]');
        await fireEvent.mouseOver(emailElem || container);
        expect(emailElem?.getAttribute('href')).toContain(email);
      });
    });
  });
});
