/* eslint-disable @typescript-eslint/no-var-requires */
import { BootstrapVue } from 'bootstrap-vue';
import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import Component from '@/components/LocusPropertyTable.vue';
import axios from 'axios';
import locusProperty from '@/api/services/locusProperty';

const apiConfig = require('../mocks/config.json');
const locusProperties = require('../mocks/locus_properties.json');

jest.mock('axios');

describe('LocusPropertyTable Component', () => {
  const localVue = createLocalVue();

  localVue.use(Vuex);
  localVue.use(BootstrapVue);

  it('Loads the table', async () => {
    (axios.get as jest.MockedFunction<any>).mockImplementation(async (url: string) => {
      // console.log(url);
      switch (url) {
        case '/config.json':
          // console.log('calling config.json');
          return Promise.resolve(apiConfig);
        default:
          return Promise.reject(new Error('not found'));
      }
    });

    jest.spyOn(locusProperty, 'list').mockImplementation(() => locusProperties);

    const wrapper = mount(Component, {
      localVue,
    });

    await wrapper.vm.$nextTick(); // One to render
    await wrapper.vm.$nextTick(); // two for the async rendering of the table
    // console.log(wrapper.html());
    const table = wrapper.find('table');
    expect(table.exists()).toBe(true);
    const body = table.find('tbody');
    expect(body.exists()).toBe(true);
    expect(body.find('tr').exists()).toBe(true);
  });
});
