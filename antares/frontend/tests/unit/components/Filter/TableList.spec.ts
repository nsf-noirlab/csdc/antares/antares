import { RenderOptions, RenderResult, waitFor } from '@testing-library/vue';
import { LIST_STATUS } from '@/common/constants';
import Component from '@/components/Filter/TableList.vue';
import { User } from '@/common/types';
import { renderWithPlugins } from '../../../test-utils';
import filtersMock from '../../mocks/filters-deserialized.json';

const renderFilterTableList = <V extends Vue, S>(options?: RenderOptions<V, S>): RenderResult => (
  renderWithPlugins(Component, {
    ...options,
    props: {
      fetchFilters: () => Promise.resolve(null),
      showAdminView: false,
      toggleFilterVisibility: () => Promise.resolve(true),
      visibilityErrorMsg: '',
      ...(options?.props || {}),
    },
  })
);

const FILTER_LIST_MOCKS = {
  empty: {
    data: [],
    meta: {
      count: 0,
    },
  },
  nonEmpty: filtersMock,
};

const ERROR_TITLE = 'Error Loading Filters';

describe('FilterTableList Component', () => {
  describe(`when status prop is ${LIST_STATUS.LOADING}`, () => {
    it('displays a content loader', () => {
      const { container } = renderFilterTableList({
        props: {
          status: LIST_STATUS.LOADING,
        },
      });
      const loadingElem = container.querySelector('svg');
      expect(loadingElem).toBeTruthy();
    });
  });

  describe(`when status prop is ${LIST_STATUS.ERROR}`, () => {
    let result: RenderResult;

    beforeEach(async () => {
      result = renderFilterTableList({
        props: {
          status: LIST_STATUS.ERROR,
        },
      });
    });

    it('displays title indicating there are errors', () => {
      expect(result.queryByText(ERROR_TITLE)).toBeTruthy();
    });

    it('displays error as a code block', () => {
      const codeElem = result.container.querySelector('pre');
      expect(codeElem).toBeTruthy();
    });
  });

  describe(`when status prop is ${LIST_STATUS.EMPTY}`, () => {
    it('displays a table without rows', () => {
      const { container } = renderFilterTableList({
        props: {
          status: LIST_STATUS.EMPTY,
        },
      });
      const rowElems = container.querySelectorAll('table tbody tr');
      expect(rowElems).toHaveLength(0);
    });
  });

  describe(`when status prop is ${LIST_STATUS.SUCCESS}`, () => {
    let result: RenderResult;

    describe('when showAdminView prop is true', () => {
      beforeEach(() => {
        result = renderFilterTableList({
          props: {
            showAdminView: true,
            status: LIST_STATUS.SUCCESS,
          },
        });
      });

      it('displays "Author" column', () => {
        expect(result.queryByText('Author')).toBeTruthy();
      });
    });

    describe('when showAdminView prop is false', () => {
      beforeEach(() => {
        result = renderFilterTableList({
          props: {
            showAdminView: false,
            status: LIST_STATUS.SUCCESS,
          },
        });
      });

      it('does not display "Author" column', () => {
        expect(result.queryByText('Author')).toBeNull();
      });
    });

    describe('when fetchFilters prop function returns no records', () => {
      it('displays a table without rows', async () => {
        const { container } = renderFilterTableList({
          props: {
            fetchFilters: () => Promise.resolve(FILTER_LIST_MOCKS.empty),
            status: LIST_STATUS.SUCCESS,
          },
        });
        await waitFor(() => {
          const rowElems = container.querySelectorAll('table tbody tr');
          expect(rowElems).toHaveLength(0);
        });
      });
    });

    describe('when fetchFilters prop function returns records', () => {
      it('displays a table with the same amount of rows as records', async () => {
        const { container } = renderFilterTableList({
          props: {
            fetchFilters: () => Promise.resolve(FILTER_LIST_MOCKS.nonEmpty),
            status: LIST_STATUS.SUCCESS,
          },
        });
        await waitFor(() => {
          const rowElems = container.querySelectorAll('table tbody tr');
          expect(rowElems).toHaveLength(FILTER_LIST_MOCKS.nonEmpty.meta.count);
        });
      });

      describe('"Author" column', () => {
        const props = {
          showAdminView: true,
          status: LIST_STATUS.SUCCESS,
        };

        describe('when a filter row has no author', () => {
          beforeEach(async () => {
            const [firstFilter, ...restFilters] = FILTER_LIST_MOCKS.nonEmpty.data;
            const updatedFilter = {
              ...firstFilter,
              owner: null,
            };
            const deserializedFilters = {
              ...FILTER_LIST_MOCKS.nonEmpty,
              data: [
                updatedFilter,
                ...restFilters,
              ],
            };
            result = renderFilterTableList({
              props: {
                ...props,
                fetchFilters: () => Promise.resolve(deserializedFilters),
              },
            });
          });

          it('displays a blank cell', async () => {
            await waitFor(() => {
              const authorColumn = result.container.querySelector('table tbody tr td:nth-child(2)');
              expect(authorColumn?.textContent).toBe('');
            });
          });
        });

        describe('when a filter row has an author without name', () => {
          let ownerWithoutName : User;

          beforeEach(async () => {
            const [firstFilter, ...restFilters] = FILTER_LIST_MOCKS.nonEmpty.data;
            ownerWithoutName = {
              ...firstFilter.owner,
              name: undefined,
            };
            const updatedFilter = {
              ...firstFilter,
              owner: ownerWithoutName,
            };
            const deserializedFilters = {
              ...FILTER_LIST_MOCKS.nonEmpty,
              data: [
                updatedFilter,
                ...restFilters,
              ],
            };
            result = renderFilterTableList({
              props: {
                ...props,
                fetchFilters: () => Promise.resolve(deserializedFilters),
              },
            });
          });

          it('displays only email', async () => {
            await waitFor(() => {
              const authorColumn = result.container.querySelector('table tbody tr td:nth-child(2)');
              expect(authorColumn?.textContent).toBe(ownerWithoutName.email);
            });
          });
        });

        describe('when a filter row has an author with name', () => {
          let ownerWithName : User;

          beforeEach(async () => {
            const [firstFilter] = FILTER_LIST_MOCKS.nonEmpty.data;
            ownerWithName = firstFilter.owner;
            result = renderFilterTableList({
              props: {
                ...props,
                fetchFilters: () => Promise.resolve(FILTER_LIST_MOCKS.nonEmpty),
              },
            });
          });

          it('displays name and email in parentheses', async () => {
            await waitFor(() => {
              const authorColumn = result.container.querySelector('table tbody tr td:nth-child(2)');
              expect(authorColumn?.textContent).toContain(ownerWithName.name);
              expect(authorColumn?.textContent).toContain(`(${ownerWithName.email})`);
            });
          });
        });
      });

      describe('"Enabled" column', () => {
        const props = {
          status: LIST_STATUS.SUCCESS,
        };

        describe('when a filter row is not enabled', () => {
          beforeEach(() => {
            result = renderFilterTableList({
              props: {
                ...props,
                fetchFilters: () => Promise.resolve(FILTER_LIST_MOCKS.nonEmpty),
              },
            });
          });

          it('displays "No"', async () => {
            await waitFor(() => {
              const enabledColumn = result.container.querySelector('table tbody tr td:nth-child(6)');
              expect(enabledColumn?.textContent).toContain('No');
            });
          });
        });

        describe('when a filter row is enabled', () => {
          const enabledFields = {
            enabled: true,
            enabled_version: {
              id: '2',
              type: 'filter_version',
            },
          };

          beforeEach(async () => {
            const [firstFilter, ...restFilters] = FILTER_LIST_MOCKS.nonEmpty.data;
            const updatedFilter = {
              ...firstFilter,
              ...enabledFields,
            };
            const deserializedFilters = {
              ...FILTER_LIST_MOCKS.nonEmpty,
              data: [
                updatedFilter,
                ...restFilters,
              ],
            };
            result = renderFilterTableList({
              props: {
                ...props,
                fetchFilters: () => Promise.resolve(deserializedFilters),
              },
            });
          });

          it('displays "Yes" and the enabled version ID', async () => {
            await waitFor(() => {
              const enabledColumn = result.container.querySelector('table tbody tr td:nth-child(6)');
              expect(enabledColumn?.textContent).toContain('Yes');
              expect(enabledColumn?.textContent).toContain(`Version #${enabledFields.enabled_version.id}`);
            });
          });
        });
      });
    });
  });

  /* TODO: implement interaction tests
    Due to errors while interacting programmatically with the component, this is not yet
    implemented. Still, the behavior descriptions are depicted here so it serves as a guideline
    when there is time to implement it. The whole set of tests are skipped for now.
    An alternative is to test these interactions in e2e tests
  */
  describe.skip('User interaction', () => {
    describe('Public field interaction', () => {
      describe('when filter is private and public switch is clicked', () => {
        it('displays a confirmation modal with an input field to confirm the action', async () => {
          expect(true).toBe(true);
        });

        describe('when action is canceled', () => {
          it('closes confirmation modal', () => {
            expect(true).toBe(true);
          });

          it('displays field with private state', () => {
            expect(true).toBe(true);
          });
        });

        describe('when action is confirmed', () => {
          it('closes confirmation modal', () => {
            expect(true).toBe(true);
          });

          describe('when toggleFilterVibility prop function returns true', () => {
            it('displays field with public state', () => {
              expect(true).toBe(true);
            });
          });

          describe('when toggleFilterVibility prop function returns false', () => {
            it('displays field with private state', () => {
              expect(true).toBe(true);
            });
          });
        });
      });

      describe('when filter is public and public switch is clicked', () => {
        it('displays a confirmation modal with an input field to confirm the action', async () => {
          expect(true).toBe(true);
        });

        describe('when action is canceled', () => {
          it('closes confirmation modal', () => {
            expect(true).toBe(true);
          });

          it('displays field with public state', () => {
            expect(true).toBe(true);
          });
        });

        describe('when action is confirmed', () => {
          it('closes confirmation modal', () => {
            expect(true).toBe(true);
          });

          describe('when toggleFilterVibility prop function returns true', () => {
            it('displays field with private state', () => {
              expect(true).toBe(true);
            });
          });

          describe('when toggleFilterVibility prop function returns false', () => {
            it('displays field with public state', () => {
              expect(true).toBe(true);
            });
          });
        });
      });
    });
  });
});
