import { RenderResult } from '@testing-library/vue';
import Component from '@/components/Filter/VersionsSidebar.vue';
import filterVersionsMock from '../../mocks/filter-versions-deserialized.json';
import { renderWithPlugins } from '../../../test-utils';

const { data: versions } = filterVersionsMock;

describe('FilterVersionsSidebar component', () => {
  let result: RenderResult;

  describe('when component receives an empty list as versions', () => {
    const NO_VERSIONS_TEXT = 'No versions to show';

    beforeEach(() => {
      result = renderWithPlugins(Component, {
        props: {
          versions: [],
        },
      });
    });

    it('does not display the shortcut buttons', () => {
      const shortcutButtons = result.container.querySelectorAll('.versions-sidebar .btn-group button');
      expect(shortcutButtons).toHaveLength(0);
    });

    it('displays a message indicating there are no filter versions', () => {
      expect(result.queryByText(NO_VERSIONS_TEXT, { exact: false })).toBeTruthy();
    });
  });

  describe('when component receives a non empty list as versions', () => {
    describe('shortcut buttons', () => {
      describe('when latestVersionId prop is present', () => {
        const latestVersionId = versions[0].id;

        it('displays the "Latest" shortcut button', () => {
          result = renderWithPlugins(Component, {
            props: {
              latestVersionId,
              versions,
            },
          });
          expect(result.queryByText('Latest')).toBeTruthy();
        });

        describe('when selectedVersionId prop is the same as latestVersionId', () => {
          const selectedVersionId = latestVersionId;

          it('displays the "Latest" shortcut button as marked', () => {
            result = renderWithPlugins(Component, {
              props: {
                latestVersionId,
                selectedVersionId,
                versions,
              },
            });

            const latestButton = result.container.querySelector('.versions-sidebar .btn-group button:first-child');
            expect(result.queryByText('Latest ✓')).toBeTruthy();
            expect(latestButton?.getAttribute('disabled')).toBe('disabled');
          });
        });
      });

      describe('when enabledVersionId prop is present', () => {
        const enabledVersionId = versions[0].id;

        it('displays the "Enabled" shortcut button', () => {
          result = renderWithPlugins(Component, {
            props: {
              enabledVersionId,
              versions,
            },
          });
          expect(result.queryByText('Enabled')).toBeTruthy();
        });

        describe('when selectedVersionId prop is the same as enabledVersionId', () => {
          const selectedVersionId = enabledVersionId;

          it('displays the "Enabled" shortcut button as marked', () => {
            result = renderWithPlugins(Component, {
              props: {
                enabledVersionId,
                selectedVersionId,
                versions,
              },
            });
            const enabledButton = result.container.querySelector('.versions-sidebar .btn-group button:last-child');
            expect(result.queryByText('Enabled ✓')).toBeTruthy();
            expect(enabledButton?.getAttribute('disabled')).toBe('disabled');
          });
        });
      });
    });

    describe('versions list', () => {
      it('displays the same amount of items as the amount of the versions prop', () => {
        result = renderWithPlugins(Component, {
          props: {
            versions,
          },
        });
        const versionItems = result.container.querySelectorAll('.versions-sidebar .list-group-item');
        const versionItemsTitles = result.queryAllByText('Version #', { exact: false });
        expect(versionItems).toHaveLength(versions.length);
        expect(versionItemsTitles).toHaveLength(versions.length);
      });

      describe('when selectedVersionId prop is present', () => {
        const selectedVersionId = versions[0].id;

        it('highlights the version item corresponding to the selected version', () => {
          result = renderWithPlugins(Component, {
            props: {
              selectedVersionId,
              versions,
            },
          });
          const activeVersionItem = result.container.querySelector('.versions-sidebar .list-group-item.active');
          expect(activeVersionItem?.textContent).toMatch(`Version #${selectedVersionId}`);
        });
      });
    });

    describe('disabled prop', () => {
      const versionId = versions[0].id;
      describe('when component receives disabled prop as true', () => {
        it('displays sidebar buttons disabled', () => {
          result = renderWithPlugins(Component, {
            props: {
              disabled: true,
              enabledVersionId: versionId,
              latestVersionId: versionId,
              versions,
            },
          });
          const enabledButtons = result.container.querySelectorAll('.versions-sidebar button:not(:disabled)');
          const disabledButtons = result.container.querySelectorAll('.versions-sidebar button:disabled');
          expect(enabledButtons).toHaveLength(0);
          expect(disabledButtons).toHaveLength(versions.length + 2); // Version items + 2 shortcuts
        });
      });

      describe('when component receives disabled prop as false', () => {
        it('displays sidebar buttons enabled', () => {
          result = renderWithPlugins(Component, {
            props: {
              disabled: false,
              versions,
            },
          });
          const enabledButtons = result.container.querySelectorAll('.versions-sidebar button:not(:disabled)');
          const disabledButtons = result.container.querySelectorAll('.versions-sidebar button:disabled');
          expect(enabledButtons).toHaveLength(versions.length); // Only version items
          expect(disabledButtons).toHaveLength(0); // No shortcuts
        });
      });
    });
  });
});
