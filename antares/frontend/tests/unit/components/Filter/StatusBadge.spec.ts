import Component from '@/components/Filter/StatusBadge.vue';
import { renderWithPlugins } from '../../../test-utils';

describe('FilterStatusBadge component', () => {
  describe('when component receives a status prop with an uppercase single word', () => {
    it('displays badge with lowercase word', () => {
      const { queryByText } = renderWithPlugins(Component, {
        props: {
          status: 'SINGLE',
        },
      });
      expect(queryByText('single')).toBeTruthy();
    });
  });

  describe('when component receives a status prop with an uppercase multi word separated by underscores', () => {
    it('displays badge with lowercase words separated by blank spaces', () => {
      const { queryByText } = renderWithPlugins(Component, {
        props: {
          status: 'MULTI_WORD',
        },
      });
      expect(queryByText('multi word')).toBeTruthy();
    });
  });
});
