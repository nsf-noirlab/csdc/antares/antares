/* eslint-disable @typescript-eslint/no-var-requires */
import { BootstrapVue } from 'bootstrap-vue';
import { createLocalVue, mount } from '@vue/test-utils';
import Component from '@/components/LocusProperties.vue';

const locusProps = require('../mocks/loci_properties.json');

describe('LocusProperties Component', () => {
  const localVue = createLocalVue();
  localVue.use(BootstrapVue);

  it('Loads the component', async () => {
    // disable console error: complains about prop data type
    console.error = () => null;

    const wrapper = mount(Component, {
      localVue,
      propsData: {
        locusId: 'someLocusId',
        locusPropertiesKVs: locusProps.properties,
      },
    });

    const tbody = wrapper.find('tbody');
    expect(tbody.exists()).toBe(true);

    const rows = tbody.findAll('tr');
    expect(rows.length).toBe(5);
  });
});
