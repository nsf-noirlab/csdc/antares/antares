import { BootstrapVue } from 'bootstrap-vue';
import Vuex from 'vuex';
import { createLocalVue, mount } from '@vue/test-utils';
import Component from '@/components/LocusButtonComment.vue';

require('@/plugins/fontawesome-vue');

describe('LocusButtonComment Component', () => {
  const localVue = createLocalVue();
  let store: any;
  const getters = {
    isAuthenticated: () => true,
  };

  localVue.use(Vuex);
  localVue.use(BootstrapVue);
  //  localVue.component('font-awesome-icon', FontAwesomeIcon);
  //  localVue.use(IconsPlugin);

  it('Locus annotation is enabled', () => {
    store = new Vuex.Store({ getters });
    // Bootstrap complains about tooltip target
    // not being valid and not validating prop
    // value 'size'
    // disabling warnings for clean output.
    console.warn = () => null;
    console.error = () => null;

    const wrapper = mount(
      Component,
      {
        mocks: {
          $store: store,
        },
        propsData: {
          size: 'md',
        },
        localVue,
      },
    );
    const div = wrapper.find('div');

    expect(div.exists()).toBe(true);

    expect(div.attributes().title).toBe('Annotate this locus');
  });

  it('Locus annotation is disabled', async () => {
    getters.isAuthenticated = () => false;

    store = new Vuex.Store({ getters });

    const wrapper = mount(
      Component,
      {
        mocks: {
          $store: store,
        },
        propsData: {
          color: 'blue',
          size: 'md',
        },
        localVue,
      },
    );

    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();

    const div = wrapper.find('div');

    expect(div.exists()).toBe(true);

    expect(div.attributes().title).toBe('Login to annotate this locus');

    // console.log(wrapper.html());
  });
});
