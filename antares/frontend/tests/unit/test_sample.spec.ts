import { render } from '@testing-library/vue';
import { mount, createLocalVue } from '@vue/test-utils';
import Component from '@/components/TestComponent.vue';
import { BootstrapVue } from 'bootstrap-vue';

describe('Test Sample', () => {
  const { container, queryByText } = render(Component);
  const localVue = createLocalVue();

  localVue.use(BootstrapVue);

  it('Has 1 H1 tag', () => {
    const h1 = container.querySelectorAll('h1');
    // debug();
    expect(h1.length).toEqual(1);
  });

  it('Hides "Hidden value"', () => {
    const hidden = queryByText('Hidden value');
    expect(hidden).toEqual(null);
  });

  it('loads a table', async () => {
    const wrapper = mount(Component, { localVue });
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();
    // expect(wrapper.html()).toMatchSnapshot();
  });
});
