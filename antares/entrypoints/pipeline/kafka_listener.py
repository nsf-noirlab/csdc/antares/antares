import logging
import re
import time
from datetime import datetime
from typing import Any, Optional, TypedDict

import antares.exceptions
from antares.adapters.concurrency import AbstractDistributedLockFactory
from antares.adapters.messages import KafkaMessageSubscriptionService
from antares.adapters.repository import AbstractLocusRepository
from antares.bootstrap import bootstrap
from antares.entrypoints.pipeline import stages
from antares.observability.metrics.base import AbstractMetrics
from antares.observability.tracing.base import AbstractTracer
from antares.utils import spatial


class SurveyTopic(TypedDict):
    name: str
    topic: str
    watchlists_on: bool
    filters_on: bool


def process_survey_message(
    survey: SurveyTopic,
    message: Any,
    locus_repository: AbstractLocusRepository,
    lock_factory: AbstractDistributedLockFactory,
    tracer: AbstractTracer,
    lock_ttl: float,
):
    with tracer.trace("process_survey_message"):
        alert_packet = stages.load_alert_packet(survey["name"], message, tracer=tracer)
        grav_wave_events_metadata = stages.add_grav_wave_events_to_alert_packet(
            alert_packet
        )
        locus = stages.ingest_alert_packet(alert_packet)
        stages.add_grav_wave_events_to_locus(alert_packet, locus)

        logging.info("Process locus %s, passed L1: %s", locus.id, alert_packet.is_good)
        if not alert_packet.is_good:
            return
        with lock_factory(f"{locus.id}", lock_ttl):
            alert_history, timeseries = stages.update_locus_history(locus)
            catalog_objects = stages.crossmatch_locus_with_catalogs(locus)
            if survey["watchlists_on"]:
                watch_objects = stages.crossmatch_locus_with_watchlists(locus)
            if survey["filters_on"]:
                stages.run_user_filters(
                    locus,
                    alert_history,
                    timeseries,
                    catalog_objects,
                    grav_wave_events_metadata,
                )
            locus_repository.update(locus)
        locus.htm16 = spatial.get_htm_id(locus.location, 16)
        if survey["watchlists_on"]:
            for watch_object in watch_objects:
                stages.send_watchlist_notification(locus, watch_object)

        stages.update_search_index(locus)
        # Publishing the locus to users should be the last step because
        # that way we don't send multiple times the same locus if an error
        # happens and we have to reprocess the message
        # Same applies to our indexer
        stages.publish_locus_to_users(locus)


def process_kafka_messages(
    kafka_subscription: KafkaMessageSubscriptionService,
    surveys_topics: list[SurveyTopic],
    locus_repository: AbstractLocusRepository,
    lock_factory: AbstractDistributedLockFactory,
    tracer: AbstractTracer,
    metrics: AbstractMetrics,
    lock_ttl: float,
    limit: Optional[int] = None,
    timeout: Optional[float] = 300.0,
    retry_interval_seconds: float = 0.5,
) -> int:
    """
    Poll for and process a batch of ZTF alert packets from Kafka.

    Parameters
    ----------
    kafka_subscription: KafkaMessageSubscriptionService
    surveys_topics: list[SurveyTopic]:
        specifies the topic for each survey; for more topics in the same survey, add another SurveyTopic with the same
        topic name, but different survey
    limit: Optional[int] (default, None)
        Process at most `limit` messages before returning.
    timeout: Optional[float] (default, 300.0)
        Block while polling for a new message for up to `timeout` seconds. If None,
        block indefinitely. This function will catch the antares.exceptions.Timeout
        raised by the Kafka polling library and return.
    retry_interval_seconds: float (default, 0.5)
        Time to delay retrying processing of an alert in the event of an exception.

    Returns
    ----------
    int
        Number of alert packets received and successfully processed.

    Notes
    ----------
    Currently this entrypoint expects to only receive messages from ZTF's Kafka broker
    that are true ZTF alert packets. We might extend this in the future to handle any
    arbitrary message format and such an architecture might register tuples like:
    (topic_name, Event) that will create and dispatch event(s) on the message bus
    when it receives a message.
    """
    count = 0
    while True:
        if not limit or count < limit:
            # TODO: Handle other Kafka exceptions (e.g. topic does not exist)
            with tracer.trace("poll_kafka") as span:
                try:
                    topic, message, published_at = kafka_subscription.poll(timeout)
                    span.set_attribute("topic", topic)
                except antares.exceptions.Timeout as e:
                    span.set_error("Timeout waiting for Kafka", e)
                    logging.info("Timeout waiting for messages from Kafka")
                    return count
            # (2022-04-14, Band-aid)
            # We use a broad try/except clause here in order to retry message
            # processing on failure. The ZTF Kafka broker doesn't support static
            # partition assignment which can lead to cascading delays in alert
            # processing. By handling the retry logic at application-level we
            # allow this consumer to keep its partition assignment and try again.
            while True:
                with tracer.trace("process_kafka_message") as span:
                    try:
                        span.set_attribute("topic", topic)
                        # Survey topics shouldn't contain regular expressions that matches other survey topics because that can lead to errors
                        survey = next(
                            (
                                survey_topic
                                for survey_topic in surveys_topics
                                if re.match(survey_topic["topic"], topic)
                            ),
                            None,
                        )
                        if not survey:
                            raise ValueError(
                                f"Survey configuration not found for topic {topic}"
                            )
                        span.set_attribute("survey", survey["name"])
                        process_survey_message(
                            survey,
                            message,
                            locus_repository,
                            lock_factory,
                            tracer,
                            lock_ttl,
                        )
                        count += 1
                        metrics.increment_pipeline_alerts_received()
                        # If we have a message with `confluent_kafka.TIMESTAMP_NOT_AVAILABLE` we can't calculate this
                        if published_at:
                            delay_sec = (
                                datetime.utcnow() - published_at
                            ).total_seconds()
                            metrics.set_pipeline_alerts_processing_lag(delay_sec, topic)
                        else:
                            logging.error("Message without published_at timestamp")
                        kafka_subscription.commit()
                        break
                    except Exception as e:
                        span.set_error("Error processing message", e)
                        logging.exception(
                            "Error processing message (retry in %fs)",
                            retry_interval_seconds,
                        )
                        time.sleep(retry_interval_seconds)
                        continue
        else:
            return count


def main():
    """
    Entrypoint for subscribing to Kafka topics and indefinitely processing messages.
    """
    container = bootstrap()

    topics = [
        survey["topic"]
        for survey in container.config.pipeline.ingestion.broker.surveys()
    ]
    logging.info(
        "Starting Kafka listener, subscribing to topics: %s", ", ".join(topics)
    )
    kafka_subscription = KafkaMessageSubscriptionService(
        container.config.pipeline.ingestion.broker.settings(),
        topics,
    )
    # We are running three nested while True...
    while True:
        process_kafka_messages(
            kafka_subscription,
            container.config.pipeline.ingestion.broker.surveys(),
            locus_repository=container.locus_repository(),
            tracer=container.tracer(),
            metrics=container.metrics(),
            lock_factory=container.lock_factory(),
            lock_ttl=container.config.pipeline.ingestion.lock.ttl(),
            timeout=container.config.pipeline.ingestion.broker.timeout(),
            retry_interval_seconds=container.config.pipeline.retry_interval_seconds(),
        )


if __name__ == "__main__":
    main()
