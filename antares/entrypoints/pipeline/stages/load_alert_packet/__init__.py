import io
from typing import Any

import fastavro

from antares.observability.tracing.base import AbstractTracer

from .base import AbstractAlertPacket
from .decat import DecatAlertPacket
from .ztf.helper import ZtfAlertPacket


def load_alert_packet(
    survey: str, message: Any, tracer: AbstractTracer
) -> AbstractAlertPacket:
    """
    Processes the message we receive from external surveys and prepares an alert packet
    for ingestion.

    Parameters
    ----------
    survey: str
    message: Any

    Returns
    -------
    AbstractAlertPacket

    """
    # TODO: Handle malformed data (e.g. send messages into DLQ if we cant deserialize them)
    if survey == "ztf":
        buffer = io.BytesIO(message)
        buffer.seek(0)
        for message in fastavro.reader(buffer):
            with tracer.trace("process_ztf_message"):
                return ZtfAlertPacket(message)
    elif survey == "decat":
        buffer = io.BytesIO(message)
        buffer.seek(0)
        for message in fastavro.reader(buffer):
            with tracer.trace("process_decat_message"):
                return DecatAlertPacket(message)
    raise ValueError(f"Survey '{survey}' not found")
