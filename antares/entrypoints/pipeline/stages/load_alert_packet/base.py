import abc
from datetime import datetime
from typing import Optional

from astropy.coordinates import SkyCoord

from antares.adapters.repository import AbstractLocusRepository
from antares.domain.models import Alert, Locus
from antares.observability.metrics.base import AbstractMetrics


class AbstractAlertPacket(abc.ABC):
    # We are not using this variable
    received_at: datetime

    def __init__(self):
        self.received_at = datetime.utcnow()

    @property
    @abc.abstractmethod
    def alerts(self) -> list[Alert]:
        """
        The alerts contained in this alert packet, normalized to the ANTARES `Alert`
        data model.

        Returns
        -------
        list[Alert]

        """
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def triggering_alert(self) -> Alert:
        """
        The primary Alert for this alert packet.
        """
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def previous_alerts(self) -> list[Alert]:
        """
        The previous alerts that comes with an alert packet.
        """
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def location(self) -> SkyCoord:
        """
        The "location" of this alert packet. Because there might be many observations in
        an alert packet, this field is survey-dependent. A concrete implementation might
        use the location of the latest alert, the centroid of all alerts in the packet
        or any other method of determining a valid location. This position is used for
        cross-matching against catalogs and loci.

        Returns
        -------
        SkyCoord

        """
        pass

    @property
    @abc.abstractmethod
    def is_good(self) -> bool:
        """
        Boolean property that indicates if this alert packet is "good" or "bad". The
        interpretation of this might vary from survey to survey but the downstream
        implication is that we might not do expensive computation on a "bad" alert
        packet (e.g. one from poor seeing conditions).

        Returns
        -------
        bool

        """
        pass

    @abc.abstractmethod
    def get_associated_locus(
        self, locus_repository: AbstractLocusRepository, metrics: AbstractMetrics
    ) -> Optional[Locus]:
        """
        This method should return the `Locus` we believe this alert packet is associated
        with or else `None` if no such locus exists in our database. The implementation
        of this method might be survey-dependent. Concrete implementations might use a
        cone search around a location, some survey-specific field stored on extant loci,
        or some combination of these approaches.

        Parameters
        ----------
        locus_repository

        Returns
        -------
        Optional[Locus]

        """
        pass
