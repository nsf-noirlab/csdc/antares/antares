"""
Notes on terminology:

* "ZTF Event"
  - ANTARES receives event notifications from ZTF when they detect a significant change
    in brightness of an object. These "ZTF Events" are packets of data that contain the
    "candidate" that triggered the notification as well as some partial history of
    "previous candidates".
* "ZTF Candidate"
* "ZTF Measurement"
* "ZTF Upper Limit"

Notes on:
To know if an alert is an upper limit you should check if the alert have candid. If it has one, the event candidate is an upper limit
"""
from datetime import datetime
from typing import Any, Dict, Optional, Union, cast

from astropy import units as u
from astropy.coordinates import Angle, SkyCoord

from antares.adapters.repository import AbstractLocusRepository
from antares.domain.models import Alert, AlertNormalizedProperties, Locus, Survey
from antares.entrypoints.pipeline.stages.load_alert_packet.base import (
    AbstractAlertPacket,
)
from antares.entrypoints.pipeline.stages.load_alert_packet.ztf.schema import (
    ZtfEvent,
    ZtfEventCandidate,
    ZtfEventPrvCandidate,
)
from antares.observability.metrics.base import AbstractMetrics
from antares.utils import get_median_coords, jd_to_mjd, nearest


def is_measurement(candidate: Union[ZtfEventCandidate, ZtfEventPrvCandidate]) -> bool:
    return candidate.get("candid") is not None


def create_alert_from_ztf_candidate(
    candidate: Union[ZtfEventCandidate, ZtfEventPrvCandidate], event: ZtfEvent
) -> Alert:
    location = None
    if is_measurement(candidate):
        location = SkyCoord(ra=candidate["ra"], dec=candidate["dec"], unit=u.deg)
    return Alert(
        id=get_alert_id_from_ztf_candidate(candidate, event["objectId"]),
        location=location,
        mjd=jd_to_mjd(candidate["jd"]),
        survey=Survey.SURVEY_ZTF,
        created_at=datetime.utcnow(),
        normalized_properties=compute_normalized_alert_properties(candidate),
        properties=compute_survey_specific_alert_properties(candidate, event),
        non_persistent_properties={"ztf_fp_hists": event.get("fp_hists", [])},
    )


ZTF_MEASUREMENT_FORMAT = "ztf_candidate:{candid}"
ZTF_UPPER_LIMIT_FORMAT = "ztf_upper_limit:{ztf_object_id}-{pid}"


def get_alert_id_from_ztf_candidate(
    candidate: Union[ZtfEventCandidate, ZtfEventPrvCandidate], ztf_object_id: str
) -> str:
    """
    Builds a unique ID for a candidate in a ZTF event.

    This logic largely exists to maintain backwards compatibility with older versions of
    ANTARES. There are two types of ZTF candidates--"measurements" and "upper limits".
    Measurements have an ID of the form "ztf_candidate:<candid>" and upper limits have
    an ID of the form "<ztf_object_id>:<candidate_pid>". The ZTF object ID is stored on
    the root of the alert packet which is why it's required as an argument.
    """
    if is_measurement(candidate):
        return ZTF_MEASUREMENT_FORMAT.format(candid=candidate["candid"])
    else:
        return ZTF_UPPER_LIMIT_FORMAT.format(
            ztf_object_id=ztf_object_id, pid=candidate["pid"]
        )


ZTF_FID_TO_ANT_PASSBAND = {1: "g", 2: "R", 3: "i"}


def compute_normalized_alert_properties(
    candidate: Union[ZtfEventCandidate, ZtfEventPrvCandidate]
) -> AlertNormalizedProperties:
    # These properties are required and guaranteed to exist on ZTF alerts
    if candidate["diffmaglim"] is None:
        raise RuntimeError(
            "Can't calculate normalized alert properties: diffmaglim property is needed."
        )
    properties = {
        "ant_mjd": jd_to_mjd(candidate["jd"]),
        "ant_time_received": round(datetime.utcnow().timestamp()),
        "ant_survey": 1 if is_measurement(candidate) else 2,
        "ant_maglim": float(candidate["diffmaglim"]),
        "ant_passband": ZTF_FID_TO_ANT_PASSBAND[candidate["fid"]],
    }
    # These properties don't exist on upper limits but exist on candidates
    if ra := candidate.get("ra"):
        properties["ant_ra"] = ra
    if dec := candidate.get("dec"):
        properties["ant_dec"] = dec
    if mag := candidate.get("magpsf"):
        properties["ant_mag"] = mag
    if magerr := candidate.get("sigmapsf"):
        properties["ant_magerr"] = magerr
    return cast(AlertNormalizedProperties, properties)


def compute_survey_specific_alert_properties(
    candidate: Union[ZtfEventCandidate, ZtfEventPrvCandidate], event: ZtfEvent
) -> Dict[str, Any]:
    properties = {
        f"ztf_{key}": value for key, value in candidate.items() if value is not None
    }
    properties["ztf_object_id"] = event["objectId"]
    return properties


def extract_alerts(event: ZtfEvent) -> list[Alert]:
    """
    Important: This method should return the triggering candidate
    in the last position of the list because the code is dependant
    to the order in which the alerts are.
    """
    alerts: list[Alert] = []
    if event["prv_candidates"]:
        for candidate in event["prv_candidates"]:
            alerts.append(create_alert_from_ztf_candidate(candidate, event))
    alerts.append(create_alert_from_ztf_candidate(event["candidate"], event))
    return alerts


class ZtfAlertPacket(AbstractAlertPacket):
    def __init__(
        self,
        event: ZtfEvent,
        locus_association_radius: Angle = Angle("1.5s"),
        locus_association_search_radius: Angle = Angle("5s"),
    ):
        super().__init__()
        self.event: ZtfEvent = event
        if locus_association_radius > locus_association_search_radius:
            raise ValueError(
                (
                    "locus_association_radius must be less than or equal to "
                    "locus_association_search_radius."
                )
            )
        self.locus_association_radius = locus_association_radius
        self.locus_association_search_radius = locus_association_search_radius
        self._alerts: list[Alert] = extract_alerts(event)

    @property
    def alerts(self) -> list[Alert]:
        return self._alerts

    @property
    def triggering_alert(self) -> Alert:
        return self.alerts[-1]

    @property
    def previous_alerts(self) -> list[Alert]:
        return self.alerts[:-1]

    @property
    def location(self) -> SkyCoord:
        ra, dec = get_median_coords(
            (alert.location.ra.deg, alert.location.dec.deg)
            for alert in self.alerts
            if alert.location is not None
        )
        return SkyCoord(ra=ra, dec=dec, unit=u.deg)

    @property
    def is_good(self) -> bool:
        """Based on https://zwickytransientfacility.github.io/ztf-avro-alert/filtering.html"""
        properties = self.triggering_alert.properties
        # Check if this alert is a bad detection
        if ("ztf_rb" in properties) and properties["ztf_rb"] < 0.55:
            return False
        if ("ztf_nbad" in properties) and properties["ztf_nbad"] != 0:
            return False
        if ("ztf_magdiff" in properties) and (
            properties["ztf_magdiff"] < -1.0 or properties["ztf_magdiff"] > 1.0
        ):
            return False
        # Check if this alert has bad seeing
        if ("ztf_fwhm" in properties) and properties["ztf_fwhm"] > 5.0:
            return False
        if ("ztf_elong" in properties) and properties["ztf_elong"] > 1.2:
            return False
        return True

    def get_associated_locus(
        self, locus_repository: AbstractLocusRepository, metrics: AbstractMetrics
    ) -> Optional[Locus]:
        """Attempt to find a locus suitable for association with this alert packet."""
        # Begin by finding all loci within some large search radius (we typically set
        # this to something like 5"). If there is nothing in this result set we return
        # None.
        nearby_loci = locus_repository.list_by_cone_search(
            self.location, self.locus_association_search_radius
        )
        nearby_loci = list(nearby_loci)
        if not nearby_loci:
            return None
        # Warn if there are multiple loci within this region as it might lead to a poor
        # association.
        if len(nearby_loci) > 1:
            metrics.increment_pipeline_multiple_nearby_loci()
        # A helper function for getting the nearest locus from a list of `loci` but not
        # outside `max_angle`.

        def get_nearest_locus(loci: list[Locus], max_angle: Angle) -> Optional[Locus]:
            return nearest(
                self.location,
                loci,
                key=lambda locus: locus.location,
                max_angle=max_angle,
            )

        # We look for an object within our the large search radius used to originally build
        # `nearby_loci` with a matching ZTF Object ID. If we find one, we consider it
        # a successful association.
        ztf_object_id = self.event["objectId"]
        locus: Optional[Locus] = get_nearest_locus(
            [
                locus
                for locus in nearby_loci
                if locus.properties.get("ztf_object_id") == ztf_object_id
            ],
            self.locus_association_search_radius,
        )
        if locus:
            return locus
        # Otherwise, just return the nearest locus to our search location. In this
        # case we use a smaller radius (typically 1.5") since we don't have something
        # like an object ID to tie the alert to this locus.
        return get_nearest_locus(nearby_loci, self.locus_association_radius)
