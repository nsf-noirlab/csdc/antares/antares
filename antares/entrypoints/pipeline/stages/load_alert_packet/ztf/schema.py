from typing import Optional, TypedDict


class ZtfEventBaseCandidate(TypedDict):
    # Observation Julian date at start of exposure [days]
    jd: float
    # Filter ID (1=g; 2=r; 3=i)
    fid: int
    # Processing ID for science image to facilitate archive retrieval
    pid: int
    # Expected 5-sigma mag limit in difference image based on global noise estimate [mag]
    diffmaglim: Optional[float]
    # Filename of positive (sci minus ref) difference image
    pdiffimfilename: Optional[str]
    # Principal investigator attached to program ID
    programpi: Optional[str]
    # Program ID: encodes either public, collab, or caltech mode
    programid: int
    # Internal pipeline table extraction ID
    tblid: Optional[int]
    # Night ID
    nid: Optional[int]
    # Readout channel ID [00 .. 63]
    rcid: Optional[int]
    # ZTF field ID
    field: Optional[int]
    # x-image position of candidate [pixels]
    xpos: Optional[float]
    # y-image position of candidate [pixels]
    ypos: Optional[float]
    # Reduced chi-square for PSF-fit
    chipsf: Optional[float]
    # Aperture mag using 14 pixel diameter aperture [mag]
    magap: Optional[float]
    # 1-sigma uncertainty in magap [mag]
    sigmagap: Optional[float]
    # Distance to nearest source in reference image PSF-catalog within 30 arcsec [pixels]
    distnr: Optional[float]
    # Magnitude of nearest source in reference image PSF-catalog within 30 arcsec [mag]
    magnr: Optional[float]
    # 1-sigma uncertainty in magnr within 30 arcsec [mag]
    sigmagnr: Optional[float]
    # DAOPhot chi parameter of nearest source in reference image PSF-catalog within 30 arcsec
    chinr: Optional[float]
    # DAOPhot sharp parameter of nearest source in reference image PSF-catalog within 30 arcsec
    sharpnr: Optional[float]
    # Local sky background estimate [DN]
    sky: Optional[float]
    # Difference: magap - magpsf [mag]
    magdiff: Optional[float]
    # Full Width Half Max assuming a Gaussian core, from SExtractor [pixels]
    fwhm: Optional[float]
    # Star/Galaxy classification score from SExtractor
    classtar: Optional[float]
    # Distance to nearest edge in image [pixels]
    mindtoedge: Optional[float]
    # Difference: diffmaglim - magap [mag]
    magfromlim: Optional[float]
    # Ratio: difffwhm / fwhm
    seeratio: Optional[float]
    # Windowed profile RMS afloat major axis from SExtractor [pixels]
    aimage: Optional[float]
    # Windowed profile RMS afloat minor axis from SExtractor [pixels]
    bimage: Optional[float]
    # Ratio: aimage / fwhm
    aimagerat: Optional[float]
    # Ratio: bimage / fwhm
    bimagerat: Optional[float]
    # Ratio: aimage / bimage
    elong: Optional[float]
    # Number of negative pixels in a 5 x 5 pixel stamp
    nneg: Optional[int]
    # Number of prior-tagged bad pixels in a 5 x 5 pixel stamp
    nbad: Optional[int]
    # RealBogus quality score from Random Forest classifier; range is 0 to 1 where closer to 1 is more reliable
    rb: Optional[float]
    # Distance to nearest known solar system object if exists within 30 arcsec; set to -999.0 if none [arcsec]
    ssdistnr: Optional[float]
    # Magnitude of nearest known solar system object if exists within 30 arcsec (usually V-band from MPC archive); set to -999.0 if none [mag]
    ssmagnr: Optional[float]
    # Name of nearest known solar system object if exists within 30 arcsec (from MPC archive); ‘null’ if none
    ssnamenr: Optional[str]
    # Ratio: sum(pixels) / sum(abs(pixels)) in a 5 x 5 pixel stamp where stamp is first median-filtered to mitigate outliers
    sumrat: Optional[float]
    # Aperture mag using 18 pixel diameter aperture [mag]
    magapbig: Optional[float]
    # 1-sigma uncertainty in magapbig [mag]
    sigmagapbig: Optional[float]
    # Peak-pixel signal-to-noise ratio in point source matched-filtered detection image
    scorr: Optional[float]
    # Magnitude zero point for photometry estimates [mag]
    magzpsci: Optional[float]
    # Magnitude zero point uncertainty (in magzpsci) [mag]
    magzpsciunc: Optional[float]
    # RMS (deviation from average) in all differences between instrumental photometry and matched photometric calibrators from science image processing [mag]
    magzpscirms: Optional[float]
    # Color coefficient from linear fit from photometric calibration of science image
    clrcoeff: Optional[float]
    # Color coefficient uncertainty from linear fit (corresponding to clrcoeff)
    clrcounc: Optional[float]
    # Version of Random Forest classifier model used to assign RealBogus (rb) quality score
    rbversion: str


class ZtfEventCandidate(ZtfEventBaseCandidate):
    # Candidate ID from operations DB
    candid: int
    # t or 1 => candidate is from positive (sci minus ref) subtraction; f or 0 => candidate is from negative (ref minus sci) subtraction
    isdiffpos: str
    # Right Ascension of candidate; J2000 [deg]
    ra: float
    # Declination of candidate; J2000 [deg]
    dec: float
    # Magnitude from PSF-fit photometry [mag]
    magpsf: float
    # 1-sigma uncertainty in magpsf [mag]
    sigmapsf: float
    # Right Ascension of nearest source in reference image PSF-catalog; J2000 [deg]
    ranr: float
    # Declination of nearest source in reference image PSF-catalog; J2000 [deg]
    decnr: float
    # g-band PSF-fit magnitude of closest source from PS1 catalog; if exists within 30 arcsec [mag]
    sgmag1: Optional[float]
    # r-band PSF-fit magnitude of closest source from PS1 catalog; if exists within 30 arcsec [mag]
    srmag1: Optional[float]
    # i-band PSF-fit magnitude of closest source from PS1 catalog; if exists within 30 arcsec [mag]
    simag1: Optional[float]
    # z-band PSF-fit magnitude of closest source from PS1 catalog; if exists within 30 arcsec [mag]
    szmag1: Optional[float]
    # Star/Galaxy score of closest source from PS1 catalog; if exists within 30 arcsec: 0 <= sgscore <= 1 where closer to 1 implies higher likelihood of being a star
    sgscore1: Optional[float]
    # Distance to closest source from PS1 catalog; if exists within 30 arcsec [arcsec]
    distpsnr1: Optional[float]
    # Number of spatially-coincident detections falling within 1.5 arcsec going back to beginning of survey; only detections that fell on the same field and readout-channel ID where the input candidate was observed are counted. All raw detections down to a photometric S/N of ~ 3 are included.
    ndethist: int
    # Number of times input candidate position fell on any field and readout-channel going back to beginning of survey
    ncovhist: int
    # Earliest Julian date of epoch corresponding to ndethist [days]
    jdstarthist: Optional[float]
    # Latest Julian date of epoch corresponding to ndethist [days]
    jdendhist: Optional[float]
    # 1 => candidate is from a Target-of-Opportunity (ToO) exposure; 0 => candidate is from a non-ToO exposure
    tooflag: Optional[int]
    # Object ID of closest source from PS1 catalog; if exists within 30 arcsec
    objectidps1: Optional[int]
    # Object ID of second closest source from PS1 catalog; if exists within 30 arcsec
    objectidps2: Optional[int]
    # g-band PSF-fit magnitude of second closest source from PS1 catalog; if exists within 30 arcsec [mag]
    sgmag2: Optional[float]
    # r-band PSF-fit magnitude of second closest source from PS1 catalog; if exists within 30 arcsec [mag]
    srmag2: Optional[float]
    # i-band PSF-fit magnitude of second closest source from PS1 catalog; if exists within 30 arcsec [mag]
    simag2: Optional[float]
    # z-band PSF magnitude of second closest source from PS1 catalog; if exists within 30 arcsec [mag]
    szmag2: Optional[float]
    # Star/Galaxy score of second closest source from PS1 catalog; if exists within 30 arcsec: 0 <= sgscore <= 1 where closer to 1 implies higher likelihood of being a star
    sgscore2: Optional[float]
    # Distance to second closest source from PS1 catalog; if exists within 30 arcsec [arcsec]
    distpsnr2: Optional[float]
    # Object ID of third closest source from PS1 catalog; if exists within 30 arcsec
    objectidps3: Optional[int]
    # g-band PSF-fit magnitude of third closest source from PS1 catalog; if exists within 30 arcsec [mag]
    sgmag3: Optional[float]
    # r-band PSF-fit magnitude of third closest source from PS1 catalog; if exists within 30 arcsec [mag]
    srmag3: Optional[float]
    # i-band PSF-fit magnitude of third closest source from PS1 catalog; if exists within 30 arcsec [mag]
    simag3: Optional[float]
    # z-band PSF-fit magnitude of third closest source from PS1 catalog; if exists within 30 arcsec [mag]
    szmag3: Optional[float]
    # Star/Galaxy score of third closest source from PS1 catalog; if exists within 30 arcsec: 0 <= sgscore <= 1 where closer to 1 implies higher likelihood of being a star
    sgscore3: Optional[float]
    # Distance to third closest source from PS1 catalog; if exists within 30 arcsec [arcsec]
    distpsnr3: Optional[float]
    # Number of source matches from PS1 catalog falling within 30 arcsec
    nmtchps: int
    # Processing ID for reference image to facilitate archive retrieval
    rfid: int
    # Observation Julian date of earliest exposure used to generate reference image [days]
    jdstartref: float
    # Observation Julian date of latest exposure used to generate reference image [days]
    jdendref: float
    # Number of frames (epochal images) used to generate reference image
    nframesref: int
    # Ratio: D/stddev(D) on event position where D = difference image
    dsnrms: Optional[float]
    # Ratio: S/stddev(S) on event position where S = image of convolution: D (x) PSF(D)
    ssnrms: Optional[float]
    # Difference of statistics: dsnrms - ssnrms
    dsdiff: Optional[float]
    # Number of PS1 photometric calibrators used to calibrate science image from science image processing
    nmatches: int
    # Covariance in magzpsci and clrcoeff from science image processing [mag^2]
    zpclrcov: Optional[float]
    # Magnitude zero point from median of all differences between instrumental photometry and matched photometric calibrators from science image processing [mag]
    zpmed: Optional[float]
    # Median color of all PS1 photometric calibrators used from science image processing [mag]: for filter (fid) = 1, 2, 3, PS1 color used = g-r, g-r, r-i respectively
    clrmed: Optional[float]
    # RMS color (deviation from average) of all PS1 photometric calibrators used from science image processing [mag]
    clrrms: Optional[float]
    # Distance to closest source from Gaia DR1 catalog irrespective of magnitude; if exists within 90 arcsec [arcsec]
    neargaia: Optional[float]
    # Distance to closest source from Gaia DR1 catalog brighter than magnitude 14; if exists within 90 arcsec [arcsec]
    neargaiabright: Optional[float]
    # Gaia (G-band) magnitude of closest source from Gaia DR1 catalog irrespective of magnitude; if exists within 90 arcsec [mag]
    maggaia: Optional[float]
    # Gaia (G-band) magnitude of closest source from Gaia DR1 catalog brighter than magnitude 14; if exists within 90 arcsec [mag]
    maggaiabright: Optional[float]
    # Integration time of camera exposure [sec]
    exptime: Optional[float]
    # RealBogus quality score from Deep-Learning-based classifier; range is 0 to 1 where closer to 1 is more reliable
    drb: Optional[float]
    # version of Deep-Learning-based classifier model used to assign RealBogus (drb) quality score
    drbversion: str


class ZtfEventPrvCandidate(ZtfEventBaseCandidate):
    # Candidate ID from operations DB
    candid: Optional[int]
    # t or 1 => candidate is from positive (sci minus ref) subtraction; f or 0 => candidate is from negative (ref minus sci) subtraction
    isdiffpos: Optional[str]
    # Right Ascension of candidate; J2000 [deg]
    ra: Optional[float]
    # Declination of candidate; J2000 [deg]
    dec: Optional[float]
    # Magnitude from PSF-fit photometry [mag]
    magpsf: Optional[float]
    # 1-sigma uncertainty in magpsf [mag]
    sigmapsf: Optional[float]
    # Right Ascension of nearest source in reference image PSF-catalog; J2000 [deg]
    ranr: Optional[float]
    # Declination of nearest source in reference image PSF-catalog; J2000 [deg]
    decnr: Optional[float]


class ZtfEventCutout(TypedDict):
    """We don't use this property in the main pipeline but it's used in the stamp extractor pipeline"""

    fileName: str
    # fits.gz
    stampData: bytes


class ZtfEventFPHist(TypedDict):
    # ZTF field ID
    field: Optional[int]
    # Readout channel ID [00 .. 63]
    rcid: Optional[int]
    # Filter ID (1=g; 2=R; 3=i)
    fid: int
    # Processing ID for image
    pid: int
    # Processing ID for reference image to facilitate archive retrieval
    rfid: int
    # Effective FWHM of sci image [pixels]
    sciinpseeing: Optional[float]
    # Background level in sci image [DN]
    scibckgnd: Optional[float]
    # Robust sigma per pixel in sci image [DN]
    scisigpix: Optional[float]
    # Magnitude zero point for photometry estimates [mag]
    magzpsci: Optional[float]
    # Magnitude zero point uncertainty (in magzpsci) [mag]
    magzpsciunc: Optional[float]
    # RMS (deviation from average) in all differences between instrumental photometry and matched photometric calibrators from science image processing [mag]
    magzpscirms: Optional[float]
    # Color coefficient from linear fit from photometric calibration of science image
    clrcoeff: Optional[float]
    # Color coefficient uncertainty from linear fit (corresponding to clrcoeff)
    clrcounc: Optional[float]
    # Integration time of camera exposure [sec]
    exptime: Optional[float]
    # Full sci image astrometric RMS along R.A. with respect to Gaia1 [arcsec]
    adpctdif1: Optional[float]
    # Full sci image astrometric RMS along Dec. with respect to Gaia1 [arcsec]
    adpctdif2: Optional[float]
    # Expected 5-sigma mag limit in difference image based on global noise estimate [mag]
    diffmaglim: Optional[float]
    # Program ID: encodes either public, collab, or caltech mode
    programid: int
    # Observation Julian date at start of exposure [days]
    jd: float
    # Forced difference image PSF-fit flux [DN]
    forcediffimflux: Optional[float]
    # 1-sigma uncertainty in forcediffimflux [DN]
    forcediffimfluxunc: Optional[float]
    # Forced photometry processing status codes (0 => no warnings)
    procstatus: Optional[str]
    # Distance to nearest source in reference image PSF-catalog [arcsec]
    distnr: Optional[float]
    # Right Ascension of nearest source in reference image PSF-catalog; J2000 [deg]
    ranr: float
    # Declination of nearest source in reference image PSF-catalog; J2000 [deg]
    decnr: float
    # Magnitude of nearest source in reference image PSF-catalog [mag]
    magnr: Optional[float]
    # 1-sigma uncertainty in magnr [mag]
    sigmagnr: Optional[float]
    # DAOPhot chi parameter of nearest source in reference image PSF-catalog
    chinr: Optional[float]
    # DAOPhot sharp parameter of nearest source in reference image PSF-catalog
    sharpnr: Optional[float]


class ZtfEvent(TypedDict):
    """We are using ZTF schema version 4.02"""

    # Schema version used
    schemavsn: str
    # Origin of alert packet
    publisher: str
    # Object identifier or name
    objectId: str
    candid: int
    candidate: ZtfEventCandidate
    prv_candidates: Optional[list[ZtfEventPrvCandidate]]
    fp_hists: Optional[list[ZtfEventFPHist]]
    cutoutScience: Optional[ZtfEventCutout]
    cutoutTemplate: Optional[ZtfEventCutout]
    cutoutDifference: Optional[ZtfEventCutout]
