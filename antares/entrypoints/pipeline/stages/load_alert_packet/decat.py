from datetime import datetime
from typing import Optional, TypedDict, cast

from astropy import units as u
from astropy.coordinates import Angle, SkyCoord

from antares.adapters.repository import AbstractLocusRepository
from antares.domain.models import Alert, AlertNormalizedProperties, Locus, Survey
from antares.observability.metrics.base import AbstractMetrics
from antares.utils import get_median_coords, nearest

from .base import AbstractAlertPacket


class DecatEventSource_0_11(TypedDict):
    # Unique ID for observation
    sourceid: int
    # RA of observed transient (Degrees, J2000)
    ra: float
    # Dec of observed transient (Degrees, J2000)
    dec: float
    # Magnitude of transient
    mag: float
    # Error on magnitude of transient
    magerr: float
    # Flux of transient
    flux: float
    # Error on flux of transioent
    fluxerr: float
    # FWHM of object on difference
    fwhm: Optional[float]
    # Real/bogus score from ML filter (0..1; higher = more likely real)
    rb: float
    # Minimum rb cut used to decide if an alert should be generated
    rbcut: float
    # NOAO proposal ID of the exposure (from header PROPID)
    propid: str
    # Filter of exposure (from header FILTER)
    filter: str
    # Modified Julian Date of observation (from header MJD-OBS)
    mjd: float
    # NOAO archive filename of the search exposure (from header DTNSANAM)
    expname: Optional[str]
    # Exposure time (s) (from header EXPTIME)
    exptime: Optional[float]
    # True if stack, false if individual image
    is_stack: bool
    # MJD of first image in stack
    jdstartstack: Optional[float]
    # MJD of last image in stack
    jdsendstack: Optional[float]
    # Meadian MJD of images in stack
    jdmed: Optional[float]
    # NOAO archive filenames of images in this stack
    # TODO: These cant be converted into a DF/Table
    stackimages: list[str]
    # Exposure times of images in stack
    # TODO: These cant be converted into a DF/Table
    stackexptimes: list[float]
    # DECam chip number of observation
    ccdnum: int
    # Seeing of image (arcsec), determined by pipeline
    seeing: Optional[float]
    # Uncertainty on seeing of image (arcsec)
    seeingerr: Optional[float]
    # Sigma of sky background of image
    skysig: Optional[float]
    # Zeropoint of image
    magzp: Optional[float]
    # Limiting magnitude of subtraction (or is it image?)
    limmag: Optional[float]
    # URL to preprocessed exposure at NERSC
    sciurl: str
    # URL to coadded/remapped reference exposure at NERSC
    refrul: str
    # URL to difference image at NERSC
    diffurl: str
    # FITS image of cutout around candidate on search image
    scicutout: Optional[bytes]
    # FITS image of cutout around candidate on coadded reference image
    refcutout: Optional[bytes]
    # FITS image of cutout around candidate on subtraction
    diffcutout: Optional[bytes]


class DecatEventObject_0_11(TypedDict):
    # Unique ID for candidate
    objectid: int
    # RA of candidate (Degrees, J2000)
    ra: float
    # Dec of candidate (Degrees, J2000)
    dec: float
    # Galactic longitude l of candidate (Degrees)
    gallong: float
    # Galactic latitude b of candidate (Degrees)
    gallat: float
    # Parallax from GAIA catalog if it exists
    parallax: Optional[float]
    # Time pipeline discovered the candidate (ISO 8601)
    tdisc: str
    # Did we search for nearest Legacy Survey DR9 object?
    ls_check: bool
    # ID of nearest object (within ...) from Legacy Survey DR9 if it exists
    lsobjectid: Optional[str]
    # Type of nearest object from Legacy Survey DR9
    lstype: Optional[str]
    # Distance from candidate to nearest DR9 object in arseconds
    lsdist: Optional[float]
    # Meadian photo-z of nearest DR9 object
    lszphotmed: Optional[float]
    # Average photo-z of nearest DR9 object
    lzsphotmean: Optional[float]
    # 68% lower limit photo-z of nearest DR9 object
    lszphotl68: Optional[float]
    # 68% upper limit photo-z of nearest DR9 object
    lszphotu68: Optional[float]
    # 95% lower limit photo-z of nearest DR9 object
    lszphotl95: Optional[float]
    # 95% upper limit photo-z of nearest DR9 object
    lszphotu95: Optional[float]
    # Observations of this candidate
    sources: list[DecatEventSource_0_11]


def compute_normalized_alert_properties(
    source: DecatEventSource_0_11,
) -> AlertNormalizedProperties:
    # TODO: Verify all of these
    properties = {
        "ant_ra": source["ra"],
        "ant_dec": source["dec"],
        "ant_mjd": source["mjd"],
        "ant_time_received": round(datetime.utcnow().timestamp()),
        "ant_mag": source["mag"],
        "ant_magerr": source["magerr"],
        "ant_passband": source["filter"],
        "ant_survey": 3,
    }
    if limmag := source.get("limmag"):
        properties["ant_maglim"] = limmag
    return cast(AlertNormalizedProperties, properties)


def compute_survey_specific_alert_properties(
    source: DecatEventSource_0_11, event: DecatEventObject_0_11
) -> dict:
    ignore = ["stackimages", "stackexptimes"]
    properties = {
        f"decat_{key}": value
        for key, value in source.items()
        if value is not None and key not in ignore
    }
    properties["decat_object_id"] = event["objectid"]
    return properties


def create_alert_from_decat_candidate(
    source: DecatEventSource_0_11, event: DecatEventObject_0_11
) -> Alert:
    return Alert(
        id=f"decat:{source['sourceid']}",
        location=SkyCoord(ra=source["ra"], dec=source["dec"], unit=u.deg),
        mjd=source["mjd"],
        survey=Survey.SURVEY_DECAT,
        created_at=datetime.utcnow(),
        normalized_properties=compute_normalized_alert_properties(source),
        properties=compute_survey_specific_alert_properties(source, event),
    )


def extract_alerts(event: DecatEventObject_0_11) -> list[Alert]:
    alerts: list[Alert] = []
    for source in event["sources"]:
        alerts.append(create_alert_from_decat_candidate(source, event))
    return alerts


class DecatAlertPacket(AbstractAlertPacket):
    def __init__(
        self,
        event: DecatEventObject_0_11,
        locus_association_radius: Angle = Angle("1.5s"),
        locus_association_search_radius: Angle = Angle("5s"),
    ):
        super().__init__()
        self.event: DecatEventObject_0_11 = event
        if locus_association_radius > locus_association_search_radius:
            raise ValueError(
                (
                    "locus_association_radius must be less than or equal to "
                    "locus_association_search_radius."
                )
            )
        self.locus_association_radius = locus_association_radius
        self.locus_association_search_radius = locus_association_search_radius
        self._alerts: list[Alert] = extract_alerts(event)

    @property
    def alerts(self) -> list[Alert]:
        return self._alerts

    @property
    def triggering_alert(self) -> Alert:
        return self.alerts[-1]

    @property
    def previous_alerts(self) -> list[Alert]:
        return self.alerts[:-1]

    @property
    def location(self) -> SkyCoord:
        ra, dec = get_median_coords(
            (alert.location.ra.deg, alert.location.dec.deg)
            for alert in self.alerts
            if alert.location is not None
        )
        return SkyCoord(ra=ra, dec=dec, unit=u.deg)

    def get_associated_locus(
        self, locus_repository: AbstractLocusRepository, metrics: AbstractMetrics
    ) -> Optional[Locus]:
        """Attempt to find a locus suitable for association with this alert packet."""
        # Begin by finding all loci within some large search radius (we typically set
        # this to something like 5"). If there is nothing in this result set we return
        # None.
        nearby_loci = locus_repository.list_by_cone_search(
            self.location, self.locus_association_search_radius
        )
        nearby_loci = list(nearby_loci)
        if not nearby_loci:
            return None
        # A helper function for getting the nearest locus from a list of `loci` but not
        # outside of `max_angle`.

        def get_nearest_locus(loci: list[Locus], max_angle: Angle) -> Optional[Locus]:
            return nearest(
                self.location,
                loci,
                key=lambda locus: locus.location,
                max_angle=max_angle,
            )

        # We look for an object within our the large search radius used to originally build
        # `nearby_loci` with a matching ZTF Object ID. If we find one, we consider it
        # a successful association.
        decat_object_id = self.event["objectid"]
        locus: Optional[Locus] = get_nearest_locus(
            [
                locus
                for locus in nearby_loci
                if decat_object_id
                in locus.properties.get("survey", {}).get("decat", {}).get("id", [])
            ],
            self.locus_association_search_radius,
        )
        if locus:
            return locus
        # Otherwise, just return the nearest locus to our search location. In this
        # case we use a smaller radius (typically 1.5") since we don't have something
        # like an object ID to tie the alert to this locus.
        return get_nearest_locus(nearby_loci, self.locus_association_radius)

    @property
    def is_good(self) -> bool:
        # TODO: Define "goodness" criteria for DECat data
        return True
