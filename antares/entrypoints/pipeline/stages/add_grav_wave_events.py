import logging

from dependency_injector.wiring import Provide, inject

from antares.adapters.repository.base import AbstractGravWaveRepository
from antares.domain.models import AlertGravWaveEvent, GravWaveNotice
from antares.utils import mjd_to_datetime


@inject
def add_grav_wave_events_to_alert_packet(
    alert_packet,
    grav_wave_repo: AbstractGravWaveRepository = Provide["grav_wave_repository"],
) -> dict[str, GravWaveNotice]:
    current_notices = grav_wave_repo.get_current_notices()
    alert = alert_packet.triggering_alert
    alert_datetime = mjd_to_datetime(alert.mjd)
    location = alert.location or alert_packet.location
    events_metadata = {}

    for notice in current_notices:
        if notice.event_datetime is None:
            raise ValueError("Can't add grav_wave_event without event_datetime")
        if alert_datetime < notice.event_datetime:
            continue
        level, area = notice.get_probability_contour_level_and_area(location)
        if level < 99.5:
            logging.info(
                "Tagging Alert %s with GravWave %s", alert.id, notice.gracedb_id
            )
            grav_wave_event = AlertGravWaveEvent(
                gracedb_id=notice.gracedb_id,
                contour_level=level,
                contour_area=area.value,
            )
            alert.upsert_grav_wave_event(grav_wave_event)
            events_metadata[notice.gracedb_id] = notice.copy_without_skymap()
    return events_metadata


def add_grav_wave_events_to_locus(alert_packet, locus) -> None:
    alert = alert_packet.triggering_alert
    for event in alert.grav_wave_events:
        logging.info("Tagging Locus %s with GravWave %s", locus.id, event["gracedb_id"])

        locus.add_grav_wave_event_association(event["gracedb_id"])
    return None
