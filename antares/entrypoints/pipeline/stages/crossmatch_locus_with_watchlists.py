from dependency_injector.wiring import Provide, inject

from antares.adapters.repository import AbstractWatchObjectRepository
from antares.domain.models import Locus, WatchObject
from antares.observability.tracing.base import AbstractTracer


@inject
def crossmatch_locus_with_watchlists(
    # fmt: off
    locus: Locus,
    watch_object_repository: AbstractWatchObjectRepository = Provide["watch_object_repository"],
    tracer: AbstractTracer = Provide["tracer"]
    # fmt: on
) -> list[WatchObject]:
    with tracer.trace("crossmatch_locus_with_watchlists") as span:
        watch_objects = []
        try:
            for watch_object in watch_object_repository.list_by_location(
                locus.location
            ):
                locus.add_watch_object_association(watch_object)
                watch_objects.append(watch_object)
        except Exception as e:
            span.set_error("Error associating with watch objects", e)
        return watch_objects
