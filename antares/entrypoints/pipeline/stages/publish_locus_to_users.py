import zlib
from typing import TypedDict

import bson
import marshmallow_jsonapi
import marshmallow_jsonapi.fields
from dependency_injector.wiring import Provide, inject

from antares.adapters.messages import AbstractMessagePublicationService
from antares.domain.models import Locus
from antares.observability.metrics.base import AbstractMetrics
from antares.observability.tracing.base import AbstractTracer


class OutputTriggerCriteria(TypedDict, total=False):
    has_tag: str
    new_tag: str


class OutputTrigger(TypedDict):
    name: str
    criteria: OutputTriggerCriteria
    topic: str


@inject
def publish_locus_to_users(
    # fmt: off
    locus: Locus,
    triggers: list[OutputTrigger] = Provide["config.pipeline.output.triggers"],
    message_publication_service: AbstractMessagePublicationService = Provide["message_publication_service"],
    api_base_url: str = Provide["config.api.base_url"],
    tracer: AbstractTracer = Provide["tracer"],
    metrics: AbstractMetrics = Provide["metrics"],
    # fmt: on
) -> None:
    class LocusUpdatedMessageSchema(marshmallow_jsonapi.Schema):
        class Meta:
            type_ = "locus"
            self_url = f"{api_base_url}/loci/{{locus_id}}"
            self_url_kwargs = {"locus_id": "<id>"}

        id = marshmallow_jsonapi.fields.Str()
        htm16 = marshmallow_jsonapi.fields.Int()
        ra = marshmallow_jsonapi.fields.Float(attribute="location.ra.deg")
        dec = marshmallow_jsonapi.fields.Float(attribute="location.dec.deg")
        properties = marshmallow_jsonapi.fields.Dict()
        tags = marshmallow_jsonapi.fields.List(marshmallow_jsonapi.fields.Str())
        catalogs = marshmallow_jsonapi.fields.List(marshmallow_jsonapi.fields.Str())
        alerts = marshmallow_jsonapi.fields.Relationship(
            related_url=f"{api_base_url}/loci/{{locus_id}}/alerts",
            related_url_kwargs={"locus_id": "<id>"},
            many=True,
            type_="alert",
        )

    schema = LocusUpdatedMessageSchema()
    with tracer.trace("publish_locus_to_users"):
        payload = schema.dump(locus)
        for tag in locus.tags:
            for trigger in triggers:
                if trigger["criteria"]["has_tag"] == tag:
                    user_topic = f"client.{trigger['topic']}"
                    message_publication_service.publish(
                        user_topic,
                        zlib.compress(bson.dumps(payload)),
                    )
                    metrics.increment_pipeline_loci_sent_to_users(user_topic)
