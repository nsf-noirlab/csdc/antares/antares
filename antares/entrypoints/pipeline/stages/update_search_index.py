import zlib
from uuid import UUID

import bson
import marshmallow
import marshmallow.fields
from dependency_injector.wiring import Provide, inject

from antares.adapters.messages import AbstractMessagePublicationService
from antares.domain.models import Locus
from antares.observability.tracing.base import AbstractTracer


@inject
def update_search_index(
    # fmt: off
    locus: Locus,
    message_publication_service: AbstractMessagePublicationService = Provide["message_publication_service"],
    index: str = Provide["config.archive.ingestion.broker.topic"],
    tracer: AbstractTracer = Provide["tracer"]
    # fmt: on
) -> None:
    class LocusUpdatedMessageSchema(marshmallow.Schema):
        locus_id = marshmallow.fields.Str(attribute="id", required=True)
        ra = marshmallow.fields.Float(attribute="location.ra.deg", required=True)
        dec = marshmallow.fields.Float(attribute="location.dec.deg", required=True)
        htm16 = marshmallow.fields.Int()
        properties = marshmallow.fields.Dict(required=True)
        tags = marshmallow.fields.List(marshmallow.fields.Str())
        grav_wave_events = marshmallow.fields.List(marshmallow.fields.Str())
        catalogs = marshmallow.fields.List(marshmallow.fields.Str())
        watch_list_ids = marshmallow.fields.Method("get_watch_list_ids")
        watch_object_ids = marshmallow.fields.Method("get_watch_object_ids")

        def get_watch_list_ids(self, locus: Locus) -> list[UUID]:
            return [id_ for (id_, _) in locus.watch_object_matches]

        def get_watch_object_ids(self, locus: Locus) -> list[UUID]:
            return [id_ for (_, id_) in locus.watch_object_matches]

    with tracer.trace("update_search_index"):
        schema = LocusUpdatedMessageSchema()
        payload = schema.dump(locus)
        message_publication_service.publish(index, zlib.compress(bson.dumps(payload)))
