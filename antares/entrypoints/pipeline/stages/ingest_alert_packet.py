from typing import Callable, Optional

from dependency_injector.wiring import Provide, Provider, inject

from antares.adapters.concurrency import AbstractDistributedLockFactory
from antares.adapters.repository import AbstractAlertRepository, AbstractLocusRepository
from antares.domain.models import Locus
from antares.entrypoints.pipeline.stages.load_alert_packet.base import (
    AbstractAlertPacket,
)
from antares.observability.metrics.base import AbstractMetrics
from antares.observability.tracing.base import AbstractTracer


@inject
def ingest_alert_packet(
    # fmt: off
    alert_packet: AbstractAlertPacket,
    alert_repository: AbstractAlertRepository = Provide["alert_repository"],
    locus_repository: AbstractLocusRepository = Provide["locus_repository"],
    locus_id_factory: Callable[[AbstractAlertPacket], str] = Provider["locus_id_factory"],
    tracer: AbstractTracer = Provide["tracer"],
    metrics: AbstractMetrics = Provide["metrics"],
    lock_factory: AbstractDistributedLockFactory = Provide["lock_factory"],
    lock_ttl: float = Provide["config.pipeline.ingestion.lock.ttl"],
    # fmt: on
) -> Locus:
    """
    This handler ingests the alerts contained within an alert packet. At a high level,
    this process comprises looking for an existing locus that the alert packet should be
    associated with, or creating one if it isn't found. Once we have a locus to
    associate with, we store all of the alerts within the packet on that locus.

    Parameters
    ----------
    alert_packet: AbstractAlertPacket
    alert_repository
    locus_repository
    locus_id_factory
    lock_factory
    lock_ttl

    Returns
    -------
    Locus
        The locus that this alert packet was associated with

    """
    with tracer.trace("ingest_alert_packet") as span:
        span.set_attribute("alert_id", alert_packet.alerts[-1].id)
        span.set_attribute("alert_is_good", alert_packet.is_good)
        locus: Optional[Locus] = alert_packet.get_associated_locus(
            locus_repository, metrics
        )
        if not locus:
            # If we don't find an existing locus to associate this alert with we need to
            # create one in a thread-safe manner. There are two necessary conditions here:
            # (1) is that the check-and-set must happen atomically and (2) is that
            # sequential consistency must be enforced between one process's set and the
            # next's check.
            with (
                lock_factory("ingest_alert_packet", lock_ttl),
                tracer.trace("create_locus") as lock_trace,
            ):
                locus = alert_packet.get_associated_locus(locus_repository, metrics)
                if not locus:
                    locus_id = locus_id_factory(alert_packet)
                    locus = Locus(locus_id, location=alert_packet.location)
                    lock_trace.set_attribute("locus_id", locus.id)
                    locus_repository.add(locus)

        # It may be that the above add fails to add the locus, but an error will be reported through opentracing,
        # so it will not be completely lost.  Further, the alerts below might be added leaving the following locus fields
        # empty in the database:
        # created_at, dec, props, ra, updated_at
        # So these will need to be updated manually
        alert_repository.add(alert_packet.triggering_alert, locus.id)
        alert_repository.add_many_if_not_exists(
            (alert, locus.id) for alert in alert_packet.previous_alerts
        )
        return locus
