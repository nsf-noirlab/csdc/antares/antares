import logging
import math
from collections.abc import Collection, Iterable
from typing import TypedDict, Union

import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.timeseries import TimeSeries
from dependency_injector.wiring import Provide, inject
from numpy import ma

from antares.adapters.repository import AbstractAlertRepository
from antares.domain.models import Alert, Locus, Survey
from antares.observability.tracing.base import AbstractTracer
from antares.utils import mjd_to_datetime, ztf_flux_correction
from antares.utils.validation import (
    validate_dec_degree,
    validate_lists_are_equal_length,
    validate_ra_degree,
)


@inject
def update_locus_history(
    # fmt: off
    locus: Locus,
    alert_repository: AbstractAlertRepository = Provide["alert_repository"],
    tracer: AbstractTracer = Provide["tracer"],
    # fmt: on
) -> tuple[list[Alert], TimeSeries]:
    with tracer.trace("update_locus_history") as span:
        span.set_attribute("locus_id", locus.id)
        logging.debug("Updating locus %s", locus.id)
        with tracer.trace("fetch_locus"):
            alerts = list(alert_repository.list_by_locus_id(locus.id))
        with tracer.trace("sort_alerts"):
            alerts.sort(key=lambda alert: (alert.mjd, alert.id))
        with tracer.trace("build_timeseries"):
            timeseries = compute_timeseries(alerts)
        update_locus_properties(locus, alerts, timeseries, tracer)
        span.set_attribute("num_alerts", locus.properties["num_alerts"])
        span.set_attribute("num_mag_values", locus.properties["num_mag_values"])
        return alerts, timeseries


def update_locus_properties(
    locus: Locus,
    alerts: list[Alert],
    timeseries: TimeSeries,
    tracer: AbstractTracer,
) -> None:
    with tracer.trace("update_locus_properties"):
        # TODO: This isn't correct because we can receive alerts out of order
        triggering_alert = alerts[-1]
        locus.lightcurve = get_lightcurve_from_timeseries(timeseries)
        locus.location = compute_centroid(alerts)
        locus.properties.update(
            compute_survey_normalized_properties(alerts, timeseries)
        )
        locus.properties.update(compute_backwards_compatibility_properties(alerts))
        # TODO: Is it better to compute these properties for every alert?
        locus.properties.update(
            compute_survey_specific_properties(alerts, triggering_alert.survey)
        )


class LocusPropertiesAntares(TypedDict):
    brightest_alert_id: str
    brightest_alert_magnitude: float
    brightest_alert_observation_time: float
    newest_alert_id: str
    newest_alert_magnitude: float
    newest_alert_observation_time: float
    num_alerts: int
    num_mag_values: int
    oldest_alert_id: str
    oldest_alert_magnitude: float
    oldest_alert_observation_time: float
    is_corrected: str


class LocusPropertiesSurveySpecificZtf(TypedDict):
    id: Collection[str]
    rcid: Collection[int]
    field: Collection[int]
    ssnamenr: Collection[str]


class LocusPropertiesSurveySpecificDecat(TypedDict):
    id: Collection[str]


def get_surveys(alerts: list[Alert]) -> set[Survey]:
    return set(alert.survey for alert in alerts)


def compute_timeseries(alerts: list[Alert]) -> TimeSeries:
    timeseries = TimeSeries(
        data=[
            {
                "alert_id": alert.id,
                **alert.normalized_properties,
                **alert.properties,
            }
            for alert in alerts
        ],
        time=[mjd_to_datetime(alert.mjd) for alert in alerts],
    )
    df, _, _ = ztf_flux_correction.correct_mags(timeseries)
    validate_lists_are_equal_length(df["ant_mag_corrected"], timeseries)
    timeseries["ant_mag_corrected"] = ma.masked_invalid(df["ant_mag_corrected"])
    timeseries["ant_magerr_corrected"] = ma.masked_invalid(df["ant_magerr_corrected"])
    timeseries["ant_magulim_corrected"] = ma.masked_invalid(df["ant_magulim_corrected"])
    timeseries["ant_magllim_corrected"] = ma.masked_invalid(df["ant_magllim_corrected"])
    return timeseries


def get_lightcurve_from_timeseries(timeseries: TimeSeries) -> TimeSeries:
    # If the `time` column is not selected, the input TimeSeries is automatically cast
    # to an `astropy.table.Table`.
    columns = [
        "time",
        "alert_id",
        "ant_mjd",
        "ant_survey",
        "ant_ra",
        "ant_dec",
        "ant_passband",
        "ant_mag",
        "ant_magerr",
        "ant_maglim",
        "ant_mag_corrected",
        "ant_magerr_corrected",
        "ant_magulim_corrected",
        "ant_magllim_corrected",
    ]
    return timeseries[columns]


def compute_backwards_compatibility_properties(alerts: list[Alert]) -> dict:
    # Backwards compatibility layer for old ZTF pipeline:
    properties = {}
    try:
        # In a previous version of ANTARES, every time we received a new alert on a
        # locus, we stored a property `locus.properties["ztf_object_id"]` with the ZTF
        # object ID of the latest alert. This field is widely used by our clients but,
        # strictly speaking, is semantically invalid. Our alert-to-locus association
        # strategy results in a many-to-many mapping from alerts to loci. This field
        # is better modeled by the newer `locus.properties["survey"]["ztf"]["id"]` field
        # which stores a `set` collection of all ZTF object IDs present on a locus. We
        # continue to create the field here for backwards compatibility.
        #
        # A note on the conditional in this list comprehension: not all ZTF alerts have
        # an `alert.properties["ztf_object_id"]` value stored on them. We added this
        # field when we migrated the pipeline to ingest from multiple data sources and
        # it hasn't been backfilled to old alerts (and likely never will, as we treat
        # them immutably). There will always be *at least one* alert that has it and
        # we store the most recent on the locus. The check for the truthy-ness of each
        # alert's `alert.properties.get("ztf_object_id")` return is necessary.
        ztf_object_ids = [
            alert.properties["ztf_object_id"]
            for alert in alerts
            if (
                alert.survey == Survey.SURVEY_ZTF
                and alert.properties.get("ztf_object_id")
            )
        ]
        newest_ztf_object_id = next(reversed(ztf_object_ids))
        properties["ztf_object_id"] = newest_ztf_object_id
        # Ditto for `ztf_ssnamenr`
        ztf_ssnamenrs = [
            alert.properties["ztf_ssnamenr"]
            for alert in alerts
            if (
                alert.survey == Survey.SURVEY_ZTF
                and alert.properties.get("ztf_ssnamenr")
            )
        ]
        newest_ztf_ssnamenr = next(reversed(ztf_ssnamenrs))
        properties["ztf_ssnamenr"] = newest_ztf_ssnamenr
        return properties
    except StopIteration:
        pass
    finally:
        return properties


def compute_survey_specific_properties(
    alerts: list[Alert], survey: Survey
) -> dict[
    str,
    dict[
        str, Union[LocusPropertiesSurveySpecificZtf, LocusPropertiesSurveySpecificDecat]
    ],
]:
    if survey is Survey.SURVEY_ZTF:
        return {
            "survey": {
                Survey.SURVEY_ZTF.value: compute_survey_specific_properties_ztf(alerts)
            }
        }
    elif survey is Survey.SURVEY_DECAT:
        return {
            "survey": {
                Survey.SURVEY_DECAT.value: compute_survey_specific_properties_decat(
                    alerts
                )
            }
        }
    else:
        raise ValueError(f"Unrecognized survey {survey}")


def compute_survey_specific_properties_ztf(
    alerts: list[Alert],
) -> LocusPropertiesSurveySpecificZtf:
    # This can be refactored to use one iterator instead of multiple
    ztf_alerts = [alert for alert in alerts if alert.survey == Survey.SURVEY_ZTF]
    return LocusPropertiesSurveySpecificZtf(
        # TODO: Should this be `object_id`?
        id=sorted(
            list(
                set(
                    alert.properties["ztf_object_id"]
                    for alert in ztf_alerts
                    if "ztf_object_id" in alert.properties
                )
            )
        ),
        rcid=sorted(
            list(
                set(
                    alert.properties["ztf_rcid"]
                    for alert in ztf_alerts
                    if "ztf_rcid" in alert.properties
                )
            )
        ),
        field=sorted(
            list(
                set(
                    alert.properties["ztf_field"]
                    for alert in ztf_alerts
                    if "ztf_field" in alert.properties
                )
            )
        ),
        ssnamenr=sorted(
            list(
                set(
                    alert.properties["ztf_ssnamenr"]
                    for alert in ztf_alerts
                    if "ztf_ssnamenr" in alert.properties
                )
            )
        ),
    )


def compute_survey_specific_properties_decat(
    alerts: list[Alert],
) -> LocusPropertiesSurveySpecificDecat:
    decat_alerts = [alert for alert in alerts if alert.survey == Survey.SURVEY_ZTF]
    return LocusPropertiesSurveySpecificDecat(
        # TODO: Should this be `object_id`?
        id=sorted(
            list(
                set(
                    alert.properties["decat_object_id"]
                    for alert in decat_alerts
                    if "ztf_object_id" in alert.properties
                )
            )
        ),
    )


def compute_centroid(alerts: Iterable[Alert]) -> SkyCoord:
    """
    This function recomputes the centroid of a locus based on the positions of all
    alerts owned by that locus.
    """
    x, y, z = [], [], []
    for alert in alerts:
        # Some alerts do not have positional information (e.g. ZTF's upper limits) and
        # are associated to a locus but should not be used for the centroid computation.
        if alert.location:
            ra_degree = alert.location.ra.degree
            dec_degree = alert.location.dec.degree
            validate_ra_degree(ra_degree)
            validate_dec_degree(dec_degree)
            ra_radian = math.radians(ra_degree)
            dec_radian = math.radians(dec_degree)
            x.append(math.cos(dec_radian) * math.cos(ra_radian))
            y.append(math.cos(dec_radian) * math.sin(ra_radian))
            z.append(math.sin(dec_radian))

    # Compute the mean xyz vector of all input xyz vectors
    x_median, y_median, z_median = np.median(x), np.median(y), np.median(z)
    magnitude = np.linalg.norm(
        np.array([x_median, y_median, z_median])
    )  # Norm (length) of xyz vector
    ra_degree = math.degrees(math.atan2(y_median, x_median))
    dec_degree = 90 - math.degrees(math.acos(z_median / magnitude))

    # Final range checking
    if ra_degree < 0:
        ra_degree += 360
    validate_ra_degree(ra_degree)
    validate_dec_degree(dec_degree)
    if dec_degree == -90 or dec_degree == 90:
        # ra is undefined when dec is -90 or 90.
        ra_degree = 0
    return SkyCoord(ra=ra_degree, dec=dec_degree, unit=u.deg)


def compute_survey_normalized_properties(
    alerts: list[Alert], timeseries: TimeSeries
) -> LocusPropertiesAntares:
    mag_values = [
        alert for alert in alerts if alert.normalized_properties.get("ant_mag")
    ]
    oldest_alert = mag_values[0]
    newest_alert = mag_values[-1]
    # Typing gets fixed after python 3.10
    brightest_alert = min(
        mag_values, key=lambda alert: alert.normalized_properties["ant_mag"]  # type: ignore
    )
    if np.ma.any(timeseries["ant_mag_corrected"]):
        is_corrected = "true"
    else:
        is_corrected = "false"
    return LocusPropertiesAntares(
        num_alerts=len(alerts),
        num_mag_values=len(mag_values),
        brightest_alert_id=brightest_alert.id,
        brightest_alert_magnitude=brightest_alert.normalized_properties["ant_mag"],  # type: ignore #ant_mag can't be None
        brightest_alert_observation_time=brightest_alert.mjd,
        newest_alert_id=newest_alert.id,
        newest_alert_magnitude=newest_alert.normalized_properties["ant_mag"],  # type: ignore #ant_mag can't be None
        newest_alert_observation_time=newest_alert.mjd,
        oldest_alert_id=oldest_alert.id,
        oldest_alert_magnitude=oldest_alert.normalized_properties["ant_mag"],  # type: ignore #ant_mag can't be None
        oldest_alert_observation_time=oldest_alert.mjd,
        is_corrected=is_corrected,
    )
