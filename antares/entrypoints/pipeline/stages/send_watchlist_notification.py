import logging

from dependency_injector.wiring import Provide, inject

from antares.adapters.notifications import AbstractNotificationService
from antares.adapters.repository import AbstractWatchListRepository
from antares.domain.models import Locus, WatchObject
from antares.observability.tracing.base import AbstractTracer


@inject
def send_watchlist_notification(
    # fmt: off
    locus: Locus,
    watch_object: WatchObject,
    watch_list_repository: AbstractWatchListRepository = Provide["watch_list_repository"],
    notification_service: AbstractNotificationService = Provide["notification_service"],
    tracer: AbstractTracer = Provide["tracer"],
    # fmt: on
):
    with tracer.trace("send_watchlist_notification") as span:
        span.set_attribute("watch_object_id", str(watch_object.id))
        span.set_attribute("watch_list_id", str(watch_object.watch_list_id))
        watch_list = watch_list_repository.get(watch_object.watch_list_id)
        if not watch_list:
            message = f"Orphan watch object found: {watch_object}"
            logging.warning(message)
            span.set_error(message)
            return
        if watch_list.slack_channel == "":
            message = f"Watch object without slack channel found: {watch_object}"
            logging.warning(message)
            span.set_error(message)
            return
        message = (
            f"Hit on watch list <https://antares.noirlab.edu/watch-lists/{watch_list.id}|{watch_list.name}>"
            f"\n"
            f"Object: {watch_object.name} ({watch_object.location.ra.deg}, {watch_object.location.dec.deg})"
            f"\n"
            f"*<https://antares.noirlab.edu/loci/{locus.id}|Associated Locus>*"
            f" ra=`{locus.location.ra.deg}`"
            f" dec=`{locus.location.dec.deg}`"
        )
        with notification_service as notifications:
            notifications.send(watch_list.slack_channel, message)
