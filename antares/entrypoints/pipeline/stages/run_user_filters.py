import logging
from typing import Optional

from astropy.timeseries import TimeSeries
from dependency_injector.wiring import Provide, inject

from antares.adapters.notifications import (
    AbstractInternalNotificationService,
    AbstractNotificationService,
)
from antares.adapters.repository import (
    AbstractFilterRepository,
    AbstractFilterRevisionRepository,
)
from antares.domain.models import Alert, CatalogObject, GravWaveNotice, Locus
from antares.domain.models.filter import (
    FilterErrored,
    FilterSkipped,
    FilterSucceeded,
    run_filter_against_locus,
)
from antares.exceptions import HaltPipeline
from antares.observability.metrics.base import AbstractMetrics
from antares.observability.tracing.base import AbstractTracer


@inject
def run_user_filters(
    # fmt: off
    locus: Locus,
    alerts: list[Alert],
    timeseries: TimeSeries,
    catalog_objects: list[CatalogObject],
    grav_wave_events_metadata: Optional[dict[str, GravWaveNotice]] = None,
    filter_repository: AbstractFilterRepository = Provide["filter_repository"],
    filter_revision_repository: AbstractFilterRevisionRepository = Provide["filter_revision_repository"],
    notification_service: AbstractNotificationService = Provide["notification_service"],
    internal_notification_service: AbstractInternalNotificationService = Provide[
        "internal_notification_service"
    ],
    tracer: AbstractTracer = Provide["tracer"],
    metrics: AbstractMetrics = Provide["metrics"]
    # fmt: on
) -> None:
    with tracer.trace("run_user_filters"):
        for filter_, filter_executable in filter_repository.get_filter_executables(
            filter_revision_repository,
            internal_notification_service,
            tracer,
        ):
            if filter_.enabled_filter_revision_id is None:
                raise RuntimeError(
                    f"Filter {filter_.name} doesn't have enabled_filter_revision_id"
                )
            with tracer.trace("run_filter") as span:
                span.set_attribute(
                    "antares.filter.revision", filter_.enabled_filter_revision_id
                )
                span.set_attribute("locus.id", locus.id)
                span.set_attribute("antares.filter.name", filter_.name)
                logging.debug(
                    "Running filter %s against locus %s", filter_.name, locus.id
                )
                filter_return = run_filter_against_locus(
                    filter_executable,
                    locus,
                    alerts,
                    timeseries,
                    catalog_objects,
                    grav_wave_events_metadata,
                )

                if isinstance(filter_return, FilterSucceeded):
                    filter_return_status = "Succeeded"
                elif isinstance(filter_return, FilterSkipped):
                    filter_return_status = "Skipped"
                elif isinstance(filter_return, FilterErrored):
                    filter_return_status = "Errored"
                else:
                    filter_return_status = "Unknown"
                span.set_attribute("Execution", filter_return_status)

                if isinstance(filter_return, FilterErrored):
                    if isinstance(filter_return.exception, HaltPipeline):
                        return
                    span.set_error(
                        f"Failed running filter {filter_.name}", filter_return.exception
                    )
                    warning_msg = f"Filter {filter_.name} crashed on locus {locus.id}"
                    logging.warning(warning_msg, exc_info=filter_return.exception)
                    filter_revision = filter_revision_repository.get(
                        filter_.enabled_filter_revision_id
                    )
                    if filter_revision is None:
                        raise RuntimeError(
                            f"No revision found for filter `{filter_.name}`"
                            f" with revision_id={filter_.enabled_filter_revision_id}"
                        )
                    feedback = f"Disabled automatically by the pipeline: {warning_msg}"
                    filter_revision.disable(filter_=filter_, feedback=feedback)
                    filter_revision_repository.update(filter_revision)
                    filter_repository.update(filter_)
                    message = (
                        f"Filter `{filter_.name}` crashed on locus `{locus.id}` with exception:"
                        f"\n"
                        f"```{filter_return.exception}```"
                    )
                    # Notify internal staff
                    with internal_notification_service as notifier:
                        notifier.notify_after_filter_revision_transitioned(
                            filter_,
                            filter_revision,
                            custom_message=f"```{filter_return.exception}```",
                        )
                    # Notify filter owner
                    with notification_service as notifications:
                        notifications.send(filter_executable.slack_channel, message)
                else:
                    metrics.increment_pipeline_filter_run(filter_.name)
