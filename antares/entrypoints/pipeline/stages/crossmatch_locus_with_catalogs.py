import logging

from dependency_injector.wiring import Provide, inject

from antares.adapters.repository import AbstractCatalogObjectRepository
from antares.domain.models import CatalogObject, Locus
from antares.observability.metrics.base import AbstractMetrics
from antares.observability.tracing.base import AbstractTracer


@inject
def crossmatch_locus_with_catalogs(
    # fmt: off
    locus: Locus,
    catalog_object_repository: AbstractCatalogObjectRepository = Provide["catalog_object_repository"],
    tracer: AbstractTracer = Provide["tracer"],
    metrics: AbstractMetrics = Provide["metrics"]
    # fmt: on
) -> list[CatalogObject]:
    with tracer.trace("crossmatch_locus_with_catalogs") as span:
        try:
            catalog_objects = catalog_object_repository.list_by_location(
                locus.location, metrics
            )
            catalog_objects = list(catalog_objects)
            logging.debug("Found catalog objects for %s", locus.id)
            for catalog_object in catalog_objects:
                logging.debug("%s: %s", catalog_object.catalog_name, catalog_object.id)
                locus.add_catalog_object_association(catalog_object)
            return catalog_objects
        except Exception as e:
            logging.exception("Error in crossmatch locus with catalogs")
            span.set_error("Error in crossmatch locus with catalogs", e)
            return []
