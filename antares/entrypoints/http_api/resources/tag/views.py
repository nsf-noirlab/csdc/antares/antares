from dependency_injector.wiring import Provide, inject

from antares.adapters.repository import (
    AbstractFilterRepository,
    AbstractFilterRevisionRepository,
)
from antares.domain.models.filter import FilterDevkitApi
from antares.domain.models.tag import Tag
from antares.entrypoints.http_api.common.exceptions import ResourceNotFoundException
from antares.entrypoints.http_api.common.resource import (
    ResourceDetailReadOnly,
    ResourceListReadOnly,
)

from .schemas import TagSchema


class TagList(ResourceListReadOnly):
    """
    This method only retrieves tags from filters that are public and enabled. If we allow just public
    filters, users can inject malicious code because we apply an exec to retrieve the filter class.
    User can change the visibility of a filter whenever they want.

    Another option without loading the filter class is to parse the string code and retrive the tags list,
    but this can be tedius.
    Note:
    Not all filters add tag, thus this number won't represent the right number of tags displayed in a page but works for pagination since we have to iterate over all filters
    """

    resource_schema = TagSchema

    @inject
    def get_resource_collection(
        self,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        filter_revision_repository: AbstractFilterRevisionRepository = Provide[
            "filter_revision_repository"
        ],
        *args,
        **kwargs,
    ):
        tags = []
        for filter_ in filter_repository.list_enabled_public_filters(
            limit=kwargs.get("limit"), offset=kwargs.get("offset")
        ):
            if filter_.enabled_filter_revision_id is None:
                raise ValueError(
                    "Filter without enabled_filter_revision_id can not be loaded."
                )
            filter_revision = filter_revision_repository.get(
                filter_.enabled_filter_revision_id
            )
            if filter_revision is not None:
                filter_class: type[
                    FilterDevkitApi
                ] = filter_revision._load_filter_class()
                for tag in filter_class.OUTPUT_TAGS:
                    if (
                        tag.get("name") is not None
                        and tag.get("description") is not None
                    ):
                        tags.append(
                            Tag(
                                name=tag["name"],
                                description=tag["description"],
                                filter_version_id=filter_.version_id,
                            )
                        )
        n_filters = len(list(filter_repository.list_enabled_public_filters()))
        return tags, n_filters, None


class TagDetail(ResourceDetailReadOnly):
    """Expensive call which doesn't say more information than TagList, maybe we can delete it"""

    resource_schema = TagSchema

    @inject
    def get_resource(
        self,
        tag_name: str,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        filter_revision_repository: AbstractFilterRevisionRepository = Provide[
            "filter_revision_repository"
        ],
        *args,
        **kwargs,
    ):
        for filter_ in filter_repository.list_enabled_public_filters(
            limit=kwargs.get("limit"), offset=kwargs.get("offset")
        ):
            if filter_.enabled_filter_revision_id is None:
                raise ValueError(
                    "Filter without enabled_filter_revision_id can not be loaded."
                )
            filter_revision = filter_revision_repository.get(
                filter_.enabled_filter_revision_id
            )
            if filter_revision is not None:
                filter_class: type[
                    FilterDevkitApi
                ] = filter_revision._load_filter_class()
                for tag in filter_class.OUTPUT_TAGS:
                    if (
                        tag.get("description") is not None
                        and tag.get("name") == tag_name
                    ):
                        output_tag = Tag(
                            name=tag["name"],
                            description=tag["description"],
                            filter_version_id=filter_.version_id,
                        )
                        return output_tag, None
        raise ResourceNotFoundException(f"No resource tag with ID {tag_name}")
