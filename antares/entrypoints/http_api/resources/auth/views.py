"""
The /auth endpoints of the ANTARES API don't conform to the JSON:API
specification. We subclass flask_restful.Resource in this module for
views instead of the antares.entrypoints.http_api.common.resource.Resource* classes.
"""
import datetime
import logging
from http import HTTPStatus

from dependency_injector.wiring import Provide, inject
from flask import jsonify, request
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    current_user,
    get_jwt,
    jwt_required,
    set_access_cookies,
    set_refresh_cookies,
    unset_jwt_cookies,
    verify_jwt_in_request,
)
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask_mail import Message
from flask_restful import Resource
from marshmallow import ValidationError

from antares.adapters.repository import (
    AbstractJwtBlocklistRepository,
    AbstractUserRepository,
)
from antares.domain.models import JwtRecord
from antares.entrypoints.http_api.main import mail

from .schemas import (
    ForgotPasswordCredentials,
    LoginCredentials,
    LoginQueryParameters,
    PasswordResetCredentials,
    TokenResponse,
)


def _construct_token_response_body(
    user,
    access_expires_delta: datetime.timedelta,
    refresh_expires_delta: datetime.timedelta,
    fresh_access_token=False,
    include_refresh_token=False,
):
    payload = {
        "access_token": create_access_token(
            user,
            expires_delta=access_expires_delta,
            fresh=fresh_access_token,
        ),
        "token_type": "bearer",
    }
    if include_refresh_token:
        payload["refresh_token"] = create_refresh_token(
            user,
            expires_delta=refresh_expires_delta,
        )
    return TokenResponse().load(payload)


def _construct_token_response_cookie(
    user,
    access_expires_delta: datetime.timedelta,
    refresh_expires_delta: datetime.timedelta,
    fresh_access_token=False,
    include_refresh_token=False,
):
    access_token = create_access_token(
        user,
        expires_delta=access_expires_delta,
        fresh=fresh_access_token,
    )
    if include_refresh_token:
        refresh_token = create_refresh_token(
            user,
            expires_delta=refresh_expires_delta,
        )
    response = jsonify({"identity": user.id})
    set_access_cookies(response, access_token)
    if include_refresh_token:
        set_refresh_cookies(response, refresh_token)
    response.status_code = HTTPStatus.OK
    return response


def construct_token_response(
    user,
    access_expires_delta: datetime.timedelta,
    refresh_expires_delta: datetime.timedelta,
    type_="token",
    fresh_access_token=False,
    include_refresh_token=False,
):
    if type_ == "token":
        return _construct_token_response_body(
            user,
            access_expires_delta,
            refresh_expires_delta,
            fresh_access_token,
            include_refresh_token,
        )
    elif type_ == "cookie":
        return _construct_token_response_cookie(
            user,
            access_expires_delta,
            refresh_expires_delta,
            fresh_access_token,
            include_refresh_token,
        )
    else:
        raise ValueError('type_ must be one of ["token", "cookie"]')


class AuthCheck(Resource):
    methods = ["GET"]

    @jwt_required()
    def get(self):
        response = jsonify({})
        response.status_code = HTTPStatus.NO_CONTENT
        return response


class Forgot(Resource):
    methods = ["POST"]

    @inject
    def post(
        self,
        user_repository: AbstractUserRepository = Provide["user_repository"],
    ):
        try:
            data = request.get_json()
            if not data:
                raise ValidationError("Request empty")
            credentials = ForgotPasswordCredentials().load(data)
        except ValidationError:
            return {"error, ValidationError": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Find the user and check their password
        user = user_repository.get_by_username(credentials["username"])
        if user and user.email:
            message = self._build_email(user)
            mail.send(message)

        response = jsonify({})
        response.status_code = HTTPStatus.NO_CONTENT
        return response

    @inject
    def _build_email(
        self,
        user,
        access_expires_seconds: float = Provide[
            "config.api.authentication.parameters.access_token_expires"
        ],
        external_smtp_sender: str = Provide["config.external.smtp.sender"],
        frontend_base_url: str = Provide["config.frontend.base_url"],
    ):
        access_token = create_access_token(
            user,
            expires_delta=datetime.timedelta(seconds=access_expires_seconds),
            fresh=True,
        )

        message = Message(
            "ANTARES Password Reset",
            sender=external_smtp_sender,
            recipients=[user.email],
        )
        url = f"{frontend_base_url}/forgot-password?token={access_token}"
        message.html = "".join(
            [
                "<p>Click or copy and paste the link below into your browser "
                "to reset your ANTARES password. If you didn't request to reset "
                "your password, you can ignore this email.</p>",
                f'<a href="{url}">{url}</a>',
            ]
        )
        return message


class Login(Resource):
    methods = ["POST"]

    @inject
    def post(
        # fmt: off
        self,
        user_repository: AbstractUserRepository = Provide["user_repository"],
        access_expires_seconds: float = Provide["config.api.authentication.parameters.access_token_expires"],
        refresh_expires_seconds: float = Provide["config.api.authentication.parameters.refresh_token_expires"],
        # fmt: on
    ):
        try:
            query_parameters = LoginQueryParameters().load(request.args)
            data = request.get_json()
            if not data:
                raise ValidationError("Request empty")
            credentials = LoginCredentials().load(data)
        except ValidationError:
            return {"error, ValidationError": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Find the user and check their password
        user = user_repository.get_by_username(credentials["username"])
        if not user or not user.check_password(credentials["password"]):
            return {"error": "invalid_client"}, HTTPStatus.UNAUTHORIZED

        return construct_token_response(
            user,
            access_expires_delta=datetime.timedelta(seconds=access_expires_seconds),
            refresh_expires_delta=datetime.timedelta(seconds=refresh_expires_seconds),
            type_=query_parameters["type"],
            include_refresh_token=True,
        )


class LoginFresh(Resource):
    methods = ["POST"]

    @inject
    def post(
        # fmt: off
        self,
        user_repository: AbstractUserRepository = Provide["user_repository"],
        access_expires_seconds: float = Provide["config.api.authentication.parameters.access_token_expires"],
        refresh_expires_seconds: float = Provide["config.api.authentication.parameters.refresh_token_expires"],
        # fmt: on
    ):
        try:
            query_parameters = LoginQueryParameters().load(request.args)
            data = request.get_json()
            if not data:
                raise ValidationError("Request empty")
            credentials = LoginCredentials().load(data)
        except ValidationError:
            return {"error, ValidationError": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Find the user and check their password
        user = user_repository.get_by_username(credentials["username"])
        if not user or not user.check_password(credentials["password"]):
            return {"error": "invalid_client"}, HTTPStatus.UNAUTHORIZED

        return construct_token_response(
            user,
            access_expires_delta=datetime.timedelta(seconds=access_expires_seconds),
            refresh_expires_delta=datetime.timedelta(seconds=refresh_expires_seconds),
            type_=query_parameters["type"],
            fresh_access_token=True,
            include_refresh_token=False,
        )


class Reset(Resource):
    methods = ["POST"]

    @jwt_required(fresh=True)
    @inject
    def post(
        # fmt: off
        self,
        user_repository: AbstractUserRepository = Provide["user_repository"],
        access_expires_seconds: float = Provide["config.api.authentication.parameters.access_token_expires"],
        refresh_expires_seconds: float = Provide["config.api.authentication.parameters.refresh_token_expires"],
        # fmt: on
    ):
        """
        Reset a user's password.

        Routes
        ----------
        POST /auth/reset

        Notes
        ----------
        Successfully resetting a user's password requires a fresh JWT token.
        A user can obtain a fresh token from the /auth/login-fresh endpoint.

        Resetting a user's password DOES NOT blocklist their current access
        or refresh tokens and DOES NOT sign them out of current sessions. It
        likely should but the risk that this attack surface exposes is
        acceptable for the time-being. If, in the future, we implement a
        method of tracking tokens by user ID, we should address this issue.

        """
        try:
            query_parameters = LoginQueryParameters().load(request.args)
            data = request.get_json()
            if not data:
                raise ValidationError("Request empty")
            credentials = PasswordResetCredentials().load(data)
        except ValidationError:
            return {"error": "invalid_request"}, HTTPStatus.BAD_REQUEST
        user = user_repository.get(current_user.id)
        if not user:
            return {"error": "user_not_found"}, HTTPStatus.BAD_REQUEST
        user.set_password(credentials["password"])
        user_repository.update(user)
        return construct_token_response(
            current_user,
            access_expires_delta=datetime.timedelta(seconds=access_expires_seconds),
            refresh_expires_delta=datetime.timedelta(seconds=refresh_expires_seconds),
            type_=query_parameters["type"],
            fresh_access_token=False,
            include_refresh_token=True,
        )


class Logout(Resource):
    methods = ["POST"]

    @inject
    def post(
        # fmt: off
        self,
        jwt_blocklist_repository: AbstractJwtBlocklistRepository = Provide["jwt_blocklist_repository"],
        # fmt: on
    ):
        """
        The POST /auth/logout route serves two purposes. One is to add a valid refresh
        token to our database's blocklist. The other is to clear the authorization
        cookies from the user's browser, if they're set. These are HTTP only cookies
        to prevent XSS attacks but the browser will continue sending the cookies and
        raising "invalid token" errors or whatever if the server doesn't clear them.
        We assume that if a client POST /auth/logout they want to clear their cookies
        and so no matter what happens, we clear the cookies in the response.

        If there's an error inside the except the cookies won't be cleared
        """
        response = jsonify({})
        try:
            verify_jwt_in_request(refresh=True)
            jwt = get_jwt()
            jwt_blocklist_repository.add(
                JwtRecord(
                    "refresh", jwt["jti"], datetime.datetime.fromtimestamp(jwt["exp"])
                )
            )
            response.status_code = HTTPStatus.NO_CONTENT
        except NoAuthorizationError:
            response.status_code = HTTPStatus.UNAUTHORIZED
        except BaseException as e:
            # We should avoid this baseexception by adding the new errors from the logs
            logging.error("%s: %s", e.__class__.__name__, e)
            response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
        unset_jwt_cookies(response)
        return response


class Refresh(Resource):
    methods = ["POST"]

    @jwt_required(refresh=True)
    @inject
    def post(
        # fmt: off
        self,
        jwt_blocklist_repository: AbstractJwtBlocklistRepository = Provide["jwt_blocklist_repository"],
        access_expires_seconds: float = Provide["config.api.authentication.parameters.access_token_expires"],
        refresh_expires_seconds: float = Provide["config.api.authentication.parameters.refresh_token_expires"],
        # fmt: on
    ):
        try:
            query_parameters = LoginQueryParameters().load(request.args)
        except ValidationError:
            return {"error": "invalid_request"}, HTTPStatus.BAD_REQUEST

        # Blocklist old refresh token
        jwt = get_jwt()
        jwt_blocklist_repository.add(
            JwtRecord(
                "refresh", jwt["jti"], datetime.datetime.fromtimestamp(jwt["exp"])
            )
        )

        # Create access and refresh tokens
        access_token = create_access_token(
            current_user,
            expires_delta=datetime.timedelta(seconds=access_expires_seconds),
        )
        refresh_token = create_refresh_token(
            current_user,
            expires_delta=datetime.timedelta(seconds=refresh_expires_seconds),
        )

        # If the requester asked for a token...
        if query_parameters["type"] == "token":
            return TokenResponse().load(
                {
                    "access_token": access_token,
                    "refresh_token": refresh_token,
                    "token_type": "bearer",
                }
            )

        # Otherwise, if they asked for a cookie...
        elif query_parameters["type"] == "cookie":
            response = jsonify({})
            set_access_cookies(response, access_token)
            set_refresh_cookies(response, refresh_token)
            response.status_code = HTTPStatus.NO_CONTENT
            return response
