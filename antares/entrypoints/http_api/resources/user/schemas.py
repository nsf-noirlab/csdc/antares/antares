from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Relationship, Schema


class UserSchema(Schema):
    class Meta:
        type_ = "user"
        self_view = "user_detail"
        self_view_kwargs = {"user_id": "<id>"}
        self_view_many = "user_list"

    id = fields.UUID()
    name = fields.String(required=True)
    username = fields.String(required=True)
    email = fields.String(required=True)
    password = fields.String(required=True, load_only=True)
    staff = fields.Boolean()
    admin = fields.Boolean()

    locus_annotations = Relationship(
        related_view="user_locus_annotations",
        related_view_kwargs={"user_id": "<id>"},
        schema="LocusAnnotationSchema",
        type_="locus_annotation",
        many=True,
    )
