from dependency_injector.wiring import Provide, inject
from flask_jwt_extended import current_user

from antares.adapters.repository import AbstractUserRepository
from antares.domain.models import User
from antares.entrypoints.http_api.common import resource
from antares.entrypoints.http_api.common.decorators import (
    admin_required,
    login_required,
)
from antares.entrypoints.http_api.common.exceptions import (
    BadRequest,
    ResourceConflictException,
    ResourceNotFoundException,
    Unauthorized,
)
from antares.exceptions import KeyViolationException

from .schemas import UserSchema


class UserList(resource.ResourceList):
    resource_schema = UserSchema

    @admin_required
    @inject
    def get_resource_collection(
        self,
        user_respository: AbstractUserRepository = Provide["user_repository"],
        *args,
        **kwargs,
    ):
        users = list(user_respository.list())
        count = len(users)
        # TODO: Filtering
        # users = (
        #     session.query(SUser)
        #     .order_by(SUser.username.desc())
        #     .limit(kwargs["limit"])
        #     .offset(kwargs["offset"])
        # )
        return users, count, {}

    def create_resource(
        self,
        resource,
        user_repository: AbstractUserRepository = Provide["user_repository"],
        *args,
        **kwargs,
    ):
        try:
            user = User(
                name=resource["name"],
                email=resource["email"],
                username=resource["username"],
            )
            user.set_password(resource["password"])
            user_repository.add(user)
        except KeyViolationException as e:
            raise ResourceConflictException(str(e))
        return user, {}


class UserDetail(resource.ResourceDetail):
    resource_schema = UserSchema

    @login_required
    def get_resource(
        self,
        user_id,
        user_repository: AbstractUserRepository = Provide["user_repository"],
        *args,
        **kwargs,
    ):
        if current_user.id != user_id and not current_user.staff:
            raise Unauthorized(None)
        user = user_repository.get(user_id)
        if user is None:
            raise ResourceNotFoundException(f"No resource user with ID {user_id}")
        return user, {}

    @admin_required
    def patch_resource(
        self,
        resource,
        user_id,
        user_repository: AbstractUserRepository = Provide["user_repository"],
        *args,
        **kwargs,
    ):
        del resource["id"]
        allowed_fields = {"admin", "staff"}
        user = user_repository.get(user_id)
        if not user:
            raise Unauthorized(None)
        if set(resource.keys()) - allowed_fields:
            raise BadRequest(
                f"Cannot PATCH fields {set(resource.keys()) - allowed_fields}"
            )
        for key, value in resource.items():
            setattr(user, key, value)
        user_repository.update(user)
        return user, {}
