"""
The JSON:API spec for this endpoint looks like:
{
    "data": {
        "type": "watch_list",
        "relationships": {
            "owner": {
                "data": {
                    "type": "user",
                    "id": "1"
                }
            }
        },
        "id": str,
        "attributes": {
            "owner_id": int
            "name": str,
            "description": str,
            "slack_channel": str,
            "created_at": datetime.datetime,
            "enabled": bool,
            "objects": [
                {
                    "comment": str,
                    "right_ascension": float,
                    "declination": float,
                    "radius": float,
                    "id": str,
                },
                ...
                {
                    "comment": str,
                    "right_ascension": float,
                    "declination": float,
                    "radius": float,
                    "id": str,
                }
            ]
        }
    }
}

"""

from marshmallow import Schema as PlainSchema
from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Relationship, Schema


class WatchObjectSchema(PlainSchema):
    right_ascension = fields.Float(attribute="location.ra.deg")
    declination = fields.Float(attribute="location.dec.deg")
    radius = fields.Float(attribute="radius.deg")  # degrees
    comment = fields.Str(attribute="name")


class WatchListSchema(Schema):
    class Meta:
        type_ = "watch_list"
        self_view = "watch_list_detail"
        self_view_kwargs = {"watch_list_id": "<id>"}
        self_view_many = "watch_list_list"

    id = fields.String()
    name = fields.String(required=True)
    created_at = fields.DateTime(dump_only=True)
    slack_channel = fields.String()
    enabled = fields.Boolean()
    description = fields.String()
    objects = fields.List(fields.Nested(WatchObjectSchema))
    owner = Relationship(
        related_view="user_detail",
        related_view_kwargs={"user_id": "<owner_id>"},
        schema="UserSchema",
        type_="user",
        many=False,
        attribute="owner_id",
        include_resource_linkage=True,
    )
