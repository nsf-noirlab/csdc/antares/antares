"""
API Resource exposing user watch lists.

Routes
----------

/users/<user_id:int>/watch_lists (GET, POST)
/watch_lists/<watch_list_id:int> (GET, DELETE)

Notes
----------

These routes expose the "watch list" and "watch objects" as a single
"watch list" resource. A watch list resource has, among other fields,
an `object` property that comprises a list of objects the user would
like to watch.

These routes are additionally interesting because they have to
fetch/create/delete data from both the C* and SQL databases.
"""
import uuid

from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from dependency_injector.wiring import Provide, inject
from flask_jwt_extended import current_user

from antares.adapters.repository import (
    AbstractWatchListRepository,
    AbstractWatchObjectRepository,
)
from antares.domain.models import WatchList, WatchObject
from antares.entrypoints.http_api.common import resource
from antares.entrypoints.http_api.common.decorators import login_required
from antares.entrypoints.http_api.common.exceptions import (
    ResourceConflictException,
    ResourceNotFoundException,
    Unauthorized,
)
from antares.exceptions import KeyViolationException

from .schemas import WatchListSchema


class WatchListList(resource.ResourceList):
    resource_schema = WatchListSchema

    @login_required
    @inject
    def get_resource_collection(
        # fmt: off
        self,
        watch_list_repository: AbstractWatchListRepository = Provide["watch_list_repository"],
        *args,
        **kwargs,
        # fmt: on
    ):
        user_id = kwargs.get("user_id")
        if current_user.id != user_id:
            raise Unauthorized(None)
        if user_id is None:
            raise ResourceNotFoundException("user_id can't be None")
        watch_lists = list(watch_list_repository.list_by_owner_id(user_id))
        count = len(watch_lists)
        return watch_lists, count, {}

    @login_required
    @inject
    def create_resource(
        self,
        resource: dict,
        watch_list_repository: AbstractWatchListRepository = Provide[
            "watch_list_repository"
        ],
        watch_object_repository: AbstractWatchObjectRepository = Provide[
            "watch_object_repository"
        ],
        *args,
        **kwargs,
    ):
        if (
            current_user.id != kwargs.get("user_id")
            or kwargs.get("user_id") != resource["owner_id"]
        ):
            raise Unauthorized(None)
        objects = resource.pop("objects", [])
        watch_list = WatchList(**resource)
        watch_objects = [
            WatchObject(
                watch_list_id=watch_list.id,
                name=d["name"],
                location=SkyCoord(
                    ra=d["location"]["ra"]["deg"],
                    dec=d["location"]["dec"]["deg"],
                    unit=u.deg,
                ),
                radius=Angle(angle=d["radius"]["deg"], unit="deg"),
            )
            for d in objects
        ]
        try:
            watch_list_repository.add(watch_list)
        except KeyViolationException as e:
            raise ResourceConflictException(str(e))
        for watch_object in watch_objects:
            watch_object_repository.add(watch_object)
        return watch_list, {}


class WatchListDetail(resource.ResourceDetail):
    resource_schema = WatchListSchema
    methods = ["GET", "DELETE"]

    @login_required
    @inject
    def get_resource(
        # fmt: off
        self,
        watch_list_id: uuid.UUID,
        watch_list_repository: AbstractWatchListRepository = Provide["watch_list_repository"],
        watch_object_repository: AbstractWatchObjectRepository = Provide["watch_object_repository"],
        *args,
        **kwargs,
        # fmt: on
    ):
        watch_list = watch_list_repository.get(watch_list_id)
        if watch_list is None or watch_list.owner_id != current_user.id:
            raise Unauthorized(None)
        watch_objects = watch_object_repository.list_by_watch_list_id(watch_list_id)
        watch_objects = list(watch_objects)
        # Because we're building a compound object from two repositories this return
        # type is a bit messy.
        return (
            {
                "id": watch_list.id,
                "name": watch_list.name,
                "created_at": watch_list.created_at,
                "slack_channel": watch_list.slack_channel,
                "enabled": watch_list.enabled,
                "description": watch_list.description,
                "owner_id": watch_list.owner_id,
                "objects": watch_objects,
            },
            {},
        )

    @login_required
    @inject
    def delete_resource(
        # fmt: off
        self,
        watch_list_id,
        watch_list_repository: AbstractWatchListRepository = Provide["watch_list_repository"],
        *args,
        **kwargs,
        # fmt: on
    ):
        watch_list = watch_list_repository.get(watch_list_id)
        if watch_list is None or watch_list.owner_id != current_user.id:
            raise Unauthorized(None)
        watch_list_repository.delete(watch_list.id)
        return {}
