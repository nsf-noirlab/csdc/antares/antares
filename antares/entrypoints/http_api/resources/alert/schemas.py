from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Relationship, Schema


class AlertSchema(Schema):
    class Meta:
        type_ = "alert"
        self_view = "alert_detail"
        self_view_kwargs = {"alert_id": "<id>"}
        self_view_many = "alert_list"

    id = fields.Str()
    mjd = fields.Float()
    properties = fields.Function(
        lambda alert: {**alert.properties, **alert.normalized_properties}
    )
    grav_wave_events = fields.List(fields.Dict())
    processed_at = fields.DateTime(attribute="created_at")
    thumbnails = Relationship(
        related_view="alert_thumbnail_list",
        related_view_kwargs={"alert_id": "<id>"},
        schema="AlertThumbnailSchema",
        type_="alert_thumbnail",
        many=True,
    )


class AlertThumbnailSchema(Schema):
    class Meta:
        type_ = "alert_thumbnail"

    id = fields.Str()
    filename = fields.Str()
    filemime = fields.Str()
    src = fields.Str()
    thumbnail_type = fields.Function(lambda thumbnail: thumbnail.thumbnail_type.value)
    resource_meta = fields.Dict()
