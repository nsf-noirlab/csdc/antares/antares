from http import HTTPStatus

from dependency_injector.wiring import Provide, inject
from flask import abort

from antares.adapters.repository import (
    AbstractAlertRepository,
    AbstractAlertThumbnailRepository,
)
from antares.entrypoints.http_api.common import resource

from .schemas import AlertSchema, AlertThumbnailSchema


class AlertList(resource.ResourceListReadOnly):
    resource_schema = AlertSchema

    @inject
    def get_resource_collection(
        self,
        locus_id: str,
        alert_repository: AbstractAlertRepository = Provide["alert_repository"],
        **kwargs,
    ):
        alerts = list(alert_repository.list_by_locus_id(locus_id))
        return alerts, len(alerts), {"count": len(alerts)}


class AlertDetail(resource.ResourceDetailReadOnly):
    resource_schema = AlertSchema

    @inject
    def get_resource(
        self,
        alert_id: str,
        alert_repository: AbstractAlertRepository = Provide["alert_repository"],
        **kwargs,
    ):
        if (alert := alert_repository.get(alert_id)) is None:
            abort(HTTPStatus.NOT_FOUND)
        return alert, None


class AlertThumbnailList(resource.ResourceListReadOnly):
    resource_schema = AlertThumbnailSchema

    @inject
    def get_resource_collection(
        # fmt: off
        self,
        alert_id: str,
        alert_thumbnail_repository: AbstractAlertThumbnailRepository = Provide["alert_thumbnail_repository"],
        **kwargs
        # fmt: on
    ):
        thumbnails = alert_thumbnail_repository.list_by_alert_id(alert_id)
        thumbnails = list(thumbnails)
        return thumbnails, len(thumbnails), {"count": len(thumbnails)}
