import datetime
import decimal
import itertools
from typing import Any

from astropy import units as u
from astropy.coordinates import SkyCoord
from dependency_injector.wiring import Provide, inject
from flask import request

from antares import utils
from antares.adapters.repository import AbstractCatalogObjectRepository
from antares.domain.models import CatalogObject
from antares.entrypoints.http_api.common.exceptions import BadRequest
from antares.entrypoints.http_api.common.resource import ResourceListReadOnly
from antares.observability.metrics.base import AbstractMetrics

from .schemas import CatalogSampleSchema


def catalog_object_to_json_schema(catalog_object: CatalogObject) -> dict[str, Any]:
    properties = {**catalog_object.properties}
    # Handle conversion to JSON-friendly types
    for key in properties:
        if isinstance(properties[key], datetime.datetime):
            properties[key] = utils.format_dt(properties[key])
        elif isinstance(properties[key], decimal.Decimal):
            properties[key] = float(properties[key])
    return {
        "object_id": catalog_object.id,
        "catalog_name": catalog_object.catalog_name,
        "data": properties,
    }


def generate_catalog_sample(
    catalog_object_repository: AbstractCatalogObjectRepository,
    n: int,
) -> tuple[list[dict[str, Any]], int]:
    catalog_samples = []
    for catalog in catalog_object_repository.list_catalogs():
        catalog_objects = catalog_object_repository.list_by_catalog_id(catalog.id)
        for catalog_object in itertools.islice(catalog_objects, n):
            catalog_samples.append(catalog_object_to_json_schema(catalog_object))
    count = len(catalog_samples)
    return catalog_samples, count


class CatalogSampleList(ResourceListReadOnly):
    resource_schema = CatalogSampleSchema

    @inject
    def get_resource_collection(
        # fmt: off
        self,
        catalog_object_repository: AbstractCatalogObjectRepository = Provide["catalog_object_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        """
        Get catalog sample data and convert to CatalogSampleSchema format.
        """
        n = request.args.get("n", default=5, type=int)
        if n >= 100:
            raise BadRequest("Sample number should be less than or equal to 100")
        catalog_samples, count = generate_catalog_sample(
            catalog_object_repository=catalog_object_repository,
            n=n,
        )
        return catalog_samples, count, None


class CatalogSearchList(ResourceListReadOnly):
    resource_schema = CatalogSampleSchema

    @inject
    def get_resource_collection(
        # fmt: off
        self,
        ra: float,
        dec: float,
        catalog_object_repository: AbstractCatalogObjectRepository = Provide["catalog_object_repository"],
        metrics: AbstractMetrics = Provide["metrics"],
        *args,
        **kwargs
        # fmt: on
    ):
        """
        Get matched catalogs data in the ra,dec location using the CatalogSampleSchema format.
        """
        catalog_objects = catalog_object_repository.list_by_location(
            SkyCoord(ra=ra, dec=dec, unit=u.deg),
            metrics,
        )
        catalog_objects_combined = []
        for catalog_object in catalog_objects:
            catalog_objects_combined.append(
                catalog_object_to_json_schema(catalog_object)
            )
        count = len(catalog_objects_combined)
        return catalog_objects_combined, count, None
