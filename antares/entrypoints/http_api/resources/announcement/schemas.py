from marshmallow import validate
from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Schema


class AnnouncementSchema(Schema):
    class Meta:
        type_ = "announcement"
        self_view_many = "announcement_list"

    id = fields.Integer(as_string=True)
    active = fields.Bool()
    variant = fields.Str(
        validate=validate.OneOf(
            [
                "primary",
                "secondary",
                "success",
                "danger",
                "warning",
                "info",
                "light",
                "dark",
            ]
        )
    )
    message = fields.Str()
    created_at = fields.DateTime()
