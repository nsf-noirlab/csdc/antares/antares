from dependency_injector.wiring import Provide, inject
from sqlalchemy.exc import IntegrityError

from antares.adapters.repository import AbstractAnnouncementRepository
from antares.domain.models import Announcement
from antares.entrypoints.http_api.common.decorators import admin_required
from antares.entrypoints.http_api.common.exceptions import (
    BadRequest,
    ResourceConflictException,
    ResourceNotFoundException,
)
from antares.entrypoints.http_api.common.resource import ResourceDetail, ResourceList
from antares.exceptions import KeyViolationException

from .schemas import AnnouncementSchema


class AnnouncementList(ResourceList):
    resource_schema = AnnouncementSchema

    def get_resource_collection(
        # fmt: off
        self,
        announcement_repository: AbstractAnnouncementRepository = Provide["announcement_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        """
        Get all announcements
        """
        announcements = announcement_repository.list(query_filters=kwargs.get("filter"))
        announcements = list(announcements)
        return announcements, len(announcements), {}

    @admin_required
    @inject
    def create_resource(
        # fmt: off
        self,
        resource: dict,
        announcement_repository: AbstractAnnouncementRepository = Provide["announcement_repository"],
        *args,
        **kwargs,
        # fmt: on
    ):
        announcement = Announcement(**resource)
        try:
            announcement_repository.add(announcement)
            return announcement, None
        except KeyViolationException as error:
            db_error: IntegrityError = error.args[0]
            if db_error.orig is not None:
                raise ResourceConflictException(str(db_error.orig.args))
            raise ResourceConflictException("Can't create announcement")


class AnnouncementDetail(ResourceDetail):
    resource_schema = AnnouncementSchema
    methods = ["GET", "PATCH"]

    @inject
    def get_resource(
        # fmt: off
        self,
        announcement_id: int,
        announcement_repository: AbstractAnnouncementRepository = Provide["announcement_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        announcement = announcement_repository.get(announcement_id)
        if announcement is None:
            raise ResourceNotFoundException(
                f"No resource announcement with ID {announcement_id}"
            )
        return announcement, None

    @admin_required
    @inject
    def patch_resource(
        # fmt: off
        self,
        resource: dict,
        announcement_id: int,
        announcement_repository: AbstractAnnouncementRepository = Provide["announcement_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        allowed_fields = {"active"}
        del resource["id"]
        if set(resource.keys()) - allowed_fields:
            raise BadRequest(
                f"Cannot PATCH fields {set(resource.keys()) - allowed_fields}"
            )
        announcement = announcement_repository.get(announcement_id)
        if not announcement:
            raise ResourceNotFoundException(
                f"No resource announcement with ID {announcement_id}"
            )
        for key, value in resource.items():
            setattr(announcement, key, value)
        announcement_repository.update(announcement)
        return announcement, {}
