from marshmallow_jsonapi import fields

from antares.domain.models import GravWaveNoticeTypes
from antares.entrypoints.http_api.common.schema import Schema


class GravWaveNoticeSchema(Schema):
    class Meta:
        type_ = "grav_wave_notice"

    id = fields.Int()
    gracedb_id = fields.Str()
    notice_type = fields.Enum(GravWaveNoticeTypes)
    notice_datetime = fields.DateTime()
    event_datetime = fields.DateTime()
    false_alarm_rate = fields.Float()
    skymap_base64 = fields.Str()
    external_coinc = fields.Dict()
    full_notice = fields.Dict()
    version_id = fields.Int()
