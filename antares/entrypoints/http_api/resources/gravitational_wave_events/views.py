import datetime

from dependency_injector.wiring import Provide, inject
from flask import request

from antares.adapters.repository import AbstractGravWaveRepository
from antares.entrypoints.http_api.common.exceptions import (
    BadRequest,
    ResourceNotFoundException,
)
from antares.entrypoints.http_api.common.resource import (
    ResourceDetailReadOnly,
    ResourceListReadOnly,
)

from .schemas import GravWaveNoticeSchema


class LatestGravWaveNoticeDetail(ResourceDetailReadOnly):
    resource_schema = GravWaveNoticeSchema

    @inject
    def get_resource(
        self,
        gracedb_id: str,
        grav_wave_repository: AbstractGravWaveRepository = Provide[
            "grav_wave_repository"
        ],
        *args,
        **kwargs,
    ):
        if notice := grav_wave_repository.get_latest_by_gracedb_id(gracedb_id):
            return notice, None
        raise ResourceNotFoundException(
            f"No gravitational wave found with gracedb_id {gracedb_id}"
        )


class GravWaveNoticeDetail(ResourceDetailReadOnly):
    resource_schema = GravWaveNoticeSchema

    @inject
    def get_resource(
        self,
        gracedb_id: str,
        notice_datetime: datetime.datetime,
        grav_wave_repository: AbstractGravWaveRepository = Provide[
            "grav_wave_repository"
        ],
        *args,
        **kwargs,
    ):
        if notice_id := grav_wave_repository.get_id(gracedb_id, notice_datetime):
            if notice := grav_wave_repository.get(notice_id):
                return notice, None
        raise ResourceNotFoundException(
            f"No gravitational wave found with gracedb_id {gracedb_id} and notice_datetime {notice_datetime}"
        )


class LatestGravWaveNoticeList(ResourceListReadOnly):
    resource_schema = GravWaveNoticeSchema

    @inject
    def get_resource_collection(
        self,
        grav_wave_repository: AbstractGravWaveRepository = Provide[
            "grav_wave_repository"
        ],
        *args,
        **kwargs,
    ):
        if ids_argument := request.args.get("ids", type=str):
            ids = ids_argument.split(",")
            grav_wave_notices = list(
                grav_wave_repository.get_latest_by_gracedb_ids(ids)
            )
            return grav_wave_notices, len(grav_wave_notices), None
        else:
            raise BadRequest(
                "This endpoint receives comma separated ids in the query. Example: <url>?ids=id1,id2,id3"
            )
