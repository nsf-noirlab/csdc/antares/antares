from typing import Optional

from dependency_injector.wiring import Provide, inject
from flask_jwt_extended import current_user

from antares.adapters.repository import (
    AbstractLocusAnnotationRepository,
    AbstractLocusRepository,
)
from antares.domain.models import LocusAnnotation
from antares.entrypoints.http_api.common import resource
from antares.entrypoints.http_api.common.decorators import login_required
from antares.entrypoints.http_api.common.exceptions import (
    BadRequest,
    ResourceConflictException,
    ResourceNotFoundException,
    Unauthorized,
)
from antares.exceptions import KeyViolationException

from .schemas import LocusAnnotationSchema


class LocusAnnotationList(resource.ResourceList):
    resource_schema = LocusAnnotationSchema

    @login_required
    @inject
    def get_resource_collection(
        # fmt: off
        self,
        locus_repository: AbstractLocusRepository = Provide["locus_repository"],
        locus_annotation_repository: AbstractLocusAnnotationRepository = Provide["locus_annotation_repository"],
        **kwargs
        # fmt: on
    ):
        user_id = kwargs.get("user_id")
        if not current_user.admin and current_user.id != user_id:
            raise Unauthorized(None)
        if user_id is None:
            raise ResourceNotFoundException("user_id can't be None")
        locus_annotations = locus_annotation_repository.list_by_owner_id(user_id)
        locus_annotations = list(locus_annotations)

        # TODO: filtering
        # annotations, count = query_collection(
        #     session,
        #     SLocusAnnotation,
        #     limit=kwargs["limit"],
        #     offset=kwargs["offset"],
        #     filters=(
        #         SLocusAnnotation.owner_id == kwargs.get("user_id"),
        #         *build_query_filters(SLocusAnnotation, kwargs["filter"]),
        #     ),
        # )

        count = len(locus_annotations)
        return locus_annotations, count, None

    @login_required
    @inject
    def create_resource(
        # fmt: off
        self,
        resource: dict,
        locus_annotation_repository: AbstractLocusAnnotationRepository = Provide["locus_annotation_repository"],
        *args,
        **kwargs,
        # fmt: on
    ) -> tuple[LocusAnnotation, Optional[dict]]:
        if (
            current_user.id != kwargs.get("user_id")
            or kwargs.get("user_id") != resource["owner_id"]
        ):
            raise Unauthorized(None)
        locus_annotation = LocusAnnotation(**resource)
        try:
            locus_annotation_repository.add(locus_annotation)
        except KeyViolationException as e:
            raise ResourceConflictException(str(e))
        return locus_annotation, None


class LocusAnnotationDetail(resource.ResourceDetail):
    methods = ["PATCH"]
    resource_schema = LocusAnnotationSchema

    @login_required
    def patch_resource(
        # fmt: off
        self,
        resource: dict,
        locus_annotation_id: int,
        locus_annotation_repository: AbstractLocusAnnotationRepository = Provide["locus_annotation_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        del resource["id"]
        allowed_fields = {"comment", "favorite"}
        if set(resource.keys()) - allowed_fields:
            raise BadRequest(
                f"Cannot PATCH fields {set(resource.keys()) - allowed_fields}"
            )
        locus_annotation = locus_annotation_repository.get(locus_annotation_id)
        if locus_annotation is None or locus_annotation.owner_id != current_user.id:
            raise Unauthorized(None)
        for key, value in resource.items():
            setattr(locus_annotation, key, value)
        locus_annotation_repository.update(locus_annotation)
        return locus_annotation, {}
