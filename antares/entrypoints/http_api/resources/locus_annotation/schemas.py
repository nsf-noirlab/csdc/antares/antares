from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Relationship, Schema


class LocusAnnotationSchema(Schema):
    class Meta:
        type_ = "locus_annotation"

    id = fields.Integer(attribute="id", as_string=True)
    comment = fields.String()
    favorited = fields.Boolean(attribute="favorite")
    locus = Relationship(
        related_view="locus_detail",
        related_view_kwargs={"locus_id": "<locus_id>"},
        schema="LocusSchema",
        type_="locus",
        many=False,
        attribute="locus_id",
        include_resource_linkage=True,
    )
    owner = Relationship(
        related_view="user_detail",
        related_view_kwargs={"user_id": "<owner_id>"},
        schema="UserSchema",
        type_="user",
        many=False,
        attribute="owner_id",
        include_resource_linkage=True,
    )
