from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Schema


class LociPropertySchema(Schema):
    class Meta:
        type_ = "locus_property"
        self_view = "locus_property_detail"
        self_view_kwargs = {"locus_property_name": "<name>"}
        self_view_many = "locus_property_list"

    id = fields.Str(attribute="name")
    type = fields.Str()
    origin = fields.Str()
    # filter_version = Relationship(
    #     related_view="filter_version_detail",
    #     related_view_kwargs={
    #         "filter_id": "<id>",
    #         "filter_version_id": "<latest_version_id>",
    #     },
    #     schema="FilterVersionSchema",
    #     type_="filter_version",
    # )
    description = fields.Str()
    es_mapping = fields.Str()


class LocusPropertySchema(Schema):
    class Meta:
        type_ = "locus_properties"
        self_view = "locus_property_detail"
        self_view_kwargs = {"locus_id": "<id>"}

    id = fields.Str()
    properties = fields.Dict()
