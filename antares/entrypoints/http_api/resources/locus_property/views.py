from dependency_injector.wiring import Provide

from antares.adapters.repository import AbstractLocusRepository
from antares.deprecated.sql.schema import SLocusProperty
from antares.entrypoints.http_api.common.exceptions import ResourceNotFoundException
from antares.entrypoints.http_api.common.resource import (
    ResourceDetailReadOnly,
    ResourceListReadOnly,
)
from antares.entrypoints.http_api.common.sql_utils import query_collection
from antares.external.sql.engine import session as start_session

from .schemas import LociPropertySchema, LocusPropertySchema


class LociPropertyList(ResourceListReadOnly):
    resource_schema = LociPropertySchema

    def get_resource_collection(self, *args, **kwargs):
        with start_session() as session:
            locus_properties, count = query_collection(
                session,
                SLocusProperty,
                limit=kwargs["limit"],
                offset=kwargs["offset"],
                sort=kwargs["sort"],
            )
            return locus_properties, count, None


class LocusPropertyList(ResourceListReadOnly):
    resource_schema = LocusPropertySchema

    def get_resource_collection(
        self,
        locus_id: str,
        locus_repository: AbstractLocusRepository = Provide["locus_repository"],
        *args,
        **kwargs,
    ):
        locus = locus_repository.get(locus_id)
        if locus is None:
            raise ResourceNotFoundException(f"No resource locus with ID {locus_id}")
        return [locus], len(locus.properties), None


class LocusPropertyDetail(ResourceDetailReadOnly):
    resource_schema = LocusPropertySchema

    def get_resource(self, locus_property_name, *args, **kwargs):
        with start_session() as session:
            locus_property = session.query(SLocusProperty).get(locus_property_name)
            if locus_property is None:
                raise ResourceNotFoundException(
                    f"No resource locus_property with ID {locus_property_name}"
                )
            return locus_property, None
