import datetime
import decimal
import io
from http import HTTPStatus

import flask_restful
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from dependency_injector.wiring import Provide, inject
from elasticsearch_dsl import Search
from flask import abort, request, send_file, url_for

from antares import utils
from antares.adapters.repository import (
    AbstractAlertRepository,
    AbstractAlertThumbnailRepository,
    AbstractCatalogObjectRepository,
    AbstractLocusRepository,
)
from antares.entrypoints.http_api.common import resource
from antares.entrypoints.http_api.common.elasticsearch import (
    transform_sky_distance_query,
)
from antares.entrypoints.http_api.common.exceptions import SeeOther
from antares.external.elasticsearch.ingest import connect as es_connect
from antares.observability.metrics.base import AbstractMetrics

from ..alert.schemas import AlertThumbnailSchema
from ..catalog.schemas import CatalogEntrySchema
from . import finder_chart
from .schemas import LocusListingSchema, LocusSchema, LocusStatisticsSchema


def transform_elasticsearch_query(query):
    """Applys transformations to an Elasticsearch query."""
    transform_sky_distance_query(query, "htm16", 16)


def serialize_coordinates(coordinates: SkyCoord) -> dict:
    """
    Takes ra, dec and returns a dictionary of coordinates in different formats.

    Returns
    ----------
    {
        "icrs": {
            "ra": ra,
            "dec": dec,
        },
        "galactic": {
            "l": ...,
            "b": ...,
        },
    }
    """
    return {
        "icrs": {"ra": coordinates.ra.deg, "dec": coordinates.dec.deg},
        "galactic": {"l": coordinates.galactic.l.deg, "b": coordinates.galactic.b.deg},
    }


class LocusFinderChart(flask_restful.Resource):
    methods = ["POST", "GET"]

    @inject
    def post(
        self,
        locus_id,
        frontend_base_url: str = Provide["config.frontend.base_url"],
        alert_repository: AbstractAlertRepository = Provide["alert_repository"],
        *args,
        **kwargs,
    ):
        if "application/pdf" not in request.accept_mimetypes:
            return None, HTTPStatus.NOT_ACCEPTABLE
        buffer = io.BytesIO()
        alerts = alert_repository.list_by_locus_id(locus_id)
        finder_chart.render_report(
            locus_id, alerts, buffer, "r", frontend_base_url=frontend_base_url
        )
        buffer.seek(0)
        return send_file(
            buffer,
            mimetype="application/pdf",
            as_attachment=True,
            attachment_filename=f"antares-{locus_id}.pdf",
        )


class LocusDetail(resource.ResourceDetailReadOnly):
    resource_schema = LocusSchema

    @inject
    def get_resource(
        # fmt: off
        self,
        locus_id: str,
        locus_repository: AbstractLocusRepository = Provide["locus_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        if (locus := locus_repository.get(locus_id)) is None:
            abort(HTTPStatus.NOT_FOUND)
        if "replaced_by" in locus.properties:
            raise SeeOther(
                url_for("locus_detail", locus_id=locus.properties["replaced_by"])
            )
        # locus.lightcurve = rtdb.get_lightcurve(locus_id)
        setattr(
            locus,
            "resource_meta",
            {"coordinates": serialize_coordinates(locus.location)},
        )
        return locus, {}


class LocusStatisticsDetail(resource.ResourceDetailReadOnly):
    resource_schema = LocusStatisticsSchema

    @inject
    def get_resource(
        self,
        index: str = Provide["config.archive.ingestion.storage.parameters.index"],
        *args,
        **kwargs,
    ):
        s = Search()
        s.aggs.bucket("tags", "terms", field="tags", size=100)
        s.aggs.bucket("catalogs", "terms", field="catalogs", size=100)
        s.aggs.metric("max_num_measurements", "max", field="properties.num_mag_values")
        s.aggs.metric("min_num_measurements", "min", field="properties.num_mag_values")
        s = s.extra(size=0)
        # Allow the caller to pass an ES query body to the API. We use "query" in the
        # strictest sense of the term in an ES request. That is, users cannot pass
        # custom aggregations, sizes, or other search parameters. They may only pass
        # the bit that lives under the top-level "query" field in the ES request body.
        if kwargs["elasticsearch_query"] and kwargs["elasticsearch_query"].get("query"):
            query = kwargs["elasticsearch_query"]["query"]
            transform_elasticsearch_query(query)
            s.update_from_dict({"query": query})
        # Now we search and format the returned documents appropriately.
        body = s.to_dict()
        client = es_connect()
        search_result = client.search(index=index, body=body)
        statistics = {
            "catalogs": {
                bucket["key"]: bucket["doc_count"]
                for bucket in search_result["aggregations"]["catalogs"]["buckets"]
            },
            "tags": {
                bucket["key"]: bucket["doc_count"]
                for bucket in search_result["aggregations"]["tags"]["buckets"]
            },
            "min_num_measurements": int(
                search_result["aggregations"]["min_num_measurements"]["value"] or 0
            ),
            "max_num_measurements": int(
                search_result["aggregations"]["max_num_measurements"]["value"] or 0
            ),
        }
        return statistics, None


class LocusList(resource.ResourceListReadOnly):
    resource_schema = LocusListingSchema

    @inject
    def get_resource_collection(
        # fmt: off
        self,
        alert_thumbnail_repository: AbstractAlertThumbnailRepository = Provide["alert_thumbnail_repository"],
        index: str = Provide["config.archive.ingestion.storage.parameters.index"],
        *args,
        **kwargs
        # fmt: on
    ):
        s = Search()

        # Pagination and sorting
        limit = kwargs["limit"]
        offset = kwargs["offset"]
        s = s[offset : offset + limit]
        if kwargs["sort"]:
            s = s.sort({kwargs["sort"]["field"]: {"order": kwargs["sort"]["order"]}})

        # Allow the caller to pass an ES query body to the API. We use "query" in the
        # strictest sense of the term in an ES request. That is, users cannot pass
        # custom aggregations, sizes, or other search parameters. They may only pass
        # the bit that lives under the top-level "query" field in the ES request body.
        if kwargs["elasticsearch_query"] and kwargs["elasticsearch_query"].get("query"):
            query = kwargs["elasticsearch_query"]["query"]
            transform_elasticsearch_query(query)
            s.update_from_dict({"query": query})

        # Some documents in the index have a "properties.replaced_by" field that indicates
        # they have been merged into another locus. We favor the newer locus so we will exclude
        # any documents that have this field.
        s = s.exclude("exists", field="properties.replaced_by")

        # Now we search and format the returned documents appropriately.
        body = s.to_dict()
        client = es_connect()
        search_result = client.search(index=index, body=body)
        docs = search_result["hits"]["hits"]
        count = search_result["hits"]["total"]["value"]
        loci = []
        for doc in docs:
            locus = doc["_source"]
            locus["resource_meta"] = {
                "newest_thumbnails": [
                    AlertThumbnailSchema().dump(thumbnail)
                    for thumbnail in alert_thumbnail_repository.list_by_alert_id(
                        locus["properties"]["newest_alert_id"]
                    )
                ]
            }
            # Resolve schema differences between the ES response and what our (de)serialization
            # library expects.
            locus["id"] = locus.pop("locus_id")
            locus["location"] = SkyCoord(
                ra=locus.pop("ra"), dec=locus.pop("dec"), unit=u.deg
            )
            loci.append(locus)

        return loci, count, None


class LocusCatalogMatchList(resource.ResourceListReadOnly):
    resource_schema = CatalogEntrySchema

    @inject
    def get_resource_collection(
        # fmt: off
        self,
        locus_id,
        locus_repository: AbstractLocusRepository = Provide["locus_repository"],
        catalog_object_repository: AbstractCatalogObjectRepository = Provide["catalog_object_repository"],
        metrics: AbstractMetrics = Provide["metrics"],
        *args,
        **kwargs,
        # fmt: on
    ):
        # Try and find the locus in our database. Return 404 NOT FOUND if it isn't there
        # or a 303 SEE OTHER if it has the `properties.replaced_by` property from being
        # merged with another locus.
        locus = locus_repository.get(locus_id)
        if locus is None:
            abort(HTTPStatus.NOT_FOUND)
        if "replaced_by" in locus.properties:
            raise SeeOther(
                url_for(
                    "locus_catalog_match_list", locus_id=locus.properties["replaced_by"]
                )
            )

        # Extract and format matches
        matches = []
        catalog_objects = catalog_object_repository.list_by_location(
            locus.location, metrics
        )
        for catalog_object in catalog_objects:
            catalog = catalog_object_repository.get_catalog_by_catalog_id(
                catalog_object.catalog_id
            )
            if catalog is None:
                continue

            unique_id = f"{catalog_object.catalog_name}:{catalog_object.id}"
            radius = (
                catalog_object.radius
                if catalog_object.radius is not None
                else Angle("1s")
            )
            separation = catalog_object.location.separation(locus.location).arcsecond
            properties = {**catalog_object.properties}
            # Handle conversion to JSON-friendly types
            for key in properties:
                if isinstance(properties[key], datetime.datetime):
                    properties[key] = utils.format_dt(properties[key])
                elif isinstance(properties[key], decimal.Decimal):
                    properties[key] = float(properties[key])
            matches.append(
                {
                    "catalog_entry_id": unique_id,
                    "catalog_id": catalog_object.catalog_id,
                    "dec": catalog_object.location.dec.deg,
                    "object_id": catalog_object.id,
                    "object_name": catalog_object.name,
                    "properties": properties,
                    "ra": catalog_object.location.ra.deg,
                    "resource_meta": {
                        "catalog_name": catalog.name,
                        "radius": radius.arcsecond,
                        "separation": separation,
                    },
                }
            )
        return matches, len(matches), {}
