import io
import uuid
from collections.abc import Mapping
from typing import Any, Optional

from astropy import timeseries
from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Relationship, Schema


class TimeSeries(fields.Field):
    """
    A field to help (de)serialize `astropy.TimeSeries` objects.

    Parameters
    ----------
    include: Optional[Collection[str]] (default, None)
        A list of columns to include in the output. If `None` all columns are included.

    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _serialize(
        self, value: timeseries.TimeSeries, attr: str, obj: Any, **kwargs
    ) -> str:
        if not value:
            return ""
        buffer = io.StringIO()
        value.write(buffer, format="csv")
        return buffer.getvalue()

    def _deserialize(
        self,
        value: str,
        attr: Optional[str],
        data: Optional[Mapping[str, Any]],
        **kwargs
    ) -> timeseries.TimeSeries:
        pass


class LocusStatisticsSchema(Schema):
    class Meta:
        type_ = "locus_listing_statistic"

    id = fields.Str(dump_default=uuid.uuid4)
    catalogs = fields.Dict(keys=fields.Str(), values=fields.Int())
    tags = fields.Dict(keys=fields.Str(), values=fields.Int())
    min_num_measurements = fields.Int()
    max_num_measurements = fields.Int()


class LocusListingSchema(Schema):
    class Meta:
        type_ = "locus_listing"
        self_view_many = "locus_list"

    id = fields.Str()
    htm16 = fields.Int()
    ra = fields.Float(attribute="location.ra.deg")
    dec = fields.Float(attribute="location.dec.deg")
    grav_wave_events = fields.List(fields.Str())
    properties = fields.Dict()
    locus = Relationship(
        related_view="locus_detail",
        related_view_kwargs={"locus_id": "<id>"},
        schema="LocusSchema",
        type_="locus",
        many=False,
        include_resource_linkage=True,
    )
    alerts = Relationship(
        related_view="alert_list",
        related_view_kwargs={"locus_id": "<id>"},
        schema="AlertSchema",
        type_="alert",
        many=True,
    )
    newest_alert = Relationship(
        related_view="alert_detail",
        related_view_kwargs={"alert_id": "<properties.newest_alert_id>"},
        schema="AlertSchema",
        type_="alert",
        many=False,
        include_resource_linkage=True,
    )
    oldest_alert = Relationship(
        related_view="alert_detail",
        related_view_kwargs={"alert_id": "<properties.oldest_alert_id>"},
        schema="AlertSchema",
        type_="alert",
        many=False,
        include_resource_linkage=True,
    )
    brightest_alert = Relationship(
        related_view="alert_detail",
        related_view_kwargs={"alert_id": "<properties.brightest_alert_id>"},
        schema="AlertSchema",
        type_="alert",
        many=False,
        include_resource_linkage=True,
    )
    tags = fields.List(fields.Str())
    catalogs = fields.List(fields.Str())
    resource_meta = fields.ResourceMeta()
    document_meta = fields.DocumentMeta()


class LocusSchema(Schema):
    class Meta:
        type_ = "locus"
        self_view = "locus_detail"
        self_view_kwargs = {"locus_id": "<id>"}
        self_view_many = "locus_list"

    id = fields.Str()
    htm16 = fields.Int()
    ra = fields.Float(attribute="location.ra.deg")
    dec = fields.Float(attribute="location.dec.deg")
    grav_wave_events = fields.List(fields.Str())
    properties = fields.Dict()
    lightcurve = TimeSeries()
    alerts = Relationship(
        related_view="alert_list",
        related_view_kwargs={"locus_id": "<id>"},
        schema="AlertSchema",
        type_="alert",
        many=True,
    )
    newest_alert = Relationship(
        related_view="alert_detail",
        related_view_kwargs={"alert_id": "<properties.newest_alert_id>"},
        schema="AlertSchema",
        type_="alert",
        many=False,
        include_resource_linkage=True,
    )
    oldest_alert = Relationship(
        related_view="alert_detail",
        related_view_kwargs={"alert_id": "<properties.oldest_alert_id>"},
        schema="AlertSchema",
        type_="alert",
        many=False,
        include_resource_linkage=True,
    )
    brightest_alert = Relationship(
        related_view="alert_detail",
        related_view_kwargs={"alert_id": "<properties.brightest_alert_id>"},
        schema="AlertSchema",
        type_="alert",
        many=False,
        include_resource_linkage=True,
    )
    tags = fields.List(fields.Str())
    catalogs = fields.List(fields.Str())
    resource_meta = fields.ResourceMeta()
