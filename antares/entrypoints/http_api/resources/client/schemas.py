from marshmallow_jsonapi import fields

from antares.entrypoints.http_api.common.schema import Schema


class ClientConfigStreamingSchema(Schema):
    class Meta:
        type_ = "client_config_streaming"

    id = fields.Str()
    type = fields.Str()
    options = fields.Dict()
