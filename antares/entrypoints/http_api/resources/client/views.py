from dependency_injector.wiring import Provide, inject

from antares.entrypoints.http_api.common.resource import ResourceDetailReadOnly

from .schemas import ClientConfigStreamingSchema


class ClientConfigStreamingDefault(ResourceDetailReadOnly):
    resource_schema = ClientConfigStreamingSchema

    @inject
    def get_resource(
        self, options: dict = Provide["config.api.client.settings"], *args, **kwargs
    ):
        return {"id": 1, "type": "kafka", "options": options}, None
