from http import HTTPStatus

from dependency_injector.wiring import Provide, inject
from flask import abort

from antares.adapters.repository import AbstractCatalogObjectRepository
from antares.entrypoints.http_api.common.resource import (
    ResourceDetailReadOnly,
    ResourceListReadOnly,
)

from .schemas import CatalogSchema


class CatalogList(ResourceListReadOnly):
    resource_schema = CatalogSchema

    @inject
    def get_resource_collection(
        # fmt: off
        self,
        catalog_object_repository: AbstractCatalogObjectRepository = Provide["catalog_object_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        """
        ew
        Get catalog sample data and convert to CatalogSampleSchema format.
        """
        catalogs = catalog_object_repository.list_catalogs()
        catalogs = list(catalogs)
        return catalogs, len(catalogs), {}


class CatalogDetail(ResourceDetailReadOnly):
    resource_schema = CatalogSchema

    @inject
    def get_resource(
        # fmt: off
        self,
        catalog_id: str,
        catalog_object_repository: AbstractCatalogObjectRepository = Provide["catalog_object_repository"],
        *args,
        **kwargs
        # fmt: on
    ):
        """
        Get catalog sample data and convert to CatalogSampleSchema format.
        """
        catalog = catalog_object_repository.get_catalog_by_catalog_id(catalog_id)
        if catalog is None:
            abort(HTTPStatus.NOT_FOUND)
        return catalog, {}
