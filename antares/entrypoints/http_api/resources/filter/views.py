from dependency_injector.wiring import Provide, inject
from flask_jwt_extended import current_user

from antares.adapters.notifications import AbstractInternalNotificationService
from antares.adapters.repository import (
    AbstractFilterRepository,
    AbstractFilterRevisionRepository,
    AbstractUserRepository,
)
from antares.domain.models import Filter, FilterRevision, FilterRevisionStatus
from antares.entrypoints.http_api.common.decorators import (
    admin_required,
    login_optional,
    login_required,
)
from antares.entrypoints.http_api.common.exceptions import (
    BadRequest,
    ResourceConflictException,
    ResourceNotFoundException,
    Unauthorized,
)
from antares.entrypoints.http_api.common.resource import ResourceDetail, ResourceList
from antares.exceptions import KeyViolationException

from .schemas import FilterSchema, FilterVersionSchema


class FilterList(ResourceList):
    resource_schema = FilterSchema

    @login_optional
    @inject
    def get_resource_collection(
        # fmt: off
        self,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        user_repository: AbstractUserRepository = Provide["user_repository"],
        *args,
        **kwargs,
        # fmt: on
    ):
        # If the `user_id` kwarg is set, that means that we've reached this view under
        # a route like /users/<user_id>/filters. We should only return filters that are
        # owned by the user_id specified here.
        if kwargs.get("user_id"):
            filters = filter_repository.list_by_owner_id(kwargs["user_id"])
        else:
            filters = filter_repository.list()
        # If the user is not logged in, they can only view public filters:
        if not current_user:
            filters = (f for f in filters if f.public)
        else:
            # Admins can list all filters in the system but other users can only list
            # their filters and any public ones owned by other users.
            if not current_user.admin:
                filters = (
                    f for f in filters if f.public or f.owner_id == current_user.id
                )

        # TODO:
        # if kwargs["filter"]:
        #     query_filters += tuple(build_query_filters(SFilter, kwargs["filter"]))
        # with start_session() as session:
        #     filters, count = query_collection(
        #         session,
        #         SFilter,
        #         limit=kwargs["limit"],
        #         offset=kwargs["offset"],
        #         sort=kwargs["sort"],
        #         filters=query_filters,
        #     )
        filters = list(filters)

        # If `include` kwarg (which is a set) has the "owner" value, we need to fetch
        # owner users associated with each filter and attach them to the response
        # Note this might not be the best solution as we're performing two queries
        # and manually attaching the owner to each filter. However since we're not using
        # foreign keys in any model and this option will be likely used only by admins,
        # we decided to do this manual work in order to keep consistency and simplicity
        if "owner" in kwargs["include"]:
            owner_ids = [filter_.owner_id for filter_ in filters]
            users = user_repository.list(filtered_ids=owner_ids)
            users_by_id = {user.id: user for user in users}
            for filter_ in filters:
                if owner := users_by_id.get(filter_.owner_id):
                    filter_.owner = owner

        count = len(filters)
        return filters, count, None

    @login_required
    @inject
    def create_resource(
        # fmt: off
        self,
        resource: dict,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        *args,
        **kwargs,
        # fmt: on
    ):
        if current_user.id != kwargs.get("user_id"):
            raise Unauthorized(None)
        if resource.get("owner_id", current_user.id) != current_user.id:
            raise Unauthorized(None)
        resource["owner_id"] = current_user.id
        filter_ = Filter(**resource)
        try:
            filter_repository.add(filter_)
            return filter_, None
        except KeyViolationException as e:
            raise ResourceConflictException(str(e))


class FilterDetail(ResourceDetail):
    resource_schema = FilterSchema
    methods = ["GET", "PATCH"]

    @login_optional
    @inject
    def get_resource(
        # fmt: off
        self,
        filter_id: int,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        *args,
        **kwargs,
        # fmt: on
    ):
        filter_ = filter_repository.get(filter_id)
        if filter_ is None:
            raise ResourceNotFoundException(f"No resource filter with ID {filter_id}")
        if (
            not (current_user and current_user.admin)
            and not (current_user and filter_.owner_id == current_user.id)
            and not filter_.public
        ):
            raise Unauthorized(None)
        return filter_, None

    @login_required
    def patch_resource(
        # fmt: off
        self,
        resource: dict,
        filter_id: int,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        filter_revision_repository: AbstractFilterRevisionRepository = Provide[
            "filter_revision_repository"
        ],
        internal_notification_service: AbstractInternalNotificationService = Provide[
            "internal_notification_service"
        ],
        *args,
        **kwargs,
        # fmt: on
    ):
        del resource["id"]  # Required in payload but not allowed to update
        allowed_fields = {"public", "enabled_filter_revision_id"}
        filter_ = filter_repository.get(filter_id)
        if not filter_:
            raise ResourceNotFoundException(f"No resource filter with ID {filter_id}")
        if set(resource.keys()) - allowed_fields:
            raise BadRequest(
                f"Cannot PATCH fields {set(resource.keys()) - allowed_fields}"
            )
        # Update constraints:
        if "public" in resource:
            # To set filter as public or private, the requester must be an admin or the
            # filter owner.
            if (not current_user.admin) and (filter_.owner_id != current_user.id):
                raise Unauthorized(None)
            if resource["public"]:
                filter_.make_public()
            else:
                filter_.make_private()
        if "enabled_filter_revision_id" in resource:
            # To enable a filter revision, the requester must be an admin.
            if not current_user.admin:
                raise Unauthorized(None)
            enabled_filter_revision_id = int(resource["enabled_filter_revision_id"])
            filter_revision = filter_revision_repository.get(enabled_filter_revision_id)
            if not filter_revision:
                raise ResourceNotFoundException(
                    f"No resource filter_revision with ID {enabled_filter_revision_id}"
                )
            filter_revision.enable(filter_=filter_)
            filter_revision_repository.update(filter_revision)

            # Notify internal staff
            with internal_notification_service as notifier:
                notifier.notify_after_filter_revision_transitioned(
                    filter_,
                    filter_revision,
                )

        filter_repository.update(filter_)
        return filter_, {}


class FilterVersionDetail(ResourceDetail):
    resource_schema = FilterVersionSchema
    methods = ["GET", "PATCH"]

    @login_optional
    @inject
    def get_resource(
        # fmt: off
        self,
        filter_id: int,
        filter_version_id: int,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        filter_revision_repository: AbstractFilterRevisionRepository = Provide[
            "filter_revision_repository"
        ],
        *args,
        **kwargs,
        # fmt: on
    ):
        filter_ = filter_repository.get(filter_id)
        if filter_ is None:
            raise ResourceNotFoundException(f"No resource filter with ID {filter_id}")
        if (
            not (current_user and current_user.admin)
            and not (current_user and filter_.owner_id == current_user.id)
            and not filter_.public
        ):
            raise Unauthorized(None)
        filter_revision = filter_revision_repository.get(filter_version_id)
        if filter_revision is None:
            raise ResourceNotFoundException(
                f"No resource filter_version with ID {filter_version_id}"
            )
        return filter_revision, {}

    @admin_required
    @inject
    def patch_resource(
        # fmt: off
        self,
        resource: dict,
        filter_id: int,
        filter_version_id: int,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        filter_revision_repository: AbstractFilterRevisionRepository = Provide[
            "filter_revision_repository"
        ],
        internal_notification_service: AbstractInternalNotificationService = Provide[
            "internal_notification_service"
        ],
        *args,
        **kwargs,
        # fmt: on
    ):
        # Initial checks
        filter_ = filter_repository.get(filter_id)
        if not filter_:
            raise ResourceNotFoundException(f"No resource filter with ID {filter_id}")

        filter_revision = filter_revision_repository.get(filter_version_id)
        if not filter_revision:
            raise ResourceNotFoundException(
                f"No resource filter_version with ID {filter_version_id}"
            )
        if filter_revision.filter_id != filter_id:
            raise Unauthorized(
                f"Filter revision's filter ID ({filter_revision.filter_id}) "
                f"should be the same as filter_id parameter ({filter_id})"
            )

        allowed_fields = {"feedback", "status"}
        del resource["id"]  # Required in payload but not allowed to update
        # TODO: How to verify that the URL and PATCH payload refer to the same object?
        if set(resource.keys()) - allowed_fields:
            raise BadRequest(
                f"Cannot PATCH fields {set(resource.keys()) - allowed_fields}"
            )

        # Handle status with state machine together with feedback
        status = resource.pop("status", None)
        feedback = resource.pop("feedback", "")
        if status:
            filter_revision.transition_to(
                status,
                feedback=feedback,
                filter_=filter_,
            )

        # Handle the rest of fields
        for key, value in resource.items():
            setattr(filter_revision, key, value)

        filter_revision_repository.update(filter_revision)
        filter_repository.update(filter_)

        # Notify internal staff if status transitioned
        if status:
            with internal_notification_service as notifier:
                notifier.notify_after_filter_revision_transitioned(
                    filter_,
                    filter_revision,
                )

        return filter_revision, {}


class FilterVersionList(ResourceList):
    resource_schema = FilterVersionSchema

    @login_optional
    @inject
    def get_resource_collection(
        # fmt: off
        self,
        filter_id: int,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        filter_revision_repository: AbstractFilterRevisionRepository = Provide[
            "filter_revision_repository"
        ],
        *args,
        **kwargs,
        # fmt: on
    ):
        filter_ = filter_repository.get(filter_id)
        if filter_ is None:
            raise ResourceNotFoundException(f"No resource filter with ID {filter_id}")
        if (
            not (current_user and current_user.admin)
            and not (current_user and filter_.owner_id == current_user.id)
            and not filter_.public
        ):
            raise Unauthorized(None)
        filter_revisions = list(filter_revision_repository.list_by_filter_id(filter_id))
        count = len(filter_revisions)

        # TODO:
        # filter_versions, count = query_collection(
        #     session,
        #     SFilterVersion,
        #     limit=kwargs["limit"],
        #     offset=kwargs["offset"],
        #     sort=kwargs["sort"],
        #     filters=(SFilterVersion.filter_id == filter_id,),
        # )

        return filter_revisions, count, None

    @login_required
    @inject
    def create_resource(
        # fmt: off
        self,
        resource: dict,
        filter_id: int,
        filter_repository: AbstractFilterRepository = Provide["filter_repository"],
        filter_revision_repository: AbstractFilterRevisionRepository = Provide[
            "filter_revision_repository"
        ],
        internal_notification_service: AbstractInternalNotificationService = Provide[
            "internal_notification_service"
        ],
        *args,
        **kwargs,
        # fmt: on
    ):
        filter_revision = FilterRevision(**resource)
        if filter_id != filter_revision.filter_id:
            raise Unauthorized(None)
        filter_ = filter_repository.get(filter_id)
        if filter_ is None:
            raise ResourceNotFoundException(f"No resource filter with ID {filter_id}")
        if not current_user.admin and filter_.owner_id != current_user.id:
            raise Unauthorized(None)
        # TODO: Testing coverage for this branch
        if (
            filter_revision.status != FilterRevisionStatus.PENDING_REVIEW
            and not current_user.admin
        ):
            raise Unauthorized(None)
        filter_revision_repository.add(filter_revision)

        # Notify internal staff about new filter revision to be reviewed
        with internal_notification_service as notifier:
            notifier.notify_after_filter_revision_created(filter_, filter_revision)

        return filter_revision, None
