from marshmallow_jsonapi import fields

from antares.domain.models import FilterRevision, FilterRevisionStatus
from antares.entrypoints.http_api.common.schema import Relationship, Schema


class FilterSchema(Schema):
    class Meta:
        type_ = "filter"
        self_view = "filter_detail"
        self_view_kwargs = {"filter_id": "<id>"}
        self_view_many = "filter_list"

    id = fields.Integer(as_string=True)
    name = fields.Str()
    created_at = fields.DateTime()
    disabled_at = fields.DateTime()
    updated_at = fields.DateTime()
    priority = fields.Int()
    user_id = fields.UUID(attribute="owner_id")
    owner = Relationship(
        related_view="user_detail",
        related_view_kwargs={"user_id": "<owner_id>"},
        schema="UserSchema",
        type_="user",
        attribute="owner",
        include_resource_linkage=True,
    )
    description = fields.Str()
    enabled_version = Relationship(
        related_view="filter_version_detail",
        related_view_kwargs={
            "filter_id": "<id>",
            "filter_version_id": "<enabled_filter_revision_id>",
        },
        schema="FilterVersionSchema",
        type_="filter_version",
        attribute="enabled_filter_revision_id",
        include_resource_linkage=True,
    )
    public = fields.Bool(dump_default=False)
    enabled = fields.Bool(dump_default=False)


class FilterVersionSchema(Schema):
    class Meta:
        type_ = "filter_version"
        self_view = "filter_version_detail"
        self_view_kwargs = {
            "filter_version_id": "<id>",
            "filter_id": "<filter_id>",
        }
        self_view_many = "filter_version_list"

    id = fields.Str()
    filter = Relationship(
        related_view="filter_detail",
        related_view_kwargs={"filter_id": "<filter_id>"},
        schema=FilterSchema,
        type_="filter",
        required=True,
        include_resource_linkage=True,
        attribute="filter_id",
    )
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
    transitioned_at = fields.DateTime()
    code = fields.Str()
    comment = fields.Str()
    feedback = fields.Str()
    validated_at = fields.DateTime()  # Deprecated, will be removed from schema
    enabled_at = fields.DateTime()  # Deprecated, will be removed from schema
    disabled_at = fields.DateTime()  # Deprecated, will be removed from schema
    # disabled_log_id = fields.Int() # Deprecated, will be removed from schema
    public = fields.Boolean()  # Deprecated, will be removed from schema
    status = fields.Method("serialize_status", deserialize="deserialize_status")

    def serialize_status(self, filter_revision: FilterRevision) -> str:
        if not filter_revision.status:
            raise ValueError(f"Filter version (ID {filter_revision.id}) with no status")
        return filter_revision.status.name

    def deserialize_status(self, status: str) -> FilterRevisionStatus:
        try:
            return FilterRevisionStatus[status]
        except KeyError:
            raise ValueError(f"Unknown filter status {status}")
