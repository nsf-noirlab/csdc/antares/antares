"""
This is the entrypoint to the ANTARES HTTP API. This API conforms to
version 1.0 of the JSON:API specification.
"""
import datetime
import uuid

import ddtrace
from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_mail import Mail
from flask_restful import Api
from opentelemetry.instrumentation.flask import FlaskInstrumentor
from werkzeug.routing import BaseConverter, ValidationError

from antares.bootstrap import bootstrap
from antares.entrypoints.http_api import handlers
from antares.settings import Container

mail = Mail()
jwt = JWTManager()


class UUIDFromConverter(BaseConverter):
    def to_python(self, value: str) -> uuid.UUID:
        try:
            return uuid.UUID(value)
        except:
            try:
                return uuid.UUID(int=int(value))
            except:
                raise ValidationError

    def to_url(self, value: uuid.UUID) -> str:
        return str(value)


class ISODatetimeFromConverter(BaseConverter):
    def to_python(self, value: str) -> datetime.datetime:
        try:
            return datetime.datetime.fromisoformat(value)
        except:
            raise ValidationError

    def to_url(self, value: datetime.datetime) -> str:
        return value.isoformat()


def create_app(container: Container):
    if container.config.observability.type() == "datadog":
        ddtrace.tracer.configure(
            enabled=container.config.observability.tracing.enabled(),
            hostname=container.config.observability.tracing.parameters.host(),
            port=container.config.observability.tracing.parameters.port(),
        )
        ddtrace.patch(flask=True)
    api = Api(prefix="/v1")
    app = Flask("api")
    app.config["DEBUG"] = container.config.debug()
    # flask_restful hijacks the Flask error handler registry which causes some issues
    # with how we register error handlers. To keep error handling consistent with the
    # development environment we can set the PROPAGATE_EXCEPTIONS flag to True.
    # See:
    # https://github.com/vimalloc/flask-jwt-extended/issues/86#issuecomment-444983119
    # for more information.
    auth_params = container.config.api.authentication.parameters
    app.config["PROPAGATE_EXCEPTIONS"] = container.config.api.propagate_exceptions()
    app.config["MAIL_SERVER"] = container.config.external.smtp.host()
    app.config["MAIL_PORT"] = container.config.external.smtp.port()
    app.config["MAIL_USE_TLS"] = container.config.external.smtp.tls()
    app.config["MAIL_USERNAME"] = container.config.external.smtp.username()
    app.config["MAIL_PASSWORD"] = container.config.external.smtp.password()
    app.config["JWT_SECRET_KEY"] = auth_params.secret_key()
    app.config["JWT_TOKEN_LOCATION"] = auth_params.token_location()
    app.config["JWT_ACCESS_COOKIE_PATH"] = auth_params.access_cookie_path()
    app.config["JWT_REFRESH_COOKIE_PATH"] = auth_params.refresh_cookie_path()
    app.config["JWT_COOKIE_DOMAIN"] = auth_params.cookie_domain()
    app.config["JWT_COOKIE_SECURE"] = auth_params.cookie_secure()
    app.config["JWT_COOKIE_CSRF_PROTECT"] = auth_params.cookie_csrf_protection()
    app.config["JWT_COOKIE_SAMESITE"] = auth_params.cookie_samesite()
    app.config["JWT_SESSION_COOKIE"] = auth_params.session_cookie()
    app.url_map.converters["uuid_from"] = UUIDFromConverter
    app.url_map.converters["datetime_from"] = ISODatetimeFromConverter

    CORS(app, supports_credentials=True)
    jwt.init_app(app)
    handlers.register_jwt_handlers(jwt)
    mail.init_app(app)

    from antares.entrypoints.http_api.resources.routes import initialize_routes

    initialize_routes(api)
    api.init_app(app)
    handlers.register_error_handlers(app)
    if container.config.observability.type() == "gcp":
        # Call container and metrics to setup metrics and traces
        container.tracer()
        container.metrics()
        FlaskInstrumentor().instrument_app(app)
    return app


if __name__ == "__main__":
    container = bootstrap()
    app = create_app(container)
    app.run(debug=container.config.debug(), host="0.0.0.0", port=8000)
