import functools

from flask_jwt_extended import current_user, verify_jwt_in_request

from .exceptions import Unauthorized


def login_optional(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request(optional=True)
        return f(*args, **kwargs)

    return decorated_function


def login_required(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request()
        return f(*args, **kwargs)

    return decorated_function


def staff_required(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request()
        if not current_user.staff:
            raise Unauthorized(None)
        return f(*args, **kwargs)

    return decorated_function


def admin_required(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        verify_jwt_in_request()
        if not current_user.admin:
            raise Unauthorized(None)
        return f(*args, **kwargs)

    return decorated_function
