import functools
import json
import re
import urllib
from http import HTTPStatus
from typing import Union

import flask_restful
import flask_restful.reqparse
from flask import jsonify, request
from marshmallow.exceptions import MarshmallowError
from marshmallow_jsonapi.exceptions import JSONAPIError

from antares.entrypoints.http_api.common.exceptions import BadRequest
from antares.entrypoints.http_api.common.schema import Schema

SORT_REGEX = re.compile(r"^\-?(?P<field>[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*)$")


def validate_query_parameters(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        schema = func.__self__.resource_schema
        type_ = schema.Meta.type_
        parser = flask_restful.reqparse.RequestParser()

        def sparse_fieldset(value):
            for field in value.split(","):
                if field not in schema._declared_fields:
                    raise ValueError(f"No field {field} on resource of type {type_}")
            return set(("id",)) | set(value.split(","))

        def sparse_include(value):
            return sparse_fieldset(value) - set(("id",))

        def sort_argument(value):
            match = re.match(SORT_REGEX, value)
            if match:
                # This is a hack to deal with not knowing what the full
                # schema is of something like a Locus document. If a user
                # passes the query parameter ?sort=properties.num_alerts
                # we are unable to verify that num_alerts is a member of
                # properties because the properties field on the Locus schema
                # is just defined as fields.Dict(). We'll just check that the
                # name before the first (if any) dot is on the resource schema.
                #
                # TODO: A better solution is to move this logic (and the filter
                # and fieldset logic) out of this wrapper function and into
                # the base resource class. Sorting and filtering are not allowed
                # on each resource and the API should return a 400 Bad Request
                # if they are passed.
                field = match.group("field").split(".")[0]
                if field not in schema._declared_fields:
                    raise ValueError(
                        f"No field {match.group('field')} on resource of type {type_}"
                    )
                if value.startswith("-"):
                    return {"field": match.group("field"), "order": "desc"}
                return {"field": match.group("field"), "order": "asc"}
            raise ValueError(f"Invalid search term: {value}")

        parser.add_argument(
            "page[limit]", type=int, dest="limit", default=10, location="args"
        )
        parser.add_argument(
            "page[offset]", type=int, dest="offset", default=0, location="args"
        )
        parser.add_argument(
            f"fields[{type_}]", type=sparse_fieldset, dest="fields", location="args"
        )
        parser.add_argument(
            "filter", type=json.loads, dest="filter", location="args", default=list
        )
        parser.add_argument(
            "data",
            type=dict,
            dest="data",
            location="json",
            default=dict,
        )
        parser.add_argument(
            f"elasticsearch_query[{type_}]",
            type=json.loads,
            dest="elasticsearch_query",
            location="args",
        )
        parser.add_argument("sort", type=sort_argument, dest="sort", location="args")
        parser.add_argument(
            "include", type=sparse_include, dest="include", location="args", default=set
        )
        query_params = parser.parse_args()
        return func(*args, **query_params, **kwargs)

    return wrapper


class Resource(flask_restful.Resource):
    method_decorators = [validate_query_parameters]
    resource_schema: Union[bool, type[Schema]] = NotImplemented

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.resource_schema is NotImplemented:
            raise NotImplementedError(f"Must declare resource_schema on {self}")

    def add_links(self, response, count=0, **kwargs):
        response["links"] = {"self": request.url}
        limit = kwargs["limit"]
        offset = kwargs["offset"]
        if 0 < offset < count:
            params = urllib.parse.parse_qs(urllib.parse.urlparse(request.url).query)
            params["page[offset]"] = max(0, offset - limit)
            params["page[limit]"] = min(limit, offset)
            query_string = urllib.parse.urlencode(params, doseq=True)
            response["links"]["prev"] = request.base_url + "?" + query_string
        if offset + limit < count:
            params = urllib.parse.parse_qs(urllib.parse.urlparse(request.url).query)
            params["page[offset]"] = offset + limit
            params["page[limit]"] = min(limit, count - (offset + limit))
            query_string = urllib.parse.urlencode(params, doseq=True)
            response["links"]["next"] = request.base_url + "?" + query_string
        return response


class ResourceDetailReadOnly(Resource):
    methods = ["GET"]

    def get(self, *args, **kwargs):
        resource, meta = self.get_resource(*args, **kwargs)
        response = self.resource_schema(only=kwargs["fields"]).dump(resource)
        self.add_links(response, **kwargs)
        if meta:
            response["meta"] = meta
        return response

    def get_resource(self, *args, **kwargs):
        raise NotImplementedError


class ResourceDetail(ResourceDetailReadOnly):
    methods = ["GET", "PATCH", "DELETE"]

    def patch(self, *args, **kwargs):
        if not request.data:
            raise BadRequest("Must specify payload with PATCH method")
        try:
            resource = self.resource_schema().load(
                json.loads(request.data), partial=True
            )
        except (MarshmallowError, JSONAPIError) as e:
            raise BadRequest(str(e))
        resource, meta = self.patch_resource(resource, *args, **kwargs)
        response = self.resource_schema(only=kwargs["fields"]).dump(resource)
        self.add_links(response, **kwargs)
        if meta:
            response["meta"] = meta
        # should return 201, HTTPStatus.CREATED
        return response

    def delete(self, *args, **kwargs):
        meta = self.delete_resource(*args, **kwargs)
        if meta:
            return {"meta": meta}
        response = jsonify({})
        response.status_code = HTTPStatus.NO_CONTENT
        return response

    def patch_resource(self, *args, **kwargs):
        raise NotImplementedError

    def delete_resource(self, *args, **kwargs):
        raise NotImplementedError


class ResourceListReadOnly(Resource):
    methods = ["GET"]

    def get(self, *args, **kwargs):
        resource, count, meta = self.get_resource_collection(*args, **kwargs)
        response = self.resource_schema(
            only=kwargs["fields"], many=True, include_data=kwargs["include"]
        ).dump(resource)
        self.add_links(response, count, **kwargs)
        response["meta"] = {"count": count}
        if meta:
            response["meta"].update(meta)
        return response

    def get_resource_collection(self, *args, **kwargs):
        raise NotImplementedError


class ResourceList(ResourceListReadOnly):
    methods = ["GET", "POST"]

    def post(self, *args, **kwargs):
        try:
            resource = self.resource_schema().load(json.loads(request.data))
        except (MarshmallowError, JSONAPIError) as e:
            raise BadRequest(str(e))
        resource, meta = self.create_resource(resource, *args, **kwargs)
        response = self.resource_schema(only=kwargs["fields"]).dump(resource)
        self.add_links(response, **kwargs)
        if meta:
            response["meta"] = meta
        # should return 201, HTTPStatus.CREATED
        return response

    def create_resource(self, resource, *args, **kwargs):
        pass
