import re

import htm
from astropy import units as u
from astropy.coordinates import SkyCoord
from marshmallow import Schema, ValidationError, fields


class DistanceString(fields.Field):
    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return ""
        return f"{value} degree"

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            distance, units = value.split()
            if units != "degree":
                raise Exception
            return float(distance)
        except Exception as e:
            raise ValidationError(f"Invalid distance string {value}") from e


class CoordinateString(fields.Field):
    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return ""
        return f"{value.ra.degree:.12f} {value.dec.degree:.12f}"

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            try:
                clean_value = re.sub("[ ]{2,}", " ", value)  # remove repeated spaces
                clean_value = re.sub(
                    "[^0-9^.dms:+ -]", "", clean_value
                )  # remove all but 0-9.dms:+ space and -
                if ":" in clean_value or clean_value.count(" ") > 2:
                    return SkyCoord(
                        clean_value, unit=(u.hourangle, u.deg)
                    )  # pylint: disable=no-member
                else:
                    return SkyCoord(clean_value)
            except Exception:
                return SkyCoord(value, unit=u.deg)
        except ValueError:
            raise ValidationError(f"Invalid coordinate string {value}")


def transform_sky_distance_query(query, field, htm_level=20):
    QuerySchema = Schema.from_dict(
        {
            "distance": DistanceString(),
            field: fields.Nested(Schema.from_dict({"center": CoordinateString()})),
        }
    )

    def transform(value):
        sky_distance = QuerySchema().load(value)
        distance = sky_distance["distance"]
        coordinates = sky_distance[field]["center"]
        htm_ranges = htm.get_htm_circle_region(
            coordinates.ra.degree,
            coordinates.dec.degree,
            distance,
            htm_level,
        )
        return {
            "should": [
                {"range": {field: {"gte": min_, "lte": max_}}}
                for min_, max_ in htm_ranges
            ]
        }

    transform_dictionary(query, "sky_distance", transform, new_key="bool")
    return query


def transform_dictionary(d, key, transform, new_key=None):
    # From https://stackoverflow.com/questions/9807634/find-all-occurrences-of-a-key-in-nested-dictionaries-and-lists
    if hasattr(d, "items"):
        for k, v in list(d.items()):
            if k == key:
                d[k] = transform(v)
                if new_key:
                    d[new_key] = d.pop(k)
            if isinstance(v, dict):
                transform_dictionary(v, key, transform, new_key=new_key)
            elif isinstance(v, list):
                for d in v:
                    transform_dictionary(d, key, transform, new_key=new_key)
