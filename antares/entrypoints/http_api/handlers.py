import uuid
from http import HTTPStatus

from dependency_injector.wiring import Provide, inject
from flask import jsonify

from antares.adapters.repository import (
    AbstractJwtBlocklistRepository,
    AbstractUserRepository,
)
from antares.entrypoints.http_api.common.exceptions import (
    BaseException,
    SeeOther,
    Unauthorized,
)


@inject
def register_jwt_handlers(
    # fmt: off
    jwt,
    user_repository: AbstractUserRepository = Provide["user_repository"],
    jwt_blocklist_repository: AbstractJwtBlocklistRepository = Provide["jwt_blocklist_repository"],
    # fmt: on
):
    @jwt.token_in_blocklist_loader
    def check_if_token_in_blocklist(jwt_header, jwt_payload):
        """
        Check if a user-supplied token is in our SQL token blocklist.

        Notes
        -----------
        We only blocklist refresh tokens and assume that shorter-lived,
        access tokens will expire in a short enough window that we don't
        need to force-expire them.
        """
        if jwt_payload["type"] == "access":
            return
        elif jwt_payload["type"] == "refresh":
            return jwt_blocklist_repository.get_by_jti(jwt_payload["jti"])
        else:
            raise ValueError(f"Unrecogized token type {jwt_payload['type']}")

    @jwt.additional_claims_loader
    def add_claims_to_access_token(user):
        return {
            "user_id": user.id,
            "username": user.username,
        }

    @jwt.user_identity_loader
    def user_identity_lookup(user):
        return user.id

    @jwt.user_lookup_loader
    def user_loader_callback(jwt_header, jwt_payload):
        try:
            # In v2 of the system, users have UUID IDs
            user_id = uuid.UUID(jwt_payload["sub"])
        except (AttributeError, ValueError):
            try:
                # Handle the case where a user has a cookie stored from v1 of the system
                # in their browser still
                user_id = uuid.UUID(int=int(jwt_payload["sub"]))
            except:
                raise Unauthorized(None)
        return user_repository.get(user_id)

    @jwt.user_lookup_error_loader
    def user_error_callback(jwt_header, jwt_payload):
        raise Unauthorized(None)

    @jwt.token_verification_failed_loader
    def claims_verification_failed(jwt_header, jwt_payload):
        raise Unauthorized(None)

    @jwt.revoked_token_loader
    def revoked_token(jwt_header, jwt_payload):
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": "Token has been revoked",
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response

    @jwt.needs_fresh_token_loader
    def needs_fresh_token(jwt_header, jwt_payload):
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": "Fresh token required",
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response

    @jwt.invalid_token_loader
    def invalid_token(error):
        payload = [
            {
                "status": HTTPStatus.UNPROCESSABLE_ENTITY,
                "title": "Unauthorized",
                "detail": error,
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNPROCESSABLE_ENTITY
        return response

    @jwt.expired_token_loader
    def expired_token(jwt_headers, jwt_payload):
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": "Token has expired",
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response

    @jwt.unauthorized_loader
    def unauthorized(error):
        payload = [
            {
                "status": HTTPStatus.UNAUTHORIZED,
                "title": "Unauthorized",
                "detail": error,
            }
        ]
        response = jsonify({"errors": payload})
        response.status_code = HTTPStatus.UNAUTHORIZED
        return response


def register_error_handlers(app):
    @app.errorhandler(HTTPStatus.NOT_FOUND)
    def handle_404(error):
        payload = {
            "status": HTTPStatus.NOT_FOUND,
            "title": "404 Not Found",
            "detail": "The requested URL was not found.",
        }
        response = jsonify({"errors": [payload]})
        response.status_code = HTTPStatus.NOT_FOUND
        return response

    @app.errorhandler(SeeOther)
    def handle_303(error):
        response = jsonify({})
        response.status_code = HTTPStatus.SEE_OTHER
        response.headers["Location"] = error.location
        return response

    @app.errorhandler(Exception)
    def handle_exception(error):
        if isinstance(error, BaseException):
            response = jsonify({"errors": [error.to_dict()]})
            response.status_code = error.status_code
            return response
        payload = {
            "status": HTTPStatus.INTERNAL_SERVER_ERROR,
            "title": "Internal Server Error",
            "detail": "A server-side problem occured.",
        }
        if app.config["DEBUG"]:
            payload["detail"] = str(error)
        response = jsonify({"errors": [payload]})
        response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
        return response
