from antares.bootstrap import bootstrap
from antares.entrypoints.grav_wave_watcher import main


def init():
    bootstrap()
    if __name__ == "__main__":
        main()


init()
