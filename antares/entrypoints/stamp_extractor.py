import gzip
import io
import logging
import os
import time
from typing import Any, Optional

import fastavro
import numpy as np
import numpy.typing as npt
import pyvips
from google.auth.credentials import AnonymousCredentials
from google.cloud import storage
from google.cloud.storage.bucket import Bucket
from scipy.stats import mode, sigmaclip

import antares.exceptions
from antares.adapters.messages import KafkaMessageSubscriptionService
from antares.bootstrap import bootstrap
from antares.observability.metrics.base import AbstractMetrics

# logging.debug shows everything from pyvips


def bytscl(image_array: npt.NDArray[np.float32], min_value: float, max_value: float):
    # Top is 255 because if the maximum value of the output range for 8-bit images
    top = 255
    # Python version of IDL's BYTSCL (https://www.l3harrisgeospatial.com/docs/bytscl.html)
    # used as part of the algoritm to convert FITS images to PNGs
    # scales all values of Array that lie in the range (Min≤x≤Max) into the range (0 ≤x≤Top)
    return ((image_array - min_value) / (max_value - min_value)) * (top + 0.9999)


def create_gcs_blob_name(file_name: str):
    without_gz_extension = slice(0, -3)
    file_name = file_name[without_gz_extension]
    # Different delta images must be standardize with diff
    file_name = file_name.replace("refmsci", "diff")
    file_name = file_name.replace("scimref", "diff")
    return f"{file_name}.png"


def calculate_image_stats(image_array: npt.NDArray[np.float32]):
    temporary_vector = image_array.flatten()
    temporary_vector = temporary_vector[~np.isnan(temporary_vector)]
    sigma_clipped_vector, *_ = sigmaclip(temporary_vector)
    if len(sigma_clipped_vector) > 1:
        temporary_vector = sigma_clipped_vector
    image_sigma = temporary_vector.std()
    image_mode, _ = mode(temporary_vector, keepdims=False)
    image_max_value = temporary_vector.max()
    return image_mode.max(), image_sigma, image_max_value


def retrieve_stamp_from_ztf_message(message: Any, gcs_bucket: Bucket, overwrite: bool):
    """
    When using pyvips.Image.new_from_buffer the image is not the same,
    that's why we write the file to the filesystem. There are some
    errors when using tempfile.NamedTemporaryFile so that's not an option.
    Temporary files should be deleted at the end of the process because pyvips
    open those files when some arguments are needed.
    """
    for packet in fastavro.reader(io.BytesIO(message)):
        logging.debug("Extracting alerts from %s", packet["objectId"])
        for key in ("cutoutScience", "cutoutTemplate", "cutoutDifference"):
            gcs_blob_name = create_gcs_blob_name(packet[key]["fileName"])
            stamp_data_decompressed = gzip.decompress(packet[key]["stampData"])
            temporary_file_name = f"{gcs_blob_name}.fits"
            with open(temporary_file_name, "wb") as tmp:
                tmp.write(stamp_data_decompressed)
            image = pyvips.Image.new_from_file(
                temporary_file_name, access="sequential-unbuffered"
            )
            image_height = image.height
            image_width = image.width
            image_bands = image.bands
            image_array = image.numpy().reshape(image_height, image_width, image_bands)
            (
                image_mode,
                image_sigma,
                image_max_value,
            ) = calculate_image_stats(image_array)
            if image_sigma <= 0:
                image_sigma = 1.0
            image_min_value = max(image_mode - 2.0 * image_sigma, image_array.min())
            if np.isnan(image_min_value):
                image_min_value = image_array.min()
            scaled_image_array = bytscl(
                image_array=np.arcsinh((image_array - image_min_value) / image_sigma),
                min_value=0,
                max_value=np.arcsinh((image_max_value - image_min_value) / image_sigma),
            )
            scaled_image = pyvips.Image.new_from_array(scaled_image_array)
            image_bytes = scaled_image.write_to_buffer(
                format_string=".png", compression=0, interlace=False, dither=0
            )
            blob = gcs_bucket.blob(gcs_blob_name)
            if not overwrite and blob.exists():
                logging.debug("Image `%s` already exists", gcs_blob_name)
                return
            blob.upload_from_string(image_bytes, content_type="image/png")
            logging.debug("Image uploaded to %s", gcs_blob_name)
            os.remove(temporary_file_name)


def process_stamp_kafka_messages(
    kafka_subscription: KafkaMessageSubscriptionService,
    gcs_bucket: Bucket,
    overwrite: bool,
    metrics: AbstractMetrics,
    limit: Optional[int] = None,
    timeout: Optional[float] = 300.0,
    retry_interval_seconds: float = 0.5,
):
    count = 0
    while True:
        if not limit or count < limit:
            try:
                _, message, _ = kafka_subscription.poll(timeout)
            except antares.exceptions.Timeout:
                logging.info("Timeout waiting for messages from Kafka")
                return count
            while True:
                try:
                    retrieve_stamp_from_ztf_message(message, gcs_bucket, overwrite)
                    count += 1
                    metrics.increment_stamp_extractor_alerts_received()
                    kafka_subscription.commit()
                    break
                except Exception:
                    logging.exception(
                        "Error processing message (retry in %fs)",
                        retry_interval_seconds,
                    )
                    time.sleep(retry_interval_seconds)
                    continue
        else:
            return count


if __name__ == "__main__":
    container = bootstrap()
    if os.getenv("GCS_EMULATOR") and str(os.getenv("GCS_EMULATOR")).upper() == "TRUE":
        gcs_client = storage.Client(
            credentials=AnonymousCredentials(),
            project="test",
            client_options={"api_endpoint": "http://gcs:4443"},
        )  # This can be abstracted but will collide with GcsAlertThumbnailRepository
        gcs_bucket = gcs_client.bucket(container.config.stamp_extractor.bucket_name())
        if not gcs_bucket.exists():
            gcs_bucket.create()
    else:
        gcs_client = storage.Client()
        gcs_bucket = gcs_client.bucket(container.config.stamp_extractor.bucket_name())
    kafka_subscription = KafkaMessageSubscriptionService(
        container.config.stamp_extractor.broker.settings(),
        container.config.stamp_extractor.broker.topics(),
    )
    overwrite = False
    if (
        container.config.stamp_extractor.overwrite()
        and container.config.stamp_extractor.overwrite() is True
    ):
        overwrite = True
    # If this is not within a while True our Kubernetes pods crashes when this
    # exists without error. `Completed - exit code: 0`
    while True:
        process_stamp_kafka_messages(
            kafka_subscription=kafka_subscription,
            gcs_bucket=gcs_bucket,
            overwrite=overwrite,
            metrics=container.metrics(),
        )
