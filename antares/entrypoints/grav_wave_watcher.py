import json
import logging

from dependency_injector.wiring import Provide, inject

from antares.adapters.messages import KafkaMessageSubscriptionService
from antares.adapters.repository import AbstractGravWaveRepository
from antares.domain.models import GravWaveNotice
from antares.exceptions import Timeout


@inject
def main(
    max_loops=None,
    kafka_config: dict = Provide["config.grav_wave_watcher.broker.settings"],
    kafka_topic: str = Provide["config.grav_wave_watcher.broker.topic"],
    kafka_timeout: float = Provide["config.grav_wave_watcher.broker.timeout"],
    grav_wave_repo: AbstractGravWaveRepository = Provide["grav_wave_repository"],
    skip_test: bool = Provide["config.grav_wave_watcher.skip_test"],
):
    kafka_subscription = KafkaMessageSubscriptionService(
        kafka_config,
        [kafka_topic],
    )
    loop = 0
    while True:
        loop += 1
        if max_loops and loop > max_loops:
            break
        try:
            _, notice_json, _ = kafka_subscription.poll(kafka_timeout)
        except Timeout:
            continue

        notice = GravWaveNotice.from_gcn(json.loads(notice_json))

        if skip_test and notice.gracedb_id[0] != "S":
            logging.info("Skipping mock/test notice %s", notice.gracedb_id)
            continue

        existing_notice_id = grav_wave_repo.get_id(
            notice.gracedb_id, notice.notice_datetime
        )
        if existing_notice_id is None:
            logging.info(
                "Ingesting new grav wave notice, "
                f"GRACEDB_ID: {notice.gracedb_id}, "
                f"NOTICE DATETIME: {notice.notice_datetime}"
            )
            grav_wave_repo.add(notice)
        else:
            logging.info(
                ("Duplicate grav wave notice seen: ID: %s, " "GRACEDB_ID: %s"),
                existing_notice_id,
                notice.gracedb_id,
            )
