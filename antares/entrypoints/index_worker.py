import logging

import elasticsearch.exceptions

from antares.bootstrap import bootstrap
from antares.external.elasticsearch import ingest
from antares.observability.metrics.base import AbstractMetrics
from antares.observability.tracing.base import AbstractTracer

ES_RETRY_ERRORS = (
    ConnectionError,
    elasticsearch.exceptions.ConnectionError,
    elasticsearch.exceptions.TransportError,
)


def sync_elasticsearch(
    topic: str,
    index: str,
    chunk_size: int,
    kafka_settings: dict,
    poll_timeout: float,
    tracer: AbstractTracer,
    metrics: AbstractMetrics,
):
    logging.info("Starting Elasticsearch indexing job")
    ingest.index_alerts_from_kafka(
        [topic],
        index,
        max_chunk_size=chunk_size,
        kafka_config=kafka_settings,
        kafka_poll_timeout=poll_timeout,
        tracer=tracer,
        metrics=metrics,
    )


def main():
    container = bootstrap()
    ingest.wait_for_online()
    while True:
        try:
            sync_elasticsearch(
                container.config.archive.ingestion.broker.topic(),
                container.config.archive.ingestion.storage.parameters.index(),
                container.config.archive.ingestion.batch_size(),
                container.config.archive.ingestion.broker.settings(),
                container.config.archive.ingestion.broker.timeout(),
                container.tracer(),
                container.metrics(),
            )
        except ES_RETRY_ERRORS as e:
            logging.warning("Caught expected Elasticsearch connection error: %s", e)
            continue


if __name__ == "__main__":
    main()
