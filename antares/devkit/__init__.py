from antares.devkit.filter import Filter, run_filter, run_many
from antares.devkit.get_data import (
    get_crash_log,
    get_locus,
    get_locus_ids,
    get_sample_catalog_data,
    get_thumbnails,
    locus_from_crash_log,
    locus_from_dict,
    locus_from_file,
    print_crash_log,
    search_catalogs,
    upload_file,
)
from antares.devkit.log import log, log_init
from antares.domain.models import FilterContext as Locus
from antares.observability.logging.default import (
    init_default_logging as antares_log_init,
)

__all__ = [
    "init",
    "Filter",
    "Locus",
    "get_locus",
    "get_locus_ids",
    "get_sample_catalog_data",
    "search_catalogs",
    "locus_from_dict",
    "locus_from_file",
    "log",
    "upload_file",
    "get_crash_log",
    "print_crash_log",
    "locus_from_crash_log",
    "get_thumbnails",
    "run_filter",
    "run_many",
    "get_ligo_events",
]


def dont_print(*args, **kwargs):
    pass


def init(log_level="WARN", quiet=False, configure=True):
    from antares.bootstrap import bootstrap

    p = dont_print if quiet else print

    if configure:
        log_init(log_level)
        antares_log_init(log_level)

    bootstrap()

    # Test ability to load Loci
    p("Testing loading a random Locus with `dk.get_locus()`...")
    ids = get_locus_ids(1)
    if not ids:
        p = print  # Disable quiet mode
        p("The database appears to be empty.")
        p("Please restart your Jupyter kernel and try again.")
        p("Or, contact ANTARES team for support on Slack or at antares@noirlab.edu")
        return
    get_locus()

    # Print success message
    import antares

    p("")
    p(f"ANTARES v{antares.__version__} DevKit is ready!")
    p("Website: https://antares.noirlab.edu")
    p("Documentation: https://nsf-noirlab.gitlab.io/csdc/antares/antares/")
    p("")


def get_ligo_events():
    raise NotImplementedError
