import itertools
from typing import Optional

import antares_client
from dependency_injector.wiring import Provide, inject

from antares.adapters.repository import AbstractBlobRepository
from antares.deprecated.sql.schema import SFilterCrashLog
from antares.domain.models import Blob
from antares.domain.models import FilterContext as Locus
from antares.domain.models import FilterContextAlert
from antares.utils.collections_extensions import (
    RestrictedWriteDictWrapper,
    RestrictedWriteSetWrapper,
)


def build_filter_context_from_client(
    locus: antares_client.models.Locus, include_gravitational_wave_data: bool = False
) -> Optional[Locus]:
    if not locus:
        return None
    grav_wave_notices = {}
    if include_gravitational_wave_data and locus.grav_wave_events:
        for notice in antares_client.search.get_multiple_grav_wave_notices(
            locus.grav_wave_events
        ):
            grav_wave_notices[notice.gracedb_id] = notice
    return Locus(
        locus_id=locus.locus_id,
        alerts=[
            FilterContextAlert(
                alert_id=alert.alert_id,
                locus_id=locus.locus_id,
                mjd=alert.mjd,
                processed_at=alert.processed_at,
                properties={**alert.properties},
                grav_wave_events=alert.grav_wave_events,
            )
            for alert in locus.alerts
        ],
        ra=locus.ra,
        dec=locus.dec,
        properties=RestrictedWriteDictWrapper(locus.properties),
        tags=RestrictedWriteSetWrapper(set(locus.tags)),
        catalog_objects=locus.catalog_objects,
        grav_wave_events_metadata=grav_wave_notices,
    )


def get_locus(
    locus_id: Optional[str] = None,
    alert_id: Optional[str] = None,
    ztf_object_id: Optional[str] = None,
    include_gravitational_wave_data: bool = False,
) -> Optional[Locus]:
    """
    Load a Locus from the DB by locus_id, alert_id, or ztf_object_id.

    If none of the above are specified, a random Locus from the
    DB will be selected.

    :param ztf_object_id:
    :param locus_id:
    :param alert_id:
    :param include_gravitational_wave_data:
    :return: Locus
    """
    if alert_id:
        # This can be done by adding an endpoint and using _get_locus_id_by_alert_id
        raise NotImplementedError
    if not any((locus_id, ztf_object_id)):
        return build_filter_context_from_client(
            antares_client.search.get_by_id(get_locus_id()),
            include_gravitational_wave_data,
        )
    if bool(locus_id) and bool(ztf_object_id):
        raise ValueError("Search must use locus_id or ztf_object_id, not both.")
    if ztf_object_id:
        return build_filter_context_from_client(
            antares_client.search.get_by_ztf_object_id(ztf_object_id),
            include_gravitational_wave_data,
        )
    return build_filter_context_from_client(
        antares_client.search.get_by_id(locus_id), include_gravitational_wave_data
    )


def get_locus_id():
    """
    Get a random locus_id from the DB.
    """
    return get_locus_ids(1)[0]


def get_locus_ids(n):
    """
    Get `n` random locus_ids from the DB.

    :param n: number of locus_ids to get, up to max of 1000
    :return: list of locus_ids
    """
    if n > 1000:
        raise ValueError("n must be less than or equal to 1000")

    # Build an ES query
    query = {
        "query": {
            "function_score": {
                "random_score": {},
            }
        }
    }
    return [
        locus.locus_id
        for locus in itertools.islice(antares_client.search.search(query), n)
    ]


def get_sample_catalog_data(n=10):
    """
    Get a sample of catalog data from database for all catalogs.

    :param n: number of rows per catalog
    :return: dict
    """
    return antares_client.search.get_catalog_samples(n)


def search_catalogs(ra: float, dec: float) -> dict[str, list[dict]]:
    """
    Cone-search ANTARES' object catalogs at an (ra, dec) coordinate.

    The result is the same data as would be available to a Filter processing
    an Alert at that (ra, dec) position.

    Returned data structure is of form::

      {
        catalog_name_x: [obj_x1, obj_x2, ...],
        catalog_name_y: [obj_y1, ...]
        ...
      }

    Where ``obj_*`` objects are dicts representing rows from the catalog tables.

    :param ra:
    :param dec:
    :return: dict of lists
    """
    return antares_client.search.catalog_search(ra, dec)


def locus_from_dict(locus_dict) -> Locus:
    """
    Load a FilterContext(Locus) from it's `to_dict()` representation

    :param locus_dict: dict
    :return: Locus
    """
    return Locus.from_dict(locus_dict)


def locus_from_file(filename) -> Locus:
    """
    Load a FilterContext(LocusData) from a file.

    :param filename:
    :return: LocusData
    """
    return Locus.from_file(filename)


@inject
def upload_file(
    key: str,
    file_path: str,
    blob_repository: AbstractBlobRepository = Provide["blob_repository"],
):
    """
    Store a datafile in ANTARES for use by filters.

    The `key` must be formatted like::

        <author>_<file-name>_<version>.<extension>

    eg::

        stubens_myFile_v1.txt
        wolf_nnmodel_v4.pickle

    Files are stored as binary strings, not ASCII.
    All file types are supported.
    The Filter which uses the file is responsible for parsing it.

    :param key: key under which to store the file.
    :param file_path: path of file.
    :return: the key, for confirmation.
    """
    if blob_repository.get(id_=key):
        raise FileExistsError("There is already a file with the same key")
    else:
        with open(file_path, "rb") as f:
            blob_repository.add(Blob(id=key, data=f.read()))
            return key


def get_crash_log(log_id):
    return SFilterCrashLog.get(log_id)


def print_crash_log(log_id):
    from antares.deprecated.sql.schema import SFilterCrashLog

    cl = SFilterCrashLog.get(log_id)
    if cl is None:
        print("Crash log not found")
    print("Filter ID:", cl["filter_id"])
    print("Filter Version ID:", cl["filter_version_id"])
    print("Locus ID:", cl["locus_id"])
    print("Alert ID:", cl["alert_id"])
    print("Stacktrace:")
    print()
    print(cl["stacktrace"])


def locus_from_crash_log(crash_log_id):
    crash_log = get_crash_log(crash_log_id)
    return locus_from_dict(crash_log["locus"])


def get_locus_id_by_ztf_object_id(ztf_object_id):
    raise NotImplementedError
    # TODO


def get_thumbnails(alert_id: str):
    """
    Get thumbnail images for an alert.

    Not all alerts are guaranteed to have thumbnails.

    :param alert_id
    :return: dict
    """
    return antares_client.search.get_thumbnails(alert_id)
