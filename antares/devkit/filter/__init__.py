"""
Utilities to support users in developing and testing ANTARES filters.
"""

import copy
import inspect
import time
from typing import Optional, Union

import numpy as np

from antares.devkit.filter.harness import RunnableFilter
from antares.devkit.get_data import get_locus, get_locus_id, get_locus_ids
from antares.devkit.log import log, log_init
from antares.domain.models.filter import FilterContext as Locus
from antares.domain.models.filter import FilterDevkitApi as Filter
from antares.domain.models.filter import (
    FilterExecutable,
    alert_satisfies_filter_constraints,
    filter_context_guard,
    locus_satisfies_filter_constraints,
)
from antares.exceptions import HaltPipeline


def get_filter_executable(filter_code: Union[type[Filter], Filter]) -> FilterExecutable:
    filter_instance: Filter
    if inspect.isclass(filter_code) and issubclass(filter_code, Filter):
        filter_instance = filter_code()
    elif isinstance(filter_code, Filter):
        filter_instance = filter_code
    else:
        raise RuntimeError(
            "Filter subclass is required to generate a filter executable"
        )
    filter_executable = filter_instance.to_filter_executable()
    return filter_executable


def run_filter(
    f: Union[type[Filter], Filter],
    locus: Optional[Union[str, Locus]] = None,
    verbose: bool = False,
):
    """
    Test a Filter.

    :param f: Filter object or class
    :param locus: Locus, locus_id, or None
    :param verbose: if True, print detailed log
    """
    log_init("DEBUG" if verbose else None, system_wide=True)
    filter_executable = get_filter_executable(f)
    try:
        return _run_filter(filter_executable, locus=locus)
    finally:
        log_init(system_wide=True)


def _run_filter(
    filter_executable: FilterExecutable, locus: Optional[Union[str, Locus]] = None
):
    if isinstance(locus, Locus):
        pass
    elif locus is None or isinstance(locus, str):
        locus = get_locus(locus or get_locus_id())
    else:
        raise ValueError(f"Invalid argument passed: {locus} (should be Locus or str)")
    if locus is None:
        raise RuntimeError("Locus not found while running the filter")
    if not locus_satisfies_filter_constraints(locus, filter_executable):
        print(
            f"Locus {locus.locus_id} skipped because it doesn't satisfy the filter constraints:"
            " a property is not in the Locus's input_properties or an input_tag is not in the Locus's input_tags."
        )
        return {}
    if not alert_satisfies_filter_constraints(locus.alert, filter_executable):
        print(
            f"Locus {locus.locus_id} skipped because Alert {locus.alert.alert_id} doesn't satisfy the filter alert property constraints."
        )
        return {}
    # Run filter
    old_properties = copy.deepcopy(locus.properties)
    old_tags = copy.deepcopy(locus.tags)
    halt = False
    log.debug("")
    log.debug("Running filter:")
    log.debug("- - - - -")
    start_time = time.process_time()
    try:
        with filter_context_guard(locus, filter_executable.output_specification):
            filter_executable(locus)
    except HaltPipeline:
        log.debug("Filter raised HaltPipeline")
        halt = True
    except Exception as e:
        log.error("Filter crashed:\n\n%s", e)
    end_time = time.process_time()
    log.debug("- - - - -")
    log.debug("Filter completed.")
    log.debug("")

    new_locus_properties = {}
    for key, value in locus.properties.items():
        if key not in old_properties or locus.properties[key] != value:
            new_locus_properties[key] = value
    new_locus_tags = {key for key in locus.tags if key not in old_tags}

    # Print computed Locus Properties
    if new_locus_properties:
        log.debug("Filter computed new Locus Properties:")
        for k, v in new_locus_properties.items():
            log.debug('"{}" = {}'.format(k, repr(v)))
    else:
        log.debug("Filter did not compute new Locus Properties.")

    # Print computed tags
    if new_locus_tags:
        log.debug("Filter added Tags to the Locus:")
        for tag in new_locus_tags:
            log.debug(tag)
    else:
        log.debug("Filter did not Tag the Locus.")

    log.debug("Done.")
    log.debug("")
    return dict(
        locus_id=locus.locus_id,
        locus_data=locus,
        t=end_time - start_time,
        new_locus_properties=new_locus_properties,
        new_alert_properties={},
        new_tags=new_locus_tags,
        raised_halt=halt,
    )


def run_many(f, locus_ids=None, n=100, verbose=False, keep_results=True):
    """
    Run a filter on many Loci.

    Locus IDs may be passed in as `locus_ids` or will be chosen randomly.

    Response format::

      {
        'n': <number of times filter ran>
        'results': <dict mapping alert id to result of run_filter()>
        't_50_percentile': <50th percentile of filter execution time>
        't_90_percentile': <90th percentile of filter execution time>
        't_95_percentile': <95th percentile of filter execution time>
        't_99_percentile': <99th percentile of filter execution time>
      }

    :param f: Python Filter object or class
    :param locus_ids: list of locus_id, or None
    :param n: if locus_ids is None, this many random Locus IDs will be used.
    :param verbose:
    :param keep_results: if False, `results` are discarded and not returned
    :return: dict
    """
    log_init("DEBUG" if verbose else None, system_wide=True)
    try:
        filter_executable = get_filter_executable(f)
        if not locus_ids:
            if not n:
                raise RuntimeError("run_many() requires either `alert_ids` or `n`.")
            locus_ids = get_locus_ids(n)
        del n
        results = []
        times = []
        for locus_id in locus_ids:
            locus = get_locus(locus_id)
            result = _run_filter(filter_executable, locus)
            if result != {}:
                if keep_results:
                    results.append(result)
                times.append(result["t"])
        if times == []:
            return {
                "n": len(locus_ids),
                "results": None,
                "t_50_percentile": None,
                "t_90_percentile": None,
                "t_95_percentile": None,
                "t_99_percentile": None,
            }
        else:
            return {
                "n": len(locus_ids),
                "results": results or None,
                "t_50_percentile": np.percentile(times, 50),
                "t_90_percentile": np.percentile(times, 90),
                "t_95_percentile": np.percentile(times, 95),
                "t_99_percentile": np.percentile(times, 99),
            }
    finally:
        log_init(system_wide=True)
