import logging
import re
import traceback
from typing import Any

import numpy as np

from antares.deprecated.models import Locus
from antares.deprecated.sql.schema import SStorage
from antares.exceptions import HaltPipeline
from antares.utils.collections_extensions import (
    RestrictedWriteDictWrapper,
    RestrictedWriteSetWrapper,
)


class FilterConfigError(Exception):
    pass


class RunnableFilter(object):
    def __init__(self, f):
        """

        :param f: Filter
        """
        self._filter = f

    def setup(self):
        self._validate_filter_config()
        self._load_files()

        # Call setup()
        try:
            self._filter.setup()
        except Exception:
            logging.exception("Caught Exception: ")
            return traceback.format_exc()

    def _load_files(self):
        self._filter.files = {
            name: SStorage.get_data(name) for name in self._filter.REQUIRES_FILES or []
        }

    def run(self, locus: Locus):
        """
        Run this filter over `locus`.

        This function is responsible for constructing a `LocusFilterAdapter` object that
        restricts a filter's ability to edit certain properties.

        :param locus_data: LocusData (not LocusDataAPI)
        :return: None on success, else return a stacktrace str
        """
        try:
            adapter = LocusFilterAdapter(
                locus,
                writeable_properties=[
                    property["name"]
                    for property in self._filter.OUTPUT_LOCUS_PROPERTIES
                ],
                writeable_tags=[tag["name"] for tag in self._filter.OUTPUT_TAGS],
            )
            self._filter.run(adapter)
            return
        except HaltPipeline:
            raise
        except Exception as e:
            logging.exception("Caught Exception: ")
            return self.format_traceback()

    def format_traceback(self):
        return traceback.format_exc()

    def validate_locus_property(self, name, value):
        for prop_dict in self._filter.OUTPUT_LOCUS_PROPERTIES:
            if prop_dict["name"] == name:
                value = _validate_property_value(value)
                if value is not None:
                    value = _validate_property_type(value, prop_dict["type"])
                return value
        raise RuntimeError("Undeclared Locus property")

    def validate_alert_property(self, name, value):
        for prop_dict in self._filter.OUTPUT_ALERT_PROPERTIES:
            if prop_dict["name"] == name:
                value = _validate_property_value(value)
                if value is not None:
                    value = _validate_property_type(value, prop_dict["type"])
                return value
        raise RuntimeError("Undeclared Alert property")

    def validate_tag(self, name):
        if name not in {dic["name"] for dic in self._filter.OUTPUT_TAGS}:
            raise RuntimeError("Undeclared Tag")

    def _validate_filter_config(self):
        for dic in self._filter.OUTPUT_LOCUS_PROPERTIES:
            if not dic.get("name"):
                raise FilterConfigError('OUTPUT_LOCUS_PROPERTIES must have a "name"')
            if not dic.get("type"):
                raise FilterConfigError('OUTPUT_LOCUS_PROPERTIES must have a "type"')
            if not dic.get("description"):
                raise FilterConfigError(
                    'OUTPUT_LOCUS_PROPERTIES must have a "description"'
                )
            _validate_property_name(dic["name"])
            _validate_property_type_definition(dic["type"])
        for dic in self._filter.OUTPUT_TAGS:
            if not dic.get("name"):
                raise FilterConfigError('OUTPUT_TAGS must have a "name"')
            if not dic.get("description"):
                raise FilterConfigError('OUTPUT_TAGS must have a "description"')
            _validate_tag_name(dic["name"])

    @property
    def filter_id(self):
        return None

    @property
    def filter_version_id(self):
        return None

    @property
    def name(self):
        return self._filter.NAME

    @property
    def level(self):
        return None

    @property
    def priority(self):
        return None

    def disable(self, log_id):
        raise NotImplementedError

    def get_config(self, name):
        if name != name.upper():
            raise FilterConfigError(f"Config name '{name}' must be in uppercase")
        if name.startswith("_"):
            raise FilterConfigError(f"Config name '{name}' can't start with '_'")
        return getattr(self._filter, name)

    def __str__(self):
        return "<RunnableFilter {}>".format(self.name)

    __repr__ = __str__


VALID_PROPERTY_TYPES = {"int", "float", "str"}


def _validate_property_type_definition(type_str):
    if type_str not in VALID_PROPERTY_TYPES:
        raise FilterConfigError(
            f'Output property type "{type_str}" must be one of {VALID_PROPERTY_TYPES}'
        )


def _validate_filter_name(name):
    if not re.match(r"^[a-zA-Z][a-zA-Z0-9_\-. ]*$", name):
        raise FilterConfigError(
            f"Filter name '{name}' doesn't match the regex '^[a-za-z][a-za-z0-9_\\-. ]*$'"
        )
    if len(name) > 30:
        raise FilterConfigError(
            f"Filter name '{name}' can't have more than 30 characters"
        )


PROPERTY_NAME_REGEX = r"^[a-z][a-z0-9_]*$"
TAG_NAME_REGEX = r"^[a-z][a-z0-9_]*$"


def _validate_property_name(name):
    if not re.match(PROPERTY_NAME_REGEX, name):
        raise FilterConfigError(
            f'Property names must match regex "{PROPERTY_NAME_REGEX}"'
        )
    if len(name) > 100:
        raise FilterConfigError("Property names must be <= 100 characters long")


def _validate_tag_name(name):
    if not re.match(TAG_NAME_REGEX, name):
        raise FilterConfigError(f'Tag names must match regex "{TAG_NAME_REGEX}"')
    if len(name) > 100:
        raise FilterConfigError("Tag names must be <= 100 characters long")
    if len(name) > 249:
        raise ValueError(
            "Tag name can't have more than 249 characters because of kafka topic limits"
        )


def _validate_property_value(value):
    if value is None or np.isnan(value):
        return None
    if isinstance(value, float) or isinstance(value, np.floating):
        value = float(value)  # Convert numpy floats to vanilla floats
    if isinstance(value, int) or isinstance(value, np.integer):
        value = int(value)  # Convert numpy ints to vanilla ints
    if isinstance(value, bool):
        raise RuntimeError(f"{value} can not be boolean")
    if not isinstance(value, (str, int, float)):
        raise RuntimeError(f"{type(value)} is not available class ({str, int, float})")
    if value in {float("+inf"), float("-inf")}:
        raise RuntimeError(f"{value} can not be float('+inf') or float('-inf')")
    return value


def _validate_property_type(value: Any, type_str: str):
    _validate_property_type_definition(type_str)
    if type_str == "int":
        return int(value)
    if type_str == "float":
        return float(value)
    if type_str == "str":
        if not isinstance(value, str):
            raise RuntimeError(f"Value '{value}' is not a string when type_str='str'")
        return value
    raise RuntimeError(
        "Unexpected type_str"
    )  # Due to the validation, this line is unreachable


class LocusFilterAdapter:
    def __init__(self, locus: Locus, writeable_properties=(), writeable_tags=()):
        self.locus_id = locus.locus_id
        self.alerts = locus.alerts
        self.properties = RestrictedWriteDictWrapper(
            locus.properties, writeable_keys=writeable_properties
        )
        self.tags = RestrictedWriteSetWrapper(
            locus.tags, writeable_elements=writeable_tags
        )
        # self.lightcurve = locus.lightcurve
        # self.timeseries = locus.timeseries

    @property
    def alert(self):
        return self.alerts[-1]

    @property
    def halt(self):
        """
        Return the HaltPipeline exception class.

        Stages can raise this exception to halt the pipeline like::

          raise locus.halt

        :return: class `HaltPipeline`
        """
        return HaltPipeline

    def tag(self, name):
        """
        Tag the Locus.

        :param name: Tag name
        """
        self.tags.add(name)
