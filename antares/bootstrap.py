import os
from typing import Optional

from antares.settings import Container


def setup_dependency_container() -> Container:
    container = Container()
    if path := os.environ.get("ANTARES_CONFIG_YAML"):
        container.config.from_yaml(path, required=True)
    container.init_resources()
    return container


def bootstrap(container: Optional[Container] = None) -> Container:
    """
    Public interface to wire dependency injection.

    Parameters
    ----------
    container: Optional[Container] (default, None)

    Notes
    ----------
    This function should also be used in testing for stubbing interfaces. See
    `antares/test/conftest.py:container` for examples of setting up a testing environment.

    """
    container = container or setup_dependency_container()
    import antares.devkit.get_data
    import antares.entrypoints
    import antares.external.bigtable.bigtable
    import antares.external.elasticsearch.ingest
    import antares.external.sql.engine
    import antares.query

    container.wire(
        packages=[
            antares.devkit.get_data,
            antares.external.bigtable.bigtable,
            antares.external.elasticsearch.ingest,
            antares.external.sql.engine,
            antares.entrypoints,
            antares.query,
        ]
    )
    return container
