class KeyViolationException(Exception):
    pass


class StaleDataException(Exception):
    pass


class Timeout(Exception):
    pass


class HaltPipeline(Exception):
    pass
