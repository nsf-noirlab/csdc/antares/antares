import re
from typing import Optional

from antares.adapters.repository import AbstractAlertThumbnailRepository
from antares.domain.models import AlertThumbnail, AlertThumbnailType


class GcsAlertThumbnailRepository(AbstractAlertThumbnailRepository):
    # This repository is only used to retrieve data
    def __init__(self, bucket_url: str):
        self.bucket_url = bucket_url

    def _build_filename(
        self, candidate_id: str, thumbnail_type: AlertThumbnailType
    ) -> str:
        if thumbnail_type == AlertThumbnailType.DIFFERENCE:
            return f"candid{candidate_id}_pid{candidate_id[0:13]}_targ_diff.fits.png"
        elif thumbnail_type == AlertThumbnailType.SCIENCE:
            return f"candid{candidate_id}_pid{candidate_id[0:13]}_targ_sci.fits.png"
        elif thumbnail_type == AlertThumbnailType.TEMPLATE:
            return f"candid{candidate_id}_ref.fits.png"

    def _build_src(self, candidate_id: str, thumbnail_type: AlertThumbnailType) -> str:
        return self.bucket_url + self._build_filename(candidate_id, thumbnail_type)

    def _get_candidate_id(self, alert_id: str) -> Optional[str]:
        if match := re.match(r"ztf_candidate:(?P<candid>\d+)", alert_id):
            return match.group("candid")
        return None

    def list_by_alert_id(self, alert_id: str) -> list[AlertThumbnail]:
        if candidate_id := self._get_candidate_id(alert_id):
            return [
                AlertThumbnail(
                    id=self._build_filename(candidate_id, thumbnail_type),
                    src=self._build_src(candidate_id, thumbnail_type),
                    filemime="image/png",
                    filename=self._build_filename(candidate_id, thumbnail_type),
                    thumbnail_type=thumbnail_type,
                )
                for thumbnail_type in AlertThumbnailType
            ]
        return []
