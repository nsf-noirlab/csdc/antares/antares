import operator
import uuid
from collections.abc import Callable, Iterable
from datetime import datetime, timedelta
from typing import TYPE_CHECKING, List, Optional

import sqlalchemy.orm.exc
from sqlalchemy import Column, MetaData, Table, UniqueConstraint, desc, event, func
from sqlalchemy.dialects.mysql import (
    BOOLEAN,
    DATETIME,
    ENUM,
    FLOAT,
    INTEGER,
    JSON,
    LONGBLOB,
    MEDIUMTEXT,
    TEXT,
    TINYINT,
    VARCHAR,
)
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import mapper
from sqlalchemy.orm.session import Session
from sqlalchemy.types import TypeDecorator

if TYPE_CHECKING:  # Avoiding circular import
    from antares.adapters.notifications import AbstractInternalNotificationService

import logging

from antares.domain.models import (
    Announcement,
    Blob,
    Filter,
    FilterExecutable,
    FilterRevision,
    FilterRevisionStatus,
    GravWaveNotice,
    GravWaveNoticeTypes,
    JwtRecord,
    LocusAnnotation,
    User,
)
from antares.exceptions import KeyViolationException, StaleDataException
from antares.observability.tracing.base import AbstractTracer

from .base import (
    AbstractAnnouncementRepository,
    AbstractBlobRepository,
    AbstractFilterRepository,
    AbstractFilterRevisionRepository,
    AbstractGravWaveRepository,
    AbstractJwtBlocklistRepository,
    AbstractLocusAnnotationRepository,
    AbstractUserRepository,
    ListQueryFilters,
)

metadata = MetaData()


class UUIDAsInt(TypeDecorator):
    """
    This is a custom type that stores a UUID as an integer in the database backend.
    The fact that we use this is, frankly, terrible.

    Its purpose is to act as a shim while we migrate tables into Cassandra.

    """

    impl = INTEGER
    cache_ok = True

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = int(value)

        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = uuid.UUID(int=value)
        return value

    def process_literal_param(self, value, dialect):
        if value is not None:
            value = int(value)

        return value


filter_table = Table(
    "filter",
    metadata,
    Column(
        "filter_id",
        INTEGER(unsigned=True),
        autoincrement=True,
        primary_key=True,
        key="id",
    ),
    Column("name", VARCHAR(100), nullable=False, unique=True),
    Column("created_at", DATETIME, default=datetime.utcnow),
    Column("disabled_at", DATETIME),
    Column("updated_at", DATETIME, onupdate=datetime.utcnow),
    Column("user_id", UUIDAsInt(unsigned=True), key="owner_id"),
    Column("description", VARCHAR(2048)),
    Column("priority", INTEGER),
    Column("latest_version_id", INTEGER(unsigned=True)),  # Deprecated
    Column(
        "enabled_version_id", INTEGER(unsigned=True), key="enabled_filter_revision_id"
    ),
    Column("public", BOOLEAN, nullable=False, default=False),
    Column("version_id", INTEGER, nullable=False),
)


filter_revision_table = Table(
    "filter_version",
    metadata,
    Column(
        "filter_version_id",
        INTEGER(unsigned=True),
        autoincrement=True,
        primary_key=True,
        key="id",
    ),
    Column("filter_id", INTEGER(unsigned=True)),
    Column("created_at", DATETIME, default=datetime.utcnow),
    Column("code", TEXT),
    Column("comment", VARCHAR(255)),
    Column("feedback", VARCHAR(255), default=""),
    Column("validated_at", DATETIME, key="reviewed_at"),  # Deprecated
    Column("enabled_at", DATETIME),  # Deprecated
    Column("disabled_at", DATETIME),  # Deprecated
    Column("transitioned_at", DATETIME),
    Column("disabled_log_id", INTEGER(unsigned=True)),  # Deprecated
    Column("public", TINYINT, nullable=False, default=False),  # Deprecated
    Column(
        "status",
        ENUM(FilterRevisionStatus),
        nullable=False,
        default=FilterRevisionStatus.PENDING_REVIEW,
    ),
    Column("updated_at", DATETIME, onupdate=datetime.utcnow),
    Column("version_id", INTEGER, nullable=False),
)


locus_annotation_table = Table(
    "locus_annotation",
    metadata,
    Column("locus_annotation_id", INTEGER, primary_key=True, key="id"),
    Column("owner_id", UUIDAsInt(unsigned=True), nullable=False),
    Column("locus_id", VARCHAR(36), nullable=False),
    Column("comment", VARCHAR(255)),
    Column("favorited", BOOLEAN, default=False, key="favorite"),
    Column("version_id", INTEGER, nullable=False),
)


jwt_token_blocklist_table = Table(
    "token_blacklist",  # Named "blacklist" for compatibility w/ old database schema
    metadata,
    Column("token_id", INTEGER(unsigned=True), primary_key=True, autoincrement=True),
    Column("token_type", VARCHAR(10), nullable=False),
    Column("jti", VARCHAR(36), nullable=False),
    Column("expires", DATETIME, nullable=False),
)


user_table = Table(
    "user",
    metadata,
    Column(
        "user_id",
        UUIDAsInt(unsigned=True),
        autoincrement=True,
        primary_key=True,
        key="id",
    ),
    Column("name", VARCHAR(32), nullable=False),
    Column("username", VARCHAR(32), nullable=False, unique=True),
    Column("email", VARCHAR(255), nullable=False),
    Column("_password", VARCHAR(255), key="password_hash"),
    Column("staff", BOOLEAN, nullable=False, default=False),
    Column("admin", BOOLEAN, nullable=False, default=False),
    Column("version_id", INTEGER, nullable=False),
)


storage_table = Table(
    "storage",
    metadata,
    Column("storage_key", VARCHAR(255), primary_key=True, key="id"),
    Column("created_at", DATETIME, default=datetime.utcnow),
    Column("data", LONGBLOB),
    Column("version_id", INTEGER, nullable=False),
)


announcement_table = Table(
    "announcement",
    metadata,
    Column(
        "announcement_id",
        INTEGER(unsigned=True),
        autoincrement=True,
        primary_key=True,
        key="id",
    ),
    Column("active", BOOLEAN, nullable=False, default=False),
    Column("variant", VARCHAR(36), nullable=False),
    Column("message", VARCHAR(2048)),
    Column("created_at", DATETIME, default=datetime.utcnow),
    Column("version_id", INTEGER, nullable=False),
)

grav_wave_table = Table(
    "grav_wave_notice",
    metadata,
    Column(
        "grav_wave_notice_id",
        INTEGER(unsigned=True),
        autoincrement=True,
        primary_key=True,
        key="id",
    ),
    Column("gracedb_id", VARCHAR(32), nullable=False),
    Column("notice_type", ENUM(GravWaveNoticeTypes), nullable=False),
    Column("notice_datetime", DATETIME(fsp=6), nullable=False),
    Column("event_datetime", DATETIME(fsp=6)),
    Column("false_alarm_rate", FLOAT(precision=32, decimal_return_scale=None)),
    Column("skymap_base64", MEDIUMTEXT),
    Column("external_coinc", JSON),
    Column("full_notice", JSON),
    Column("version_id", INTEGER, nullable=False),
    UniqueConstraint("gracedb_id", "notice_datetime", name="uq_grav_wave"),
)


mapper(
    Filter,
    filter_table,
    version_id_col=filter_table.c.version_id,
)
mapper(
    FilterRevision,
    filter_revision_table,
    version_id_col=filter_revision_table.c.version_id,
)
mapper(JwtRecord, jwt_token_blocklist_table)
mapper(
    LocusAnnotation,
    locus_annotation_table,
    version_id_col=locus_annotation_table.c.version_id,
)
mapper(User, user_table, version_id_col=user_table.c.version_id)
mapper(Blob, storage_table, version_id_col=storage_table.c.version_id)
mapper(Announcement, announcement_table, version_id_col=announcement_table.c.version_id)
mapper(GravWaveNotice, grav_wave_table, version_id_col=grav_wave_table.c.version_id)


@event.listens_for(FilterRevision, "load")
def receive_filter_revision_load(target, context):
    """
    This event hook is called after initial attribute population is performed
    by SQLAlchemy. It is required because SQLAlchemy does not call `__init__` when
    recreating objects from database rows (therefore `__post_init__` is not called
    like expected from a dataclass).
    More info here: https://docs.sqlalchemy.org/en/13/orm/constructors.html
    """
    target.__post_init__()


# @event.listens_for(FilterRevisionSchema, "after_insert")
# def update_latest_filter_version(mapper, connection, target):
#     q = (
#         FilterSchema.__table__.update()
#             .where(FilterSchema.filter_id == target.filter_id)
#             .values(
#             latest_version_id=target.filter_version_id, updated_at=target.created_at
#         )
#     )
#     connection.execute(q)
#     return
#
#
# class FilterCrashLogSchema(Base):
#     __tablename__ = "filter_crash_log"
#     log_id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
#     created_at = Column(DATETIME, default=datetime.utcnow)
#     filter_id = Column(INTEGER(unsigned=True))
#     filter_version_id = Column(INTEGER(unsigned=True))
#     locus_id = Column(VARCHAR(100))
#     alert_id = Column(VARCHAR(100))
#     stacktrace = Column(LONGTEXT)
#     locus = Column(LONGTEXT)


def create_filter_id_for_cache(filter_: Filter) -> str:
    return f"{filter_.name}#{filter_.id}#{filter_.enabled_filter_revision_id}"


class SqlAlchemyFilterRepository(AbstractFilterRepository):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory
        self._filter_executables: dict[str, tuple[Filter, FilterExecutable]] = {}

    def get(self, id_: int) -> Optional[Filter]:
        session = self.session_factory()
        try:
            filter_ = session.query(Filter).get(id_)
            if filter_:
                session.expunge(filter_)
            return filter_
        except Exception:
            raise
        finally:
            session.close()

    def list(self) -> Iterable[Filter]:
        session = self.session_factory()
        try:
            for filter_ in session.query(Filter):
                session.expunge(filter_)
                yield filter_
        except Exception:
            raise
        finally:
            session.close()

    def update(self, filter_: Filter) -> None:
        try:
            self.add(filter_)
        except sqlalchemy.orm.exc.StaleDataError:
            raise StaleDataException("Attempted update of failed")

    def add(self, filter_: Filter) -> None:
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(filter_)
            session.commit()
            session.expunge(filter_)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception:
            raise
        finally:
            session.close()

    def list_by_owner_id(self, owner_id: uuid.UUID) -> Iterable[Filter]:
        session = self.session_factory()
        try:
            for filter_ in session.query(Filter).filter(Filter.owner_id == owner_id):
                session.expunge(filter_)
                yield filter_
        except Exception:
            raise
        finally:
            session.close()

    def list_enabled_filters(self) -> Iterable[Filter]:
        session = self.session_factory()
        try:
            for filter_ in session.query(Filter).filter(
                Filter.enabled_filter_revision_id.isnot(None)  # type: ignore
            ):
                session.expunge(filter_)
                yield filter_
        except Exception:
            raise
        finally:
            session.close()

    def list_enabled_public_filters(
        self, limit: Optional[int] = None, offset: Optional[int] = None
    ) -> Iterable[Filter]:
        session = self.session_factory()
        try:
            for filter_ in (
                session.query(Filter)
                .filter(
                    Filter.public == True,
                    Filter.enabled_filter_revision_id.isnot(None),  # type: ignore
                )
                .limit(limit)
                .offset(offset)
            ):
                session.expunge(filter_)
                yield filter_
        except Exception:
            raise
        finally:
            session.close()

    def get_filter_executables(
        self,
        filter_revision_repository: AbstractFilterRevisionRepository,
        internal_notification_service: "AbstractInternalNotificationService",
        tracer: AbstractTracer,
    ) -> List[tuple[Filter, FilterExecutable]]:
        """
        Loads the latest enabled filters either from the database (expensive)
        or from cache.

        :param filter_revision_repository:
        :return: list[tuple[Filter, FilterExecutable]]
        """
        with tracer.trace("get_filter_executables"):
            enabled_filters = {
                create_filter_id_for_cache(filter_): filter_
                for filter_ in self.list_enabled_filters()
            }
            if (
                self._filter_executables
                and self._filter_executables.keys() == enabled_filters.keys()
            ):
                return list(self._filter_executables.values())
            filter_ids_to_remove = (
                self._filter_executables.keys() - enabled_filters.keys()
            )
            for key in filter_ids_to_remove:
                self._filter_executables.pop(key)
            filter_ids_to_add = enabled_filters.keys() - self._filter_executables.keys()
            for key in filter_ids_to_add:
                filter_ = enabled_filters[key]
                try:
                    self._filter_executables[
                        create_filter_id_for_cache(filter_)
                    ] = filter_revision_repository.get_filter_executable(
                        filter_, tracer
                    )
                except Exception as e:  # get_filter_executable logs an error and raises
                    # This can be done in get_filter_executable, but the call will require to include this repository,
                    # resulting into more overhead of passing data through two classes (remember we cache the filters
                    # in this repository now). Since there is no other call to get_filter_executable than here,
                    # I leave the disable part here.
                    if filter_.enabled_filter_revision_id is None:
                        logging.exception(
                            "Filter `%s` doesn't have enabled_filter_revision_id",
                            filter_.name,
                        )
                        continue
                    filter_revision = filter_revision_repository.get(
                        filter_.enabled_filter_revision_id
                    )
                    if filter_revision is None:
                        # TODO: See if log or attach it to the trace
                        logging.exception(
                            "No revision found for filter `%s` with revision_id=%d",
                            filter_.name,
                            filter_.enabled_filter_revision_id,
                        )
                        continue
                    feedback = f"Disabled automatically by the pipeline: Filter failed to load as executable. Error: {e}"
                    filter_revision.disable(filter_=filter_, feedback=feedback)
                    filter_revision_repository.update(filter_revision)
                    self.update(filter_)
                    # Notify internal staff
                    with internal_notification_service as notifier:
                        notifier.notify_after_filter_revision_transitioned(
                            filter_,
                            filter_revision,
                        )
                    continue  # this could still be refactored
            return list(self._filter_executables.values())


class SqlAlchemyFilterRevisionRepository(AbstractFilterRevisionRepository):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory

    def get(self, id_: int) -> Optional[FilterRevision]:
        session = self.session_factory()
        try:
            filter_revision = session.query(FilterRevision).get(id_)
            if filter_revision:
                session.expunge(filter_revision)
            return filter_revision
        except Exception:
            raise
        finally:
            session.close()

    def get_filter_executable(
        self, filter_: Filter, tracer: AbstractTracer
    ) -> tuple[Filter, FilterExecutable]:
        with tracer.trace("get_filter_executable") as span:
            if filter_.enabled_filter_revision_id is None:
                raise RuntimeError(
                    f"Filter {filter_.name} doesn't have enabled_filter_revision_id"
                )
            filter_revision = self.get(filter_.enabled_filter_revision_id)
            if filter_revision is None:
                raise RuntimeError(
                    f"No revision found for filter `{filter_.name}` with revision_id={filter_.enabled_filter_revision_id}"
                )
            """
            Attempt to convert the filter_revision that a user has submitted to executable
            code. This is a DANGEROUS operation! We need to be careful about executing
            user-submitted code that could be malicious or poorly written. We handle the
            "maliciousness" check by manual review. We also want to prevent any crashing
            code from bringing down the rest of the pipeline.
            """
            try:
                filter_executable = filter_revision.to_filter_executable()
            except Exception as e:
                # Log and skip this filter
                logging.exception(
                    "Filter %s failed to load as executable:", filter_.name
                )
                span.set_error(f"Failed setting up filter {filter_.name}", e)
                raise
            return filter_, filter_executable

    def list_by_filter_id(self, filter_id: int) -> Iterable[FilterRevision]:
        session = self.session_factory()
        try:
            for filter_revision in session.query(FilterRevision).filter(
                FilterRevision.filter_id == filter_id
            ):
                session.expunge(filter_revision)
                yield filter_revision
        except Exception:
            raise
        finally:
            session.close()

    def update(self, filter_revision: FilterRevision) -> None:
        try:
            self.add(filter_revision)
        except sqlalchemy.orm.exc.StaleDataError:
            raise StaleDataException("Attempted update of failed")

    def add(self, filter_revision: FilterRevision) -> None:
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(filter_revision)
            session.commit()
            session.expunge(filter_revision)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception:
            raise
        finally:
            session.close()


class SqlAlchemyJwtBlocklistRepository(AbstractJwtBlocklistRepository):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory

    def add(self, jwt: JwtRecord) -> None:
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(jwt)
            session.commit()
            session.expunge(jwt)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception:
            raise
        finally:
            session.close()

    def get_by_jti(self, jti: str) -> Optional[JwtRecord]:
        session = self.session_factory()
        try:
            jwt = session.query(JwtRecord).filter(JwtRecord.jti == jti).one_or_none()
            if jwt:
                session.expunge(jwt)
            return jwt
        finally:
            session.close()


class SqlAlchemyLocusAnnotationRepository(AbstractLocusAnnotationRepository):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory

    def get(self, id_: int) -> Optional[LocusAnnotation]:
        session = self.session_factory()
        try:
            locus_annotation = session.query(LocusAnnotation).get(id_)
            if locus_annotation:
                session.expunge(locus_annotation)
            return locus_annotation
        except Exception:
            raise
        finally:
            session.close()

    def update(self, locus_annotation: LocusAnnotation) -> None:
        try:
            self.add(locus_annotation)
        except sqlalchemy.orm.exc.StaleDataError:
            raise StaleDataException("Attempted update of failed")

    def add(self, locus_annotation: LocusAnnotation) -> None:
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(locus_annotation)
            session.commit()
            session.expunge(locus_annotation)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception:
            raise
        finally:
            session.close()

    def list_by_owner_id(self, owner_id: uuid.UUID) -> Iterable[LocusAnnotation]:
        session = self.session_factory()
        try:
            for locus_annotation in session.query(LocusAnnotation).filter(
                LocusAnnotation.owner_id == owner_id
            ):
                session.expunge(locus_annotation)
                yield locus_annotation
        except Exception:
            raise
        finally:
            session.close()


class SqlAlchemyUserRepository(AbstractUserRepository):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory

    def get(self, id_: uuid.UUID) -> Optional[User]:
        session = self.session_factory()
        try:
            user = session.query(User).get(id_)
            if user:
                session.expunge(user)
            return user
        except Exception:
            raise
        finally:
            session.close()

    def get_by_username(self, username: str) -> Optional[User]:
        session = self.session_factory()
        try:
            user: Optional[User] = (
                session.query(User).filter(User.username == username).one_or_none()
            )
            if user:
                session.expunge(user)
            return user
        except StopIteration:
            return None
        finally:
            session.close()

    def update(self, user: User) -> None:
        try:
            self.add(user)
        except sqlalchemy.orm.exc.StaleDataError:
            raise StaleDataException("Attempted update of failed")

    def add(self, user: User) -> None:
        # TODO: Raise integrity error, see api/resources/filter/views.py
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(user)
            session.commit()
            session.expunge(user)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception:
            raise
        finally:
            session.close()

    def list(
        self, filtered_ids: Optional[Iterable[uuid.UUID]] = None
    ) -> Iterable[User]:
        session = self.session_factory()
        try:
            query = session.query(User)
            if filtered_ids:
                query = query.filter(User.id.in_(filtered_ids))  # type: ignore # Sqlalchemy 2.0 fix this
            for user in query:
                session.expunge(user)
                yield user
        except Exception:
            raise
        finally:
            session.close()


class SqlAlchemyBlobRepository(AbstractBlobRepository):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory

    def get(self, id_: str) -> Optional[Blob]:
        session = self.session_factory()
        try:
            blob = session.query(Blob).get(id_)
            if blob:
                session.expunge(blob)
            return blob
        except Exception:
            raise
        finally:
            session.close()

    def update(self, blob: Blob) -> None:
        try:
            self.add(blob)
        except sqlalchemy.orm.exc.StaleDataError:
            raise StaleDataException("Attempted update of failed")

    def add(self, blob: Blob) -> None:
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(blob)
            session.commit()
            session.expunge(blob)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception:
            raise
        finally:
            session.close()


class SqlAlchemyAnnouncementRepository(AbstractAnnouncementRepository):
    def __init__(self, session_factory: Callable[..., Session]):
        self.session_factory = session_factory

    def get(self, id_: int) -> Optional[Announcement]:
        session = self.session_factory()
        try:
            announcement = session.query(Announcement).get(id_)
            if announcement:
                session.expunge(announcement)
            return announcement
        except Exception:
            raise
        finally:
            session.close()

    def list(
        self, query_filters: Optional[ListQueryFilters] = None
    ) -> Iterable[Announcement]:
        session = self.session_factory()
        try:
            query = session.query(Announcement)
            if query_filters:
                query = query.filter(
                    *[
                        getattr(operator, query_filter["op"])(
                            getattr(Announcement, query_filter["field"]),
                            query_filter["value"],
                        )
                        for query_filter in query_filters
                    ]
                )
            for announcement in query:
                session.expunge(announcement)
                yield announcement
        except Exception:
            raise
        finally:
            session.close()

    def update(self, announcement: Announcement) -> None:
        try:
            self.add(announcement)
        except sqlalchemy.orm.exc.StaleDataError:
            raise StaleDataException("Attempted update of failed")

    def add(self, announcement: Announcement) -> None:
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(announcement)
            session.commit()
            session.expunge(announcement)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception:
            raise
        finally:
            session.close()


class SqlAlchemyGravWaveRepository(AbstractGravWaveRepository):
    def __init__(self, session_factory: Callable[..., Session], lookback_days=10):
        self.session_factory = session_factory
        self.lookback_days = lookback_days
        self._current_notices: dict[int, GravWaveNotice] = dict()

    def add(self, notice: GravWaveNotice) -> None:
        session = self.session_factory(expire_on_commit=False)
        try:
            session.add(notice)
            session.commit()
            session.expunge(notice)
        except IntegrityError as e:
            raise KeyViolationException(e)
        except Exception as e:
            raise
        finally:
            session.close()

    def get(self, id_: int) -> GravWaveNotice:
        session = self.session_factory()
        try:
            notice = session.query(GravWaveNotice).get(id_)
            if notice:
                session.expunge(notice)
        except Exception as e:
            raise
        finally:
            session.close()
        return notice

    def get_id(self, gracedb_id: str, notice_datetime: datetime) -> Optional[int]:
        session = self.session_factory()
        try:
            query_result = (
                session.query(GravWaveNotice.id)
                .filter_by(gracedb_id=gracedb_id, notice_datetime=notice_datetime)
                .first()
            )
            if query_result:
                return query_result.id
        except Exception as e:
            raise
        finally:
            session.close()
        return None

    def get_latest_by_gracedb_id(self, gracedb_id: str) -> Optional[GravWaveNotice]:
        session = self.session_factory()
        try:
            notice = (
                session.query(GravWaveNotice)
                .filter_by(gracedb_id=gracedb_id)
                .order_by(desc(GravWaveNotice.notice_datetime))
                .first()
            )
            if notice:
                session.expunge(notice)
            return notice
        except Exception:
            raise
        finally:
            session.close()

    def get_latest_by_gracedb_ids(
        self, gracedb_ids: list[str]
    ) -> Iterable[GravWaveNotice]:
        session = self.session_factory()
        try:
            subquery = (
                session.query(
                    GravWaveNotice.gracedb_id.label("gracedb_id"),  # type: ignore
                    func.max(GravWaveNotice.notice_datetime).label(
                        "max_notice_datetime"
                    ),
                )
                .filter(GravWaveNotice.gracedb_id.in_(gracedb_ids))  # type: ignore
                .group_by(GravWaveNotice.gracedb_id)
                .subquery()
            )
            notices = (
                session.query(GravWaveNotice)
                .join(
                    subquery,
                    (GravWaveNotice.gracedb_id == subquery.c.gracedb_id)
                    & (
                        GravWaveNotice.notice_datetime == subquery.c.max_notice_datetime
                    ),
                )
                .all()
            )
            for notice in notices:
                yield notice
        except Exception:
            raise
        finally:
            session.close()

    def get_latest_active_notice_ids(self) -> set[int]:
        session = self.session_factory()
        since_datetime = datetime.utcnow() - timedelta(days=self.lookback_days)
        try:
            subq = (
                session.query(
                    GravWaveNotice.id,
                    GravWaveNotice.notice_type,
                    func.row_number()
                    .over(
                        partition_by=GravWaveNotice.gracedb_id,
                        order_by=desc(GravWaveNotice.notice_datetime),
                    )
                    .label("n"),
                )
                .filter(GravWaveNotice.event_datetime > since_datetime)  # type: ignore
                .subquery()
            )
            query_result = (
                session.query(subq.c.id)
                .filter(subq.c.n == 1)
                .filter(subq.c.notice_type != GravWaveNoticeTypes.RETRACTION)
                .all()
            )
            if query_result:
                return {result.id for result in query_result}
        except Exception as e:
            raise
        finally:
            session.close()
        return set()

    def get_current_notices(self) -> list[GravWaveNotice]:
        current_ids = self.get_latest_active_notice_ids()

        if current_ids == self._current_notices.keys():
            return list(self._current_notices.values())

        for _id in current_ids - self._current_notices.keys():
            logging.info("Adding Notice %s to cache", _id)
            self._current_notices[_id] = self.get(_id)

        for _id in self._current_notices.keys() - current_ids:
            logging.info("Removing Notice %s from cache", _id)
            del self._current_notices[_id]

        return list(self._current_notices.values())
