"""Module to add, get and list locus from BigTable"""
import logging
from ast import literal_eval
from collections.abc import Iterable
from datetime import datetime
from typing import Optional

import bson
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from astropy.timeseries import TimeSeries as AstropyTimeSeries
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter
from google.cloud.bigtable.row_set import RowSet

from antares.adapters.repository.base import AbstractLocusRepository
from antares.adapters.repository.bigtable.schemas import LocusColumns
from antares.adapters.repository.bigtable.utils import (
    AlertLookupMethods,
    AlertLookupType,
    decode_any,
    encode_any,
)
from antares.domain.models import Locus
from antares.exceptions import KeyViolationException
from antares.utils import (
    astropy_table_from_string,
    astropy_table_to_string,
    mjd_to_datetime,
    spatial,
)


class LocusNotFoundError(Exception):
    """Error to show when searching for a locus is done, but there's no record of it"""

    pass


class BigtableLocusRepository(AbstractLocusRepository, AlertLookupMethods):
    """Concrete implementation of the locus repository backed by Bigtable.

    Attributes:
        locus_table: Table that stores all the information of locus
        locus_cf: Column family for locus in the locus table
        alert_lut: Table used to store locus_ids along with their HTM positions
        alert_lut_cf : Column family of the alert look up table

    This repository reads and writes the tables described by `BigtableLocusSchema` and
    `BigtableAlertLookupSchema`.

    Notes
    =====
    In v1.0.0 of the system we were, for a time, implementing a "locus merging"
    algorithm that sought to reconcile earlier mis-associations (e.g. if we had
    created two loci for alerts that we now think should belong together). This
    type of mutation was difficult to do correctly and ended up confusing a lot
    of users. We've stopped doing these sorts of merges but there is a lot of
    leftover artifacts from them in our database. When a locus was superseded by
    another, it got marked with `locus.properties["replaced_by"] = <id>`, the
    ID of the correct locus. The C* locus repository implementation hides this
    detail from the rest of the application and ignores any rows it finds where
    this property has been marked.
    """

    def __init__(
        self,
        instance_: BigtableInstance,
        locus_table_id: str,
        locus_cf: str,
        alert_lut_id: str,
        alert_lut_cf: str,
    ):
        super().__init__(
            alert_lut=instance_.table(alert_lut_id), alert_lut_cf=alert_lut_cf
        )
        self.locus_table = instance_.table(locus_table_id)
        self.locus_cf = locus_cf
        logging.info("BigtableLocusRepository created successfully")

    def create_locus_row(self, locus: Locus) -> DirectRow:
        """Method to create a bigtable row object which stores data as bytes"""
        row = self.locus_table.direct_row(LocusColumns.create_row_key(locus.id))
        row.set_cell(
            self.locus_cf, LocusColumns.RA, encode_any(locus.location.ra.deg, "double")
        )
        row.set_cell(
            self.locus_cf,
            LocusColumns.DEC,
            encode_any(locus.location.dec.deg, "double"),
        )
        properties = {
            "ant_grav_wave_events": locus.grav_wave_events,
            **locus.properties,
        }
        row.set_cell(self.locus_cf, LocusColumns.PROPS, bson.dumps(properties))
        row.set_cell(self.locus_cf, LocusColumns.TAGS, str(list(locus.tags)))
        row.set_cell(
            self.locus_cf,
            LocusColumns.INSTANCE_CREATED_DATETIME,
            encode_any(datetime.utcnow(), "timestamp"),
        )
        lightcurve = ""
        if locus.lightcurve:
            lightcurve = astropy_table_to_string(
                locus.lightcurve, "csv", exclude=["time"]
            )
        row.set_cell(self.locus_cf, LocusColumns.LIGHTCURVE, lightcurve)
        row.set_cell(self.locus_cf, LocusColumns.CATALOGS, str(list(locus.catalogs)))
        return row

    def create_locus_from_row(self, row: PartialRowData) -> Locus:
        """Method to deserialize bytes from a bigtable row to create a Locus object"""
        table = astropy_table_from_string(
            row.cell_value(self.locus_cf, LocusColumns.LIGHTCURVE).decode() or "",
            "csv",
        )
        if table:
            lightcurve = AstropyTimeSeries(
                table, time=mjd_to_datetime(table["ant_mjd"])
            )
        else:
            lightcurve = AstropyTimeSeries()
        properties = bson.loads(row.cell_value(self.locus_cf, LocusColumns.PROPS))
        grav_wave_events = properties.pop("ant_grav_wave_events", [])
        return Locus(
            id=row.row_key.decode(),
            location=SkyCoord(
                ra=decode_any(row.cell_value(self.locus_cf, LocusColumns.RA), "double"),
                dec=decode_any(
                    row.cell_value(self.locus_cf, LocusColumns.DEC), "double"
                ),
                unit=u.degree,
            ),
            properties=properties,
            grav_wave_events=grav_wave_events,
            tags=set(
                literal_eval(row.cell_value(self.locus_cf, LocusColumns.TAGS).decode())
            ),
            catalogs=set(
                literal_eval(
                    row.cell_value(self.locus_cf, LocusColumns.CATALOGS).decode()
                )
            ),
            lightcurve=lightcurve,
        )

    def get(self, id_: str) -> Optional[Locus]:
        """Fetches a single locus given a locus ID.

        Single lookup into the table described by `BigtableLocusSchema`.
        """
        if row := self.locus_table.read_row(id_, filter_=CellsColumnLimitFilter(1)):
            return self.create_locus_from_row(row)
        else:
            raise LocusNotFoundError(f"Locus {id_} not found")

    def add(self, locus: Locus) -> None:
        """Persist a locus in the backing Bigtable data store.

        This is a two-stage query path:

        1. Insert the locus into the `BigtableLocusSchema` table.
            This mutation MUST only insert values for the L (Locus) column family.

        2. Compute the HTM IDs of the trixels this locus is located in at each of the
           levels defined in `self.LOCUS_LOOKUP_HTM_LEVEL`. Insert these, along with
           the value of `BigtableAlertLookupType.LOCUS` and the locus's ID into the
           table described by `BigtableAlertLookupSchema`.

        Stages (1) and (2) are independent and can be executed concurrently, though
        they run serially in the current implementation.

        """
        if self.locus_table.read_row(locus.id, filter_=CellsColumnLimitFilter(1)):
            raise KeyViolationException
        # Insert a locus into the locus table
        row = self.create_locus_row(locus)
        row.commit()
        # Insert an entry into the HTM lookup table at each of the levels we may want to
        rows_to_write_in_alert_lut = self.create_alert_lut_rows_for_locus(locus)
        responses = self.alert_lut.mutate_rows(rows_to_write_in_alert_lut)
        errors = []
        for response in responses:
            if response.code != 0:
                errors.append(response.message)
        if errors:
            logging.error("%d errors while writing locus in alert_lut", len(errors))
            logging.error(errors)

    def update(self, locus: Locus) -> None:
        """Persist changes to `locus` in the `BigtableLocusSchema` table.
        TODO: Support wl_ids, wo_ids
        """
        row = self.create_locus_row(locus)
        row.set_cell(
            self.locus_cf,
            LocusColumns.UPDATED_DATETIME,
            encode_any(datetime.utcnow(), "timestamp"),
        )
        row.commit()

    def list_by_cone_search(self, location: SkyCoord, radius: Angle) -> Iterable[Locus]:
        """List all the loci near a location filtering by the radius of the search
        Note:This method takes so much time
        """
        htm_ids = spatial.get_htm_region_denormalized(location, radius, level=16)
        locus_ids = self._get_locus_ids_by_htm_ids(htm_ids)
        loci = self._list_by_locus_ids(locus_ids)
        yield from (
            locus for locus in loci if location.separation(locus.location) < radius
        )

    def _get_locus_ids_by_htm_ids(self, htm_ids: list[int]) -> list[str]:
        """To find the nearest locus to a position in the sky,
        we search for the locus based on its HTM IDs.
        """
        if htm_ids:
            row_set = RowSet()
            for htm in htm_ids:
                row_set.add_row_range_with_prefix(
                    f"{htm}#{AlertLookupType.LOCUS.value}"
                )
            locus_ids = []
            for row in self.alert_lut.read_rows(
                filter_=CellsColumnLimitFilter(1), row_set=row_set
            ):
                htm, _, value = row.row_key.decode().split("#")
                locus_ids.append(value)
            return locus_ids
        else:
            return []

    def _list_by_locus_ids(self, locus_ids: list[str]) -> Iterable[Locus]:
        """Fetch loci given a list of locus IDs.

        When we want the locus objects after searching locus ids
        near a location in the sky, this method helps in that process
        """
        if locus_ids:
            row_set = RowSet()
            for locus_id in locus_ids:
                if (
                    locus_id == ""
                ):  # defensive against an empty locus_id that would return the whole table
                    raise ValueError(
                        "ERROR: not expecting empty string for locus_id in locus_ids"
                    )
                row_set.add_row_key(locus_id)
            for row in self.locus_table.read_rows(
                filter_=CellsColumnLimitFilter(1), row_set=row_set
            ):
                locus = self.create_locus_from_row(row)
                if not locus.properties.get("replaced_by"):
                    yield locus
        else:
            return
            yield
