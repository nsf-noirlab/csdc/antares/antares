"""Module to add, get and delete watch_lists from BigTable

This module adds the BigtableWatchListRepository class and the build_owner_id_filter
method.

TODO:
    * Handle version_id
        # row.set_cell(self.watch_list_cf, "v", watch_list.version_id) doesn't exist
"""

import logging
from datetime import datetime
from typing import Optional
from uuid import UUID

from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData
from google.cloud.bigtable.row_filters import (
    BlockAllFilter,
    CellsColumnLimitFilter,
    ColumnQualifierRegexFilter,
    ConditionalRowFilter,
    ExactValueFilter,
    PassAllFilter,
    RowFilterChain,
)

from antares.adapters.repository.base import AbstractWatchListRepository
from antares.adapters.repository.bigtable.schemas import WatchListColumns
from antares.adapters.repository.bigtable.utils import decode_any, encode_any
from antares.domain.models import WatchList


def build_owner_id_filter(watch_list_owner_id: UUID):
    """Creates a ConditionalRowFilter to filter the rows that match
        owner_id input and return all columns

    The steps taken to filter are:
    First performs a search for the required owner_id in all 'o' cells of all rows:
    - If the owner_id is found, all cells of the rows will be returned.
    - If not, no rows will be returned.

    If this search is done without the ConditionalRowFilter with PassAllFilter and
    BlockAllFilter, the cell values needed to create the watch_list are not found,
    as it will only return the owner_id cell.
    """
    condition = RowFilterChain(
        filters=[
            CellsColumnLimitFilter(1),
            ColumnQualifierRegexFilter(WatchListColumns.OWNER_ID),
            ExactValueFilter(str(watch_list_owner_id)),
        ],
    )
    conditional_filter = ConditionalRowFilter(
        base_filter=condition,
        true_filter=PassAllFilter(True),
        false_filter=BlockAllFilter(True),
    )
    return conditional_filter


class BigtableWatchListRepository(AbstractWatchListRepository):
    """Class that works as a mediator between the system and bigtable for watch_lists

    This repository reads, writes and deletes on the `watch_list` table.

    Attributes:
        watch_list_table: Table that stores all the information of the watch_lists
        watch_list_cf: Column family for watch_list in the watch_list table
    Note:
        We handle cases by looking if any column exists and that is because some
        rows were inserted into the database without that column. I think this was
        done manually because some rows have test names.
    """

    def __init__(
        self, instance_: BigtableInstance, watch_list_table_id: str, watch_list_cf: str
    ):
        self.watch_list_table = instance_.table(watch_list_table_id)
        self.watch_list_cf = watch_list_cf
        logging.info("BigtableWatchListRepository created successfully")

    def create_watchlist_from_row(self, watch_list_row: PartialRowData) -> WatchList:
        """Method to deserialize bytes from a bigtable row to create a WatchList object"""
        if (
            WatchListColumns.CREATED_DATETIME
            in watch_list_row.cells[self.watch_list_cf]
        ):
            created_at = decode_any(
                watch_list_row.cell_value(
                    self.watch_list_cf, WatchListColumns.CREATED_DATETIME
                ),
                "timestamp",
            )
        else:
            created_at = datetime.min
        if not isinstance(created_at, datetime):
            raise ValueError(
                f"Expected 'created_at' to be datetime. Argument given: {created_at}"
            )
        if WatchListColumns.ENABLED in watch_list_row.cells[self.watch_list_cf]:
            enabled = decode_any(
                watch_list_row.cell_value(self.watch_list_cf, WatchListColumns.ENABLED),
                "bool",
            )
        else:
            enabled = False
        if not isinstance(enabled, bool):
            raise ValueError(
                f"Expected 'enabled' to be boolean. Argument given: {enabled}"
            )
        if WatchListColumns.SLACK_CHANNEL in watch_list_row.cells[self.watch_list_cf]:
            slack_channel = watch_list_row.cell_value(
                self.watch_list_cf, WatchListColumns.SLACK_CHANNEL
            ).decode()
        else:
            slack_channel = ""
        return WatchList(
            owner_id=UUID(
                hex=(
                    watch_list_row.cell_value(
                        self.watch_list_cf, WatchListColumns.OWNER_ID
                    ).decode()
                )
            ),
            name=watch_list_row.cell_value(
                self.watch_list_cf, WatchListColumns.NAME
            ).decode(),
            description=watch_list_row.cell_value(
                self.watch_list_cf, WatchListColumns.DESCRIPTION
            ).decode(),
            slack_channel=slack_channel,
            created_at=created_at,
            enabled=enabled,
            id=UUID(hex=watch_list_row.row_key.decode()),
        )

    def create_watchlist_row(self, watch_list: WatchList) -> DirectRow:
        """Method to create a bigtable row object which stores data as bytes"""
        row = self.watch_list_table.direct_row(
            WatchListColumns.create_row_key(watch_list.id)
        )
        row.set_cell(
            self.watch_list_cf,
            WatchListColumns.OWNER_ID,
            str(watch_list.owner_id),
        )
        row.set_cell(self.watch_list_cf, WatchListColumns.NAME, watch_list.name)
        row.set_cell(
            self.watch_list_cf,
            WatchListColumns.DESCRIPTION,
            watch_list.description,
        )
        row.set_cell(
            self.watch_list_cf,
            WatchListColumns.SLACK_CHANNEL,
            watch_list.slack_channel,
        )
        row.set_cell(
            self.watch_list_cf,
            WatchListColumns.CREATED_DATETIME,
            encode_any(watch_list.created_at, "timestamp"),
        )
        row.set_cell(
            self.watch_list_cf,
            WatchListColumns.ENABLED,
            encode_any(watch_list.enabled, "bool"),
        )
        return row

    def add(self, watch_list: WatchList):
        """Method to add a WatchList to the watch_list table"""
        row = self.create_watchlist_row(watch_list)
        row.commit()

    def get(self, watchlist_id: UUID) -> Optional[WatchList]:
        """Method to retrieve a WatchList object given its id"""
        if row := self.watch_list_table.read_row(
            str(watchlist_id), filter_=CellsColumnLimitFilter(1)
        ):
            return self.create_watchlist_from_row(row)
        return None

    def list_by_owner_id(self, watch_list_owner_id: UUID) -> list[WatchList]:
        """Method to retrieve all WatchList objects associated with a WatchList
        owner id by using a filter
        """
        watch_lists = []
        for row in self.watch_list_table.read_rows(
            filter_=build_owner_id_filter(watch_list_owner_id)
        ):
            watch_lists.append(self.create_watchlist_from_row(row))
        return watch_lists

    def delete(self, watch_list_id: UUID):
        """Method to delete a WatchList from the watch_list table
        by using a WatchList id
        Note:
            Even if we use CellsColumnLimitFilter, the entire row is removed.
        """
        row = self.watch_list_table.row(
            str(watch_list_id), filter_=CellsColumnLimitFilter(1)
        )
        row.delete()
        row.commit()
