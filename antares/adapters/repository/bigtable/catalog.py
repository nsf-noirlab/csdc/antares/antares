"""Module to add, get and list catalog objects from BigTable"""
import logging
import math
from collections.abc import Iterable
from pathlib import Path
from typing import Any, Literal, Optional, SupportsFloat, SupportsIndex, TypedDict

import yaml
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter
from google.cloud.bigtable.row_set import RowSet
from google.cloud.bigtable.table import Table

from antares.adapters.repository.base import AbstractCatalogObjectRepository
from antares.adapters.repository.bigtable.schemas import (
    CatalogColumns,
    CatalogLutColumns,
)
from antares.adapters.repository.bigtable.utils import (
    LOOKUP_HTM_LEVEL,
    decode_any,
    encode_any,
    get_optimal_htm_level,
)
from antares.domain.models import Catalog, CatalogObject
from antares.observability.metrics.base import AbstractMetrics
from antares.utils import spatial


class BigtableCatalogObjectTableDescription(TypedDict):
    """
    Description of the minimum necessary columns in each "catalog object" table.

    To explain a bit more--all of our cross-match catalogs share a lookup table and
    entries in that table contain (catalog_id, catalog_object_id) tuples. The objects of
    each catalog are stored in a different table, and the configuration of those tables
    is passed to ANTARES at run-time.

    A configuration object of type `list[BigtableCatalogObjectTableDescription]` needs
    to be passed to the `BigtableCatalogObjectRepository` on instantiation. This is
    necessary so that the repository, on hitting the lookup table and receiving back
    (catalog_id, catalog_object_id) tuples, can map the `catalog_id`, via this object,
    to a table name (`table`). It can then dynamically build a query that fetches the
    objects with row_key `catalog_object_id` from the appropriate,
    catalog-specific table.

    Note:
    The column families that are set for a catalog should never change, because if
    we did change them, it would make hard to retrieve the written information. (It
    would require a direct connection to DB and fetch instead of using an API).
    Another option is to use only one column_family.
    """

    table: str
    column_family: bytes
    # catalog_id breaks convention with the rest of our codebase which prefers strings
    # for IDs. This is to preserve backwards compatibility.
    catalog_id: int
    display_name: str
    enabled: bool
    ra_column: str
    dec_column: str
    object_id_column: str
    object_id_type: str
    object_name_column: str
    radius: Optional[Angle]
    radius_column: Optional[str]
    radius_unit: Optional[Literal["degree", "arcsecond"]]
    columns_with_data_types: dict


def check_properties_have_minimal_columns(
    properties: dict, table_description: BigtableCatalogObjectTableDescription
):
    required_columns = {
        table_description[column]  # type: ignore # (see: https://github.com/python/mypy/issues/4122)
        for column in (
            "ra_column",
            "dec_column",
            "object_id_column",
            "object_name_column",
        )
    }
    if table_description["radius_column"]:
        required_columns.add(table_description["radius_column"])
    if not required_columns.issubset(properties.keys()):
        raise ValueError("Catalog row doesn't have the required catalog properties")


def load_schema_values(
    schema_file: Path, schema: BigtableCatalogObjectTableDescription
) -> BigtableCatalogObjectTableDescription:
    """
    We avoid updating schema directly because if we call this method twice,
    new_schema["radius"] will be an Angle instance and will throw an error
    """
    with open(schema_file, encoding="utf-8") as file:
        updated_schema = schema.copy()
        table_schemas = yaml.safe_load(file)
        schema_key = schema["table"]
        if table_schemas is None:
            raise ValueError(f"schema_file: {schema_file} is empty")
        if schema_key not in table_schemas:
            raise ValueError(
                f"'{schema_key}' not in schema_file: {table_schemas.keys()}"
            )
        updated_schema["columns_with_data_types"] = table_schemas[schema_key]
        if updated_schema["radius"]:
            if isinstance(
                updated_schema["radius"], (SupportsFloat, SupportsIndex)
            ) and not math.isnan(updated_schema["radius"]):
                updated_schema["radius"] = Angle(updated_schema["radius"], unit=u.deg)
            else:
                raise RuntimeError(
                    "Schema radius must be a number in degrees. " f"schema: {schema}"
                )
        return updated_schema


class BigtableCatalogObjectRepository(AbstractCatalogObjectRepository):
    """Concrete implementation of the catalog object repository backed by BigTable.

    Attributes:
        catalog_tables_with_description: Dictionary of catalog table and descriptions
        catalog_lut: Table used to store catalogs along with their HTM positions
        catalog_lut_cf : Column family of the catalog look up table

    Catalog objects are indexed by the location of the region on the HTM sphere they
    overlap with. We store both "extended source" and "point source" type objects (the
    former extend over a circular region while the latter are points on the sky).
    Internally, everything is considered an extended source--point source objects are
    stored with a small radius of some single digit arc-seconds.

    We have these tables in the backing BigTable data store:
        * alert_lut: An HTM id -> (catalog id, catalog object id) lookup table.
            The level of the HTM id is determined based on the radius of the
            object--an optimal level is selected from among the options in
            the LOOKUP_HTM_LEVEL variable of this class.
        * Zero or more catalog tables that use (catalog object id) as their partition
          key and have any number of catalog-dependent other columns.
    """

    def __init__(
        self,
        instance_: BigtableInstance,
        catalog_lut_id: str,
        catalog_lut_cf: str,
        schemas: list[BigtableCatalogObjectTableDescription],
        schema_file: str,
    ):
        catalogs: list[Catalog] = []
        self.catalog_tables_with_description: dict[
            int, tuple[Table, BigtableCatalogObjectTableDescription]
        ] = {}
        for schema in schemas:
            if schema["enabled"]:
                updated_schema = load_schema_values(Path(schema_file), schema)
                catalogs.append(
                    Catalog(
                        str(updated_schema["catalog_id"]), str(updated_schema["table"])
                    )
                )
                self.catalog_tables_with_description[updated_schema["catalog_id"]] = (
                    instance_.table(updated_schema["table"]),
                    updated_schema,
                )
        super().__init__(catalogs)
        if len(self.catalog_tables_with_description) != len(catalogs):
            raise ValueError(
                "There are some duplicates of catalog_id in catalog table descriptions"
            )
        self.catalog_lut = instance_.table(catalog_lut_id)
        self.catalog_lut_cf = catalog_lut_cf
        logging.info(
            "BigtableCatalogObjectRepository created successfully with %d schemas enabled",
            len(catalogs),
        )

    def get_catalog_table_with_description(
        self, catalog_id: int
    ) -> tuple[Table, BigtableCatalogObjectTableDescription]:
        """Retrieve a Catalog table along with its description by the catalog_id
        Since we load multiple catalog at load time, we have to access to that
        dictionary and this method helps with that.
        """
        if not isinstance(catalog_id, int):
            raise ValueError(
                f"catalog_id must be an integer. Received type: {type(catalog_id)}"
            )
        catalog_table_with_description = self.catalog_tables_with_description.get(
            catalog_id
        )
        if catalog_table_with_description is None:
            raise ValueError(
                "Catalog table description could not be found for catalog id: "
                f"{catalog_id}. The defined catalog ids are "
                f"{self.catalog_tables_with_description.keys()}"
            )
        return catalog_table_with_description

    def add(self, catalog_object: CatalogObject) -> None:
        """Save a new catalog object to the database.

        This is a two-stage query path:

        1. Determining the optimal HTM level for storing an entry in the lookup table
           and calculating the HTM trixels that the circular region defined by the
           object's location and radius intersects.

        2. Inserting the catalog object into the appropriate table and (catalog id,
           catalog object id) tuples into the lookup table.

        Queries (1) and (2) may be executed in parallel, but they are not in the current
        implementation.
        """
        catalog_table, table_description = self.get_catalog_table_with_description(
            int(catalog_object.catalog_id)
        )

        if catalog_object.radius is not None:
            htm_level = get_optimal_htm_level(catalog_object.radius)
            htm_ids = spatial.get_htm_region_denormalized(
                catalog_object.location,
                catalog_object.radius,
                htm_level,
            )

        elif table_description["radius"] is not None:
            htm_level = get_optimal_htm_level(table_description["radius"])
            htm_ids = spatial.get_htm_region_denormalized(
                catalog_object.location,
                table_description["radius"],
                htm_level,
            )
        else:
            raise ValueError(
                "Catalog object has no radius and/or table has no default radius"
            )
        check_properties_have_minimal_columns(
            catalog_object.properties, table_description
        )
        # 1. Determine optimal HTM level and the overlapping trixels
        value = f"{catalog_object.catalog_id}:{catalog_object.id}"
        # 2. Insert into the lookup table
        rows_to_write_in_catalog_lut = [
            self.create_catalog_lut_row(level, value) for level in htm_ids
        ]
        self.catalog_lut.mutate_rows(rows_to_write_in_catalog_lut)
        # 3. Insert into the catalog table
        self.insert_catalog_object_into_its_catalog_table(
            catalog_object, catalog_table, table_description
        )

    def create_catalog_lut_row(self, htm_level: int, value: str) -> DirectRow:
        """Method to create a bigtable row object which stores data as bytes"""
        row = self.catalog_lut.direct_row(
            CatalogLutColumns.create_row_key(htm_level, value)
        )
        row.set_cell(self.catalog_lut_cf, CatalogLutColumns.DATA, b".")
        return row

    def insert_catalog_object_into_its_catalog_table(
        self,
        catalog_object: CatalogObject,
        catalog_table: Table,
        table_description: BigtableCatalogObjectTableDescription,
    ):
        """Method to create a bigtable row which stores data as bytes
        Since we have multiple catalogs, we have to create a connection to
        the table that we want to write to and then assign the values to
        the row.

        Note:
        An option to store the catalogs is to store all the properties in a cell,
        but this can have performance issues due to storing too much data in one
        cell because a catalog can have more than 10 MB of data in a single cell
        and that's not recommended.
        Also, filters query catalogs by their column names and not by a
        properties value. If we stored all the values of a catalog in a
        single cell, the API for the filters would need to be changed.
        Important:
        This method is not being used because catalogs are ingested using other
        tools like cbt. All variables are retrieved from the properties and
        not from the catalog attributes.
        Catalog inside properties must be in the same unit as defined in the
        table_description.
        """
        row = catalog_table.direct_row(CatalogColumns.create_row_key(catalog_object.id))
        if table_description["radius_column"]:
            if catalog_object.radius is None:
                raise ValueError(
                    "Can't insert catalog with radius_column when radius is None"
                )
            catalog_object.properties[table_description["radius_column"]] = float(
                catalog_object.radius.to(table_description["radius_unit"]).value
            )
        for column, value in catalog_object.properties.items():
            encoded_value = encode_any(
                value, table_description["columns_with_data_types"][column]
            )
            row.set_cell(
                table_description["column_family"],
                column.encode(),
                encoded_value,
            )
        row.commit()

    def create_catalog_object_from_row(
        self,
        catalog_object_row: PartialRowData,
        table_description: BigtableCatalogObjectTableDescription,
    ) -> CatalogObject:
        """Method to deserialize bytes from a bigtable row to create a CatalogObject
        First we fill all the properties with None, then we retrieve the values from
        the cells in cases when a value exists.
        Note: when accessing BigTable cells, the data comes inside a list
        Important: When decoding catalogs the row_key can only be a byte string that
        can be decoded as string or integer, but in cases of mismatches between the id
        specified in table_description["columns_with_data_types"] and
        table_description["object_id_type"], the value inside the property will
        use the table_description["columns_with_data_types"] type
        """
        row_key = catalog_object_row.row_key.decode()
        if table_description["object_id_type"] == "int":
            object_id = int(row_key)
        else:
            object_id = row_key
        properties: dict[str, Any] = {}
        for column in table_description["columns_with_data_types"].keys():
            properties[column] = None
        properties[table_description["object_id_column"]] = object_id
        for column, cells in catalog_object_row.cells[
            table_description["column_family"]
        ].items():
            column = column.decode()
            data = cells[0].value
            if column in table_description["columns_with_data_types"].keys():
                properties[column] = decode_any(
                    data, table_description["columns_with_data_types"][column]
                )
            else:
                raise ValueError(
                    f"Row {row_key} have a column that is not specified in the columns_with_data_types dictionary "
                    f'input: {column}. columns_with_data_types: {table_description["columns_with_data_types"].keys()}'
                )
        if (
            table_description["radius_column"] is not None
            and properties[table_description["radius_column"]] is not None
        ):
            radius = Angle(
                properties[table_description["radius_column"]],
                unit=table_description["radius_unit"],
            )
        elif table_description["radius"] is not None:
            radius = table_description["radius"]
        else:
            radius = Angle(1, unit=u.arcsecond)
        return CatalogObject(
            id=catalog_object_row.row_key.decode(),
            name=properties[table_description["object_name_column"]],
            catalog_id=str(table_description["catalog_id"]),
            catalog_name=table_description["table"],
            location=SkyCoord(
                ra=properties[table_description["ra_column"]],
                dec=properties[table_description["dec_column"]],
                unit=u.deg,
            ),
            radius=radius,
            properties=properties,
        )

    def list_by_catalog_id(self, catalog_id: str) -> Iterable[CatalogObject]:
        """Yield objects from a particular catalog.
        This is useful for getting sample data.
        """
        catalog_table, table_description = self.get_catalog_table_with_description(
            int(catalog_id)
        )
        yield from (
            self.create_catalog_object_from_row(row, table_description)
            for row in catalog_table.read_rows(filter_=CellsColumnLimitFilter(1))
        )

    def list_by_location(
        self, location: SkyCoord, metrics: AbstractMetrics
    ) -> Iterable[CatalogObject]:
        """Find all catalog objects that overlap `location`.

        This is a two-step process:

        1. Map the location to its HTM ID at various levels and query the HTM id ->
           (catalog id, catalog object id) lookup table for all matching (catalog id,
           catalog object id) tuples.

        2. Map the catalog ids of each tuple returned with the respective
            table names and read the row of each object.
        """
        # Map the queried location to its associated HTM trixels at various levels.
        row_set = RowSet()
        for level in LOOKUP_HTM_LEVEL:
            htm = spatial.get_htm_id(location, level)
            row_set.add_row_range_with_prefix(f"{htm}#")
        catalog_matches: list[
            tuple[Table, BigtableCatalogObjectTableDescription, str]
        ] = []
        # 1. Query the lookup table for (catalog_id, catalog_object_id) tuples.
        for row in self.catalog_lut.read_rows(
            filter_=CellsColumnLimitFilter(1), row_set=row_set
        ):
            _, value = row.row_key.decode().split("#")
            catalog_id, catalog_object_id = value.split(":")
            try:
                (
                    catalog_table,
                    table_description,
                ) = self.get_catalog_table_with_description(int(catalog_id))
                catalog_matches.append(
                    (catalog_table, table_description, catalog_object_id)
                )
            except ValueError:
                logging.warning(
                    (
                        "Catalog_id = %s not found, it should be disabled or no longer exist."
                        " Remove it from the database to avoid this error"
                    ),
                    catalog_id,
                )
        # 2. Lookup catalog objects from the appropriate catalog tables.
        for catalog_table, table_description, catalog_object_id in catalog_matches:
            row = catalog_table.read_row(
                catalog_object_id.encode(), filter_=CellsColumnLimitFilter(1)
            )
            if not row:
                # Handle the case where we have a "phantom" LUT entry and there is no
                # matching object in the catalog tables.
                metrics.increment_repository_catalog_object_not_found(
                    table_description["display_name"]
                )
                logging.warning(
                    "Catalog object not found (catalog_id=%d, object_id=%s)",
                    table_description["catalog_id"],
                    catalog_object_id,
                )
                continue
            catalog_object = self.create_catalog_object_from_row(row, table_description)
            if location.separation(catalog_object.location) < catalog_object.radius:
                yield catalog_object
