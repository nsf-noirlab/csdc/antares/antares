"""Module that stores methods used by multiple repositories
We use C data types names because that is the data types used by the databases. In this case, Cassandra and HBase.
HBase is used in dataproc to ingest data directly into Bigtable
Fore more information about data types: https://docs.python.org/3/library/struct.html#format-characters
"""
import struct
from dataclasses import dataclass
from datetime import datetime, timezone
from decimal import Decimal
from enum import Enum
from typing import Union

from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from google.cloud.bigtable.row import Row
from google.cloud.bigtable.row_set import RowSet
from google.cloud.bigtable.table import Table

from antares.adapters.repository.bigtable.schemas import AlertLutColumns
from antares.domain.models import Locus, WatchObject
from antares.utils import spatial


def create_prefix_row_set(prefix: str) -> RowSet:
    """Creates a RowSet that search all keys starting with prefix"""
    row_set = RowSet()
    row_set.add_row_range_with_prefix(f"{prefix}#")
    return row_set


class AlertLookupType(Enum):
    """Possible values that the `type` column in the alert_lut table can take"""

    LOCUS = 1
    WATCH_OBJECT = 2


def get_optimal_htm_level(radius: Angle) -> int:
    """Returns htm level based on angle"""
    if radius <= Angle(angle=0.001, unit=u.deg):
        return 16
    elif radius <= Angle(angle=0.005, unit=u.deg):
        return 14
    elif radius <= Angle(angle=0.02, unit=u.deg):
        return 12
    elif radius <= Angle(angle=0.05, unit=u.deg):
        return 10
    elif radius <= Angle(angle=0.2, unit=u.deg):
        return 8
    elif radius <= Angle(angle=1.0, unit=u.deg):
        return 6
    else:
        return 4


LOOKUP_HTM_LEVEL = {4, 6, 8, 10, 12, 14, 16}


@dataclass
class AlertLookupMethods:
    """Class to store all the alert look up table interactions

    Alert_lut means alert look up table

    Since multiple repositories use this table, it's better to store all the code
    in one place instead of having a couple of functions that works with this
    table in each repository.

    Attributes needed:
        alert_lut: Table that stores all information about lookups of alerts/locus
        alert_lut_cf: Column family of the alert look up table
    """

    alert_lut: Table
    alert_lut_cf: str

    def create_alert_lut_row(self, htm: int, type_value: int, value: str) -> Row:
        """Method to create a bigtable row object which stores data as bytes
        Since all of this information is stored in row_key and
        BigTable doesn't allow rows with no data, we use a column that
        only stores a dot
        """
        row = self.alert_lut.direct_row(
            AlertLutColumns.create_row_key(htm, type_value, value)
        )
        row.set_cell(self.alert_lut_cf, AlertLutColumns.DATA, b".")
        return row

    def create_alert_lut_rows_for_locus(self, locus: Locus):
        """Creates a row to store a locus_id using a htm_id"""
        rows = []
        for level in LOOKUP_HTM_LEVEL:
            htm_id = spatial.get_htm_id(locus.location, level)
            rows.append(
                self.create_alert_lut_row(
                    htm_id,
                    AlertLookupType.LOCUS.value,
                    locus.id,
                )
            )
        return rows

    def create_alert_lut_rows_for_watch_object(self, watch_object: WatchObject):
        """Creates a row to store a watch_object ids using a htm_id"""
        htm_ids = spatial.get_htm_region_denormalized(
            watch_object.location,
            watch_object.radius,
            get_optimal_htm_level(watch_object.radius),
        )
        rows = []
        for htm_id in htm_ids:
            row = self.create_alert_lut_row(
                htm=htm_id,
                type_value=AlertLookupType.WATCH_OBJECT.value,
                value=f"{watch_object.watch_list_id}:{watch_object.id}",
            )
            rows.append(row)
        return rows

    def create_row_set_for_htm_levels(self, location: SkyCoord, type_: AlertLookupType):
        """Creates a row_set able to search a type of objects near to a location"""
        row_set = RowSet()
        for level in LOOKUP_HTM_LEVEL:
            htm = spatial.get_htm_id(location, level)
            row_set.add_row_range_with_prefix(f"{htm}#{type_.value}#")
        return row_set


def encode_any(
    value: Union[str, float, int, datetime, Decimal, bool], data_type: str
) -> bytes:
    """Encodes a value into bytes for Bigtable
    The stored value can be bytes or an integer according to Google Data API docs
    https://googleapis.dev/python/bigtable/latest/row.html#google.cloud.bigtable.row.ConditionalRow.set_cell
    We return the first item since unpack returns a tuple like (value, )
    Timestamp notes:
        Encode:
            Transform Python datetime (which has resolution of microseconds 0.000001)
            back into a timestamp integer that is in milliseconds 0.001000 represented in bytes
            Returns 64-bit signed integer encoded epoch in milliseconds converted to a Python datetime
        Decode:
            The input is equivalent to a timestamp datatype and java.util.date new Date(); Date.getTime();
            This could cause a loss of precision from microseconds down to milliseconds
    """
    if data_type == "bool":
        return struct.pack(">?", value)
    if data_type == "str" and isinstance(value, str):
        return value.encode()
    if data_type == "float" and isinstance(value, float):
        return struct.pack(">f", value)
    if data_type == "double" and isinstance(value, float):
        return struct.pack(">d", value)
    if data_type == "tinyint" and isinstance(value, int):
        return struct.pack(">b", value)
    if data_type == "smallint" and isinstance(value, int):
        return struct.pack(">h", value)
    if data_type == "int" and isinstance(value, int):
        return struct.pack(">i", value)
    if data_type == "bigint" and isinstance(value, int):
        return struct.pack(">q", value)
    if data_type == "timestamp" and isinstance(value, datetime):
        return struct.pack(
            ">q", int(value.replace(tzinfo=timezone.utc).timestamp() * 1000)
        )
    if data_type == "decimal" and isinstance(value, Decimal):
        return str(value).encode()
    raise ValueError(
        f"value is type {type(value).__name__} and data_type is {data_type} ; expected value to be"
        " bool, str, float, double, tinyint, smallint, int, bigint, timestamp, datetime or decimal"
    )


def decode_any(
    value: bytes, data_type: str
) -> Union[str, float, int, datetime, Decimal, bool]:
    """Decode bytes using the specified data type, because
    we can't know what is stored in bytes.
    """
    if data_type == "bool":
        return struct.unpack(">?", value)[0]
    if data_type == "str":
        return value.decode()
    if data_type == "float":
        return struct.unpack(">f", value)[0]
    if data_type == "double":
        return struct.unpack(">d", value)[0]
    if data_type == "tinyint":
        return struct.unpack(">b", value)[0]
    if data_type == "smallint":
        return struct.unpack(">h", value)[0]
    if data_type == "int":
        return struct.unpack(">i", value)[0]
    if data_type == "bigint":
        return struct.unpack(">q", value)[0]
    if data_type == "timestamp":
        return datetime.fromtimestamp(
            struct.unpack(">q", value)[0] / 1000, tz=timezone.utc
        ).replace(tzinfo=None)
    if data_type == "decimal":
        return Decimal(value.decode())
    raise ValueError(
        f"Cannot decode {data_type}; expected value to be"
        " bool, str, float, double, tinyint, smallint, int, bigint, timestamp or decimal"
    )
