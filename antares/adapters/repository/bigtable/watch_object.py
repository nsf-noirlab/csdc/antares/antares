"""Module to store watch objects in BigTable

This module includes the add and list methods to interact with watch objects.
TODO:
    * Handle version_id
        # row.set_cell(self.watch_object_cf, "v", watch_object.version_id) doesn't exist
"""

import logging
from collections.abc import Iterable
from uuid import UUID

from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter
from google.cloud.bigtable.row_set import RowSet

from antares.adapters.repository.base import AbstractWatchObjectRepository
from antares.adapters.repository.bigtable.schemas import WatchObjectColumns
from antares.adapters.repository.bigtable.utils import (
    AlertLookupMethods,
    AlertLookupType,
    create_prefix_row_set,
    decode_any,
    encode_any,
    get_optimal_htm_level,
)
from antares.domain.models import WatchObject


class BigtableWatchObjectRepository(AbstractWatchObjectRepository, AlertLookupMethods):
    """Concrete implementation of the watch object repository backed by BigTable.
    Attributes:
        watch_object_table: Table that stores all the information of the watch objects
        watch_object_cf: Column family for watch objects in the watch object table
        alert_lut: Table used to store watch objects along with their htm positions
        alert_lut_cf : Column family of the alert look up table

    We have two tables in the backing BigTable data store:

        1. The table that stores watch objects, indexed by a compound key (wl_id, wo_id)
           where `wl_id` is the object's associated watch list's ID and wo_id is the
           ID of that object. We don't enforce global uniqueness on `wo_id`s.

        2. A lookup table that maps HTM ids to the (compound) partition key that can be
           used to retrieve matches from table (1). We share this table with the locus
           lookup table due to performance, because it saves having to search in two
           tables. Rows in the lookup table have the form `(htm, type, value)`.
           Watch objects can be indexed at any of the HTM levels defined in the
           WATCH_OBJECT_LOOKUP_HTM_LEVEL member variable so reads MUST check a
           coordinate at all levels. Writes MUST determine the optimal level for storing
           a lookup entry. Entries in the lookup table corresponding to watch objects
           will have a `type` of 2 (use the AlertLookupType.WATCH_OBJECT enum member)
           and for `value` a colon-separated string "<wl_id>:<wo_id>".

    Note:
        * UUID are stored as their str representation because when we are
            concatenating bytes and sometimes the bytes can have '#' which is
            the value we use to split the concatenation.
    """

    def __init__(
        self,
        instance_: BigtableInstance,
        watch_object_table_id: str,
        watch_object_cf: str,
        alert_lut_id: str,
        alert_lut_cf: str,
    ):
        super().__init__(
            alert_lut=instance_.table(alert_lut_id), alert_lut_cf=alert_lut_cf
        )
        self.watch_object_table = instance_.table(watch_object_table_id)
        self.watch_object_cf = watch_object_cf
        logging.info("BigtableWatchObjectRepository created successfully")

    def create_watch_object_from_row(
        self, watch_object_row: PartialRowData
    ) -> WatchObject:
        """Method to deserialize bytes from a bigtable row object to
        create a WatchObject object
        """
        watch_list_id, id_ = watch_object_row.row_key.decode().split("#")
        ra = decode_any(
            watch_object_row.cell_value(self.watch_object_cf, WatchObjectColumns.RA),
            "double",
        )
        dec = decode_any(
            watch_object_row.cell_value(self.watch_object_cf, WatchObjectColumns.DEC),
            "double",
        )
        radius = decode_any(
            watch_object_row.cell_value(
                self.watch_object_cf, WatchObjectColumns.RADIUS
            ),
            "double",
        )
        if WatchObjectColumns.NAME in watch_object_row.cells[self.watch_object_cf]:
            name = watch_object_row.cell_value(
                self.watch_object_cf, WatchObjectColumns.NAME
            ).decode()
        else:
            name = ""
        return WatchObject(
            watch_list_id=UUID(hex=watch_list_id),
            name=name,
            location=SkyCoord(ra=ra, dec=dec, unit=u.deg),
            radius=Angle(radius, unit=u.deg),
            id=UUID(id_),
        )

    def create_watch_object_row(self, watch_object: WatchObject) -> DirectRow:
        """Method to create a bigtable row object which stores data as bytes"""
        row = self.watch_object_table.direct_row(
            WatchObjectColumns.create_row_key(
                str(watch_object.watch_list_id), str(watch_object.id)
            )
        )
        row.set_cell(
            self.watch_object_cf,
            WatchObjectColumns.NAME,
            watch_object.name,
        )
        row.set_cell(
            self.watch_object_cf,
            WatchObjectColumns.RA,
            encode_any(watch_object.location.ra.deg, "double"),
        )
        row.set_cell(
            self.watch_object_cf,
            WatchObjectColumns.DEC,
            encode_any(watch_object.location.dec.deg, "double"),
        )
        htm_level = get_optimal_htm_level(watch_object.radius)
        row.set_cell(self.watch_object_cf, WatchObjectColumns.HTM_LEVEL, htm_level)
        row.set_cell(
            self.watch_object_cf,
            WatchObjectColumns.RADIUS,
            encode_any(watch_object.radius.deg, "double"),
        )
        return row

    def add(self, watch_object: WatchObject):
        """Save a new watch object to the database.

        This is a two steps process:

        1. Insert entries into the watch object table (watch_object).

        2. Determine the optimal HTM level for storing an entry in the lookup table
           and calculate the HTM trixels that the watch object intersects. Insert
           entries into the HTM lookup table (alert_lut).
        """
        watch_object_row = self.create_watch_object_row(watch_object)
        watch_object_row.commit()
        rows_to_write_in_alert_lut = self.create_alert_lut_rows_for_watch_object(
            watch_object
        )
        responses = self.alert_lut.mutate_rows(rows_to_write_in_alert_lut)
        errors = []
        for response in responses:
            if response.code != 0:
                errors.append(response.message)
        if errors:
            logging.error(
                "%d errors while writing watch_objects in alert_lut", len(errors)
            )
            logging.error(errors)

    def list_by_location(self, location: SkyCoord) -> Iterable[WatchObject]:
        """Find all watch objects that overlap `location`.

        This is a two-step process:

        1. Map the location to its HTM ID at various levels and query the lookup table
           for all matching (watch_list_id, watch_object_id) tuples.

        2. Lookup results in the `watch_object` table.
        """
        alert_lut_row_set = self.create_row_set_for_htm_levels(
            location, AlertLookupType.WATCH_OBJECT
        )
        if alert_lut_row_set.row_keys == [] and alert_lut_row_set.row_ranges == []:
            return
            yield
        watch_object_row_set = RowSet()
        for row in self.alert_lut.read_rows(
            filter_=CellsColumnLimitFilter(1), row_set=alert_lut_row_set
        ):
            htm, _, value = row.row_key.decode().split("#")
            watch_list_id, watch_object_id = value.split(":")
            watch_list_id = UUID(watch_list_id)
            watch_object_id = UUID(watch_object_id)
            watch_object_row_set.add_row_key(f"{watch_list_id}#{watch_object_id}")
        if (
            watch_object_row_set.row_keys == []
            and watch_object_row_set.row_ranges == []
        ):
            return
            yield
        for row in self.watch_object_table.read_rows(
            filter_=CellsColumnLimitFilter(1), row_set=watch_object_row_set
        ):
            watch_object = self.create_watch_object_from_row(row)
            if location.separation(watch_object.location) < watch_object.radius:
                yield watch_object

    def list_by_watch_list_id(self, watch_list_id: UUID) -> Iterable[WatchObject]:
        """List all the watch_objects searching by a watch_list_id"""
        row_set = create_prefix_row_set(str(watch_list_id))
        print(row_set)
        if row_set.row_keys == [] and row_set.row_ranges == []:
            return
            yield
        for row in self.watch_object_table.read_rows(
            filter_=CellsColumnLimitFilter(1), row_set=row_set
        ):
            yield self.create_watch_object_from_row(row)
