"""
Concrete implementation of the ANTARES repository interface using BigTable as a backing
data store.

The first section of this source code file contains table schema (`*Schema` classes) and
type definitions (`*Row` classes that inherit from `TypedDict`) that describe the
structure of table rows returned by queries.

The second section contains concrete implementations of the abstract repositories
defined in the sibling, .base, module.


Ideas:
    - Move configuration to this file to remove the instances creation
    - Row_key comments can be removed from the Column enums
"""


import uuid
from enum import Enum
from typing import Union

from google.cloud.bigtable.column_family import MaxVersionsGCRule


class MissingTableForSchema(Exception):
    pass


# ======================================================================================
# Schema
# ======================================================================================


Schemas = dict[str, dict[str, Union[str, int]]]


class BaseSchema:
    table_id: str = ""

    def __init__(self, schemas: Schemas):
        if self.table_id in schemas:
            self.__table_name__ = schemas[self.table_id]["table_name"]
            self.column_families = {
                schemas[self.table_id]["column_family_id"]: MaxVersionsGCRule(
                    schemas[self.table_id]["max_versions_rule"]
                )
            }
        else:
            raise MissingTableForSchema(f"{self.table_id} not found in config file")


# --------------------------------------------------------------------------------------
# Loci and Alerts
# --------------------------------------------------------------------------------------


class BigtableAlertSchema(BaseSchema):
    table_id = "alert"


class BigtableAlertLookupSchema(BaseSchema):
    """
    Table used to support spatial lookups from HTM IDs to BOTH locus IDs and watch
    object IDs.

    Locus IDs are clustered under `type` = 1, watch object IDs are clustered under
    `type` = 2. These values are also encoded in the `BigtableAlertLookupType`
    enumeration and using them by, e.g., `BigtableAlertLookupType.LOCUS.value` is the
    preferred approach.

    The `value` column contains the locus ID, in the case of loci (where `type` = 1) or
    a string of the form "<watch_list_id>:<watch_object_id>", in the case of watch
    objects (where `type` = 2).
    """

    table_id = "alert_lut"


class BigtableAlertLookupType(Enum):
    """
    Possible values that the `type` column in the table described by
    `BigtableAlertLookupSchema` can take.
    """

    LOCUS = 1
    WATCH_OBJECT = 2


class BigtableLocusByAlertIdSchema(BaseSchema):
    """
    Table used to map alert IDs to locus IDs.
    """

    table_id = "locus_by_alert_id"


class BigtableLocusSchema(BaseSchema):
    """
    Table containing loci AND alerts.
    """

    table_id = "locus"


# --------------------------------------------------------------------------------------
# Watch Lists
# --------------------------------------------------------------------------------------


class BigtableWatchListSchema(BaseSchema):
    """
    Table containing watch lists.
    """

    table_id = "watch_list"


class BigtableWatchObjectSchema(BaseSchema):
    """
    Table containing watch objects.
    """

    table_id = "watch_object"


# --------------------------------------------------------------------------------------
# Catalog Cross-matching
# --------------------------------------------------------------------------------------


"""
class BigtableCatalogSchema:
    table_id = "catalog"
    There are multiple tables, one for each catalog and they are created dynamically
"""


class BigtableCatalogLookupSchema(BaseSchema):
    """
    Table used as a lookup index to map HTM IDs to the partition keys of catalog
    objects.
    """

    table_id = "catalog_lut"


# --------------------------------------------------------------------------------------
# Storage
# --------------------------------------------------------------------------------------


# TODO: Implement filter file fetching from this table
class BigtableStorageSchema(BaseSchema):
    """
    Table containing arbitrary key/value data.
    """

    table_id = "storage"


# --------------------------------------------------------------------------------------
# Schemas columns
# --------------------------------------------------------------------------------------


class LocusColumns:
    @staticmethod
    def create_row_key(locus_id: str) -> bytes:
        return locus_id.encode()

    CATALOGS = b"c"
    INSTANCE_CREATED_DATETIME = b"i"
    DEC = b"d"
    LIGHTCURVE = b"l"
    PROPS = b"p"
    TAGS = b"t"
    RA = b"r"
    UPDATED_DATETIME = b"u"


class AlertColumns:
    @staticmethod
    def create_row_key(locus_id: str, alert_mjd: float, alert_id: str) -> bytes:
        return f"{locus_id}#{alert_mjd}#{alert_id}".encode()

    INSTANCE_CREATED_DATETIME = b"i"
    PROPERTIES = b"p"


class AlertLutColumns:
    @staticmethod
    def create_row_key(htm: int, type_value: int, value: str) -> bytes:
        return f"{htm}#{type_value}#{value}".encode()

    DATA = b"d"


class StorageColumns:
    @staticmethod
    def create_row_key(blob_key: str) -> bytes:
        return blob_key.encode()

    DATA = b"d"


class LocusByAlertIdColumns:
    @staticmethod
    def create_row_key(alert_id: str) -> bytes:
        return alert_id.encode()

    LOCUS_ID = b"l"


class WatchListColumns:
    @staticmethod
    def create_row_key(watch_list_id: uuid.UUID) -> bytes:
        return str(watch_list_id).encode()

    CREATED_DATETIME = b"c"
    DESCRIPTION = b"d"
    ENABLED = b"e"
    NAME = b"n"
    OWNER_ID = b"o"
    SLACK_CHANNEL = b"s"
    # VERSION_ID = b"v"


class WatchObjectColumns:
    @staticmethod
    def create_row_key(watch_list_id: str, object_id: str) -> bytes:
        return f"{watch_list_id}#{object_id}".encode()

    RA = b"r"
    DEC = b"d"
    HTM_LEVEL = b"h"
    RADIUS = b"R"
    NAME = b"n"
    # VERSION_ID = b"v"


class CatalogColumns:
    """
    Catalog columns are loaded from a YAML in BigtableCatalogObjectRepository
    because we need to know the data type when decoding and each
    catalog has different amount of columns

    The double underscore is to avoid collision of name with the column names of the
    properties of a catalog, since each property is going to be a column.
    """

    @staticmethod
    def create_row_key(catalog_object_id: str) -> bytes:
        return catalog_object_id.encode()


class CatalogLutColumns:
    @staticmethod
    def create_row_key(htm_level: int, value: str) -> bytes:
        return f"{htm_level}#{value}".encode()

    DATA = b"d"


# --------------------------------------------------------------------------------------
# Exports
# --------------------------------------------------------------------------------------

SCHEMAS = [
    BigtableAlertSchema,
    BigtableAlertLookupSchema,
    BigtableLocusSchema,
    BigtableStorageSchema,
    BigtableCatalogLookupSchema,
    BigtableLocusByAlertIdSchema,
    BigtableWatchListSchema,
    BigtableWatchObjectSchema,
]
