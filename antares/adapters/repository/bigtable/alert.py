"""Module to add and get alerts from BigTable

This module adds the BigtableAlertRepository class and the MissingAlertError exception.

TODO:
    * Handle duplicate alerts based on the survey
"""

import logging
from collections.abc import Iterable
from datetime import datetime
from typing import Optional, cast

import bson
from astropy import units as u
from astropy.coordinates import SkyCoord
from google.cloud.bigtable.instance import Instance as BigtableInstance
from google.cloud.bigtable.row import DirectRow, PartialRowData
from google.cloud.bigtable.row_filters import CellsColumnLimitFilter
from google.cloud.bigtable.row_set import RowSet

from antares.adapters.repository.base import AbstractAlertRepository
from antares.adapters.repository.bigtable.schemas import (
    AlertColumns,
    LocusByAlertIdColumns,
)
from antares.adapters.repository.bigtable.utils import (
    create_prefix_row_set,
    decode_any,
    encode_any,
)
from antares.domain.models import Alert, AlertNormalizedProperties, Survey


class MissingAlertError(Exception):
    """Error to show when there's an alert_id but there's no record of it"""

    pass


class BigtableAlertRepository(AbstractAlertRepository):
    """Class that works as a mediator between the system and bigtable for alerts

    This repository reads and writes in the `alert` and `locus_by_alert_id` tables.

    Attributes:
        alert_table: Table that stores all the information of the alerts
        alert_cf: Column family for the alert in the alert table
        locus_by_alert_id_table: Table used to retrieve locus_id based on alert_id
        locus_by_alert_id_cf :
            Column family of the lookup in the locus_by_alert_id table
    """

    def __init__(
        self,
        instance_: BigtableInstance,
        alert_table_id: str,
        alert_cf: str,
        locus_by_alert_id_table_id: str,
        locus_by_alert_id_cf: str,
    ):
        self.alert_table = instance_.table(alert_table_id)
        self.alert_cf = alert_cf
        self.locus_by_alert_id_table = instance_.table(locus_by_alert_id_table_id)
        self.locus_by_alert_id_cf = locus_by_alert_id_cf
        logging.info("BigtableAlertRepository created successfully")

    def get(self, alert_id: str) -> Optional[Alert]:
        """Method to retrieve an alert object given its id

        The process starts by searching the unique locus associated with an alert,
        then searches all alerts associated with that locus, and finally
        returns an alert object if the data exists.
        """
        if locus_id := self._get_locus_id_by_alert_id(alert_id):
            row_set = create_prefix_row_set(locus_id)
            if row_set.row_keys == [] and row_set.row_ranges == []:
                return None
            rows = self.alert_table.read_rows(
                filter_=CellsColumnLimitFilter(1), row_set=row_set
            )
            for alert_row in rows:
                _, _, retrieved_alert_id = alert_row.row_key.decode().split("#")
                if retrieved_alert_id == alert_id:
                    return self.create_alert_from_row(alert_row)
            raise MissingAlertError(
                f"There's a locus_id_by_alert_id: {alert_id}"
                " but the alert doesn't exist"
            )
        else:
            return None

    def list_by_locus_id(self, locus_id: str) -> list[Alert]:
        """Method to retrieve all alert objects associated with a locus_id"""
        row_set = create_prefix_row_set(locus_id)
        if row_set.row_keys == [] and row_set.row_ranges == []:
            return []
        return [
            self.create_alert_from_row(row)
            for row in self.alert_table.read_rows(
                filter_=CellsColumnLimitFilter(1), row_set=row_set
            )
        ]

    def create_locus_by_alert_id_row(self, alert_id: str, locus_id: str) -> DirectRow:
        """Method to create a bigtable row object which stores data as bytes"""
        row = self.locus_by_alert_id_table.direct_row(
            LocusByAlertIdColumns.create_row_key(alert_id)
        )
        row.set_cell(
            self.locus_by_alert_id_cf, LocusByAlertIdColumns.LOCUS_ID, locus_id
        )
        return row

    def create_alert_from_row(self, alert_row: PartialRowData) -> Alert:
        """Method to deserialize bytes from a bigtable row to create an Alert object

        An `Alert` instance has both `.properties` and `.normalized_properties` fields.
        The former is an extensible collection for user- and survey-defined properties
        whereas the latter is a set of tightly controlled ANTARES-defined properties.

        We have a single column, `properties`, in the Bigtable table that stores both
        of these. Combining these two fields into one for serialization is easy.
        Deserialization is more complicated--each of the normalized properties has a
        known prefix, "ant_*", and so in the `_load_alert` deserialization routine we
        split them checking for that prefix.
        """
        locus_id, mjd, alert_id = alert_row.row_key.decode().split("#")
        mjd = float(mjd)
        properties = bson.loads(
            alert_row.cell_value(self.alert_cf, AlertColumns.PROPERTIES) or {}
        )
        location = None
        if (
            properties.get("ant_ra") is not None
            and properties.get("ant_dec") is not None
        ):
            location = SkyCoord(
                ra=properties["ant_ra"], dec=properties["ant_dec"], unit=u.deg
            )
        grav_wave_events = properties.pop("ant_grav_wave_events", [])
        normalized_properties = cast(
            AlertNormalizedProperties,
            {key: value for key, value in properties.items() if key.startswith("ant_")},
        )
        for key in normalized_properties:
            del properties[key]
        processed_at = decode_any(
            alert_row.cell_value(self.alert_cf, AlertColumns.INSTANCE_CREATED_DATETIME),
            "timestamp",
        )
        if not isinstance(processed_at, datetime):
            raise ValueError(
                "Expected 'processed_at' to be datetime. Argument given: processed_at"
            )
        return Alert(
            id=alert_id,
            location=location,
            mjd=mjd,
            survey=Survey.from_int(normalized_properties["ant_survey"]),
            normalized_properties=normalized_properties,
            grav_wave_events=grav_wave_events,
            created_at=processed_at,
            properties=properties,
        )

    def create_alert_row(self, alert: Alert, locus_id: str) -> DirectRow:
        """Method to create a bigtable row object which stores data as bytes"""
        row = self.alert_table.direct_row(
            AlertColumns.create_row_key(locus_id, alert.mjd, alert.id)
        )
        row.set_cell(
            self.alert_cf,
            AlertColumns.INSTANCE_CREATED_DATETIME,
            encode_any(alert.created_at, "timestamp"),
        )
        properties = {
            "ant_grav_wave_events": alert.grav_wave_events,
            **alert.properties,
            **alert.normalized_properties,
        }
        row.set_cell(self.alert_cf, AlertColumns.PROPERTIES, bson.dumps(properties))
        return row

    def add(self, alert: Alert, locus_id: str):
        """Method to add alert rows to alert and locus_by_alert_id tables

        From Cassandra:
            TODO
                * We should also be adding a lookup entry to locus_by_day. (This table was never used)
        """
        row = self.create_locus_by_alert_id_row(alert.id, locus_id)
        row.commit()
        row = self.create_alert_row(alert, locus_id)
        row.commit()

    def add_many_if_not_exists(self, alerts_with_locus_id: Iterable[tuple[Alert, str]]):
        """Same as `BigtableAlertRepository.add` but as a batch
        This method requires a list and not an iterable because it passes over the
        list two times and an iterable can't do that.
        """
        if not isinstance(alerts_with_locus_id, list):
            alerts_with_locus_id = list(alerts_with_locus_id)
        rows_to_write_in_alert_table = []
        rows_to_write_in_locus_by_alert_id_table = []
        row_set_to_search = RowSet()
        for alert, locus_id in alerts_with_locus_id:
            row_set_to_search.add_row_key(
                AlertColumns.create_row_key(locus_id, alert.mjd, alert.id)
            )
        existing_alerts = []
        if row_set_to_search.row_keys:
            existing_alerts = [
                row.row_key
                for row in self.alert_table.read_rows(
                    row_set=row_set_to_search, filter_=CellsColumnLimitFilter(1)
                )
            ]
        for alert, locus_id in alerts_with_locus_id:
            if (
                AlertColumns.create_row_key(locus_id, alert.mjd, alert.id)
                in existing_alerts
            ):
                continue
            rows_to_write_in_alert_table.append(self.create_alert_row(alert, locus_id))
            rows_to_write_in_locus_by_alert_id_table.append(
                self.create_locus_by_alert_id_row(alert.id, locus_id)
            )
        response_alert = self.alert_table.mutate_rows(rows_to_write_in_alert_table)
        response_locus_by_alert_id = self.locus_by_alert_id_table.mutate_rows(
            rows_to_write_in_locus_by_alert_id_table
        )
        errors = []
        for status_alert, status_locus_by_alert_id in zip(
            response_alert, response_locus_by_alert_id
        ):
            if status_alert.code != 0:
                errors.append(status_alert.message)
            if status_locus_by_alert_id.code != 0:
                errors.append(status_alert.message)
        if errors:
            logging.error("%d errors while writing alerts", len(errors))

    def _get_locus_id_by_alert_id(self, alert_id: str) -> Optional[str]:
        """Method to retrieve the locus id of an alert id"""
        if row := self.locus_by_alert_id_table.read_row(
            alert_id, filter_=CellsColumnLimitFilter(1)
        ):
            return row.cell_value(
                self.locus_by_alert_id_cf, LocusByAlertIdColumns.LOCUS_ID
            ).decode()
        else:
            return None
