"""
Abstract base classes for ANTARES repositories.

We use a "Persistence-Oriented" repository pattern, and here is an excerpt from
"Implementing Domain-Driven Design" where this pattern is defined:

> For times when a collection-oriented style doesn’t work, you will need to employ a
> persistence-oriented, save-based Repository. This will be the case when your
> persistence mechanism doesn’t implicitly or explicitly detect and track object
> changes. This happens to be the case when using an in-memory Data Fabric (4), or by
> any other name a NoSQL key-value data store. Every time you create a new Aggregate
> instance or change a preexisting one, you will have to put it into the data store by
> using save() or a save-like Repository method.

We currently have a hybrid SQL/Cassandra database deployment and plans to move to NoSQL,
entirely, in the future. We settled on this persistence-oriented pattern after coming up
against difficulties trying to implement a unit of work that could manage both types of
databases--accesses to each are commingled throughout our application. We incur a bit of
cost for the extra session management with our SQL repositories and we lose some of the
transactional features that SQL and sqlalchemy provide but it makes our code much
simpler to reason about.

TODO: All `add` methods should raise KeyViolationException or be rewritten
as `add_if_not_exists`
"""

import abc
import uuid
from collections.abc import Iterable
from datetime import datetime
from typing import TYPE_CHECKING, Any, List, Literal, Optional, TypedDict

from astropy.coordinates import Angle, SkyCoord

if TYPE_CHECKING:  # Avoiding circular import
    from antares.adapters.notifications import AbstractInternalNotificationService

from antares.domain.models import (
    Alert,
    AlertThumbnail,
    Announcement,
    Blob,
    Catalog,
    CatalogObject,
    Filter,
    FilterExecutable,
    FilterRevision,
    GravWaveNotice,
    JwtRecord,
    Locus,
    LocusAnnotation,
    User,
    WatchList,
    WatchObject,
)
from antares.exceptions import KeyViolationException
from antares.observability.metrics.base import AbstractMetrics
from antares.observability.tracing.base import AbstractTracer


class ListQueryFilter(TypedDict):
    op: Literal["eq", "neq", "gt", "gte", "lt", "lte"]
    field: str
    value: Any


ListQueryFilters = list[ListQueryFilter]


class AbstractLocusRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, id_: str) -> Optional[Locus]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_cone_search(self, location: SkyCoord, radius: Angle) -> Iterable[Locus]:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, locus: Locus) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, locus: Locus) -> None:
        raise NotImplementedError


class AbstractLocusAnnotationRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, id_: int) -> Optional[LocusAnnotation]:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, locus_annotation: LocusAnnotation) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, locus_annotation: LocusAnnotation) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_owner_id(self, owner_id: uuid.UUID) -> Iterable[LocusAnnotation]:
        raise NotImplementedError


class AbstractFilterRevisionRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, id_: int) -> Optional[FilterRevision]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_filter_id(self, filter_id: int) -> Iterable[FilterRevision]:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, filter_revision: FilterRevision) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, filter_revision: FilterRevision) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_filter_executable(
        self, filter_: Filter, tracer: AbstractTracer
    ) -> tuple[Filter, FilterExecutable]:
        raise NotImplementedError


class AbstractFilterRepository(abc.ABC):
    @abc.abstractmethod
    def list(self) -> Iterable[Filter]:
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, id_: int) -> Optional[Filter]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_filter_executables(
        self,
        filter_revision_repository: AbstractFilterRevisionRepository,
        internal_notification_service: "AbstractInternalNotificationService",
        tracer: AbstractTracer,
    ) -> List[tuple[Filter, FilterExecutable]]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_enabled_public_filters(
        self, limit: Optional[int] = None, offset: Optional[int] = None
    ) -> Iterable[Filter]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_owner_id(self, owner_id: uuid.UUID) -> Iterable[Filter]:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, filter_: Filter) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, filter_: Filter) -> None:
        raise NotImplementedError


class AbstractCatalogObjectRepository(abc.ABC):
    def __init__(self, catalogs: list[Catalog]):
        self._catalogs = catalogs

    def get_catalog_by_catalog_id(self, id_: str) -> Optional[Catalog]:
        try:
            return next(catalog for catalog in self._catalogs if catalog.id == id_)
        except StopIteration:
            return None

    def list_catalogs(self) -> Iterable[Catalog]:
        return self._catalogs

    @abc.abstractmethod
    def list_by_location(
        self, location: SkyCoord, metrics: AbstractMetrics
    ) -> Iterable[CatalogObject]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_catalog_id(self, catalog_id: str) -> Iterable[CatalogObject]:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, catalog_object: CatalogObject) -> None:
        raise NotImplementedError


class AbstractWatchListRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, watch_list: WatchList) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, id_: uuid.UUID) -> Optional[WatchList]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_owner_id(self, owner_id: uuid.UUID) -> Iterable[WatchList]:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, id_: uuid.UUID):
        raise NotImplementedError


class AbstractWatchObjectRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, watch_object: WatchObject) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_location(self, location: SkyCoord) -> Iterable[WatchObject]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_watch_list_id(self, watch_list_id: uuid.UUID) -> Iterable[WatchObject]:
        raise NotImplementedError


class AbstractAlertRepository(abc.ABC):
    """
    Repository for `Alert` objects.

    This repository interface is a bit convoluted because of how we historically modeled
    alerts and their relationship to loci in Cassandra. A few methods on this interface:

        * `add`
        * `add_many`
        * `list_by_locus_id`

    take a parameter `locus_id` (in addition, in the `add_*` methods, to the `Alert` the
    caller wants to persist). In our current design of the system, an `Alert` must be
    associated to a single `Locus`. We don't store this relationship explicitly (e.g. as
    a member like `alert.locus_id`) and so we pass the `locus_id` as a
    persistence/storage concern. Concrete implementations of this repository can handle
    persisting alerts any way that they'd like to so long as the semantics of all
    operations are correct.

    """

    @abc.abstractmethod
    def add(self, alert: Alert, locus_id: str) -> None:
        """
        Save an alert in persistent storage. The `locus_id` parameter is necessary as an
        implementation detail of the BigTable alert database and so we have to expose
        it on this abstract interface. Concrete implementations of this repository can
        handle persisting alerts any way that they'd like to

        Parameters
        ----------
        alert : Alert
            The alert to persist.
        locus_id : str
            The ID of the locus that this alert should be associated with.

        Returns
        -------
        None

        Raises
        ------
        KeyViolationException
            If an alert with `alert.id` already exists in the persistent store.

        """
        raise NotImplementedError

    def add_many_if_not_exists(self, alerts: Iterable[tuple[Alert, str]]) -> None:
        """
        Save many alerts in persistent storage. This interface provides a naive
        implementation of this method but a concrete repository might choose to
        re-implement in a more performant manner. If any alerts already exist, this
        method should ignore them and continue to add others.

        TODO: This method might become obsolete if we were to have an async def add()

        Parameters
        ----------
        alerts : Iterable[tuple[Alert, str]]
            This is an iterable of (alert: Alert, locus_id: str) tuples that the
            repository should store.

        Returns
        -------
        None

        """
        for alert, locus_id in alerts:
            try:
                self.add(alert, locus_id)
            except KeyViolationException:
                continue

    @abc.abstractmethod
    def get(self, id_: str) -> Optional[Alert]:
        """
        Return the alert with the ID `alert_id` or `None` if it does not exist.

        Parameters
        ----------
        alert_id : str

        Returns
        -------
        Optional[Alert]

        """
        raise NotImplementedError

    @abc.abstractmethod
    def list_by_locus_id(self, locus_id: str) -> Iterable[Alert]:
        """
        Return an iterable of all alerts associated with the locus with ID `locus_id`.

        Parameters
        ----------
        locus_id : str

        Returns
        -------
        Iterable[Alert]

        """
        raise NotImplementedError


class AbstractUserRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, user: User) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, user: User) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def list(
        self, filtered_ids: Optional[Iterable[uuid.UUID]] = None
    ) -> Iterable[User]:
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, id_: uuid.UUID) -> Optional[User]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_by_username(self, username: str) -> Optional[User]:
        raise NotImplementedError


class AbstractJwtBlocklistRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, jwt: JwtRecord) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_by_jti(self, jti: str) -> Optional[JwtRecord]:
        raise NotImplementedError


class AbstractBlobRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, id_: str) -> Optional[Blob]:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, blob: Blob) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, blob: Blob) -> None:
        raise NotImplementedError


class AbstractAnnouncementRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, announcement: Announcement) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, announcement: Announcement) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def list(
        self, query_filters: Optional[ListQueryFilters] = None
    ) -> Iterable[Announcement]:
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, id_: int) -> Optional[Announcement]:
        raise NotImplementedError


class AbstractAlertThumbnailRepository(abc.ABC):
    @abc.abstractmethod
    def list_by_alert_id(self, alert_id: str) -> Iterable[AlertThumbnail]:
        raise NotImplementedError


class AbstractGravWaveRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, id_: int) -> Optional[GravWaveNotice]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_id(self, gracedb_id: str, notice_datetime: datetime) -> Optional[int]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_by_gracedb_id(self, gracedb_id: str) -> Optional[GravWaveNotice]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_current_notices(self) -> list[GravWaveNotice]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_active_notice_ids(self) -> set[int]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_by_gracedb_ids(
        self, gracedb_ids: list[str]
    ) -> Iterable[GravWaveNotice]:
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, notice: GravWaveNotice) -> None:
        raise NotImplementedError
