"""
Notification Services, all unused except for Slack API
This module could be simplified.
"""

import abc
import logging
import smtplib
from typing import Any, Dict, Optional

import requests

from antares.adapters.repository import AbstractUserRepository
from antares.domain.models import Filter, FilterRevision, User


class AbstractNotificationService(abc.ABC):
    """ABC for notification services"""

    @abc.abstractmethod
    def send(self, destination: str, message: Any):
        raise NotImplementedError

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class AbstractInternalNotificationService(AbstractNotificationService):
    @abc.abstractmethod
    def notify_after_filter_revision_created(
        self,
        filter_: Filter,
        filter_revision: FilterRevision,
    ):
        raise NotImplementedError

    @abc.abstractmethod
    def notify_after_filter_revision_transitioned(
        self,
        filter_: Filter,
        filter_revision: FilterRevision,
        custom_message: str = "",
    ):
        raise NotImplementedError


class EmailNotificationService(AbstractNotificationService):
    # pylint: disable=too-many-arguments
    """UNUSED implementation for emails notifications"""

    def __init__(
        self,
        smtp_host: str,
        smtp_port: int,
        smtp_sender: str,
        smtp_username: Optional[str] = None,
        smtp_password: Optional[str] = None,
    ):
        logging.info(
            "Configuring email notification service to %s:%d", smtp_host, smtp_port
        )
        self.smtp = smtplib.SMTP(host=smtp_host, port=smtp_port)
        self.smtp_sender = smtp_sender
        self.smtp_username = smtp_username
        self.smtp_password = smtp_password

    def send(self, destination: str, message: Any):
        message = f"Subject: ANTARES Notification\n{message}"
        self.smtp.sendmail(self.smtp_sender, destination, message)

    def __enter__(self):
        if self.smtp_username:
            self.smtp.login(self.smtp_username, self.smtp_password)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.smtp.quit()


class SlackApiNotificationService(AbstractNotificationService):
    # pylint: disable=too-few-public-methods
    """implementation for slack notifications"""

    def __init__(self, api_token: str):
        self.api_token = api_token

    def send(self, destination: str, message: Any):
        url = "https://slack.com/api/chat.postMessage"
        data = {
            "token": self.api_token,
            "text": message,
            "mrkdwn": "true",
            "channel": destination,
            "username": "ANTARES",
            "icon_emoji": ":telescope:",
            "unfurl_links": "false",
            "unfurl_media": "false",
        }
        requests.post(url, data)


class InternalSlackApiNotificationService(
    AbstractInternalNotificationService,
    SlackApiNotificationService,
):
    def __init__(
        self,
        api_token: str,
        environment: str,
        frontend_base_url: str,
        notification_channels: Dict[str, str],
        user_repository: AbstractUserRepository,
    ):
        super().__init__(api_token=api_token)
        self._environment = environment
        self._frontend_base_url = frontend_base_url
        self._notification_channels = notification_channels
        self._user_repository = user_repository

    def build_filter_revision_notification_message(
        self,
        filter_: Filter,
        filter_revision: FilterRevision,
        is_new_filter_revision: bool,
        user: Optional[User],
        custom_message: str = "",
    ):
        newline = "\n"
        return (
            (
                f"New filter revision submitted to {self._environment}: "
                f"<{self._frontend_base_url}/admin/filters/{filter_.id}|View details>"
                f"{newline * 2}"
                f"Filter ID: {filter_.id}, Revision ID: {filter_revision.id}"
                f"{newline}"
                f'Submitted {f"by {user.name} ({user.email}) " if user else ""}'
                f"at UTC {filter_revision.created_at}"
                f"{newline}"
                f"Comment: {filter_revision.comment}"
            )
            if is_new_filter_revision
            else (
                f"Filter revision transitioned to *{filter_revision.status.name}* "
                f"on {self._environment}: "
                f"<{self._frontend_base_url}/admin/filters/{filter_.id}|View details>"
                f"{newline * 2}"
                f"Filter ID: {filter_.id}, Revision ID: {filter_revision.id}"
                f"{newline}"
                f"Transitioned at UTC {filter_revision.transitioned_at}"
                f"{newline}"
                f"{f'Author: {user.name} ({user.email}){newline}' if user else f''}"
                f"Feedback: {filter_revision.feedback or '(empty)'}"
                f"{f'{newline}Additional info: {custom_message}' if custom_message else f''}"  # noqa: E501
            )
        )

    def notify_filter_revision_activity(
        self,
        filter_: Filter,
        filter_revision: FilterRevision,
        is_new_filter_revision: bool,
        custom_message: str = "",
    ):
        user = self._user_repository.get(filter_.owner_id)
        message = self.build_filter_revision_notification_message(
            filter_,
            filter_revision,
            is_new_filter_revision,
            user,
            custom_message,
        )
        self.send(self._notification_channels["filter_activity"], message)

    def notify_after_filter_revision_transitioned(
        self,
        filter_: Filter,
        filter_revision: FilterRevision,
        custom_message: str = "",
    ) -> None:
        self.notify_filter_revision_activity(
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=False,
            custom_message=custom_message,
        )

    def notify_after_filter_revision_created(
        self,
        filter_: Filter,
        filter_revision: FilterRevision,
    ) -> None:
        self.notify_filter_revision_activity(
            filter_=filter_,
            filter_revision=filter_revision,
            is_new_filter_revision=True,
        )


class SlackWebhookNotificationService(AbstractNotificationService):
    # pylint: disable=too-few-public-methods
    """UNUSED implementation for slack webhooks"""

    def __init__(self, url: str):
        self.url = url

    def send(self, destination: str, message: Any):
        requests.post(self.url, json={"text": message})
