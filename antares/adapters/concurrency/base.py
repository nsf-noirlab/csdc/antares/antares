"""
Abstract concurrency classes (just distributed locks)
"""

import abc
from collections.abc import Callable
from typing import Optional


class AbstractDistributedLock(abc.ABC):
    """
    Synchronization primitive to provide mutual exclusion in a distributed system.

    Notes
    -----
    I'm not sure how we structure usage of this. The typical pattern that you'll see
    in Python is something like:

    ```
    with Lock("lock-name"):
        do_critical_section()
    ```

    Which is equivalent to:

    ```
    lock = Lock("lock-name")
    lock.acquire()
    try:
        do_critical_section()
    finally:
        lock.release()
    ```

    We are defining this abstract interface but different concrete implementations might
    have different constructors (e.g. the Redis-backed lock will need a Redis hostname,
    port and database name).

    We'd like to be able to use this with our dependency injection framework where a
    function like:

    ```
    def some_function(lock_class: type[AbstractDistributedLock]):
        # lock_class injected at runtime
        with lock_class(...):
            pass
    ```

    is unaware of the implementation-specific details necessary to instantiate the Lock.
    What design pattern might we use here? I don't think we can use:

    ```
    def some_function(lock: AbstractDistributedLock):
        # lock injected at runtime
        with lock:
            pass
    ```

    Since the function should be able to specify what the ID of the lock is (i.e. via
    `Lock("lock-name")`). The cleanest approach I can think of is that we use a factory
    function:

    ```
    def concrete_distributed_lock_factory(id_: str) -> AbstractDistributedLock:
        lock = ConcreteDistributedLock(id_, config.CONCRETE_PARAMETER_1, ...)
        return lock

    def some_function(lock_factory: Callable[[str], AbstractDistributedLock]):
        # lock_factory injected at runtime
        with lock_factory("lock-name"):
            pass
    ```

    Any ideas?

    """

    # def __init__(self, id_: str, ttl: Optional[float] = None):
    #     """
    #     Abstract interface for a mutex implementation. `id_` should uniquely identify
    #     this lock between processes.
    #
    #     Parameters
    #     ----------
    #     id_ : str
    #     ttl : float
    #
    #     """
    #     pass

    @abc.abstractmethod
    def acquire(self, blocking: bool = True, timeout: Optional[float] = None) -> bool:
        """
        Attempt to acquire a hold on this lock.

        Parameters
        ----------
        blocking : bool
            True if the call to `acquire` should block indefinitely. If False, the call
            will attempt once to acquire the lock and return after `timeout` seconds if
            it is unable (default, True).
        timeout: Optional[float]
            Length (in seconds) to attempt to acquire the lock for before returning. If
            timeout is `None`, this method will only attempt to acquire the lock once.
            If `blocking` is `False` this argument must be `None` (default, None).

        Raises
        ------
        ValueError
            If `blocking` is passed as False and `timeout` is a positive number.
        ValueError
            If `timeout` is less than or equal to zero.

        """
        raise NotImplementedError

    @abc.abstractmethod
    def release(self) -> None:
        """Release this mutex."""
        raise NotImplementedError

    @abc.abstractmethod
    def locked(self) -> bool:
        """
        Return `True` if this lock is held, otherwise `False`.

        Should this return `False` if TTL has lapsed? Seems a slippery slope?

        """
        raise NotImplementedError

    def __enter__(self):
        self.acquire()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()


AbstractDistributedLockFactory = Callable[
    [str, Optional[float]], AbstractDistributedLock
]
