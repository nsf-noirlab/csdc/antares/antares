"""
Concrete subclasses for Redis concurrency tools
"""

import logging
import time
import uuid
from typing import Optional

from redis.client import Redis

from antares import utils

from .base import AbstractDistributedLock


class RedisDistributedLock(AbstractDistributedLock):
    """
    Redis distributed lock implementation.
    Highly recommended to use as context manager so cleanup happens.
    Warning: context manager will block forever and isn't configurable
    Warning: TTL behavior not fully implemented
    """

    def __init__(self, client: Redis, id_: str, ttl: Optional[float] = None):
        """
        ttl : Optional[float]
            Seconds to acquire a lease on this lock for. Callers should pass this value
            as floating point seconds and are limited to millisecond precision.

        """
        if not isinstance(id_, str):
            raise ValueError("Lock `id_` must be of type `str`")
        self._key: str = f"lock:{id_}"
        self._client: Redis = client
        self._unique_id: Optional[str] = None
        self._ttl: Optional[int] = None
        if ttl:
            self._ttl = int(ttl * 1000)  # Convert to integer milliseconds
        self._locked: bool = False

    def acquire(self, blocking: bool = True, timeout: Optional[float] = None):
        if not blocking and timeout:
            raise ValueError("can't specify a timeout for a non-blocking call")
        start_time = time.perf_counter()
        wait = 0.01  # 10ms
        wait_increase_factor = 1.5
        self._unique_id = str(uuid.uuid4())
        logging.info("Acquiring lock for %s (%s)", self._key, self._unique_id)
        while True:
            if self._client.set(self._key, self._unique_id, nx=True, px=self._ttl):
                self._locked = True
                logging.info("Acquired lock for %s (%s)", self._key, self._unique_id)
                return True
            if not blocking:
                logging.warning(
                    "Failed to acquire lock for %s (%s)", self._key, self._unique_id
                )
                return False
            if timeout and (time.perf_counter() - start_time) > timeout:
                logging.warning(
                    "Timed out waiting to acquire lock for %s (%s)",
                    self._key,
                    self._unique_id,
                )
                return False
            logging.warning(
                "Retry lock acquisition in %ds for %s (%s)",
                wait,
                self._key,
                self._unique_id,
            )
            time.sleep(wait)
            wait *= wait_increase_factor
            continue

    def release(self) -> None:
        logging.info("Releasing lock for %s (%s)", self._key, self._unique_id)
        unlock_script = utils.unindent(
            """
            if redis.call("get", KEYS[1]) == ARGV[1] then
                return redis.call("del", KEYS[1])
            else
                return 0
            end
            """
        ).strip()
        self._client.eval(unlock_script, 1, self._key, self._unique_id)
        self._locked = False
        logging.info("Released lock for %s (%s)", self._key, self._unique_id)

    def locked(self):
        return self._locked
