"""
Abstraction and implementation for message pub/sub.
I think this may be unneccessary and we should consider removing it.
"""

import abc
from collections.abc import Collection
from datetime import datetime
from typing import Any, Optional

import confluent_kafka

import antares.exceptions
from antares.external.kafka import KafkaConsumer, KafkaProducer, KafkaWatermarkOffset


class AbstractMessagePublicationService(abc.ABC):
    # pylint: disable=too-few-public-methods
    """ABC for Message Pub Service"""

    @abc.abstractmethod
    def publish(self, destination: str, message: Any):
        raise NotImplementedError


class AbstractMessageSubscriptionService(abc.ABC):
    # pylint: disable=too-few-public-methods
    """ABC for Message Sub Service"""

    @abc.abstractmethod
    def poll(
        self, timeout: Optional[float] = None
    ) -> tuple[str, Any, Optional[datetime]]:
        """
        Returns a (str, Any, datetime) tuple where the first element is the name of the
        queue or pubsub topic, the second element is the data member and the third
        element is the time at which the message was produced (if available).

        If the poll times out before receiving a message, concrete implementations
        should raise antares.exceptions.Timeout.
        """
        raise NotImplementedError


class KafkaMessagePublicationService(AbstractMessagePublicationService):
    # pylint: disable=too-few-public-methods
    """Kafka Message Pub Service"""

    def __init__(self, kafka_config: dict, create_missing_topics=True):
        self._producer = KafkaProducer(kafka_config, create_missing_topics)

    def publish(self, destination: str, message: Any):
        self._producer.produce(destination, message)
        self._producer.flush()


class KafkaMessageSubscriptionService(AbstractMessageSubscriptionService):
    """Kafka Message Sub Service
    TODO AN-1598: This code needs some refactoring, because multiple surveys
    can have different kafka configurations and endpoints. A solution can be
    to do multiple consumers, and they should be grouped by a configuration.
    """

    def __init__(self, kafka_config: dict, sources: Collection[str]):
        super().__init__()
        self._consumer = KafkaConsumer(kafka_config, topics=sources)

    def poll(
        self, timeout: Optional[float] = None
    ) -> tuple[str, Any, Optional[datetime]]:
        topic, message = self._consumer.poll(timeout)
        if topic is None:
            raise antares.exceptions.Timeout(
                f"No response from Kafka after {timeout} seconds"
            )
        timestamp_type, timestamp = message.timestamp()
        if timestamp_type == confluent_kafka.TIMESTAMP_NOT_AVAILABLE:
            timestamp = None
        else:
            timestamp = datetime.fromtimestamp(timestamp / 1000.0)
        return topic, message.value(), timestamp

    def commit(self):
        self._consumer.commit()

    def get_watermark_offsets(self) -> list[KafkaWatermarkOffset]:
        return self._consumer.get_watermark_offsets()
