import abc


class AbstractMetrics(abc.ABC):
    # Should this be abstracted one layer more and the repository should handle their own monitoring??
    # That's not the best approach because ...
    @abc.abstractmethod
    def increment_index_worker_documents_indexed(self, value: int):
        raise NotImplementedError

    @abc.abstractmethod
    def increment_stamp_extractor_alerts_received(self):
        raise NotImplementedError

    @abc.abstractmethod
    def increment_pipeline_alerts_received(self):
        raise NotImplementedError

    # Not even used
    # @abc.abstractmethod
    # def increment_pipeline_alerts_waiting(self):
    #    raise NotImplementedError

    @abc.abstractmethod
    def set_pipeline_alerts_processing_lag(self, value: float, topic: str):
        raise NotImplementedError

    @abc.abstractmethod
    def increment_pipeline_filter_run(self, filter_name: str):
        raise NotImplementedError

    @abc.abstractmethod
    def increment_pipeline_loci_sent_to_users(self, user_topic: str):
        raise NotImplementedError

    @abc.abstractmethod
    def increment_pipeline_multiple_nearby_loci(self):
        raise NotImplementedError

    @abc.abstractmethod
    def increment_repository_catalog_object_not_found(self, catalog_name: str):
        raise NotImplementedError
