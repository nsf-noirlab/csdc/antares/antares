import logging

from prometheus_client import CollectorRegistry, Counter, Gauge, start_http_server

from antares.observability.metrics.base import AbstractMetrics


class PrometheusMetrics(AbstractMetrics):
    def __init__(self):
        self.registry = CollectorRegistry()
        self.INDEX_WORKER_DOCUMENTS_INDEXED = Counter(
            "antares_index_worker_documents_indexed",
            "Number of documents indexed by the ANTARES index worker",
            registry=self.registry,
        )
        self.STAMP_EXTRACTOR_ALERTS_RECEIVED = Counter(
            "antares_stamp_extractor_alerts_received",
            "Number of alerts received by the ANTARES stamp extractor",
            registry=self.registry,
        )
        self.PIPELINE_ALERTS_RECEIVED = Counter(
            "antares_pipeline_alerts_received",
            "Number of alerts received by the ANTARES pipeline",
            registry=self.registry,
        )
        self.PIPELINE_ALERT_PROCESSING_LAG = Gauge(
            "antares_pipeline_alert_processing_lag",
            "Lag (in seconds) between when an alert is produced and ANTARES processes it",
            ["topic"],
            registry=self.registry,
        )
        self.PIPELINE_FILTERS_RUN = Counter(
            "antares_pipeline_filters_run",
            "Number of ANTARES pipeline filters executed",
            ["filter_name"],
            registry=self.registry,
        )
        self.PIPELINE_LOCI_SENT_TO_CLIENTS = Counter(
            "antares_pipeline_loci_sent_to_clients",
            "Number of loci sent to Kafka clients",
            ["topic"],
            registry=self.registry,
        )
        self.PIPELINE_MULTIPLE_NEARBY_LOCI = Counter(
            "antares_pipeline_multiple_nearby_loci",
            "How often we see multiple loci in association that are too close to disambiguate",
            registry=self.registry,
        )
        self.REPOSITORY_CATALOG_OBJECT_NOT_FOUND = Counter(
            "antares_repository_catalog_object_not_found",
            "How often an HTM LUT entry exists but there is not matching catalog object",
            ["catalog_name"],
            registry=self.registry,
        )
        try:
            start_http_server(port=9100)
        except OSError as e:
            logging.warning("Prometheus failed to start with %s", e)

    def increment_index_worker_documents_indexed(self, value: int):
        self.INDEX_WORKER_DOCUMENTS_INDEXED.inc(value)

    def increment_stamp_extractor_alerts_received(self):
        self.STAMP_EXTRACTOR_ALERTS_RECEIVED.inc()

    def increment_pipeline_alerts_received(self):
        self.PIPELINE_ALERTS_RECEIVED.inc()

    def set_pipeline_alerts_processing_lag(self, value: float, topic: str):
        self.PIPELINE_ALERT_PROCESSING_LAG.labels(topic).set(value)

    def increment_pipeline_filter_run(self, filter_name: str):
        self.PIPELINE_FILTERS_RUN.labels(filter_name).inc()

    def increment_pipeline_loci_sent_to_users(self, user_topic: str):
        self.PIPELINE_LOCI_SENT_TO_CLIENTS.labels(user_topic).inc()

    def increment_pipeline_multiple_nearby_loci(self):
        self.PIPELINE_MULTIPLE_NEARBY_LOCI.inc()

    def increment_repository_catalog_object_not_found(self, catalog_name: str):
        self.REPOSITORY_CATALOG_OBJECT_NOT_FOUND.labels(catalog_name).inc()
