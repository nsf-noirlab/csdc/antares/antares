from opentelemetry import metrics
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import (
    ConsoleMetricExporter,
    PeriodicExportingMetricReader,
)

from antares.observability.metrics.base import AbstractMetrics


class OpenTelemetryMetrics(AbstractMetrics):
    def __init__(self):
        metrics.set_meter_provider(
            MeterProvider(
                metric_readers=[
                    PeriodicExportingMetricReader(
                        ConsoleMetricExporter(),
                        export_interval_millis=1_000,  # Send metrics every 1 second
                    )
                ],
            )
        )
        self.meter = metrics.get_meter(__name__)
        self.INDEX_WORKER_DOCUMENTS_INDEXED = self.meter.create_counter(
            name="antares.index_worker.documents_indexed",
            description="Number of documents indexed by the ANTARES index worker",
        )
        self.STAMP_EXTRACTOR_ALERTS_RECEIVED = self.meter.create_counter(
            name="antares.stamp_extractor.alerts_received",
            description="Number of alerts received by the ANTARES stamp extractor",
        )
        self.PIPELINE_ALERTS_RECEIVED = self.meter.create_counter(
            name="antares.pipeline.alerts_received",
            description="Number of alerts received by the ANTARES pipeline",
            # Label to use can be topic
        )
        self.PIPELINE_ALERT_PROCESSING_LAG = self.meter.create_gauge(
            name="antares.pipeline.alert_processing_lag",
            unit="seconds",
            description="Lag between when an alert is produced and is processed",
            # Label to use is topic
        )

        # Not quite useful(?
        self.PIPELINE_FILTERS_RUN = self.meter.create_counter(
            name="antares.pipeline.filters_run",
            description="Number of ANTARES pipeline filters executed",
            # Label to use is filter_name
        )
        self.PIPELINE_LOCI_SENT_TO_CLIENTS = self.meter.create_counter(
            name="antares.pipeline.loci_sent_to_clients",
            description="Number of loci sent to Kafka clients",
            # Label to use is topic
        )
        self.PIPELINE_MULTIPLE_NEARBY_LOCI = self.meter.create_counter(
            name="antares.pipeline.multiple_nearby_loci",
            description="Times a loci are too close to disambiguate in association",
        )
        self.REPOSITORY_CATALOG_OBJECT_NOT_FOUND = self.meter.create_counter(
            name="antares.repository.catalog.object_not_found",
            description="How often HTM LUT entry exists but there's no matching catalog object",
            # Label to use is catalog_name
        )

    def increment_index_worker_documents_indexed(self, value: int):
        self.INDEX_WORKER_DOCUMENTS_INDEXED.add(value)

    def increment_stamp_extractor_alerts_received(self):
        self.STAMP_EXTRACTOR_ALERTS_RECEIVED.add(1)

    def increment_pipeline_alerts_received(self):
        self.PIPELINE_ALERTS_RECEIVED.add(1)

    def set_pipeline_alerts_processing_lag(self, value: float, topic: str):
        self.PIPELINE_ALERT_PROCESSING_LAG.set(value, {"broker_topic": topic})

    def increment_pipeline_filter_run(self, filter_name: str):
        self.PIPELINE_FILTERS_RUN.add(1, {"filter_name": filter_name})

    def increment_pipeline_loci_sent_to_users(self, user_topic: str):
        self.PIPELINE_LOCI_SENT_TO_CLIENTS.add(1, {"user_topic": user_topic})

    def increment_pipeline_multiple_nearby_loci(self):
        self.PIPELINE_MULTIPLE_NEARBY_LOCI.add(1)

    def increment_repository_catalog_object_not_found(self, catalog_name: str):
        self.REPOSITORY_CATALOG_OBJECT_NOT_FOUND.add(1, {"catalog_id": catalog_name})
