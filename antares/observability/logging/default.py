import logging


def init_default_logging(level="DEBUG"):
    logging.basicConfig(
        level=level.upper(),
        format="%(asctime)s - %(levelname)s %(threadName)s %(filename)s:%(funcName)s:%(lineno)d - %(message)s",
    )
