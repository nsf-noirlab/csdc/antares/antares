from typing import Any, Optional, Union

from ddtrace.opentracer import Tracer as ddtraceTracer
from ddtrace.opentracer import set_global_tracer
from ddtrace.opentracer.span import Span as ddtraceSpan

from antares.observability.tracing.base import AbstractSpan, AbstractTracer
from antares.observability.tracing.instrumentation.opentracing_redis import (
    instrument_opentracing_redis,
)
from antares.observability.tracing.instrumentation.opentracing_sqlalchemy import (
    instrument_opentracing_sqlalchemy,
)


class DatadogSpan(AbstractSpan):
    def __init__(self, span: ddtraceSpan):
        self.span = span
        self.active_span = span.span

    def set_attribute(self, key: str, value: Any):
        self.active_span.set_tag(key, value)

    def set_error(self, message: str, exception: Optional[Exception] = None):
        if not exception:
            exception = ValueError(message)
        self.active_span.set_tag("error", True)
        self.active_span.log_kv(
            {
                "event": "error",
                "error.kind": type(exception),
                "error.object": exception,
                "stack": exception.__traceback__,
                "message": message,
            }
        )

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.span.__exit__(exc_type, exc_value, traceback)


class DatadogTracer(AbstractTracer):
    def __init__(self, host, port, enabled):
        datadog_config = {
            "agent_hostname": host,
            "agent_port": port,
            "enabled": enabled,
        }
        self.tracer = ddtraceTracer("ddtrace_tracer", config=datadog_config)
        self.patch_for_instrumentation()
        set_global_tracer(self.tracer)
        self.instrument()

    def instrument(self):
        instrument_opentracing_redis(self.tracer)
        instrument_opentracing_sqlalchemy(self.tracer)

    def patch_for_instrumentation(self):
        log_kv_original = ddtraceSpan.log_kv
        set_tag_original = ddtraceSpan.set_tag

        def log_kv_wrapper(
            self: ddtraceSpan,
            key_values: dict[Union[str, bytes], Any],
            timestamp: Optional[float] = None,
        ) -> ddtraceSpan:
            """
            This is a wrapper to workaround a bug/limitation in Datadog's OpenTracing
            implementation.

            From https://github.com/DataDog/dd-trace-py/issues/2030#issuecomment-847427044
            """
            log_kv_original(self, key_values, timestamp)
            if key_values.get("event") == "error":
                try:
                    exc_type = key_values["error.kind"]
                    exc_val = key_values["error.object"]
                    exc_tb = key_values["stack"]

                    self._dd_span.set_exc_info(exc_type, exc_val, exc_tb)
                except KeyError:
                    # welp
                    pass
            return self

        def set_tag_wrapper(
            self: ddtraceSpan, key: Union[str, bytes], value: Any
        ) -> ddtraceSpan:
            """
            The datadog set_tag method returns None but this method returns
            the Span because sqlalchemy uses it when assigning tags
            to a query_span
            """
            set_tag_original(self, key, value)
            return self

        setattr(ddtraceSpan, "log_kv", log_kv_wrapper)
        setattr(ddtraceSpan, "set_tag", set_tag_wrapper)

    def trace(self, trace_name: str) -> AbstractSpan:
        return DatadogSpan(self.tracer.start_active_span(trace_name))
