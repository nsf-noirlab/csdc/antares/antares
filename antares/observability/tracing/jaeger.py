from typing import Any, Optional

from jaeger_client import Config
from opentracing import Span as opentracingSpan

from antares.observability.tracing.base import AbstractSpan, AbstractTracer
from antares.observability.tracing.instrumentation.opentracing_redis import (
    instrument_opentracing_redis,
)
from antares.observability.tracing.instrumentation.opentracing_sqlalchemy import (
    instrument_opentracing_sqlalchemy,
)


class JaegerSpan(AbstractSpan):
    def __init__(self, span: opentracingSpan):
        self.span = span
        self.active_span = span.span

    def set_attribute(self, key: str, value: Any):
        self.active_span.set_tag(key, value)

    def set_error(self, message: str, exception: Optional[Exception] = None):
        if not exception:
            exception = ValueError(message)
        self.active_span.set_tag("error", True)
        self.active_span.log_kv(
            {
                "event": "error",
                "error.kind": type(exception),
                "error.object": exception,
                "stack": exception.__traceback__,
                "message": message,
            }
        )

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.span.__exit__(exc_type, exc_value, traceback)
        # self.active_span.finish()


class JaegerTracer(AbstractTracer):
    def __init__(self, host: str, service_name: str):
        jaeger_config = Config(
            config={
                "sampler": {
                    "type": "const",
                    "param": 1,
                },
                "local_agent": {
                    "reporting_host": host,
                    "reporting_port": "6831",
                },
            },
            service_name=service_name,
            validate=True,
        )
        self.tracer = jaeger_config.initialize_tracer()
        while self.tracer is None:
            Config._initialized = False
            self.tracer = jaeger_config.initialize_tracer()
        self.instrument()

    def instrument(self):
        instrument_opentracing_redis(self.tracer)
        instrument_opentracing_sqlalchemy(self.tracer)

    def trace(self, trace_name: str) -> AbstractSpan:
        if self.tracer is None:
            raise ValueError("Can't create an span without tracer")
        return JaegerSpan(self.tracer.start_active_span(trace_name))
