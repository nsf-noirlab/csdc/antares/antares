"""
Modified from https://github.com/zalando-zmon/opentracing-utils, originally licensed
under MIT (reproduced below with original copyright notice).

The MIT License (MIT)

Copyright (c) 2017 Zalando SE, https://tech.zalando.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

"""

import opentracing
from opentracing.ext import tags
from sqlalchemy.engine import Engine
from sqlalchemy.event import listens_for


def trace_sqlalchemy(
    tracer=None,
    operation_name=None,
    skip_span=None,
    enrich_span=None,
    use_scope_manager=False,
):
    """
    Trace Sqlalchemy database queries.
    :param tracer:
    :param operation_name: Callable to return the operation name of the query. By
                           default, operation_name will be the clause of the SQL
                           statement (e.g. select, update, delete).
    :type operation_name: Callable[conn, cursor, statement, parameters, context,
    executemany]
    :param skip_span: Callable to determine whether to skip this SQL query(ies)
                      spans. If returned ``True`` then span will be skipped.
    :type skip_span: Callable[conn, cursor, statement, parameters, context, executemany]
    :param enrich_span: Callable to enrich the span with additional data.
    :type enrich_span: Callable[span, conn, cursor, statement, parameters, context,
    executemany]
    :param use_scope_manager: Always use the scope manager when starting the span.
    :type use_scope_manager: bool
    """
    tracer = tracer or opentracing.tracer

    @listens_for(Engine, "before_cursor_execute")
    def trace_before_cursor_execute(
        conn, cursor, statement, parameters, context, executemany
    ):
        if callable(skip_span) and skip_span(
            conn, cursor, statement, parameters, context, executemany
        ):
            return

        parent_span = None
        using_scope_manager = False
        try:
            parent_span = tracer.active_span
            using_scope_manager = True if parent_span else False
        except AttributeError:
            pass

        if context:
            op_name = statement.split(" ")[0].lower() or "query"
            if callable(operation_name):
                op_name = operation_name(
                    conn, cursor, statement, parameters, context, executemany
                )

            query_span = tracer.start_span(operation_name=op_name, child_of=parent_span)

            if context:
                (
                    query_span.set_tag(tags.COMPONENT, "antares")
                    .set_tag("db.type", "sql")
                    .set_tag("db.engine", context.dialect.name)
                    .set_tag("db.statement", statement)
                )

                if callable(enrich_span):
                    enrich_span(
                        query_span,
                        conn,
                        cursor,
                        statement,
                        parameters,
                        context,
                        executemany,
                    )

                if use_scope_manager or using_scope_manager:
                    scope = tracer.scope_manager.activate(
                        query_span, finish_on_close=True
                    )
                    context._query_scope = scope

                context._query_span = query_span

    @listens_for(Engine, "after_cursor_execute")
    def trace_after_cursor_execute(
        conn, cursor, statement, parameters, context, executemany
    ):
        if hasattr(context, "_query_scope"):
            context._query_scope.close()
        elif hasattr(context, "_query_span"):
            context._query_span.finish()

    @listens_for(Engine, "handle_error")
    def trace_handle_error(exception_context):
        context = exception_context.execution_context
        if hasattr(context, "_query_span"):
            context._query_span.log_kv(
                {"exception": str(exception_context.original_exception)}
            )
            context._query_span.set_tag("error", True)
        if hasattr(context, "_query_scope"):
            context._query_scope.close()
        elif hasattr(context, "_query_span"):
            context._query_span.finish()


"""Added code which was not copy/pasted below"""


def enrich_span(span, *args):
    span.set_tag("service.name", "mariadb")


def operation_name(conn, cursor, statement, parameters, context, executemany):
    return statement


def instrument_opentracing_sqlalchemy(tracer):
    trace_sqlalchemy(
        tracer=tracer,
        enrich_span=enrich_span,
        operation_name=operation_name,
    )
