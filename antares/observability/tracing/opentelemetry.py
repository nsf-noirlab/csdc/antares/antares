from typing import Any, Optional

from opentelemetry import trace
from opentelemetry.instrumentation.redis import RedisInstrumentor
from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor
from opentelemetry.sdk.trace import Span, TracerProvider
from opentelemetry.sdk.trace.export import ConsoleSpanExporter, SimpleSpanProcessor
from opentelemetry.trace import Status, StatusCode

from antares.observability.tracing.base import AbstractSpan, AbstractTracer


class OpenTelemetrySpan(AbstractSpan):
    def __init__(self, span: Span):
        self.span = span
        self.active_span = None

    def set_attribute(self, key: str, value: Any):
        if self.active_span is None:
            raise ValueError("Can't assign attribute to non active span")
        self.active_span.set_attribute(key, value)

    def set_error(self, message: str, exception: Optional[Exception] = None):
        if self.active_span is None:
            raise ValueError("Can't assign attribute to non active span")
        if exception:
            self.active_span.record_exception(exception)
        self.active_span.set_status(Status(StatusCode.ERROR, message))

    def __enter__(self):
        self.active_span = self.span.__enter__()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.span.__exit__(exc_type, exc_value, traceback)


class OpenTelemetryTracer(AbstractTracer):
    def __init__(self):
        self.provider = TracerProvider()
        trace.set_tracer_provider(self.provider)
        self.provider.add_span_processor(SimpleSpanProcessor(ConsoleSpanExporter()))
        self.tracer = trace.get_tracer(__name__)
        self.instrument()

    def intrument(self):
        RedisInstrumentor().instrument()
        SQLAlchemyInstrumentor().instrument()

    def trace(self, trace_name: str) -> AbstractSpan:
        return OpenTelemetrySpan(self.tracer.start_as_current_span(trace_name))
