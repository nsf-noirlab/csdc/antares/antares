import abc
from typing import Any, Optional


class AbstractSpan(abc.ABC):
    @abc.abstractmethod
    def set_attribute(self, key: str, value: Any):
        raise NotImplementedError

    @abc.abstractmethod
    def set_error(self, message: str, exception: Optional[Exception] = None):
        raise NotImplementedError

    @abc.abstractmethod
    def __enter__(self):
        raise NotImplementedError

    @abc.abstractmethod
    def __exit__(self, exc_type, exc_value, traceback):
        raise NotImplementedError


class AbstractTracer(abc.ABC):
    @abc.abstractmethod
    def trace(self, trace_name: str) -> AbstractSpan:
        raise NotImplementedError
