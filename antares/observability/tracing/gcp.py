import os
from typing import Any, Optional

from opentelemetry import trace
from opentelemetry.exporter.cloud_trace import CloudTraceSpanExporter
from opentelemetry.instrumentation.redis import RedisInstrumentor
from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor
from opentelemetry.sdk.resources import (
    SERVICE_INSTANCE_ID,
    SERVICE_NAME,
    SERVICE_VERSION,
    Resource,
)
from opentelemetry.sdk.trace import Span, TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace import Status, StatusCode

import antares
from antares.observability.tracing.base import AbstractSpan, AbstractTracer


class GCPSpan(AbstractSpan):
    def __init__(self, span: Span):
        self.span = span
        self.active_span = None

    def set_attribute(self, key: str, value: Any):
        if self.active_span is None:
            raise ValueError("Can't assign attribute to non active span")
        self.active_span.set_attribute(key, value)

    def set_error(self, message: str, exception: Optional[Exception] = None):
        if self.active_span is None:
            raise ValueError("Can't assign attribute to non active span")
        if exception:
            self.active_span.record_exception(exception)
        self.active_span.set_status(Status(StatusCode.ERROR, message))

    def __enter__(self):
        self.active_span = self.span.__enter__()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.span.__exit__(exc_type, exc_value, traceback)


class GCPTracer(AbstractTracer):
    def __init__(self, service_name: str):
        # From https://opentelemetry.io/docs/specs/semconv/resource/
        self.provider = TracerProvider(
            resource=Resource.create(
                {
                    SERVICE_NAME: service_name,
                    SERVICE_VERSION: str(antares.__version__),
                    # Use the PID as the service.instance.id to avoid duplicate timeseries
                    # from different Gunicorn worker processes.
                    SERVICE_INSTANCE_ID: f"worker-{os.getpid()}",
                }
            )
        )
        trace.set_tracer_provider(self.provider)
        self.provider.add_span_processor(BatchSpanProcessor(CloudTraceSpanExporter()))
        self.tracer = trace.get_tracer(service_name)
        self.instrument()

    def instrument(self):
        RedisInstrumentor().instrument()
        SQLAlchemyInstrumentor().instrument()

    def trace(self, trace_name: str) -> AbstractSpan:
        return GCPSpan(self.tracer.start_as_current_span(trace_name))
