import math
import numbers
from collections.abc import Container, MutableMapping, MutableSet


class RestrictedWriteDictWrapper(MutableMapping):
    def __init__(self, wrapped_dict, writeable_keys: Container = ()):
        self.dict = wrapped_dict
        self.writeable_keys = writeable_keys

    def __setitem__(self, key, value):
        if key not in self.writeable_keys:
            raise KeyError(f"Attempt to set key '{key}' not specified writeable")
        if isinstance(value, numbers.Number) and math.isnan(value):
            raise ValueError(
                "Writing key '{key}' with numeric values using NaN is not allowed"
            )
        self.dict[key] = value

    def __getitem__(self, key):
        return self.dict[key]

    def __contains__(self, key):
        return key in self.dict

    def __delitem__(self, key):
        del self.dict[key]

    def __iter__(self):
        return iter(self.dict)

    def __len__(self):
        return len(self.dict)


class RestrictedWriteSetWrapper(MutableSet):
    def __init__(self, wrapped_set: set, writeable_elements: Container = ()):
        self.set = wrapped_set
        self.writeable_elements = writeable_elements

    def __contains__(self, item):
        return item in self.set

    def __len__(self):
        return len(self.set)

    def __iter__(self):
        return iter(self.set)

    def add(self, element) -> None:
        if element not in self.writeable_elements:
            raise ValueError(
                f"Attempt to add element '{element}' not specified writeable"
            )
        self.set.add(element)

    def discard(self, element) -> None:
        if element not in self.writeable_elements:
            raise ValueError(
                f"Attempt to discard element '{element}' not specified writeable"
            )
        self.set.discard(element)
