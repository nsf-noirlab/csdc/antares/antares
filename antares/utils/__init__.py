import datetime
import math
import random
import tempfile
from collections.abc import Callable, Iterable
from io import StringIO
from typing import Any, Literal, Optional

import numpy as np
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord
from astropy.table import Table

from antares.utils.validation import (
    validate_dec_degree,
    validate_ra_degree,
    validate_ra_degree_180,
    validate_values_are_sorted,
)


def astropy_table_to_string(
    table: Table,
    format_: Literal["csv"] = "csv",
    include: Optional[list[str]] = None,
    exclude: Optional[list[str]] = None,
) -> str:
    """
    Convert an astropy.table.Table object to a string representation.

    Parameters
    ----------
    table : astropy.table.Table
    format_ : Literal["csv"] (default, "csv")
        The format for the writer to output the string as, defaults to CSV.
    include : Optional[list[str]] (default, None)
        A subset of columns to include in the output string. Only columns in this list
        will be included. Only one of `include` or `exclude` may be specified.
    exclude : Optional[list[str]] (default, None)
        A subset of columns to exclude from the output string. All columns not in this
        list will be included. Only one of `include` or `exclude` may be specified.

    Returns
    -------
    str

    Raises
    ------
    ValueError
        If both `include` and `exclude` are passed as arguments.

    """
    if include and exclude:
        raise ValueError("Only one of `include` or `exclude` may be passed.")
    columns = list(table.columns)
    if include:
        columns = [c for c in columns if c in include]
    if exclude:
        columns = [c for c in columns if c not in exclude]
    buffer = StringIO()
    table[columns].write(buffer, format=format_)
    return buffer.getvalue()


def astropy_table_from_string(raw: str, format_: Literal["csv"] = "csv") -> Table:
    """
    Loads an astropy.table.Table from string.

    Parameters
    ----------
    raw : str
    format_ : Literal["csv"] (default, "csv")
        Format for reader to use to parse the input string.

    Returns
    -------
    astropy.table.Table

    """
    with tempfile.NamedTemporaryFile("r+") as f:
        f.write(raw)
        f.seek(0)
        return Table.read(f.name, format=format_)


def none_if_nan(x):
    """
    Return None if x is NaN, else return x.

    >>> none_if_nan(3.1)
    3.1
    >>> none_if_nan(float('nan')) is None
    True

    :param x:
    :return:
    """
    if isinstance(x, float) and np.isnan(x):
        return None
    if x == float("nan"):
        return None
    return x


def within(p1, objs, angle, key=lambda x: x):
    """
    Find all objects in `objs` that are within `angle` degrees of `p1`.
    Returns `(separation, object)` tuples, sorted on increasing separation values.
    """
    objects_within = []
    p1 = SkyCoord(ra=p1[0], dec=p1[1], unit=u.deg)
    angle = Angle(angle, unit=u.deg)
    for o in objs:
        ra, dec = key(o)
        p2 = SkyCoord(ra=ra, dec=dec, unit=u.deg)
        separation = p1.separation(p2)
        if separation <= angle:
            objects_within.append((separation, o))
    objects_within.sort(key=lambda x: x[0])
    return objects_within


def nearest(
    p1: SkyCoord,
    objs: Iterable[Any],
    key: Callable[[Any], SkyCoord] = lambda x: x,
    max_angle: Angle = None,
) -> Optional[Any]:
    """
    Return the index of the point in `points` that is nearest to `p1`.

    All points must be (ra, dec) tuples in fractional degrees.

    >>> nearest(SkyCoord("0d 0d"),
               [SkyCoord("0d 1d"), SkyCoord("1d 1d"), SkyCoord("0.5d 0.5d")])
    SkyCoord("0.5d 0.5d")
    >>> nearest(SkyCoord("0d 0d"), [SkyCoord("1d 1d")])
    SkyCoord("1d 1d")
    >>> nearest(SkyCoord("0d 0d"), [SkyCoord("1d 1d")], max_angle=Angle("0.5d")) is None
    True

    :param p1: (ra, dec) tuple
    :param objs: objects to select the nearest if
    :param key: func which maps an element of `objs` to an (ra, dec) tuple.
    :param max_angle: maximum angle to consider, in degrees.
    :return: index of nearest point in `points` to `p1`
    """
    objects_within = []
    for o in objs:
        ptest = key(o)
        separation = p1.separation(ptest)
        if (max_angle is not None) and separation > max_angle:
            continue
        objects_within.append((separation, o))
    if not objects_within:
        return None
    objects_within.sort(key=lambda x: x[0])
    _, nearest_object = objects_within[0]
    return nearest_object


def angle_between(p1, p2):
    """
    Return angle in degrees between two (ra, dec) points.

    >>> angle_between((0, 0), (0, 1))
    1.0

    :param p1: tuple like (ra, dec) in degrees
    :param p2: tuple like (ra, dec) in degrees
    :return: angle in degrees
    """
    p1 = SkyCoord(ra=p1[0], dec=p1[1], unit=u.deg)
    p2 = SkyCoord(ra=p2[0], dec=p2[1], unit=u.deg)
    angle = p1.separation(p2)
    return angle.degree


def seconds_until_end_of_utc_day():
    """
    Compute number of seconds until end of current UTC day.

    :return: int
    """
    # All times here are in UTC
    utc_now = datetime.datetime.utcnow()
    last_midnight = utc_now.replace(hour=0, minute=0, second=0, microsecond=0)
    this_midnight = last_midnight + datetime.timedelta(days=1)
    delta_until_midnight = this_midnight - utc_now
    return delta_until_midnight.total_seconds()


def unindent(s):
    """
    Unindent a block of text.

    Indentation is determined from the number of spaces at beginning of first
    non-empty line. All lines then have that many characters removed.

    >>> unindent("    foo")
    'foo'
    >>> unindent('    foo\\n        bar\\n')
    'foo\\n    bar\\n'
    >>> unindent('\\n    foo')
    '\\nfoo'

    :param s: multi-line string of text
    :return: un-indented version
    """
    lines = s.split("\n")
    first_line = None
    for line in lines:
        if line.strip():
            first_line = line
            break
    if first_line is None:
        return ""
    n_spaces = 0
    for char in first_line:
        if char == " ":
            n_spaces += 1
        else:
            break
    return "\n".join(line[n_spaces:] for line in lines)


def group_by(iterable, key):
    """
    Group objects into a dict of lists by a key.

    :param iterable:
    :param key: attribute name, or a function
    :return: dict
    """
    get_key = key if callable(key) else lambda x: getattr(x, key)
    grouped = {}
    for obj in iterable:
        grouped.setdefault(get_key(obj), []).append(obj)


def format_dt(utc_datetime):
    """
    A system-wide standard timestamp format.
    """
    return utc_datetime.isoformat(sep=" ")


def timestamp():
    return format_dt(datetime.datetime.utcnow())


def jd_to_mjd(jd):
    """
    Convert JD to MJD.
    """
    return jd - 2400000.5


def mjd_to_jd(mjd):
    """
    Convert MJD to JD.
    """
    return mjd + 2400000.5


def mjd_to_datetime(mjd) -> datetime.datetime:
    """
    Convert MJD to Datetime.
    """
    from astropy.time import Time

    return Time([mjd], format="mjd")[0].to_datetime()


def dt_to_mjd(utc_datetime):
    """
    Convert UTC Datetime to MJD.
    """
    from astropy.time import Time

    return Time([utc_datetime.isoformat()], format="isot", scale="utc")[0].mjd


def randfloat(min_, max_):
    """
    Return a random float from a range.
    """
    validate_values_are_sorted(min_, max_)
    return (random.random() * (max_ - min_)) + min_


def get_median_coords(points):
    """
    Return the median of a list of (ra, dec) points.

    :param points: list of (ra, dec) points, in units of degrees.
    :return: (ra, dec) tuple, in units of degrees.

    note: this function was used to calculate centroids but to avoid
    outliers began to use the median instead of mean in the
    calculation
    """
    # Compute x, y, z vectors
    x, y, z = [], [], []
    for ra, dec in points:
        validate_ra_degree(ra)
        validate_dec_degree(dec)
        ra = math.radians(ra)
        dec = math.radians(dec)
        x.append(math.cos(dec) * math.cos(ra))
        y.append(math.cos(dec) * math.sin(ra))
        z.append(math.sin(dec))

    # Compute the median xyz vector of all input xyz vectors
    x, y, z = np.median(x), np.median(y), np.median(z)

    # Convert xyz vector to ra/dec in degrees
    r = np.linalg.norm(np.array([x, y, z]))  # Norm (length) of xyz vector
    ra = math.degrees(math.atan2(y, x))
    dec = 90 - math.degrees(math.acos(z / r))

    # Final range checking
    validate_ra_degree_180(ra)
    if ra < 0:
        ra += 360
    validate_ra_degree(ra)
    validate_dec_degree(dec)
    if dec == -90 or dec == 90:
        # ra is undefined when dec is -90 or 90.
        ra = 0
    return ra, dec


def format_traceback(code, traceback, file_name):
    """
    Format a traceback which originated from eval()'d code.

    When using eval(), tracebacks don't include the source code lines,
    and the file name shows up as '<string>'.

    This function places the source code lines into the stacktrace,
    making it readable as if it originated from normal Python code.

    :param code: raw code in which the exception occurred
    :param traceback: string of traceback as reported by traceback.format_exc()
    :param file_name: name to replace '<string>' in traceback
    :return: str
    """
    lines = traceback.split("\n")
    out_lines = []
    call_stack_line_header = 'File "<string>", line '
    for line in lines:
        line_num = None
        if line.strip().startswith(call_stack_line_header):
            line_num = int(
                line.strip()[len(call_stack_line_header) :].split()[0].strip(",")
            )
        if line_num is None:
            out_lines.append(line)
            continue
        line = line.replace('File "<string>", line', f'Filter "{file_name}", line')
        out_lines.append(line)
        err_line = code.split("\n")[line_num - 1].strip()
        out_lines.append("    " + err_line)
    output = "\n".join(out_lines)
    return output
