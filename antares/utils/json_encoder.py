import datetime
import json
from decimal import Decimal


class ANTARESJsonEncoder(json.JSONEncoder):
    """
    Encodes the following types as strings:
    - datetime.date
    - datetime.datetime
    - Decimal
    """

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            s = obj.isoformat()
            if len(s) == 19:
                # There is no timezone attached to this ISO 8601 string
                return s + "+00:00"  # GMT
            else:
                # There is a timezone, eg: "2015-02-15T19:39:11-08:00" (PST)
                #                          "2015-02-15T19:39:11+00:00" (GMT)
                #                          "2015-02-15T19:39:11Z"      (GMT)
                return s
        if isinstance(obj, datetime.date):
            return obj.isoformat()
        if isinstance(obj, Decimal):
            return str(obj)
        return super().default(obj)


def json_dumps(
    o, indent=None, sort_keys=False, cls=ANTARESJsonEncoder, allow_nan=False, **kw
):
    return json.dumps(
        o, indent=indent, sort_keys=sort_keys, cls=cls, allow_nan=allow_nan, **kw
    )
