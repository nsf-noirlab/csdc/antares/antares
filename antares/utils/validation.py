def validate_ra_degree(ra_degree):
    if 0 > ra_degree > 360:
        raise ValueError(f"ra_degree '{ra_degree}' is not between 0 and 360")


def validate_ra_degree_180(ra_degree):
    if -180 > ra_degree > 180:
        raise ValueError(f"ra_degree '{ra_degree}' is not between -180 and 180")


def validate_dec_degree(dec_degree):
    if -90 > dec_degree > 90:
        raise ValueError(f"dec_degree '{dec_degree}' is not between -90 and 90")


def validate_values_are_sorted(min_, max_):
    if min_ > max_:
        raise ValueError(
            f"Minimum value '{min_}' can't be greater than the maximum value '{max_}'"
        )


def validate_lists_are_equal_length(list_a, list_b):
    if len(list_a) != len(list_b):
        raise ValueError(f"Unequal lists of length {len(list_a)} and {len(list_b)}")
