from datetime import datetime


def parse_datetime(datetime_str: str, fmts: list[str]) -> datetime:
    for fmt in fmts:
        try:
            return datetime.strptime(datetime_str, fmt)
        except ValueError:
            pass
    raise ValueError(f"Unparsable Datetime String {datetime_str}")
