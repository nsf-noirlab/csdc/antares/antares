import htm
from astropy.coordinates import Angle, SkyCoord

from antares.utils.validation import validate_values_are_sorted


def get_htm_region(
    location: SkyCoord, radius: Angle, level: int = 20
) -> list[tuple[int, int]]:
    """Return a list of HTM (min, max) ranges describing a circular region."""
    try:
        region = htm.get_htm_circle_region(
            location.ra.deg,
            location.dec.deg,
            radius.deg,
            level=level,
        )
    except htm.PrecisionError:
        # Our HTM library occasionally encounters precision issues. We work around this
        # by detecting them and trying again with a slightly increased radius. The
        # returned set may be larger than necessary but it won't miss any trixels that
        # should be included.
        region = htm.get_htm_circle_region(
            location.ra.deg,
            location.dec.deg,
            radius.deg + 1e-6,
            level=level,
        )
    return region


def get_htm_region_denormalized(
    location: SkyCoord, radius: Angle, level: int
) -> list[int]:
    """
    Get or compute a search region as a full list of htm values.

    Where get_region returns something like [(20, 22), (24, 25)]), this
    function would yield from the series (20, 21, 22, 24, 25).
    """

    def f():
        for htm_min, htm_max in get_htm_region(location, radius, level):
            validate_values_are_sorted(htm_min, htm_max)
            for h in range(htm_min, htm_max + 1):
                yield h

    return list(f())


def get_htm_id(location: SkyCoord, level: int) -> int:
    """Compute the HTM ID of a point at depth `level` in the HTM sphere."""
    return htm.get_htm_id(location.ra.deg, location.dec.deg, level)
