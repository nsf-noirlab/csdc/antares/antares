import logging
import os
import random
import string
from typing import Optional

from dependency_injector import containers, providers
from google.cloud.bigtable.instance import Instance as BigtableInstance
from redis import Redis

import antares
from antares.adapters.concurrency import (
    AbstractDistributedLockFactory,
    RedisDistributedLock,
)
from antares.adapters.messages import KafkaMessagePublicationService
from antares.adapters.notifications import (
    EmailNotificationService,
    InternalSlackApiNotificationService,
    SlackApiNotificationService,
)
from antares.adapters.repository import (
    BigtableAlertRepository,
    BigtableCatalogObjectRepository,
    BigtableLocusRepository,
    BigtableWatchListRepository,
    BigtableWatchObjectRepository,
    GcsAlertThumbnailRepository,
    SqlAlchemyAnnouncementRepository,
    SqlAlchemyBlobRepository,
    SqlAlchemyFilterRepository,
    SqlAlchemyFilterRevisionRepository,
    SqlAlchemyGravWaveRepository,
    SqlAlchemyJwtBlocklistRepository,
    SqlAlchemyLocusAnnotationRepository,
    SqlAlchemyUserRepository,
)
from antares.external.bigtable.bigtable import wait_for_bigtable
from antares.external.sql.engine import (
    get_session_factory as sqlalchemy_session_factory_factory,
)
from antares.observability.logging.default import init_default_logging
from antares.observability.logging.gcp import init_gcp_logging
from antares.observability.metrics.gcp import GCPMetrics
from antares.observability.metrics.prometheus import PrometheusMetrics
from antares.observability.tracing.datadog import DatadogTracer
from antares.observability.tracing.gcp import GCPTracer
from antares.observability.tracing.jaeger import JaegerTracer
from antares.utils import mjd_to_datetime


def default_locus_id_factory(alert_packet) -> str:
    # Get the year of the most recent candidate alert in this alert packet
    year = mjd_to_datetime(alert_packet.alerts[-1].mjd).year
    if not isinstance(year, int):
        raise ValueError(f"Year '{year}' must be an integer")
    if len(str(year)) != 4:
        raise ValueError(f"Year must be exactly 4 digits, found {year}")
    random_string = "".join(
        random.SystemRandom().choice(string.ascii_lowercase + string.digits)
        for _ in range(12)
    )
    return f"ANT{year}{random_string}"


def bigtable_instance_factory(project_id: str, instance_id: str) -> BigtableInstance:
    logging.info("Establishing connection to Bigtable")
    instance = wait_for_bigtable(project_id, instance_id)
    logging.info("Connection to Bigtable established")

    return instance


def redis_distributed_lock_factory_factory(
    host: str, port: int, db: int
) -> AbstractDistributedLockFactory:
    client = Redis(host, port, db=db)

    def redis_distributed_lock_factory(
        id_: str, ttl: Optional[float] = None
    ) -> RedisDistributedLock:
        return RedisDistributedLock(client, id_, ttl=ttl)

    return redis_distributed_lock_factory


class Container(containers.DeclarativeContainer):
    default_directory = os.path.dirname(antares.__file__)
    config = providers.Configuration(
        yaml_files=[os.path.join(default_directory, "default.yaml")],
    )
    setup_logging = providers.Resource(
        providers.Selector(
            config.observability.type,
            jaeger=providers.Resource(
                init_default_logging,
                level=config.observability.logging.level,
            ),
            datadog=providers.Resource(
                init_default_logging,
                level=config.observability.logging.level,
            ),
            gcp=providers.Resource(
                init_gcp_logging,
                level=config.observability.logging.level,
            ),
        )
    )
    tracer = providers.Singleton(
        providers.Selector(
            config.observability.type,
            jaeger=providers.Singleton(
                JaegerTracer,
                service_name=config.observability.tracing.parameters.service,
                host=config.observability.tracing.parameters.host,
            ),
            datadog=providers.Singleton(
                DatadogTracer,
                service=config.observability.tracing.parameters.service,
                host=config.observability.tracing.parameters.host,
                port=config.observability.tracing.parameters.port,
                enabled=config.observability.tracing.enabled,
            ),
            gcp=providers.Singleton(
                GCPTracer,
                service_name=config.observability.tracing.parameters.service,
            ),
        )
    )
    metrics = providers.Singleton(
        providers.Selector(
            config.observability.type,
            jaeger=providers.Singleton(
                PrometheusMetrics,
            ),
            datadog=providers.Singleton(
                PrometheusMetrics,
            ),
            gcp=providers.Singleton(
                GCPMetrics,
                service_name=config.observability.tracing.parameters.service,
                export_interval_millis=config.observability.tracing.parameters.export_interval_millis,
            ),
        )
    )
    bt_schemas = config.external.bigtable.schemas
    alert_repository = providers.Singleton(
        providers.Selector(
            config.repositories.alert.datastore,
            bigtable=providers.Singleton(
                BigtableAlertRepository,
                instance_=providers.Factory(
                    bigtable_instance_factory,
                    project_id=config.external.bigtable.parameters.project_id,
                    instance_id=config.external.bigtable.parameters.instance_id,
                ),
                alert_table_id=bt_schemas.alert.table_name,
                alert_cf=bt_schemas.alert.column_family_id,
                locus_by_alert_id_table_id=bt_schemas.locus_by_alert_id.table_name,
                locus_by_alert_id_cf=bt_schemas.locus_by_alert_id.column_family_id,
            ),
        )
    )
    alert_thumbnail_repository = providers.Singleton(
        GcsAlertThumbnailRepository,
        bucket_url=config.repositories.thumbnail.parameters.bucket_url,
    )
    announcement_repository = providers.Singleton(
        SqlAlchemyAnnouncementRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
    )
    blob_repository = providers.Singleton(
        SqlAlchemyBlobRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
    )
    catalog_object_repository = providers.Singleton(
        providers.Selector(
            config.repositories.catalog.datastore,
            bigtable=providers.Singleton(
                BigtableCatalogObjectRepository,
                instance_=providers.Factory(
                    bigtable_instance_factory,
                    project_id=config.external.bigtable.parameters.project_id,
                    instance_id=config.external.bigtable.parameters.instance_id,
                ),
                catalog_lut_id=bt_schemas.catalog_lut.table_name,
                catalog_lut_cf=bt_schemas.catalog_lut.column_family_id,
                schemas=bt_schemas.catalog.tables,
                schema_file=config.repositories.catalog.schema_file,
            ),
        ),
    )
    filter_repository = providers.Singleton(
        SqlAlchemyFilterRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
    )
    filter_revision_repository = providers.Singleton(
        SqlAlchemyFilterRevisionRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
    )
    grav_wave_repository = providers.Singleton(
        SqlAlchemyGravWaveRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
        lookback_days=config.repositories.grav_wave_notice.lookback_days,
    )
    locus_repository = providers.Singleton(
        providers.Selector(
            config.repositories.locus.datastore,
            bigtable=providers.Singleton(
                BigtableLocusRepository,
                instance_=providers.Factory(
                    bigtable_instance_factory,
                    project_id=config.external.bigtable.parameters.project_id,
                    instance_id=config.external.bigtable.parameters.instance_id,
                ),
                locus_table_id=bt_schemas.locus.table_name,
                locus_cf=bt_schemas.locus.column_family_id,
                alert_lut_id=bt_schemas.alert_lut.table_name,
                alert_lut_cf=bt_schemas.alert_lut.column_family_id,
            ),
        )
    )
    locus_annotation_repository = providers.Singleton(
        SqlAlchemyLocusAnnotationRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
    )
    jwt_blocklist_repository = providers.Singleton(
        SqlAlchemyJwtBlocklistRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
    )
    user_repository = providers.Singleton(
        SqlAlchemyUserRepository,
        session_factory=providers.Factory(sqlalchemy_session_factory_factory),
    )
    watch_list_repository = providers.Singleton(
        providers.Selector(
            config.repositories.watch_list.datastore,
            bigtable=providers.Singleton(
                BigtableWatchListRepository,
                instance_=providers.Factory(
                    bigtable_instance_factory,
                    project_id=config.external.bigtable.parameters.project_id,
                    instance_id=config.external.bigtable.parameters.instance_id,
                ),
                watch_list_table_id=bt_schemas.watch_list.table_name,
                watch_list_cf=bt_schemas.watch_list.column_family_id,
            ),
        )
    )
    watch_object_repository = providers.Singleton(
        providers.Selector(
            config.repositories.watch_object.datastore,
            bigtable=providers.Singleton(
                BigtableWatchObjectRepository,
                instance_=providers.Factory(
                    bigtable_instance_factory,
                    project_id=config.external.bigtable.parameters.project_id,
                    instance_id=config.external.bigtable.parameters.instance_id,
                ),
                watch_object_table_id=bt_schemas.watch_object.table_name,
                watch_object_cf=bt_schemas.watch_object.column_family_id,
                alert_lut_id=bt_schemas.alert_lut.table_name,
                alert_lut_cf=bt_schemas.alert_lut.column_family_id,
            ),
        )
    )
    notification_service = providers.Singleton(
        providers.Selector(
            config.notifications.type,
            email=providers.Singleton(
                EmailNotificationService,
                smtp_host=config.external.smtp.host,
                smtp_port=config.external.smtp.port,
                smtp_sender=config.external.smtp.sender,
                smtp_username=config.external.smtp.username,
                smtp_password=config.external.smtp.password,
            ),
            slack=providers.Singleton(
                SlackApiNotificationService,
                api_token=config.external.slack.api_token,
            ),
        )
    )
    internal_notification_service = providers.Singleton(
        providers.Selector(
            config.internal_notifications.type,
            email=providers.Singleton(
                EmailNotificationService,
                smtp_host=config.external.internal_smtp.host,
                smtp_port=config.external.internal_smtp.port,
                smtp_sender=config.external.internal_smtp.sender,
                smtp_username=config.external.internal_smtp.username,
                smtp_password=config.external.internal_smtp.password,
            ),
            slack=providers.Singleton(
                InternalSlackApiNotificationService,
                api_token=config.external.internal_slack.api_token,
                environment=config.environment,
                frontend_base_url=config.frontend.base_url,
                notification_channels=config.internal_notifications.channels,
                user_repository=user_repository,
            ),
        )
    )
    message_publication_service = providers.Singleton(
        KafkaMessagePublicationService,
        kafka_config=config.pipeline.output.broker.settings,
    )
    locus_id_factory = providers.Factory(default_locus_id_factory)
    lock_factory = providers.Factory(
        redis_distributed_lock_factory_factory,
        host=config.external.redis.host,
        port=config.external.redis.port,
        db=config.external.redis.db,
    )
