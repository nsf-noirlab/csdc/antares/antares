from contextlib import contextmanager

from dependency_injector.wiring import Provide, inject
from sqlalchemy import create_engine as _create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool, QueuePool

# Config
DEFAULT_ENGINE = "antares"


# SQLAlchemy `Engine` objects
_engines: dict[str, Engine] = {}


def close():
    """
    Close all connections and sessions.
    """
    for e in _engines.values():
        e.dispose()
    _engines.clear()


@inject
def create_engine(
    url: str = Provide["config.external.sql.url"],
    parameters: dict = Provide["config.external.sql.parameters"],
    echo: bool = Provide["config.external.sql.echo"],
):
    engine_params = (parameters or {}).copy()
    if engine_params.get("pool_size") == 0:
        # The QueuePool takes pool_size=0 to mean "unlimited".
        # So, to implement a pool of size 0 we use the NullPool instead.
        poolclass = NullPool
        # The NullPool does not accept the following params:
        engine_params.pop("pool_size", None)
        engine_params.pop("max_overflow", None)
        engine_params.pop("pool_recycle", None)
    else:
        poolclass = QueuePool
    return _create_engine(
        url,
        echo=echo,
        echo_pool=echo,
        poolclass=poolclass,
        **engine_params,
    )


def get_engine(name=DEFAULT_ENGINE):
    if name not in _engines:
        _engines[name] = create_engine()
    return _engines[name]


def get_session_factory(name=DEFAULT_ENGINE):
    return sessionmaker(bind=get_engine(name=name), autoflush=False)


def get_session(name=DEFAULT_ENGINE):
    """Return an SQLAlchemy Session for the DB.

    Caller is responsible for closing the Session.

    Returns:
        `sqlalchemy.orm.session.Session`
    """
    return get_session_factory(name)()


@contextmanager
def session(name=DEFAULT_ENGINE):
    """
    Get an Alert DB Session using a contextmanager.

    eg:
    with alert_db_session() as s:
        s.execute('...')

    """
    s = get_session(name=name)
    try:
        yield s
    finally:
        s.close()
