import logging

import sqlalchemy_utils

from . import engine

__all__ = ["drop_main_db"]


def validate_database_url(url: str):
    if not url.endswith("_dev") and not url.endswith("_staging"):
        raise ValueError(
            f"Can't drop database '{url}': doesn't end with '_dev' or '_staging'"
        )


def drop_main_db():
    e = engine.get_engine(name=engine.DEFAULT_ENGINE)
    url = str(e.url)  # convert SQLAlchemy URL class to str
    validate_database_url(url)
    drop_db(url)


def drop_db(url):
    validate_database_url(url)
    logging.info("Dropping DB: %s", url)
    if sqlalchemy_utils.database_exists(url):
        sqlalchemy_utils.drop_database(url)
