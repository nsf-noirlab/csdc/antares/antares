import logging

from confluent_kafka.admin import AdminClient
from confluent_kafka.cimpl import NewTopic


class KafkaAdmin(object):
    def __init__(self, kafka_config):
        self._kafka_config = kafka_config
        self._admin = AdminClient(kafka_config)

    def create_topic(self, name, operation_timeout=5):
        """
        Create a new Topic.
        """
        # Request to create new topic
        logging.info("Creating Kafka topic '%s'", name)
        futures = self._admin.create_topics(
            [NewTopic(name, num_partitions=1, replication_factor=1)],
            operation_timeout=operation_timeout,
        )
        # Wait for response
        for _, future in futures.items():
            future.result()  # Raises exception if request failed

    def delete_topic(self, topic, force=False):
        """
        Delete a topic.
        """
        if not force and not topic.endswith("_dev") and not topic.endswith("_staging"):
            raise ValueError(
                f"Kafka topic {topic} can't be deleted: "
                "doesn't ends with '_dev' nor '_staging' and force is not True"
            )
        logging.info("Deleting Kafka topic '%s'", topic)
        # Request to delete topic
        futures = self._admin.delete_topics([topic], operation_timeout=30)
        # Wait for response
        for _, future in futures.items():
            future.result()  # Raises exception if request failed
        topic_names = self.list_topic_names()
        if topic in topic_names:
            raise ValueError(f"Topic '{topic}' can't be within {topic_names}")

    def delete_all_topics(self, force=False):
        for topic in self.list_topic_names():
            if not topic.startswith("_"):
                self.delete_topic(topic, force)

    def print_status(self, topics=True, partitions=False):
        """
        Print Kafka broker information.

        From:
        https://github.com/confluentinc/confluent-kafka-python/blob/ab161a3193bd390eb9a3d01c0ef7a088ab12e796/examples/adminapi.py#L235
        """
        metadata = self.list_topics()
        print(
            "Cluster {} metadata (response from broker {}):".format(
                metadata.cluster_id, metadata.orig_broker_name
            )
        )
        print(" {} brokers:".format(len(metadata.brokers)))
        for broker in iter(metadata.brokers.values()):
            if broker.id == metadata.controller_id:
                print("  {}  (controller)".format(broker))
            else:
                print("  {}".format(broker))

        if topics:
            print(" {} topics:".format(len(metadata.topics)))
            for topic in iter(metadata.topics.values()):
                if topic.error is not None:
                    errstr = ": {}".format(topic.error)
                else:
                    errstr = ""
                print(
                    '  "{}" with {} partition(s){}'.format(
                        topic, len(topic.partitions), errstr
                    )
                )
                if partitions:
                    for partition in iter(topic.partitions.values()):
                        if partition.error is not None:
                            errstr = ": {}".format(partition.error)
                        else:
                            errstr = ""
                        print(
                            "partition {} leader: {}, replicas: {}, isrs: {}".format(
                                partition.id,
                                partition.leader,
                                partition.replicas,
                                partition.isrs,
                            )
                        )

    def list_topics(self, timeout=10):
        return self._admin.list_topics(timeout=timeout)

    def list_topic_names(self, timeout=10):
        """
        Return list of names of all topics which exist.
        """
        return [str(topic) for topic in self.list_topics(timeout).topics.values()]
