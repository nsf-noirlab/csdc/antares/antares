import logging

import backoff
from confluent_kafka import Producer
from confluent_kafka.cimpl import KafkaException

from antares.external.kafka.admin import KafkaAdmin


class TopicDoesNotExist(Exception):
    pass


class KafkaProducer(object):
    def __init__(self, kafka_config, create_missing_topics=True):
        self._kafka_config = kafka_config
        self._producer = Producer(kafka_config)
        self._create_missing_topics = create_missing_topics
        self._known_topics = set()

    @backoff.on_exception(backoff.expo, BufferError, max_tries=10)
    def produce(self, topic, value, callback=None):
        self._ensure_topic(topic)
        logging.debug("Sending messsage to topic '%s'", topic)
        try:
            self._producer.produce(topic, value=value, callback=callback)
        except BufferError:
            # Queue is full
            self._producer.flush()
            raise  # Try again

    def flush(self):
        self._producer.flush()

    def _ensure_topic(self, topic):
        """
        Create a topic if it does not exist.

        :param topic:
        """
        if topic in self._known_topics:
            return
        a = KafkaAdmin(self._kafka_config)
        self._known_topics = set(a.list_topic_names())
        if topic in self._known_topics:
            return
        if not self._create_missing_topics:
            raise TopicDoesNotExist
        try:
            logging.info("Creating Kafka topic '%s'", topic)
            a.create_topic(topic)
            self._known_topics.add(topic)
        except KafkaException as e:
            if "TOPIC_ALREADY_EXISTS" in str(e):
                self._known_topics.add(topic)
            else:
                raise
