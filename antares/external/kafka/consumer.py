import itertools
import logging
import time
import uuid
import warnings
from collections.abc import Callable
from dataclasses import dataclass
from typing import Any, Optional, Union

from confluent_kafka import Consumer
from confluent_kafka.cimpl import (  # pylint: disable=no-name-in-module
    KafkaError,
    KafkaException,
    Message,
)
from gcn_kafka import Consumer as GCNConsumer


@dataclass(frozen=True)
class KafkaWatermarkOffset:
    topic: str
    partition: int
    low: int
    high: int


class KafkaConsumer(object):
    _POLLING_FREQUENCY = 1.0
    _STATS_INTERVAL_MS = 10 * 1000  # 10 seconds

    def __init__(
        self,
        kafka_config,
        topics=None,
        group=None,
        random_group=False,
        stats_cb=None,
    ):
        """
        Set up a KafkaConsumer.

        :param kafka_config: config dict, or None to use default config
        :param topics: list of topics to subscribe to
        :param random_group: if True, generate a random 'group.id'
        :param stats_cb: if given, call this "stats callback" periodically
        """
        if group:
            kafka_config["group.id"] = group
        if random_group:
            kafka_config["group.id"] = str(uuid.uuid4())
        if stats_cb:
            kafka_config["statistics.interval.ms"] = self._STATS_INTERVAL_MS
            kafka_config["stats_cb"] = stats_cb
        self._kafka_config = kafka_config
        if kafka_config and kafka_config.get("client_id"):
            self._consumer = GCNConsumer(
                kafka_config, logger=logging.getLogger("kafka_consumer")
            )
        else:
            self._consumer = Consumer(
                kafka_config, logger=logging.getLogger("kafka_consumer")
            )
        if topics:
            self._consumer.subscribe(topics)

    def iter(self, num_alerts=None):
        """
        Yield from ANTARES alert streams.

        Parameters
        -----------
        num_alerts: int
            Maximum number of alerts to yield. If None, yield alerts
            indefinitely (default, None).

        Yields
        ----------
        (topic, alert): str, dict

        """
        for i in itertools.count(start=1, step=1):
            yield self.poll()
            if num_alerts and i >= num_alerts:
                return

    def _timed_poll(
        self, timeout: float, message_parser: Callable[[Message], Any] = lambda msg: msg
    ) -> Union[tuple[None, None], tuple[str, Any]]:
        start_time = time.perf_counter()
        while (time.perf_counter() - start_time) < timeout:
            try:
                message = self._consumer.poll(timeout=self._POLLING_FREQUENCY)
                if message is not None:
                    if message.error():
                        raise KafkaException(message.error(), message.topic())
                    topic = message.topic()
                    parsed_message = message_parser(message)
                    return topic, parsed_message
            except KafkaException as e:
                error = e.args[0]
                # pylint: disable=protected-access
                if error.code() == KafkaError._PARTITION_EOF:
                    pass
                elif error.code() == KafkaError.UNKNOWN_TOPIC_OR_PART:
                    warnings.warn(
                        f"The topic you subscribed to ({e.args[1]}) does not exist"
                    )
                # pylint: disable=protected-access
                elif error.code() == KafkaError._TIMED_OUT:
                    exception_fmt = "There was an error connecting to ANTARES: {}"
                    raise ConnectionError(exception_fmt.format(error))
                else:
                    exception_fmt = "There was an error consuming from ANTARES: {}"
                    raise RuntimeError(exception_fmt.format(error))
        return None, None

    def poll(
        self,
        timeout: Optional[float] = None,
        message_parser: Callable[[Message], Any] = lambda msg: msg,
    ) -> tuple[Optional[str], Any]:
        """
        Retrieve a single alert. This method blocks until ``timeout``
        seconds have elapsed (by default, an infinite amount of time).

        Parameters
        ----------
        timeout: Optional[float]
            Number of seconds to block waiting for an alert. If None,
            block indefinitely (default, None).

        Returns
        ----------
        (topic, alert): (str, dict)
            Or ``(None, None)`` if ``timeout`` seconds elapse with no response

        """
        if timeout:
            return self._timed_poll(timeout, message_parser)
        while True:
            topic, alert = self._timed_poll(self._POLLING_FREQUENCY, message_parser)
            if alert:
                return topic, alert

    def consume(self, **kwargs):
        return self._consumer.consume(**kwargs)

    def subscribe(self, topics, **kwargs):
        self._consumer.subscribe(topics, **kwargs)

    def list_topics(self, **kwargs):
        return self._consumer.list_topics(**kwargs)

    def commit(self, **kwargs):
        self._consumer.commit(**kwargs)

    def close(self, **kwargs):
        self._consumer.close(**kwargs)

    def get_watermark_offsets(self) -> list[KafkaWatermarkOffset]:
        assignment = self._consumer.assignment()
        offsets = []
        for partition in assignment:
            low, high = self._consumer.get_watermark_offsets(partition)
            offsets.append(
                KafkaWatermarkOffset(partition.topic, partition.partition, low, high)
            )
        return offsets

    def reset_offsets(self):
        # This is for testing only.
        assignment = self._consumer.assignment()
        for partition in assignment:
            partition.offset = 0
            self._consumer.seek(partition)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()
