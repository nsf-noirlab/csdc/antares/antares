import logging
import math
import numbers
import time
import uuid
from typing import Mapping

import elasticsearch
import elasticsearch.helpers
from dependency_injector.wiring import Provide, inject
from elasticsearch import Elasticsearch, NotFoundError

from antares.deprecated import serialize
from antares.external import kafka

PYTHON_TYPE_STR_TO_ES_TYPE = {str: "text", float: "float", int: "long"}
MAPPING_CACHE_KEY = str(uuid.uuid1())  # Unique to this Python process
LARGEST_POSITIVE_AVAILABLE_IN_ES_FOR_FLOATS = (2 - 2**-23) * 2**127


@inject
def connect(
    hosts: list[str] = Provide["config.external.elasticsearch.hosts"],
    options: Mapping = Provide["config.external.elasticsearch.parameters"],
    test_connection=False,
):
    logging.info("Connecting to ElasticSearch %s", hosts)
    elasticsearch_client = Elasticsearch(hosts, **options)
    if test_connection and not elasticsearch_client.ping():
        raise ConnectionError("Unable to connect to Elasticsearch")
    logging.info("Connection to ElasticSearch %s established.", hosts)
    return elasticsearch_client


def wait_for_online():
    # Wait for ES to come online
    while True:
        try:
            connect(test_connection=True)
            break
        except ConnectionError:
            logging.info("Waiting for ES to come online...")
            time.sleep(5)
            continue


def _request(method, url, params=None, body=None):
    params = params or {}
    return connect().transport.perform_request(
        method,
        url,
        params=params,
        body=body,
        headers={"Content-Type": "application/json"},
    )


def describe_index(index):
    return _request("GET", f"/{index}")


def get_aliases():
    return _request("GET", "/_aliases")


def get_all_documents(index, n=1000):
    """
    Get all (<= n) documents in an index. USE ONLY IN TESTS.
    """
    return _request("GET", f"/{index}/_search/?size={n}")["hits"]["hits"]


def reindex(source, dest):
    return _request(
        "POST", "/_reindex", body={"source": {"index": source}, "dest": {"index": dest}}
    )


def delete_index(index: str):
    """
    Delete an index if it exists, else return None.
    """
    if not index.endswith("_dev") and not index.endswith("_staging"):
        raise ValueError(
            "Index {index} cann't be deleted: doesn't end with '_dev' or '_staging'"
        )
    logging.info("Deleting index '%s'", index)
    try:
        return _request("DELETE", f"/{index}")
    except NotFoundError:
        return


def get_mappings(index):
    r = _request("GET", f"/{index}")
    (name,) = list(r.keys())
    return r[name]["mappings"]


def put_mappings(index, mappings):
    properties = "properties"
    if properties not in mappings:
        raise ValueError(f"{properties} not found in mappings")
    mappings_properties = mappings[properties]
    if not isinstance(mappings_properties, dict):
        raise ValueError(
            f"mappings_properties must be a dict. Current value '{mappings_properties}'"
        )
    return _request("PUT", f"/{index}/_mapping", body=mappings)


def create_index(index, mappings):
    return _request("PUT", f"/{index}", body={"mappings": mappings})


def _generate_bulk_index_actions_from_kafka(
    kafka_client,
    index,
    max_chunk_size=1000,
    kafka_poll_timeout=None,
    document_id_getter=lambda _: uuid.uuid1(),
    message_parser=lambda msg: msg,
):
    """
    Generate ElasticSearch bulk actions.

    By default this will yield alert index actions from the topics that
    `kafka_client` is subscribed to infinitely. This may or may not be
    desired behavior depending on how these actions are used. In fact
    it probably isn't desired behavior but we make it the default interface
    because it is likely what the caller expects.

    Consider passing this generator to the `elasticsearch.helpers.bulk`
    bulk operation helper: that function takes a parameter `max_chunk_size`
    that is, by default, 500. It will consume items from a generator until
    either it has received `max_chunk_size` items or the generator has raised
    a `StopIteration`--then it will send the actions to the Elasticsearch
    server.

    This is fine if your input stream is high-volume and uniform, if you'd
    like to minimize HTTP requests or if indexing documents is not
    time-critical. The other benefit is that the `bulk()` function can
    be passed the default construction of this generator and it will index
    documents indefinitely.

    The way that we use it requires additional flow control though. Our desired
    behavior is: fetch up to `max_chunk_size` documents to index--if there
    aren't `max_chunk_size` documents available, wait enough time to know that
    we haven't just hit some network latency and then send the chunk of
    documents that we have, regardless of its size.

    The approximate worst case is that after receiving its first alert,
    exhausting the generator takes (in seconds):

       kafka_poll_timeout * (max_chunk_size - 1)

    For range of values that elasticsearch supports for floats see
    https://www.elastic.co/guide/en/elasticsearch/reference/current/number.html#_which_type_should_i_use
    """
    # The Kafka client doesn't provide a way to explicitly establish
    # a connection to streams--it happens implicitly on the first
    # call to poll. The first call to poll in this generator function
    # will wait to receive a message indefinitely.
    #
    # Once we've established a connection we can be reasonably sure
    # that if we have to wait a long time to get a message from the Kafka
    # stream there is nothing new to receive.
    #
    for i in range(max_chunk_size):
        timeout = None if i == 0 else kafka_poll_timeout
        _, document = kafka_client.poll(timeout, message_parser=message_parser)
        if document is None:
            break

        # Remove None values from document
        for k, v in list(document.items()):
            if v is None or (isinstance(v, numbers.Number) and math.isnan(v)):
                del document[k]
        for k, v in list(document["properties"].items()):
            if (
                v is None
                or (isinstance(v, numbers.Number) and math.isnan(v))
                or (
                    isinstance(v, float)
                    and v >= LARGEST_POSITIVE_AVAILABLE_IN_ES_FOR_FLOATS
                )
            ):
                del document["properties"][k]

        # Yield a batch operation
        yield {
            "_op_type": "index",
            "_index": index,
            "_type": "_doc",
            "_id": document_id_getter(document),
            "_source": document,
        }


def index_alerts_from_kafka(
    from_kafka_topics,
    to_elasticsearch_index,
    tracer,
    metrics,
    max_chunk_size=1000,
    kafka_config=None,
    kafka_poll_timeout=None,
):
    """
    Indefinitely index alerts from Kafka topics.

    N.B.: I think that an incorrect Kafka API key/secret pair will cause
    a silent error that, symptomatically, will look as if you're connected
    to a topic with no messages in it.

    :raises: ConnectionError, elasticsearch.helpers.BulkIndexError
    """
    elasticsearch_client = connect(test_connection=True)
    with kafka.KafkaConsumer(kafka_config, from_kafka_topics) as kafka_client:
        logging.info("Subscribed to Kafka topics: %s", from_kafka_topics)
        logging.info(
            "Subscribed to Kafka partitions: %s",
            kafka_client._consumer.position(kafka_client._consumer.assignment()),
        )
        while True:
            with tracer.trace("ingest_batch") as span:
                actions = _generate_bulk_index_actions_from_kafka(
                    kafka_client,
                    to_elasticsearch_index,
                    max_chunk_size=max_chunk_size,
                    kafka_poll_timeout=kafka_poll_timeout,
                    message_parser=lambda msg: serialize.loads(msg.value()),
                    document_id_getter=lambda doc: doc["locus_id"],
                )
                try:
                    number_documents_indexed, _ = elasticsearch.helpers.bulk(
                        elasticsearch_client, actions, chunk_size=max_chunk_size
                    )
                    metrics.increment_index_worker_documents_indexed(
                        number_documents_indexed
                    )
                    logging.info("Indexed %d documents", number_documents_indexed)
                except ConnectionError as e:
                    span.set_error("Network Exception", e)
                    raise
                except elasticsearch.helpers.BulkIndexError as e:
                    span.set_error("Elasticsearch Exception", e)
                    raise
                kafka_client.commit()
