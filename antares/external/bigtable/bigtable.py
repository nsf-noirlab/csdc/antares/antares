import logging
import os
import sys
import time

import google.cloud.bigtable as bigtable
import grpc
from dependency_injector.wiring import Provide, inject
from google.cloud.bigtable.instance import Instance as BigtableInstance


def exception_capture_sleep(e: Exception):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    if exc_tb:
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(
            "ERROR : %s\n\tIN: %s, %s, %s",
            e,
            exc_type,
            fname,
            exc_tb.tb_lineno,
            exc_info=e,
        )
    else:
        logging.error("ERROR : %s\n\tIN: %s", e, str(exc_type), exc_info=e)
    sleep_time = 5
    logging.error("Sleeping %d seconds", sleep_time)
    time.sleep(sleep_time)


def replace_emulator_channel_fn(transport, options):
    """
    Used to work round a Bigtable emulator fault that occurs when running
        inside containers see
        https://github.com/GoogleCloudPlatform/cloud-sdk-docker/issues/253#issuecomment-972247899
        for details on why we do this
    """
    return grpc.insecure_channel(target=os.environ["BIGTABLE_EMULATOR_HOST"])


@inject
def wait_for_bigtable(
    # fmt: off
    project_id: str,
    instance_id: str,
    timeout: float = Provide["config.external.bigtable.parameters.timeout"],
    retry_max_attempts: int = Provide["config.external.bigtable.parameters.retry.max"],
    retry_delay: int = Provide["config.external.bigtable.parameters.retry.delay"],
    n=300,
    default_timeout=None
    # fmt: on
) -> BigtableInstance:
    giveup_time = time.time() + n

    while True:
        try:
            logging.info(
                "Waiting for Bigtable." "project_id: %s instance_id: %s",
                project_id,
                instance_id,
            )
            client = bigtable.Client(project=project_id, admin=True)
            if running_emulator := os.getenv("BIGTABLE_EMULATOR_HOST"):
                logging.info("Running Bigtable emulator  %s", str(running_emulator))
                # see https://github.com/GoogleCloudPlatform/cloud-sdk-docker/issues/253#issuecomment-972247899
                # for details on why we do this
                setattr(client, "_emulator_channel", replace_emulator_channel_fn)

            instance = client.instance(instance_id)
            logging.info("Bigtable instance.display_name = %s", instance.display_name)
            logging.info("Tables:")
            logging.info([x.name for x in instance.list_tables()])
            logging.info("---")
            logging.info("Bigtable is alive (responded to list_tables())")
            break
        except Exception as e:
            exception_capture_sleep(e)
        if time.time() > giveup_time:
            raise RuntimeError(
                "Bigtable did not respond within the configured"
                f" {giveup_time} seconds, giving up"
            )

    return instance
