from collections.abc import Iterable

from dependency_injector.wiring import Provide, inject

from antares.adapters.repository import AbstractBlobRepository


@inject
def load_files(
    # fmt: off
    storage_keys: Iterable[str],
    blob_repository: AbstractBlobRepository = Provide["blob_repository"],
    # fmt: on
):
    blobs = [blob_repository.get(key) for key in storage_keys]
    return {blob.id: blob.data for blob in blobs if blob}
